
		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config.asp" -->
<%
	on_idx = request.QueryString("on_idx")
	menucode = request.QueryString("menucode")

	sql = "SELECT * FROM " & tablename & " WHERE on_idx = '" & on_idx & "'"

	Call DbOpen()

	Set rs = dbconn.execute(sql)

	If Not(rs.eof) Then

		on_idx			= rs("on_idx")
		on_status 		= rs("on_status")
		on_type 		= rs("on_type")
		on_cate 		= rs("on_cate")
		on_company 		= rs("on_company")
		on_name			= rs("on_name")
		on_mobile 		= rs("on_mobile")
		on_email 		= rs("on_email")
		on_title 		= rs("on_title")
		on_content 		= rs("on_content")
		on_file1 		= rs("on_file1")
		on_file2 		= rs("on_file2")

		re_name 		= rs("re_name")
		re_content 		= rs("re_content")
		regdate 		= rs("regdate")
		replydate		= rs("replydate")
		g_idx			= rs("g_idx")
	Else
		response.Write("코드에러")
	End If

	If on_type = "estimate1" Then
		on_type_txt = "견적요청(일반)"
	ElseIf on_type = "estimate2" Then
		on_type_txt = "견적요청(솔루션)"
	ElseIf on_type = "tech" Then
		on_type_txt = "기술문의"
	ElseIf on_type = "demo" Then
		on_type_txt = "데모장비 신청 및 테스트의뢰"
	ElseIf on_type = "etc" Then
		on_type_txt = "기타문의"
	End If

	If on_status = "Y" Then
		on_status_txt = "답변완료"
	Else
		on_status_txt = "접수완료"
	End If

	If re_name = "" Or isnull(re_name) Then
		re_name = session("adusername")
	End If

	rs.close()
'	Set rs = Nothing

'	Call DbClose()
%>
			<article>
			<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>문의정보</h2>
					</header>

					<!-- widget div-->
					<div class="mt50">

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<% If g_idx <> "" Then %>
						<table class="od_cart_table">
							<colgroup>
								<col style="width:5%">
								<col style="width:15%">
								<col style="width:*">
								<col style="width:35%">
							</colgroup>
							<thead>
								<tr>
									<th>No</th>
									<th>이미지</th>
									<th>제품명</th>
									<th>옵션</th>
								</tr>
							</thead>
							<tbody>
<%
	sql = "SELECT * FROM GOODS WHERE g_type = 1 AND g_display <> 'srm' AND g_act = 'Y' AND g_idx IN (" & g_idx & ")"
	Set rs = dbconn.execute(sql)


	If Not rs.eof Then
		i = 1
		Do While Not rs.eof
%>
								<tr>
									<td class="ta_center"><%=i%></td>
									<td class="thumb"><div class="img_box">
										<img src="/upload/goods/<%=rs("g_simg")%>">
									</div></td>
									<td class="subject">
										<dl>
											<dt><%=rs("g_name")%></dt>
											<dd class="code">제품코드 # <span><%=rs("g_idx")%></span></dd>
										</dl>
									</td>
<%
		g_optionList = rs("g_optionList")
		g_optionGroup	= rs("g_optionGroup")

		If g_optionList <> "" Then

			strSQL = "SELECT ogroup_content FROM cate_option_group WHERE ogroup_idx = " & g_optionGroup & ""
			Set optRS = dbconn.execute(strSQL)

			If Not optRS.eof Then
				ogroup_content = optRS("ogroup_content")

				arr_ogroup_content = Split(ogroup_content, ", ")
			End If

			optRS.close
%>
									<td class="option_">
<%
			For j = 0 To Ubound(arr_ogroup_content)
				sql = "SELECT opt_name, opt_content FROM cate_option WHERE opt_name = '" & Trim(arr_ogroup_content(j)) & "'"
				Set optRS = dbconn.execute(sql)

				If Not optRS.eof Then
					opt_name = optRS("opt_name")
					opt_content = optRS("opt_content")

					arr_opt_content = Split(opt_content, ", ")
				End If

				optRS.close
				Set optRS = Nothing

				strSQL = "SELECT g_idx, g_optionList FROM board_online_goods WHERE g_idx = " & rs("g_idx") & " AND on_idx = " & on_idx & ""
				Set bogRS = dbconn.execute(strSQL)

				If bogRS("g_optionList") <> "" Then
					arr_g_optionList1 = Split(bogRS("g_optionList"), ",")
					arr_g_optionList2 = Split(arr_g_optionList1(j), "||")

%>
										<div class="ot_div">
											<span><%=opt_name%></span>
											<select class="AXSelect" name="g_optionList_<%=i%>_<%=j+1%>" id="g_optionList_<%=i%>_<%=j+1%>">
												<option value="">-- 선택<%=arr_g_optionList2(1)%> --</option>
										<% For k = 0 To Ubound(arr_opt_content) %>
												<option value="<%=opt_name%>||<%=arr_opt_content(k)%>" <% If arr_opt_content(k) = Trim(arr_g_optionList2(1)) Then %> selected<% End If %>><%=arr_opt_content(k)%></option>
										<% Next %>
											</select>
										</div>
<%
				End If

				bogRS.close
				Set bogRS = Nothing
			Next
		End If

		i = i + 1
		rs.MoveNext
	Loop
End If
%>
									</td>
								</tr>
							</tbody>
						</table>
						<% End If %>

						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>구분</th>
										<td><%=on_type_txt%></td>
										<th>분류</th>
										<td><%=on_cate%></td>
									</tr>
									<tr>
										<th><% If on_type = "demo" Then %>멤버십 인증번호<% Else %>담당자<% End If %></th>
										<td><%=on_name%></td>
										<th>회사명</th>
										<td><%=on_company%></td>
									</tr>
									<tr>
										<th>휴대폰번호</th>
										<td><%=on_mobile%></td>
										<th>이메일</th>
										<td><%=on_email%></td>
									</tr>
									<tr>
										<th>작성일</th>
										<td colspan="3"><%=regdate%></td>
									</tr>
									<tr>
										<th>문의제목</th>
										<td colspan="3"><%=on_title%></td>
									</tr>
									<tr>
										<th>문의내용</th>
										<td colspan="3"><%=Replace(on_content, Chr(10), "<br>")%></td>
									</tr>
									<tr>
										<th>첨부파일</th>
										<td colspan="3">
										<% If on_file1 <> "" Then%>
											<a href="/download.asp?fn=<%=escape(on_file1)%>&ph=online"><%=on_file1%></a>
										<% End If %>
										<% If on_file2 <> "" Then%>
											<br><a href="/download.asp?fn=<%=escape(on_file2)%>&ph=online"><%=on_file2%></a>
										<% End If %>
										</td>
									</tr>
								</tbody>

							</table>
						</div>
					</div>
				</div>

				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>답변내용</h2>
					</header>

					<div class="mt50">
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<form name="replyform" method="post" action="./online_exec.asp" onsubmit="reply_chk();return false;">
							<input type="hidden" name="n" value="<%=on_idx%>">
							<input type="hidden" name="m" value="upt">
							<input type="hidden" name="on_name" value="<%=on_name%>">
							<input type="hidden" name="on_email" value="<%=on_email%>">
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>담당자</th>
										<td colspan="3"><input type="text" name="re_name" id="re_name" value="<%=re_name%>"></td>
									</tr>
								<% If replydate <> "" Then %>
									<tr>
										<th>답변일</th>
										<td colspan="3"><%=replydate%></td>
									</tr>
								<% End If %>
									<tr>
										<th>답변내용</th>
										<td colspan="3"><%call Editor("re_content",re_content)%></td>
									</tr>
								</tbody>
							</table>

							<footer style="float:right;">
								<input type="submit" class="btn btn-primary" value="답변등록">
								<a href="./online_list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
							</footer>

							</form>
						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->
			</article>
			<!-- WIDGET END -->

		<script>
			$(document).ready(function() {
				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			});
		</script>

		<!-- #include file="../inc/footer.asp" -->