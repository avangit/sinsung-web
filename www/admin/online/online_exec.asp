<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp" -->

<%
on_idx		= SQL_Injection(Trim(RequestS("n")))
mode		= SQL_Injection(Trim(RequestS("m")))
on_name		= SQL_Injection(Trim(RequestS("on_name")))
on_email	= SQL_Injection(Trim(RequestS("on_email")))
re_name		= SQL_Injection(Trim(RequestS("re_name")))
re_content	= SQL_Injection(Trim(RequestS("re_content")))

Call DbOpen()

If mode = "upt" Then

	Sql = "UPDATE " & tablename & " SET "
	Sql = Sql & "on_status = 'Y', "
	Sql = Sql & "re_name = N'" & re_name & "', "
	Sql = Sql & "re_content = N'" & re_content & "', "
	Sql = Sql & "replydate = GETDATE() "
	Sql = Sql & " WHERE on_idx = '" & on_idx & "'"

	dbconn.execute(Sql)
	dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&session("aduserid")&"','"&on_name&"','문의답변등록','문의답변등록','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

	Call HtmlMailSend_2003(FromMail, on_email, "", "[신성CNS] 문의주신 내용에 대한 답변입니다.", re_content, "")

	Call jsAlertMsgUrl("답변이 등록되었습니다.", "./online_view.asp?menucode="&menucode&"&on_idx="&on_idx&"")

ElseIf mode = "del" Then
	on_idx = Replace(on_idx, " ", ",")

	dbconn.execute("DELETE FROM " & tablename & " WHERE on_idx IN (" & on_idx & ")")
	dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES ('"&session("aduserid")&"','"&on_name&"','문의삭제','문의삭제','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

	Call jsAlertMsgUrl("삭제되었습니다.", "./online_list.asp?menucode="&menucode)
Else

	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.", "/")
End If

Call DbClose()
%>