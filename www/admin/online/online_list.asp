<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->
<!-- #include file="./config.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<style>
.hasDatepicker {
	position: relative;
	z-index: 9999;
}
</style>
<article>
<script src="/admin/js/jquery-2.1.1.min.js"></script>
  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>문의리스트</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(RequestS("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(RequestS("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request.QueryString("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= "*"
Dim query_Tablename		: query_Tablename	= tablename
Dim query_where			: query_where		= " on_idx > 0 "
Dim query_orderby		: query_orderby		= " ORDER BY on_idx DESC"

sdate		= SQL_Injection(Trim(RequestS("sdate")))
edate 		= SQL_Injection(Trim(RequestS("edate")))
on_type 	= SQL_Injection(Trim(RequestS("on_type")))
on_cate 	= SQL_Injection(Trim(RequestS("on_cate")))
isMember 	= SQL_Injection(Trim(RequestS("isMember")))
on_status 	= SQL_Injection(Trim(RequestS("on_status")))

If sdate <> "" And edate <> "" Then
	query_where = query_where &  " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day,1,'"&edate&"')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where &  " AND regdate >= '" & sdate & "'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where &  " AND regdate <= DATEADD(day,1,'" & edate & "')"
End If

If on_type <> "" Then
	query_where = query_where &  " AND on_type = '" & on_type &"'"
End If

If on_cate <> "" Then
	query_where = query_where &  " AND on_cate = '" & on_cate &"'"
End If

If isMember <> "" Then
	query_where = query_where &  " AND isMember = '" & isMember &"'"
End If

If on_status <> "" Then
	query_where = query_where &  " AND on_status = '" & on_status &"'"
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

'response.Write getQuery

sql = getQuery
Call dbopen
Set rs = dbconn.execute(sql)
%>
		<form name = "frm" id="frm" action = "" class="smart-form m10"  method="get" >
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table class="table table-striped table-bordered line-h" width="100%">
			<tr>
				<th>기간</th>
				<td colspan="3">
					<input type="text" name="sdate" id="sdate" value="<%=sdate%>" autocomplete="off"> ~ <input type="text" name="edate" id="edate" value="<%=edate%>" autocomplete="off">
				</td>
			</tr>
			<tr>
				<th>구분</th>
				<td colspan="3">
					<label class="select col-2">
					<select name="on_type" id="on_type">
						<option value="">전체</option>
						<option value="estimate1" <% If on_type = "estimate1" Then %> selected<% End If %>>견적요청(일반)</option>
						<option value="estimate2" <% If on_type = "estimate2" Then %> selected<% End If %>>견적요청(솔루션)</option>
						<option value="tech" <% If on_type = "tech" Then %> selected<% End If %>>기술문의</option>
						<option value="demo" <% If on_type = "demo" Then %> selected<% End If %>>데모장비 신청 및 테스트의뢰</option>
						<option value="etc" <% If on_type = "etc" Then %> selected<% End If %>>기타문의</option>
					</select><i></i>
					</label>

					<!--label class="select col-1">
					<select name="on_cate" id="on_cate">
						<option value="">전체</option>
						<option value="신성리커버리" <% If on_cate = "신성리커버리" Then %> selected<% End If %>>신성리커버리</option>
						<option value="백업 및 복구" <% If on_cate = "백업 및 복구" Then %> selected<% End If %>>백업 및 복구</option>
						<option value="서버이중화" <% If on_cate = "서버이중화" Then %> selected<% End If %>>서버이중화</option>
						<option value="가상화" <% If on_cate = "가상화" Then %> selected<% End If %>>가상화</option>
						<option value="문서중앙화" <% If on_cate = "문서중앙화" Then %> selected<% End If %>>문서중앙화</option>
						<option value="화상회의" <% If on_cate = "화상회의" Then %> selected<% End If %>>화상회의</option>
					</select><i></i>
					</label-->
				</td>
			</tr>
			<tr>
				<th>회원여부</th>
				<td>
					<label class="select col-3">
					<select name="isMember" id="mailYN">
						<option value="">전체</option>
						<option value="Y" <% If isMember = "Y" Then %> selected<% End If %>>회원</option>
						<option value="N" <% If isMember = "N" Then %> selected<% End If %>>비회원</option>
					</select><i></i>
					</label>
				</td>
				<th>답변여부</th>
				<td>
					<label class="select col-3">
					<select name="on_status" id="on_status">
						<option value="">전체</option>
						<option value="N" <% If on_status = "N" Then %> selected<% End If %>>접수완료</option>
						<option value="Y" <% If on_status = "Y" Then %> selected<% End If %>>답변완료</option>
					</select><i></i>
					</label>
				</td>
			</tr>
			<tr>
				<th>검색</th>
				<td colspan="3">
					<label class="select w10 pro_detail">
					<select class="input-sm h34" name="fieldname" id="fieldname">
						<option value="on_title" <% If fieldname = "on_title" Then %> selected<% End If %>>제목</option>
						<option value="on_content" <% If fieldname = "on_content" Then %> selected<% End If %>>내용</option>
						<option value="on_company" <% If fieldname = "on_company" Then %> selected<% End If %>>회사명</option>
						<option value="on_name" <% If fieldname = "on_name" Then %> selected<% End If %>>담당자</option>
						<option value="on_mobile" <% If fieldname = "on_mobile" Then %> selected<% End If %>>연락처</option>
						<option value="on_email" <% If fieldname = "on_email" Then %> selected<% End If %>>이메일</option>
					</select>
					</label>
					<div class="icon-addon addon-md col-md-10 col-4">
						<input type="text" name="fieldvalue" id="fieldvalue" class="form-control col-10" value="<%=fieldvalue%>">
						<label for="fieldvalue" class="glyphicon glyphicon-search " rel="tooltip" title="fieldvalue"></label>
					</div>
					<a href="javascript:frm.submit();" class="btn btn-primary"><i class="fa fa-search"></i> 검색</a>
					<a href="./online_list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-refresh"></i> 검색 초기화</a>
				</td>
			</tr>
		</table>
		</form>

		<%=intTotalCount%>건

		<form name="list" method="get" action="./online_exec.asp" style="margin:0;">
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="message">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:5%">
			<col style="width:5%">
			<col style="width:12%">
			<col style="width:7%">
			<col style="width:*">
			<col style="width:10%">
			<col style="width:5%">
			<col style="width:7%">
			<col style="width:10%">
			<col style="width:7%">
			<col style="width:5%">
		</colgroup>
		<thead>
			<tr>
				<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
				<th data-class="expand">번호</th>
				<th data-hide="phone,tablet">구분</th>
				<th data-class="expand">분류</th>
				<th data-class="expand">제목</th>
				<th data-class="expand">회사명</th>
				<th data-hide="phone,tablet">담당자</th>
				<th data-hide="phone,tablet">연락처</th>
				<th data-hide="phone,tablet">이메일</th>
				<th data-hide="phone,tablet">작성일</th>
				<th data-hide="phone,tablet">답변여부</th>
			</tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="11" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	rs.move MoveCount
	Do while Not rs.eof

		on_idx			= rs("on_idx")
		on_status 		= rs("on_status")
		on_type 		= rs("on_type")
		on_cate 		= rs("on_cate")
		on_company 		= rs("on_company")
		on_name			= rs("on_name")
		on_mobile 		= rs("on_mobile")
		on_email 		= rs("on_email")
		on_title 		= rs("on_title")
		regdate 		= rs("regdate")

		If on_type = "estimate1" Then
			on_type_txt = "견적요청(일반)"
		ElseIf on_type = "estimate2" Then
			on_type_txt = "견적요청(솔루션)"
		ElseIf on_type = "tech" Then
			on_type_txt = "기술문의"
		ElseIf on_type = "etc" Then
			on_type_txt = "기타문의"
		ElseIf on_type = "demo" Then
			on_type_txt = "데모장비 신청 및 테스트의뢰"
		End If

		If on_status = "Y" Then
			on_status_txt = "답변완료"
		Else
			on_status_txt = "접수완료"
		End If
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><input type="checkbox" value="<%=on_idx%>"></td>
				<td><%=intNowNum%></td>
				<td><%=on_type_txt%></td>
				<td><%=on_cate%></td>
				<td><a href="online_view.asp?menucode=<%=menucode%>&on_idx=<%=on_idx%>"><%=on_title%></a></td>
				<td><%=on_company%></td>
				<td><%=on_name%></td>
				<td><%=on_mobile%></td>
				<td><%=on_email%></td>
				<td><%=Left(regdate, 10)%></td>
				<td><%=on_status_txt%></td>
			</tr>
<%
		intNowNum = intNowNum - 1
		rs.movenext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
		</tbody>
		</table>

		<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:10 0 0 0;">
			<tr>
				<td>
					<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
				</td>
				<!--td align="right">
					 <a href="./mem_excel_write.asp" class="btn btn-primary"><i class="fa fa-upload"></i> 엑셀 업로드</a>
					 <a href="./exceldownload.asp?cd=s12&sdate=<%=sdate%>&edate=<%=edate%>&intGubun=<%=intGubun%>&mailYN=<%=mailYN%>&smsYN=<%=smsYN%>&yeosinYN=<%=yeosinYN%>&fieldname=<%=fieldname%>&fieldvalue=<%=fieldvalue%>&page=<%=intNowPage%>" class="btn btn-default"><i class="fa fa-download"></i> 엑셀 다운로드</a>
					 <a href="./exceldownload_srm.asp" class="btn btn-default"><i class="fa fa-download"></i> SRM 담당자 다운로드</a>
				</td-->
			</tr>
		</table>
		</form>

		<%call Paging_list("")%>
	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->