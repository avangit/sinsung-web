
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->

			<%
			Dim intSeq
			intSeq = request.querystring("intSeq")
			%>

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
						<h2>SMS</h2>
					</header>
	
					<!-- widget div-->
					<div class="mt50">
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">
								
							<form name="frmRequestForm" method="post" onsubmit="return frmRequestForm_Submit(this);" action="proc.asp"  class="smart-form" >
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>보내는사람</th>
										<td>
											<label for="strForm" class="input">
												<input type="text" name="strForm" id="strForm" value="avan@avansoft.co.kr">
											</label>
										</td>
									</tr>
									<%
									Dim sql, rs, hp
									sql = "select * from mtb_member2 where intSeq = '"&intSeq&"'"
									Set rs = dbconn.execute(sql)
									If Not rs.eof Then 
									%>
									<tr>
										<th>아이디</th>
										<td><%=rs("strId")%></td>
									</tr>
									
									<tr>
										<th>이메일</th>
										<td><%=rs("strEmail")%><input type="hidden" name="strTo" value="<%=rs("strEmail")%>"></td>
									</tr>
									<%else%>
									<tr>
										<th>구분</th>
										<td>
											<label class="radio ib">
												<input type="radio" name="mGubun" value="1">
											<i></i>전체</label>

											<label class="radio ib">
												<input type="radio" name="mGubun" value="2">
											<i></i>지정</label>
											<label for="strTo" class="input col-2 ib">
												<input type="text" name="strTo" id="strTo" value="" >
											</label>
										</td>
									</tr>
									<%End if%>
									<tr>
										<th>제목</th>
										<td>
											<label for="strSubject" class="input">
												<input type="text" name="strSubject" id="strSubject">
											</label></td>
									</tr>
									<tr>
										<th>에디터</th>
										<td><%call Editor("strMailMemo","")%></td>
									</tr>
									
								</tbody>
						
							</table>

							<footer style="float:right;">
								<input type="button" class="btn btn-primary" onclick="this.form.onsubmit();" value="작성하기"></button>
							</footer>
							
							</form>	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->

				<script type="text/javascript">

					function frmRequestForm_Submit(frm){
						<%if len(request.querystring("intSeq")) < 1 then%>
						if ( frm.mGubun[1].checked == true) 
							{
								if( frm.strTo.value.replace(/ /gi, "") == "" )
								{
									alert("E-mail을 입력해주세요"); frm.strTo.focus(); return false; 
								}
							}
						<%end if%>
						if ( frm.strSubject.value.replace(/ /gi, "") == "" ) { alert("제목을 입력해주세요"); frm.strSubject.focus(); return false; }
						//if ( frm.strMailMemo.value.replace(/ /gi, "") == "" ) { alert("내용을 입력해주세요"); frm.strMailMemo.focus(); return false; }		
						frm.submit();
					}

				</script>


			

			</article>
			<!-- WIDGET END -->

			


		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		<!-- #include file="../inc/footer.asp" -->
