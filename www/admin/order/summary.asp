
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config.asp" -->

				<%
				Sub TopList(byval strWhere)
					Dim rs,totalmoney,pay,send_name,use_point

					strSQL = "SELECT TOP 5 order_num,name,pay,STATUS,realmoney,date  FROM order_regist WHERE  order_num <> '' " & strWhere & "  order by date DESC"

					set rs = dbconn.execute(strSQL)

				%>
				<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
					 <colgroup>
						<col style="width:20%">
						<col style="width:20%">
						<col style="width:15%">
						<col style="width:15%">
						<col style="width:15%">
						<col style="width:15%">
					</colgroup>
					<thead>
						<tr>
							<th data-class="expand">일자</th>
							<th>주문번호</th>
							<th data-hide="phone,tablet">주문자정보</th>
							<th data-hide="phone,tablet">결제금액</th>
							<th data-hide="phone,tablet">결제방식</th>
							<th data-hide="phone,tablet">처리방식</th>
						</tr>
					</thead>

					<tbody>
						<%
						'// 글이 없을 경우
						if  rs.eof then
						%>
						<tr>
							<td colspan="6" class="aling_c">현재 등록된 글이 없습니다.</td>
						</tr>
						<%
					
						Else	
						Dim strName
						Dim dtmDate,strStatus	
						Do while not rs.eof	
																
							dtmDate		= rs("date")
							order_num 	= rs("order_num")
							strName		= rs("name")		
							pay			= rs("pay")
							strStatus	= rs("STATUS")			
							realmoney	= rs("realmoney")
						%>		
						<tr>
							<td>qqq<%=dtmDate%></td>
							<td><a href = "order_view.asp?order_num=<%=order_num%>"><%=order_num%></a></td>
							<td><a href = "order_view.asp?order_num=<%=order_num%>"><%=strName%></a></td>
							<td><%=FormatNumber(realmoney,0)%>원</td>
							<td><%=getPayMethod(pay)%></td>
							<td><%=result_num(strStatus)%></td>
						</tr>
						<%
						rs.movenext
						loop
						End If
						rs.close()
						set rs= nothing
						%>			
					</tbody>
			
				</table>
				
				<%End Sub%>
	

				
				<%Call Dbopen()%>

				
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>신규 [미입금] 리스트  </h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget content -->
						<div class="widget-body no-padding">
									
							<%call TopList("and status = '1'")%>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->

				
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>배송준비 리스트  </h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget content -->
						<div class="widget-body no-padding">
									
							<%call TopList("and status in ('2','e2')")%>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->


				
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>배송중 리스트 </h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">
	
							<%call TopList("and status in ('3','e3')")%>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->


				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>반품/취소/환불 리스트</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">
	
							<%call TopList("and status in ('4','e4','m4','5','e5','6','e7','e8')")%>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->



				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>입금계좌 해지 리스트</h2>
	
					</header>
	
					<!-- widget div-->
					<div style="margin-bottom:50px;">
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">
	
							<%call TopList("and status in ('e9')")%>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->


			</div>			


		</div>
		<!-- END MAIN PANEL -->

		<!-- #include file="../inc/footer.asp" -->


		
		

		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		

	</body>

</html>