<% @CODEPAGE="65001" language="vbscript" %>
<% option explicit%>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file = "config.asp"-->
<%
    '/* ============================================================================== */
    '/* =   PAGE : 결과 처리 PAGE                                                    = */
    '/* = -------------------------------------------------------------------------- = */
    '/* =   Copyright (c)  2006   KCP Inc.   All Rights Reserverd.                   = */
    '/* ============================================================================== */
%>
<%
    '/* ============================================================================== */
    '/* =   지불 결과                                                                = */
    '/* = -------------------------------------------------------------------------- = */
	Dim req_tx,use_pay_method,bSucc,res_cd,res_msg
	Dim ordr_idxx,tno,good_name,buyr_name,buyr_tel1,buyr_tel2,buyr_mail
	Dim card_cd,card_name,app_time,app_no,quota
	Dim bank_name,bankname,account
	Dim escw_used,deli_term,bask_cntx,good_info,rcvr_name,rcvr_tel1,rcvr_tel2,rcvr_mail,rcvr_zipx,rcvr_add1,rcvr_add2,total
	Dim ProductSum,total_point,totalbesong,pay_method,req_tx_name,totalMoney
	Dim updateSQL
	Dim p_money,p_remainder
	Dim sum_money 
	Dim remainder	
	Dim arrListData 	
	Dim pointSQL 
	Dim strstatus 
	Dim status_scentG 
	Dim strScentG
	Dim status,AdminMemo,CustomerMessage,mod_type,deli_corp,deli_numb
	Dim refund_account,refund_nm,bank_code,strGubun,viewgubun
	Dim SQL_DEL
	Dim strSQL_LIST
	Dim option1,option2,wr_date,g_money,g_name
	Dim sql_DE
	Dim status_scent
	Dim strScent 
	Dim one_change ,rs3
	Dim use_point
	Dim max_num

	
    req_tx         = request( "req_tx" )                      ' 요청 구분(승인/매입)
    use_pay_method = request( "use_pay_method" )              ' 사용 결제 수단
    bSucc          = request( "bSucc" )                       ' 업체 DB 정상처리 완료 여부
    '/* = -------------------------------------------------------------------------- = */
    res_cd         = request( "res_cd" )                      ' 결과 코드
    res_msg        = request( "res_msg" )                     ' 결과 메시지
    '/* = -------------------------------------------------------------------------- = */
    ordr_idxx      = request( "ordr_idxx" )                   ' 주문번호
    tno            = request( "tno" )                         ' KCP 거래번호
    good_mny       = request( "good_mny" )                    ' 결제 금액
    good_name      = request( "good_name" )                   ' 상품명
    buyr_name      = request( "buyr_name" )                   ' 구매자명
    buyr_tel1      = request( "buyr_tel1" )                   ' 구매자 전화번호
    buyr_tel2      = request( "buyr_tel2" )                   ' 구매자 휴대폰번호
    buyr_mail      = request( "buyr_mail" )                   ' 구매자 E-Mail
    '/* = -------------------------------------------------------------------------- = */
    ' 신용카드
    card_cd        = request( "card_cd" )                     ' 카드 코드
    card_name      = request( "card_name" )                   ' 카드명
    app_time       = request( "app_time" )                    ' 승인시간 (공통)
    app_no         = request( "app_no" )                      ' 승인번호
    quota          = request( "quota" )                       ' 할부개월
    '/* = -------------------------------------------------------------------------- = */
    ' 계좌이체
    bank_name      = request( "bank_name" )                   ' 은행명
    '/* = -------------------------------------------------------------------------- = */
    ' 가상계좌
    bankname       = request( "bankname" )                    ' 입금 은행
    depositor      = request( "depositor" )                   ' 입금계좌 예금주
    account        = request( "account" )                     ' 입금계좌 번호
    '/* = -------------------------------------------------------------------------- = */
    ' 에스크로 추가 필드
    escw_used      = request( "escw_used" )                   ' 에스크로 사용 여부
    deli_term      = request( "deli_term" )                   ' 배송 소요일
    bask_cntx      = request( "bask_cntx" )                   ' 장바구니 상품 개수
    good_info      = request( "good_info" )                   ' 장바구니 상품 상세 정보
    rcvr_name      = request( "rcvr_name" )                   ' 수취인 이름
    rcvr_tel1      = request( "rcvr_tel1" )                   ' 수취인 전화번호
    rcvr_tel2      = request( "rcvr_tel2" )                   ' 수취인 휴대폰번호
    rcvr_mail      = request( "rcvr_mail" )                   ' 수취인 E-Mail
    rcvr_zipx      = request( "rcvr_zipx" )                   ' 수취인 우편번호
    rcvr_add1      = request( "rcvr_add1" )                   ' 수취인 주소
    rcvr_add2      = request( "rcvr_add2" )                   ' 수취인 상세주소
	mod_type	   = request( "mod_type" ) 
	
    '/* = -------------------------------------------------------------------------- = */


	strMemo        = request( "strMemo" )                   ' 수취인 상세주소
	total          = request( "total" )                     ' 수취인 상세주소
	total_point    = request( "total_point" )                   ' 상품적립금
	use_point      = request( "use_point" )                   ' 내 적립금에서 사용할 포인트
	good_mny       = request( "good_mny" )                   ' 적립금 뺀 금액 
	totalMoney	   = request( "totalMoney" )                   '배송료 포함 금액
	pay_method     = request( "pay_method" )                   '결재방법
	strZip 		   = request("strZip")
	strAddr1 	   = request("strAddr1")
	strAddr2 	   = request("strAddr2")
	AdminMemo	   = request("AdminMemo") 
	
	 '/* = -------------------------------------------------------------------------- = */
	'//관리자페이지
	strstatus	   = request("status") 			'//상태
	deli_corp 	   = request("deli_corp")  		'//택배사
	deli_numb	   = request("deli_numb")   	'//송장번호
	refund_account = request("refund_account")  '//계좌번호
	refund_nm	   = request("refund_nm") 		'//예금주
	bank_code	   = request("bank_code") 		'//은행코드
	income		   = request("income") 			'//입금여부
	CustomerMessage= request("CustomerMessage") '//고객에게 전하는 글
	strGubun	   = request("gubun") 
'	viewgubun	   = request("viewgubun")	
'	income		   = request("income")
	
	'strstatus	   = request("strstatus") 
	
	
	
	If isValue(ordr_idxx) = False Then ordr_idxx = requestQ("order_num")
	
	
	call Dbopen

		'//정상거래 및 거래 취소
	If Ucase(Trim(strstatus)) = "N"  Then
	
		call AdoConnExecute(AdminAccount(strstatus,ordr_idxx))
		
		Dim rs
			
		strSQL = " select b.goods_num,a.status,b.ea,a.order_num,a.id,urlGubun from Order_Regist a,OrderDetail b "
		strSQL = strSQL & " where a.order_num = b.regist_num and a.order_num = '"&ordr_idxx&"'"

		set rs = dbconn.execute(strSQL)		
		Dim strOrderId
		If not rs.bof Then					
		
			Do While not rs.eof							
				'//수량 추가	
					strOrderId = rs("id")	
					urlGubun   = rs("urlGubun")	
				
					updateSQL = "update GOODS set  total_op = total_op + "&rs("ea")&"  WHERE g_idx = '"&RS("goods_num")&"' and g_use_totalChk = 0 "			
	
					Call AdoConnExecute(updateSQL)						
										
				rs.movenext
			Loop
			
			strSQL = "UPDATE Order_Regist SET AdminMemo='"&AdminMemo&"' , CustomerMessage= '"&CustomerMessage&"' "
			strSQL =  strSQL & ",besong_company='"&deli_corp&"' ,besong_num='"&deli_numb&"' ,refund_account='"&refund_account&"' ,refund_nm='"&refund_nm&"',bank_code='"&bank_code&"',income='"&income&"'"
			strSQL =  strSQL & "  WHERE order_num = '"&ordr_idxx&"'"
			
		'	alert(strSQL)
			
			call AdoConnExecute(strSQL)
			
			'//거래취소시 적립금 취소
			If  isValue(strOrderId) Then							
				
				
				'//거래취소에 대한 포인트 insert (더해진 값 빼는 식)		
				Call PointInsertId("상품 거래취소",getPointCancelMoney(ordr_idxx,"OD"),ordr_idxx,ordr_idxx,getPointCancelId(ordr_idxx,"OD"),"OD")				
				
			
				'//즐겨찾기,네이버
				'If urlGubun = "1" or urlGubun = "2" Then
				'	If urlGubun = "1" Then strUrlTitle = "거래 취소 - 즐겨찾기"
				'	If urlGubun = "2" Then strUrlTitle = "거래 취소 - 네이버 검색"
															
				'	Call PointInsertId(strUrlTitle,getPointCancelMoney(ordr_idxx,"U"),ordr_idxx,ordr_idxx,getPointCancelId(ordr_idxx,"U"),"U")				
				'End If
				
				'If lcase(memberAgreeId(strPointId)) = lcase(session("userid")) Then
				'	Call PointInsertId("거래 취소 - 추천인 적립",getPointCancelMoney(ordr_idxx,"R"),ordr_idxx,ordr_idxx,getPointCancelId(ordr_idxx,"R"),"R")				
				'End If
			End If

		'//거래취소시 사용및 적립포인트 삭제					
			
			
		
		End If
		rs.close()
		set rs = nothing
	
	'//거래취소일 경우 포인트 및 재고 처리 end

	ElseIf Ucase(Trim(strstatus)) = "Y"  Then
	
		strSQL = "UPDATE Order_Regist SET AdminMemo='"&AdminMemo&"' , CustomerMessage= '"&CustomerMessage&"' "
		strSQL =  strSQL & ",besong_company='"&deli_corp&"' ,besong_num='"&deli_numb&"' ,refund_account='"&refund_account&"' ,refund_nm='"&refund_nm&"',bank_code='"&bank_code&"',income='"&income&"'"
		strSQL =  strSQL & "  WHERE order_num = '"&ordr_idxx&"'"
		
	'	alert(strSQL)

		'Dim strPoint,arrPointData,strPointId,strPointpay,intRealmoney,intBesongmoney,urlGubun,strUrlTitle
		
		arrPointData = getAdoRsArray(" SELECT point,id,pay from Order_Regist where order_num = '"&ordr_idxx&"'")
		
		If isArray(arrPointData) Then
			strPoint		= arrPointData(0, 0)
			strPointId		= arrPointData(1, 0)	
			strPointpay		= arrPointData(2, 0)	
			intRealmoney	= arrPointData(3, 0)		
			intBesongmoney	= arrPointData(4, 0)		
			urlGubun		= arrPointData(5, 0)
					
			If strPointId <> "" and strPointpay <> "111111111113" Then
				CALL PointInsertId("상품구매포인트",strPoint,ordr_idxx,"",strPointId,"O" )
				'//레벨 2면 50% 추가적립
				if session("userlevel") = 2 then CALL PointInsertId("렉스친구 추가적립금",strPoint*1,ordr_idxx,"",strPointId,"O" )
				'//레벨 3이면 100% 추가적립	
				if session("userlevel") = 3 then CALL PointInsertId("베스트렉스친구 추가적립금",strPoint*2,ordr_idxx,"",strPointId,"O" )
			End If
			
			
			'//즐겨찾기,네이버
			If urlGubun = "1" or urlGubun = "2" Then
				If urlGubun = "1" Then strUrlTitle = "즐겨찾기"
				If urlGubun = "2" Then strUrlTitle = "네이버 검색"
				Dim CourseMoney 
				CourseMoney = (realmoney + besongmoney) * pointCourse
				Call PointInsertId(strUrlTitle, CourseMoney, ordr_idxx, "",strPointId,"U")
			End If
			
			If lcase(memberAgreeId(strPointId)) = lcase(session("userid")) Then
				'Dim AgreeMoney 
				'AgreeMoney = (realmoney + besongmoney) * pointRecommender
				'Call PointInsertId("추천인 적립", AgreeMoney, ordr_idxx, "",strPointId,"R" )
			End If
			
		End If
		
		call AdoConnExecute(strSQL)

		call AdoConnExecute(AdminAccount(strstatus,ordr_idxx))
	
	ElseIf ucase(strGubun) = "G"  Then		
		
		call AdoConnExecute(AdminAccount(strStatus,ordr_idxx))
		
		Dim basongData
		If  Trim(strstatus) = "3"  Then '//배송중일때 7일이 지나면 자동 Y		
			basongData = " ,basongData = '"&now()&"'"
		Else		
			basongData = ""					
		End If	
		
		strSQL = "UPDATE Order_Regist SET AdminMemo='"&AdminMemo&"' , CustomerMessage= '"&CustomerMessage&"' "
		strSQL =  strSQL & ",besong_company='"&deli_corp&"' ,besong_num='"&deli_numb&"' ,refund_account='"&refund_account&"' ,refund_nm='"&refund_nm&"',bank_code='"&bank_code&"',income='"&income&"'"&basongData
		strSQL =  strSQL & "  WHERE order_num = '"&ordr_idxx&"'"
		
	'	alert(strSQL)
		
		call AdoConnExecute(strSQL)
			
		
		Call jsAlertMsgUrl("처리되었습니다", "order_view.asp?order_num="&ordr_idxx)
		
	End If

	'----------------------------------------------------------------------------------------------------------------


    req_tx_name = ""
	

    if req_tx = "pay" then
        req_tx_name = "지불"

    elseif req_tx = "mod" then
        req_tx_name = "매입"

    elseif req_tx = "mod_escrow" then
        req_tx_name = "에스크로 상태변경"

    end if

%>
   
    <script language="javascript">
        <!-- 신용카드 영수증 연동 스크립트 -->
        function receiptView(tno)
        {
            receiptWin = "http://admin.kcp.co.kr/Modules/Sale/Card/ADSA_CARD_BILL_Receipt.jsp?c_trade_no=" + tno
            window.open(receiptWin , "" , "width=420, height=670")
        }
    </script>
   
                
<%
	
    if req_tx = "pay" then                ' 거래 구분 : 승인

        if not bSucc = "false" then       ' 업체 DB 처리 정상

            if res_cd = "0000" then       ' 정상 승인
			
                if use_pay_method = "100000000000" or use_pay_method = "001000000000" then            ' 신용카드			
				
					call jsAlertUrl(UserPath_sub&getString(UserPath_order&"&strOrderMode=orderinfo&order_num="&ordr_idxx))
								

                elseif use_pay_method = "010000000000" then        ' 계좌이체
					call jsAlertUrl(UserPath_sub&getString(UserPath_order&"&strOrderMode=orderinfo&order_num="&ordr_idxx))

                elseif use_pay_method = "000010000000" then        ' 휴대폰

%>
                    <table width="90%" align="center">
						<tr>
							<td bgcolor="CFCFCF" height='2' colspan="2"></td>
						</tr>
						<tr>
							<td align="center" colspan="2">결과 페이지(<%=req_tx_name%>)</td>
						</tr>
						<tr>
							<td bgcolor="CFCFCF" height='2'></td>
						</tr>
						<tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
						<tr>
							<td>결제수단 </td>
							<td>휴대폰</td>
						</tr>
						<tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
						<tr>
							<td>승인시간</td>
							<td><%=app_time%></td>
						</tr>
					</table>
					
               
<%

                elseif use_pay_method = "000100000000" then        ' 카드사 포인트

%>
                    <table width="90%" align="center">
					<tr>
						<td bgcolor="CFCFCF" height='2' colspan="2"></td>
					</tr>
					<tr>
						<td align="center" colspan="2">결과 페이지(<%=req_tx_name%>)</td>
					</tr>
					<tr>
						<td bgcolor="CFCFCF" height='2'></td>
					</tr>
					<tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
                    <tr>
                        <td>결제수단 </td>
                        <td>카드사 포인트</td>
                    </tr>
                    <tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
                    <tr>
                        <td>승인시간</td>
                        <td><%=app_time%></td>
                    </tr>
                </table>
<%

                elseif use_pay_method = "000000000010" then        ' ARS

%>
					<table width="90%" align="center">
					<tr>
						<td bgcolor="CFCFCF" height='2' colspan="2"></td>
					</tr>
					<tr>
						<td align="center" colspan="2">결과 페이지(<%=req_tx_name%>)</td>
					</tr>
					<tr>
						<td bgcolor="CFCFCF" height='2'></td>
					</tr>
                    <tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
                    <tr>
                        <td>결제수단 </td>
                        <td>ARS</td>
                    </tr>
                    <tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
                    <tr>
                        <td>승인시간</td>
                        <td><%=app_time%></td>
                    </tr>
                </table>
<%

                end if

            else                ' 승인 실패

%>
				<%If res_cd = "3002" Then%>
				<font color='red' size = "12pt"><b> * 제어판의 프로그램 추가/삭제에서<br>
				 한국 사이버 페이먼트(Payplus플러그인) 삭제하시고<br> 재부팅 후 다시 결재를 시작해주세요.</b></font>
				<%Else%>
              
				<table width="85%" align="center" border='0' cellpadding='0' cellspacing='1'>
                    <tr>
						<td bgcolor="CFCFCF" height='2' colspan="2"></td>
					</tr>
					<tr>
						<td align="center" colspan="2">결과 페이지(<%=req_tx_name%>)</td>
					</tr>
					<tr>
						<td bgcolor="CFCFCF" height='2'></td>
					</tr>
					<tr>
                        <td>결과코드</td>
                        <td><%=res_cd %></td>
                    </tr>
                    <tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
                    <tr>
                        <td>결과 메세지</td>
                        <td><%=res_msg%></td>
                    </tr>
                </table>
<%
				End If

            end if

        else                    ' 업체 DB 처리 실패

%>
                <table width="85%" align="center" border='0' cellpadding='0' cellspacing='1'>
                    <tr>
                        <td nowrap>취소 결과코드</td>
                        <td><%=res_cd%></td>
                    </tr>
                    <tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
                    <tr>
                        <td nowrap>취소 결과 메세지</td>
                        <td><%=res_msg%></td>
                    </tr>
                    <tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
                    <tr>
                        <td nowrap>상세메세지</td>
                        <td>
<%
            if res_cd = "0000" then

                     response.write("결제는 정상적으로 이루어졌지만 쇼핑몰에서 결제 결과를 처리하는 중 오류가 발생하여 시스템에서 자동으로 취소 요청을 하였습니다. <br> 쇼핑몰로 전화하여 확인하시기 바랍니다.")

            else

                     response.write("결제는 정상적으로 이루어졌지만 쇼핑몰에서 결제 결과를 처리하는 중 오류가 발생하여 시스템에서 자동으로 취소 요청을 하였으나, <br> <b>취소가 실패 되었습니다.</b><br> 쇼핑몰로 전화하여 확인하시기 바랍니다.")

            end if
%>
                        </td>
                    </tr>
                </table>
<%

        end if

    elseif req_tx = "mod" then       ' 거래 구분 : 매입

%>

                <table width="85%" align="center" border='0' cellpadding='0' cellspacing='1'>
                    <tr>
                        <td>결과코드</td>
                        <td><%=res_cd %></td>
                    </tr>
                    <tr><td colspan="2"><IMG SRC="img/dot_line.gif" width="100%"></td></tr>
                    <tr>
                        <td>결과 메세지</td>
                        <td><%=res_msg%></td>
                    </tr>
                </table>
<%

    elseif req_tx = "mod_escrow" then       ' 거래 구분 : 에스크로 상태변경	

		If not (strstatus = "N"  or  strstatus = "Y") Then
		
			'call AdoConnExecute(AdminAccount(strstatus,ordr_idxx))			
			
			strSQL = "UPDATE Order_Regist SET  AdminMemo='"&AdminMemo&"' , CustomerMessage= '"&CustomerMessage&"' "
			strSQL =  strSQL & ",mod_type='"&mod_type&"',deli_corp='"&deli_corp&"' ,refund_account='"&refund_account&"' ,refund_nm='"&refund_nm&"'"
			strSQL =  strSQL & ",bank_code = '"&bank_code&"',res_cd = '"&res_cd&"' , res_msg = '"&res_msg&"',income='"&income&"' WHERE tno = '"&tno&"'"

			dbconn.execute(strSQL)	
		Else
				
		End If

		Call jsAlertMsgUrl("처리되었습니다", "order_view.asp?order_num="&ordr_idxx)
		
	 else	 	
			
		Call jsAlertMsgUrl("처리되었습니다", "order_view.asp?order_num="&ordr_idxx)

    end if

	alert(req_tx)
	call dbclose()

%>
              