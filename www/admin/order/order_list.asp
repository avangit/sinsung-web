
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config.asp" -->

		<%session("userlevel") = "9"%>

			<!-- NEW WIDGET START -->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark col-lg-12" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"

					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>전체리스트</h2>
					</header>

					<!-- widget div-->
					<div>

						<!-- widget content -->


					<script language="javascript">
					$(document).ready(function() {
						$("#songjang_chg").click(function(){
							var frm = document.list;

							 var len = parseInt($("input[name^=songjang]").length);
							 $("input[name=n]").val("");

							 if(!len || len==0) {
								alert('데이터가 없습니다.');
							 } else {
								var arr_val_list = new Array();
								var j = 0;

								$("input[name^=songjang]").each(function(i){
									arr_val_list[i] = $(this).attr("rel");
									j++;
								});

								$("input[name=n]").val(arr_val_list.join(','));
								$("input[name=m]").val('songjang');

								if(!j || j==0) {
									$("input[name=n]").val("");
								} else {
									document.list.submit();
								}
							 }
						});
					});

					function AllCk()
					{
						var fn = document.frmID;
						var len = fn.CkDel.length;

						if (fn.Cktype.value=="all")
						{
							for (i=1;i<len;i++)
							{
								fn.CkDel[i].checked = true;
							}
							fn.Cktype.value = "alldel"
						}
						else
						{
							for (i=1;i<len;i++)
							{
								fn.CkDel[i].checked = false
							}
							fn.Cktype.value = "all"
						}
					}
					//송장엑셀저장
					function songjang(f){

						var fn = document.frmID;
						var len = fn.CkDel.length;
						var k = 0;
						var order_num = "";

						for (i=1;i<len;i++)
						{
							if (fn.CkDel[i].checked==true)
							{
								k++
								order_num = order_num + fn.CkDel[i].value + ","
							}
						}

						if (k==0)
						{
							alert("출력하실 항목을 체크해 주세요.");
							return ;
						}
						fn.action = "/AVANshop_v3/order/"+f+"?order_num="+order_num
						fn.submit();
					}


					</script>
					<%


						'##==========================================================================
						'##
						'##	페이징 관련 함수 - 게시판등...
						'##
						'##==========================================================================
						'##
						'## [설정법]
						'## 다음 코드가 상단에 위치 하여야 함
						'##
						Dim rs
						Dim search : 	search 		= requestS("search")
						Dim keyword : 	keyword 	= requestS("keyword")
										result  	= requestS("result")
										order_num	= requestS("order_num")


						dim intTotalCount, intTotalPage

						dim intNowPage			: intNowPage 		= Request.QueryString("page")
						dim intPageSize			: intPageSize 		= 20
						dim intBlockPage		: intBlockPage 		= 10

						dim query_filde			: query_filde		= "*"
						dim query_Tablename		: query_Tablename	= " orders "
						dim query_where			: query_where		= " o_idx > 0 "
						dim query_orderby		: query_orderby		= " order by o_idx DESC "

						If keyword <>  "" AND search <> "" Then
							query_where = query_where &" and "& search &" like '%"&keyword&"%'"
						End If

						'//마이페이지 및 주문 리스트
						If result <> "" Then    '//주문내역조회[0:전자결제요청/1:결제대기/2:결제확인/3:상품배송/4:판매완료/15:전자결제반려/20:전자결제승인/91:취소/92:교환/93:반품]
							If result = 0 Then
								query_where = query_where & " and o_status IN (0,15,20)"
							Else
								query_where = query_where & " and o_status = " & result
							End If
						End If

						call intTotal

						'##
						'## 1. intTotal : call intTotal
						'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
						'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
						'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
						'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
						'##
						'##
						'#############################################################################


						sql = GetQuery()

						call dbopen
						set rs = dbconn.execute(sql)
					%>
						<div class="widget-body no-padding">
							<form name = "frm" action = "?<%=getstring("")%>" onsubmit="return regist(this)"  method="post" >
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h search_t smart-form" width="100%">
									<colgroup>
										<col style="width:15%">
										<col style="">
									</colgroup>
									<tbody>
										<th>검색어</th>
										<td>
										<label class="select w10 pro_detail">
										<select class="input-sm h34" name="search" id="search">
											<option value="o_name" <% If search = "o_name" Then %> selected<% End If %>>주문자명</option>
											<option value="strId" <% If search = "o_name" Then %> selected<% End If %>>아이디</option>
											<option value="o_company" <% If search = "o_company" Then %> selected<% End If %>>회사명</option>
											<option value="o_department" <% If search = "o_department" Then %> selected<% End If %>>부서명</option>
											<option value="o_num" <% If search = "o_num" Then %> selected<% End If %>>주문번호</option>
										</select> <i></i> </label>
										<div class="icon-addon addon-md col-md-10 col-2">
											<input type="text" name="keyword" id="keyword" class="form-control col-10" value="<%=keyword%>" >
<!--											<label for="email" class="glyphicon glyphicon-search " rel="tooltip" title="email"></label>-->
										</div>


										<input type="submit" class="btn btn-default" value="검색">
										</td>
									</tbody>
								</table>
							</form>
								<form name="list" method="get" action="./order_exec_get.asp" style="margin:0;">
								<input type="hidden" name="n">
								<input type="hidden" name="m">
								<input type="hidden" name="message">
								<input type="hidden" name="arr_val_list">
								<input type="hidden" name="menucode" value="<%=menucode%>">
								<input type="hidden" name="page" value="<%=intNowPage%>">
								<input type="hidden" name="result" value="<%=result%>">
								<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
									 <colgroup>
										<col style="width:5%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:10%">
										<col style="width:10%">
										<col style="width:10%">
										<col style="width:10%">
										<col style="width:10%">
										<col style="width:10%">
										<col style="width:10%">
									<% If result > 1 Then %>
										<col style="width:10%">
									<% End If %>
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand" class="aling_c"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
											<th data-class="expand">주문일시</th>
											<th>주문번호</th>
											<th data-hide="phone,tablet">주문자</th>
											<th data-hide="phone,tablet">아이디</th>
											<th data-hide="phone,tablet">회사명</th>
											<th data-hide="phone,tablet">부서명</th>
											<th data-hide="phone,tablet">주문방법</th>
											<th data-hide="phone,tablet">주문금액</th>
											<th data-hide="phone,tablet">주문상태</th>
									<% If result > 1 Then %>
											<th data-hide="phone,tablet">송장번호</th>
									<% End If %>
										</tr>
									</thead>

									<tbody>
<%
										dim pagei : pagei = (intTotalCount-MoveCount)
										'// 글이 없을 경우
										if  rs.eof then
%>
										<tr>
											<td colspan="10" align="center">현재 등록된 주문이 없습니다.</td>
										</tr>
<%
										Else

											rs.move MoveCount
											Do while not rs.eof
												regdate		= rs("regdate")
												order_num 	= rs("o_num")
												strStatus	= rs("o_status")
												o_total_price	= rs("o_total_price")
	'											OrderStat	= rs("OrderStat")

												If rs("o_status") = "0" Then
													status_txt = "구매요청"
												ElseIf rs("o_status") = "1" Then
													status_txt = "결제대기"
												ElseIf rs("o_status") = "2" Then
													status_txt = "결제확인"
												ElseIf rs("o_status") = "3" Then
													status_txt = "상품배송"
												ElseIf rs("o_status") = "4" Then
													status_txt = "판매완료"
												ElseIf rs("o_status") = "15" Then
													status_txt = "구매반려"
												ElseIf rs("o_status") = "20" Then
													status_txt = "구매승인"
												ElseIf rs("o_status") = "91" Then
													status_txt = "취소"
												ElseIf rs("o_status") = "92" Then
													status_txt = "교환"
												ElseIf rs("o_status") = "93" Then
													status_txt = "반품"
												End If

												If rs("o_payment") = "card" Then
													payment_txt = "카드결제"
												ElseIf rs("o_payment") = "account" Then
													payment_txt = "무통장입금"
												ElseIf rs("o_payment") = "yeosin" Then
													payment_txt = "여신결제"
												End If
										%>
										<tr>
											<td class="aling_c">
												<input type="checkbox" value="<%=rs("o_idx")%>">
											</td>
											<td><%=regdate%></td>
											<td><a href="order_view.asp?result=<%=result%>&idx=<%=rs("o_idx")%>&menucode=<%=menucode%>"><%=order_num%></a></td>
											<td><a href="order_view.asp?result=<%=result%>&idx=<%=rs("o_idx")%>&menucode=<%=menucode%>"><%=rs("o_name")%></a></td>
											<td><a href="order_view.asp?result=<%=result%>&idx=<%=rs("o_idx")%>&menucode=<%=menucode%>"><%=rs("strId")%></a></td>
											<td><%=rs("o_company")%></td>
											<td><%=rs("o_department")%></td>
											<td><%=payment_txt%></td>
											<td><%=Formatnumber(o_total_price, 0)%>원</td>
											<td><%=status_txt%></td>
									<% If result = 2 Then %>
											<td><input type="text" name="songjang<%=rs("o_idx")%>" value="<%=rs("o_songjang")%>" rel="<%=rs("o_idx")%>"></td>
									<% ElseIf result > 2 Then %>
											<td><%=rs("o_songjang")%></td>
									<% End If %>
										</tr>

										<%
											pagei = pagei-1
										rs.movenext
										loop
										End If
										%>
									</tbody>

								</table>
								<% If result = 0 Then %>
								<div class="bot_btns">
									<a href="javascript:GetCheckbox(document.list, 'result_15', '구매반려');" class="btn btn-primary"><i class=""></i> 구매반려</a>
									<a href="javascript:GetCheckbox(document.list, 'result_20', '구매승인');" class="btn btn-default"><i class=""></i> 구매승인</a>
								</div>
								<% ElseIf result = 1 Then %>
									<a href="javascript:GetCheckbox(document.list, 'result_2', '결제완료');" class="btn btn-primary"><i class=""></i> 결제완료</a>
								<% ElseIf result = 2 Then %>
									<a href="javascript:GetCheckbox(document.list, 'result_4', '판매완료');" class="btn btn-primary"><i class=""></i> 판매완료</a>
									<a href="javascript:;" class="btn btn-primary" id="songjang_chg"><i class=""></i> 송장번호 저장</a>
								<% End If %>
							</form>

						</div>
						<!-- end widget content -->
						<%call Paging_list("&search="&search&"&keyword="&keyword)%>
					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->


		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>



		<!-- #include file="../inc/footer.asp" -->
