		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config.asp" -->
<%
	o_idx = request.QueryString("idx")
	result = request.QueryString("result")
	menucode = request.QueryString("menucode")

	sql = "SELECT * FROM " & tablename & " WHERE o_idx = " & o_idx & " ORDER BY o_idx DESC "
	Set rs = dbconn.execute(sql)


	'주문상세를 보면 시리얼코드 생성 함수를 실행.
	call SN(rs("o_num"), rs("g_idx"), rs("g_cnt"), rs("o_total_mount"), rs("strID"), rs("masterId"))

	If rs.eof Then
		Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","/admin/")
	Else
		If InStr(rs("g_idx"), ",") > 0 Then
			arr_g_cnt = Split(rs("g_cnt"), ",")
			arr_g_price = Split(rs("g_price"), ",")
		Else
			arr_g_cnt = rs("g_cnt")
			arr_g_price = rs("g_price")
		End If

		If rs("o_status") = "0" Then
			status_txt = "구매요청"
		ElseIf rs("o_status") = "1" Then
			status_txt = "결제대기"
		ElseIf rs("o_status") = "2" Then
			status_txt = "결제확인"
		ElseIf rs("o_status") = "3" Then
			status_txt = "상품배송"
		ElseIf rs("o_status") = "4" Then
			status_txt = "판매완료"
		ElseIf rs("o_status") = "15" Then
			status_txt = "구매반려"
		ElseIf rs("o_status") = "20" Then
			status_txt = "구매승인"
		ElseIf rs("o_status") = "91" Then
			status_txt = "취소"
		ElseIf rs("o_status") = "92" Then
			status_txt = "교환"
		ElseIf rs("o_status") = "93" Then
			status_txt = "반품"
		End If

		If rs("o_payment") = "card" Then
			payment_txt = "카드결제"
		ElseIf rs("o_payment") = "account" Then
			payment_txt = "무통장입금"
		ElseIf rs("o_payment") = "yeosin" Then
			payment_txt = "여신결제"
		End If

		strSQL = "SELECT g_idx, g_name, g_simg FROM GOODS WHERE g_idx IN (" & rs("g_idx") & ") ORDER BY g_idx DESC"
		Set rs2 = dbconn.execute(strSQL)
%>
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>주문상세</h2>
					</header>

					<div class="mt50">
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:*">
									<col style="width:10%">
									<col style="width:10%">
									<col style="width:10%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand">상품명</th>
										<th data-hide="phone,tablet">상품가격</th>
										<th data-hide="phone,tablet">수량</th>
										<th data-hide="phone,tablet">합계</th>
									</tr>
								</thead>
								<tbody>
<%
		i = 0
		Do While Not rs2.eof
%>
									<tr>
										<td>
											<div class="cart_div">
												<div class="img_box">
													<span class="pro_detail mr10"><%=getImgSizeView(100,80,"goods/" & rs2("g_simg"))%></span>
													<!--a href="#"><img src="/upload/goods/<%=rs2("g_simg")%>"></a-->
												</div>
												<div class="txt_box">
													<a href="/admin/goods/product_view.asp?menucode=m05s28&g_idx=<%=rs2("g_idx")%>" class="pro_code" target="_blank">제품코드 : #<%=rs2("g_idx")%></a>
													<h3><%=rs2("g_name")%></h3>
												</div>
											</div>
										</td>
									<% If InStr(rs("g_idx"), ",") > 0 Then %>
										<td class="price"><%=FormatNumber(arr_g_price(i), 0)%>원</td>
										<td><%=arr_g_cnt(i)%>개</td>
										<td><%=FormatNumber(arr_g_price(i)*arr_g_cnt(i), 0)%></td>
									<% Else %>
										<td class="price"><%=FormatNumber(arr_g_price, 0)%>원</td>
										<td><%=arr_g_cnt%>개</td>
										<td><%=FormatNumber(arr_g_price*arr_g_cnt, 0)%></td>
									<% End If %>
									</tr>
<%
			i = i + 1
			rs2.MoveNext
		Loop

		rs2.close
		Set rs2 = Nothing
%>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>주문정보 </h2>
					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="*">
									<col style="width:15%">
									<col style="*">
								</colgroup>
								<tbody>
									<tr>
										<th data-class="expand">주문번호</th>
										<td><%=rs("o_num")%></td>
										<th data-class="expand">주문일자</th>
										<td><%=rs("regdate")%></td>
									</tr>
									<!--tr>
										<th>주문자</th>
										<td><%=rs("strName")%></td>
										<th>아이디</th>
										<td><%=rs("strId")%></td>
									</tr>
									<tr>
										<th>회사명</th>
										<td><%=rs("o_company")%></td>
										<th>부서명</th>
										<td><%=rs("o_department")%></td>
									</tr-->
									<tr>
										<th>결제방법</th>
										<td colspan="3"><%=payment_txt%></td>
									</tr>
									<tr>
										<th>주문상태</th>
										<td colspan="3"><%=status_txt%></td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>


					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i> </span>
							<h2>주문하시는 분</h2>
						</header>

						<!-- widget div-->
						<div>

							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->

							</div>
							<!-- end widget edit box -->

							<!-- widget content -->
							<div class="widget-body no-padding">
								<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
									 <colgroup>
										<col style="width:15%">
										<col style="width:*">
										<col style="width:15%">
										<col style="width:*">
									</colgroup>
									<tbody>
										<tr>
											<th data-class="expand">회사명</th>
											<td><%=rs("o_company")%></td>
											<th data-class="expand">주문부서</th>
											<td><%=rs("o_department")%></td>
										</tr>
										<tr>
											<th data-class="expand">주문자</th>
											<td colspan="3"><%=rs("o_name")%></td>
										</tr>
										<tr>
											<th data-class="expand">이메일</th>
											<td><%=rs("o_email")%></td>
											<th data-class="expand">휴대폰번호</th>
											<td><%=rs("o_mobile")%></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- end widget content -->
						</div>
						<!-- end widget div -->
					</div>

					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i> </span>
							<h2>받는 분</h2>
						</header>

						<!-- widget div-->
						<div>

							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->

							</div>
							<!-- end widget edit box -->

							<!-- widget content -->
							<div class="widget-body no-padding">
								<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
									 <colgroup>
										<col style="width:15%">
										<col style="width:*">
										<col style="width:15%">
										<col style="width:*">
									</colgroup>
									<tbody>
										<tr>
											<th data-class="expand">수령인</th>
											<td><%=rs("s_name")%></td>
											<th data-class="expand">연락처</th>
											<td><%=rs("s_phone")%></td>
										</tr>
										<tr>
											<th data-class="expand">휴대폰 번호</th>
											<td><%=rs("s_mobile")%></td>
											<th data-class="expand">이메일 주소</th>
											<td><%=rs("s_email")%></td>
										</tr>
										<tr>
											<th data-class="expand">주소</th>
											<td colspan="3">[<%=rs("s_zip")%>]&nbsp;<%=rs("s_addr1")%><br><%=rs("s_addr2")%></td>
										</tr>
										<tr>
											<th data-class="expand">주문 시 요청사항</th>
											<td colspan="3"><%=rs("s_request")%></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- end widget content -->

						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				<% If rs("o_exchange") <> "" Then %>
					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i> </span>
							<h2>교환정보</h2>
						</header>

						<!-- widget div-->
						<div>

							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->

							</div>
							<!-- end widget edit box -->

							<!-- widget content -->
							<div class="widget-body no-padding">
								<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
									 <colgroup>
										<col style="width:15%">
										<col style="width:*">
									</colgroup>
									<tbody>
										<tr>
											<th data-class="expand">교환사유</th>
											<td colspan="3"><%=rs("o_exchange")%></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- end widget content -->
						</div>
						<!-- end widget div -->
					</div>
				<% End If %>
				<% If rs("o_return") <> "" Then %>
					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i> </span>
							<h2>반품정보</h2>
						</header>

						<!-- widget div-->
						<div>

							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->

							</div>
							<!-- end widget edit box -->

							<!-- widget content -->
							<div class="widget-body no-padding">
								<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
									 <colgroup>
										<col style="width:15%">
										<col style="width:*">
									</colgroup>
									<tbody>
										<tr>
											<th data-class="expand">반품사유</th>
											<td colspan="3"><%=rs("o_return")%></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- end widget content -->
						</div>
						<!-- end widget div -->
					</div>
				<% End If %>
							<footer>
								<button type="button" class="btn btn-default" onclick="location.href='./order_list.asp?result=<%=result%>&menucode=<%=menucode%>'">
									리스트
								</button>
							</footer>
				</div>
				<!-- end widget -->
<%
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});
		</script>
		<!-- #include file="../inc/footer.asp" -->