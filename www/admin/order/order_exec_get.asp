<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp" -->

<%
o_idx		= SQL_Injection(Trim(Request.QueryString("n")))
mode		= SQL_Injection(Trim(Request.QueryString("m")))
message		= SQL_Injection(Trim(Request.QueryString("message")))
result		= SQL_Injection(Trim(Request.QueryString("result")))
menucode	= SQL_Injection(Trim(Request.QueryString("menucode")))
page		= SQL_Injection(Trim(Request.QueryString("page")))

If o_idx = "" Or ISNULL(o_idx) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","./order_list.asp?menucode="&menucode&"&result="&result&"&page="&page)
Else
	o_idx = Replace(o_idx, " ", ",")
End If

Call dbOpen()

'// 삭제 시
If mode = "del" Then
	message = "삭제"

	dbconn.execute("DELETE FROM " & tablename & " WHERE o_idx IN (" & o_idx & ")")

'// 주문상태 변경 시
ElseIf Left(mode, 6) = "result" Then
	status_val = Split(mode, "_")(1)

	dbconn.execute("UPDATE " & tablename & " SET o_status = '" & status_val & "' WHERE o_idx IN (" & o_idx & ")")

'// 송장번호 부여 시
ElseIf mode = "songjang" Then
	message = "송장번호가 부여"

	For i = 0 To Ubound(Split(o_idx, ","))
		sql = "UPDATE " & tablename & " SET "
		sql = sql & "o_songjang = '" & SQL_Injection(Trim(Request("songjang" & Split(o_idx, ",")(i)))) & "' "
		sql = sql & " WHERE o_idx = " & Split(o_idx, ",")(i) & ""

		dbconn.execute(Sql)
	Next
Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("" & message & "되었습니다.", "./order_list.asp?menucode="&menucode&"&result="&result&"&page="&page)
%>