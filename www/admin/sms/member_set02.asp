	<!-- #include file="../_inc/head.asp" -->
</head>
<%Dim intSeq
	intSeq = request.querystring("intSeq")
%>
<body>
	<div id="A_Wrap">
		<div id="A_Header"> 
			<!-- #include file="../_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container_L">
				<!-- #include file="../_inc/left_menu.asp" -->
			</div>
			<div id="A_Container_C">
				<p class="main_nav">회원관리 > SMS발송</p>
				<h2 class="main_tit">SMS발송</h2>
				<div class="admin">
					<form name="frmRequestForm" method="post" class="smart-form">
						<fieldset>
							<legend>SMS발송</legend>
							<table class="table">
								<caption>SMS발송</caption>
								<colgroup>
									<col style="width:260px" />
									<col style="width:*" />
								</colgroup>
								<tbody>
<%	Dim sql, rs, hp
		sql = "select * from mtb_member2 where intSeq = '"&intSeq&"'"
		Set rs = dbconn.execute(sql)
		If Not rs.eof Then 

		If Len(rs("strMobil")) > 0 Then hp =  Trim(Replace(Decrypt_Seed(rs("strMobil")),"-","")) End If
%>
									<tr>
										<th>아이디</th>
										<td><%=rs("strId")%></td>
									</tr>
									
									<tr>
										<th>번호</th>
										<td><%=hp%><input type="hidden" name="hp" value="<%=hp%>"></td>
									</tr>
<%		else%>
									<tr>
										<th>구분</th>
										<td class="left">
											<input type="radio" id="division_01" name="str_smsUse" value="1" /><label for="division_01">전체</label>
											<input type="radio" id="division_02" name="str_smsUse" value="0" /><label for="division_02">지정</label>
											<input type="text" class="w50" name="hp" id="hp" />
										</td>
									</tr>
<%		End if%>
									<tr>
										<th>내용</th>
										<td class="left">
											<textarea class="w100 h300" name="msg"></textarea>
										</td>
									</tr>
								</tbody>
							</table>
							<button type="button" class="btn_write f_right mt_10" onclick="frmRequestForm_Submit(this);" value="작성하기">작성하기</button>
						</fieldset>
					</form>
				</div>
			</div> 
		</div>  
		<div id="A_Footer"> 
			<!-- #include file="../_inc/footer.asp" -->
		</div>
	</div>
</body>
<script type="text/javascript">

function frmRequestForm_Submit(frm){
	frm.action = "member_send.asp?intSeq=<%=intSeq%>"
	frm.submit();
}
</script>
</html>
