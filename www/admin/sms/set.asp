
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->

			<%
			Dim intSeq
			intSeq = request.querystring("intSeq")
			%>

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
						<h2>SMS</h2>
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">
								
							
							<form name="frmRequestForm" method="post" action="proc.asp" class="smart-form"  enctype="multipart/form-data" style="overflow:hidden">

							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									
									<tr>
										<th>SMS 사용 </th>
										<td>
											<label class="radio ib">
												<input type="radio" name="str_smsUse" value="1">
											<i></i>예(1)</label>

											<label class="radio ib">
												<input type="radio" name="str_smsUse" value="0">
											<i></i>아니오(0)</label>
											SMS는 별도로 신청하여야만 사용이 가능합니다. (문의:1544-7098)&nbsp;&nbsp;
				
											str_smsUse
										</td>
									</tr>
									
									<tr>
										<th>회신번호</th>
										<td>
											<label for="str_smsReTelNo" class="input col-2 ib">
												<input type="text" name="str_smsReTelNo" id="str_smsReTelNo" value="<%=oBprint("str_smsReTelNo")%>" >
											</label>
											str_smsReTelNo
										</td>
									</tr>
									<tr>
										<th>SMS계정번호</th>
										<td>
											<label for="str_smsID" class="input col-2 ib">
												<input type="text" name="str_smsID" id="str_smsID" value="<%=oBprint("str_smsID")%>" >
											</label>
												솔루션호스팅 SMS 서비스계정번호를 입력해주세요.  
											str_smsID
										</td>
									</tr>
									<tr>
										<th>SMS인증키</th>
										<td>
											<label for="str_smsKey" class="input col-4 ib">
												<input type="text" name="str_smsKey" id="str_smsKey" value="<%=oBprint("str_smsKey")%>" >
											</label>
												str_smsKey
										</td>
									</tr>
									
								</tbody>
						
							</table>

							<footer style="float:right;">
								<input type="submit" name="submit" class="btn btn-primary" value="저장"></button>
							</footer>
							
							</form>	










							<form name="frmRequestForm" method="post" action="sms_send.asp" class="smart-form">

							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									
									<tr>
										<th>수신번호</th>
										<td>
											<label for="hp" class="input col-2 ib">
												<input type="text" name="hp" id="hp" value="" >
											</label>
										</td>
									</tr>
									<tr>
										<th>문자내용</th>
										<td>
											<label class="textarea col-4 ">
											<textarea class="h150 input02" name="msg" >문자발송 테스트입니다.</textarea>
											</label>
										</td>
									</tr>
									
									
								</tbody>
						
							</table>

							<footer style="float:right;">
								<input type="submit" class="btn btn-primary" value="발송"></button>
							</footer>
							
							</form>	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->

				<script type="text/javascript">

					function frmRequestForm_Submit(frm){
						if ( frm.strTitle.value == "" ) { alert("제목을 입력해주세요"); frm.strTitle.focus(); return false; }
						frm.action = "basic_insert.asp"
						frm.submit();
					}

				</script>


			

			</article>
			<!-- WIDGET END -->

			


		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		<!-- #include file="../inc/footer.asp" -->
