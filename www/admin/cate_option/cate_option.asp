<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->

<article>
<script src="/admin/js/jquery-2.1.1.min.js"></script>
  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>옵션관리</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
	Dim menucode : menucode = Request.QueryString("menucode")
	Dim i, sql, rs, mode, j

	Call DbOpen()

	sql = "SELECT * FROM cate_option ORDER BY opt_idx"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		mode = "upt"
	Else
		mode = "reg"
	End If
%>

		<form name="list" method="post" action="./option_exec.asp" style="margin:0;">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<input type="hidden" name="m" value="<%=mode%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:5%">
			<col style="width:20%">
			<col style="width:*">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>옵션명</th>
				<th>옵션 내용</th>
			</tr>
		</thead>
		<tbody>
<%
	If rs.eof Or rs.bof Then
		For i = 1 To 20
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><%=i%></td>
				<td>
					<input type="text" name="opt_name<%=i%>" style="width:100%">
				</td>
				<td>
					<input type="text" name="opt_content<%=i%>" style="width:100%">
				</td>
			</tr>
<%
		Next
	Else
		i = 1
		Do while Not rs.eof
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><%=i%></td>
				<td>
					<input type="text" name="opt_name<%=i%>" style="width:100%" value="<%=rs("opt_name")%>">
				</td>
				<td>
					<input type="text" name="opt_content<%=i%>" style="width:100%" value="<%=rs("opt_content")%>">
				</td>
			</tr>
<%
			i = i + 1
			rs.movenext
		Loop

		If i < 20 Then
			For j = i To 20
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><%=j%></td>
				<td>
					<input type="text" name="opt_name<%=j%>" style="width:100%">
				</td>
				<td>
					<input type="text" name="opt_content<%=j%>" style="width:100%">
				</td>
			</tr>
<%
			Next
		End If
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
		</tbody>
		</table>

		<footer style="float:right;">
			<input type="submit" class="btn btn-primary" value="등록">
		</footer>

		</form>

	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->