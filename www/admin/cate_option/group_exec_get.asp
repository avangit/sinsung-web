<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
menucode	= SQL_Injection(Trim(Request.QueryString("menucode")))
group_idx	= SQL_Injection(Trim(Request.QueryString("n")))
mode		= SQL_Injection(Trim(Request.QueryString("m")))

If group_idx = "" Or ISNULL(group_idx) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","history.back();")
Else
	group_idx = Replace(group_idx, " ", ",")
End If

Call dbOpen()

'// 삭제 시
If mode = "del" Then

	dbconn.execute("DELETE FROM cate_option_group WHERE ogroup_idx IN (" & group_idx & ")")
'		dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES ('"&session("aduserid")&"','"&strId&"','삭제','옵션그룹삭제','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("삭제되었습니다.", "./cate_option_grouplist.asp?menucode="&menucode)
%>