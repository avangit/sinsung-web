<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<article>

  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>옵션 그룹 관리</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
	Dim menucode : menucode = Request.QueryString("menucode")
	Dim intTotalCount, i, sql, rs, mode
	Dim group_idx, group_name, group_content, regdate

	Call DbOpen()

	sql = "SELECT COUNT(*) FROM cate_option_group"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		intTotalCount = rs(0)
	End If

	rs.close

	sql = "SELECT * FROM cate_option_group ORDER BY ogroup_idx DESC"
	Set rs = dbconn.execute(sql)
%>

		<%=intTotalCount%>건
		<footer style="float:right;" class="m_button">
			<a href="./group_write.asp?menucode=<%=menucode%>" class="btn btn-primary"><i class="fa fa-pencil"></i> 등록</a>
		</footer>

		<form name="list" method="get" action="./group_exec_get.asp" style="margin:0;">
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="message">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:5%">
			<col style="width:15%">
			<col style="width:*">
		</colgroup>
		<thead>
			<tr>
				<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
				<th data-hide="phone,tablet">옵션 그룹명</th>
				<th data-hide="phone,tablet">옵션</th>
			</tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="3" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	Do while Not rs.eof
		group_idx		= rs("ogroup_idx")
		group_name 		= rs("ogroup_name")
		group_content 	= rs("ogroup_content")
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><input type="checkbox" value="<%=group_idx%>"></td>
				<td align="center"><%=group_name%></td>
				<td><a href="group_write.asp?menucode=<%=menucode%>&n=<%=group_idx%>"><%=group_content%></a></td>
			</tr>
<%
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
		</tbody>
		</table>

		<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:10 0 0 0;">
			<tr>
				<td>
					<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
				</td>
			</tr>
		</table>
		</form>

	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->