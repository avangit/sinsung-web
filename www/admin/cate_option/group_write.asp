<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->

<article>
<script src="/admin/js/jquery-2.1.1.min.js"></script>
  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>옵션관리</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
	Dim menucode : menucode = SQL_Injection(Trim(Request.QueryString("menucode")))
	Dim i, sql, rs, mode, j
	Dim group_idx, group_name, group_content, cnt, arr_group_content, checked

	group_idx = SQL_Injection(Trim(Request.QueryString("n")))

	Call DbOpen()

	If group_idx <> "" Then
		mode = "upt"

		sql = "SELECT * FROM cate_option_group WHERE ogroup_idx = '" & group_idx & "'"
		Set rs = dbconn.execute(sql)

		If Not(rs.bof Or rs.eof) Then
			group_name = rs("ogroup_name")
			group_content = rs("ogroup_content")

			arr_group_content = Split(group_content, ", ")
		End If

		rs.close
	Else
		mode = "reg"
	End If
%>

		<form name="list" method="post" action="./group_exec.asp" style="margin:0;">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<input type="hidden" name="n" value="<%=group_idx%>">
		<input type="hidden" name="m" value="<%=mode%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:10%">
			<col style="width:*">
		</colgroup>
		<tbody>
			<tr>
				<th>옵션 그룹명 *</th>
				<td colspan="2">
					<label for="group_name" class="input">
						<input type="text" name="group_name" id="group_name" value="<%=group_name%>">
					</label>
				</td>
			</tr>
			<tr>
				<th>옵션 선택 *</th>
				<td colspan="2">
<%
	sql = "SELECT opt_name FROM cate_option ORDER BY opt_idx"
	Set rs = dbconn.execute(sql)

	If rs.bof Or rs.eof Then
%>
						옵션관리 메뉴에서 옵션을 등록하세요.
<%
	Else

		i = 1
		Do while Not rs.eof
			If mode = "upt" Then
				For j = 0 To Ubound(arr_group_content)
					If Trim(rs("opt_name")) = Trim(arr_group_content(j)) Then
						checked = " checked"
						Exit For
					Else
						checked = ""
					End If
				Next
			End If
%>
					<input type="checkbox" name="group_content" value="<%=rs("opt_name")%>" <%=checked%>> <%=rs("opt_name")%>&nbsp;&nbsp; <% If i Mod 5 = 0 Then %><br><% End If %>
<%
			i = i + 1
			rs.MoveNext
		Loop
	End If
%>
					<br><font color="red"> * 옵션을 1개 이상 선택해 주세요.</font>
				</td>
			</tr>
<%
	rs.close
	Set rs = Nothing

	Call DbClose()
%>
		</tbody>
		</table>

		<footer align="center">
			<input type="button" class="btn btn-default" value="취소" onclick="location.href='./cate_option_grouplist.asp?menucode=<%=menucode%>'">
			<input type="submit" class="btn btn-primary" value="등록">
		</footer>

		</form>

	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->