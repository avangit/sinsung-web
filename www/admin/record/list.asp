<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<link rel="stylesheet" href="/avanplus/_master/js/jquery-ui.css" media="screen" title="no title">
<script src="/avanplus/_master/js/jquery-ui.js"></script>
<script>
$(function() {
 $(".datepicker").datepicker({
	 dateFormat: 'yy-mm-dd',
	 changeMonth:'true',
	 changeYear:'true'
	 });
})

function explain1()
{
  document.getElementById("explain1").style.display = "block";
}
function explain2()
{
  document.getElementById("explain1").style.display = "none";
}
</script>

			<!-- NEW WIDGET START -->
			<article>
			<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
					-->

					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>개인정보접속기록 <a href="javascript:explain1();" style="color:#ffffff;">&nbsp;?&nbsp;</a></h2>
					</header>

					<div id="explain1" style="display:none;position:absolute;z-index:99;width:600px;">
					  <table border="0" cellpadding="0" cellspacing="0">
					    <tr>
						  <td style="text-align:right;"><a href="javascript:explain2();" style="color:#000000;">Close</a></td>
					    </tr>
					    <tr>
						  <td>
						    <p>개인정보의 안전성 확보에 필요한 조치에 관한 사항<br>
							   개인정보의 기술적 관리적 보호조치 기준에 따라 정보통신서비스 제공자 등은 개인정보취급자가<br>
							   개인정보처리시스템에 접속한 기록을 월 1회 이상 정기적으로 확인·감독하여야 하며,<br>
							   시스템 이상 유무의 확인 등을 위해 최소 6개월 이상 접속기록을 보존·관리하여야 합니다.</p>
						  </td>
					    </tr>
					  </table>
					</div>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">

							<%

							dim intSeq
							'//검색처리부분
							'//일반과 검색을 위한 설정
							dim where, keyword, keyword_option

							keyword 		= requestS("keyword")
							keyword_option 	= requestS("keyword_option")
							'response.write GetString("keyword="&keyword&"&keyword_option="&keyword_option)
							'//폼으로 넘어온경우 백로가면 검색화면 페이지표시할수 없음처리
							if len(request.Form("keyword")) > 0 then response.Redirect("?"&GetString("keyword="&keyword&"&keyword_option="&keyword_option))


							'//검색일경우 첫페이지로 돌리기 위한 설정
							if len(keyword_option) > 0 then
								where = " intSeq > 0 and "& keyword_option &" like '%"& keyword &"%' "
							else
								where  = " intSeq > 0 "
							end If
							
							If Len(Request.QueryString("keyword")) > 0 Then
							  Where = Where & " and menu like '%"& keyword &"%' "
							End If
							
							If Len(Request.QueryString("dtmStartDate1")) > 0 And Len(Request.QueryString("dtmEndDate1")) > 0 Then
                              Where = Where & " and inputdate >= '"&Replace(Request.QueryString("dtmStartDate1"),"-","") & "000000" &"' and inputdate <= '"&Replace(Request.QueryString("dtmEndDate1"),"-","") & "999999" &"' "
							End if
							
							'//카테고리 값이 넘어오는 경우 처리
							if len(requestQ("mcate")) > 0 then	where = where & " and strCategory = '"& requestQ("mcate") &"' "

							'Response.write Replace(Request.QueryString("dtmStartDate1"),"-","") & "000000" & "<br>"
							'Response.write Replace(Request.QueryString("dtmEndDate1"),"-","") & "999999" & "<br>"
							'Response.write Where



							'##==========================================================================
							'##
							'##	페이징 관련 함수 - 게시판등...
							'##
							'##==========================================================================
							'##
							'## [설정법]
							'## 다음 코드가 상단에 위치 하여야 함
							'##
							
							dim intTotalCount, intTotalPage
							dim intNowPage			: intNowPage 		= Request.QueryString("page")    
							dim intPageSize			: intPageSize 		= 15
							dim intBlockPage		: intBlockPage 		= 10

							dim query_filde			: query_filde		= "*"
							dim query_Tablename		: query_Tablename	= " Record_views"
							dim query_where			: query_where		= Where
							dim query_orderby		: query_orderby		= " order by intSeq DESC "
							
							
							Dim strSearch 			: strSearch 		= request("strSearch")
							Dim strKeyword			: strKeyword 		= request("strKeyword")
							
							call intTotal

							'##
							'## 1. intTotal : call intTotal
							'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
							'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
							'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
							'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
							'##
							'##
							'#############################################################################

							dim sql, rs
							sql = GetQuery()
							'response.Write(sql)
							call dbopen
							set rs = dbconn.execute(sql)

							Dim dtmStartDate1, dtmEndDate1
						%>
							<form name = "frm" action = "?<%=getstring("")%>" onsubmit="return regist(this)" class="smart-form m10" method="get">
							<input type="hidden" name="menucode" value="<%=Request.QueryString("menucode")%>" />
								<table>
								  <tr>
								    <td style="border:#dddddd 1px solid;width:120px;background-color:#eeeeee;padding-left:5px;">회원 아이디</td>
									<td style="border:#dddddd 1px solid;width:350px;padding:2px 0 2px 5px;"><input type="text" name="keyword" class="form-control col-10"></td>
							      </tr>
								  <tr>
								    <td style="border:#dddddd 1px solid;background-color:#eeeeee;height:38px;padding-left:5px;">검색기간</td>
									<td style="border:#dddddd 1px solid;padding:2px 0 2px 5px;">
									  <input type="text" name="dtmStartDate1" class="form-control datepicker" style="width:109px;float:left;" value="<%=dtmStartDate1%>" maxlength="10" >
									  <span style="float:left;margin-top:5px;">&nbsp;~&nbsp;</span>
									  <input type="text" name="dtmEndDate1" class="form-control datepicker" style="width:109px;float:left;" value="<%=dtmEndDate1%>" maxlength="10" >
									  <input type="submit" style="float:left;margin-left:6px;" class="btn btn-default" value="검색">
									</td>
								  </tr>
								</table>
							</form>

							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								 <colgroup>
									<col style="width:5%">
									<col style="width:25%">
									<col style="">
									<col style="width:15%">
									<col style="width:15%">
									<col style="width:20%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand">번호</th>
										<th data-class="expand">접속메뉴</th>
										<th>관리자ID</th>
										<th data-hide="phone,tablet">관리자성명</th>
										<th data-hide="phone,tablet">접속IP</th>
										<th data-hide="phone,tablet">접근일시</th>
									</tr>
								</thead>
	
								<tbody>
									<%
										dim pagei : pagei = (intTotalCount-MoveCount)
										'// 글이 없을 경우
										if  rs.eof then
										Else

										rs.move MoveCount
										Do while not rs.eof
									%>
									<tr style="border-bottom:#dddddd 1px solid;">
										<td><%=pagei%></td>
										<td><%=rs("menu")%></td>
										<td><%=rs("manage_id")%></td>
										<td><%=rs("manage_name")%></td>
										<td><%=rs("ip")%></td>
										<td><%=Left(rs("inputdate"),4)%>-<%=mid(rs("inputdate"),5,2)%>-<%=mid(rs("inputdate"),7,2)%>&nbsp;<%=mid(rs("inputdate"),9,2)%>:<%=mid(rs("inputdate"),11,2)%>:<%=mid(rs("inputdate"),13,2)%></td>
									</tr>
								    <%
									pagei = pagei-1
									rs.movenext
									loop
									rs.close()
									set rs = nothing
								End If
								%>
								</tbody>
						
							</table>
							<%call Paging_list("")%>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->



			

			</article>
			<!-- WIDGET END -->

		
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		<!-- #include file="../inc/footer.asp" -->