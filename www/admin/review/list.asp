
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>리뷰</h2>
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">

							<%

							dim intseq
							'//검색처리부분
							'//일반과 검색을 위한 설정
							dim where, keyword, keyword_option

							keyword 		= requestS("keyword")
							keyword_option 	= requestS("keyword_option")
							'response.write GetString("keyword="&keyword&"&keyword_option="&keyword_option)
							'//폼으로 넘어온경우 백로가면 검색화면 페이지표시할수 없음처리
							if len(request.Form("keyword")) > 0 then response.Redirect("?"&GetString("keyword="&keyword&"&keyword_option="&keyword_option))


							'//검색일경우 첫페이지로 돌리기 위한 설정
							if len(keyword_option) > 0 then
								where = " intSeq > 0 and "& keyword_option &" like '%"& keyword &"%' "
							else
								where  = " intSeq > 0 "
							end if


							'//카테고리 값이 넘어오는 경우 처리
							if len(requestQ("mcate")) > 0 then	where = where & " and strCategory = '"& requestQ("mcate") &"' "



							'##==========================================================================
							'##
							'##	페이징 관련 함수 - 게시판등...
							'##
							'##==========================================================================
							'##
							'## [설정법]
							'## 다음 코드가 상단에 위치 하여야 함
							'##
							
							dim intTotalCount, intTotalPage
							Dim r_no, b_idx, userid, username, r_icon, r_text, r_date, r_title, r_img, r_check, r_adminimg, r_num, r_hit
							dim intNowPage			: intNowPage 		= Request.QueryString("page")    
							dim intPageSize			: intPageSize 		= 10
							dim intBlockPage		: intBlockPage 		= 10

							dim query_filde			: query_filde		= "*"
							dim query_Tablename		: query_Tablename	= " Good_Reply"
							dim query_where			: query_where		= " r_no > 0 "
							dim query_orderby		: query_orderby		= " order by r_no DESC "
							
							
							Dim strSearch 			: strSearch 		= request("strSearch")
							Dim strKeyword			: strKeyword 		= request("strKeyword")
							
							If isValue(strKeyword) Then
								If strSearch= "g_name" Then
									query_Tablename = query_Tablename & ", OrderDetail "
									query_where = query_where & " and r_num = num and good_name like '%"&strKeyword&"%'"
								Else
									query_where = query_where & " and "&strSearch&" like '%"&strKeyword&"%'"
								End If
							Else
								strSearch = ""
								strKeyword = ""
							End If

							call intTotal

							'##
							'## 1. intTotal : call intTotal
							'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
							'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
							'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
							'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
							'##
							'##
							'#############################################################################

							dim sql, rs
							sql = GetQuery()
							'response.Write(sql)
							call dbopen
							set rs = dbconn.execute(sql)
						%>



							<form name = "frm" action = "?<%=getstring("")%>" onsubmit="return regist(this)" class="smart-form m10"  method="post" >
								<label class="select w10 pro_detail">
								<select class="input-sm h34" name="keyword_option" >
									<option value="r_title">제목</option>
									<option value="username">이름</option>
								</select> <i></i> </label>
								<div class="icon-addon addon-md col-md-10 col-2">
									<input type="text" name="keyword" class="form-control col-10">
									<label for="keyword" class="glyphicon glyphicon-search " rel="tooltip" title="keyword"></label>
								</div>
								<input type="submit" class="btn btn-default" value="검색">
								
							</form>	
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								 <colgroup>
									<col style="width:5%">
									<col style="width:25%">
									<col style="">
									<col style="width:10%">
									<col style="width:10%">
									<col style="width:5%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand">번호</th>
										<th data-class="expand">상품정보</th>
										<th>제목</th>
										<th data-hide="phone,tablet">글쓴이</th>
										<th data-hide="phone,tablet">작성일</th>
										<th data-hide="phone,tablet">조회</th>
									</tr>
								</thead>
	
								<tbody>
									<%
										dim pagei : pagei = (intTotalCount-MoveCount)
										'// 글이 없을 경우
										if  rs.eof then
									%>
									<%

										Else

									%>
									<%	
										Dim arrData,good_name,good_img
										rs.move MoveCount 
										Do while not rs.eof	'										
											r_no			= rs("r_no")
											b_idx 			= rs("b_idx")
											userid			= rs("userid")
											username		= rs("username")
											r_icon			= rs("r_icon")
											r_date			= rs("r_date")
											r_title			= rs("r_title")
											r_img			= rs("r_img")
											r_hit			= rs("r_hit")
											r_num			= rs("r_num")
											
											if datediff ("n",r_date,Now()) < 1440 then 
												dim strnewarticle : strnewarticle = " <img src='/img/b_new.gif' align='absmiddle' border='0'>&nbsp;"
											else
												strnewarticle = ""
											end if	
											
											good_img = ""
											
											arrData = getAdoRsArray("SELECT good_name,good_img FROM OrderDetail WHERE num ='"& r_num &"'")				
										
											If isArray(arrData) Then	
												 
												good_name 			= arrData(0,0)
												good_img		 	= arrData(1,0)				
											End If
											
											
									

									%>
		
									<tr>
										<td><%=pagei%></td>
										<td><a href="modify.asp?r_no=<%=r_no%>"><%=good_name%></a></td>
										<td><a href="modify.asp?r_no=<%=r_no%>"><%=leftH(r_title,10)%></a>
											<%If isValue(r_img) Then%><img src = "/admin/img/icon_file.gif" border="0" /><%End If%><%=strnewarticle%></a>
										</td>
										<td><%=username%></td>
										<td><%=left(r_date, 10)%></td>
										<td><%=r_hit%></td>
									</tr>
									
								<%
										pagei = pagei-1
									rs.movenext
									loop
									rs.close()
									set rs = nothing
								End If
								%>
								</tbody>
						
							</table>
							<%call Paging_list("")%>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->



			

			</article>
			<!-- WIDGET END -->

		
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		<!-- #include file="../inc/footer.asp" -->
