
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>BASIC</h2>
					</header>
	
					<!-- widget div-->
					<div class="mt50">
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">


							<%

							Dim g_idx,gNum,r_no
							Dim b_idx, userid, username, r_icon, r_text, r_date, r_title, r_img, r_check, r_adminimg, r_num, r_hit	
							Dim arrData,strAction,strUrl

							r_no    = requestQ("r_no")
							call dbopen()


							If isValue(r_no)  Then	
									
								arrData = getAdoRsArray("select * from Good_Reply where r_no = '"& r_no &"'")				
									
								If isArray(arrData) Then		 
									r_no 			= arrData(0,0)
									b_idx		 	= arrData(1,0)
									userid			= arrData(2,0)				
									username		= arrData(3,0)	
									r_icon			= arrData(4,0)	
									r_text			= arrData(5,0)	
									r_date			= arrData(6,0)	
									r_title			= arrData(7,0)	
									r_img			= arrData(8,0)	
									r_check			= arrData(9,0)	
									r_adminimg		= arrData(10,0)	
									r_num			= arrData(11,0)	
									r_hit			= arrData(12,0)			
								End If
								g_idx		= b_idx '상품번호
								gNum		= r_num '주문상세번호
								strAction 	= "update.asp?r_no="&r_no
								strUrl      = "location.href='list.asp'"
							Else
								 g_idx 		= requestQ("gIdx") '상품번호
								 gNum    	= requestQ("gNum") '주문상세번호
								strAction 	= "insert.asp"
								strUrl      ="document.reply.reset();"
							End If
							%>


							<form name="frmRequestForm" method="post" onsubmit="return frmRequestForm_Submit(this);"  action="<%=strAction%>" class="smart-form" enctype="multipart/form-data">
							   <input type = "hidden" name = "b_idx" value = "<%=g_idx%>">
							   <input type = "hidden" name = "gNum" value = "<%=gNum%>"> <!--주문상세번호를 넣어 한번만 작성하게.-->
							   <input type = "hidden" name = "userid" value = "<%=userid%>">  
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									
									<tr>
										<th>제목</th>
										<td>
											<label for="r_title" class="input">
												<input type="text" name="r_title" id="r_title" placeholder="" value = "<%=r_title%>" >
											</label>
										</td>
									</tr>
									
									<tr>
										<th>작성자</th>
										<td>
											<label for="username" class="input">
												<input type="text" name="username" id="username" placeholder="" value="<%=username%>">
											</label>
										</td>
									</tr>
									
									
									<tr>
										<th>평점</th>
										<td>
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="r_icon" value="5" <%If isValue(r_no) Then%><%=chk_checked(r_icon,5)%><%Else%>checked<%End If%>>
													<i></i>★★★★★</label>
												<label class="radio">
													<input type="radio" name="r_icon" value="4"  <%=chk_checked(r_icon,4)%>>
													<i></i>★★★★</label>
												<label class="radio">
													<input type="radio" name="r_icon" value="3"   <%=chk_checked(r_icon,3)%>>
													<i></i>★★★</label>
												<label class="radio">
													<input type="radio" name="r_icon" value="2"   <%=chk_checked(r_icon,2)%>>
													<i></i>★★</label>
												<label class="radio">
													<input type="radio" name="r_icon" value="1"  <%=chk_checked(r_icon,1)%>>
													<i></i>★</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>내용</th>
										<td colspan="2">
											<label class="textarea"> 										
												<textarea rows="3" name="r_text" class="h150 pro_detail"><%=r_text%></textarea> 
												
											</label>
										</td>
									</tr>
									<tr>
										<th>첨부파일</th>
										<td>
											<section>
												<div class="input input-file w20 pro_detail">
													<span class="button"><input id="r_img" type="file" name="r_img" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%If isValue(r_img) Then Response.write split(r_img,":")(1)%>">
												</div><% call FileDownLink_uppath(r_img,"/upload/")%>
											</section>
											
											<%If isValue(r_img) then%>
											<a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a>
											<%End if%>

										</td>
									</tr>
									<%If isValue(r_no) Then%>
									<tr>
										<th>관리자링크</th>
										<td>
											<label for="r_adminimg" class="input">
												<input type="text" name="r_adminimg" id="r_adminimg" placeholder="" value = "<%=r_adminimg%>" >
											</label>
										</td>
									</tr>
									<tr>
										<th>포토포인트</th>
										<td>
											<div class="inline-group margin_t">
												<label class="checkbox">
													<input type="checkbox" name="r_check" value = "1" <%=chk_checked(r_check,1)%>>
													<i></i>포토 포인트는 관리자가 체크를 해줘야만 주문이 정상거래가 될때 포인트가 플러스됩니다.</label>
											</div>
										</td>
									</tr>
									<%End If%>
									
								</tbody>
						
							</table>

							<footer style="float:right;">
								<input type="button" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
								<input type="button" class="btn btn-default"  onclick="window.history.back();" value="리스트">
								<input type="button" class="btn btn-default"  onclick="location.href='delete.asp?r_no=<%=r_no%>'" value="삭제">
							</footer>
							
							</form>	
							
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->

				<script language="javascript">
<!--

					function frmRequestForm_Submit(frm){
						if ( frm.r_title.value == "" ) { alert("제목을 입력해주세요"); frm.r_title.focus(); return false; }
						frm.submit();
					}

				//-->
				</script>


			

			</article>
			<!-- WIDGET END -->


		
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>
		<!-- #include file="../inc/footer.asp" -->