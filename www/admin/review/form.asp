
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>BASIC</h2>
					</header>
	
					<!-- widget div-->
					<div class="mt50">
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">



							<form name="frmRequestForm" method="post" onsubmit="return frmRequestForm_Submit(this);" class="smart-form" enctype="multipart/form-data">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									
									<tr>
										<th>제목</th>
										<td>
											<label for="strTitle" class="input">
												<input type="text" name="strTitle" id="strTitle" placeholder="">
											</label>
										</td>
									</tr>
									
									<tr>
										<th>이름</th>
										<td>
											<label for="strName" class="input">
												<input type="text" name="strName" id="strName" placeholder="">
											</label>
										</td>
									</tr>
									<tr>
										<th>라디오</th>
										<td>
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="strRadio" checked="checked" value="Alexandra">
													<i></i>Alexandra</label>
												<label class="radio">
													<input type="radio" name="strRadio" value="Alice</">
													<i></i>Alice</label>
												<label class="radio">
													<input type="radio" name="strRadio" value="Anastasia">
													<i></i>Anastasia</label>
												<label class="radio">
													<input type="radio" name="strRadio" value="Avelina">
													<i></i>Avelina</label>
												<label class="radio">
													<input type="radio" name="strRadio" value="Beatrice">
													<i></i>Beatrice</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>체크박스</th>
										<td>
											<div class="inline-group margin_t">
												<label class="checkbox">
													<input type="checkbox" name="strChack" checked="checked" value="Red">
													<i></i>Red</label>
												<label class="checkbox">
													<input type="checkbox" name="strChack" value="Blue">
													<i></i>Blue</label>
												<label class="checkbox">
													<input type="checkbox" name="strChack" value="Black">
													<i></i>Black</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>셀렉트</th>
										<td>
											<label class="select state-disabled w20">
											<select class="input-sm" name="strSelect">
												<option value="0">Choose name</option>
												<option value="1">Alexandra</option>
												<option value="2">Alice</option>
												<option value="3">Anastasia</option>
												<option value="4">Avelina</option>
											</select> <i></i> </label>
										</td>
									</tr>


									<tr>
										<th>파일</th>
										<td>
										<div class="input input-file w20">
											<span class="button"><input type="file" id="file" name="strImage" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
										</div>
										</td>
									</tr>
									
									<tr class="h300">
										<th>에디터</th>
										<td><%call Editor("strcontent","")%></td>
									</tr>
									
								</tbody>
						
							</table>

							<footer style="float:right;">
								<input type="button" class="btn btn-primary" onclick="this.form.onsubmit();" value="작성하기"></button>
							</footer>
							
							</form>	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->

				<script type="text/javascript">

					function frmRequestForm_Submit(frm){
						if ( frm.strTitle.value == "" ) { alert("제목을 입력해주세요"); frm.strTitle.focus(); return false; }
						frm.action = "basic_insert.asp"
						frm.submit();
					}

				</script>


			

			</article>
			<!-- WIDGET END -->

			
		</div>

		</div>
		<!-- END MAIN PANEL -->

		<!-- #include file="../inc/footer.asp" -->



		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

	</body>

</html>