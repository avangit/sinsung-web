
		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config.asp" -->

			<!-- NEW WIDGET START -->
			<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
			<script>
				function openDaumPostcode() {new daum.Postcode({
						oncomplete: function(data) {
							// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

							// 각 주소의 노출 규칙에 따라 주소를 조합한다.
							// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
							var fullAddr = ''; // 최종 주소 변수
							var extraAddr = ''; // 조합형 주소 변수

							// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
							if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
								fullAddr = data.roadAddress;

							} else { // 사용자가 지번 주소를 선택했을 경우(J)
								fullAddr = data.jibunAddress;
							}

							// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
							if(data.userSelectedType === 'R'){
								//법정동명이 있을 경우 추가한다.
								if(data.bname !== ''){
									extraAddr += data.bname;
								}
								// 건물명이 있을 경우 추가한다.
								if(data.buildingName !== ''){
									extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
								}
								// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
								fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
							}

							// 우편번호와 주소 정보를 해당 필드에 넣는다.
							document.getElementById('cZip').value = data.zonecode; //5자리 새우편번호 사용
							document.getElementById('cAddr1').value = fullAddr;

							// 커서를 상세주소 필드로 이동한다.
							document.getElementById('cAddr2').focus();
						}
					}).open();
				}
			</script>
			<article>
			<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>회원정보</h2>
					</header>

					<!-- widget div-->
					<div class="mt50">

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<form name = "joinform" action = "insert.asp" class="smart-form" method = "post" enctype="multipart/form-data" onsubmit="join_chk();return false;">
							<input type="hidden" name="num" value="">
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>회원구분</th>
										<td colspan="3">
											<label class="select col-1">
											<select name="intGubun">
												<option value="1">일반</option>
												<option value="2">마스터</option>
											</select><i></i>
											</label>
										</td>
									</tr>
									<tr>
										<th>아이디 *</th>
										<td colspan="3">
											<input type="text" name="strId" id="strId">&nbsp;&nbsp;<input type="button" onclick="id_check()" value="중복체크">
											<span class="add_tx" id="id_ck" style="padding-left:0px; margin-left:0px;"></span>
										</td>
									</tr>
									<tr>
										<th>비밀번호 *</th>
										<td><input type="password" name="strPwd" id="strPwd"><button value="중복테크"></button></td>
										<th>비밀번호 재확인 *</th>
										<td><input type="password" name="strPwdRe" id="strPwdRe"></td>
									</tr>
									<tr>
										<th>이름 *</th>
										<td><input type="text" name="strName" id="strName"></td>
										<th>전화번호</th>
										<td>
											<input type="text" name="strPhone1" id="strPhone1" style="width:50px" maxlength="4"> - <input type="text" name="strPhone2" id="strPhone2" style="width:50px" maxlength="4"> - <input type="text" name="strPhone3" id="strPhone3" style="width:50px" maxlength="4">
										</td>
									</tr>
									<tr>
										<th>휴대폰번호 *</th>
										<td colspan="3">
											<input type="text" name="strMobile1" id="strMobile1" style="width:50px" maxlength="4"> - <input type="text" name="strMobile2" id="strMobile2" style="width:50px" maxlength="4"> - <input type="text" name="strMobile3" id="strMobile3" style="width:50px" maxlength="4">&nbsp;&nbsp;<input type="checkbox" name="smsYN" VALUE="Y"> 수신동의</td>
									</tr>
									<tr>
										<th>이메일주소 *</th>
										<td colspan="3">
											<input type="text" name="strEmail1" id="strEmail1" style="width:150px"> @ <input type="text" name="strEmail2" id="strEmail2" style="width:150px" readonly>
											<select name="strEmail3" id="strEmail3" onchange="ChangeEmail1();">
												<option value="">메일주소 선택</option>
												<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												<option value="empal.com">empal.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="lycos.co.kr">lycos.co.kr</option>
												<option value="naver.com">naver.com</option>
												<option value="nate.com">nate.com</option>
												<option value="netsgo.com">netsgo.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="paran.com">paran.com</option>
												<option value="0">직접입력</option>
											</select><i></i>&nbsp;&nbsp;<input type="checkbox" name="mailYN" VALUE="Y"> 수신동의
										</td>
									</tr>
									<tr>
										<th>여신결재 사용여부</th>
										<td colspan="3">
											<input type="radio" name="yeosinYN" value="Y"> 사용&nbsp;&nbsp;
											<input type="radio" name="yeosinYN" value="N" checked> 미사용
										</td>
									</tr>
								</tbody>

							</table>
						</div>
					</div>
				</div>

				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>회사정보</h2>
					</header>

					<div class="mt50">
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>상호(법인명) *</th>
										<td>
											<input type="text" name="cName" id="cName" style="width:220px">
										</td>
										<th>사업자등록번호 *</th>
										<td>
											<input type="text" name="cNum1" id="cNum1" style="width:50px" maxlength="3"> - <input type="text" name="cNum2" id="cNum2" style="width:50px" maxlength="2"> - <input type="text" name="cNum3" id="cNum3" style="width:50px" maxlength="5">
										</td>
									</tr>
									<tr>
										<th>대표자</th>
										<td>
											<input type="text" name="ceo" id="ceo" style="width:220px">
										</td>
										<th>전화번호</th>
										<td>
											<input type="text" name="cPhone1" id="cPhone1" style="width:50px" maxlength="4"> - <input type="text" name="cPhone2" id="cPhone2" style="width:50px" maxlength="4"> - <input type="text" name="cPhone3" id="cPhone3" style="width:50px" maxlength="4">
										</td>
									</tr>
									<tr>
										<th>휴대폰번호</th>
										<td>
											<input type="text" name="cMobile1" id="cMobile1" style="width:50px" maxlength="4"> - <input type="text" name="cMobile2" id="cMobile2" style="width:50px" maxlength="4"> - <input type="text" name="cMobile3" id="cMobile3" style="width:50px" maxlength="4">
										</td>
										<th>이메일주소</th>
										<td>
											<input type="text" name="cEmail1" id="cEmail1" style="width:150px"> @ <input type="text" name="cEmail2" id="cEmail2" style="width:150px" readonly>
											<label class="select col-3">
											<select name="cEmail3" id="cEmail3" onchange="ChangeEmail2();">
												<option value="">메일주소 선택</option>
												<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												<option value="empal.com">empal.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="lycos.co.kr">lycos.co.kr</option>
												<option value="naver.com">naver.com</option>
												<option value="nate.com">nate.com</option>
												<option value="netsgo.com">netsgo.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="paran.com">paran.com</option>
												<option value="0">직접입력</option>
											</select><i></i>
											</label>
										</td>
									</tr>
									<tr>
										<th>사업장주소(도로명주소) *</th>
										<td colspan="3">
											<p class="pd3-0">
												<input type="text" name="cZip" id="cZip" class="AXInput W100" readonly="readonly">
												<input type="button" value="우편번호검색" class="AXButtonSmall Classic" onclick="openDaumPostcode();">
											</p>
											<p class="pd3-0"><input type="text" name="cAddr1" id="cAddr1" class="AXInput" readonly="readonly" style="width:30%"></p>
											<p class="pd3-0"><input type="text" name="cAddr2" id="cAddr2" class="AXInput" style="width:30%"></p>
										</td>
									</tr>
									<tr>
										<th>업태</th>
										<td>
											<input type="text" name="cType" id="cType" style="width:220px">
										</td>
										<th>종목</th>
										<td>
											<input type="text" name="cCate" id="cCate" style="width:220px">
										</td>
									</tr>
									<tr>
										<th>사업자등록증 사본</th>
										<td>
											<input type="file" name="strFile1" class="AXInput w50p"> 첨부파일 : 최대 용량 10M 이하
										</td>
										<th>회사CI</th>
										<td>
											<input type="file" name="strFile2" class="AXInput w50p"> 첨부파일 : 최대 용량 10M 이하
										</td>
									</tr>
								</tbody>
							</table>

							<footer style="float:right;">
								<input type="submit" class="btn btn-primary" value="등록">
								<a href="./list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
							</footer>

							</form>
						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->
			</article>
			<!-- WIDGET END -->

		<script>
			$(document).ready(function() {
				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			});

		</script>
		<!-- #include file="../inc/footer.asp" -->