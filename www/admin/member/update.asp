<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!-- #include file="config.asp" -->
<%
'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/member/")
UploadForm.MaxFileLen = int(1024 * 1024 * 10)

intseq			= SQL_Injection(Trim(UploadForm("intseq")))
ori_intGubun	= SQL_Injection(Trim(UploadForm("ori_intGubun")))
intGubun		= SQL_Injection(Trim(UploadForm("intGubun")))
strId			= SQL_Injection(Trim(UploadForm("strId")))
strPwd			= SQL_Injection(Trim(UploadForm("strPwd")))
strName			= SQL_Injection(Trim(UploadForm("strName")))
strPhone1		= SQL_Injection(Trim(UploadForm("strPhone1")))
strPhone2		= SQL_Injection(Trim(UploadForm("strPhone2")))
strPhone3		= SQL_Injection(Trim(UploadForm("strPhone3")))
strMobile1		= SQL_Injection(Trim(UploadForm("strMobile1")))
strMobile2		= SQL_Injection(Trim(UploadForm("strMobile2")))
strMobile3		= SQL_Injection(Trim(UploadForm("strMobile3")))
smsYN			= SQL_Injection(Trim(UploadForm("smsYN")))
strEmail1		= SQL_Injection(Trim(UploadForm("strEmail1")))
strEmail2		= SQL_Injection(Trim(UploadForm("strEmail2")))
mailYN			= SQL_Injection(Trim(UploadForm("mailYN")))
yeosinYN		= SQL_Injection(Trim(UploadForm("yeosinYN")))

cName			= SQL_Injection(Trim(UploadForm("cName")))
cNum1			= SQL_Injection(Trim(UploadForm("cNum1")))
cNum2			= SQL_Injection(Trim(UploadForm("cNum2")))
cNum3			= SQL_Injection(Trim(UploadForm("cNum3")))
ceo				= SQL_Injection(Trim(UploadForm("ceo")))
cPhone1			= SQL_Injection(Trim(UploadForm("cPhone1")))
cPhone2			= SQL_Injection(Trim(UploadForm("cPhone2")))
cPhone3			= SQL_Injection(Trim(UploadForm("cPhone3")))
cMobile1		= SQL_Injection(Trim(UploadForm("cMobile1")))
cMobile2		= SQL_Injection(Trim(UploadForm("cMobile2")))
cMobile3		= SQL_Injection(Trim(UploadForm("cMobile3")))
cEmail1			= SQL_Injection(Trim(UploadForm("cEmail1")))
cEmail2			= SQL_Injection(Trim(UploadForm("cEmail2")))
cZip			= SQL_Injection(Trim(UploadForm("cZip")))
cAddr1			= SQL_Injection(Trim(UploadForm("cAddr1")))
cAddr2			= SQL_Injection(Trim(UploadForm("cAddr2")))
comType			= SQL_Injection(Trim(UploadForm("cType")))
cCate			= SQL_Injection(Trim(UploadForm("cCate")))
ori_file1_del1	= SQL_Injection(Trim(UploadForm("ori_file1_del1")))
ori_file1_del2	= SQL_Injection(Trim(UploadForm("ori_file1_del2")))

manager1		= SQL_Injection(Trim(UploadForm("manager1")))
m1_email1		= SQL_Injection(Trim(UploadForm("m1_email1")))
m1_email2		= SQL_Injection(Trim(UploadForm("m1_email2")))
m1_Phone1		= SQL_Injection(Trim(UploadForm("m1_Phone1")))
m1_Phone2		= SQL_Injection(Trim(UploadForm("m1_Phone2")))
m1_Phone3		= SQL_Injection(Trim(UploadForm("m1_Phone3")))
m1_Mobile1		= SQL_Injection(Trim(UploadForm("m1_Mobile1")))
m1_Mobile2		= SQL_Injection(Trim(UploadForm("m1_Mobile2")))
m1_Mobile3		= SQL_Injection(Trim(UploadForm("m1_Mobile3")))
manager2		= SQL_Injection(Trim(UploadForm("manager2")))
m2_email1		= SQL_Injection(Trim(UploadForm("m2_email1")))
m2_email2		= SQL_Injection(Trim(UploadForm("m2_email2")))
m2_Phone1		= SQL_Injection(Trim(UploadForm("m2_Phone1")))
m2_Phone2		= SQL_Injection(Trim(UploadForm("m2_Phone2")))
m2_Phone3		= SQL_Injection(Trim(UploadForm("m2_Phone3")))
m2_Mobile1		= SQL_Injection(Trim(UploadForm("m2_Mobile1")))
m2_Mobile2		= SQL_Injection(Trim(UploadForm("m2_Mobile2")))
m2_Mobile3		= SQL_Injection(Trim(UploadForm("m2_Mobile3")))
manager3		= SQL_Injection(Trim(UploadForm("manager3")))
m3_email1		= SQL_Injection(Trim(UploadForm("m3_email1")))
m3_email2		= SQL_Injection(Trim(UploadForm("m3_email2")))
m3_Phone1		= SQL_Injection(Trim(UploadForm("m3_Phone1")))
m3_Phone2		= SQL_Injection(Trim(UploadForm("m3_Phone2")))
m3_Phone3		= SQL_Injection(Trim(UploadForm("m3_Phone3")))
m3_Mobile1		= SQL_Injection(Trim(UploadForm("m3_Mobile1")))
m3_Mobile2		= SQL_Injection(Trim(UploadForm("m3_Mobile2")))
m3_Mobile3		= SQL_Injection(Trim(UploadForm("m3_Mobile3")))

'// 변수 가공
strPhone = strPhone1 & "-" & strPhone2 & "-" & strPhone3
strMobile = strMobile1 & "-" & strMobile2 & "-" & strMobile3
strEmail = strEmail1 & "@" & strEmail2
cNum = cNum1 & "-" & cNum2 & "-" & cNum3
cPhone = cPhone1 & "-" & cPhone2 & "-" & cPhone3
cMobile = cMobile1 & "-" & cMobile2 & "-" & cMobile3
cEmail = cEmail1 & "@" & cEmail2
m1_email = m1_email1 & "@" & m1_email2
m1_Phone = m1_Phone1 & "-" & m1_Phone2 & "-" & m1_Phone3
m1_Mobile = m1_Mobile1 & "-" & m1_Mobile2 & "-" & m1_Mobile3
m2_email = m2_email1 & "@" & m2_email2
m2_Phone = m2_Phone1 & "-" & m2_Phone2 & "-" & m2_Phone3
m2_Mobile = m2_Mobile1 & "-" & m2_Mobile2 & "-" & m2_Mobile3
m3_email = m3_email1 & "@" & m3_email2
m3_Phone = m3_Phone1 & "-" & m3_Phone2 & "-" & m3_Phone3
m3_Mobile = m3_Mobile1 & "-" & m3_Mobile2 & "-" & m3_Mobile3

If smsYN <> "Y" Then
	smsYN = "N"
End If

If mailYN <> "Y" Then
	mailYN = "N"
End If

Call DbOpen()

strSQL = "SELECT strFile1, strFile2 FROM " & tablename & " WHERE intseq = '" & intseq & "'"

Set rs = dbconn.execute(strSQL)

ReDim mFile(1), rsFile(1)

If Not(rs.eof) Then
	For sf=1 To UploadCnt
		rsFile(sf-1) = rs("strFile" & sf)
	Next
End If

Set rs = Nothing

For i = 1 To UploadCnt
	If UploadForm("strFile" & i).FileName <> "" Then
		'// 업로드 가능 여부 체크
		If UploadFileChk(UploadForm("strFile" & i).FileName)=False Then
			Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 용량 체크
		If UploadForm("strFile" & i).FileLen > UploadForm.MaxFileLen Then
			Response.Write "<script language='javascript'>alert('10MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'기존파일 삭제
		If rsFile(i-1) <> "" Then
			UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile(i-1)
		End If

		'// 파일 저장
		mFile(i-1) = UploadForm("strFile" & i).Save(, False)
		mFile(i-1) = UploadForm("strFile" & i).LastSavedFileName

		Sql = "UPDATE " & tablename & " SET strFile" & i & "=N'" & SQL_Injection(mFile(i-1)) & "' WHERE intseq='" & intseq & "'" & Chr(10) & Chr(13)
		dbconn.execute(Sql)

	'// 새로운 파일 업로드 없이 기존 파일 삭제만 선택한 경우
	ElseIf UploadForm("strFile" & i).FileName = "" And UploadForm("ori_file1_del" & i) = "Y" Then
		'기존파일 삭제
		If rsFile(i-1) <> "" Then
			UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile(i-1)
		End If

		Sql = "UPDATE " & tablename & " SET strFile" & i & "='' WHERE intseq='" & intseq & "'" & Chr(10) & Chr(13)
		dbconn.execute(Sql)
	End If
Next

Set UploadForm = Nothing

Sql = "UPDATE " & tablename & " SET "
Sql = Sql & "intGubun='" & intGubun & "', "
If strPwd <> "" Then
	Sql = Sql & "strPwd='" & Encrypt_Sha(strPwd) & "', "
End If
Sql = Sql & "strName = N'" & strName & "', "
Sql = Sql & "strPhone = '" & strPhone & "', "
Sql = Sql & "strMobile = '" & strMobile & "', "
Sql = Sql & "smsYN = '" & smsYN & "', "
Sql = Sql & "strEmail = N'" & strEmail & "', "
Sql = Sql & "mailYN = '" & mailYN & "', "
Sql = Sql & "yeosinYN = '" & yeosinYN & "', "
Sql = Sql & "cName = N'" & cName & "', "
Sql = Sql & "cNum = '" & cNum & "', "
Sql = Sql & "ceo = N'" & ceo & "', "
Sql = Sql & "cPhone = '" & cPhone & "', "
Sql = Sql & "cMobile = '" & cMobile & "', "
Sql = Sql & "cEmail = N'" & cEmail & "', "
Sql = Sql & "cZip = '" & cZip & "', "
Sql = Sql & "cAddr1 = N'" & cAddr1 & "', "
Sql = Sql & "cAddr2 = N'" & cAddr2 & "', "
Sql = Sql & "cType = N'" & comType & "', "
Sql = Sql & "cCate = N'" & cCate & "', "
Sql = Sql & "manager1 = N'" & manager1 & "', "
Sql = Sql & "m1_Email = N'" & m1_Email & "', "
Sql = Sql & "m1_Phone = '" & m1_Phone & "', "
Sql = Sql & "m1_Mobile = '" & m1_Mobile & "', "
Sql = Sql & "manager2 = N'" & manager2 & "', "
Sql = Sql & "m2_Email = N'" & m2_Email & "', "
Sql = Sql & "m2_Phone = '" & m2_Phone & "', "
Sql = Sql & "m2_Mobile = '" & m2_Mobile & "', "
Sql = Sql & "manager3 = N'" & manager3 & "', "
Sql = Sql & "m3_Email = N'" & m3_Email & "', "
Sql = Sql & "m3_Phone = '" & m3_Phone & "', "
Sql = Sql & "m3_Mobile = '" & m3_Mobile & "', "
Sql = Sql & "update_date = '" & Date() & "' "
Sql = Sql & " WHERE intseq = '" & intseq & "'"

dbconn.execute(Sql)

If intGubun <> ori_intGubun Then
	dbconn.execute("INSERT INTO Record_views2 (work1,before_right,change_right,member_id,manage_id,ip) values('변경','"&ori_intGubun&"','"&intGubun&"','"&strId&"','"&session("aduserid")&"','"&Request.ServerVariables("REMOTE_ADDR")&"') ")
End if

Call DbClose()

Call jsAlertMsgUrl("수정되었습니다.", "modify.asp?intSeq="&intseq&"&menucode="&menucode)
%>