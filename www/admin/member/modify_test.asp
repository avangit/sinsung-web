
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config.asp" -->

			<!-- NEW WIDGET START -->
			<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
			<script>
				function openDaumPostcode() {new daum.Postcode({
						oncomplete: function(data) {
							// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

							// 각 주소의 노출 규칙에 따라 주소를 조합한다.
							// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
							var fullAddr = ''; // 최종 주소 변수
							var extraAddr = ''; // 조합형 주소 변수

							// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
							if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
								fullAddr = data.roadAddress;

							} else { // 사용자가 지번 주소를 선택했을 경우(J)
								fullAddr = data.jibunAddress;
							}

							// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
							if(data.userSelectedType === 'R'){
								//법정동명이 있을 경우 추가한다.
								if(data.bname !== ''){
									extraAddr += data.bname;
								}
								// 건물명이 있을 경우 추가한다.
								if(data.buildingName !== ''){
									extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
								}
								// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
								fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
							}

							// 우편번호와 주소 정보를 해당 필드에 넣는다.
							document.getElementById('cZip').value = data.zonecode; //5자리 새우편번호 사용
							document.getElementById('cAddr1').value = fullAddr;

							// 커서를 상세주소 필드로 이동한다.
							document.getElementById('cAddr2').focus();
						}
					}).open();
				}
			</script>
			<script type="text/javascript">
				
		$(document).on('ready', function() {

			$(".tab_contents").hide();
			$(".modify_tab li:first").addClass("active").show();
			$(".tab_contents:first").show();

			$(".modify_tab li").click(function() {
				$(".modify_tab li").removeClass("active");
				$(this).addClass("active");
				$(".tab_contents").hide();
				var activeTab = $(this).find("a").attr("href");
				$(activeTab).fadeIn();
				return false;
			});
		});
    
	
			</script>
			<ul class="modify_tab">
				<li><a href="#tab01">회원정보</a></li>
				<li><a href="#tab02">하위회원</a></li>
				<li><a href="#tab03">회원 히스토리</a></li>
			</ul>
			<article id="tab01" class="tab_contents">
			<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>회원정보</h2>
					</header>

					<!-- widget div-->
					<div class="mt50">

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
<%
	intseq = request.QueryString("intseq")
	menucode = request.QueryString("menucode")

	strSQL = "SELECT * FROM "&tablename& " WHERE intseq = '"&intseq&"'"

	Call DbOpen()

	Set rs = dbconn.execute(strSQL)

	If Not(rs.eof) Then

		intGubun		= rs("intGubun")
		strId 			= rs("strId")
		strName 		= rs("strName")
		strPhone 		= rs("strPhone")
		strMobile		= rs("strMobile")
		smsYN			= rs("smsYN")
		strEmail 		= rs("strEmail")
		mailYN			= rs("mailYN")
		yeosinYN		= rs("yeosinYN")

		cName			= rs("cName")
		cNum 			= rs("cNum")
		ceo 			= rs("ceo")
		cPhone 			= rs("cPhone")
		cMobile 		= rs("cMobile")
		cEmail 			= rs("cEmail")
		cZip			= rs("cZip")
		cAddr1 			= rs("cAddr1")
		cAddr2 			= rs("cAddr2")
		comType 		= rs("cType")
		cCate 			= rs("cCate")
		strFile1 		= rs("strFile1")
		strFile2 		= rs("strFile2")
		manager1 		= rs("manager1")
		m1_Email 		= rs("m1_Email")
		m1_Phone 		= rs("m1_Phone")
		m1_Mobile 		= rs("m1_Mobile")
		manager2 		= rs("manager2")
		m2_Email 		= rs("m2_Email")
		m2_Phone 		= rs("m2_Phone")
		m2_Mobile 		= rs("m2_Mobile")
		manager3 		= rs("manager3")
		m3_Email 		= rs("m3_Email")
		m3_Phone 		= rs("m3_Phone")
		m3_Mobile 		= rs("m3_Mobile")
		dtmInsertDate 	= rs("dtmInsertDate")
		login_date		= rs("login_date")
		login_cnt		= rs("login_cnt")
		login_ip		= rs("login_ip")
	Else
		response.Write("코드에러")
	End If

	If cNum <> "" Then
		ArrComNum = Split(cNum, "-")
		If Ubound(ArrComNum) = 2 Then
			cNum1 = ArrComNum(0)
			cNum2 = ArrComNum(1)
			cNum3 = ArrComNum(2)
		End If
	End If

	If strPhone <> "" Then
		ArrPhone = Split(strPhone, "-")
		If Ubound(ArrPhone) = 2 Then
			strPhone1 = ArrPhone(0)
			strPhone2 = ArrPhone(1)
			strPhone3 = ArrPhone(2)
		End If
	End If

	If strMobile <> "" Then
		ArrMobile = Split(strMobile, "-")
		If Ubound(ArrMobile) = 2 Then
			strMobile1 = ArrMobile(0)
			strMobile2 = ArrMobile(1)
			strMobile3 = ArrMobile(2)
		End If
	End If

	If strEmail <> "" Then
		ArrstrEmail = Split(strEmail, "@")
		If Ubound(ArrstrEmail) = 1 Then
			strEmail1 = ArrstrEmail(0)
			strEmail2 = ArrstrEmail(1)
		End If
	End If

	If cPhone <> "" Then
		ArrcPhone = Split(cPhone, "-")
		If Ubound(ArrcPhone) = 2 Then
			cPhone1 = ArrcPhone(0)
			cPhone2 = ArrcPhone(1)
			cPhone3 = ArrcPhone(2)
		End If
	End If

	If cMobile <> "" Then
		ArrcMobile = Split(cMobile, "-")
		If Ubound(ArrcMobile) = 2 Then
			cMobile1 = ArrcMobile(0)
			cMobile2 = ArrcMobile(1)
			cMobile3 = ArrcMobile(2)
		End If
	End If

	If cEmail <> "" Then
		ArrcEmail = Split(cEmail, "@")
		If Ubound(ArrcEmail) = 1 Then
			cEmail1 = ArrcEmail(0)
			cEmail2 = ArrcEmail(1)
		End If
	End If

	If m1_Email <> "" Then
		Arrm1_Email = Split(m1_Email, "@")
		If Ubound(Arrm1_Email) = 1 Then
			m1_Email1 = Arrm1_Email(0)
			m1_Email2 = Arrm1_Email(1)
		End If
	End If

	If m1_Phone <> "" Then
		Arrm1_Phone = Split(m1_Phone, "-")
		If Ubound(Arrm1_Phone) = 2 Then
			m1_Phone1 = Arrm1_Phone(0)
			m1_Phone2 = Arrm1_Phone(1)
			m1_Phone3 = Arrm1_Phone(2)
		End If
	End If

	If m1_Mobile <> "" Then
		Arrm1_Mobile = Split(m1_Mobile, "-")
		If Ubound(Arrm1_Mobile) = 2 Then
			m1_Mobile1 = Arrm1_Mobile(0)
			m1_Mobile2 = Arrm1_Mobile(1)
			m1_Mobile3 = Arrm1_Mobile(2)
		End If
	End If

	If m2_Email <> "" Then
		Arrm2_Email = Split(m2_Email, "@")
		If Ubound(Arrm2_Email) = 1 Then
			m2_Email1 = Arrm2_Email(0)
			m2_Email2 = Arrm2_Email(1)
		End If
	End If

	If m2_Phone <> "" Then
		Arrm2_Phone = Split(m2_Phone, "-")
		If Ubound(Arrm2_Phone) = 2 Then
			m2_Phone1 = Arrm2_Phone(0)
			m2_Phone2 = Arrm2_Phone(1)
			m2_Phone3 = Arrm2_Phone(2)
		End If
	End If

	If m2_Mobile <> "" Then
		Arrm2_Mobile = Split(m2_Mobile, "-")
		If Ubound(Arrm2_Mobile) = 2 Then
			m2_Mobile1 = Arrm2_Mobile(0)
			m2_Mobile2 = Arrm2_Mobile(1)
			m2_Mobile3 = Arrm2_Mobile(2)
		End If
	End If

	If m3_Email <> "" Then
		Arrm3_Email = Split(m3_Email, "@")
		If Ubound(Arrm3_Email) = 1 Then
			m3_Email1 = Arrm3_Email(0)
			m3_Email2 = Arrm3_Email(1)
		End If
	End If

	If m3_Phone <> "" Then
		Arrm3_Phone = Split(m3_Phone, "-")
		If Ubound(Arrm3_Phone) = 2 Then
			m3_Phone1 = Arrm3_Phone(0)
			m3_Phone2 = Arrm3_Phone(1)
			m3_Phone3 = Arrm3_Phone(2)
		End If
	End If

	If m3_Mobile <> "" Then
		Arrm3_Mobile = Split(m3_Mobile, "-")
		If Ubound(Arrm3_Mobile) = 2 Then
			m3_Mobile1 = Arrm3_Mobile(0)
			m3_Mobile2 = Arrm3_Mobile(1)
			m3_Mobile3 = Arrm3_Mobile(2)
		End If
	End If

	menu_name = "회원리스트>회원상세(" & strId & ")"

	dbconn.execute("INSERT INTO Record_views (menu,manage_id,manage_name,ip) VALUES('"&menu_name&"','"&session("aduserid")&"','"&session("adusername")&"','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

	Set rs = Nothing
	Call DbClose()
%>
							<form name = "joinform" action = "update.asp?<%=getstring("")%>" class="smart-form" method = "post" enctype="multipart/form-data" >
							<input type = "hidden" name = "intseq" value = "<%=intseq%>">
							<input type = "hidden" name = "ori_intGubun" value = "<%=intGubun%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>회원구분</th>
										<td colspan="3">
											<label class="select col-1">
											<select name="intGubun">
												<option value="">--회원구분</option>
												<option value="1" <% If intGubun = 1 Then %> selected<% End If %>>일반</option>
												<option value="2" <% If intGubun = 2 Then %> selected<% End If %>>마스터</option>
											</select><i></i>
											</label>
										</td>
									</tr>
									<tr>
										<th>아이디</th>
										<td><%=strId%></td>
										<th>비밀번호</th>
										<td><input type="password" name="strPwd" id="strPwd"></td>
									</tr>
									<tr>
										<th>이름</th>
										<td><input type="text" name="strName" id="strName" value="<%=strName%>"></td>
										<th>전화번호</th>
										<td>
											<input type="text" name="strPhone1" id="strPhone1" style="width:50px" maxlength="4" value="<%=strPhone1%>"> - <input type="text" name="strPhone2" id="strPhone2" style="width:50px" maxlength="4" value="<%=strPhone2%>"> - <input type="text" name="strPhone3" id="strPhone3" style="width:50px" maxlength="4" value="<%=strPhone3%>">
										</td>
									</tr>
									<tr>
										<th>휴대폰번호</th>
										<td><input type="text" name="strMobile1" id="strMobile1" style="width:50px" maxlength="4" value="<%=strMobile1%>"> - <input type="text" name="strMobile2" id="strMobile2" style="width:50px" maxlength="4" value="<%=strMobile2%>"> - <input type="text" name="strMobile3" id="strMobile3" style="width:50px" maxlength="4" value="<%=strMobile3%>">&nbsp;&nbsp;<input type="checkbox" name="smsYN" VALUE="Y" <% If smsYN = "Y" Then %> checked<% End If %>> 수신동의</td>
										<th>이메일주소</th>
										<td>
											<input type="text" name="strEmail1" id="strEmail1" style="width:150px" value="<%=strEmail1%>"> @ <input type="text" name="strEmail2" id="strEmail2" style="width:150px" readonly value="<%=strEmail2%>">
											<select name="strEmail3" id="strEmail3" onchange="ChangeEmail1();">
												<option value="">메일주소 선택</option>
												<option value="dreamwiz.co.kr" <% If strEmail2 = "dreamwiz.co.kr" Then %> selected<% End If %>>dreamwiz.co.kr</option>
												<option value="empal.com" <% If strEmail2 = "empal.com" Then %> selected<% End If %>>empal.com</option>
												<option value="gmail.com" <% If strEmail2 = "gmail.com" Then %> selected<% End If %>>gmail.com</option>
												<option value="lycos.co.kr" <% If strEmail2 = "lycos.co.kr" Then %> selected<% End If %>>lycos.co.kr</option>
												<option value="naver.com" <% If strEmail2 = "naver.com" Then %> selected<% End If %>>naver.com</option>
												<option value="nate.com" <% If strEmail2 = "nate.com" Then %> selected<% End If %>>nate.com</option>
												<option value="netsgo.com" <% If strEmail2 = "netsgo.com" Then %> selected<% End If %>>netsgo.com</option>
												<option value="hanmail.net" <% If strEmail2 = "hanmail.net" Then %> selected<% End If %>>hanmail.net</option>
												<option value="hotmail.com" <% If strEmail2 = "hotmail.com" Then %> selected<% End If %>>hotmail.com</option>
												<option value="paran.com" <% If strEmail2 = "paran.com" Then %> selected<% End If %>>paran.com</option>
												<option value="0">직접입력</option>
											</select><i></i>&nbsp;&nbsp;<input type="checkbox" name="mailYN" VALUE="Y" <% If mailYN = "Y" Then %> checked<% End If %>> 수신동의
										</td>
									</tr>
									<tr>
										<th>여신결재 사용여부</th>
										<td colspan="3">
											<input type="radio" name="yeosinYN" value="Y" <% If yeosinYN = "Y" Then %> checked<% End If %>> 사용&nbsp;&nbsp;
											<input type="radio" name="yeosinYN" value="N" <% If yeosinYN = "N" Then %> checked<% End If %>> 미사용
										</td>
									</tr>
									<tr>
										<th>가입일</th>
										<td><%=dtmInsertDate%></td>
										<th>마지막방문일</th>
										<td><%=login_date%>&nbsp;&nbsp;접속IP : (<%=login_ip%>)</td>
									</tr>
									<tr>
										<th>로그인 수</th>
										<td><%=login_cnt%></td>
										<th>구매 수</th>
										<td><%'=login_cnt%></td>
									</tr>
									<!--tr>
										<th>메모</th>
										<td colspan="3">
											<label class="textarea">
												<textarea name="strMemo" class="h150 pro_detail"><%'=strMemo%></textarea>

											</label>
											추천인 아이디 : <%'=strAgreeId%>
										</td>
									</tr>
									<tr>
										<th></th>
										<td colspan="3">
											<a href="../sms/form.asp?intSeq=<%=intSeq%>" class="btn btn-default"><i class="fa fa-comments"></i>SMS</a>
											<a href="../mail/form.asp?intSeq=<%=intSeq%>" class="btn btn-default"><i class="fa fa-envelope"></i>MAIL</a>

										</td>
									</tr-->
								</tbody>

							</table>
						</div>
					</div>
				</div>

				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>회사정보</h2>
					</header>

					<div class="mt50">
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>상호(법인명) *</th>
										<td colspan="3">
											<input type="text" name="cName" id="cName" value="<%=cName%>" style="width:220px">
										</td>
									</tr>
									<tr>
										<th>사업자등록번호 *</th>
										<td colspan="3">
											<input type="text" name="cNum1" id="cNum1" style="width:50px" maxlength="3" value="<%=cNum1%>"> - <input type="text" name="cNum2" id="cNum2" style="width:50px" maxlength="2" value="<%=cNum2%>"> - <input type="text" name="cNum3" id="cNum3" style="width:50px" maxlength="5" value="<%=cNum3%>">
										</td>
									</tr>
									<tr>
										<th>대표자</th>
										<td>
											<input type="text" name="ceo" id="ceo" style="width:220px" value="<%=ceo%>">
										</td>
										<th>전화번호</th>
										<td>
											<input type="text" name="cPhone1" id="cPhone1" style="width:50px" maxlength="4" value="<%=cPhone1%>"> - <input type="text" name="cPhone2" id="cPhone2" style="width:50px" maxlength="4" value="<%=cPhone2%>"> - <input type="text" name="cPhone3" id="cPhone3" style="width:50px" maxlength="4" value="<%=cPhone3%>">
										</td>
									</tr>
									<tr>
										<th>휴대폰번호</th>
										<td>
											<input type="text" name="cMobile1" id="cMobile1" style="width:50px" maxlength="4" value="<%=cMobile1%>"> - <input type="text" name="cMobile2" id="cMobile2" style="width:50px" maxlength="4" value="<%=cMobile2%>"> - <input type="text" name="cMobile3" id="cMobile3" style="width:50px" maxlength="4" value="<%=cMobile3%>">
										</td>
										<th>이메일주소</th>
										<td>
											<input type="text" name="cEmail1" id="cEmail1" style="width:150px" value="<%=cEmail1%>"> @ <input type="text" name="cEmail2" id="cEmail2" style="width:150px" readonly value="<%=cEmail2%>">
											<select name="cEmail3" id="cEmail3" onchange="ChangeEmail2();">
												<option value="">메일주소 선택</option>
												<option value="dreamwiz.co.kr" <% If cEmail2 = "dreamwiz.co.kr" Then %> selected<% End If %>>dreamwiz.co.kr</option>
												<option value="empal.com" <% If cEmail2 = "empal.com" Then %> selected<% End If %>>empal.com</option>
												<option value="gmail.com" <% If cEmail2 = "gmail.com" Then %> selected<% End If %>>gmail.com</option>
												<option value="lycos.co.kr" <% If cEmail2 = "lycos.co.kr" Then %> selected<% End If %>>lycos.co.kr</option>
												<option value="naver.com" <% If cEmail2 = "naver.com" Then %> selected<% End If %>>naver.com</option>
												<option value="nate.com" <% If cEmail2 = "nate.com" Then %> selected<% End If %>>nate.com</option>
												<option value="netsgo.com" <% If cEmail2 = "netsgo.com" Then %> selected<% End If %>>netsgo.com</option>
												<option value="hanmail.net" <% If cEmail2 = "hanmail.net" Then %> selected<% End If %>>hanmail.net</option>
												<option value="hotmail.com" <% If cEmail2 = "hotmail.com" Then %> selected<% End If %>>hotmail.com</option>
												<option value="paran.com" <% If cEmail2 = "paran.com" Then %> selected<% End If %>>paran.com</option>
												<option value="0">직접입력</option>
											</select><i></i>
										</td>
									</tr>
									<tr>
										<th>사업장주소(도로명주소) *</th>
										<td colspan="3">
											<p class="pd3-0">
												<input type="text" name="cZip" id="cZip" value="<%=cZip%>" class="AXInput W100" readonly="readonly">
												<input type="button" value="우편번호검색" class="AXButtonSmall Classic" onclick="openDaumPostcode();">
											</p>
											<p class="pd3-0"><input type="text" name="cAddr1" id="cAddr1" class="AXInput" readonly="readonly" style="width:30%" value="<%=cAddr1%>"></p>
											<p class="pd3-0"><input type="text" name="cAddr2" id="cAddr2" class="AXInput" style="width:30%" value="<%=cAddr2%>"></p>
										</td>
									</tr>
									<tr>
										<th>업태</th>
										<td>
											<input type="text" name="cType" id="cType" style="width:220px" value="<%=comType%>">
										</td>
										<th>종목</th>
										<td>
											<input type="text" name="cCate" id="cCate" style="width:220px" value="<%=cCate%>">
										</td>
									</tr>
									<tr>
										<th>사업자등록증 사본</th>
										<td>
											<input type="file" name="strFile1" class="AXInput w50p">
											<% If strFile1 <> "" Then %>
												<a href="/download.asp?fn=<%=escape(strFile1)%>&ph=member"><%=strFile1%></a>&nbsp;&nbsp;<input type="checkbox" name="ori_file1_del1" value="Y"> 삭제<br>
											<% End If %>첨부파일 : 최대 용량 10M 이하
										</td>
										<th>회사CI</th>
										<td>
											<input type="file" name="strFile2" class="AXInput w50p">
											<% If strFile2 <> "" Then %>
												<a href="/download.asp?fn=<%=escape(strFile2)%>&ph=member"><%=strFile2%></a>&nbsp;&nbsp;<input type="checkbox" name="ori_file1_del2" value="Y"> 삭제<br>
											<% End If %>첨부파일 : 최대 용량 10M 이하
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<% If intGubun = 1 Then %>
					<footer style="float:right;">
					<% If menucode = "s12" Then %>
						<a href="./list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
					<% Else %>
						<a href="./leave_list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
					<% End If %>
					<% If session("aduserlevel") = 9 Then %>
						<a href="javascript:update_chk();" class="btn btn-primary"><i class="fa fa-pencil"></i> 수정하기</a>
						<a href="./delete.asp?intSeq=<%=intSeq%>'" class="btn btn-default"><i class="fa fa-trash"></i> 삭제</a>
					<% End If %>
					</footer>

					</form>
					<% End If %>
				</div>

				<% If intGubun = 2 Then %>
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>담당자정보</h2>
					</header>

					<div class="mt50">
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<h5>영업 담당자 정보</h5>
									<tr>
										<th>이름</th>
										<td>
											<input type="text" name="manager1" id="manager1" style="width:220px" value="<%=manager1%>">
										</td>
										<th>이메일</th>
										<td>
											<input type="text" name="m1_email1" id="m1_email1" style="width:120px" value="<%=m1_email1%>"> @ <input type="text" name="m1_email2" id="m1_email2" style="width:120px" value="<%=m1_email2%>">
										</td>
									</tr>
									<tr>
										<th>일반전화</th>
										<td>
											<input type="text" name="m1_Phone1" id="m1_Phone1" style="width:50px" maxlength="4" value="<%=m1_Phone1%>"> - <input type="text" name="m1_Phone2" id="m1_Phone2" style="width:50px" maxlength="4" value="<%=m1_Phone2%>"> - <input type="text" name="m1_Phone3" id="m1_Phone3" style="width:50px" maxlength="4" value="<%=m1_Phone3%>">
										</td>
										<th>휴대폰 번호</th>
										<td>
											<input type="text" name="m1_Mobile1" id="m1_Mobile1" style="width:50px" maxlength="4" value="<%=m1_Mobile1%>"> - <input type="text" name="m1_Mobile2" id="m1_Mobile2" style="width:50px" maxlength="4" value="<%=m1_Mobile2%>"> - <input type="text" name="m1_Mobile3" id="m1_Mobile3" style="width:50px" maxlength="4" value="<%=m1_Mobile3%>">
										</td>
									</tr>
								</tbody>
							</table>

							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<h5>기술 전담 담당자 정보</h5>
									<tr>
										<th>이름</th>
										<td>
											<input type="text" name="manager2" id="manager2" style="width:220px" value="<%=manager2%>">
										</td>
										<th>이메일</th>
										<td>
											<input type="text" name="m2_email1" id="m2_email1" style="width:120px" value="<%=m2_email1%>"> @ <input type="text" name="m2_email2" id="m2_email2" style="width:120px" value="<%=m2_email2%>">
										</td>
									</tr>
									<tr>
										<th>일반전화</th>
										<td>
											<input type="text" name="m2_Phone1" id="m2_Phone1" style="width:50px" maxlength="4" value="<%=m2_Phone1%>"> - <input type="text" name="m2_Phone2" id="m2_Phone2" style="width:50px" maxlength="4" value="<%=m2_Phone2%>"> - <input type="text" name="m2_Phone3" id="m2_Phone3" style="width:50px" maxlength="4" value="<%=m2_Phone3%>">
										</td>
										<th>휴대폰 번호</th>
										<td>
											<input type="text" name="m2_Mobile1" id="m2_Mobile1" style="width:50px" maxlength="4" value="<%=m2_Mobile1%>"> - <input type="text" name="m2_Mobile2" id="m2_Mobile2" style="width:50px" maxlength="4" value="<%=m2_Mobile2%>"> - <input type="text" name="m2_Mobile3" id="m2_Mobile3" style="width:50px" maxlength="4" value="<%=m2_Mobile3%>">
										</td>
									</tr>
								</tbody>
							</table>

							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<h5>세금계산서 담당자 정보</h5>
									<tr>
										<th>이름</th>
										<td>
											<input type="text" name="manager3" id="manager3" style="width:220px" value="<%=manager3%>">
										</td>
										<th>이메일</th>
										<td>
											<input type="text" name="m3_email1" id="m3_email1" style="width:120px" value="<%=m3_email1%>"> @ <input type="text" name="m3_email2" id="m3_email2" style="width:120px" value="<%=m3_email2%>">
										</td>
									</tr>
									<tr>
										<th>일반전화</th>
										<td>
											<input type="text" name="m3_Phone1" id="m3_Phone1" style="width:50px" maxlength="4" value="<%=m3_Phone1%>"> - <input type="text" name="m3_Phone2" id="m3_Phone2" style="width:50px" maxlength="4" value="<%=m3_Phone2%>"> - <input type="text" name="m3_Phone3" id="m3_Phone3" style="width:50px" maxlength="4" value="<%=m3_Phone3%>">
										</td>
										<th>휴대폰 번호</th>
										<td>
											<input type="text" name="m3_Mobile1" id="m3_Mobile1" style="width:50px" maxlength="4" value="<%=m3_Mobile1%>"> - <input type="text" name="m3_Mobile2" id="m3_Mobile2" style="width:50px" maxlength="4" value="<%=m3_Mobile2%>"> - <input type="text" name="m3_Mobile3" id="m3_Mobile3" style="width:50px" maxlength="4" value="<%=m3_Mobile3%>">
										</td>
									</tr>
								</tbody>
							</table>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

					<footer style="float:right;">
					<% If menucode = "s12" Then %>
						<a href="./list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
					<% Else %>
						<a href="./leave_list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
					<% End If %>
					<% If session("aduserlevel") = 9 Then %>
						<a href="javascript:update_chk();" class="btn btn-primary"><i class="fa fa-pencil"></i> 수정하기</a>
						<a href="./delete.asp?intSeq=<%=intSeq%>'" class="btn btn-default"><i class="fa fa-trash"></i> 삭제</a>
					<% End If %>
					</footer>

					</form>
					<% End If %>
				</div>
				<!-- end widget -->

			</article>

			<article id="tab02" class="tab_contents">
				<div class="top_btns"><button type="button" class="btn btn-primary btn_register">등록하기</button></div>
				<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
					<colgroup>
						<col style="width:5%">
						<col style="width:5%">
						<col style="width:10%">
						<col style="width:10%">
						<col style="width:10%">
						<col style="width:15%">
						<col style="width:10%">
						<col style="width:10%">
						<col style="width:10%">
					</colgroup>
					<thead>
						<tr>
							<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
							<th data-class="expand">번호</th>
							<th data-class="expand">이름</th>
							<th data-class="expand">아이디</th>
							<th data-class="expand">부서명</th>
							<th data-class="expand">이메일</th>
							<th data-class="expand">휴대폰 번호</th>
							<th data-class="expand">상태</th>
							<th data-class="expand">등록일</th>
						</tr>
					</thead>
					<tbody>

						<tr style="border-bottom:#dddddd 1px solid;">
							<td><input type="checkbox" value="<%=intseq%>"></td>
							<td>6</td>
							<td class="info_modify">홍길동</td>
							<td class="info_modify">sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>사용</td>
							<td>YYYY-MM-DD</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td><input type="checkbox" value="<%=intseq%>"></td>
							<td>5</td>
							<td class="info_modify">홍길동</td>
							<td class="info_modify">sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>사용</td>
							<td>YYYY-MM-DD</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td><input type="checkbox" value="<%=intseq%>"></td>
							<td>4</td>
							<td class="info_modify">홍길동</td>
							<td class="info_modify">sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>사용</td>
							<td>YYYY-MM-DD</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td><input type="checkbox" value="<%=intseq%>"></td>
							<td>3</td>
							<td class="info_modify">홍길동</td>
							<td class="info_modify">sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>사용</td>
							<td>YYYY-MM-DD</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td><input type="checkbox" value="<%=intseq%>"></td>
							<td>2</td>
							<td class="info_modify">홍길동</td>
							<td class="info_modify">sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>사용</td>
							<td>YYYY-MM-DD</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td><input type="checkbox" value="<%=intseq%>"></td>
							<td>1</td>
							<td class="info_modify">홍길동</td>
							<td class="info_modify">sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>사용</td>
							<td>YYYY-MM-DD</td>
						</tr>

					</tbody>
				</table>
				<div class="bot_btns">
					<button type="button" class="btn btn-primary">사용</button>
					<button type="button" class="btn btn-default">미사용</button>
					<button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</button>
				</div>


				<!--  하위 회원 수정  -->
				<div class="popup pop_info_modify">
					<div class="popup_wrap">
						<div class="pop_tit">
							<strong>하위 회원 수정</strong>
							<button type="button" class="btn_pop_close"><i class="fa fa-times" aria-hidden="true"></i></button>
						</div>
						<div class="pop_con">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:20%" />
									<col style="" />
								</colgroup>
								<tbody>
									<tr>
										<th>이름 *</th>
										<td><input type="text" name="strName" id="strName" /></td>
									</tr>
									<tr>
										<th>부서 *</th>
										<td><input type="text" name="strName" id="strName" /></td>
									</tr>
									<tr>
										<th>아이디 *</th>
										<td>sample</td>
									</tr>
									<tr>
										<th>비밀번호 *</th>
										<td><input type="password" name="strPw" id="strPw" /></td>
									</tr>
									<tr>
										<th>전화번호 *</th>
										<td>
											<input type="text" name="strPhone1" id="strPhone1" style="width:50px" maxlength="4" value=""> - <input type="text" name="strPhone2" id="strPhone2" style="width:50px" maxlength="4" value=""> - <input type="text" name="strPhone3" id="strPhone3" style="width:50px" maxlength="4" value="">
										</td>
									</tr>
									<tr>
										<th>휴대폰번호 *</th>
										<td><input type="text" name="strMobile1" id="strMobile1" style="width:50px" maxlength="4" value=""> - <input type="text" name="strMobile2" id="strMobile2" style="width:50px" maxlength="4" value=""> - <input type="text" name="strMobile3" id="strMobile3" style="width:50px" maxlength="4" value="">
									</tr>
									<tr>
										<th>이메일주소</th>
										<td>
											<input type="text" name="strEmail1" id="strEmail1" style="width:150px" value=""> @ <input type="text" name="strEmail2" id="strEmail2" style="width:150px" readonly="" value="">
											<select name="strEmail3" id="strEmail3" onchange="ChangeEmail1();">
												<option value="">메일주소 선택</option>
												<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												<option value="empal.com" selected="">empal.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="lycos.co.kr">lycos.co.kr</option>
												<option value="naver.com">naver.com</option>
												<option value="nate.com">nate.com</option>
												<option value="netsgo.com">netsgo.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="paran.com">paran.com</option>
												<option value="0">직접입력</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="pop_btns">
							<button type="button" class="btn btn-default">취소</button>
							<button type="button" class="btn btn-primary">확인</button>
						</div>
					</div>
				</div>


				<!-- 하위 회원 등록 -->
				<div class="popup pop_register">
					<div class="popup_wrap">
						<div class="pop_tit">
							<strong>하위 회원 등록</strong>
							<button type="button" class="btn_pop_close"><i class="fa fa-times" aria-hidden="true"></i></button>
						</div>
						<div class="pop_con">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:22%" />
									<col style="" />
								</colgroup>
								<tbody>
									<tr>
										<th>이름 *</th>
										<td><input type="text" name="strName" id="strName" /></td>
									</tr>
									<tr>
										<th>부서 *</th>
										<td><input type="text" name="strName" id="strName" /></td>
									</tr>
									<tr>
										<th>아이디 *</th>
										<td><input type="text" name="strName" id="strName" /> <button type="button" class="btn btn-primary">중복체크</button></td>
									</tr>
									<tr>
										<th>비밀번호 *</th>
										<td><input type="password" name="strPw" id="strPw" /></td>
									</tr>
									<tr>
										<th>비밀번호 확인 *</th>
										<td><input type="password" name="strPw" id="strPw" /></td>
									</tr>
									<tr>
										<th>전화번호</th>
										<td>
											<input type="text" name="strPhone1" id="strPhone1" style="width:50px" maxlength="4" value=""> - <input type="text" name="strPhone2" id="strPhone2" style="width:50px" maxlength="4" value=""> - <input type="text" name="strPhone3" id="strPhone3" style="width:50px" maxlength="4" value="">
										</td>
									</tr>
									<tr>
										<th>휴대폰번호 *</th>
										<td><input type="text" name="strMobile1" id="strMobile1" style="width:50px" maxlength="4" value=""> - <input type="text" name="strMobile2" id="strMobile2" style="width:50px" maxlength="4" value=""> - <input type="text" name="strMobile3" id="strMobile3" style="width:50px" maxlength="4" value="">
									</tr>
									<tr>
										<th>이메일주소</th>
										<td>
											<input type="text" name="strEmail1" id="strEmail1" style="width:150px" value=""> @ <input type="text" name="strEmail2" id="strEmail2" style="width:150px" readonly="" value="">
											<select name="strEmail3" id="strEmail3" onchange="ChangeEmail1();">
												<option value="">메일주소 선택</option>
												<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												<option value="empal.com" selected="">empal.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="lycos.co.kr">lycos.co.kr</option>
												<option value="naver.com">naver.com</option>
												<option value="nate.com">nate.com</option>
												<option value="netsgo.com">netsgo.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="paran.com">paran.com</option>
												<option value="0">직접입력</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="pop_btns">
							<button type="button" class="btn btn-default">취소</button>
							<button type="button" class="btn btn-primary">확인</button>
						</div>
					</div>
				</div>
			</article>		

			<article id="tab03" class="tab_contents">
				<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
					<colgroup>
						<col style="width:5%">
						<col style="width:10%">
						<col style="width:15%">
						<col style="width:10%">
						<col style="width:15%">
						<col style="width:15%">
						<col style="width:10%">
						<col style="width:15%">
					</colgroup>
					<thead>
						<tr>
							<th data-class="expand">번호</th>
							<th data-class="expand">이름</th>
							<th data-class="expand">아이디</th>
							<th data-class="expand">부서명</th>
							<th data-class="expand">이메일</th>
							<th data-class="expand">휴대폰 번호</th>
							<th data-class="expand">처리상태</th>
							<th data-class="expand">일자</th>
						</tr>
					</thead>
					<tbody>

						<tr style="border-bottom:#dddddd 1px solid;">
							<td>6</td>
							<td>홍길동</td>
							<td>sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>생성</td>
							<td>YYYY-MM-DD 00:00</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td>5</td>
							<td>홍길동</td>
							<td>sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>생성</td>
							<td>YYYY-MM-DD 00:00</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td>4</td>
							<td>홍길동</td>
							<td>sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>생성</td>
							<td>YYYY-MM-DD 00:00</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td>3</td>
							<td>홍길동</td>
							<td>sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>생성</td>
							<td>YYYY-MM-DD 00:00</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td>2</td>
							<td>홍길동</td>
							<td>sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>생성</td>
							<td>YYYY-MM-DD 00:00</td>
						</tr>
						<tr style="border-bottom:#dddddd 1px solid;">
							<td>1</td>
							<td>홍길동</td>
							<td>sample</td>
							<td>부서A</td>
							<td>sample@mail.co.kr</td>
							<td>010-1234-5678</td>
							<td>생성</td>
							<td>YYYY-MM-DD 00:00</td>
						</tr>

					</tbody>
				</table>
				<%call Paging_list("")%>
			</article>
			<!-- WIDGET END -->

		<script>
			$(document).ready(function() {
				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

				$('.info_modify').click(function(){
					$('.pop_info_modify').fadeIn();
				});

				$('.btn_register').click(function(){
					$('.pop_register').fadeIn();
				});

				$('.popup .btn_pop_close, .popup .pop_btns .btn-default').click(function(){
					$('.popup').fadeOut();
				});
			});


		</script>

		<!-- #include file="../inc/footer.asp" -->