<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp" -->

<%
menucode	= SQL_Injection(Trim(Request.QueryString("cd")))
sdate		= SQL_Injection(Trim(Request.QueryString("sdate")))
edate		= SQL_Injection(Trim(Request.QueryString("edate")))
intGubun	= SQL_Injection(Trim(Request.QueryString("intGubun")))
mailYN		= SQL_Injection(Trim(Request.QueryString("mailYN")))
smsYN		= SQL_Injection(Trim(Request.QueryString("smsYN")))
yeosinYN	= SQL_Injection(Trim(Request.QueryString("yeosinYN")))
fieldname	= SQL_Injection(Trim(Request.QueryString("fieldname")))
fieldvalue	= SQL_Injection(Trim(Request.QueryString("fieldvalue")))
page		= SQL_Injection(Trim(Request.QueryString("page")))

whereQuery = " "
If sdate <> "" And edate <> "" Then
	query_where = query_where &  " AND dtmInsertDate BETWEEN '" & sdate & "' AND DATEADD(day,1,'"&edate&"')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where &  " AND dtmInsertDate >= '" & sdate  &"'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where &  " AND dtmInsertDate <= DATEADD(day,1,'"&edate&"')"
End If

If intGubun <> "" Then
	whereQuery = whereQuery & " And intGubun = " & intGubun
End If

If mailYN <> "" Then
	whereQuery = whereQuery & " And mailYN = '" & mailYN & "'"
End If

If smsYN <> "" Then
	smsYN = whereQuery & " And smsYN = '" & smsYN & "'"
End If

If yeosinYN <> "" Then
	whereQuery = whereQuery & " And yeosinYN = '" & yeosinYN & "'"
End If

If fieldvalue <> "" Then
	fieldvalue = whereQuery & " And " & fieldname & " LIKE '%" & fieldvalue & "%'"
End If

Call DbOpen()

If menucode = "s12" Then
	sql = "SELECT * FROM " & TableName & " WHERE intAction = 0 AND intGubun IN ('1', '2') " & whereQuery & " ORDER BY intseq DESC"
ElseIf menucode = "s14" Then
	sql = "SELECT * FROM " & TableName & " WHERE intAction <> 0 AND intGubun IN ('1', '2') " & whereQuery & " ORDER BY intseq DESC"
End If

Set rs = dbconn.execute(sql)

fn = "회원다운로드"

Response.Buffer=True
Response.ContentType = "application/vnd.ms-excel;charset=utf-8"
Response.CacheControl = "public"
Response.AddHeader "Content-Disposition", "attachment;filename="& Server.URLPathEncode(fn) &".xls"
%>

	<table width="100%" cellpadding="2" cellspacing="1" border="1" bgcolor="#ffffff">
		<tr>
			<th>회원구분</th>
			<th>아이디</th>
			<th>성명</th>
			<th>전화번호</th>
			<th>휴대폰번호</th>
			<th>SMS수신</th>
			<th>이메일</th>
			<th>이메일수신</th>
			<th>여신결제</th>
			<th>상호명</th>
			<th>사업자등록번호</th>
			<th>대표자명</th>
			<th>회사전화번호</th>
			<th>휴대폰번호</th>
			<th>이메일</th>
			<th>우편번호</th>
			<th>사업장주소1</th>
			<th>사업장주소2</th>
			<th>업태</th>
			<th>종목</th>
		</tr>
<%
If Not rs.eof Then
	intGubun = rs("intGubun")

	If intGubun = 1 Then
		intGubun = "일반"
	ElseIf intGubun = 2 Then
		intGubun = "마스터"
	End If

	Do while Not rs.eof
%>
		<tr>
			<td align="center"><%=intGubun%></td>
			<td align="center"><%=rs("strId")%></td>
			<td align="center"><%=rs("strName")%></td>
			<td align="center"><%=rs("strPhone")%></td>
			<td align="center"><%=rs("strMobile")%></td>
			<td align="center"><%=rs("smsYN")%></td>
			<td align="center"><%=rs("strEmail")%></td>
			<td align="center"><%=rs("mailYN")%></td>
			<td align="center"><%=rs("yeosinYN")%></td>
			<td align="center"><%=rs("cName")%></td>
			<td align="center"><%=rs("cNum")%></td>
			<td align="center"><%=rs("ceo")%></td>
			<td align="center"><%=rs("cPhone")%></td>
			<td align="center"><%=rs("cMobile")%></td>
			<td align="center"><%=rs("cEmail")%></td>
			<td style="mso-number-format:'\@'" align="center"><%=rs("czip")%></td>
			<td width="250px" align="center"><%=rs("cAddr1")%></td>
			<td style="mso-number-format:'\@'" align="center"><%=rs("cAddr2")%></td>
			<td align="center"><%=rs("cType")%></td>
			<td align="center"><%=rs("cCate")%></td>
		</tr>
<%
		rs.MoveNext
	Loop

	rs.close()
	Set rs = Nothing
End If

Call DbClose()
%>
	<table>