
		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config.asp" -->

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">

					<header>
						<span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
						<h2>엑셀등록</h2>
					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<form action="./mem_excel_exec.asp" class="smart-form" method="post" onSubmit="return regist(this);" enctype="multipart/form-data" name="excelup" id="excelup">
						<input type="hidden" name="menucode" value="<%=menucode%>">
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>

								<tbody>
									<tr>
										<th>업로드종류</th>
										<td>
											<label class="select col-1">
											<select name="mem_mode" id="mem_mode">
												<option value="">전체</option>
												<option value="master">SRM 담당자</option>
												<option value="normal">일반회원</option>
											</select><i></i>
											</label>
										</td>
									</tr>
									<tr>
										<th>첨부파일</th>
										<td><input type="file" name="strFile1" class="AXInput w250p">
										</td>

									</tr>
									<tr>

									</tr>
								</tbody>

							</table>

							<footer>
								<input type="submit" class="btn btn-primary" value="업로드">
								<a href="./list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
							</footer>


						</div>
						</form>


						<script language="javascript">
						<!--
							function regist(frm) {
								if ( frm.mem_mode.value == "" ) {
									alert("업로드종류를 선택해주세요");
									frm.mem_mode.focus();
									return false;
								} else {
									frm.submit();
								}
							}
						//-->
						</script>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
			</article>
			<!-- WIDGET END -->

		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});
		</script>

		<!-- #include file="../inc/footer.asp" -->