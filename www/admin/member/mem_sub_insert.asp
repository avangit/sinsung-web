<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp" -->

<%
intSeq			= SQL_Injection(Trim(Request.Form("intSeq")))
strName			= SQL_Injection(Trim(Request.Form("strName_sub")))
department		= SQL_Injection(Trim(Request.Form("department")))
strId			= SQL_Injection(Trim(Request.Form("strId_sub")))
strPwd			= SQL_Injection(Trim(Request.Form("strPwd_sub")))
strPhone1		= SQL_Injection(Trim(Request.Form("strPhone1_sub")))
strPhone2		= SQL_Injection(Trim(Request.Form("strPhone2_sub")))
strPhone3		= SQL_Injection(Trim(Request.Form("strPhone3_sub")))
strMobile1		= SQL_Injection(Trim(Request.Form("strMobile1_sub")))
strMobile2		= SQL_Injection(Trim(Request.Form("strMobile2_sub")))
strMobile3		= SQL_Injection(Trim(Request.Form("strMobile3_sub")))
strEmail1		= SQL_Injection(Trim(Request.Form("strEmail1_sub")))
strEmail2		= SQL_Injection(Trim(Request.Form("strEmail2_sub")))

'// 변수 가공
strPhone = strPhone1 & "-" & strPhone2 & "-" & strPhone3
strMobile = strMobile1 & "-" & strMobile2 & "-" & strMobile3
strEmail = strEmail1 & "@" & strEmail2
intGubun = 3
smsYN = "N"
mailYN = "N"
yeosinYN = "N"

Call DbOpen()

sql = "SELECT strId, cName, cNum, cZip, cAddr1, cAddr2 FROM " & tablename & " WHERE intseq = '" & intSeq & "'"

Set rs = dbconn.execute(sql)

If Not(rs.eof) Then
	strAgreeId = rs("strId")
	cName = rs("cName")
	cNum = rs("cNum")
	cZip = rs("cZip")
	cAddr1 = rs("cAddr1")
	cAddr2 = rs("cAddr2")
End If

rs.close
Set rs = Nothing

Sql = "INSERT INTO mTb_Member2("
Sql = Sql & "intGubun, "
Sql = Sql & "strId, "
Sql = Sql & "strPwd, "
Sql = Sql & "strName, "
Sql = Sql & "strPhone, "
Sql = Sql & "strMobile, "
Sql = Sql & "smsYN, "
Sql = Sql & "strEmail, "
Sql = Sql & "mailYN, "
Sql = Sql & "yeosinYN, "
Sql = Sql & "cName, "
Sql = Sql & "cNum, "
Sql = Sql & "cZip, "
Sql = Sql & "cAddr1, "
Sql = Sql & "cAddr2, "
Sql = Sql & "department, "
Sql = Sql & "strAgreeId) VALUES("
Sql = Sql & "'" & intGubun & "',"
Sql = Sql & "N'" & strId & "',"
Sql = Sql & "'" & Encrypt_Sha(strPwd) & "',"
Sql = Sql & "N'" & strName & "',"
Sql = Sql & "'" & strPhone & "',"
Sql = Sql & "'" & strMobile & "',"
Sql = Sql & "'" & smsYN & "',"
Sql = Sql & "N'" & strEmail & "',"
Sql = Sql & "'" & mailYN & "',"
Sql = Sql & "'" & yeosinYN & "',"
Sql = Sql & "N'" & cName & "',"
Sql = Sql & "'" & cNum & "',"
Sql = Sql & "'" & cZip & "',"
Sql = Sql & "N'" & cAddr1 & "',"
Sql = Sql & "N'" & cAddr2 & "',"
Sql = Sql & "N'" & department & "',"
Sql = Sql & "N'" & strAgreeId & "')"

dbconn.execute(Sql)
dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&session("aduserid")&"','"&strId&"','생성','하위회원등록','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

Call DbClose()

Call jsAlertMsgUrl("회원 등록이 완료되었습니다.", "./modify.asp?menucode="&menucode&"&intSeq="&intSeq&"&go=2")
%>