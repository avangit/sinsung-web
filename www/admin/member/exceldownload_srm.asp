<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp" -->

<%
Call DbOpen()

'// 활동중인 전체 마스터회원
sql = "SELECT * FROM " & TableName & " WHERE intAction = 0 AND intGubun = 2 ORDER BY intseq DESC"

Set rs = dbconn.execute(sql)

fn = "SRM담당자다운로드"

Response.Buffer=True
Response.ContentType = "application/vnd.ms-excel;charset=utf-8"
Response.CacheControl = "public"
Response.AddHeader "Content-Disposition", "attachment;filename="& Server.URLPathEncode(fn) &".xls"
%>

	<table width="100%" cellpadding="2" cellspacing="1" border="1" bgcolor="#ffffff">
		<tr>
			<th>회원번호</th>
			<th>아이디</th>
			<th>성명</th>
			<th>영업담당자</th>
			<th>이메일</th>
			<th>일반전화</th>
			<th>휴대폰번호</th>
			<th>기술담당자</th>
			<th>이메일</th>
			<th>일반전화</th>
			<th>휴대폰번호</th>
			<th>세금계산서 담당자</th>
			<th>이메일</th>
			<th>일반전화</th>
			<th>휴대폰번호</th>
		</tr>
<%
If Not rs.eof Then
	Do while Not rs.eof
%>
		<tr>
			<td align="center"><%=rs("intSeq")%></td>
			<td align="center"><%=rs("strId")%></td>
			<td align="center"><%=rs("strName")%></td>
			<td><%=rs("manager1")%></td>
			<td><%=rs("m1_Email")%></td>
			<td style="mso-number-format:'\@'"><%=rs("m1_Phone")%></td>
			<td style="mso-number-format:'\@'"><%=rs("m1_Mobile")%></td>
			<td><%=rs("manager2")%></td>
			<td><%=rs("m2_Email")%></td>
			<td style="mso-number-format:'\@'"><%=rs("m2_Phone")%></td>
			<td style="mso-number-format:'\@'"><%=rs("m2_Mobile")%></td>
			<td><%=rs("manager3")%></td>
			<td><%=rs("m3_Email")%></td>
			<td style="mso-number-format:'\@'"><%=rs("m3_Phone")%></td>
			<td style="mso-number-format:'\@'"><%=rs("m3_Mobile")%></td>

		</tr>
<%
		rs.MoveNext
	Loop

	rs.close()
	Set rs = Nothing
End If

Call DbClose()
%>
	<table>