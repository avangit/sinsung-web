<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->
<!-- #include file="config.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<style>
.hasDatepicker {
	position: relative;
	z-index: 9999;
}
</style>
<article>

  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>회원리스트</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(RequestS("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(RequestS("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request.QueryString("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= "*"
Dim query_Tablename		: query_Tablename	= TableName
Dim query_where			: query_where		= " intSeq > 0 AND intAction = 0 AND intGubun IN ('1', '2') "
Dim query_orderby		: query_orderby		= " ORDER BY intseq DESC"

sdate		= SQL_Injection(Trim(RequestS("sdate")))
edate 		= SQL_Injection(Trim(RequestS("edate")))
intGubun 	= SQL_Injection(Trim(RequestS("intGubun")))
mailYN 		= SQL_Injection(Trim(RequestS("mailYN")))
smsYN 		= SQL_Injection(Trim(RequestS("smsYN")))
yeosinYN 	= SQL_Injection(Trim(RequestS("yeosinYN")))

param = "sdate=" & sdate & "&edate=" & edate & "&intGubun=" & intGubun & "&mailYN=" & mailYN & "&smsYN=" & smsYN & "&yeosinYN=" & yeosinYN & "&fieldname=" & fieldname & "&fieldvalue=" & fieldvalue & ""

If sdate <> "" And edate <> "" Then
	query_where = query_where &  " AND dtmInsertDate BETWEEN '" & sdate & "' AND DATEADD(day,1,'"&edate&"')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where &  " AND dtmInsertDate >= '" & sdate  &"'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where &  " AND dtmInsertDate <= DATEADD(day,1,'"&edate&"')"
End If

If intGubun <> "" Then
	query_where = query_where &  " AND intGubun = '" & intGubun &"'"
End If

If yeosinYN <> "" Then
	query_where = query_where &  " AND yeosinYN = '" & yeosinYN &"'"
End If

If mailYN <> "" Then
	query_where = query_where &  " AND mailYN = '" & mailYN &"'"
End If

If smsYN <> "" Then
	query_where = query_where &  " AND smsYN = '" & smsYN &"'"
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

'response.Write getQuery

sql = getQuery
Call dbopen
Set rs = dbconn.execute(sql)

dbconn.execute("INSERT INTO Record_views (menu,manage_id,manage_name,ip) VALUES('회원리스트','"&session("aduserid")&"','"&session("adusername")&"','"&Request.ServerVariables("REMOTE_ADDR")&"') ")
%>
		<form name = "frm" id="frm" action = "" class="smart-form m10"  method="get" >
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table class="table table-striped table-bordered line-h" width="100%">
			<tr>
				<th>가입일</th>
				<td colspan="3">
					<input type="text" name="sdate" id="sdate" value="<%=sdate%>" autocomplete="off"> ~ <input type="text" name="edate" id="edate" value="<%=edate%>" autocomplete="off">
				</td>
			</tr>
			<tr>
				<th>회원구분</th>
				<td colspan="3">
					<label class="select col-1">
					<select name="intGubun" id="intGubun">
						<option value="">전체</option>
						<option value="1" <% If intGubun = "1" Then %> selected<% End If %>>일반</option>
						<option value="2" <% If intGubun = "2" Then %> selected<% End If %>>마스터</option>
					</select><i></i>
					</label>
				</td>
			</tr>
			<tr>
				<th>수신여부</th>
				<td>
					<label class="select col-3">
					<select name="mailYN" id="mailYN">
						<option value="">메일</option>
						<option value="Y" <% If mailYN = "Y" Then %> selected<% End If %>>수신</option>
						<option value="N" <% If mailYN = "N" Then %> selected<% End If %>>미수신</option>
					</select><i></i>
					</label>

					<label class="select col-3">
					<select name="smsYN" id="smsYN">
						<option value="">SMS</option>
						<option value="Y" <% If smsYN = "Y" Then %> selected<% End If %>>수신</option>
						<option value="N" <% If smsYN = "N" Then %> selected<% End If %>>미수신</option>
					</select><i></i>
					</label>
				</td>
				<th>여신결재 사용여부</th>
				<td>
					<label class="select col-3">
					<select name="yeosinYN" id="yeosinYN">
						<option value="">전체</option>
						<option value="Y" <% If yeosinYN = "Y" Then %> selected<% End If %>>사용</option>
						<option value="N" <% If yeosinYN = "N" Then %> selected<% End If %>>미사용</option>
					</select><i></i>
					</label>
				</td>
			</tr>
			<tr>
				<th>검색</th>
				<td colspan="3">
					<label class="select w10 pro_detail">
					<select class="input-sm h34" name="fieldname" id="fieldname">
						<option value="strId" <% If fieldname = "strId" Then %> selected<% End If %>>아이디</option>
						<option value="strName" <% If fieldname = "strName" Then %> selected<% End If %>>이름</option>
					</select>
					</label>
					<div class="icon-addon addon-md col-md-10 col-4">
						<input type="text" name="fieldvalue" id="fieldvalue" class="form-control col-10" value="<%=fieldvalue%>">
						<label for="fieldvalue" class="glyphicon glyphicon-search " rel="tooltip" title="fieldvalue"></label>
					</div>
					<a href="javascript:frm.submit();" class="btn btn-primary"><i class="fa fa-search"></i> 검색</a>
					<a href="./list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-refresh"></i> 검색 초기화</a>
				</td>
			</tr>
		</table>
		</form>

		
		<footer style="float:right;" class="m_button">
			<a href="./write.asp?menucode=<%=menucode%>" class="btn btn-primary"><i class="fa fa-pencil"></i> 작성하기</a>
		</footer>

		<form name="list" method="get" action="./mem_exec_get.asp" style="margin:0;">
		<span class="cont_txt"><%=intTotalCount%>건</span>
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="message">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<input type="hidden" name="page" value="<%=intNowPage%>">
		<input type="hidden" name="param" value="<%=param%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:5%">
			<col style="width:5%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:15%">
			<col style="width:10%">
			<col style="width:10%">
		</colgroup>
		<thead>
			<tr>
				<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
				<th data-class="expand">번호</th>
				<th data-hide="phone,tablet">구분</th>
				<th data-class="expand">아이디</th>
				<th data-class="expand">성명</th>
				<th data-class="expand">회사명</th>
				<th data-hide="phone,tablet">이메일</th>
				<th data-hide="phone,tablet">휴대폰번호</th>
				<th data-hide="phone,tablet">가입일</th>
			</tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="9" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	rs.move MoveCount
	Do while Not rs.eof

		intseq			= rs("intseq")
		strId 			= rs("strId")
		strName 		= rs("strName")
		strPwd 			= rs("strPwd")
		strMobile 		= rs("strMobile")
		dtmInsertDate	= rs("dtmInsertDate")
		intAction 		= rs("intAction")
		stremail 		= rs("stremail")
		cName 			= rs("cName")

		If rs("intGubun") = 1 Then
			intGubunTxt = "일반"
		ElseIf rs("intGubun") = 2 Then
			intGubunTxt = "마스터"
		End If
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><input type="checkbox" value="<%=intseq%>"></td>
				<td><%=intNowNum%></td>
				<td><%=intGubunTxt%></td>
				<td><a href="modify.asp?intSeq=<%=intseq%>&<%=getstring("menucode="&menucode)%>"><%=rs("strId")%></a></td>
				<td><a href="modify.asp?intSeq=<%=intseq%>&<%=getstring("menucode="&menucode)%>"><%=rs("strName")%></a></td>
				<td><%=cName%></td>
				<td><%=stremail%></td>
				<td><%=strMobile%></td>
				<td><%=Left(dtmInsertDate, 10)%></td>
			</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
		</tbody>
		</table>

		<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:10 0 0 0;">
			<tr>
				<td>
					<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
				</td>
				<td align="right">
					 <a href="./mem_excel_write.asp?menucode=<%=menucode%>" class="btn btn-primary"><i class="fa fa-upload"></i> 엑셀 업로드</a>
					 <a href="./exceldownload.asp?sdate=<%=sdate%>&edate=<%=edate%>&intGubun=<%=intGubun%>&mailYN=<%=mailYN%>&smsYN=<%=smsYN%>&yeosinYN=<%=yeosinYN%>&fieldname=<%=fieldname%>&fieldvalue=<%=fieldvalue%>&page=<%=intNowPage%>" class="btn btn-default"><i class="fa fa-download"></i> 엑셀 다운로드</a>
					 <a href="./exceldownload_srm.asp" class="btn btn-default"><i class="fa fa-download"></i> SRM 담당자 다운로드</a>
				</td>
			</tr>
		</table>
		</form>

		<%call Paging_list("")%>
	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->