<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp"-->
<%
intSeq		= Trim(Request.Form("n"))
ori_intSeq	= Trim(Request.Form("origin"))

Call DbOpen()

sql = "SELECT * FROM mTb_Member2 WHERE intSeq = " & intSeq & ""
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	strName		= rs("strName")
	department	= rs("department")
	strId		= rs("strId")
	strPhone	= rs("strPhone")
	strMobile	= rs("strMobile")
	strEmail	= rs("strEmail")
End If

If Trim(Replace(strPhone, "-", "")) <> "" Then
	ArrPhone = Split(strPhone, "-")
	If Ubound(ArrPhone) = 2 Then
		strPhone1 = ArrPhone(0)
		strPhone2 = ArrPhone(1)
		strPhone3 = ArrPhone(2)
	End If
End If

If Trim(Replace(strMobile, "-", "")) <> "" Then
	ArrMobile = Split(strMobile, "-")
	If Ubound(ArrMobile) = 2 Then
		strMobile1 = ArrMobile(0)
		strMobile2 = ArrMobile(1)
		strMobile3 = ArrMobile(2)
	End If
End If

If Trim(Replace(strEmail, "@", "")) <> "" Then
	ArrstrEmail = Split(strEmail, "@")
	If Ubound(ArrstrEmail) = 1 Then
		strEmail1 = ArrstrEmail(0)
		strEmail2 = ArrstrEmail(1)
	End If
End If
%>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
<script src="/admin/js/common.js"></script>
			<script type="text/javascript">
				$(document).ready(function() {
					$("#modal_show").click(function() {
						$("#layer01").modal("show");
					});

					$(".btn_pop_close").click(function() {


						$("#layer01").hide();
						$("#layer02").hide();
					});

					$('#btn_cancel').on('click', function(){
						$("#layer02").modal("hide");
					});
				});
			</script>
<!--
				<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/stove99/jquery-modal-sample@v1.4/css/jquery.modal.css" />
				<script src="//cdn.jsdelivr.net/gh/stove99/jquery-modal-sample@v1.4/js/jquery.modal.js"></script>
				<script src="//cdn.jsdelivr.net/gh/stove99/jquery-modal-sample@v1.4/js/modal.js"></script>
-->
				<div id="layer02">
					<div class="popup_wrap">
						<div class="pop_tit">
							<strong>하위 회원 수정</strong>
							<button type="button" class="btn_pop_close" id="btn_cancel" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
							<!--button type="button" class="close" data-dismiss="modal" aria-label="Close"></button-->
							<!--a href="#layer02" rel="modal:close" class="close-modal" id="btn_cancel" data-dismiss="modal"></a-->
						</div>
						<form name="subupdateform" method="post" action="./mem_sub_update.asp" onsubmit="subupdate_chk();return false;">
						<input type="hidden" name="num" value="">
						<input type="hidden" name="n" value="<%=intSeq%>">
						<input type="hidden" name="intSeq" value="<%=ori_intSeq%>">
						<input type="hidden" name="strId" value="<%=strId%>">
						<input type="hidden" name="menucode" value="<%=menucode%>">
						<div class="pop_con">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:20%" />
									<col style="" />
								</colgroup>
								<tbody>
									<tr>
										<th>이름 *</th>
										<td><input type="text" name="strName" id="strName" value="<%=strName%>"/></td>
									</tr>
									<tr>
										<th>부서 *</th>
										<td><input type="text" name="department" id="department" value="<%=department%>" /></td>
									</tr>
									<tr>
										<th>아이디 *</th>
										<td><%=strId%></td>
									</tr>
									<tr>
										<th>비밀번호 *</th>
										<td><input type="password" name="strPw" id="strPw" /></td>
									</tr>
									<tr>
										<th>전화번호 </th>
										<td>
											<input type="text" name="strPhone1" id="strPhone1" style="width:50px" maxlength="4" value="<%=strPhone1%>"> - <input type="text" name="strPhone2" id="strPhone2" style="width:50px" maxlength="4" value="<%=strPhone2%>"> - <input type="text" name="strPhone3" id="strPhone3" style="width:50px" maxlength="4" value="<%=strPhone3%>">
										</td>
									</tr>
									<tr>
										<th>휴대폰번호 *</th>
										<td><input type="text" name="strMobile1" id="strMobile1" style="width:50px" maxlength="4" value="<%=strMobile1%>"> - <input type="text" name="strMobile2" id="strMobile2" style="width:50px" maxlength="4" value="<%=strMobile2%>"> - <input type="text" name="strMobile3" id="strMobile3" style="width:50px" maxlength="4" value="<%=strMobile3%>">
									</tr>
									<tr>
										<th>이메일주소</th>
										<td>
											<input type="text" name="strEmail1" id="strEmail1" style="width:150px" value="<%=strEmail1%>"> @ <input type="text" name="strEmail2" id="strEmail2" style="width:150px" readonly="" value="<%=strEmail2%>">
											<select name="strEmail3" id="strEmail3" onchange="ChangeEmail4();">
												<option value="">메일주소 선택</option>
												<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												<option value="empal.com">empal.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="lycos.co.kr">lycos.co.kr</option>
												<option value="naver.com">naver.com</option>
												<option value="nate.com">nate.com</option>
												<option value="netsgo.com">netsgo.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="paran.com">paran.com</option>
												<option value="0">직접입력</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="pop_btns">
						<!--	<button type="button" class="btn btn-default" id="btn_cancel2">취소</button> -->
							<!--a href="#close-modal" rel="modal:close" class="close-modal ">취소</a-->
							<!--button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button-->
							<input type="submit" class="btn btn-primary" value="수정">
						</div>
						</form>
					</div>
				</div>
<%
rs.close()
Set rs = Nothing

Call DbClose()
%>