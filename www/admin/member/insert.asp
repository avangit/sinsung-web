<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp" -->
<%
'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/member/")
UploadForm.MaxFileLen = int(1024 * 1024 * 10)

intGubun	= SQL_Injection(Trim(UploadForm("intGubun")))
strId		= SQL_Injection(Trim(UploadForm("strId")))
strPwd		= SQL_Injection(Trim(UploadForm("strPwd")))
strName		= SQL_Injection(Trim(UploadForm("strName")))
strPhone1	= SQL_Injection(Trim(UploadForm("strPhone1")))
strPhone2	= SQL_Injection(Trim(UploadForm("strPhone2")))
strPhone3	= SQL_Injection(Trim(UploadForm("strPhone3")))
strMobile1	= SQL_Injection(Trim(UploadForm("strMobile1")))
strMobile2	= SQL_Injection(Trim(UploadForm("strMobile2")))
strMobile3	= SQL_Injection(Trim(UploadForm("strMobile3")))
smsYN		= SQL_Injection(Trim(UploadForm("smsYN")))
strEmail1	= SQL_Injection(Trim(UploadForm("strEmail1")))
strEmail2	= SQL_Injection(Trim(UploadForm("strEmail2")))
mailYN		= SQL_Injection(Trim(UploadForm("mailYN")))
yeosinYN	= SQL_Injection(Trim(UploadForm("yeosinYN")))

cName		= SQL_Injection(Trim(UploadForm("cName")))
cNum1		= SQL_Injection(Trim(UploadForm("cNum1")))
cNum2		= SQL_Injection(Trim(UploadForm("cNum2")))
cNum3		= SQL_Injection(Trim(UploadForm("cNum3")))
ceo			= SQL_Injection(Trim(UploadForm("ceo")))
cPhone1		= SQL_Injection(Trim(UploadForm("cPhone1")))
cPhone2		= SQL_Injection(Trim(UploadForm("cPhone2")))
cPhone3		= SQL_Injection(Trim(UploadForm("cPhone3")))
cMobile1	= SQL_Injection(Trim(UploadForm("cMobile1")))
cMobile2	= SQL_Injection(Trim(UploadForm("cMobile2")))
cMobile3	= SQL_Injection(Trim(UploadForm("cMobile3")))
cEmail1		= SQL_Injection(Trim(UploadForm("cEmail1")))
cEmail2		= SQL_Injection(Trim(UploadForm("cEmail2")))
cZip		= SQL_Injection(Trim(UploadForm("cZip")))
cAddr1		= SQL_Injection(Trim(UploadForm("cAddr1")))
cAddr2		= SQL_Injection(Trim(UploadForm("cAddr2")))
comType		= SQL_Injection(Trim(UploadForm("cType")))
cCate		= SQL_Injection(Trim(UploadForm("cCate")))

'// 변수 가공
strPhone = strPhone1 & "-" & strPhone2 & "-" & strPhone3
strMobile = strMobile1 & "-" & strMobile2 & "-" & strMobile3
strEmail = strEmail1 & "@" & strEmail2
cNum = cNum1 & "-" & cNum2 & "-" & cNum3
cPhone = cPhone1 & "-" & cPhone2 & "-" & cPhone3
cMobile = cMobile1 & "-" & cMobile2 & "-" & cMobile3
cEmail = cEmail1 & "@" & cEmail2

If smsYN <> "Y" Then
	smsYN = "N"
End If

If mailYN <> "Y" Then
	mailYN = "N"
End If

ReDim mFile(1)
For i = 1 To UploadCnt
	If UploadForm("strFile" & i).FileName <> "" Then
		'// 업로드 가능 여부 체크
		If UploadFileChk(UploadForm("strFile" & i).FileName)=False Then
			Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 용량 체크
		If UploadForm("strFile" & i).FileLen > UploadForm.MaxFileLen Then
			Response.Write "<script language='javascript'>alert('10MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 저장
		mFile(i-1) = UploadForm("strFile" & i).Save(, False)
		mFile(i-1) = UploadForm("strFile" & i).LastSavedFileName
	End If
Next

Call DbOpen()

Sql = "INSERT INTO mTb_Member2("
Sql = Sql & "intGubun, "
Sql = Sql & "strId, "
Sql = Sql & "strPwd, "
Sql = Sql & "strName, "
Sql = Sql & "strPhone, "
Sql = Sql & "strMobile, "
Sql = Sql & "smsYN, "
Sql = Sql & "strEmail, "
Sql = Sql & "mailYN, "
Sql = Sql & "yeosinYN, "
Sql = Sql & "cName, "
Sql = Sql & "cNum, "
Sql = Sql & "ceo, "
Sql = Sql & "cPhone, "
Sql = Sql & "cMobile, "
Sql = Sql & "cEmail, "
Sql = Sql & "cZip, "
Sql = Sql & "cAddr1, "
Sql = Sql & "cAddr2, "
Sql = Sql & "cType, "
Sql = Sql & "cCate, "
Sql = Sql & "strFile1, "
Sql = Sql & "strFile2) VALUES("
Sql = Sql & "'" & intGubun & "',"
Sql = Sql & "N'" & strId & "',"
Sql = Sql & "'" & Encrypt_Sha(strPwd) & "',"
Sql = Sql & "N'" & strName & "',"
Sql = Sql & "'" & strPhone & "',"
Sql = Sql & "'" & strMobile & "',"
Sql = Sql & "'" & smsYN & "',"
Sql = Sql & "N'" & strEmail & "',"
Sql = Sql & "'" & mailYN & "',"
Sql = Sql & "'" & yeosinYN & "',"
Sql = Sql & "N'" & cName & "',"
Sql = Sql & "'" & cNum & "',"
Sql = Sql & "N'" & ceo & "',"
Sql = Sql & "'" & cPhone & "',"
Sql = Sql & "'" & cMobile & "',"
Sql = Sql & "N'" & cEmail & "',"
Sql = Sql & "'" & cZip & "',"
Sql = Sql & "N'" & cAddr1 & "',"
Sql = Sql & "N'" & cAddr2 & "',"
Sql = Sql & "N'" & comType & "',"
Sql = Sql & "N'" & cCate & "',"
Sql = Sql & "N'" & SQL_Injection(Trim(mFile(0))) & "',"
Sql = Sql & "N'" & SQL_Injection(Trim(mFile(1))) & "')"

dbconn.execute(Sql)
dbconn.execute("INSERT INTO Record_views (menu,manage_id,manage_name,ip) VALUES('회원등록','"&session("aduserid")&"','"&session("adusername")&"','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

Set UploadForm = Nothing

Call DbClose()

Call jsAlertMsgUrl("회원 등록이 완료되었습니다.", "./list.asp?menucode="&menucode)
%>