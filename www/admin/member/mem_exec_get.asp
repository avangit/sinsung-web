<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp" -->

<%
ReDim mFile(1), rsFile(1)

'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.DefaultPath = Server.MapPath("/upload/member/")

intseq		= SQL_Injection(Trim(Request.QueryString("n")))
mode		= SQL_Injection(Trim(Request.QueryString("m")))
message		= SQL_Injection(Trim(Request.QueryString("message")))
menucode	= SQL_Injection(Trim(Request.QueryString("menucode")))
page		= SQL_Injection(Trim(Request.QueryString("page")))
param		= SQL_Injection(Trim(Request.QueryString("param")))
ori_intSeq	= SQL_Injection(Trim(Request.QueryString("intSeq")))

If intseq = "" Or ISNULL(intseq) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","history.back();")
Else
	intseq = Replace(intseq, " ", ",")
End If

If Left(menucode, 3) = "s18" Then
	href = "list.asp?menucode="&menucode&"&page=" & page & "&param=" & param
ElseIf Left(menucode, 5) = "s18_1" Then
	href = "modify.asp?menucode="&menucode&"&intSeq=" & ori_intSeq
ElseIf Left(menucode, 3) = "s20" Then
	href = "leave_list.asp?menucode="&menucode&"&page=" & page & "&param=" & param
End If

Call dbOpen()

'// 회원삭제 시
If mode = "del" Then
	message = "삭제"

	dbconn.execute("DELETE FROM " & tablename & " WHERE intseq IN (" & intseq & ")")

	sql = "SELECT intGubun, strId, strFile1, strFile2 FROM " & tablename & " WHERE intseq IN (" & intseq & ")"

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
			intGubun	= rs("intGubun")
			strId 		= rs("strId")

			dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES ('"&session("aduserid")&"','"&strId&"','삭제','하위회원삭제','"&Request.ServerVariables("REMOTE_ADDR")&"') ")
'			dbconn.execute("INSERT INTO Record_views2 (work1,before_right,change_right,member_id,manage_id,ip) values('삭제','"&intGubun&"','0','"&strId&"','"&session("aduserid")&"','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

			For i = 1 To UploadCnt
				rsFile(i-1) = rs("strFile" & i)

				'기존파일 삭제
				If rsFile(i-1) <> "" Then
					UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile(i-1)
				End If
			Next

			rs.movenext
		Loop
	End If

	rs.close
	Set rs = Nothing

'// 회원탈퇴 시
ElseIf mode = "sec" Then

'// 하위회원 상태변경 시
ElseIf mode = "etc" Then
	If message = "사용" Then
		mem_status = 0
	ElseIf message = "미사용" Then
		mem_status = 1
	End If

	dbconn.execute("UPDATE " & tablename & " SET intAction = '" & mem_status & "' WHERE intseq IN (" & intseq & ")")

	sql = "SELECT strId FROM " & tablename & " WHERE intseq IN (" & intseq & ")"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
			strId 		= rs("strId")
			dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&session("aduserid")&"','"&strId&"','"&message&"','하위회원상태변경','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

			rs.movenext
		Loop
	End If

	rs.close
	Set rs = Nothing

Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("" & message & "되었습니다.", href)
%>