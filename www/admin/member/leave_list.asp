<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->
<!-- #include file="config.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<article>
  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>탈퇴회원</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
	  <%
		dim fieldname 	: fieldname 	= RequestS("fieldname")
		dim fieldvalue 	: fieldvalue 	= RequestS("fieldvalue")
		dim strStart 	: strStart 		= RequestS("strStart")
		dim strEnd 		: strEnd 		= RequestS("strEnd")

		dim intTotalCount, intTotalPage

		dim intNowPage			: intNowPage 		= Request.QueryString("page")
		dim intPageSize			: intPageSize 		= 15
		dim intBlockPage		: intBlockPage 		= 10

		dim query_filde			: query_filde		= "*"
		dim query_Tablename		: query_Tablename	= TableName
		dim query_where			: query_where		= " intAction <> 0 AND intGubun IN ('1', '2')"
		dim query_orderby		: query_orderby		= "order by intseq desc"

		sdate		= RequestS("sdate")
		edate 		= RequestS("edate")

		param = "sdate='" & sdate & "'&edate='" & edate & "'&fieldname='" & fieldname & "'&fieldvalue='" & fieldvalue & "'"

		if Len(fieldname) > 0 then
			query_where = query_where &" and "& fieldname & " like '%" & fieldvalue & "%' "
		end if

		If sdate <> "" And edate <> "" Then
			query_where = query_where &  " AND leaveDate BETWEEN '" & sdate & "' AND '"&edate&"'"
		ElseIf sdate <> "" And edate = "" Then
			query_where = query_where &  " AND leaveDate >= '" & sdate  &"'"
		ElseIf sdate = "" And edate <> "" Then
			query_where = query_where &  " AND leaveDate <= DATEADD(day,1,'"&edate&"')"
		End If

		call intTotal

		dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

		'response.Write getQuery

		sql = getQuery
		call dbopen
		set rs = dbconn.execute(sql)

        dbconn.execute("INSERT INTO Record_views (menu,manage_id,manage_name,ip) VALUES('탈퇴리스트','"&session("aduserid")&"','"&session("adusername")&"','"&Request.ServerVariables("REMOTE_ADDR")&"') ")
	  %>
		<form name = "frm" action = "?<%=getstring("")%>" class="smart-form m10"  method="post" >
		<table class="table table-striped table-bordered line-h" width="100%">
			<tr>
				<th>탈퇴일</th>
				<td colspan="3">
					<input type="text" name="sdate" id="sdate" value="<%=sdate%>" autocomplete="off"> ~ <input type="text" name="edate" id="edate" value="<%=edate%>" autocomplete="off">
				</td>
			</tr>
			<tr>
				<th>검색</th>
				<td colspan="3">
					<label class="select w10 pro_detail">
					<select class="input-sm h34" name="fieldname" id="fieldname">
						<option value="strId" <% If fieldname = "strId" Then %> selected<% End If %>>아이디</option>
						<option value="strName" <% If fieldname = "strName" Then %> selected<% End If %>>이름</option>
					</select>
					</label>
					<div class="icon-addon addon-md col-md-10 col-4">
						<input type="text" name="fieldvalue" id="fieldvalue" class="form-control col-10" value="<%=fieldvalue%>">
						<label for="fieldvalue" class="glyphicon glyphicon-search " rel="tooltip" title="fieldvalue"></label>
					</div>
					<a href="javascript:frm.submit();" class="btn btn-primary"><i class="fa fa-search"></i> 검색</a>
					<a href="location.href='./leave_list.asp?menucode=<%=menucode%>';" class="btn btn-default"><i class="fa fa-refresh"></i> 검색 초기화</a>
				</td>
			</tr>
		</table>
		</form>
		<!--label class="select w10 pro_detail">
		  <select class="input-sm h34" name="fieldname">
			<option value="strId" <% If fieldname = "strId" Then %> selected<% End If %>>아이디</option>
			<option value="strName" <% If fieldname = "strName" Then %> selected<% End If %>>이름</option>
		  </select> <i></i>
		</label>
		<div class="icon-addon addon-md col-md-10 col-2">
		  <input type="text" name="fieldvalue" class="form-control col-10" value="<%=fieldvalue%>">
		  <label for="fieldvalue" class="glyphicon glyphicon-search " rel="tooltip" title="fieldvalue"></label>
		</div>
		<input type="submit" class="btn btn-default" value="검색"-->

		<%=intTotalCount%>건
		<form name="list" method="get" action="./mem_exec_get.asp" style="margin:0;">
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<input type="hidden" name="page" value="<%=intNowPage%>">
		<input type="hidden" name="param" value="<%=param%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
	    <colgroup>
		  <col style="width:5%">
		  <col style="width:5%">
		  <col style="width:10%">
		  <col style="width:15%">
		  <col style="">
		  <col style="width:10%">
		  <col style="width:10%">
		  <col style="width:10%">
	    </colgroup>
		<thead>
		  <tr>
			<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
			<th data-class="expand">번호</th>
			<th>성명</th>
			<th data-class="expand">아이디</th>
			<th data-hide="phone,tablet">회사명</th>
			<th data-hide="phone,tablet">탈퇴사유</th>
			<th data-hide="phone,tablet">가입일</th>
			<th data-hide="phone,tablet">탈퇴일</th>
		  </tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="8" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	rs.move MoveCount
	Do while Not rs.eof
		Dim leave_reason, leaveDate

		intseq			= rs("intseq")
		strId 			= rs("strId")
		strName 		= rs("strName")
		cName 			= rs("cName")
		dtmInsertDate	= rs("dtmInsertDate")
		intAction 		= rs("intAction")
		intGubun 		= rs("intGubun")
		leave_reason 	= rs("leave_reason")
		leaveDate 		= rs("leaveDate")

		If intAction = 1 Then
			intAction = "<font color=red>탈퇴</font>"
		End If
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><input type="checkbox" value="<%=intseq%>"></td>
				<td><%=intNowNum%></td>
				<td><a href="modify.asp?intSeq=<%=intseq%>&<%=getstring("menucode="&menucode)%>"><%=rs("strName")%></a></td>
				<td><a href="modify.asp?intSeq=<%=intseq%>&<%=getstring("menucode="&menucode)%>"><%=rs("strId")%></a></td>
				<td><%=cName%></td>
				<td><%=leave_reason%></td>
				<td><%=left(dtmInsertDate,10)%></td>
				<td><%=left(leaveDate,10)%></td>
			</tr>
<%
		intNowNum = intNowNum - 1
		rs.movenext
	Loop
End If

rs.close
Set rs = Nothing
dbconn.close
%>
		</tbody>
		</table>

		<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:20 0 0 0;">
			<tr>
				<td>
					<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
				</td>
			</tr>
		</table>
		</form>

		<%call Paging_list("")%>
	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->
<script>
  $(document).ready(function() {

  // DO NOT REMOVE : GLOBAL FUNCTIONS!
  pageSetUp();

  });
</script>
<!-- #include file="../inc/footer.asp" -->