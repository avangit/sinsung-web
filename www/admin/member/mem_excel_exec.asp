<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config.asp" -->

<%
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/excel/")
UploadForm.MaxFileLen = int(1024 * 1024 * 10)

mem_mode	= SQL_Injection(Trim(UploadForm("mem_mode")))

'// 업로드 파일 유무 확인
If UploadForm("strFile1").FileName <> "" Then
	mimetype = UploadForm("strFile1").MimeType

	'// 엑셀파일 여부 확인
	If mimetype = "application/vnd.ms-excel" Or mimetype="application/octet-stream" Then

		UploadForm("strFile1").Save
		strPath = UploadForm.DefaultPath & "\" & UploadForm("strFile1").FileName

'		xlsConnect ="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strPath & ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'"
		xlsConnect ="Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strPath & ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'"
		Set xlsCon  = Server.CreateObject("ADODB.Connection")

		xlsCon.Open xlsConnect

		sql = "SELECT * FROM [" & UploadForm("strFile1").FileName & "]"

		Set rs = xlsCon.execute(sql)

		'// 일반회원 업로드
		If mem_mode = "normal" Then
			If Not(rs.bof Or rs.eof) Then
				Do Until rs.eof
					intGubun	= SQL_Injection(Trim(rs(0)))
					strId		= SQL_Injection(Trim(rs(1)))
					strPwd		= SQL_Injection(Trim(rs(2)))
					strName		= SQL_Injection(Trim(rs(3)))
					strPhone	= SQL_Injection(Trim(rs(4)))
					strMobile	= SQL_Injection(Trim(rs(5)))
					smsYN		= SQL_Injection(Trim(rs(6)))
					strEmail	= SQL_Injection(Trim(rs(7)))
					mailYN		= SQL_Injection(Trim(rs(8)))
					yeosinYN	= SQL_Injection(Trim(rs(9)))
					cName		= SQL_Injection(Trim(rs(10)))
					cNum		= SQL_Injection(Trim(rs(11)))
					ceo			= SQL_Injection(Trim(rs(12)))
					cPhone		= SQL_Injection(Trim(rs(13)))
					cMobile		= SQL_Injection(Trim(rs(14)))
					cEmail		= SQL_Injection(Trim(rs(15)))
					cZip		= SQL_Injection(Trim(rs(16)))
					cAddr1		= SQL_Injection(Trim(rs(17)))
					cAddr2		= SQL_Injection(Trim(rs(18)))
					comType		= SQL_Injection(Trim(rs(19)))
					cCate		= SQL_Injection(Trim(rs(20)))

					If intGubun = "일반" Then
						intGubun = 1
					ElseIf intGubun = "마스터" Then
						intGubun = 2
					End If

					Call DbOpen()

					strSQL = "SELECT * FROM " & tablename & " WHERE strId = '" & strId & "'"

					Set rs2 = dbconn.execute(strSQL)

					If Not(rs2.bof Or rs2.eof) Then

						'// 엑셀 파일 삭제
						UploadForm.Deletefile UploadForm.DefaultPath & "\" & UploadForm("strFile1").FileName

						rs2.close
						Set rs2 = Nothing

						rs.close
						Set rs = Nothing

						Set UploadForm = Nothing

						xlsCon.close
						Set xlsCon = Nothing

						Call DbClose()

						Call jsAlertMsgUrl("중복아이디가 발견되었습니다.\n\n 해당 아이디 수정 후 이전 데이터들은 삭제하고 재시도해주세요.\n\n 아이디 : " & strId & "", "./mem_excel_write.asp?menucode="&menucode)

					Else

						sql = "INSERT INTO " & tablename & "("
						sql = Sql & "intGubun, "
						sql = Sql & "strId, "
						sql = Sql & "strPwd, "
						sql = Sql & "strName, "
						sql = Sql & "strPhone, "
						sql = Sql & "strMobile, "
						Sql = Sql & "smsYN, "
						Sql = Sql & "strEmail, "
						Sql = Sql & "mailYN, "
						Sql = Sql & "yeosinYN, "
						Sql = Sql & "cName, "
						Sql = Sql & "cNum, "
						Sql = Sql & "ceo, "
						Sql = Sql & "cPhone, "
						Sql = Sql & "cMobile, "
						Sql = Sql & "cEmail, "
						Sql = Sql & "cZip, "
						Sql = Sql & "cAddr1, "
						Sql = Sql & "cAddr2, "
						Sql = Sql & "cType, "
						Sql = Sql & "cCate) VALUES("
						Sql = Sql & "'" & intGubun & "',"
						Sql = Sql & "N'" & strId & "',"
						Sql = Sql & "'" & Encrypt_Sha(strPwd) & "',"
						Sql = Sql & "N'" & strName & "',"
						Sql = Sql & "'" & strPhone & "',"
						Sql = Sql & "'" & strMobile & "',"
						Sql = Sql & "'" & smsYN & "',"
						Sql = Sql & "N'" & strEmail & "',"
						Sql = Sql & "'" & mailYN & "',"
						Sql = Sql & "'" & yeosinYN & "',"
						Sql = Sql & "N'" & cName & "',"
						Sql = Sql & "'" & cNum & "',"
						Sql = Sql & "N'" & ceo & "',"
						Sql = Sql & "'" & cPhone & "',"
						Sql = Sql & "'" & cMobile & "',"
						Sql = Sql & "N'" & cEmail & "',"
						Sql = Sql & "'" & cZip & "',"
						Sql = Sql & "N'" & cAddr1 & "',"
						Sql = Sql & "N'" & cAddr2 & "',"
						Sql = Sql & "N'" & comType & "',"
						Sql = Sql & "N'" & cCate & "')"

						dbconn.execute(Sql)
					End If

					rs2.MoveNext
				Loop

				'// 엑셀 파일 삭제
				UploadForm.Deletefile UploadForm.DefaultPath & "\" & UploadForm("strFile1").FileName

				rs2.close
				Set rs2 = Nothing

				rs.close
				Set rs = Nothing

				Set UploadForm = Nothing

				xlsCon.close
				Set xlsCon = Nothing

				Call DbClose()

				Call jsAlertMsgUrl("회원등록이 완료되었습니다.", "./mem_excel_write.asp?menucode="&menucode)
			End If

		'// 마스터회원 담당자 업로드
		ElseIf mem_mode = "master" Then
			If Not(rs.bof Or rs.eof) Then
				Do While Not rs.eof
					intSeq		= SQL_Injection(Trim(rs(0)))
					strId		= SQL_Injection(Trim(rs(1)))
					strName		= SQL_Injection(Trim(rs(2)))
					manager1	= SQL_Injection(Trim(rs(3)))
					m1_Email	= SQL_Injection(Trim(rs(4)))
					m1_Phone	= SQL_Injection(Trim(rs(5)))
					m1_Mobile	= SQL_Injection(Trim(rs(6)))
					manager2	= SQL_Injection(Trim(rs(7)))
					m2_Email	= SQL_Injection(Trim(rs(8)))
					m2_Phone	= SQL_Injection(Trim(rs(9)))
					m2_Mobile	= SQL_Injection(Trim(rs(10)))
					manager3	= SQL_Injection(Trim(rs(11)))
					m3_Email	= SQL_Injection(Trim(rs(12)))
					m3_Phone	= SQL_Injection(Trim(rs(13)))
					m3_Mobile	= SQL_Injection(Trim(rs(14)))

					Call DbOpen()

					Sql = "UPDATE " & tablename & " SET "
					Sql = Sql & "manager1 = '" & manager1 & "', "
					Sql = Sql & "m1_Email = '" & m1_Email & "', "
					Sql = Sql & "m1_Phone = '" & m1_Phone & "', "
					Sql = Sql & "m1_Mobile = '" & m1_Mobile & "', "
					Sql = Sql & "manager2 = '" & manager2 & "', "
					Sql = Sql & "m2_Email = '" & m2_Email & "', "
					Sql = Sql & "m2_Phone = '" & m2_Phone & "', "
					Sql = Sql & "m2_Mobile = '" & m2_Mobile & "', "
					Sql = Sql & "manager3 = '" & manager3 & "', "
					Sql = Sql & "m3_Email = '" & m3_Email & "', "
					Sql = Sql & "m3_Phone = '" & m3_Phone & "', "
					Sql = Sql & "m3_Mobile = '" & m3_Mobile & "' "
					Sql = Sql & " WHERE intseq = '" & intseq & "'"

					dbconn.execute(Sql)

					rs.MoveNext
				Loop

				'// 엑셀 파일 삭제
				UploadForm.Deletefile UploadForm.DefaultPath & "\" & UploadForm("strFile1").FileName

				rs.close
				Set rs = Nothing

				Set UploadForm = Nothing

				xlsCon.close
				Set xlsCon = Nothing

				Call DbClose()

				Call jsAlertMsgUrl("담당자 등록이 완료되었습니다.", "./mem_excel_write.asp?menucode="&menucode)
			End If
		End If

	'// 엑셀 파일이 아니라면
	Else

		Set UploadForm = Nothing

		xlsCon.close
		Set xlsCon = Nothing

		Call jsAlertMsgUrl("엑셀파일만 등록가능합니다.", "./mem_excel_write.asp?menucode="&menucode)
	End If

'// 업로드 파일이 없다면
Else
	Set UploadForm = Nothing

	Call jsAlertMsgUrl("업로드 파일이 없습니다.", "./mem_excel_write.asp?menucode="&menucode)
End If
%>