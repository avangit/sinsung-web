<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config_product.asp" -->
<%
'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/goods/")
UploadForm.MaxFileLen = int(1024 * 1024 * 10)

g_idx			= SQL_Injection(Trim(UploadForm("n")))
g_name			= SQL_Injection(Trim(UploadForm("g_name")))
g_display		= SQL_Injection(Trim(UploadForm("g_display")))
g_act			= SQL_Injection(Trim(UploadForm("g_act")))
g_bestYN		= SQL_Injection(Trim(UploadForm("g_bestYN")))
g_keyword		= SQL_Injection(Trim(UploadForm("g_keyword")))
g_spec			= SQL_Injection(Trim(UploadForm("g_spec")))
g_intro			= SQL_Injection(Trim(UploadForm("g_intro")))
g_Money			= SQL_Injection(Trim(UploadForm("g_Money")))
total_op		= SQL_Injection(Trim(UploadForm("total_op")))
g_use_totalChk	= SQL_Injection(Trim(UploadForm("g_use_totalChk")))
g_optionGroup	= SQL_Injection(Trim(UploadForm("opt_group")))
g_Memo			= SQL_Injection(Trim(UploadForm("g_Memo")))
g_video			= SQL_Injection(Trim(UploadForm("g_video")))
g_menual1		= SQL_Injection(Trim(UploadForm("g_menual1")))
g_menual2		= SQL_Injection(Trim(UploadForm("g_menual2")))
g_menual3		= SQL_Injection(Trim(UploadForm("g_menual3")))
g_menual4		= SQL_Injection(Trim(UploadForm("g_menual4")))
g_menual5		= SQL_Injection(Trim(UploadForm("g_menual5")))

'// 변수 가공
If g_use_totalChk <> "Y" Then
	g_use_totalChk = "N"
End If

If g_bestYN <> "Y" Then
	g_bestYN = "N"
End If

For i = 1 To 20
	If SQL_Injection(Trim(UploadForm("g_optionList" & i))) <> "" Then
		g_optionList = g_optionList & SQL_Injection(Trim(Replace(UploadForm("g_optionList" & i), ",", ""))) & ","
	End If
Next

Call DbOpen()

sql = "SELECT c_code FROM GOODS_CATE_JOIN WHERE g_idx = '" & g_idx & "'"
Set rs = dbconn.execute(sql)

If Not(rs.eof) Then
	g_cate = rs("c_code")
End If

rs.close

If UploadForm("g_simg").FileName <> "" Then
	'// 업로드 가능 여부 체크
	If UploadFileChk(UploadForm("g_simg").FileName)=False Then
		Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
		Set UploadForm = Nothing
		Response.End
	End If

	'// 파일 용량 체크
	If UploadForm("g_simg").FileLen > UploadForm.MaxFileLen Then
		Response.Write "<script language='javascript'>alert('10MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
		Set UploadForm = Nothing
		Response.End
	End If

	strSQL = "SELECT g_simg FROM " & tablename & " WHERE g_idx = " & g_idx & ""
	Set rs = dbconn.execute(strSQL)

	ReDim rsFile(0)

	If Not(rs.eof) Then
		rsFile(0) = rs("g_simg")
	End If

	rs.close

	'기존파일 삭제
	If rsFile(0) <> "" Then
		UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile(0)
	End If

	'// 파일 저장
	g_simg = UploadForm("g_simg").Save(, False)
	g_simg = UploadForm("g_simg").LastSavedFileName

	Sql = "UPDATE " & tablename & " SET g_simg =N'" & SQL_Injection(g_simg) & "' WHERE g_idx='" & g_idx & "'"
	dbconn.execute(Sql)
End If

strSQL = "SELECT g_bimg1, g_bimg2, g_bimg3, g_bimg4, g_bimg5 FROM " & tablename & " WHERE g_idx = '" & g_idx & "'"
Set rs = dbconn.execute(strSQL)

ReDim mFile(4), rsFile1(4)

If Not(rs.eof) Then
	For j=1 To 5
		rsFile1(j-1) = rs("g_bimg" & j)
	Next
End If

rs.close
Set rs = Nothing

For k = 1 To 5
	If UploadForm("g_bimg" & k).FileName <> "" Then
		'// 업로드 가능 여부 체크
		If UploadFileChk(UploadForm("g_bimg" & k).FileName)=False Then
			Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 용량 체크
		If UploadForm("g_bimg" & k).FileLen > UploadForm.MaxFileLen Then
			Response.Write "<script language='javascript'>alert('10MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'기존파일 삭제
		If rsFile1(k-1) <> "" Then
			UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile1(k-1)
		End If

		'// 파일 저장
		mFile(k-1) = UploadForm("g_bimg" & k).Save(, False)
		mFile(k-1) = UploadForm("g_bimg" & k).LastSavedFileName

		Sql = "UPDATE " & tablename & " SET g_bimg" & k & "=N'" & SQL_Injection(mFile(k-1)) & "' WHERE g_idx='" & g_idx & "'" & Chr(10) & Chr(13)
		dbconn.execute(Sql)
	End If
Next

Sql = "UPDATE " & tablename & " SET "
Sql = Sql & "g_name = N'" & g_name & "', "
Sql = Sql & "g_display = '" & g_display & "', "
Sql = Sql & "g_act = '" & g_act & "', "
Sql = Sql & "g_category = '" & g_cate & "', "
Sql = Sql & "g_bestYN = '" & g_bestYN & "', "
Sql = Sql & "g_keyword = N'" & g_keyword & "', "
Sql = Sql & "g_spec = N'" & g_spec & "', "
Sql = Sql & "g_intro = N'" & g_intro & "', "
Sql = Sql & "g_Money = '" & g_Money & "', "
Sql = Sql & "total_op = '" & total_op & "', "
Sql = Sql & "g_use_totalChk = '" & g_use_totalChk & "', "
Sql = Sql & "g_optionGroup = '" & g_optionGroup & "', "
Sql = Sql & "g_optionList = N'" & g_optionList & "', "
Sql = Sql & "g_Memo = N'" & g_Memo & "', "
Sql = Sql & "g_video = '" & g_video & "', "
Sql = Sql & "g_menual1 = '" & g_menual1 & "', "
Sql = Sql & "g_menual2 = '" & g_menual2 & "', "
Sql = Sql & "g_menual3 = '" & g_menual3 & "', "
Sql = Sql & "g_menual4 = '" & g_menual4 & "', "
Sql = Sql & "g_menual5 = '" & g_menual5 & "' "
Sql = Sql & " WHERE g_idx = '" & g_idx & "'"

dbconn.execute(Sql)

Set UploadForm = Nothing

Call DbClose()

Call jsAlertMsgUrl("상품 수정이 완료되었습니다.", "./product.asp?menucode="&menucode)
%>