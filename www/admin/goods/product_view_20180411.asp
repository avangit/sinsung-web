
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->



			<%
				
				Dim tablename
				tablename = "GOODS"

				Const UploadFolder = "/upload/"

				Dim g_idx, g_act, g_autoimg, g_bimg, g_bimg2, g_bimg3, g_bimg4, g_CashOption, g_category, g_company, g_customerMoney, g_cyberMoney
				Dim g_icon, g_insertDay, g_keyword, g_mainimg, g_mainOption, g_Memo, g_mimg, g_Money, g_moneyMemo, g_moneyMemoOption, g_name
				Dim g_option1list, g_option2list, g_use_totalChk, g_option1_name, g_option2_name, g_origin, g_originalMoney, g_runDay, g_shortMemo
				Dim g_simg, g_specialMemo, total_op, g_relationGood,companyIdx,g_MemoTemp, g_sizeMemo, g_modelCut
				Dim strAction

				g_idx = requestQ("g_idx")
				
				Dim strSQL,rs
				
					
				Call DbOpen()					
					
					strSQL = "SELECT * FROM "&tablename& " WHERE g_idx = '"&g_idx&"'"				
				
					set rs = dbconn.execute(strSQL)		
					
					If not(rs.eof)  Then							
						g_idx 			= rs("g_idx")
						g_act 			= rs("g_act")
						g_autoimg		= rs("g_autoimg")				
						g_bimg 			= rs("g_bimg")
						g_bimg2			= rs("g_bimg2")
						g_bimg3			= rs("g_bimg3")		
						g_bimg4			= rs("g_bimg4")
						g_CashOption	= rs("g_CashOption")
						g_category		= rs("g_category")		
						g_company		= rs("g_company")
						g_customerMoney	= rs("g_customerMoney")
						g_cyberMoney 	= rs("g_cyberMoney")		
						g_icon 			= rs("g_icon")
						g_insertDay		= rs("g_insertDay")
						if g_insertDay = "1900-01-01" then g_insertDay = ""
						g_keyword		= rs("g_keyword")		
						g_mainimg		= rs("g_mainimg")
						g_mainOption	= rs("g_mainOption")
						g_Memo 			= rs("g_Memo")		
						g_mimg 			= rs("g_mimg")
						g_Money			= rs("g_Money")
						g_moneyMemo 	= rs("g_moneyMemo")							
						g_moneyMemoOption= rs("g_moneyMemoOption")
						g_name 			= rs("g_name")			
						g_option1list	= rs("g_option1list")
						g_option2list	= rs("g_option2list")
						g_use_totalChk	= rs("g_use_totalChk")			
						g_option1_name	= rs("g_option1_name")		
						g_option2_name	= rs("g_option2_name")
						g_origin		= rs("g_origin")
						g_originalMoney	= rs("g_originalMoney")	
						g_runDay		= rs("g_runDay")
						if g_runDay = "1900-01-01" then g_runDay = ""
						g_shortMemo		= rs("g_shortMemo")		
						g_simg 			= rs("g_simg")
						g_specialMemo	= rs("g_specialMemo")								
						total_op		= rs("total_op")
						g_relationGood	= rs("g_relationGood")
						companyIdx		= rs("companyIdx")
						g_MemoTemp		= rs("g_MemoTemp")
						g_sizeMemo		= rs("g_sizeMemo")
						g_modelCut		= rs("g_modelCut")
						
					
			%>
				
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">

					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>기본 정보  </h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->

						<form class="smart-form">
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="width:35%">
									<col style="width:15%">
									<col style="width:35%">
								</colgroup>
	
								<tbody>
									<tr>
										<th>상품코드</th>
										<td colspan="3"><%=g_idx%></td>
									</tr>
									
									<tr>
										<th class="text-danger">카테고리</th>
										<td colspan="3">
											<label class="textarea"> 
												<iframe name="joincatelist" src="/admin/category/joinCateList.asp?g_idx=<%=g_idx%>" width="40%" height="60" ></iframe>
												<textarea rows="3" name="info" class="w50 pro_detail" placeholder="Additional info"></textarea> 
												<a href="javascript:openWin();" class="btn btn-default"><i class="fa fa-sitemap"></i>카테고리등록</a>
												
											</label>
										</td>
									</tr>

									<tr>
										<th class="text-danger">상품명</th>
										<td colspan="3">
										<label for="address2" class="input">
											<input type="text" name="address2" id="address2" placeholder="Address">
										</label>
										</td>
									</tr>
									<tr>
										<th>상팸판매옵션</th>
										<td colspan="3">
											<div class="inline-group">
												<label class="radio">
													<input type="radio" name="radio-inline" checked="">
													<i></i>상품진열</label>
												<label class="radio ">
													<input type="radio" name="radio-inline">
													<i></i><span class="text-danger">판매대기</span></label>(상품리스트, Wishlist 모두 안보임) 
											</div>
										</td>
									</tr>

									<tr>
										<th>소비자가격</th>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" class ="w30" id="address2" placeholder="">
											</label>
										</td>

										
										<th>판매가격</th>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" class ="w30" id="address2" placeholder="">
											</label>
										</td>
									</tr>

									
									<tr>
										<th>구입원가</th>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" class ="w30" id="address2" placeholder="">
											</label>
										</td>

										
										<th>적립금</th>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" class ="w30" id="address2" placeholder="">
											</label>
										</td>
									</tr>

									<tr>
										<th>도메가</th>
										<td colspan="3">
											<label for="address2" class="input">
											<input type="text" name="address2" class ="w10 pro_view" id="address2" placeholder="">
											<input type="text" name="address2" class ="w10 pro_view" id="address2" placeholder="">
											<input type="text" name="address2" class ="w10 pro_view" id="address2" placeholder="">
											<input type="text" name="address2" class ="w10 pro_view" id="address2" placeholder="">
											<input type="text" name="address2" class ="w10 pro_view" id="address2" placeholder="">
											</label>
										</td>
									</tr>

									<tr>
										<th>관련상품</th>
										<td  colspan="3">
											<label for="address2" class="input">
											<input type="text" name="address2" id="address2" placeholder="">
											</label>
											* 상품코드를'6개'를 입력해주세요 (입력예시 : 3/7/8 ) 마지막"/"는 넣지 마세요
										</td>
									</tr>
									
									<tr>
										<th>상품등록일</th>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" class ="w30" id="address2" placeholder="" value="<%=Date()%>">
											</label>
										</td>

										
										<th>제조원</th>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" class ="w30" id="address2" placeholder="">
											</label>
										</td>
									</tr>

									<tr>
										<th>상품출시일</th>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" class ="w30" id="address2" placeholder="">
											</label>
										</td>

										
										<th>원산지</th>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" class ="w30" id="address2" placeholder="">
											</label>
										</td>
									</tr>
	
										
									<tr>
										<th>내부메모</th>
										<td colspan="3">
											<label class="textarea"> 										
												<textarea rows="3" name="info" class="h150 pro_detail" placeholder="Additional info"></textarea> 
												
											</label>
										</td>
									</tr>
									
									<tr>
										<th>상품검색어</th>
										<td colspan="3">
											<label for="address2" class="input">
												<input type="text" name="address2" id="address2" placeholder="">
											</label>
										</td>
									</tr>
									

									<tr>
										
									</tr>
								</tbody>
						
							</table>




						</div>
						</form>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>









				
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">

					<header>
						<span class="widget-icon"> <i class="fa fa-picture-o"></i> </span>
						<h2>상품이미지</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<form class="smart-form">
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="width:7%">
									<col style="">
								</colgroup>
	
								<tbody>
									<tr>
										<th>상세이미지1</th>
										<td><img src="/admin/img/images/noimg.gif"></td>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="file2" type="file" name="file2" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
												</div>
											</section>
											
											<a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a>

										</td>
									</tr>

									<tr>
										<th>상세이미지2</th>
										<td><img src="/admin/img/images/noimg.gif"></td>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="file2" type="file" name="file2" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
												</div>
											</section>
											
											<a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a>

										</td>
									</tr>

									<tr>
										<th>상세이미지3</th>
										<td><img src="/admin/img/images/noimg.gif"></td>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="file2" type="file" name="file2" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
												</div>
											</section>
											
											<a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a>

										</td>
									</tr>

									<tr>
										<th>상세이미지4</th>
										<td><img src="/admin/img/images/noimg.gif"></td>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="file2" type="file" name="file2" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
												</div>
											</section>
											
											<a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a>

										</td>
									</tr>

									<tr>
										<th>상세설명</th>
										<td colspan="2" class="h300"><%call Editor("strcontent","")%></td>
									</tr>
									
									
								</tbody>
						
							</table>




						</div>
						
						</form>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->







						
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">

					<header>
						<span class="widget-icon"> <i class="fa fa-cogs"></i> </span>
						<h2>재고 및 옵션</h2>
	
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->

						<form class="smart-form">
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
	
								<tbody>
									<tr>
										<th>옵션명 입력</th>
										<th>- 좌측 옵션명을 입력하셔야 해당 옵션이 활성화 됩니다. </td>
										
									</tr>
									<tr>
										<th>옵션A Ex)색상 입력</th>
										<th>옵션A는 가격변동이 없는 옵션입니다. (입력예시 : 적색/흑색/녹색 ) 마지막"/"는 넣지 마세요. </td>
										
									</tr>
									<tr>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" id="address2" placeholder="">
											</label>
										</td>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" id="address2" placeholder="">
											</label>
										</td>
									</tr>
				

									<tr>
										<th>옵션B<br>Ex)사이즈</th>
										<th>옵션B 가격변동이 있는 옵션입니다. (입력예시 : 남성용\20000/여성용\0/중성용\-1000)<br>
											물품구입시 옵션을 선택하면 "\"뒤 금액이 판매가격에 합산됩니다.(변동 금액에"," 를 넣지 마세요.)<br>
											가격변동이 없는 옵션으로 사용하려면 옵션A 입력예시 처럼 입력하시면 됩니다. </td>
										
									</tr>
									<tr>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" id="address2" placeholder="">
											</label>
										</td>
										<td>
											<label for="address2" class="input">
												<input type="text" name="address2" id="address2" placeholder="">
											</label>
										</td>
									</tr>

									<tr>
										<th>총재고 수량</th>
										<td>
											<label for="address2" class="input pro_detail mr10">
												<input type="text" name="address2"  id="address2" placeholder="">
											</label>
											<label class="checkbox" class="pro_detail" style="display:inline-block;">
											<input type="checkbox" name="subscription" id="subscription">
											<i></i>재고제한없음 (재고체크를 하지 않고 상시 판매를 원하시는 경우 체크바랍니다.) </label>
										</td>
									</tr>
									<tr>
										
									</tr>
								</tbody>
						
							</table>




						</div>
						</form>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>












						
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" >

					<header>
						<span class="widget-icon"> <i class="fa fa-thumb-tack"></i> </span>
						<h2>아이콘 및 메인출력 옵션</h2>
	
					</header>
	
					<!-- widget div-->
					<div style="margin-bottom:30px;">
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->

						<form class="smart-form">
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
	
								<tbody>
									<tr>
										<th>아이콘</th>
										<th>
											<div class="row">
												<div class="col col-2">
													<label class="checkbox">
														<input type="checkbox" name="checkbox" checked="checked">
														<i></i>new</label>
													<label class="checkbox">
														<input type="checkbox" name="checkbox">
														<i></i>깜짝할인</label>
												</div>
												<div class="col col-2">
													<label class="checkbox">
														<input type="checkbox" name="checkbox" checked="checked">
														<i></i>hit</label>
													<label class="checkbox">
														<input type="checkbox" name="checkbox">
														<i></i>인기상품</label>
												</div>
												<div class="col col-2">
													<label class="checkbox">
														<input type="checkbox" name="checkbox" checked="checked">
														<i></i>hot</label>
													<label class="checkbox">
														<input type="checkbox" name="checkbox">
														<i></i>hit</label>
												</div>
											</div>
										</td>
										
									</tr>


									<tr>
										<th>메인출력</th>
										<th>
											<div class="row">
												<div class="col col-2">
													<label class="checkbox">
														<input type="checkbox" name="checkbox" checked="checked">
														<i></i>베스트상품</label>
													<label class="checkbox">
														<input type="checkbox" name="checkbox">
														<i></i>사용안함</label>
												</div>
												<div class="col col-2">
													<label class="checkbox">
														<input type="checkbox" name="checkbox" checked="checked">
														<i></i>사용안함</label>
													<label class="checkbox">
														<input type="checkbox" name="checkbox">
														<i></i>사용안함</label>
												</div>
												<div class="col col-2">
													<label class="checkbox">
														<input type="checkbox" name="checkbox" checked="checked">
														<i></i>사용안함</label>
													<label class="checkbox">
														<input type="checkbox" name="checkbox">
														<i></i>사용안함</label>
												</div>
											</div>
										</td>
										
									</tr>
									
									<tr>
										
									</tr>
								</tbody>
						
							</table>




						</div>
						
						<footer>
							<button type="submit" class="btn btn-primary">
								완료
							</button>
							<button type="button" class="btn btn-default" onclick="window.history.back();">
								리스트
							</button>
							<button type="button" class="btn btn-default" onclick="window.history.back();">
								삭제
							</button>
						</footer>
		
							
						</form>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
			<!-- WIDGET END -->
			
			<% 				
			end if
			rs.close()
			set rs = nothing
			Call DbClose() 
			%>
			<script language="javascript">
			<!--
			function OpenWindow(url,w,h,scrollbar) {
				if (scrollbar=="") scrollbar=0;
				//window.open(url,"ICCwindow","width=400,height=400,status=no,scrollbars=no");
				lPos = (screen.width) ? (screen.width-w)/2 : 0; tPos = (screen.height) ? (screen.height-h)/2 : 0;
				
				window.open(url, "", "height="+h+", width="+w+", left="+lPos+", top="+tPos+", toolbar=0, location=0,directories=0,status=0,menuBar=0,scrollBars="+scrollbar+",resizable=0");
			}

			function moneyValue() {
				document.insert.g_cyberMoney.value = eval(document.insert.g_Money.value * 0.01)
			}

			function checkform(frm)
			{
				if ( document.insert.companyInfo.value.replace(/ /gi, "") == ""  ) { alert("거래처정보를 입력해주세요"); document.insert.companyInfo.focus(); return false; }
				document.insert.submit();
			}
			//-->
			</script>

						
						
			</div>

		</div>
		<!-- END MAIN PANEL -->

		<!-- #include file="../inc/footer.asp" -->



		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">

			function openWin(){  
				window.open("/admin/product_category.asp", "제품카테고리", "width=800, height=700, toolbar=no, menubar=no, scrollbars=no, resizable=yes" );  
			} 



			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

	</body>

</html>