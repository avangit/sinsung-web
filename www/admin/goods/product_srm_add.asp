<% @CODEPAGE="65001" language="vbscript" %>
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-skins.min.css">

<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/add.css">

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
			<!-- NEW WIDGET START -->
				<!-- Widget ID (each widget will need unique ID)-->
				<script src="./form.js"></script>
				<script src="/admin/js/common.js"></script>
				<article>
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>마스터 회원 선택</h2>
					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->
<%
g_idx = Request.QueryString("g_idx")

Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(RequestS("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(RequestS("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= Trim(Request.QueryString("page"))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " intSeq, strId, strName, strMobile, strEmail, cName, dtmInsertDate "
Dim query_Tablename		: query_Tablename	= " mTb_Member2 "
Dim query_where			: query_where		= " intGubun = 2 AND intAction = 0 AND intSeq NOT IN (SELECT mem_idx FROM GOODS_SRM_JOIN WHERE g_idx = " & g_idx & ") "
Dim query_orderby		: query_orderby		= " ORDER BY intSeq DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery
Call dbopen
Set rs = dbconn.execute(sql)
%>
						<!-- widget content -->
						<div class="widget-body no-padding  smart-form ">
						<form name = "frm" id="frm" action = "" class="smart-form m10"  method="get" >
						<input type="hidden" name="menucode" value="<%=menucode%>">
						<input type="hidden" name="g_idx" value="<%=g_idx%>">
						<table class="table table-striped table-bordered line-h" width="100%">
							<tr>
								<th>검색</th>
								<td colspan="3">
									<label class="select w10 pro_detail">
									<select class="input-sm h34" name="fieldname" id="fieldname">
										<option value="strName" <% If fieldname = "strName" Then %> selected<% End If %>>이름</option>
										<option value="strId" <% If fieldname = "strId" Then %> selected<% End If %>>아이디</option>
										<option value="strEmail" <% If fieldname = "strEmail" Then %> selected<% End If %>>이메일</option>
										<option value="cName" <% If fieldname = "cName" Then %> selected<% End If %>>회사명</option>
									</select>
									</label>
									<div class="icon-addon addon-md col-md-10 col-4">
										<input type="text" name="fieldvalue" id="fieldvalue" class="form-control col-10" value="<%=fieldvalue%>">
										<label for="fieldvalue" class="glyphicon glyphicon-search " rel="tooltip" title="fieldvalue"></label>
									</div>
									<a href="javascript:frm.submit();" class="btn btn-primary"><i class="fa fa-search"></i> 검색</a>
									<a href="./product_srm_add.asp?menucode=<%=menucode%>&g_idx=<%=g_idx%>" class="btn btn-default"><i class="fa fa-refresh"></i> 검색 초기화</a>
								</td>
							</tr>
						</table>
						</form>

						<h6><b><%=intTotalCount%>건</b></h6>
						<form name="list" method="post" action="./product_srm_exec.asp" style="margin:0;">
						<input type="hidden" name="n">
						<input type="hidden" name="m">
						<input type="hidden" name="message">
						<input type="hidden" name="arr_val_list" value="">
						<input type="hidden" name="menucode" value="<%=menucode%>">
						<input type="hidden" name="g_idx" value="<%=g_idx%>">
						<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
							<colgroup>
								<col style="width:5%">
								<col style="width:5%">
								<col style="width:10%">
								<col style="width:10%">
								<col style="width:10%">
								<col style="width:10%">
								<col style="width:10%">
								<col style="width:10%">
							</colgroup>
							<thead>
								<tr>
									<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
									<th data-class="expand">번호</th>
									<th data-class="expand">회사명</th>
									<th data-class="expand">이름</th>
									<th data-hide="phone,tablet">아이디</th>
									<th data-hide="phone,tablet">이메일</th>
									<th data-hide="phone,tablet">휴대폰번호</th>
									<th data-hide="phone,tablet">가입일</th>
								</tr>
							</thead>

							<tbody>
<%
	If rs.eof Then
%>
								<tr style="border-bottom:#dddddd 1px solid;">
									<td colspan="8" align="center">데이터가 없습니다.</td>
								</tr>
<%
Else
	rs.move MoveCount
	Do while Not rs.eof
%>
								<tr>
									<td><input type="checkbox" value="<%=rs("intSeq")%>"></td>
									<td><%=intNowNum%></td>
									<td><%=rs("cName")%></td>
									<td><%=rs("strName")%></td>
									<td><%=rs("strId")%></td>
									<td><%=rs("strEmail")%></td>
									<td><%=rs("strMobile")%></td>
									<td><%=Left(rs("dtmInsertDate"), 10)%></td>
								</tr>
<%
			intNowNum = intNowNum - 1
			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing
%>
							</tbody>
						</table>

						<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:15px;">
							<tr>
								<td align="center">
									<a href="javascript:;" class="btn btn-default" onclick="self.close();"><i class="fa fa-close"></i> 취소</a>
									<a href="javascript:GetCheckbox(document.list, 'add', '추가');" class="btn btn-primary"><i class="fa fa-pencil"></i> 추가</a>
								</td>
							</tr>
						</table>
						</form>

						<%call Paging_list("")%>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->
			 </div>
			 <!-- end widget -->
		</article>
		<!-- WIDGET END -->