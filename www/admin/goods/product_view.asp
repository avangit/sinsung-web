
		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config_product.asp" -->

<%
ReDim g_menual(4)

g_idx = requestQ("g_idx")

Call DbOpen()

strSQL = "SELECT * FROM " & tablename & " WHERE g_idx = '" & g_idx & "'"
Set rs = dbconn.execute(strSQL)

If Not(rs.eof) Then
	g_name 			= rs("g_name")
	g_display 		= rs("g_display")
	g_act 			= rs("g_act")
	g_bestYN 		= rs("g_bestYN")
	g_keyword 		= rs("g_keyword")
	g_spec 			= rs("g_spec")
	g_intro 		= rs("g_intro")
	g_Money			= rs("g_Money")
	total_op		= rs("total_op")
	g_use_totalChk	= rs("g_use_totalChk")
	g_optionGroup	= rs("g_optionGroup")
	g_optionList	= rs("g_optionList")
	g_simg 			= rs("g_simg")
	g_bimg1 		= rs("g_bimg1")
	g_bimg2			= rs("g_bimg2")
	g_bimg3			= rs("g_bimg3")
	g_bimg4			= rs("g_bimg4")
	g_bimg5			= rs("g_bimg5")
	g_Memo 			= rs("g_Memo")
	g_video 		= rs("g_video")
	For j = 1 To 5
		g_menual(j-1) = rs("g_menual" & j)
	Next
	g_insertDay		= rs("g_insertDay")
End If
%>
			<script src="./form.js"></script>
			<script language="javascript">
			<!--
			function OpenWindow(url,w,h,scrollbar) {
				if (scrollbar=="") scrollbar=0;
				//window.open(url,"ICCwindow","width=400,height=400,status=no,scrollbars=no");
				lPos = (screen.width) ? (screen.width-w)/2 : 0; tPos = (screen.height) ? (screen.height-h)/2 : 0;

				window.open(url, "", "height="+h+", width="+w+", left="+lPos+", top="+tPos+", toolbar=0, location=0,directories=0,status=0,menuBar=0,scrollBars="+scrollbar+",resizable=0");
			}

			function moneyValue() {
				document.insert.g_cyberMoney.value = eval(document.insert.g_Money.value * 0.01)
			}
			//-->
			</script>
			<form action="product_exec.asp" method="post" enctype="multipart/form-data"  name="insert" id="insert">
			<input type="hidden" name="n" value="<%=g_idx%>">
			<input type="hidden" name="menucode" value="<%=menucode%>">
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>기본 정보  </h2>
					</header>
					<!-- widget div-->
					<div>
						<!-- widget content -->
						<div class="widget-body no-padding smart-form">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="width:35%">
									<col style="width:15%">
									<col style="width:35%">
								</colgroup>
								<tbody>
									<tr>
										<th>상품코드</th>
										<td colspan="3"><%=g_idx%></td>
									</tr>
									<tr>
										<th class="text-danger">카테고리</th>
										<td colspan="3">
											<iframe name="joincatelist" class="ib" src="/admin/goods/joinCateList.asp?g_idx=<%=g_idx%>" width="40%" height="60" style="float:left;"></iframe>
											<a href="javascript:OpenWindow('product_category.asp?g_idx=<%=g_idx%>','600','500','1');" class="btn btn-default"><i class="fa fa-sitemap"></i>카테고리등록</a>
										</td>
									</tr>
									<tr>
										<th>홈페이지/SRM 출력</th>
										<td>
											<label class="select col-3">
											<select class="input-sm"  name="g_display">
												<option value="home" <% If g_display = "home" Or g_display = "" Then %> selected<% End If %>>홈페이지</option>
												<option value="srm" <% If g_display = "srm" Then %> selected<% End If %>>SRM</option>
												<option value="both" <% If g_display = "both" Then %> selected<% End If %>>홈페이지 + SRM</option>
												<option value="server" <% If g_display = "server" Then %> selected<% End If %>>HP서버</option>
												<option value="workstation" <% If g_display = "workstation" Then %> selected<% End If %>>HP워크스테이션</option>
												<option value="desktop" <% If g_display = "desktop" Then %> selected<% End If %>>HP데스크탑</option>
											</select> <i></i> </label>
										</td>
										<th>진열여부</th>
										<td>
											<div class="inline-group">
												<label class="select col-3">
												<select class="input-sm"  name="g_act">
													<option value="Y" <% If g_act = "Y" Or g_act = "" Then %> selected<% End If %>>진열함</option>
													<option value="N" <% If g_act = "N" Then %> selected<% End If %>>진열안함</option>
												</select> <i></i> </label>
											</div>
										</td>
									</tr>
									<tr>
										<th>BEST Seller 여부</th>
										<td colspan="3">
											<label for="g_bestYN" class="checkbox" style="display:inline-block;">
												<input type="checkbox" name="g_bestYN" id="g_bestYN" value="Y" <% If g_bestYN = "Y" Then %> checked<% End If %>><i></i> 메인출력
											</label>
										</td>
									</tr>
									<tr></tr>
								</tbody>
							</table>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>


				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
						<h2>제품정보</h2>
					</header>
					<!-- widget div-->
					<div>
						<div class="widget-body no-padding smart-form">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>제품명(필수)</th>
										<td colspan="3">
											<label for="g_name" class="input">
												<input type="text" name="g_name" id="g_name" value="<%=g_name%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>검색어</th>
										<td colspan="3">
											<label for="g_name" class="input">
												<input type="text" name="g_keyword" id="g_keyword" value="<%=g_keyword%>">
											</label>
											"/" 로 구분하여 입력해 주세요. 상품검색시 확장 키워드로 사용됩니다
										</td>
									</tr>
									<tr>
										<th>주요특징</th>
										<td colspan="3">
											<label class="textarea">
												<textarea rows="3" name="g_spec" class="h150 pro_detail"><%=g_spec%></textarea>
											</label>
										</td>
									</tr>
									<tr>
										<th>제품개요</th>
										<td colspan="3">
											<label class="textarea">
												<textarea rows="3" name="g_intro" class="h150 pro_detail"><%=g_intro%></textarea>
											</label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>


				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
						<h2>가격정보</h2>
					</header>
					<!-- widget div-->
					<div>
						<div class="widget-body no-padding smart-form">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>제품가격(필수)</th>
										<td colspan="3">
											<label for="g_Money" class="input">
												<input type="text" name="g_Money" class ="w30" id="g_Money" onblur="moneyValue()" <%If g_idx <> "" Then%> value = "<%=(g_Money)%>"<%Else%>value = "0"<%End if%> onKeyUp="if(isNaN(this.value))this.value='';">
											</label>
										</td>
									</tr>
									<tr>
										<th>재고</th>
										<td colspan="3">
											<label for="total_op" class="input pro_detail mr10">
												<input type="text" name="total_op"  id="total_op"  value ="<%=total_op%>"  onkeyup="if(isNaN(this.value))this.value='';">
											</label>개

											<label class="checkbox" style="display:inline-block;">
											<input name="g_use_totalChk" value="Y" type="checkbox" <%if g_use_totalChk = "Y" or g_use_totalChk = true then %>checked<%End if%>>
											<i></i>재고제한없음 (재고체크를 하지 않고 상시 판매를 원하시는 경우 체크바랍니다.) </label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>


				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
						<h2>옵션설정</h2>
					</header>
					<!-- widget div-->
					<div>
						<div class="widget-body no-padding smart-form">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="width:35%">
									<col style="width:15%">
									<col style="width:35%">
								</colgroup>
								<tbody>
<%
	sql = "SELECT * FROM cate_option_group ORDER BY ogroup_idx DESC"
	Set rs = dbconn.execute(sql)
%>
									<tr>
										<th>옵션그룹선택</th>
										<td colspan="3">
											<label class="select col-3">
											<select class="input-sm"  name="opt_group" id="opt_group" onchange="optChange(this.value, '<%=g_optionList%>');">
												<option value="0">-- 선택 --</option>
<%
											If Not(rs.bof Or rs.eof) Then
												Do while Not rs.eof
%>
												<option value="<%=rs("ogroup_idx")%>" <% If g_optionGroup = rs("ogroup_idx") Then %> selected<% End If %>><%=rs("ogroup_name")%></option>
<%
												rs.MoveNext
												Loop
											End If

											rs.close
%>
											</select> <i></i> </label>
										</td>
									</tr>
							<table  id="editOpt" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="width:*">
								</colgroup>
								<tbody>
<%
	If g_optionList <> "" Then

		sql = "SELECT ogroup_content FROM cate_option_group WHERE ogroup_idx = " & g_optionGroup & ""

		Set rs = dbconn.execute(sql)

		If Not rs.eof Then
			ogroup_content = rs("ogroup_content")

			arr_ogroup_content = Split(ogroup_content, ", ")
		End If

		rs.close

		For i = 0 To Ubound(arr_ogroup_content)
			sql = "SELECT opt_name, opt_content FROM cate_option WHERE opt_name = '" & Trim(arr_ogroup_content(i)) & "'"
			Set rs = dbconn.execute(sql)

			If Not rs.eof Then
				opt_name = rs("opt_name")
				opt_content = rs("opt_content")

				arr_opt_content = Split(opt_content, ", ")
			End If

			rs.close

			arr_g_optionList1 = Split(g_optionList, ",")

			If Ubound(arr_g_optionList1) > 0 Then
				arr_g_optionList2 = Split(arr_g_optionList1(i), "||")
%>
									<tr>
										<th><%=opt_name%></th>
										<td>
											<label class="select col-3">
											<select class="input-sm"  name="g_optionList<%=i+1%>" id="g_optionList<%=i+1%>">
												<option value="">-- 선택 --</option>
										<% For j = 0 To Ubound(arr_opt_content) %>
												<option value="<%=opt_name%>||<%=arr_opt_content(j)%>" <% If arr_opt_content(j) = Trim(arr_g_optionList2(1)) Then %> selected<% End If %>><%=arr_opt_content(j)%></option>
										<% Next %>
											</select> <i></i> </label>
										</td>
									</tr>
<%
			End If
		Next
	End If
%>
								</tbody>
							</table>
								</tbody>
							</table>
							<!-- 상세옵션영역 -->
							<div id="layer01"></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>


				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-picture-o"></i> </span>
						<h2>제품이미지</h2>
					</header>
					<!-- widget div-->
					<div>
						<div class="widget-body no-padding smart-form">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="width:7%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>리스트 이미지(필수)</th>
									<% If g_simg <> "" Then %>
										<td><%=getImgSizeView(90,90,"goods/"&g_simg)%></td>
									<% Else %>
										<td></td>
									<% End If %>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="g_simg" type="file" name="g_simg" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%=g_simg%>">
												</div> (사이즈 : 180*240)
											</section>
											<!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a-->
										</td>
									</tr>
									<tr>
										<th>상세이미지1(필수)</th>
									<% If g_bimg1 <> "" Then %>
										<td><%=getImgSizeView(90,90,"goods/"&g_bimg1)%></td>
									<% Else %>
										<td></td>
									<% End If %>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="g_bimg1" type="file" name="g_bimg1" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%=g_bimg1%>">
												</div> (사이즈 : 350*400 이하)
											</section>
											<!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a-->
										</td>
									</tr>
									<tr>
										<th>상세이미지2</th>
									<% If g_bimg2 <> "" Then %>
										<td><%=getImgSizeView(90,90,"goods/"&g_bimg2)%></td>
									<% Else %>
										<td></td>
									<% End If %>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="g_bimg2" type="file" name="g_bimg2" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%=g_bimg2%>">
												</div> (사이즈 : 350*400 이하)
											</section>
											<!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a-->
										</td>
									</tr>
									<tr>
										<th>상세이미지3</th>
									<% If g_bimg3 <> "" Then %>
										<td><%=getImgSizeView(90,90,"goods/"&g_bimg3)%></td>
									<% Else %>
										<td></td>
									<% End If %>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="g_bimg3" type="file" name="g_bimg3" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%=g_bimg3%>">
												</div> (사이즈 : 350*400 이하)
											</section>
											<!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a-->
										</td>
									</tr>
									<tr>
										<th>상세이미지4</th>
									<% If g_bimg4 <> "" Then %>
										<td><%=getImgSizeView(90,90,"goods/"&g_bimg4)%></td>
									<% Else %>
										<td></td>
									<% End If %>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="g_bimg4" type="file" name="g_bimg4" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%=g_bimg4%>">
												</div> (사이즈 : 350*400 이하)
											</section>
											<!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a-->
										</td>
									</tr>
									<tr>
										<th>상세이미지5</th>
									<% If g_bimg5 <> "" Then %>
										<td><%=getImgSizeView(90,90,"goods/"&g_bimg5)%></td>
									<% Else %>
										<td></td>
									<% End If %>
										<td>
											<section>
												<div class="input input-file w50">
													<span class="button"><input id="g_bimg5" type="file" name="g_bimg5" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%=g_bimg5%>">
												</div> (사이즈 : 350*400 이하)
											</section>
											<!--a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a-->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget -->


				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-cog"></i> </span>
						<h2>제품 상세</h2>
					</header>
					<!-- widget div-->
					<div>
						<div class="widget-body no-padding smart-form">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>상세설명</th>
										<td colspan="2" class="h350"><%call Editor("g_Memo",g_Memo)%></td>
									</tr>
<%
sql = "SELECT b_idx, b_title FROM " & tablename2 & " WHERE b_part = 'board08' ORDER BY b_idx DESC"
Set rs = dbconn.execute(sql)
%>
									<tr>
										<th>제품영상</th>
										<td colspan="2" class="h350">
											<label class="select col-3">
											<select class="input-sm" name="g_video">
												<option value="">-- 선택 --</option>
<%
										If Not(rs.eof) Then
											Do Until rs.Eof
%>
												<option value="<%=rs("b_idx")%>" <% If g_video = rs("b_idx") Then %> selected<% End If %>><%=rs("b_title")%></option>
<%
												rs.MoveNext
											Loop
										End If
rs.close
%>
											</select> <i></i> </label>
										</td>
									</tr>
<%
For i = 1 To 5
%>
									<tr>
										<th>제품 매뉴얼<%=i%></th>
										<td colspan="2" class="h350">
											<label class="select col-3">
											<select class="input-sm"  name="g_menual<%=i%>">
												<option value="">-- 선택 --</option>
<%
	sql = "SELECT b_idx, b_title FROM " & tablename2 & " WHERE b_part = 'board07' ORDER BY b_idx DESC"
	Set rs = dbconn.execute(sql)

										If Not(rs.eof) Then
											Do Until rs.Eof
%>
												<option value="<%=rs("b_idx")%>" <% If g_menual(i-1) = rs("b_idx") Then %> selected<% End If %>><%=rs("b_title")%></option>
<%
												rs.MoveNext
											Loop
										End If
%>
											</select> <i></i> </label>
										</td>
									</tr>
<%
	rs.close
	Set rs = Nothing
Next
%>
								</tbody>
							</table>

							<footer>
								<input type="button" class="btn btn-default" onclick="location.href='./product.asp?menucode=<%=menucode%>'" value="리스트">
								<input type="button" class="btn btn-primary" onclick="checkform();return false;" value="수정">
								<input type="button" class="btn btn-default" onclick="location.href='product_delete.asp?menucode=<%=menucode%>" value="삭제">
							</footer>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
			</form>

		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>



		<%
		Call DbClose()
		%>


		<!-- #include file="../inc/footer.asp" -->