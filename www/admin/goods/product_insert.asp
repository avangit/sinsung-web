<% @CODEPAGE="65001" language="vbscript" %>
<% option explicit%>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!-- #include file="config_product.asp" -->
<%
dim insert

call dbOpen()

set insert = new DextQueryClass
call insert.setUploadFolder(UploadFolder)
call insert.setDefaultData("g_name")

'// 추가되는 데이타 담기
call insert.setdataAdd("g_insertDay ", date())
call insert.setdataAdd("g_keyword", insert.getFormValue("g_keyword"))
call insert.setdataAdd("g_act", "Y")
call insert.setdataAdd("g_type", "1")
'call insert.setdataAdd("g_specialMemo", insert.getFormValue("g_specialMemo"))
'call insert.setdataAdd("g_shortMemo", insert.getFormValue("g_shortMemo"))
'call insert.setdataAdd("g_icon", " ")
'call insert.setdataAdd("g_mainOption", " ")


'// 테이블 지정
call insert.settablename(tablename)



'// 디비에 적용
sql = insert.getqueryinsert

'// 저장후 시퀀스값 반환
dim intInsertIdx : intInsertIdx = getAdoExecuteID(sql)

call dbClose()

set insert = nothing

call jsAlertMsgUrl("제품의 상세정보를 입력해 주세요.", "product_view.asp?g_idx="&intInsertIdx&"&menucode="&menucode&"&module=goods")

%>