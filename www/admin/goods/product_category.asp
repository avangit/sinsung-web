<!-- #include file="../inc/head.asp" -->

	<body class="">


		<!-- MAIN PANEL -->
		<div id="main" role="main">



			<!-- MAIN CONTENT -->
			<div id="content">
				<%
					dim tablename
					tablename = "Category"

					dim tablename_join
					tablename_join = "GOODS_CATE_JOIN"

				%>
				<% call dbopen %>


				<script type="text/JavaScript">
				<!--
				function MM_jumpMenu(targ,selObj,restore){ //v3.0
				  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
				  if (restore) selObj.selectedIndex=0;
				}
				//-->
				</script>



				<%
					'response.Write getCateStep(CateCode)
					dim i, sql_list, rs_list, temp_selected, g_idx

					g_idx = request.querystring("g_idx")
					'for i = 0 to getCateStep(CateCode)
				%>

						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-table"></i> </span>
								<h2>제품카테고리</h2>

							</header>

							<!-- widget div-->
							<div>


								<!-- widget content -->
								<div class="widget-body no-padding">
									<table id="datatable_fixed_column" class="table table-striped table-bordered smart-form" width="100%">
										 <colgroup>
											<col style="width:15%">
											<col style="">
										</colgroup>
										<thead>
											<tr>
												<th data-class="expand">STEP</th>
												<th>카테고리</th>
											</tr>
										</thead>

										<tbody>
											<%
												dim TempcateStep '// 4이상인 경우 무시 4단계때 5단계를 생성하기 때문
												TempcateStep = getCateStep(CateCode)
												if getCateStep(CateCode) > 4 then TempcateStep = 4
												i = 0
												for i = 0 to TempcateStep



													sql_list = "select * from " & tablename & " where "& CateThisWhere(i,catecode()) &" order by c_sunse asc "
														'response.Write(sql_list)
													set rs_list = dbconn.execute(sql_list)

														if rs_list.eof then



														else

											%>
											<form name="form1" id="form1">
											<tr>
												<td><%=i+1%>단계</td>
												<td>
													<label class="select state-disabled col-3">
													<select class="input-sm"  name="menu1" onchange="MM_jumpMenu('parent',this,0)">
														<option value="?<%=getString("")%>"><%=i+1%>차 카테고리 선택</option>
														<%
															do while not rs_list.eof
															'// 각 단계의 값을 리턴(넘어온 카테코드와 디비의 코드가 같으면 셀렉티드)
															temp_selected = chk_selected( upCodeReturn(i+1, cateCode() ), upCodeReturn(i+1, rs_list("c_code") ) )
														%>
															<option value="?<%=getString("category="&rs_list("c_code"))%>" <%=temp_selected%> ><%=rs_list("c_name")%></option><%'="[Code:"&rs_list("c_code")&"]"%>
														<%
															rs_list.movenext
															loop

														%>
													</select> <i></i> </label>
												</td>
											</tr>
											</form>
											<%
												end if
											next
											%>



										</tbody>

									</table>
									<div class="alert alert-block alert-info">
										<h4 class="alert-heading">선택 카테고리</h4>

										<form action="cate_insert.asp?<%=getString("g_idx="&g_idx)%>" method="post" enctype="multipart/form-data" name="gstejoin" id="gstejoin">
										메인 <%=getCateColPrint(request.QueryString("category"), "CATEGORY")%> <br>
										코드: <%=cateCode%>
										<br>
										<input type="submit" class="btn btn-default" onclick="this.form.onsubmit();" value="작성하기"></button>
										<input type="hidden" name="c_code"  value="<%=cateCode%>"/>

										<input name="colsed" type="button" class="btn btn-default" id="colsed" value="닫기" onclick="window.opener.joincatelist.location.reload();self.close()" />
										</form>
									</div>

								</div>
								<!-- end widget content -->

							</div>
							<!-- end widget div -->

						</div>
						<!-- end widget -->


			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->


		<!--================================================== -->


	</body>

</html>