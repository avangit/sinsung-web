// JavaScript Document
function checkform() {
	if(document.insert.g_name.value == "") {
		alert("제품명을 입력해 주세요");
		document.insert.g_name.focus();
		return false;
	} else if(document.insert.g_Money.value == "") {
		alert("제품가격을 입력해 주세요");
		document.insert.g_Money.focus();
	} else if(document.insert.g_simg.value == "") {
		alert("리스트 이미지를 삽입해 주세요");
	} else if(document.insert.g_bimg1.value == "") {
		alert("상세 이미지1을 삽입해 주세요");
	} else {
		document.insert.submit();
	}
}

function regist(frm){
	if ( frm.keyword.value.replace(/ /gi, "") == "" ) { alert("상품명을 입력해주세요"); frm.keyword.focus(); return false; }		

}

//$(document).on('ready', function() {
//	$("#opt_group").change(function () {
//		$("#editOpt").css('display', '');
//		$.ajax({
//			type: "post", url : "opt_group_Ajax.asp", data: "val=" + $(this).val(), success: function(resdata){
//				$("#layer01").html(resdata);
//			}
//		});
//	});
//});

// 체크박스 전체선택
var check = 1;
function checkAll(frm) {
	if (check) {
		for (i = 0; i < frm.length; i++) {
			if (frm[i].type == "checkbox") {
				if (frm[i].checked)
					continue;
				else
					frm[i].checked = true;
			}
		}
		check = 0;
	} else {
		for (i = 0; i < frm.length; i++) {
			if (frm[i].type == "checkbox") {
				if(frm[i].checked) frm[i].checked = false;
				else continue;
			}
		}
		check = 1;
	}
	return false;
}

// 체크박스 값 넘기기
function GetCheckbox(frm, mod, message) {
	var tmp = "";
	var cnt = 0;
	var i;
	var idk = false;

	for (i = 0; i < frm.length; i++) {
		if (frm[i].type != "checkbox")
			continue;
		if (frm[i].checked) {
			tmp += frm[i].value + " ";
			cnt = cnt + 1;
			idk = true;
		}
	}

	if (idk == true) {
		if (mod.indexOf("del") != -1) {
			var cfm = confirm("선택하신 내용을 삭제하시겠습니까?");
		} else if (mod == "sec") {
			var cfm = confirm("선택하신 회원을 탈퇴시키겠습니까?");
		} else if (mod == "copy") {
			if(cnt > 1) {
				alert('복사할 대상은 하나만 선택해 주세요.');
			} else {
				var cfm = confirm("선택하신 내용을 복사하시겠습니까?");
			}
		} else if (mod.substr(0,4) == "dsp-") {
			var cfm = confirm("선택하신 내용의 상태를 변경하시겠습니까?");
		} else {
			if (message != null) {
				var cfm = confirm("선택하신 내용을 '" + message + "' 하시겠습니까?");
			} else {
				var cfm = confirm("선택하신 내용의 상태를 변경하시겠습니까?");
			}
		}

		if (cfm) {
			frm.n.value = tmp.substr(0, tmp.length - 1);
			frm.m.value = mod;
			frm.message.value = message;
			frm.submit();
		} else {
			return;
		}
	} else {
		window.alert('실행할 데이터를 선택하세요');
		return;
	}
	return false;
}

function optChange(val, opt){
	$("#editOpt").css('display', 'none');
	$("#g_optionList1").val('');
	$("#g_optionList2").val('');
	$("#g_optionList3").val('');
	$("#g_optionList4").val('');
	$("#g_optionList5").val('');
	$("#g_optionList6").val('');
	$("#g_optionList7").val('');
	$("#g_optionList8").val('');
	$("#g_optionList9").val('');
	$("#g_optionList10").val('');
	$("#g_optionList11").val('');
	$("#g_optionList12").val('');
	$("#g_optionList13").val('');
	$("#g_optionList14").val('');
	$("#g_optionList15").val('');
	$("#g_optionList16").val('');
	$("#g_optionList17").val('');
	$("#g_optionList18").val('');
	$("#g_optionList19").val('');
	$("#g_optionList20").val('');

	$.ajax({
		type: "post", url : "opt_group_Ajax.asp", data: "val=" + val + "&opt=" + escape(opt), success: function(resdata){
			$("#layer01").html(resdata);
		}
	});
}

$(document).ready(function() {
	$("#srmPrice_chg").click(function(){
		var frm = document.list;

		 var len = parseInt($("input[name^=mem_price]").length);
		 $("input[name=arr_val_list]").val("");

		 if(!len || len==0) {
			alert('데이터가 없습니다.');
		 } else {
			var arr_val_list = new Array();
			var j = 0;

			$("input[name^=mem_price]").each(function(i){
				arr_val_list[i] = $(this).attr("rel");
				j++;
			});

			$("input[name=arr_val_list]").val(arr_val_list.join('@'));

			if(!j || j==0) {
				$("input[name=arr_val_list]").val("");
			} else {
				document.list.submit();
			}
		 }
	});
});

function srm_member_search(idx){
	var frm	=	document.frm;
	var company = $("#cName"+idx).text();

	$(opener.document).find("input[name=company]").val(company);
	$(opener.document).find("input[name=code]").val(idx);

	window.close();

}