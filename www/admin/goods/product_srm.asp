		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config_product.asp" -->

			<!-- NEW WIDGET START -->
				<!-- Widget ID (each widget will need unique ID)-->
				<script src="./form.js"></script>
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>SRM 설정</h2>
					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->
<%
g_idx = Request.QueryString("g_idx")

Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request.QueryString("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= "*"
Dim query_Tablename		: query_Tablename	= "GOODS_SRM_JOIN"
Dim query_where			: query_where		= " g_idx = " & g_idx
Dim query_orderby		: query_orderby		= " ORDER BY srm_idx DESC"

sql = "SELECT g_name FROM GOODS WHERE g_idx = " & g_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	g_name = rs("g_name")
End If

rs.close

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))
%>
						<!-- widget content -->
						<div class="widget-body no-padding  smart-form ">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h search_t" width="100%">
								<colgroup>
									<col style="width:15%">
									<col style="width:*">
								</colgroup>
								<tbody>
									<tr>
										<th>상품 카테고리</th>
										<td>
<%
									sql = "SELECT * FROM GOODS_CATE_JOIN WHERE g_idx = '"& g_idx &"' ORDER BY gcj_idx DESC"
									Set rs = dbconn.execute(sql)

									If Not rs.eof Then
										Do While Not rs.eof
%>
											<%="메인 " & getCateColPrint(rs("c_code"), "CATEGORY")%>
											<!--iframe name="joincatelist" class="ib" src="/admin/goods/joinCateList.asp?g_idx=<%=g_idx%>" width="40%" height="60" style="float:left;"></iframe-->
<%
											rs.movenext
										Loop
									End If

									rs.close
%>
										</td>
									</tr>
									<tr>
										<th>제품명</th>
										<td><%=g_name%></td>
									</tr>
									<tr>
										<th>제품코드</th>
										<td><%=g_idx%></td>
									</tr>
								</tbody>
							</table>

							<footer style="float:right;" class="">
								<a href="javascript:;" class="btn btn-primary" onclick="window.open('./product_srm_add.asp?g_idx=<%=g_idx%>', '_blank', 'toolbar=no,scrollbars=no,resizable=no,top=10,left=10,width=1150,height=740');"><i class="fa fa-pencil"></i> 추가</a>
							</footer>

							<h6><b><%=intTotalCount%>건</b></h6>
							<form name="list" method="post" action="./product_srm_exec.asp" style="margin:0;">
							<input type="hidden" name="arr_val_list">
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<input type="hidden" name="g_idx" value="<%=g_idx%>">
							<input type="hidden" name="m" value="upt">
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								<colgroup>
									<col style="width:5%">
									<col style="width:10%">
									<col style="width:10%">
									<col style="width:10%">
									<col style="width:5%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand">번호</th>
										<th>회사명</th>
										<th data-hide="phone,tablet">회원가격</th>
										<th data-hide="phone,tablet">메인 출력여부</th>
										<th data-hide="phone,tablet">관리</th>
									</tr>
								</thead>

								<tbody>
<%
sql = "SELECT a.srm_idx, a.mem_price, a.mainYN, a.mem_idx, b.cName FROM GOODS_SRM_JOIN a LEFT JOIN mTb_Member2 b ON a.mem_idx = b.intSeq WHERE a.g_idx = " & g_idx & ""
Set rs = dbconn.execute(sql)
	If rs.eof Then
%>
									<tr><td colspan="5" align="center">데이터가 없습니다.</td></tr>
<%
	Else
		Do While Not rs.eof
%>
									<tr>
										<td><%=intNowNum%></td>
										<td><%=rs("cName")%></td>
										<td>
											<input type="text" name="mem_price<%=rs("mem_idx")%>" value="<%=rs("mem_price")%>" rel="<%=rs("mem_idx")%>"> 원
										</td>
										<td>
											<label class="select col-3">
											<select name="mainYN<%=rs("mem_idx")%>" rel="<%=rs("mem_idx")%>">
												<option value="Y" <% If rs("mainYN") = "Y" Or rs("mainYN") = "" Then %> selected<% End If %>>출력</option>
												<option value="N" <% If rs("mainYN") = "N" Then %> selected<% End If %>>미출력</option>
											</select><i></i>
											</label>
										</td>
										<td><a href="product_srm_exec_get.asp?menucode=<%=menucode%>&n=<%=rs("srm_idx")%>&m=del&g_idx=<%=g_idx%>" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a></td>
									</tr>
<%
			intNowNum = intNowNum - 1
			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing
%>
								</tbody>
							</table>

							<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:15px;margin-bottom:15px;">
								<tr>
									<td align="center">
										<a href="./product.asp?menucode=<%=menucode%>&module=goods" class="btn btn-default"><i class="fa fa-close"></i> 취소</a>
										<a href="javascript:;" class="btn btn-primary" id="srmPrice_chg"><i class="fa fa-pencil"></i> 저장</a>
									</td>
								</tr>
							</table>
							</form>
						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				<script>
					$(document).ready(function() {

						// DO NOT REMOVE : GLOBAL FUNCTIONS!
						pageSetUp();

					});

				</script>

		<!-- #include file="../inc/footer.asp" -->