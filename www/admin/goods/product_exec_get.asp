<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config_product.asp" -->

<%
ReDim rsFile(4)

'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.DefaultPath = Server.MapPath("/upload/goods/")

g_idx		= SQL_Injection(Trim(Request.QueryString("n")))
mode		= SQL_Injection(Trim(Request.QueryString("m")))
message		= SQL_Injection(Trim(Request.QueryString("message")))
menucode	= SQL_Injection(Trim(Request.QueryString("menucode")))
page		= SQL_Injection(Trim(Request.QueryString("page")))

If g_idx = "" Or ISNULL(g_idx) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","history.back();")
Else
	g_idx = Replace(g_idx, " ", ",")
End If

href = "product.asp?menucode="&menucode&"&page=" & page

Call dbOpen()

'// 삭제
If mode = "del" Then
	message = "삭제"

	sql = "SELECT g_simg, g_bimg1, g_bimg2, g_bimg3, g_bimg4, g_bimg5 FROM " & tablename & " WHERE g_idx IN (" & g_idx & ")"
	Set rs = dbconn.execute(sql)

	'파일 삭제
	If rs("g_simg") <> "" Then
		UploadForm.Deletefile UploadForm.DefaultPath & "\" & rs("g_simg")
	End If

	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof

			For i = 1 To 5
				rsFile(i-1) = rs("g_bimg" & i)

				If rsFile(i-1) <> "" Then
					UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile(i-1)
				End If
			Next

			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing

	dbconn.execute("DELETE FROM " & tablename & " WHERE g_idx IN (" & g_idx & ");DELETE FROM GOODS_CATE_JOIN WHERE g_idx IN (" & g_idx & ");")

'// 복사
ElseIf mode = "copy" Then

	sql = "SELECT MAX(g_idx) FROM " & tablename & ""
	Set rs = dbconn.execute(sql)

	maxIdx = rs(0) + 1

	rs.close

	'// 상품복사
	sql = "SET NOCOUNT ON;SET IDENTITY_INSERT " & tablename & " ON;INSERT INTO " & tablename & " (g_idx, g_name, g_type, g_display, g_act, g_bestYN, g_keyword, g_spec, g_intro, g_Money, total_op, g_use_totalChk, g_optionGroup, g_optionList, g_Memo, g_video, g_menual1, g_menual2, g_menual3, g_menual4, g_menual5) SELECT '" & maxIdx & "', g_name, g_type, g_display, g_act, g_bestYN, g_keyword, g_spec, g_intro, g_Money, total_op, g_use_totalChk, g_optionGroup, g_optionList, g_Memo, g_video, g_menual1, g_menual2, g_menual3, g_menual4, g_menual5 FROM " & tablename & " WHERE g_idx = " & g_idx & ";SELECT SCOPE_IDENTITY();SET IDENTITY_INSERT " & tablename & " OFF;SET NOCOUNT OFF;" ', g_simg, g_bimg1, g_bimg2, g_bimg3, g_bimg4, g_bimg5 이미지는 복사한 제품의 이미지 수정 시 원본 이미지가 삭제되는 현상으로 복사하지 않음
	Set rs = dbconn.execute(sql)

	idx = rs(0)

	rs.close

	sql = "SELECT * FROM GOODS_CATE_JOIN WHERE g_idx = " & g_idx & ""
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		sql = "SELECT MAX(gcj_idx) FROM GOODS_CATE_JOIN"
		Set rs2 = dbconn.execute(sql)

		maxCate = rs2(0) + 1

		rs2.close
		Set rs = Nothing

		'// 카테고리 복사
		dbconn.execute("SET IDENTITY_INSERT GOODS_CATE_JOIN ON;INSERT INTO GOODS_CATE_JOIN (gcj_idx, g_idx, c_code) SELECT '" & maxCate & "', '" & idx & "', c_code FROM GOODS_CATE_JOIN WHERE g_idx = " & g_idx & ";SET IDENTITY_INSERT GOODS_CATE_JOIN OFF;")
	End If

	Sql = "UPDATE " & tablename & " SET "
	Sql = Sql & "g_name = N'복사본_' + g_name "
	Sql = Sql & " WHERE g_idx = '" & idx & "'"

	dbconn.execute(Sql)

'// 상태변경
ElseIf Left(mode, 4) = "dsp-" Then
	message = "상태변경"

	g_display = Trim(Replace(mode, "dsp-", ""))

	Sql = "UPDATE " & tablename & " SET "
	Sql = Sql & "g_display = '" & g_display & "' "
	Sql = Sql & " WHERE g_idx IN (" & g_idx & ")"

	dbconn.execute(Sql)

Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("" & message & " 되었습니다.", href)
%>