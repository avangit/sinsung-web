		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config_product.asp" -->

			<!-- NEW WIDGET START -->
				<!-- Widget ID (each widget will need unique ID)-->
				<script src="./form.js"></script>
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>상품관리 </h2>
					</header>
<%
	Call dbopen

	code = Request("code")

	If code <> "" Then
		param = "&code="&code

		sql = "SELECT cName FROM mTb_Member2 WHERE intSeq = " & code
		Set rs = dbconn.execute(sql)

		If Not rs.eof Then
			company = rs("cName")
		End If

		rs.close
	End If

	Dim tempWhere : tempWhere = " GOODS_SRM_JOIN.mem_idx > 0 "
	Dim tempTable : tempTable = "goods,GOODS_SRM_JOIN,mTb_Member2"

	tempWhere = tempWhere & " AND goods.g_idx = GOODS_SRM_JOIN.g_idx AND GOODS_SRM_JOIN.mem_idx = mTb_Member2.intSeq"

	if int(catecode()) > 0 then
		'//카테고리 하위 조인식
		tempWhere = tempWhere & " and "&CateAllWhere(cateCode()) & " and  goods.g_idx = Goods_cate_join.g_idx "
		tempTable = tempTable&",Goods_cate_join "
	else
		tempTable = tempTable
	end If

	'// 검색 조건 추가
	Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(RequestS("fieldname")))
	Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(RequestS("fieldvalue")))

	mainYN		= requestS("mainYN")

	If mainYN <> "" Then
		tempWhere = tempWhere & " AND ( GOODS_SRM_JOIN.mainYN = '"& mainYN &"') "
	End If

	If code <> "" Then
		tempWhere = tempWhere & " AND ( GOODS_SRM_JOIN.mem_idx = '"& code &"') "
	End If

	If Len(fieldvalue) > 0 Then
		tempWhere = tempWhere &" AND (goods."& fieldname & " LIKE '%" & fieldvalue & "%') "
	End If

	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'##
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'##
	dim intTotalCount, intTotalPage

	dim intNowPage			: intNowPage 		= Request.QueryString("page")
	dim intPageSize			: intPageSize 		= 10
	dim intBlockPage		: intBlockPage 		= 10

	dim query_filde			: query_filde		= " Goods.g_idx, g_name, g_Money, g_simg, GOODS_SRM_JOIN.srm_idx, GOODS_SRM_JOIN.mem_idx, GOODS_SRM_JOIN.mem_price, GOODS_SRM_JOIN.mainYN"
	dim query_Tablename		: query_Tablename	= tempTable
	dim query_where			: query_where		= tempWhere
	dim query_orderby		: query_orderby		= " ORDER BY GOODS_SRM_JOIN.srm_idx DESC"

	Call intTotal

	Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'##
	'##
	'#############################################################################


	if trim(query_where) <> "" then
		sql = "select distinct  "& TopCount & " " & query_filde &" from "& query_tablename & " where "& query_where & " " & query_orderby
	else
		sql = "select distinct  "& TopCount & " " & query_filde &" from "& query_tablename  & " " & query_orderby
	end if
	'End Function
	'#############################################################################

	sql = getQuery

	Set rs = dbconn.execute(sql)
'		response.Write(sql)
%>
					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding  smart-form ">
							<form name="frm" id="frm" action="./product_srm_member.asp?menucode=<%=menucode%>" class="smart-form m10"  method="post" >
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h search_t" width="100%">
								<colgroup>
									<col style="width:15%">
									<col style="width:35%">
									<col style="width:15%">
									<col style="width:35%">
								</colgroup>
								<tbody>
									<tr>
										<th>카테고리</th>
										<td colspan="3">
											<% Call navi_select(request.QueryString("category"), "CATEGORY") %>
										</td>
									</tr>
									<tr>
										<th>회원선택</th>
										<td>
											<input type="hidden" name="code" value="<%=rs("mem_idx")%>">
											<input type="text" name="company" value="<%=company%>" style="width:230px" readonly>
											<a href="javascript:;" class="btn btn-primary" onclick="window.open('./product_srm_mem_search.asp', '_blank', 'toolbar=no,scrollbars=no,resizable=no,top=10,left=10,width=1150,height=740');"><i class="fa fa-pencil"></i> 선택</a>
										</td>
										<th>메인출력</th>
										<td>
											<label class="select w20 pro_detail">
											<select class="input-sm h34" name="mainYN" id="mainYN" >
<!--												<option value="">전체</option>-->
												<option value="Y" <% If mainYN = "Y" Then %> selected<% End If %>>출력</option>
												<option value="N" <% If mainYN = "N" Then %> selected<% End If %>>미출력</option>
											</select> <i></i> </label>
										</td>
									</tr>
									<tr>
										<th>검색</th>
										<td colspan="3">
											<label class="select w10 pro_detail">
											<select class="input-sm h34" name="fieldname" id="fieldname" >
												<option value="g_name" <% If fieldname = "g_name" Then %> selected<% End If %>>상품명</option>
												<option value="g_idx" <% If fieldname = "g_idx" Then %> selected<% End If %>>상품코드</option>
											</select> <i></i> </label>
											<div class="icon-addon addon-md col-md-10 col-2">
												<input type="text" name="fieldvalue" class="form-control col-10" id="fieldvalue" value="<%=fieldvalue%>" >
												<label for="email" class="glyphicon glyphicon-search " rel="tooltip" title="email"></label>
											</div>

											<input type="button" class="btn btn-primary" value="검색" onclick="document.frm.submit();">
											<input type="button" class="btn btn-default" value="검색 초기화" onclick="location.href='./product_srm_member.asp?menucode=<%=menucode%>';">
										</td>
									</tr>
								</tbody>
							</table>
							</form>

							<h5><b><%=intTotalCount%>건</h5></b>
							<form name="list" method="post" action="./product_srm_mem_exec.asp" style="margin:0;">
							<input type="hidden" name="arr_val_list" value="">
							<input type="hidden" name="n">
							<input type="hidden" name="m">
							<input type="hidden" name="message">
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<input type="hidden" name="page" value="<%=intNowPage%>">
							<input type="hidden" name="m_idx" value="<%=code%>">
							<input type="hidden" name="param" value="<%=param%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								<colgroup>
									<col style="width:5%">
									<col style="width:5%">
									<col style="width:*">
									<col style="10%">
									<col style="5%">
									<col style="5%">
									<col style="width:10%">
									<col style="width:15%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
										<th data-class="expand">번호</th>
										<th>제품명</th>
										<th data-hide="phone,tablet">제품가격</th>
										<th data-hide="phone,tablet">회원가격</th>
										<th data-hide="phone,tablet">메인 출력여부</th>
									</tr>
								</thead>

								<tbody>
<%
							If rs.eof Then
%>
									<tr><td colspan="6" align="center">데이터가 없습니다.</td></tr>
<%							Else
								rs.move movecount
								Do while not rs.eof
									g_idx = rs("g_idx")
									g_name = rs("g_name")
									g_Money = rs("g_Money")
									g_simg = rs("g_simg")
									srm_idx = rs("srm_idx")
									mem_price = rs("mem_price")
									g_mainYN = rs("mainYN")
%>
									<tr>
										<td><input type="checkbox" value="<%=srm_idx%>"></td>
										<td><%=intNowNum%></td>
										<td>
										<% If g_simg <> "" Then %>
											<span class="pro_detail mr10"><%=getImgSizeView(70,80,"goods/" & g_simg)%></span>
										<% End If %>
											<span class="pro_detail">
												<%=g_name%> <br>
												제품코드 : <span class="text-danger"><%=g_idx%></span>
											</span>
										</td>
										<td><%=FormatNumber(g_Money, 0)%>원</td>
										<td><input type="text" name="mem_price<%=srm_idx%>" style="width:150px" value="<%=mem_price%>" rel="<%=srm_idx%>"> 원</td>
										<td>
											<label class="select w50 pro_detail">
											<select class="input-sm h34" name="g_mainYN<%=srm_idx%>" rel="<%=srm_idx%>" >
<!--												<option value="">전체</option>-->
												<option value="Y" <% If g_mainYN = "Y" Then %> selected<% End If %>>출력</option>
												<option value="N" <% If g_mainYN = "N" Then %> selected<% End If %>>미출력</option>
											</select> <i></i> </label>
										</td>
										</td>
									</tr>
<%
									intNowNum = intNowNum - 1
									rs.MoveNext
								Loop
							End If

							rs.close
							Set rs = Nothing

							Call DbClose()
%>
								</tbody>
							</table>

							<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:15px;">
								<tr>
									<td align="left">
										<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
									</td>
								</tr>
								<tr>
									<td align="center">
										<a href="javascript:;" class="btn btn-primary" id="srmPrice_chg"><i class="fa fa-pencil"></i> 저장</a>
									</td>
								</tr>
							</table>
							</form>

							<%call Paging_list("")%>
						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				<script>
					$(document).ready(function() {

						// DO NOT REMOVE : GLOBAL FUNCTIONS!
						pageSetUp();

					});

				</script>

		<!-- #include file="../inc/footer.asp" -->