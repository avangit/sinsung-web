<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config_product.asp" -->

<%
g_idx			= SQL_Injection(Trim(Request.Form("g_idx")))
m_idx			= SQL_Injection(Trim(Request.Form("n")))
mode			= SQL_Injection(Trim(Request.Form("m")))
menucode		= SQL_Injection(Trim(Request.Form("menucode")))
arr_val_list	= SQL_Injection(Trim(Request.Form("arr_val_list")))

arr_idx = Split(arr_val_list, "@")

If m_idx <> "" Then
	m_idx = Replace(m_idx, " ", ",")
	arr_m_idx = Split(m_idx, ",")
End If

Call dbOpen()

If mode = "add" Then
	message = "추가"

	For i = 0 To Ubound(arr_m_idx)
		sql = "INSERT INTO GOODS_SRM_JOIN ("
		sql = sql & "g_idx, "
		sql = sql & "mem_idx) VALUES("
		sql = sql & "'" & g_idx & "',"
		sql = sql & "'" & arr_m_idx(i) & "')"

		dbconn.execute(Sql)
	Next
ElseIf mode = "upt" Then
	message = "수정"

	If Ubound(arr_idx) > 0 Then
		For i = 0 To Ubound(arr_idx)
			sql = "UPDATE GOODS_SRM_JOIN SET "
			sql = sql & "mem_price='" & SQL_Injection(Trim(Request.Form("mem_price" & arr_idx(i)))) & "', "
			sql = sql & "mainYN = '" & SQL_Injection(Trim(Request.Form("mainYN" & arr_idx(i)))) & "' "
			sql = sql & " WHERE g_idx = " & g_idx & " AND mem_idx = " & arr_idx(i) & ""

			dbconn.execute(Sql)
		Next
	Else
		sql = "UPDATE GOODS_SRM_JOIN SET "
		sql = sql & "mem_price='" & SQL_Injection(Trim(Request.Form("mem_price" & arr_val_list))) & "', "
		sql = sql & "mainYN = '" & SQL_Injection(Trim(Request.Form("mainYN" & arr_val_list))) & "' "
		sql = sql & " WHERE g_idx = " & g_idx & " AND mem_idx = " & arr_val_list & ""

		dbconn.execute(Sql)
	End If
End If

Call dbClose()

If mode = "add" Then
%>
	<script>
		alert("추가되었습니다.");
		opener.location.reload();
		window.close();
	</script>
<%
Else
	Call jsAlertMsgUrl("" & message & "되었습니다.", "product_srm.asp?menucode="&menucode&"&g_idx="&g_idx)
End If
%>