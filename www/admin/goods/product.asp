		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config_product.asp" -->

			<!-- NEW WIDGET START -->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>상품관리 </h2>
					</header>
<%
	Call dbopen


	If exeGoodView() <> 1 Then
		Dim tempWhere : tempWhere = " goods.g_type = 1 "
		Dim tempTable : tempTable = "goods"

		if int(catecode()) > 0 then
			'//카테고리 하위 조인식
			tempWhere = tempWhere & " and "&CateAllWhere(cateCode()) & " and  goods.g_idx = Goods_cate_join.g_idx "
			tempTable = tablename&",Goods_cate_join "
		else
			tempTable = tablename
		end If

		'// 검색 조건 추가
		Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(RequestS("fieldname")))
		Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(RequestS("fieldvalue")))

		RQ_act			= requestS("g_act")
		RQ_use_totalChk = requestS("g_use_totalChk")
		RQ_total_op		= requestS("total_op")
		RQ_display		= requestS("g_display")
		orderby			= requestS("orderby")

		If RQ_act <> "" Then
			tempWhere = tempWhere & " and ( g_act = '"& RQ_act &"') "
		End If

		If RQ_use_totalChk = "Y" Then
			tempWhere = tempWhere & " and ( g_use_totalChk = '"& RQ_use_totalChk &"') "
		End If

		If RQ_total_op <> "" Then
			tempWhere = tempWhere & " and ( total_op <= " & RQ_total_op & ") "
		End If

		If RQ_display <> "" Then
			tempWhere = tempWhere & " and ( g_display = '"& RQ_display &"') "
		End If

		If Len(fieldvalue) > 0 Then
			tempWhere = tempWhere &" AND ("& fieldname & " LIKE '%" & fieldvalue & "%') "
		End If


		'##==========================================================================
		'##
		'##	페이징 관련 함수 - 게시판등...
		'##
		'##==========================================================================
		'##
		'## [설정법]
		'## 다음 코드가 상단에 위치 하여야 함
		'##
		dim intTotalCount, intTotalPage

		dim intNowPage			: intNowPage 		= Request.QueryString("page")
		dim intPageSize			: intPageSize 		= 10
		dim intBlockPage		: intBlockPage 		= 10

		dim query_filde			: query_filde		= " Goods.g_idx, g_name, g_display, g_act, g_simg, g_insertDay, g_Money, total_op, g_use_totalChk, g_read"
		dim query_Tablename		: query_Tablename	= tempTable
		dim query_where			: query_where		= tempWhere
		dim query_orderby		: query_orderby		= " Order by goods.g_idx desc"
		call intTotal

		'##
		'## 1. intTotal : call intTotal
		'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
		'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
		'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
		'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
		'##
		'##
		'#############################################################################

		If orderby <> "" Then
			If orderby = "g_Money_asc" Then
				orderby_txt = "ORDER BY Goods.g_Money ASC"
			ElseIf orderby = "g_Money_desc" Then
				orderby_txt = "ORDER BY Goods.g_Money DESC"
			Else
				orderby_txt = "ORDER BY Goods." & orderby & " DESC"
			End If
			query_orderby = orderby_txt
		End If

		'Function GetQuery()

		if trim(query_where) <> "" then
			sql = "select distinct  "& TopCount & " " & query_filde &" from "& query_tablename & " where "& query_where & " " & query_orderby
		else
			sql = "select distinct  "& TopCount & " " & query_filde &" from "& query_tablename  & " " & query_orderby
		end if
		'End Function
		'#############################################################################

		'sql = GetQuery()
'		response.Write(sql)

		set rs = dbconn.execute(sql)
%>
					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding  smart-form ">
							<form name = "frm" id="frm" action = "" class="smart-form m10"  method="post" >
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h search_t" width="100%">
								<colgroup>
									<col style="width:15%">
									<col style="width:35%">
									<col style="width:15%">
									<col style="width:35%">
								</colgroup>
								<tbody>
									<tr>
										<th>카테고리</th>
										<td colspan="3">
											<% Call navi_select(request.QueryString("category"), "category") %>
										</td>
									</tr>
									<tr>
										<th>진열여부</th>
										<td>
											<label class="select w20 pro_detail">
											<select class="input-sm h34" name="g_act" id="g_act" >
												<option value="">전체</option>
												<option value="Y" <% If RQ_act = "Y" Then %> selected<% End If %>>진열함</option>
												<option value="N" <% If RQ_act = "N" Then %> selected<% End If %>>진열안함</option>
											</select> <i></i> </label>
										</td>
										<th>재고여부</th>
										<td>
											<label class="select w20 pro_detail">
											<select class="input-sm h34" name="g_use_totalChk" id="g_use_totalChk" >
												<option value="">재고여부</option>
												<option value="Y" <% If RQ_use_totalChk = "Y" Then %> selected<% End If %>>재고제한없음</option>
											</select> <i></i> </label>
											<div class="icon-addon addon-md col-md-10 col-5">
												<input type="text" name="total_op" id="total_op" class="form-control col-5" value="<%=RQ_total_op%>"> 개 이하
												<label for="total_op" class="glyphicon glyphicon-search " rel="tooltip" title="total_op"></label>
											</div>
										</td>
									</tr>
									<tr>
										<th>홈페이지/SRM출력</th>
										<td colspan="3">
											<label class="select w10 pro_detail">
											<select class="input-sm h34" name="g_display" id="g_display" >
												<option value="">전체</option>
												<option value="home" <% If RQ_display = "home" Then %> selected<% End If %>>홈페이지</option>
												<option value="srm" <% If RQ_display = "srm" Then %> selected<% End If %>>SRM</option>
												<option value="both" <% If RQ_display = "both" Then %> selected<% End If %>>홈페이지+SRM</option>
											</select> <i></i> </label>
										</td>
									</tr>
									<tr>
										<th>검색</th>
										<td colspan="3">
											<label class="select w10 pro_detail">
											<select class="input-sm h34" name="fieldname" id="fieldname" >
												<option value="g_name" <% If fieldname = "g_name" Then %> selected<% End If %>>상품명</option>
												<option value="g_idx" <% If fieldname = "g_idx" Then %> selected<% End If %>>상품코드</option>
											</select> <i></i> </label>
											<div class="icon-addon addon-md col-md-10 col-2">
												<input type="text" name="fieldvalue" class="form-control col-10" id="fieldvalue" value="<%=fieldvalue%>" >
												<label for="email" class="glyphicon glyphicon-search " rel="tooltip" title="email"></label>
											</div>

											<input type="button" class="btn btn-primary" value="검색" onclick="document.frm.submit();">
											<input type="button" class="btn btn-default" value="검색 초기화" onclick="location.href='./product.asp?menucode=<%=menucode%>&module=goods';">
										</td>
									</tr>
								</tbody>
							</table>

							<%=intTotalCount%>건
							<footer style="float:right;" class="">
								<select class="input-sm h34" name="orderby" id="orderby" onchange="document.frm.submit();" >
									<option value="g_insertDay" <% If orderby = "g_insertDay" Then %> selected<% End If %>>최신순</option>
									<option value="g_read" <% If orderby = "g_read" Then %> selected<% End If %>>인기순</option>
									<option value="g_Money_asc" <% If orderby = "g_Money_asc" Then %> selected<% End If %>>낮은가격순</option>
									<option value="g_Money_desc" <% If orderby = "g_Money_desc" Then %> selected<% End If %>>높은가격순</option>
								</select> <i></i>
							</footer>
							</form>

							<form name="list" method="get" action="./product_exec_get.asp" style="margin:0;">
							<input type="hidden" name="n">
							<input type="hidden" name="m">
							<input type="hidden" name="message">
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<input type="hidden" name="page" value="<%=intNowPage%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								<colgroup>
									<col style="width:5%">
									<col style="width:10%">
									<col style="width:*">
									<col style="10%">
									<col style="5%">
									<col style="5%">
									<col style="width:10%">
									<col style="width:15%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
										<th data-class="expand">출력</th>
										<th>제품명</th>
										<th data-hide="phone,tablet">제품가격</th>
										<th data-hide="phone,tablet">재고</th>
										<th data-hide="phone,tablet">진열여부</th>
										<th data-hide="phone,tablet">등록일</th>
										<th data-hide="phone,tablet">기능</th>
									</tr>
								</thead>

								<tbody>
<%
							If rs.eof Then
%>
									<tr><td colspan="8" align="center">데이터가 없습니다.</td></tr>
<%							Else
								rs.move movecount
								Do while not rs.eof
									g_idx = rs("g_idx")
									g_simg = rs("g_simg")
									g_display = rs("g_display")

									If g_display = "home" Then
										g_display_txt = "홈페이지"
									ElseIf g_display = "srm" Then
										g_display_txt = "SRM"
									ElseIf g_display = "both" Then
										g_display_txt = "홈페이지<br>SRM"
									End If
%>
									<tr>
										<td><input type="checkbox" value="<%=g_idx%>"></td>
										<td><%=g_display_txt%></td>
										<td>
										<% If g_simg <> "" Then %>
											<span class="pro_detail mr10"><%=getImgSizeView(70,80,"goods/" & g_simg)%></span>
										<% End If %>
											<span class="pro_detail">
												<%=rs("g_name")%> <br>
												제품코드 : <span class="text-danger"><%=g_idx%></span>
											</span>
										</td>
										<td><%=FormatNumber(rs("g_Money"), 0)%>원</td>
										<td><%=rs("total_op")%></td>
										<td><% If rs("g_act") = "Y" Then Response.write "진열" Else Response.write "진열안함" End If  %></td>
										<td><%=Left(rs("g_insertDay"), 10)%></td>
										<td>
											<a href="product_view.asp?menucode=<%=menucode%>&g_idx=<%=g_idx%>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i>수정</a>
											<a href="product_delete.asp?g_idx=<%=g_idx%>&module=goods" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a><br>
											<a href="product_srm.asp?menucode=<%=menucode%>&g_idx=<%=g_idx%>" class="btn btn-default"><i class=""></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SRM 설정&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
										</td>
									</tr>
<%
									rs.movenext
								Loop
							End If

							rs.close
							Set rs = Nothing

							Call DbClose()
%>
								</tbody>
							</table>

							<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:15px;">
								<tr>
									<td align="left">
										<a href="javascript:GetCheckbox(document.list, 'copy', '복사');" class="btn btn-default"><i class="fa fa-clone"></i> 복사</a>
										<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-primary"><i class="fa fa-trash-o"></i> 삭제</a>
									</td>
									<td align="right">
										<a href="javascript:GetCheckbox(document.list, 'dsp-home', '');" class="btn btn-default"><i class="fa fa-eye"></i> 홈페이지</a>
										<a href="javascript:GetCheckbox(document.list, 'dsp-srm', '');" class="btn btn-default"><i class="fa fa-eye"></i> SRM</a>
										<a href="javascript:GetCheckbox(document.list, 'dsp-both', '');" class="btn btn-default"><i class="fa fa-eye"></i> 홈페이지+SRM</a>
									</td>
								</tr>
							</table>
							</form>

							<%call Paging_list("")%>
						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

<% end if %>

				<script>
					$(document).ready(function() {

						// DO NOT REMOVE : GLOBAL FUNCTIONS!
						pageSetUp();

					});

				</script>

		<!-- #include file="../inc/footer.asp" -->