
		<!-- #include file="inc/head.asp" -->

		<!-- #include file="inc/header.asp" -->

			

				<div class="row">
				<!-- NEW WIDGET START -->
					<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
								<h2>최근 3일이내 주문내역 리스트</h2>
								<a class="more_btn" href="#">더보기</a>
							</header>
							<!-- widget div-->
							<div>
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
									<colgroup>
										<col style="width:10%">
										<col style="width:25%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand">번호</th>
											<th data-class="expand">주문일시</th>
											<th data-class="expand">주문번호</th>
											<th data-class="expand">주문자/아이디</th>
											<th data-class="expand">주문금액</th>
											<th data-class="expand">주문상태</th>
										</tr>
									</thead>
									<tbody>

										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>5</td>
											<td>yyyy-mm-dd</td>
											<td>923456789</td>
											<td>홍길동(sample)</td>
											<td>10,000</td>
											<td>결제대기</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>4</td>
											<td>yyyy-mm-dd</td>
											<td>823456789</td>
											<td>홍길동(sample)</td>
											<td>10,000</td>
											<td>결제확인</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>3</td>
											<td>yyyy-mm-dd</td>
											<td>723456789</td>
											<td>홍길동(sample)</td>
											<td>10,000</td>
											<td>상품배송</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>2</td>
											<td>yyyy-mm-dd</td>
											<td>623456789</td>
											<td>홍길동(sample)</td>
											<td>10,000</td>
											<td>판매완료</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>1</td>
											<td>yyyy-mm-dd</td>
											<td>523456789</td>
											<td>홍길동(sample)</td>
											<td>10,000</td>
											<td>교환</td>
										</tr>

									</tbody>
								</table>
								<!-- end widget content -->
							</div>
							<!-- end widget div -->
						</div>
						<!-- end widget -->

					</article>
					<!-- WIDGET END -->

					
					<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
								<h2>최근 3일이내 교환/반품요청 리스트</h2>
								<a class="more_btn" href="#">더보기</a>
							</header>

							<!-- widget div-->
							<div>
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
									<colgroup>
										<col style="width:8%">
										<col style="width:17%">
										<col style="width:15%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand">번호</th>
											<th data-class="expand">요청일</th>
											<th data-class="expand">주문번호</th>
											<th data-class="expand">주문자/아이디</th>
											<th data-class="expand">회사명</th>
											<th data-class="expand">주문금액</th>
											<th data-class="expand">주문상태</th>
										</tr>
									</thead>
									<tbody>

										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>5</td>
											<td>yyyy-mm-dd</td>
											<td>923456789</td>
											<td>홍길동(sample)</td>
											<td>회사A</td>
											<td>10,000</td>
											<td>교환요청</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>4</td>
											<td>yyyy-mm-dd</td>
											<td>823456789</td>
											<td>홍길동(sample)</td>
											<td>회사B</td>
											<td>10,000</td>
											<td>반품요청</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>3</td>
											<td>yyyy-mm-dd</td>
											<td>723456789</td>
											<td>홍길동(sample)</td>
											<td>회사C</td>
											<td>10,000</td>
											<td>교환요청</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>2</td>
											<td>yyyy-mm-dd</td>
											<td>623456789</td>
											<td>홍길동(sample)</td>
											<td>회사D</td>
											<td>10,000</td>
											<td>교환요청</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>1</td>
											<td>yyyy-mm-dd</td>
											<td>523456789</td>
											<td>홍길동(sample)</td>
											<td>회사E</td>
											<td>10,000</td>
											<td>반품요청</td>
										</tr>

									</tbody>
								</table>
							</div>
							<!-- end widget div -->

						</div>
						<!-- end widget -->
					</article>
				</div>
				<div class="row">
					<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
								<h2>최근 3일이내 회원가입 리스트</h2>
								<a class="more_btn" href="#">더보기</a>
							</header>
							<!-- widget div-->
							<div>
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
									<colgroup>
										<col style="width:10%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:25%">
										<col style="width:20%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand">번호</th>
											<th data-class="expand">이름</th>
											<th data-class="expand">아이디</th>
											<th data-class="expand">이메일</th>
											<th data-class="expand">휴대폰번호</th>
											<th data-class="expand">가입일</th>
										</tr>
									</thead>
									<tbody>

										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>5</td>
											<td>홍길동</td>
											<td>sample</td>
											<td>sample@mail.co.kr</td>
											<td>010-1234-5678</td>
											<td>yyyy-mm-dd</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>4</td>
											<td>홍길동</td>
											<td>sample</td>
											<td>sample@mail.co.kr</td>
											<td>010-1234-5678</td>
											<td>yyyy-mm-dd</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>3</td>
											<td>홍길동</td>
											<td>sample</td>
											<td>sample@mail.co.kr</td>
											<td>010-1234-5678</td>
											<td>yyyy-mm-dd</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>2</td>
											<td>홍길동</td>
											<td>sample</td>
											<td>sample@mail.co.kr</td>
											<td>010-1234-5678</td>
											<td>yyyy-mm-dd</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>1</td>
											<td>홍길동</td>
											<td>sample</td>
											<td>sample@mail.co.kr</td>
											<td>010-1234-5678</td>
											<td>yyyy-mm-dd</td>
										</tr>

									</tbody>
								</table>
								<!-- end widget content -->
							</div>
							<!-- end widget div -->
						</div>
						<!-- end widget -->

					</article>
					<!-- WIDGET END -->

					
					<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
								<h2>최근 3일이내 온라인 문의</h2>
								<a class="more_btn" href="#">더보기</a>
							</header>

							<!-- widget div-->
							<div>
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
									<colgroup>
										<col style="width:8%">
										<col style="width:17%">
										<col style="width:15%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand">번호</th>
											<th data-class="expand">구분</th>
											<th data-class="expand">분류</th>
											<th data-class="expand">제목</th>
											<th data-class="expand">회사명</th>
											<th data-class="expand">작성일</th>
											<th data-class="expand">답변여부</th>
										</tr>
									</thead>
									<tbody>

										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>5</td>
											<td>견적요청(솔루션)</td>
											<td>신성리커버리</td>
											<td>제목이 노출됩니다.</td>
											<td>회사A</td>
											<td>yyyy-mm-dd</td>
											<td>접수완료</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>4</td>
											<td>기술문의</td>
											<td>PC장애</td>
											<td>제목이 노출됩니다.</td>
											<td>회사A</td>
											<td>yyyy-mm-dd</td>
											<td>접수완료</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>3</td>
											<td>기타문의</td>
											<td>주문관련</td>
											<td>제목이 노출됩니다.</td>
											<td>회사A</td>
											<td>yyyy-mm-dd</td>
											<td>답변완료</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>2</td>
											<td>견적요청(일반)</td>
											<td></td>
											<td>제목이 노출됩니다.</td>
											<td>회사A</td>
											<td>yyyy-mm-dd</td>
											<td>답변완료</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>1</td>
											<td>견적요청(솔루션)</td>
											<td>백업 및 복구</td>
											<td>제목이 노출됩니다.</td>
											<td>회사A</td>
											<td>yyyy-mm-dd</td>
											<td>답변완료</td>
										</tr>

									</tbody>
								</table>
							</div>
							<!-- end widget div -->

						</div>
						<!-- end widget -->
					</article>
				</div>

		<!-- END MAIN PANEL -->

		
		<%

		'''''' 일 매출액
		Dim c_sql, c_rs, c_money(7), i, z
		i=0
		c_sql = "select top 7 left(app_time,8) as s_date,sum(realmoney)as s_money from order_regist where status = '2' and app_time!='null' group by left(app_time,8) order by left(app_time,8) desc"
		'Response.write c_sql
		Set c_rs = dbconn.execute(c_sql)
		If c_rs.eof Then
			For z=0 To 6
				c_money(z) = 0
			next
		Else
			do while not c_rs.eof

				For z=i To 6
					If Trim(c_rs("s_date")) = Trim(Replace(Date()-i,"-","")) then
						c_money(i) = c_rs("s_money")
						i= i+1
						Exit For 
					Else
						i= i+1
					End If
				Next
				
			c_rs.movenext
			loop

		end if

		c_rs.close
		set c_rs = Nothing
		
	

		'''''월 매출액
		Dim mc_money(7)
		i=0
		c_sql = "select top 7 left(app_time,6) as s_date,sum(realmoney)as s_money from order_regist where status = '2' and app_time!='null' group by left(app_time,6) order by left(app_time,6) desc"
		'Response.write c_sql
		Set c_rs = dbconn.execute(c_sql)
		If c_rs.eof Then
			For z=0 To 6
				mc_money(z) = 0
			next
		Else
			do while not c_rs.eof

				For z=i To 6
					If Trim(c_rs("s_date")) = Trim(Replace(left(DateAdd("m",-i,now()),7),"-","")) then
						mc_money(i) = c_rs("s_money")
						i= i+1
						Exit For 
					Else
						i= i+1
					End If
				Next
				
			c_rs.movenext
			loop

		end if

		c_rs.close
		set c_rs = Nothing

		%>

		<!-- #include file="inc/footer.asp" -->
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

				/* data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type */
				if ($('#year-graph').length) {
					var day_data = [
					<%for i=0 to 6%>
					{
						"period" : '<%=date()-i%>',
						"판매액" : '<%if len(c_money(i)) > 0 then response.write c_money(i) else response.write 0 end if%>'
					},
					<%next%>
					
					
					];
					Morris.Line({
						element : 'year-graph',
						data : day_data,
						xkey : 'period',
						ykeys : ['판매액', '환불액'],
						labels : ['판매액', '환불액']
						
					})

					
				}


				
				
				/* data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type */
				if ($('#month-graph').length) {
					var day_data = [
					
					<%for i=0 to 6%>
					{
						"period" : '<%=left(DateAdd("m",-i,now()),7)%>',
						"판매액" : '<%if len(mc_money(i)) > 0 then response.write mc_money(i) else response.write 0 end if%>'
					},
					<%next%>
					
					];
					Morris.Line({
						element : 'month-graph',
						data : day_data,
						xkey : 'period',
						ykeys : ['판매액', '환불액'],
						labels : ['판매액', '환불액']
					})
				}

			});

		</script>