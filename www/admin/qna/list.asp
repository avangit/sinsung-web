
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->
		<!--#include file="config.asp"-->
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>1:1문의관리</h2>
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">

							<%
	
								
								'##==========================================================================
								'##
								'##	페이징 관련 함수 - 게시판등...
								'##
								'##==========================================================================
								'## 
								'## [설정법]
								'## 다음 코드가 상단에 위치 하여야 함
								'## 
								dim intTotalCount, intTotalPage

								
								dim intNowPage			: intNowPage 		= Request.QueryString("page")    
								dim intPageSize			: intPageSize 		= 10
								dim intBlockPage		: intBlockPage 		= 10

								dim query_filde			: query_filde		= "*"
								dim query_Tablename		: query_Tablename	= tablename
								dim query_where			: query_where		= " intseq > 0 "
								dim query_orderby		: query_orderby		= " order by intseq DESC "
								
								
								Dim strSearch 			: strSearch 		= request("strSearch")
								Dim strKeyword			: strKeyword 		= request("strKeyword")
								
								If isValue(strKeyword) Then
									If strSearch= "g_name" Then
										query_Tablename = query_Tablename & ", goods "
										query_where = query_where & " and r_num = g_idx and g_name like '%"&strKeyword&"%'"
									Else
										query_where = query_where & " and "&strSearch&" like '%"&strKeyword&"%'"
									End If
								Else
									strSearch = ""
									strKeyword = ""
								End If
								
								call intTotal
								
								'##
								'## 1. intTotal : call intTotal
								'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
								'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
								'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
								'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
								'## 
								'## 
								'#############################################################################

								dim sql, rs
								sql = GetQuery()
								'response.Write(sql)
								call dbopen
								set rs = dbconn.execute(sql)
							%>



							<form name = "frm" action = "?<%=getstring("")%>" onsubmit="return regist(this)" class="smart-form m10"  method="post" >
								<label class="select w10 pro_detail">
								<select class="input-sm h34" name="keyword_option" >
									<option value="strTitle">제목</option>
									<option value="strName">이름</option>
								</select> <i></i> </label>
								<div class="icon-addon addon-md col-md-10 col-2">
									<input type="text" name="keyword" class="form-control col-10">
									<label for="keyword" class="glyphicon glyphicon-search " rel="tooltip" title="keyword"></label>
								</div>
								<input type="submit" class="btn btn-default" value="검색">
								
							</form>	
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								 <colgroup>
									<col style="width:5%">
									<col style="width:20%">
									<col style="">
									<col style="width:10%">
									<col style="width:10%">
									<col style="width:5%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand">번호</th>
										<th data-class="expand">상품정보</th>
										<th>제목</th>
										<th data-hide="phone,tablet">글쓴이</th>
										<th data-hide="phone,tablet">작성일</th>
										<th data-hide="phone,tablet">조회</th>
									</tr>
								</thead>
	
								<tbody>
									<%
									dim pagei : pagei = (intTotalCount-MoveCount)
									'// 글이 없을 경우
									if  rs.eof then
									%>
									<td colspan="6">현재 등록된 Q&A가 없습니다.</td>
									<%	
									else
										Dim good_name,good_img,strUrlView
										rs.move MoveCount 
										Do while not rs.eof	'										
											intseq			= rs("intseq")
											strTitle		= rs("strTitle")
											strInsertDate	= rs("strInsertDate")
											strAttach		= rs("strAttach")
											intgoods_code	= rs("intgoods_code")
											strReName		= rs("strReName")
											strReTitle		= rs("strReTitle")
											intHit			= rs("intHit")
											strName			= rs("strName")
											strGubun		= rs("strGubun")
											strUpdateDate	= rs("strUpdateDate")
											
											
											if datediff ("n",strInsertDate,Now()) < 1440 then 
												dim strnewarticle : strnewarticle = " <img src='/admin/img/b_new.gif' align='absmiddle' border='0'>&nbsp;"
											else
												strnewarticle = ""
											end if	
											
											arrData = getAdoRsArray("SELECT g_name,g_bimg FROM goods WHERE g_idx ='"& intgoods_code &"'")				
										
											If isArray(arrData) Then	
												 
												good_name 			= arrData(0,0)
												good_img		 	= arrData(1,0)				
											End If
											
											
											If getBitGubun(strGubun) = False  Then
												strUrlView = "pwd"
											Else
												strUrlView = "view"
											End If
											
									

									%>
									<tr>
										<td><%=pagei%></td>
										<td><a href="modify.asp?intSeq=<%=rs("intseq")%>"><%=getImgSizeView(40,40,good_img)%> <%=good_name%></a></td>
										<td>
											<a href="modify.asp?intSeq=<%=rs("intseq")%>">
											<%If getBitGubun(strGubun) = False  Then%><img src = "/admin/img/icon_lock.gif" /> <%End If%>
											<%=strTitle%>
											<%If isValue(strAttach) Then%> <img src = "/admin/img/icon_file.gif" border="0" /> <%End If%><%=strnewarticle%>
											</a>
											<br>
											<%If isValue(rs("strReContent")) Then%>
											<a href="modify.asp?intSeq=<%=rs("intseq")%>">
											 <img src = "/admin/img/icon_re.gif"> 
											 <%If getBitGubun(strGubun) = False  Then%><img src = "/admin/img/icon_lock.gif" /> <%End If%>
											 <%=strReTItle%> <%If isValue(strAttachAdmin) Then%> <img src = "/admin/img/icon_file.gif" border="0" /> <%End If%>
											 <%=strnewarticle%> [<%=left(strUpdateDate, 10)%>]
											</a>

											<%End if%>
										</td>
										<td><%=strName%></td>
										<td><%=left(strInsertDate, 10)%></td>
										<td><%=intHit%></td>
									</tr>
									
									<%
											pagei = pagei-1
										rs.movenext
										loop
										rs.close()
										set rs = nothing
									End If
									%>
																		
								</tbody>
						
							</table>
							<%call Paging_list("")%>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->



			

			</article>
			<!-- WIDGET END -->

		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>


		<!-- #include file="../inc/footer.asp" -->
