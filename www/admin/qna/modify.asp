
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->
		<!--#include file="config.asp"-->

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>BASIC</h2>
					</header>
	
					<!-- widget div-->
					<div class="mt50">
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">


							<%
								DBopen

								Dim gIdx,g_idx,g_name,g_bimg,g_customerMoney,g_Money
								Dim strAction,strUrl
								
								gIdx 	= requestQ("gIdx")	
								intseq 	= requestQ("intseq")	
								
								If isValue(intseq) Then	
									call idxdata(intseq)
									'//공개글이면
									If getBitGubun(strGubun) = True Then		
										call passwordCheck(requestF("strPwd"),intseq)			
									End If
									
									'//비밀번호가 맞으면			
									arrData 	= getAdoRsArray("select g_idx,g_name,g_bimg,g_customerMoney,g_Money from goods where g_idx = '"& intgoods_code &"'")			
									strAction 	= "update.asp?intSeq="&intSeq
									strUrl      = "location.href='?"&getstring("strInquiryMode=list")&"'"	
								Else
								
									arrData = getAdoRsArray("select g_idx,g_name,g_bimg,g_customerMoney,g_Money from goods where g_idx = '"& gIdx &"'")				
									
									strAction 	= "insert.asp"
									strUrl      ="document.qna.reset();"	
								End If
								
								If isArray(arrData) Then			 
									g_idx 			= arrData(0,0)
									g_name		 	= arrData(1,0)
									g_bimg			= arrData(2,0)				
									g_customerMoney	= arrData(3,0)	
									g_Money			= arrData(4,0)		
								end if

							%>

							<form name="frmRequestForm" method="post" action="<%=strAction%>" onsubmit="return frmRequestForm_Submit(this);" class="smart-form" enctype="multipart/form-data">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<td rowspan="2"><%=getImgSizeView(134,134,g_bimg)%></td>
										<th><%=g_name%></th>
									</tr>
									<tr>
										<td><%=MoneyView(g_customerMoney,g_money,"",0)%></td>
									</tr>
								</tbody>
							</table>
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									
									<tr>
										<th>제목</th>
										<td>
											<label for="strTitle" class="input">
												<input type="text" name="strTitle" id="strTitle" placeholder="" value="<%=strTitle%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>내용</th>
										<td colspan="2">
											<label class="textarea"> 										
												<textarea rows="3" name="strContent" class="h150 pro_detail"><%=strContent%></textarea> 
												
											</label>
										</td>
									</tr>
									<tr>
										<th>작성자</th>
										<td>
											<label for="strName" class="input">
												<input type="text" name="strName" id="strName" placeholder="" value="<%=strName%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>비밀번호</th>
										<td>
											<label for="strPwd" class="input col-1 pro_detail" style="margin-right:5px;">
												<input type="password" name="strPwd" id="strPwd" value="<%=strPwd%>"  <%if isvalue(strpwd) then %>readonly = "true"<%End If%> >
											</label>
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="strGubun" value="1" <%if isvalue(intseq) then%><%=chk_checked(strgubun,true)%><%else%>checked="checked"<%End If%>>
													<i></i>공개</label>
												<label class="radio">
													<input type="radio" name="strGubun" value="0" <%=chk_checked(strGubun,false)%> >
													<i></i>비공개</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>첨부파일</th>
										<td>
											<section>
												<div class="input input-file w20 pro_detail" >
													<span class="button"><input id="strAttach" type="file" name="strAttach" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%If isValue(strAttach) Then Response.write split(strAttach,":")(1)%>">
												</div><% call FileDownLink_uppath(strAttach,"/upload/qna/")%>
											</section>
											

										</td>
									</tr>
									
								</tbody>
						
							</table>

							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%"style="margin-top:30px;">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th colspan="2" class="aling_c">관리자 영역</th>
									</tr>
									<tr>
										<th>제목</th>
										<td>
											<label for="strRetitle" class="input">
												<input type="text" name="strRetitle" id="strRetitle" placeholder="" value="<%=strRetitle%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>내용</th>
										<td colspan="2">
											<label class="textarea"> 										
												<textarea rows="3" name="strReContent" class="h150 pro_detail"><%=strReContent%></textarea> 
												
											</label>
										</td>
									</tr>
									<tr>
										<th>작성자</th>
										<td>
											<label for="strReName" class="input">
												<input type="text" name="strReName" id="strReName" placeholder="" value="<%=session("username")%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>첨부파일</th>
										<td>
											<section>
												<div class="input input-file w20 pro_detail" >
													<span class="button"><input id="strAttachAdmin" type="file" name="strAttachAdmin" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly value="<%If isValue(strAttachAdmin) Then Response.write split(strAttachAdmin,":")(1)%>">
												</div><% call FileDownLink_uppath(strAttachAdmin,"/upload/")%>
											</section>
											
											<%If isValue(strAttachAdmin) then%>
											<a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-trash-o"></i>삭제</a>
											<%End if%>

										</td>
									</tr>
									
								</tbody>
						
							</table>


							<footer style="float:right;">
								<input type="button" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
								<input type="button" class="btn btn-default"  onclick="window.history.back();" value="리스트">
								<input type="button" class="btn btn-default"  onclick="location.href='delete.asp?intSeq=<%=intSeq%>'" value="삭제">
							</footer>
							
							</form>	
							
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
					</div>
	
				</div>
				<!-- end widget -->

				<script language="javascript">
<!--

					function frmRequestForm_Submit(frm){
						if ( frm.strTitle.value == "" ) { alert("제목을 입력해주세요"); frm.strTitle.focus(); return false; }
						frm.submit();
					}

				//-->
				</script>


			

			</article>
			<!-- WIDGET END -->

		
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>



		<!-- #include file="../inc/footer.asp" -->

