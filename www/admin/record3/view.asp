<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<!-- NEW WIDGET START -->
<article>
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
	<!-- Widget ID (each widget will need unique ID)-->
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
		<header>
			<span class="widget-icon"> <i class="fa fa-table"></i> </span>
			<h2>개인정보처리방침 보기</h2>
		</header>

		<!-- widget div-->
		<div class="mt50">

			<!-- widget edit box -->
			<div class="jarviswidget-editbox">
				<!-- This area used as dropdown edit box -->

			</div>
			<!-- end widget edit box -->

			<!-- widget content -->
			<div class="widget-body no-padding">
				<%
				Call DbOpen()

				Dim strSQL, arrData

				strSQL = "SELECT * FROM Record_views5 WHERE intSeq = '"&request.QueryString("intseq")&"'"
				arrData = getAdoRsArray(strSQL)

				If isArray(arrData) Then

					Dim startdate, enddate, state, contents

					startdate 			= arrData(1,0)
					enddate 			= arrData(2,0)
					state   			= arrData(3,0)
					contents			= arrData(4,0)

				else

					response.Write("코드에러")

				end If

				Call DbClose()
				%>
				<form name = "joinform" action = "modify.asp?<%=getstring("")%>" class="smart-form" method = "post" enctype="multipart/form-data" >
				<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
					 <colgroup>
						<col style="width:15%">
						<col style="">
					</colgroup>
					<tbody>
						<tr>
							<th>적용시작</th>
							<td><%=startdate%></td>
						</tr>
						<tr>
							<th>적용종료</th>
							<td><%=enddate%></td>
						</tr>
						<tr>
							<th>약관내용</th>
							<td><%=contents%></td>
						</tr>
					</tbody>
				</table>
				<footer style="float:right;">
					<input type="submit" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
					<input type="button" class="btn btn-default"  onclick="window.history.back();" value="리스트">
					<input type="button" class="btn btn-default"  onclick="location.href='delete.asp?<%=getstring("")%>'" value="삭제">
				</footer>
				</form>
			</div>
			<!-- end widget content -->
		</div>
		<!-- end widget div -->

	</div>
	<!-- end widget -->

</article>
<!-- WIDGET END -->
<script>
	$(document).ready(function() {

		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		pageSetUp();

	});
</script>
<!-- #include file="../inc/footer.asp" -->