<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<link rel="stylesheet" href="/avanplus/_master/js/jquery-ui.css" media="screen" title="no title">
<script src="/avanplus/_master/js/jquery-ui.js"></script>
<script>
$(function() {
 $(".datepicker").datepicker({
	 dateFormat: 'yy-mm-dd',
	 changeMonth:'true',
	 changeYear:'true'
	 });
})
</script>

<!-- NEW WIDGET START -->
<article>
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
	<!-- Widget ID (each widget will need unique ID)-->
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
		<header>
			<span class="widget-icon"> <i class="fa fa-table"></i> </span>
			<h2>개인정보처리방침 수정</h2>
		</header>
		<!-- widget div-->
		<div class="mt50">
			<!-- widget edit box -->
			<div class="jarviswidget-editbox">
				<!-- This area used as dropdown edit box -->
			</div>
			<!-- end widget edit box -->
			<!-- widget content -->
			<div class="widget-body no-padding">
				<%
				Call DbOpen()

				Dim strSQL, arrData

				strSQL = "SELECT * FROM Record_views5 WHERE intSeq = '"&request.QueryString("intseq")&"'"
				arrData = getAdoRsArray(strSQL)

				If isArray(arrData) Then

					Dim startdate, enddate, state, contents

					startdate 			= arrData(1,0)
					enddate 			= arrData(2,0)
					state   			= arrData(3,0)
					contents			= arrData(4,0)

				else

					response.Write("코드에러")

				end If

				Call DbClose()
				%>
				<form name = "joinform" action = "update.asp?<%=getstring("")%>" class="smart-form" method = "post" enctype="multipart/form-data" >
				<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
					 <colgroup>
						<col style="width:15%">
						<col style="">
					</colgroup>
					<tbody>
						<tr>
							<th>적용시작</th>
							<td><input type="text" name="dtmStartDate1" class="form-control datepicker" style="width:109px;float:left;padding-left:3px;" value="<%=startdate%>" maxlength="10" ></td>
						</tr>
						<tr>
							<th>적용종료</th>
							<td><input type="text" name="dtmEndDate1" class="form-control datepicker" style="width:109px;float:left;padding-left:3px;" value="<%=enddate%>" maxlength="10" ></td>
						</tr>
						<tr>
							<th>약관내용</th>
							<td><%call Editor("b_text",contents)%></td>
						</tr>
					</tbody>
				</table>
				<footer style="float:right;">
					<input type="submit" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
					<input type="button" class="btn btn-default" onclick="location.href='list.asp?menucode=<%=request.QueryString("menucode")%>&page=<%=request.QueryString("page")%>'" value="리스트">
					<input type="button" class="btn btn-default" onclick="location.href='delete.asp?intSeq=<%=request.QueryString("intseq")%>'" value="삭제">
				</footer>
				</form>
			</div>
			<!-- end widget content -->

		</div>
		<!-- end widget div -->

	</div>
	<!-- end widget -->

</article>
<!-- WIDGET END -->
<script>
	$(document).ready(function() {

		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		pageSetUp();

	});

</script>
<!-- #include file="../inc/footer.asp" -->