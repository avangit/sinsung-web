<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<link rel="stylesheet" href="/avanplus/_master/js/jquery-ui.css" media="screen" title="no title">
<script src="/avanplus/_master/js/jquery-ui.js"></script>
<script>
$(function() {
 $(".datepicker").datepicker({
	 dateFormat: 'yy-mm-dd',
	 changeMonth:'true',
	 changeYear:'true'
	 });
})
</script>

	<!-- NEW WIDGET START -->
	<article>
	<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
		<!-- Widget ID (each widget will need unique ID)-->
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>개인정보처리방침 등록</h2>
			</header>

			<!-- widget div-->
			<div class="mt50">

				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->

				</div>
				<!-- end widget edit box -->
				<%
				  Dim dtmStartDate1, dtmEndDate1
				%>
				<!-- widget content -->
				<div class="widget-body no-padding">

					<form name = "joinform" action = "insert.asp?<%=getstring("")%>" onsubmit="return frmRequestForm_Submit(this);" class="smart-form" method = "post" enctype="multipart/form-data" >
					<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
						 <colgroup>
							<col style="width:15%">
							<col style="">
						</colgroup>
						<tbody>
							<tr>
								<th>적용시작</th>
								<td><input type="text" name="dtmStartDate1" class="form-control datepicker" style="width:109px;float:left;padding-left:3px;" value="<%=dtmStartDate1%>" maxlength="10" ></td>
							</tr>
							<tr>
								<th>적용종료</th>
								<td><input type="text" name="dtmEndDate1" class="form-control datepicker" style="width:109px;float:left;padding-left:3px;" value="<%=dtmEndDate1%>" maxlength="10" ></td>
							</tr>
							<tr>
								<th>약관내용</th>
								<td><%call Editor("b_text","")%></td>
							</tr>
						</tbody>
				
					</table>

					<footer style="float:right;">
						<input type="submit" class="btn btn-primary" onclick="this.form.onsubmit();" value="등록하기">
						<input type="button" class="btn btn-default"  onclick="window.history.back();" value="리스트">
					</footer>
					
					</form>	
					
	
					
				
				</div>
				<!-- end widget content -->

			</div>
			<!-- end widget div -->

		</div>
		<!-- end widget -->
	</article>
	<!-- WIDGET END -->
<script>
	$(document).ready(function() {

		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		pageSetUp();

	});

</script>

<!-- #include file="../inc/footer.asp" -->


<script language="javascript">
<!--
	function frmRequestForm_Submit(frm){

		if ( frm.dtmStartDate1.value.replace(/ /gi, "") == "" ) { alert("적용시작일을 입력해주세요"); frm.dtmStartDate1.focus(); return false; }
		if ( frm.dtmEndDate1.value.replace(/ /gi, "") == "" ) { alert("적용종료일을 입력해주세요"); frm.dtmEndDate1.focus(); return false; }

		//if ( frm.b_text.value.replace(/ /gi, "") == "" ) { alert("내용을 입력해주세요"); frm.b_text.focus(); return false; }

        // before_save_list2("month1");
   
		frm.submit();
	}

//-->
</script>
