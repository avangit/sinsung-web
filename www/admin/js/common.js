function getXMLHttpRequest() {
	if (window.ActiveXObject) {
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e1) { return null; }
		}
	} else if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else {
		return null;
	}
}
var httpRequest = null;

function sendRequest(url, params, callback, method) {
	httpRequest = getXMLHttpRequest();
	var httpMethod = method ? method : 'GET';
	if (httpMethod != 'GET' && httpMethod != 'POST') {
		httpMethod = 'GET';
	}
	var httpParams = (params == null || params == '') ? null : params;
	var httpUrl = url;
	if (httpMethod == 'GET' && httpParams != null) {
		httpUrl = httpUrl + "?" + httpParams;
	}
	httpRequest.open(httpMethod, httpUrl, true);
	httpRequest.setRequestHeader(
		'Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.onreadystatechange = callback;
	httpRequest.send(httpMethod == 'POST' ? httpParams : null);
}

function id_check(){
	var frm	=	document.joinform;
	var id=frm.strId.value;

	
	if(id.length < 4 || id.length> 20) {
		alert('4~20자 이내의 아이디를 입력하여 주십시오.');
		frm.strId.focus();
		return;
	}

	var chk_num = id.search(/[0-9]/g); 
	var chk_eng = id.search(/[a-z]/ig); 
	//var chk_esp = id.search(/[^0-9a-zA-Z]/g);  
	/*
	if(chk_eng> 0 || chk_num < 0) {
		alert('영문 또는 영문+숫자의 조합으로 아이디를 설정해 주세요 첫번째 글자는 영문입니다.'); 
		frm.strId.focus();
		return;
	}
	*/
	var params = "strId="+encodeURIComponent(id);
	sendRequest("/admin/member/ajax_idCheck.asp", params, idcheckResult, 'POST');

}

function idcheckResult() {
		if (httpRequest.readyState == 4) {

			if (httpRequest.status == 200) {
				var resultText = httpRequest.responseText;

				rel=resultText.split("||");
				var jangList=document.getElementById("id_ck");

				var resultText = httpRequest.responseText;
				jangList.innerHTML=rel[1];

				if(rel[0]=="y"){ document.joinform.num.value="y";	}else{	document.joinform.num.value="";	}

			} else {
				alert("Error: "+httpRequest.status);
			}
		}
}

function id_check2(){
	var frm	=	document.subjoinform;
	var id=frm.strId_sub.value;

	
	if(id.length < 4 || id.length> 20) {
		alert('4~20자 이내의 아이디를 입력하여 주십시오.');
		frm.strId_sub.focus();
		return;
	}

	var chk_num = id.search(/[0-9]/g); 
	var chk_eng = id.search(/[a-z]/ig); 
	//var chk_esp = id.search(/[^0-9a-zA-Z]/g);  
	/*
	if(chk_eng> 0 || chk_num < 0) {
		alert('영문 또는 영문+숫자의 조합으로 아이디를 설정해 주세요 첫번째 글자는 영문입니다.'); 
		frm.strId.focus();
		return;
	}
	*/
	var params = "strId="+encodeURIComponent(id);
	sendRequest("/admin/member/ajax_idCheck.asp", params, idcheckResult2, 'POST');

}

function idcheckResult2() {
		if (httpRequest.readyState == 4) {

			if (httpRequest.status == 200) {
				var resultText = httpRequest.responseText;

				rel=resultText.split("||");
				var jangList=document.getElementById("id_ck");

				var resultText = httpRequest.responseText;
				jangList.innerHTML=rel[1];

				if(rel[0]=="y"){ document.subjoinform.num.value="y";	}else{	document.subjoinform.num.value="";	}

			} else {
				alert("Error: "+httpRequest.status);
			}
		}
}

function id_check_admin(){
	var frm	=	document.joinform;
	var id=frm.strId.value;

	
	if(id.length < 4 || id.length> 20) {
		alert('4~20자 이내의 아이디를 입력하여 주십시오.');
		frm.strId.focus();
		return;
	}

	var chk_num = id.search(/[0-9]/g); 
	var chk_eng = id.search(/[a-z]/ig); 
	//var chk_esp = id.search(/[^0-9a-zA-Z]/g);  
	/*
	if(chk_eng> 0 || chk_num < 0) {
		alert('영문 또는 영문+숫자의 조합으로 아이디를 설정해 주세요 첫번째 글자는 영문입니다.'); 
		frm.strId.focus();
		return;
	}
	*/
	var params = "strId="+encodeURIComponent(id);
	sendRequest("/admin/basic/ajax_idCheck.asp", params, idcheckResult, 'POST');

}

function ChangeEmail1() {
	if (joinform.strEmail3.value == '0') {
		joinform.strEmail2.readOnly = false;
		joinform.strEmail2.value = '';
		joinform.strEmail2.focus();
	} else {
		joinform.strEmail2.readOnly = true;
		joinform.strEmail2.value = joinform.strEmail3.value;
	}
}

function ChangeEmail2() {
	if (joinform.cEmail3.value == '0') {
		joinform.cEmail2.readOnly = false;
		joinform.cEmail2.value = '';
		joinform.cEmail2.focus();
	} else {
		joinform.cEmail2.readOnly = true;
		joinform.cEmail2.value = joinform.cEmail3.value;
	}
}

function ChangeEmail3() {
	if (subjoinform.strEmail3_sub.value == '0') {
		subjoinform.strEmail2_sub.readOnly = false;
		subjoinform.strEmail2_sub.value = '';
		subjoinform.strEmail2_sub.focus();
	} else {
		subjoinform.strEmail2_sub.readOnly = true;
		subjoinform.strEmail2_sub.value = subjoinform.strEmail3_sub.value;
	}
}

function ChangeEmail4() {
	if (subupdateform.strEmail3.value == '0') {
		subupdateform.strEmail2.readOnly = false;
		subupdateform.strEmail2.value = '';
		subupdateform.strEmail2.focus();
	} else {
		subupdateform.strEmail2.readOnly = true;
		subupdateform.strEmail2.value = subupdateform.strEmail3.value;
	}
}

function join_chk() {
	var form=document.joinform;

	if(form.strId.value == "") {
		alert("아이디를 입력해 주세요.");
		form.strId.focus();
		return false;
	} else if(form.strId.value.length < 4 || form.strId.value.length > 20) {
		alert("아이디는 4~20자 이내입니다.");
		form.strId.focus();
		return false;
	} else if(form.num.value == "") {
		alert("아이디 중복체크를 하시거나 중복된 아이디 사용을 피해주세오.");
		id_check();
		return false;
	} else if(form.strPwd.value == "") {
		alert("비밀번호를 입력해 주세요.");
		form.strPwd.focus();
		return false;
	} else if(form.strPwd.value.length < 4 || form.strPwd.value.length > 16) {
		alert("비밀번호는 4~16자 이내입니다.");
		form.strPwd.focus();
		return false;
	} else if(form.strPwdRe.value == "") {
		alert("비밀번호를 입력해 주세요.");
		form.strPwdRe.focus();
		return false;
	} else if(form.strPwd.value != form.strPwdRe.value) {
		alert("비밀번호가 일치하지 않습니다.");
		form.strPwdRe.focus();
		return false;
	} else if(form.strName.value == "") {
		alert("이름을 입력해 주세요.");
		form.strName.focus();
		return false;
	} else if(form.strMobile1.value == "" || form.strMobile2.value == "" || form.strMobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		form.strMobile1.focus();
		return false;
	} else if(form.strEmail1.value == "" || form.strEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		form.strEmail1.focus();
		return false;
	} else if(form.cName.value == "") {
		alert("법인명을 입력해 주세요.");
		form.cName.focus();
		return false;
	} else if(form.cNum1.value == "" || form.cNum2.value == "" || form.cNum3.value == "") {
		alert("사업자등록번호를 입력해 주세요.");
		form.cNum1.focus();
		return false;
	} else if(form.cZip.value == "" || form.cAddr1.value == "" || form.cAddr2.value == "") {
		alert("사업장주소를 입력해 주세요.");
		return false;
	} else {
		form.submit();
	}
}

function join_chk_admin() {
	var form=document.joinform;

	if(form.strId.value == "") {
		alert("아이디를 입력해 주세요.");
		form.strId.focus();
		return false;
	} else if(form.strId.value.length < 4 || form.strId.value.length > 20) {
		alert("아이디는 4~20자 이내입니다.");
		form.strId.focus();
		return false;
	} else if(form.num.value == "") {
		alert("아이디 중복체크를 하시거나 중복된 아이디 사용을 피해주세오.");
		id_check();
		return false;
	} else if(form.strPwd.value == "") {
		alert("비밀번호를 입력해 주세요.");
		form.strPwd.focus();
		return false;
	} else if(form.strPwd.value.length < 4 || form.strPwd.value.length > 16) {
		alert("비밀번호는 4~16자 이내입니다.");
		form.strPwd.focus();
		return false;
	} else if(form.strPwdRe.value == "") {
		alert("비밀번호를 입력해 주세요.");
		form.strPwdRe.focus();
		return false;
	} else if(form.strPwd.value != form.strPwdRe.value) {
		alert("비밀번호가 일치하지 않습니다.");
		form.strPwdRe.focus();
		return false;
	} else if(form.strName.value == "") {
		alert("이름을 입력해 주세요.");
		form.strName.focus();
		return false;
	} else if(form.strMobile1.value == "" || form.strMobile2.value == "" || form.strMobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		form.strMobile1.focus();
		return false;
	} else if(form.strEmail1.value == "" || form.strEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		form.strEmail1.focus();
		return false;
	} else {
		form.submit();
	}
}

function update_chk(obj) {
	var form=document.joinform;

	if(form.strPwd.value != "") {
		if(form.strPwd.value.length < 4 || form.strPwd.value.length > 16) {
			alert("비밀번호는 4~16자 이내입니다.");
			form.strPwd.focus();
			return false;
		}
	}

	if(form.strName.value == "") {
		alert("이름을 입력해 주세요.");
		form.strName.focus();
		return false;
	} else if(form.strMobile1.value == "" || form.strMobile2.value == "" || form.strMobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		form.strMobile1.focus();
		return false;
	} else if(form.strEmail1.value == "" || form.strEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		form.strEmail1.focus();
		return false;
	} else if(form.cName.value == "") {
		alert("법인명을 입력해 주세요.");
		form.cName.focus();
		return false;
	} else if(form.cNum1.value == "" || form.cNum2.value == "" || form.cNum3.value == "") {
		alert("사업자등록번호를 입력해 주세요.");
		form.cNum1.focus();
		return false;
	} else if(form.cZip.value == "" || form.cAddr1.value == "" || form.cAddr2.value == "") {
		alert("사업장주소를 입력해 주세요.");
		return false;
	} else {
		form.submit();
	}
}

function subjoin_chk(obj) {
	var form=document.subjoinform;

	if(form.strName_sub.value == "") {
		alert("이름을 입력해 주세요.");
		form.strName_sub.focus();
		return false;
	} else if(form.department.value == "") {
		alert("부서를 입력해 주세요.");
		form.department.focus();
		return false;
	} else if(form.strId_sub.value == "") {
		alert("아이디를 입력해 주세요.");
		form.strId_sub.focus();
		return false;
	} else if(form.strId_sub.value.length < 4 || form.strId_sub.value.length > 20) {
		alert("아이디는 4~20자 이내입니다.");
		form.strId_sub.focus();
		return false;
	} else if(form.num.value == "") {
		alert("아이디 중복체크를 하시거나 중복된 아이디 사용을 피해주세오.");
		id_check2();
		return false;
	} else if(form.strPwd_sub.value == "") {
		alert("비밀번호를 입력해 주세요.");
		form.strPwd_sub.focus();
		return false;
	} else if(form.strPwd_sub.value.length < 4 || form.strPwd_sub.value.length > 16) {
		alert("비밀번호는 4~16자 이내입니다.");
		form.strPwd_sub.focus();
		return false;
	} else if(form.strPwdRe_sub.value == "") {
		alert("비밀번호를 입력해 주세요.");
		form.strPwdRe_sub.focus();
		return false;
	} else if(form.strPwd_sub.value != form.strPwdRe_sub.value) {
		alert("비밀번호가 일치하지 않습니다.");
		form.strPwdRe_sub.focus();
		return false;
	} else if(form.strMobile1_sub.value == "" || form.strMobile2_sub.value == "" || form.strMobile3_sub.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		form.strMobile1_sub.focus();
		return false;
//	} else if(form.strEmail1_sub.value == "" || form.strEmail2_sub.value == "") {
//		alert("이메일을 입력해 주세요.");
//		form.strEmail1_sub.focus();
//		return false;
	} else {
		form.submit();
	}
}

function subupdate_chk() {
	var form=document.subupdateform;

	if(form.strPwd.value != "") {
		if(form.strPwd.value.length < 4 || form.strPwd.value.length > 16) {
			alert("비밀번호는 4~16자 이내입니다.");
			form.strPwd.focus();
			return false;
		}
	}

	if(form.strName.value == "") {
		alert("이름을 입력해 주세요.");
		form.strName.focus();
		return false;
	} else if(form.department.value == "") {
		alert("부서명을 입력해 주세요.");
		form.department.focus();
		return false;
	} else if(form.strMobile1.value == "" || form.strMobile2.value == "" || form.strMobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		form.strMobile1.focus();
		return false;
	} else if(form.strEmail1.value == "" || form.strEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		form.strEmail.focus();
		return false;
	} else {
		form.submit();
	}
}

function reply_chk() {
	var form=document.replyform;

	if(form.re_name.value == "") {
		alert("담당자명을 입력해 주세요.");
		form.re_name.focus();
		return false;
	} else {
		form.submit();
	}
}

function popup_chk() {
	var form=document.joinform;

	if(form.sdate.value == "") {
		alert("시작기간을 입력해 주세요.");
		form.sdate.focus();
		return false;
	} else if(form.edate.value == "") {
		alert("종료기간을 입력해 주세요.");
		form.edate.focus();
	} else if(form.pop_width.value == "") {
		alert("가로크기를 입력해 주세요.");
		form.pop_width.focus();
	} else if(form.pop_height.value == "") {
		alert("세로크기를 입력해 주세요.");
		form.pop_height.focus();
		return false;
	} else {
		form.submit();
	}
}

// 체크박스 전체선택
var check = 1;
function checkAll(frm) {
	if (check) {
		for (i = 0; i < frm.length; i++) {
			if (frm[i].type == "checkbox") {
				if (frm[i].checked)
					continue;
				else
					frm[i].checked = true;
			}
		}
		check = 0;
	} else {
		for (i = 0; i < frm.length; i++) {
			if (frm[i].type == "checkbox") {
				if(frm[i].checked) frm[i].checked = false;
				else continue;
			}
		}
		check = 1;
	}
	return false;
}

// 체크박스 값 넘기기
function GetCheckbox(frm, mod, message) {
	var tmp = "";
	var cnt = 0;
	var i;
	var idk = false;

	for (i = 0; i < frm.length; i++) {
		if (frm[i].type != "checkbox")
			continue;
		if (frm[i].checked) {
			tmp += frm[i].value + " ";
			cnt = cnt + 1;
			idk = true;
		}
	}

	if (idk == true) {
		if (mod.indexOf("del") != -1) {
			var cfm = confirm("선택하신 내용을 삭제하시겠습니까?");
		} else if (mod == "sec") {
			var cfm = confirm("선택하신 회원을 탈퇴시키겠습니까?");
		} else if (mod == "copy") {
			if(cnt > 1) {
				alert('복사할 대상은 하나만 선택해 주세요.');
			} else {
				var cfm = confirm("선택하신 내용을 복사하시겠습니까?");
			}
		} else if (mod.substr(0,4) == "dsp-") {
			var cfm = confirm("선택하신 내용의 상태를 변경하시겠습니까?");
		} else if (mod.substr(0,7) == "result_") {
			var cfm = confirm("선택하신 내용을 '" + message + "' 변경하시겠습니까?");
		} else {
			if (message != null) {
				var cfm = confirm("선택하신 내용을 '" + message + "' 하시겠습니까?");
			} else {
				var cfm = confirm("선택하신 내용의 상태를 변경하시겠습니까?");
			}
		}

		if (cfm) {
			frm.n.value = tmp.substr(0, tmp.length - 1);
			frm.m.value = mod;
			frm.message.value = message;
			frm.submit();
		} else {
			return;
		}
	} else {
		window.alert('실행할 데이터를 선택하세요');
		return;
	}
	return false;
}

$(document).ready(function () {
//	$.datepicker.setDefaults($.datepicker.regional['ko']);

	$( "#sdate" ).datepicker({
		changeMonth: true,  // 월을 바꿀수 있는 셀렉트 박스를 표시한다.
		changeYear: true,	// 년을 바꿀 수 있는 셀렉트 박스를 표시한다.
		showMonthAfterYear: true,	// 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다
		nextText: '다음 달',
		prevText: '이전 달',
		dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dateFormat: "yy-mm-dd",
//					minDate: 0,		// 선택할수있는 최소날짜, ( 0 : 오늘 이전 날짜 선택 불가)
		onClose: function( selectedDate ) {
			//시작일(startDate) datepicker가 닫힐때 종료일(endDate)의 선택할수있는 최소 날짜(minDate)를 선택한 시작일로 지정
			$("#edate").datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#edate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		showMonthAfterYear: true,
		nextText: '다음 달',
		prevText: '이전 달',
		dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dateFormat: "yy-mm-dd",
//				onClose: function( selectedDate ) {
			// 종료일(endDate) datepicker가 닫힐때 시작일(startDate)의 선택할수있는 최대 날짜(maxDate)를 선택한 시작일로 지정
//					$("#sdate").datepicker( "option", "maxDate", selectedDate );
//				}

	});
});

function SetCookie(name, value, expireDays) {
	var todayDate = new Date();
	todayDate.setDate(todayDate.getDate() + expireDays);
	document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";";
}