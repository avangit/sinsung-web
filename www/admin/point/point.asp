
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="config.asp" -->
		<%
		session("userid") = "admin"
		%>
			<!-- NEW WIDGET START -->
			<article class="">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>전체리스트</h2>
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget content -->
						<div class="widget-body no-padding">
							<%
								'##==========================================================================
								'##
								'##	페이징 관련 함수 - 게시판등...
								'##
								'##==========================================================================
								'## 
								'## [설정법]
								'## 다음 코드가 상단에 위치 하여야 함
								'## 
								dim intTotalCount, intTotalPage
									
								
								dim intNowPage			: intNowPage 		= Request.QueryString("page")    
								dim intPageSize			: intPageSize 		= 20
								dim intBlockPage		: intBlockPage 		= 10
								dim query_where			: query_where		= " p_money <> 0 "
								dim query_filde			: query_filde		= "*"
								dim query_Tablename		: query_Tablename	= tablename
								
								dim search 	: search 	= requestF("search")
								dim keyword : keyword 	= requestF("keyword")
								dim strId	: strId		= requestQ("strId") 
								dim intseq	: intseq	= requestQ("intseq")

								
								
								
'								If isValue(intseq) Then
'									intPageSize 	= 10
'									query_Tablename = query_Tablename&" p, mtb_member2 "
'									query_where		= query_where&"  and intseq  = '"&intseq&"' and p_userid = strId "
								'ElseIf session("userlevel") = 9 Then	
'								Else
'									query_where		= query_where&" and p_userid = '"&session("userid")&"' "
'								End If
								
								If isValue(keyword) Then
									query_where 	= query_where& " and "&search&" like '%"&keyword&"%'"
								End If
								
								
								dim query_orderby		: query_orderby		= " order by p_idx DESC "

								call intTotal

								
								'##
								'## 1. intTotal : call intTotal
								'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
								'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
								'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
								'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
								'## 
								'## 
								'#############################################################################
								call dbopen()
								dim sql, rs
								sql = GetQuery()
								'Response.write sql
								set rs = dbconn.execute(sql)
								
							%>
							<form name = "frm" action = "?<%=getstring("")%>" onsubmit="return regist(this)"  method="post" >
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h search_t smart-form" width="100%">
									<colgroup>
										<col style="width:15%">
										<col style="">
									</colgroup>
									<tbody>
										<th>검색어</th>
										<td>
										<label class="select w10 pro_detail">
										<select class="input-sm h34" name="search" >
											<option value="p_userid">아이디</option>
											<option value="p_title">메모</option>
										</select> <i></i> </label>
										<div class="icon-addon addon-md col-md-10 col-2">
											<input type="text" name="keyword"class="form-control col-10"  id="keyword" value="<%=keyword%>" >
											<label for="email" class="glyphicon glyphicon-search " rel="tooltip" title="email"></label>
										</div>
										
										
										<input type="submit" class="btn btn-default" value="검색">
										</td>
									</tbody>
								</table>
							</form>
							<table id="datatable_fixed_column" class="table table-striped table-bordered smart-form" width="100%">
								 <colgroup>
									<col style="width:20%">
									<col style="width:10%">
									<col style="">
									<col style="width:10%">
									<col style="width:10%">
									<col style="width:10%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand">날짜</th>
										<th data-class="expand">아이디</th>
										<th>적용</th>
										<th data-hide="phone,tablet">적립</th>
										<th data-hide="phone,tablet">사용</th>
										<th data-hide="phone,tablet">작액</th>
									</tr>
								</thead>
	
								<tbody>
									<%
									dim pagei : pagei = (intTotalCount-MoveCount)
									Dim userName,p_num
									'// 글이 없을 경우
									if  rs.eof then
									%>
									<tr>
										<td colspan="6">현재 등록된 포인트가 없습니다.</td>
									</tr>
									<%	
									Else
									rs.move MoveCount 
									Dim plus,minus				
									Do while not rs.eof	
																			
										p_idx		= rs("p_idx")
										p_date 		= rs("p_date")
										p_title		= rs("p_title")
										p_money		= rs("p_money")
										p_remainder	= rs("p_remainder") '//잔액
										p_userid	= rs("p_userid")
										p_num		= rs("p_num")
										plus 		= ""
										minus 		= ""					
										If p_money < 0 Then
											minus = p_money
										Else
											plus = p_money
										End If
										
										userName = getAdoRsScalar("select strName from mtb_member2 where strid = '"&p_userid&"'")

							
									%>
									<tr>
										<td><%=p_date%></td>
										<td>[<%=userName%>]<%=p_userid%></td>
										<td><%=split(p_title,"_")(0)%><%If session("userlevel") = "9" Then%><%If p_num > 0 Then%>[주문번호:<%=p_num%>]<%End If%><%End If%></td>
										<td><%If plus <> "" Then%><%=formatnumber(plus,0)%><%End If%></td>
										<td><%If minus <> "" Then%><%=formatnumber(minus,0)%><%End If%> </td>
										<td><%=formatnumber(p_remainder,0)%> </td>
									</tr>
										
									<%
							
											pagei = pagei-1
										rs.movenext
										loop
									End If
									%>
										
								</tbody>
						
							</table>


							<%call Paging_list("")%>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->



			

			</article>
			<!-- WIDGET END -->

		
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		<!-- #include file="../inc/footer.asp" -->
