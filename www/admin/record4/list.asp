<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<script>
function law_f1(){
    document.getElementById("law_tab1").style.display="block";
	document.getElementById("law_tab2").style.display="none";
	document.getElementById("law_tab3").style.display="none";
	document.getElementById("law_c1").style.background="#ddf0f3";
	document.getElementById("law_c2").style.background="#ffffff";
	document.getElementById("law_c3").style.background="#ffffff";
}
function law_f2(){
    document.getElementById("law_tab1").style.display="none";
	document.getElementById("law_tab2").style.display="block";
	document.getElementById("law_tab3").style.display="none";
	document.getElementById("law_c1").style.background="#ffffff";
	document.getElementById("law_c2").style.background="#ddf0f3";
	document.getElementById("law_c3").style.background="#ffffff";
}
function law_f3(){
    document.getElementById("law_tab1").style.display="none";
	document.getElementById("law_tab2").style.display="none";
	document.getElementById("law_tab3").style.display="block";
	document.getElementById("law_c1").style.background="#ffffff";
	document.getElementById("law_c2").style.background="#ffffff";
	document.getElementById("law_c3").style.background="#ddf0f3";
}
</script>

<script Language="JavaScript">

  window.onload = function(){

  <%if request.querystring("gubun") = "a" then%>
  law_f1();
  <%elseif request.querystring("gubun") = "b" then%>
  law_f2();
  <%elseif request.querystring("gubun") = "c" then%>
  law_f3();
  <%end if%>

}
</script>

<!-- NEW WIDGET START -->
<article>
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
	<!-- Widget ID (each widget will need unique ID)-->
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
		<header>
			<span class="widget-icon"> <i class="fa fa-table"></i> </span>
			<h2>약관/개인정보</h2>
		</header>
		<!-- widget div-->
		<div class="mt50">
			<!-- widget edit box -->
			<div class="jarviswidget-editbox">
				<!-- This area used as dropdown edit box -->
			</div>
			<!-- end widget edit box -->
			<!-- widget content -->
			<div class="widget-body no-padding">
				<%
				Call DbOpen()

				Dim strSQL, arrData

				strSQL = "SELECT * FROM Record_views4 WHERE intSeq = 1"
				arrData = getAdoRsArray(strSQL)

				If isArray(arrData) Then

					Dim law1_yn, law1, law2, law3_yn, law3, law4_yn, law4, law5_yn, law5, law6_yn

					law1_yn			= arrData(1,0)
					law1 			= arrData(2,0)
					law2   			= arrData(3,0)
					law3_yn			= arrData(4,0)
					law3			= arrData(5,0)
					law4_yn			= arrData(6,0)
					law4			= arrData(7,0)
					law5_yn			= arrData(8,0)
					law5			= arrData(9,0)
					law6_yn			= arrData(10,0)

				else

					response.Write("코드에러")

				end If

				Call DbClose()
				%>


				<!--div style="margin:10px;">
				  <table style="margin:10px;">
				    <tr>
					  <td style="border:#dddddd 1px solid;padding:10px;background-color:#ddf0f3;" id="law_c1"><a href="javascript:law_f1();">이용약관</a></td>
				      <td style="border:#dddddd 1px solid;padding:10px;" id="law_c2"><a href="javascript:law_f2();">개인정보 동의항목 설정</a></td>
				      <td style="border:#dddddd 1px solid;padding:10px;" id="law_c3"><a href="javascript:law_f3();">14세 미만 회원가입 설정</a></td>
					</tr>
				  </table>
				</div-->


				<div id="law_tab1">
				<p style="padding:15px;">이용약관 내용</p>
				<form name="joinform1" action = "update.asp?menucode=<%=Request.QueryString("menucode")%>" class="smart-form" method = "post" enctype="multipart/form-data" >
				<input type="hidden" name="gubun" value="a" />
				<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
					 <colgroup>
						<col style="width:15%">
						<col style="">
					</colgroup>
					<tbody>
						<tr>
							<th>사용여부</th>
							<td><input type="radio" name="law1_yn" value="Y" <%If law1_yn = "Y" then%>checked<%End if%>> 사용함 &nbsp;&nbsp;&nbsp; <input type="radio" name="law1_yn" value="N" <%If law1_yn = "N" then%>checked<%End if%>> 사용 안함</td>
						</tr>
						<tr>
							<th>약관내용</th>
							<td><textarea style="width:550px;height:80px;" name="law1"><%=law1%></textarea></td>
						</tr>
					</tbody>
				</table>
				<footer style="float:right;">
					<input type="submit" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
				</footer>
				</form>
				</div>


				<div id="law_tab2" style="display:none;">
				<p style="padding:15px;">회원 대상 동의항목 설정</p>
				<form name="joinform2" action = "update.asp?menucode=<%=Request.QueryString("menucode")%>" class="smart-form" method = "post" enctype="multipart/form-data" >
				<input type="hidden" name="gubun" value="b" />
				<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
					 <colgroup>
						<col style="width:15%">
						<col style="">
					</colgroup>
					<tbody>
					    <tr>
						    <td colspan="2" style="border:none;">[필수] 개인정보수집·이용 동의</td>
						</tr>
						<tr>
							<th>내용입력</th>
							<td><textarea style="width:550px;height:80px;" name="law2"><%=law2%></textarea></td>
						</tr>
						<tr>
						    <td colspan="2" style="border:none;">[필수] 개인정보처리·위탁 동의</td>
						</tr>
						<tr>
							<th>사용여부</th>
							<td><input type="radio" name="law3_yn" value="Y" <%If law3_yn = "Y" then%>checked<%End if%>> 사용함 &nbsp;&nbsp;&nbsp; <input type="radio" name="law3_yn" value="N" <%If law3_yn = "N" then%>checked<%End if%>> 사용 안함</td>
						</tr>
						<tr>
							<th>내용입력</th>
							<td><textarea style="width:550px;height:80px;" name="law3"><%=law3%></textarea></td>
						</tr>
						<tr>
						    <td colspan="2" style="border:none;">[필수] 개인정보 제3자 제공 동의</td>
						</tr>
						<tr>
							<th>사용여부</th>
							<td><input type="radio" name="law4_yn" value="Y" <%If law4_yn = "Y" then%>checked<%End if%>> 사용함 &nbsp;&nbsp;&nbsp; <input type="radio" name="law4_yn" value="N" <%If law4_yn = "N" then%>checked<%End if%>> 사용 안함</td>
						</tr>
						<tr>
							<th>내용입력</th>
							<td><textarea style="width:550px;height:80px;" name="law4"><%=law4%></textarea></td>
						</tr>
						<tr>
						    <td colspan="2" style="border:none;">[선택] 마케팅 활용 동의</td>
						</tr>
						<tr>
							<th>사용여부</th>
							<td><input type="radio" name="law5_yn" value="Y" <%If law5_yn = "Y" then%>checked<%End if%>> 사용함 &nbsp;&nbsp;&nbsp; <input type="radio" name="law5_yn" value="N" <%If law5_yn = "N" then%>checked<%End if%>> 사용 안함</td>
						</tr>
						<tr>
							<th>내용입력</th>
							<td><textarea style="width:550px;height:80px;" name="law5"><%=law5%></textarea></td>
						</tr>
					</tbody>
				</table>
				<footer style="float:right;">
					<input type="submit" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
				</footer>
				</form>
				</div>


				<div id="law_tab3" style="display:none;">
				<p style="padding:15px;">14세 미만 회원가입 설정</p>
				<form name="joinform3" action = "update.asp?menucode=<%=Request.QueryString("menucode")%>" class="smart-form" method = "post" enctype="multipart/form-data" >
				<input type="hidden" name="gubun" value="c" />
				<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
					 <colgroup>
						<col style="width:15%">
						<col style="">
					</colgroup>
					<tbody>
						<tr>
							<th>사용여부</th>
							<td><input type="radio" name="law6_yn" value="Y" <%If law6_yn = "Y" then%>checked<%End if%>> 사용함 &nbsp;&nbsp;&nbsp; <input type="radio" name="law6_yn" value="N" <%If law6_yn = "N" then%>checked<%End if%>> 사용 안함</td>
						</tr>
					</tbody>
				</table>
				<footer style="float:right;">
					<input type="submit" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
				</footer>
				</form>
				</div>


			</div>
			<!-- end widget content -->

		</div>
		<!-- end widget div -->

	</div>
	<!-- end widget -->

</article>
<!-- WIDGET END -->
<script>
	$(document).ready(function() {

		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		pageSetUp();

	});

</script>
<!-- #include file="../inc/footer.asp" -->