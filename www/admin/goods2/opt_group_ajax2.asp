<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include file="config_product2.asp" -->

<%
intVal			= SQL_Injection(Trim(Request.Form("val")))
g_optionList	= SQL_Injection(Trim(unescape(Request.Form("opt"))))

'// 0이면 초기화
If intVal <> 0 Then
	Call DbOpen()

	sql = "SELECT ogroup_content FROM cate_option_group WHERE ogroup_idx = " & intVal & ""

	Set rs = dbconn.execute(sql)

	If Not rs.eof Then
		ogroup_content = rs("ogroup_content")

		arr_ogroup_content = Split(ogroup_content, ", ")
	End If

	rs.close

	For i = 0 To Ubound(arr_ogroup_content)
		sql = "SELECT opt_name, opt_content FROM cate_option WHERE opt_name = '" & Trim(arr_ogroup_content(i)) & "'"
		Set rs = dbconn.execute(sql)

		If Not rs.eof Then
			opt_name = rs("opt_name")
			opt_content = rs("opt_content")

			arr_opt_content = Split(opt_content, ", ")
		End If

		rs.close
%>
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="width:*">
								</colgroup>
								<tbody>
									<tr>
										<th><%=opt_name%></th>
										<td>
											<label class="select col-3">
											<select class="input-sm"  name="g_optionList<%=i+1%>" id="g_optionList<%=i+1%>">
												<option value="">-- 선택 --</option>
										<% For j = 0 To Ubound(arr_opt_content) %>
												<option value="<%=opt_name%>||<%=arr_opt_content(j)%>"><%=arr_opt_content(j)%></option>
										<% Next %>
											</select> <i></i> </label>
										</td>
									</tr>
								</tbody>
							</table>
<%
	Next

	Call DbClose()
End If
%>