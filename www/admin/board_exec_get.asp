<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
b_idx		= SQL_Injection(Trim(Request("n")))
mode		= SQL_Injection(Trim(Request("m")))
menucode	= SQL_Injection(Trim(Request("menucode")))
page		= SQL_Injection(Trim(Request("page")))
bs_code		= SQL_Injection(Trim(Request("bs_code")))

If b_idx = "" Or ISNULL(b_idx) Then
	Call jsAlertMsgBack("접근경로가 올바르지 않습니다.")
Else
	b_idx = Replace(b_idx, " ", ",")
End If

Call DbOpen()

'// 회원삭제 시
If mode = "del" Then
	message = "삭제"
	dbconn.execute("DELETE FROM BOARD_v1 WHERE b_part = '" & bs_code & "' AND b_idx IN (" & b_idx & ")")

Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call DbClose()

Call jsAlertMsgUrl("" & message & "되었습니다.", "/admin/sub.asp?menucode=m10s41&path=/AVANplus/modul/avanboard_v3/call2.asp&bs_code="&bs_code&"")
%>