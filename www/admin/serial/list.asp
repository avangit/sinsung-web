<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->
<!-- #include file="config.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<style>
.hasDatepicker {
	position: relative;
	z-index: 9999;
}
</style>
<script>
$(document).ready(function() {
	$("#serial_chg").click(function(){
		var frm = document.list;

		 var len = parseInt($("input[name^=serial]").length);
		 $("input[name=arr_val_list]").val("");

		 if(!len || len==0) {
			alert('데이터가 없습니다.');
		 } else {
			var arr_val_list = new Array();
			var j = 0;

			$("input[name^=serial]").each(function(i){
				arr_val_list[i] = $(this).attr("rel");
				j++;
			});

			$("input[name=arr_val_list]").val(arr_val_list.join('@'));

			if(!j || j==0) {
				$("input[name=arr_val_list]").val("");
			} else {
				document.list.submit();
			}
		 }
	});
});
</script>
<article>

  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>시리얼관리</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(RequestS("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(RequestS("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request.QueryString("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= "*"
Dim query_Tablename		: query_Tablename	= TableName
Dim query_where			: query_where		= ""
Dim query_orderby		: query_orderby		= " ORDER BY idx DESC"

sdate		= SQL_Injection(Trim(RequestS("sdate")))
edate 		= SQL_Injection(Trim(RequestS("edate")))

If sdate <> "" And edate <> "" Then
	query_where = query_where &  " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day,1,'"&edate&"')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where &  " AND regdate >= '" & sdate  &"'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where &  " AND regdate <= DATEADD(day,1,'"&edate&"')"
End If


query_where = " idx > 0 "
If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
		<form name = "frm" id="frm" action = "" class="smart-form m10"  method="post" >
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table class="table table-striped table-bordered line-h" width="100%">
			<tr>
				<th>검색</th>
				<td colspan="3">
					<label class="select w10 pro_detail">
					<select class="input-sm h34 sel_list" name="fieldname" id="fieldname">

						<!--option value="o_company" <% If fieldname = "o_company" Then %> selected<% End If %>>회사명</option-->
						<!--option value="strName" <% If fieldname = "strName" Then %> selected<% End If %>>제품명</option-->
						<option value="serial" <% If fieldname = "serial" Then %> selected<% End If %>>시리얼번호</option>
						<option value="key_order" <% If fieldname = "key_order" Then %> selected<% End If %>>주문번호</option>
						<option value="username" <% If fieldname = "username" Then %> selected<% End If %>>사용자</option>
						<option value="partname" <% If fieldname = "partname" Then %> selected<% End If %>>부서</option>
					</select>
					</label>
					<div class="icon-addon addon-md col-md-10 col-4 col-new">
						<input type="text" name="fieldvalue" id="fieldvalue" class="form-control col-10 ip_sereal" value="<%=fieldvalue%>">
						<label for="fieldvalue" class="glyphicon glyphicon-search " rel="tooltip" title="fieldvalue"></label>
					</div>
					<a href="javascript:frm.submit();" class="btn btn-primary"><i class="fa fa-search"></i> 검색</a>
				</td>
			</tr>
		</table>
		</form>

		<p class="cont_txt2"><%=intTotalCount%>건</p>
		<form name="list" method="post" action="./serial_exec.asp" style="margin:0;">
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="message">
		<input type="hidden" name="arr_val_list">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<input type="hidden" name="page" value="<%=intNowPage%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:5%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:15%">
			<col style="width:10%">
		</colgroup>
		<thead>
			<tr>
				<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
				<th data-class="expand">주문일자</th>
				<th data-hide="phone,tablet">주문번호</th>
				<th data-class="expand">제품코드</th>
				<th data-class="expand">제품명</th>
				<th data-class="expand">시리얼번호</th>
				<th data-hide="phone,tablet">사용자</th>
				<th data-hide="phone,tablet">부서</th>
			</tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="8" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	rs.move MoveCount
	Do while Not rs.eof

	'	o_idx			= rs("o_idx")
	'	o_num 			= rs("o_num")
	'	o_company 		= rs("o_company")
	'	g_idx 			= rs("g_idx")
	'	o_serial		= rs("o_serial")
	'	strId 			= rs("strId")
	'	o_department 	= rs("o_department")
	'	regdate 		= rs("regdate")
	orderNum = getAdoRsScalar("select o_idx from orders where o_num = '"&rs("key_order")&"'")
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><input type="checkbox" value="<%=rs("idx")%>"></td>
				<td><%=getAdoRsScalar("select regdate from orders where o_num = '"&rs("key_order")&"'")%></td>
				<td><a href="/admin/order/order_view.asp?idx=<%=orderNum%>"><%=rs("key_order")%></a></td>
				<td><a href="/admin/goods/product_view.asp?menucode=m05s28&g_idx=<%=rs("key_g_idx")%>"><%=rs("key_g_idx")%></a></td>
				<td><%=rs("productName")%></a></td>
				<td><input type="text" name="serial<%=rs("idx")%>" value="<%=rs("serial")%>" rel="<%=rs("idx")%>"></td>
				<td><input type="text" name="s_user<%=rs("idx")%>" value="<%=rs("username")%>" rel="<%=rs("idx")%>"></td>
				<td><input type="text" name="s_part<%=rs("idx")%>" value="<%=rs("partname")%>" rel="<%=rs("idx")%>"></td>
			</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
		</tbody>
		</table>

		<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:10 0 0 0;">
			<tr>
				<!--td>
					<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
				</td-->
				<td align="right">
					<a href="javascript:;" class="btn btn-primary" id="serial_chg"><i class="fa fa-pencil"></i> 시리얼저장</a>
				</td>
			</tr>
		</table>
		</form>

		<%call Paging_list("")%>
	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->