
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>BASIC</h2>
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">

							<%

							dim intseq
							'//검색처리부분
							'//일반과 검색을 위한 설정
							dim where, keyword, keyword_option

							keyword 		= requestS("keyword")
							keyword_option 	= requestS("keyword_option")
							'response.write GetString("keyword="&keyword&"&keyword_option="&keyword_option)
							'//폼으로 넘어온경우 백로가면 검색화면 페이지표시할수 없음처리
							if len(request.Form("keyword")) > 0 then response.Redirect("?"&GetString("keyword="&keyword&"&keyword_option="&keyword_option))


							'//검색일경우 첫페이지로 돌리기 위한 설정
							if len(keyword_option) > 0 then
								where = " intSeq > 0 and "& keyword_option &" like '%"& keyword &"%' "
							else
								where  = " intSeq > 0 "
							end if


							'//카테고리 값이 넘어오는 경우 처리
							if len(requestQ("mcate")) > 0 then	where = where & " and strCategory = '"& requestQ("mcate") &"' "



							'##==========================================================================
							'##
							'##	페이징 관련 함수 - 게시판등...
							'##
							'##==========================================================================
							'##
							'## [설정법]
							'## 다음 코드가 상단에 위치 하여야 함
							'##
							dim intTotalCount, intTotalPage

							dim intNowPage			: intNowPage 		= Request.QueryString("page")
							dim intPageSize			: intPageSize 		= 10
							dim intBlockPage		: intBlockPage 		= 10

							dim query_filde			: query_filde		= "*"
							dim query_Tablename		: query_Tablename	= "BASIC"
							dim query_where			: query_where		= where
							dim query_orderby		: query_orderby		= " order by intSeq DESC "
							call intTotal

							'##
							'## 1. intTotal : call intTotal
							'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
							'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
							'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
							'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
							'##
							'##
							'#############################################################################

							dim sql, rs
							sql = GetQuery()
							'response.Write(sql)
							call dbopen
							set rs = dbconn.execute(sql)
						%>



							<form name = "frm" action = "?<%=getstring("")%>" onsubmit="return regist(this)" class="smart-form m10"  method="post" >
								<label class="select w10 pro_detail">
								<select class="input-sm h34" name="keyword_option" >
									<option value="strTitle">제목</option>
									<option value="strName">이름</option>
								</select> <i></i> </label>
								<div class="icon-addon addon-md col-md-10 col-2">
									<input type="text" name="keyword" class="form-control col-10">
									<label for="keyword" class="glyphicon glyphicon-search " rel="tooltip" title="keyword"></label>
								</div>
								<input type="submit" class="btn btn-default" value="검색">
								
							</form>	
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								 <colgroup>
									<col style="width:5%">
									<col style="">
									<col style="width:10%">
									<col style="width:15%">
									<col style="width:10%">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand">번호</th>
										<th data-class="expand">제목</th>
										<th>이름</th>
										<th data-hide="phone,tablet">첨부파일</th>
										<th data-hide="phone,tablet">작성일</th>
									</tr>
								</thead>
	
								<tbody>
									<%
										dim pagei : pagei = (intTotalCount-MoveCount)
										'// 글이 없을 경우
										if  rs.eof then
									%>
									<%

										Else

									%>
									<%
										rs.move MoveCount
										Do while not rs.eof

									%>
									<tr>
										<td><%=pagei%></td>
										<td><a href="basic_modify.asp?intSeq=<%=rs("intseq")%>"><%=rs("strTitle")%></a></td>
										<td><%=rs("strName")%></td>
										<td><% call FileDownLink_uppath(rs("strImage"),"/upload/basic/")%></td>
										<td><%=rs("strDate")%></td>
									</tr>
									
								
									<%
										pagei = pagei-1
										rs.movenext
										loop
										rs.close()
										set rs = nothing
									End If

									call DbClose()
									%>
									
								</tbody>
						
							</table>

							<footer style="float:right;" class="m_button">
								<button type="submit" class="btn btn-primary" onclick="location.href='basic_form.asp'">작성하기</button>
							</footer>
							<%call Paging_list("")%>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->



			

			</article>
			<!-- WIDGET END -->
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		
		<!-- #include file="../inc/footer.asp" -->
