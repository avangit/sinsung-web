<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->
<!-- #include file="./pop_config.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<style>
.hasDatepicker {
	position: relative;
	z-index: 9999;
}
</style>
<article>
<script src="/admin/js/jquery-2.1.1.min.js"></script>
  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>팝업관리</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request.QueryString("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= "*"
Dim query_Tablename		: query_Tablename	= "popup"
Dim query_where			: query_where		= " pop_idx > 0 "
Dim query_orderby		: query_orderby		= " ORDER BY pop_idx DESC"


Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

'response.Write getQuery

sql = getQuery
Call dbopen
Set rs = dbconn.execute(sql)

menucode = SQL_Injection(Trim(RequestS("menucode")))
%>

		<%=intTotalCount%>건
		<footer style="float:right;" class="m_button">
			<a href="./pop_write.asp?menucode=<%=menucode%>" class="btn btn-primary"><i class="fa fa-pencil"></i> 등록</a>
		</footer>

		<form name="list" method="get" action="./pop_exec_get.asp" style="margin:0;">
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:5%">
			<col style="width:10%">
			<col style="width:15%">
			<col style="width:*">
			<col style="width:5%">
			<col style="width:15%">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>출력</th>
				<th>기간</th>
				<th>제목</th>
				<th>상태</th>
				<th>관리</th>
			</tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="6" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	rs.move MoveCount
	Do while Not rs.eof

		idx			= rs("pop_idx")
		display 	= rs("display")
		useYN 		= rs("useYN")

		If display = "home" Then
			display = "홈페이지"
		ElseIf display = "srm" Then
			display = "SRM"
		ElseIf display = "both" Then
			display = "홈페이지, SRM"
		End If

		If useYN = "Y" Then
			useYN = "사용"
		Else
			useYN = "미사용"
		End If
%><!-- ./pop_exec_get.asp?menucode=<%=menucode%>&idx=<%=idx%>&m=del -->
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><%=intNowNum%></td>
				<td><%=display%></td>
				<td><%=rs("sdate")%> ~ <%=rs("edate")%></td>
				<td><a href="pop_write.asp?menucode=<%=menucode%>&idx=<%=idx%>"><%=rs("title")%></a></td>
				<td><%=useYN%></td>
				<td>
					<a href="./pop_write.asp?menucode=<%=menucode%>&idx=<%=idx%>" class="btn btn-primary"><i class="fa fa-pencil"></i> 수정</a>
					<a href="./pop_exec_get.asp?menucode=<%=menucode%>&idx=<%=idx%>&m=del" class="btn btn-default" onclick="if(confirm('선택하신 팝업을 삭제하시겠습니까?')){}else{return false;}"><i class="fa fa-pencil"></i> 삭제</a>
				</td>
			</tr>
<%
		intNowNum = intNowNum - 1
		rs.movenext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
		</tbody>
		</table>

		</form>

		<%call Paging_list("")%>
	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->