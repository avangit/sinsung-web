<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
Dim menucode, bn_idx, mode

menucode	= SQL_Injection(Trim(Request.QueryString("menucode")))
bn_idx		= SQL_Injection(Trim(Request.QueryString("n")))
mode		= SQL_Injection(Trim(Request.QueryString("m")))

If bn_idx = "" Or ISNULL(bn_idx) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","history.back();")
'Else
'	bn_idx = Replace(bn_idx, " ", ",")
End If

Call dbOpen()

'// 삭제 시
If mode = "del" Then
	message = "삭제"

	sql = "SELECT strFile1 FROM banner WHERE bn_idx = " & bn_idx

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then

		Set UploadForm = Server.Createobject("DEXT.FileUpload")
		UploadForm.DefaultPath = Server.MapPath("/upload/banner/")

		'기존파일 삭제
		If rs("strFile1") <> "" Then
			UploadForm.Deletefile UploadForm.DefaultPath & "\" & rs("strFile1")
		End If

		Set UploadForm = Nothing
	End If

	dbconn.execute("DELETE FROM banner WHERE bn_idx = " & bn_idx & "")
'		dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES ('"&session("aduserid")&"','"&strId&"','삭제','하위회원삭제','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

	rs.close
	Set rs = Nothing

Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("삭제되었습니다.", "./banner_list.asp?menucode="&menucode)
%>