<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!-- #include file="../member/config.asp" -->

<%
intseq			= SQL_Injection(Trim(Request.Form("n")))
mode			= SQL_Injection(Trim(Request.Form("m")))
strId			= SQL_Injection(Trim(Request.Form("strId")))
strPwd			= SQL_Injection(Trim(Request.Form("strPwd")))
strName			= SQL_Injection(Trim(Request.Form("strName")))
strPhone1		= SQL_Injection(Trim(Request.Form("strPhone1")))
strPhone2		= SQL_Injection(Trim(Request.Form("strPhone2")))
strPhone3		= SQL_Injection(Trim(Request.Form("strPhone3")))
strMobile1		= SQL_Injection(Trim(Request.Form("strMobile1")))
strMobile2		= SQL_Injection(Trim(Request.Form("strMobile2")))
strMobile3		= SQL_Injection(Trim(Request.Form("strMobile3")))
strEmail1		= SQL_Injection(Trim(Request.Form("strEmail1")))
strEmail2		= SQL_Injection(Trim(Request.Form("strEmail2")))

'// 변수 가공
strPhone = strPhone1 & "-" & strPhone2 & "-" & strPhone3
strMobile = strMobile1 & "-" & strMobile2 & "-" & strMobile3
strEmail = strEmail1 & "@" & strEmail2
intGubun = 9
smsYN = "N"
mailYN = "N"
yeosinYN = "N"
cName = ""
cNum = ""
cZip = ""
cAddr1 = ""
cAddr2 = ""

Call DbOpen()

If mode = "reg" Then

	Sql = "INSERT INTO mTb_Member2("
	Sql = Sql & "intGubun, "
	Sql = Sql & "strId, "
	Sql = Sql & "strPwd, "
	Sql = Sql & "strName, "
	Sql = Sql & "strPhone, "
	Sql = Sql & "strMobile, "
	Sql = Sql & "smsYN, "
	Sql = Sql & "strEmail, "
	Sql = Sql & "mailYN, "
	Sql = Sql & "yeosinYN, "
	Sql = Sql & "cName, "
	Sql = Sql & "cNum, "
	Sql = Sql & "cZip, "
	Sql = Sql & "cAddr1, "
	Sql = Sql & "cAddr2) VALUES("
	Sql = Sql & "'" & intGubun & "',"
	Sql = Sql & "N'" & strId & "',"
	Sql = Sql & "'" & Encrypt_Sha(strPwd) & "',"
	Sql = Sql & "N'" & strName & "',"
	Sql = Sql & "'" & strPhone & "',"
	Sql = Sql & "'" & strMobile & "',"
	Sql = Sql & "'" & smsYN & "',"
	Sql = Sql & "N'" & strEmail & "',"
	Sql = Sql & "'" & mailYN & "',"
	Sql = Sql & "'" & yeosinYN & "',"
	Sql = Sql & "N'" & cName & "',"
	Sql = Sql & "'" & cNum & "',"
	Sql = Sql & "'" & cZip & "',"
	Sql = Sql & "N'" & cAddr1 & "',"
	Sql = Sql & "N'" & cAddr2 & "')"

	dbconn.execute(Sql)
	dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&session("aduserid")&"','"&strId&"','생성','관리자등록','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

ElseIf intseq <> "" And mode = "upt" Then

	Sql = "UPDATE " & tablename & " SET "
	If strPwd <> "" Then
		Sql = Sql & "strPwd='" & Encrypt_Sha(strPwd) & "', "
	End If
	Sql = Sql & "strName = N'" & strName & "', "
	Sql = Sql & "strPhone = '" & strPhone & "', "
	Sql = Sql & "strMobile = '" & strMobile & "', "
	Sql = Sql & "strEmail = N'" & strEmail & "', "
	Sql = Sql & "update_date = '" & Date() & "' "
	Sql = Sql & " WHERE intseq = '" & intseq & "'"

	dbconn.execute(Sql)

	sql = "SELECT strId FROM " & tablename & " WHERE intseq = " & intseq & " AND intGubun = 9"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		strId 	= rs("strId")
	End If

	rs.close
	Set rs = Nothing

	dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&session("aduserid")&"','"&strId&"','변경','관리자수정','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

End If

Call DbClose()

If mode = "reg" Then
	Call jsAlertMsgUrl("관리자 등록이 완료되었습니다.", "./admin_list.asp?menucode="&menucode)
ElseIf mode = "upt" Then
	Call jsAlertMsgUrl("관리자 수정이 완료되었습니다.", "./admin_write.asp?menucode="&menucode&"&intseq=" & intseq)
End If
%>