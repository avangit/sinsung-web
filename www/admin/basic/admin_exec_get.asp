<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!-- #include file="../member/config.asp" -->

<%
intseq		= SQL_Injection(Trim(Request.QueryString("n")))
mode		= SQL_Injection(Trim(Request.QueryString("m")))
message		= SQL_Injection(Trim(Request.QueryString("message")))
menucode	= SQL_Injection(Trim(Request.QueryString("cd")))
page		= SQL_Injection(Trim(Request.QueryString("page")))

If intseq = "" Or ISNULL(intseq) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","history.back();")
Else
	intseq = Replace(intseq, " ", ",")
End If

href = "admin_list.asp?page=" & page & "&menucode=" & menucode

Call dbOpen()

'// 삭제 시
If mode = "del" Then
	message = "삭제"

	dbconn.execute("DELETE FROM " & tablename & " WHERE intseq IN (" & intseq & ") AND intGubun = 9")

	sql = "SELECT strId FROM " & tablename & " WHERE intseq IN (" & intseq & ") AND intGubun = 9"

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
			strId 		= rs("strId")

			dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES ('"&session("aduserid")&"','"&strId&"','삭제','하위회원삭제','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

			rs.movenext
		Loop
	End If

	rs.close
	Set rs = Nothing

'// 상태변경 시
ElseIf mode = "etc" Then
	If message = "사용" Then
		mem_status = 0
	ElseIf message = "미사용" Then
		mem_status = 1
	End If

	dbconn.execute("UPDATE " & tablename & " SET intAction = '" & mem_status & "' WHERE intseq IN (" & intseq & ") AND intGubun = 9")

	sql = "SELECT strId FROM " & tablename & " WHERE intseq IN (" & intseq & ") AND intGubun = 9"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
			strId 		= rs("strId")
			dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&session("aduserid")&"','"&strId&"','"&message&"','관리자상태변경','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

			rs.movenext
		Loop
	End If

	rs.close
	Set rs = Nothing

Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("" & message & "되었습니다.", href)
%>