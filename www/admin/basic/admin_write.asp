
		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="../member/config.asp" -->
<%
Dim mode
intseq = Request.QueryString("intseq")

If intseq = "" Or isnull(intseq) Then
	mode = "reg"
Else
	mode = "upt"
End If

If intseq <> "" Then
	Call DbOpen()

	sql = "SELECT strId, strName, strPhone, strMobile, strEmail FROM " & tablename & " WHERE intseq = " & intseq & " AND intGubun = 9"

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		strId 		= rs("strId")
		strName 	= rs("strName")
		strPhone 	= rs("strPhone")
		strMobile 	= rs("strMobile")
		strEmail 	= rs("strEmail")
	End If

	If strPhone <> "" Then
		ArrPhone = Split(strPhone, "-")
		If Ubound(ArrPhone) = 2 Then
			strPhone1 = ArrPhone(0)
			strPhone2 = ArrPhone(1)
			strPhone3 = ArrPhone(2)
		End If
	End If

	If strMobile <> "" Then
		ArrMobile = Split(strMobile, "-")
		If Ubound(ArrMobile) = 2 Then
			strMobile1 = ArrMobile(0)
			strMobile2 = ArrMobile(1)
			strMobile3 = ArrMobile(2)
		End If
	End If

	If strEmail <> "" Then
		ArrstrEmail = Split(strEmail, "@")
		If Ubound(ArrstrEmail) = 1 Then
			strEmail1 = ArrstrEmail(0)
			strEmail2 = ArrstrEmail(1)
		End If
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
End If
%>
			<!-- NEW WIDGET START -->
			<article>
			<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>관리자정보</h2>
					</header>

					<!-- widget div-->
					<div class="mt50">

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<form name = "joinform" action = "admin_exec.asp" class="smart-form" method="post" <%If mode = "reg" Then %>onsubmit="join_chk_admin();return false;"<% End If %>>
							<input type="hidden" name="num" value="">
							<input type="hidden" name="n" value="<%=intseq%>">
							<input type="hidden" name="m" value="<%=mode%>">
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>아이디 *</th>
										<td colspan="3">
										<% If mode = "reg" Then %>
											<input type="text" name="strId" id="strId">&nbsp;&nbsp;<input type="button" onclick="id_check_admin()" value="중복체크">
											<span class="add_tx" id="id_ck" style="padding-left:0px; margin-left:0px;"></span>
										<% Else %>
											<%=strId%>
										<% End If %>
										</td>
									</tr>
									<tr>
										<th>비밀번호 *</th>
										<td><input type="password" name="strPwd" id="strPwd"><button value="중복테크"></button></td>
										<th>비밀번호 재확인 *</th>
										<td><input type="password" name="strPwdRe" id="strPwdRe"></td>
									</tr>
									<tr>
										<th>이름 *</th>
										<td><input type="text" name="strName" id="strName" value="<%=strName%>"></td>
										<th>전화번호</th>
										<td>
											<input type="text" name="strPhone1" id="strPhone1" style="width:50px" maxlength="4" value="<%=strPhone1%>"> - <input type="text" name="strPhone2" id="strPhone2" style="width:50px" maxlength="4" value="<%=strPhone2%>"> - <input type="text" name="strPhone3" id="strPhone3" style="width:50px" maxlength="4" value="<%=strPhone3%>">
										</td>
									</tr>
									<tr>
										<th>휴대폰번호 *</th>
										<td colspan="3">
											<input type="text" name="strMobile1" id="strMobile1" style="width:50px" maxlength="4" value="<%=strMobile1%>"> - <input type="text" name="strMobile2" id="strMobile2" style="width:50px" maxlength="4" value="<%=strMobile2%>"> - <input type="text" name="strMobile3" id="strMobile3" style="width:50px" maxlength="4" value="<%=strMobile3%>"></td>
									</tr>
									<tr>
										<th>이메일주소 *</th>
										<td colspan="3">
											<input type="text" name="strEmail1" id="strEmail1" style="width:150px" value="<%=strEmail1%>"> @ <input type="text" name="strEmail2" id="strEmail2" style="width:150px" value="<%=strEmail2%>" readonly>
											<select name="strEmail3" id="strEmail3" onchange="ChangeEmail1();">
												<option value="">메일주소 선택</option>
												<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												<option value="empal.com">empal.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="lycos.co.kr">lycos.co.kr</option>
												<option value="naver.com">naver.com</option>
												<option value="nate.com">nate.com</option>
												<option value="netsgo.com">netsgo.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="paran.com">paran.com</option>
												<option value="0">직접입력</option>
											</select><i></i>
										</td>
									</tr>
								</tbody>

							</table>

							<footer style="float:right;">
							<% If mode = "reg" Then %>
								<input type="submit" class="btn btn-primary" value="등록">
							<% Else %>
								<input type="submit" class="btn btn-primary" value="수정">
							<% End If %>
								<a href="./admin_list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
							</footer>
							</form>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->
			</article>
			<!-- WIDGET END -->

		<script>
			$(document).ready(function() {
				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			});

		</script>
		<!-- #include file="../inc/footer.asp" -->