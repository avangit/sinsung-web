<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!-- #include file="./pop_config.asp" -->

<%
pop_idx			= SQL_Injection(Trim(Request.Form("n")))
mode			= SQL_Injection(Trim(Request.Form("m")))
display			= SQL_Injection(Trim(Request.Form("display")))
sdate			= SQL_Injection(Trim(Request.Form("sdate")))
edate			= SQL_Injection(Trim(Request.Form("edate")))
pop_type		= SQL_Injection(Trim(Request.Form("pop_type")))
hide_type		= SQL_Injection(Trim(Request.Form("hide_type")))
scrollYN		= SQL_Injection(Trim(Request.Form("scrollYN")))
useYN			= SQL_Injection(Trim(Request.Form("useYN")))
pop_width		= SQL_Injection(Trim(Request.Form("pop_width")))
pop_height		= SQL_Injection(Trim(Request.Form("pop_height")))
pop_top			= SQL_Injection(Trim(Request.Form("pop_top")))
pop_left		= SQL_Injection(Trim(Request.Form("pop_left")))
title			= SQL_Injection(Trim(Request.Form("title")))
contents		= SQL_Injection(Trim(Request.Form("contents")))

If pop_type = "win" Then
	HtmlName = Replace(Date(), "-", "") & "_" & Replace(FormatDateTime(Now(), 4), ":", "") & ".html"
End If

If scrollYN <> "yes" Then
	scrollYN = "no"
End If

If pop_top = "" Or ISNULL(pop_top) Then
	pop_top = 10
End If

If pop_left = "" Or ISNULL(pop_left) Then
	pop_left = 10
End If

Call DbOpen()

If mode = "reg" Then

	Sql = "SET NOCOUNT ON;"
	Sql = Sql & "INSERT INTO " & tablename & "("
	Sql = Sql & "display, "
	Sql = Sql & "sdate, "
	Sql = Sql & "edate, "
	Sql = Sql & "pop_type, "
	Sql = Sql & "hide_type, "
	Sql = Sql & "scrollYN, "
	Sql = Sql & "useYN, "
	Sql = Sql & "pop_width, "
	Sql = Sql & "pop_height, "
	Sql = Sql & "pop_top, "
	Sql = Sql & "pop_left, "
	Sql = Sql & "title, "
	Sql = Sql & "htmlFile, "
	Sql = Sql & "contents) VALUES("
	Sql = Sql & "'" & display & "',"
	Sql = Sql & "'" & sdate & "',"
	Sql = Sql & "'" & edate & "',"
	Sql = Sql & "'" & pop_type & "',"
	Sql = Sql & "'" & hide_type & "',"
	Sql = Sql & "'" & scrollYN & "',"
	Sql = Sql & "'" & useYN & "',"
	Sql = Sql & "'" & pop_width & "',"
	Sql = Sql & "'" & pop_height & "',"
	Sql = Sql & "'" & pop_top & "',"
	Sql = Sql & "'" & pop_left & "',"
	Sql = Sql & "N'" & title & "',"
	Sql = Sql & "'" & HtmlName & "',"
	Sql = Sql & "N'" & contents & "');SELECT SCOPE_IDENTITY();"
	Sql = Sql & "SET NOCOUNT OFF;"

	Set rs = dbconn.execute(sql)

	idx = rs(0)

	rs.close
	Set rs = Nothing

	'// 브라우저 방식일 경우 html 파일 생성
	If pop_type = "win" Then

		HtmlPath = Server.MapPath("/upload/popup/") & "\" & HtmlName

		FileText = ""
		FileText = FileText & "<html>" & chr(13)
		FileText = FileText & chr(9) & "<head>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<title>" & title & "</title>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<script language=""JavaScript"" src=""" & GB_SiteVirtual & "/js/common.js""></script>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<style>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & chr(9) & "body, div, table, tr, td { margin:0; font:normal 12px 굴림; color:#333333; }" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "</style>" & chr(13)
		FileText = FileText & chr(9) & "</head>" & chr(13)
		FileText = FileText & chr(9) & "<body>" & chr(13)
		FileText = FileText & chr(9) & "<table width=""" & pop_width & """ height=""" & pop_height & """ cellpadding=""0"" cellspacing=""0"" border=""0"">" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<tr valign=""top"">" & chr(13)
		FileText = FileText & chr(9) & chr(9) & chr(9) & "<td>" & chr(13)
		FileText = FileText & "" & chr(13)
		FileText = FileText & contents & chr(13)
		FileText = FileText & "" & chr(13)
		FileText = FileText & chr(9) & chr(9) & chr(9) & "</td>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "</tr>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<tr height=""1"">" & chr(13)
		If hide_type = "day" Then
			FileText = FileText & chr(9) & chr(9) & chr(9) & "<td align=""right"" style=""padding:10 25 10 25;"">오늘 하루는 이 창을 열지 않습니다 <input type=""checkbox"" style=""width:16px; height:15px;"" onfocus=""blur();"" onclick=""SetCookie('NOTICE" & idx & "', 'Y', 1); window.close();"" border=""0""></td>" & chr(13)
		ElseIf hide_type = "none" Then
			FileText = FileText & chr(9) & chr(9) & chr(9) & "<td align=""right"" style=""padding:10 25 10 25;"">이 창을 다시는 띄우지 않습니다 <input type=""checkbox"" style=""width:16px; height:15px;"" onfocus=""blur();"" onclick=""SetCookie('NOTICE" & idx & "', 'Y', 365); window.close();"" border=""0""></td>" & chr(13)
		ElseIf hide_type = "always" Then
			FileText = FileText & chr(9) & chr(9) & chr(9) & "<td align=""right"" style=""padding:10 25 10 25;"">창 닫기 <input type=""checkbox"" style=""width:16px; height:15px;"" onfocus=""blur();"" onclick=""window.close();"" border=""0""></td>" & chr(13)
		End If
		FileText = FileText & chr(9) & chr(9) & "</tr>" & chr(13)
		FileText = FileText & chr(9) & "</table>" & chr(13)
		FileText = FileText & chr(9) & "</body>" & chr(13)
		FileText = FileText & "</html>"

		Call FileWrite_UTF8(HtmlPath, FileText)
	End If

'	dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&session("aduserid")&"','"&title&"','생성','팝업등록','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

ElseIf pop_idx <> "" And mode = "upt" Then

	If pop_type = "win" Then
		sql = "SELECT htmlFile FROM " & tablename & " WHERE pop_idx = '" & pop_idx & "'"
		Set rs = dbconn.execute(sql)

		If Not(rs.bof Or rs.eof) Then
			htmlFile = rs("htmlFile")
			HtmlPath = Server.MapPath("/upload/popup/") & "\" & htmlFile
		End If

		rs.close
		Set rs = Nothing

		FileText = ""
		FileText = FileText & "<html>" & chr(13)
		FileText = FileText & chr(9) & "<head>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<title>" & title & "</title>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<script language=""JavaScript"" src=""" & GB_SiteVirtual & "/js/common.js""></script>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<style>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & chr(9) & "body, div, table, tr, td { margin:0; font:normal 12px 굴림; color:#333333; }" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "</style>" & chr(13)
		FileText = FileText & chr(9) & "</head>" & chr(13)
		FileText = FileText & chr(9) & "<body>" & chr(13)
		FileText = FileText & chr(9) & "<table width=""" & pop_width & """ height=""" & pop_height & """ cellpadding=""0"" cellspacing=""0"" border=""0"">" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<tr valign=""top"">" & chr(13)
		FileText = FileText & chr(9) & chr(9) & chr(9) & "<td>" & chr(13)
		FileText = FileText & "" & chr(13)
		FileText = FileText & contents & chr(13)
		FileText = FileText & "" & chr(13)
		FileText = FileText & chr(9) & chr(9) & chr(9) & "</td>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "</tr>" & chr(13)
		FileText = FileText & chr(9) & chr(9) & "<tr height=""1"">" & chr(13)
		If hide_type = "day" Then
			FileText = FileText & chr(9) & chr(9) & chr(9) & "<td align=""right"" style=""padding:10 25 10 25;"">오늘 하루는 이 창을 열지 않습니다 <input type=""checkbox"" style=""width:16px; height:15px;"" onfocus=""blur();"" onclick=""SetCookie('NOTICE" & pop_idx & "', 'done', 1); window.close();"" border=""0""></td>" & chr(13)
		ElseIf hide_type = "none" Then
			FileText = FileText & chr(9) & chr(9) & chr(9) & "<td align=""right"" style=""padding:10 25 10 25;"">이 창을 다시는 띄우지 않습니다 <input type=""checkbox"" style=""width:16px; height:15px;"" onfocus=""blur();"" onclick=""SetCookie('NOTICE" & pop_idx & "', 'done', 365); window.close();"" border=""0""></td>" & chr(13)
		ElseIf hide_type = "always" Then
			FileText = FileText & chr(9) & chr(9) & chr(9) & "<td align=""right"" style=""padding:10 25 10 25;"">창 닫기 <input type=""checkbox"" style=""width:16px; height:15px;"" onfocus=""blur();"" onclick=""window.close();"" border=""0""></td>" & chr(13)
		End If
		FileText = FileText & chr(9) & chr(9) & "</tr>" & chr(13)
		FileText = FileText & chr(9) & "</table>" & chr(13)
		FileText = FileText & chr(9) & "</body>" & chr(13)
		FileText = FileText & "</html>"

		Call FileWrite_UTF8(HtmlPath, FileText)
	End If

	Sql = "UPDATE " & tablename & " SET "
	Sql = Sql & "display = '" & display & "', "
	Sql = Sql & "sdate = '" & sdate & "', "
	Sql = Sql & "edate = '" & edate & "', "
	Sql = Sql & "pop_type = '" & pop_type & "', "
	Sql = Sql & "hide_type = '" & hide_type & "', "
	Sql = Sql & "scrollYN = '" & scrollYN & "', "
	Sql = Sql & "useYN = '" & useYN & "', "
	Sql = Sql & "pop_width = '" & pop_width & "', "
	Sql = Sql & "pop_height = '" & pop_height & "', "
	Sql = Sql & "pop_top = '" & pop_top & "', "
	Sql = Sql & "pop_left = '" & pop_left & "', "
	Sql = Sql & "title = N'" & title & "', "
	Sql = Sql & "contents = N'" & contents & "' "
	Sql = Sql & " WHERE pop_idx = '" & pop_idx & "'"

	dbconn.execute(Sql)
'	dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&session("aduserid")&"','"&title&"','변경','팝업수정','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

End If

Call DbClose()

If mode = "reg" Then
	Call jsAlertMsgUrl("팝업 등록이 완료되었습니다.", "./pop_list.asp?menucode="&menucode)
ElseIf mode = "upt" Then
	Call jsAlertMsgUrl("팝업 수정이 완료되었습니다.", "./pop_write.asp?menucode="&menucode&"&idx=" & pop_idx)
End If
%>