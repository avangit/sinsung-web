<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
Dim UploadForm, rs, sql, menucode
Dim bn_idx, mode, useYN, title, strLink, strTarget
ReDim mFile(0), rsFile(0)

'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/banner/")
UploadForm.MaxFileLen = int(1024 * 1024 * 10)

menucode	= SQL_Injection(Trim(UploadForm("menucode")))
bn_idx		= SQL_Injection(Trim(UploadForm("n")))
mode		= SQL_Injection(Trim(UploadForm("m")))
useYN		= SQL_Injection(Trim(UploadForm("useYN")))
title		= SQL_Injection(Trim(UploadForm("title")))
strLink		= SQL_Injection(Trim(UploadForm("strLink")))
strTarget	= SQL_Injection(Trim(UploadForm("strTarget")))

Call DbOpen()

If mode = "reg" Then

	If UploadForm("strFile1").FileName <> "" Then
		'// 업로드 가능 여부 체크
		If UploadFileChk(UploadForm("strFile1").FileName)=False Then
			Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 용량 체크
		If UploadForm("strFile1").FileLen > UploadForm.MaxFileLen Then
			Response.Write "<script language='javascript'>alert('10MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 저장
		mFile(0) = UploadForm("strFile1").Save(, False)
		mFile(0) = UploadForm("strFile1").LastSavedFileName
	End If

	Sql = "INSERT INTO banner("
	Sql = Sql & "useYN, "
	Sql = Sql & "title, "
	Sql = Sql & "strFile1, "
	Sql = Sql & "strLink, "
	Sql = Sql & "strTarget) VALUES("
	Sql = Sql & "'" & useYN & "',"
	Sql = Sql & "N'" & title & "',"
	Sql = Sql & "N'" & SQL_Injection(Trim(mFile(0))) & "',"
	Sql = Sql & "N'" & strLink & "',"
	Sql = Sql & "N'" & strTarget & "')"

	dbconn.execute(Sql)

ElseIf mode = "upt" Then

	sql = "SELECT strFile1 FROM banner WHERE bn_idx = '" & bn_idx & "'"

	Set rs = dbconn.execute(sql)

	If Not(rs.eof) Then
		rsFile(0) = rs("strFile1")
	End If

	Set rs = Nothing

	If UploadForm("strFile1").FileName <> "" Then
		'// 업로드 가능 여부 체크
		If UploadFileChk(UploadForm("strFile1").FileName)=False Then
			Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 용량 체크
		If UploadForm("strFile1").FileLen > UploadForm.MaxFileLen Then
			Response.Write "<script language='javascript'>alert('10MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'기존파일 삭제
		If rsFile(0) <> "" Then
			UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile(0)
		End If

		'// 파일 저장
		mFile(0) = UploadForm("strFile1").Save(, False)
		mFile(0) = UploadForm("strFile1").LastSavedFileName

		Sql = "UPDATE banner SET strFile1 ='" & SQL_Injection(Trim(mFile(0))) & "' WHERE bn_idx='" & bn_idx & "'" & Chr(10) & Chr(13)
		dbconn.execute(Sql)
	End If

	Sql = "UPDATE banner SET "
	Sql = Sql & "useYN='" & useYN & "', "
	Sql = Sql & "title = N'" & title & "', "
	Sql = Sql & "strLink = '" & strLink & "', "
	Sql = Sql & "strTarget = '" & strTarget & "' "
	Sql = Sql & " WHERE bn_idx = '" & bn_idx & "'"

	dbconn.execute(Sql)
End If

Set UploadForm = Nothing

Call DbClose()

If mode = "reg" Then
	Call jsAlertMsgUrl("배너 등록이 완료되었습니다.", "./banner_list.asp?menucode="&menucode)
Else
	Call jsAlertMsgUrl("배너 수정이 완료되었습니다.", "./banner_write.asp?menucode="&menucode&"&idx="& bn_idx)
End If
%>