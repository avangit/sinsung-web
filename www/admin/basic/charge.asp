		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
<%
menucode = SQL_Injection(Trim(RequestS("menucode")))

sql = "SELECT * FROM Record_views3"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	idx = rs("idx")
	cost = rs("cost")
End If

rs.close
Set rs = Nothing
%>
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">

					<header>
						<span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
						<h2>배송비등록</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<form action="charge_ok.asp" class="smart-form" onSubmit="return regist(this)" method="post" name="regForm">
						<input type="hidden" name="n" value="<%=idx%>">
						<input type="hidden" name="menucode" value="<%=menucode%>">
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>

								<tbody>
									<tr>
										<th>배송비</th>
										<th><label for="cost" class="input">
												<input type="text" name="cost" id="cost" value="<%=cost%>">
											</label>
										</td>
									</tr>
									<tr></tr>
								</tbody>
							</table>

							<footer>
							<p class="p_info_txt">0원일경우에 무료배송</p>
								<input type="submit" class="btn btn-primary" value="완료">
							</footer>
						</div>
						</form>
						

						<script language="javascript">
							<!--
								function regist(frm){
									if ( frm.cost.value == "" ) { alert("배송비를 입력해주세요"); frm.cost.focus(); return false; }

								}
							//-->
						</script>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
			</article>
			<!-- WIDGET END -->

			<script>
				$(document).ready(function() {

					// DO NOT REMOVE : GLOBAL FUNCTIONS!
					pageSetUp();

				});
			</script>
			<!-- #include file="../inc/footer.asp" -->