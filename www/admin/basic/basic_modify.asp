
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>BASIC</h2>
					</header>
	
					<!-- widget div-->
					<div class="mt50">
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">


							<%
								dim intSeq, sql, rs

								intSeq = request.QueryString("intSeq")


								Call DbOpen()


									sql = "SELECT * FROM basic WHERE intSeq = '"&intSeq&"'"
									'response.write strsql

									set rs = dbconn.execute(sql)


									if not rs.eof then


							%>

							<form name="frmRequestForm" method="post" onsubmit="return frmRequestForm_Submit(this);" class="smart-form" enctype="multipart/form-data">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									
									<tr>
										<th>제목</th>
										<td>
											<label for="strTitle" class="input">
												<input type="text" name="strTitle" id="strTitle" placeholder="" value="<%=rs("strtitle")%>">
											</label>
										</td>
									</tr>
									
									<tr>
										<th>이름</th>
										<td>
											<label for="strName" class="input">
												<input type="text" name="strName" id="strName" placeholder="" value="<%=rs("strName")%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>라디오</th>
										<td>
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="strRadio" value="Alexandra" <%If Trim(rs("strRadio")) =  "Alexandra" then%>checked <%End if%>>
													<i></i>Alexandra</label>
												<label class="radio">
													<input type="radio" name="strRadio" value="Alice"  <%If Trim(rs("strRadio")) =  "Alice" then%>checked <%End if%> >
													<i></i>Alice</label>
												<label class="radio">
													<input type="radio" name="strRadio" value="Anastasia"  <%If Trim(rs("strRadio")) =  "Anastasia" then%>checked <%End if%>>
													<i></i>Anastasia</label>
												<label class="radio">
													<input type="radio" name="strRadio" value="Avelina"  <%If Trim(rs("strRadio")) =  "Avelina" then%>checked <%End if%>>
													<i></i>Avelina</label>
												<label class="radio">
													<input type="radio" name="strRadio" value="Beatrice"  <%If Trim(rs("strRadio")) =  "Beatrice" then%>checked <%End if%>>
													<i></i>Beatrice</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>체크박스</th>
										<td>
											<div class="inline-group margin_t">
												<label class="checkbox">
													<input type="checkbox" name="strChack" value="Red" <%If InStr(rs("strChack"),"Red") > 0 then%>checked <%End if%>>
													<i></i>Red</label>
												<label class="checkbox">
													<input type="checkbox" name="strChack" value="Blue" <%If InStr(rs("strChack"),"Blue") > 0 then%>checked <%End if%>>
													<i></i>Blue</label>
												<label class="checkbox">
													<input type="checkbox" name="strChack" value="Black" <%If InStr(rs("strChack"),"Black") > 0 then%>checked <%End if%>>
													<i></i>Black</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>셀렉트</th>
										<td>
											<label class="select state-disabled w20">
											<select class="input-sm" name="strSelect">
												<option value="0" <%If Trim(rs("strSelect")) = "0" then%>selected<%End if%>>Choose name</option>
												<option value="1" <%If Trim(rs("strSelect")) = "1" then%>selected<%End if%>>Alexandra</option>
												<option value="2" <%If Trim(rs("strSelect")) = "2" then%>selected<%End if%>>Alice</option>
												<option value="3" <%If Trim(rs("strSelect")) = "3" then%>selected<%End if%>>Anastasia</option>
												<option value="4" <%If Trim(rs("strSelect")) = "4" then%>selected<%End if%>>Avelina</option>
											</select> <i></i> </label>
										</td>
									</tr>


									<tr>
										<th>파일</th>
										<td>
										<div class="input input-file w20">
											<span class="button"><input type="file" id="file" name="strImage" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="" value="<%=getFileNumName(rs("strImage"))%>">
										</div>
										<% call FileDownLink_uppath(rs("strImage"),"/upload/basic/")%>
										</td>
									</tr>
									
									<tr class="h300">
										<th>에디터</th>
										<td><%call Editor("strcontent",rs("strcontent"))%></td>
									</tr>
									
								</tbody>
						
							</table>

							<footer style="float:right;">
								<input type="button" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
								<input type="button" class="btn btn-default"  onclick="window.history.back();" value="리스트">
								<input type="button" class="btn btn-default"  onclick="location.href='basic_delete.asp?intSeq=<%=intSeq%>'" value="삭제">
							</footer>
							
							</form>	
							
							<%
							End if
							%>
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->

				<script language="javascript">
<!--

					function frmRequestForm_Submit(frm){
						if ( frm.strTitle.value == "" ) { alert("제목을 입력해주세요"); frm.strTitle.focus(); return false; }
						frm.action = "basic_update.asp?intSeq=<%=intSeq%>"
						frm.submit();
					}

				//-->
				</script>


			

			</article>
			<!-- WIDGET END -->

		
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

			});

		</script>

		<!-- #include file="../inc/footer.asp" -->