		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
<%
menucode = SQL_Injection(Trim(RequestS("menucode")))

sql = "SELECT * FROM Record_views3"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	idx = rs("idx")
	email = rs("email")
End If

rs.close
Set rs = Nothing
%>
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">

					<header>
						<span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
						<h2>발신메일등록</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<form class="smart-form" onSubmit="return regist(this)" method="post" name="regForm" action="mail_ok.asp">
						<input type="hidden" name="n" value="<%=idx%>">
						<input type="hidden" name="menucode" value="<%=menucode%>">
						<div class="widget-body no-padding">
							<table id="datatable_fixed_column" class="table table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>

								<tbody>
									<tr>
										<th>메일주소</th>
										<th><label for="cost" class="input">
												<input type="text" name="email" id="email" value="<%=email%>">
											</label>
										</td>
									</tr>
									<tr></tr>
								</tbody>
							</table>

							<footer>
								<input type="submit" class="btn btn-primary" value="완료">
							</footer>
						</div>
						</form>

						<script language="javascript">
							<!--
								function regist(frm){
									if ( frm.email.value == "" ) { alert("메일주소를 입력해주세요"); frm.email.focus(); return false; }

								}
							//-->
						</script>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
			</article>
			<!-- WIDGET END -->

			<script>
				$(document).ready(function() {

					// DO NOT REMOVE : GLOBAL FUNCTIONS!
					pageSetUp();

				});
			</script>
			<!-- #include file="../inc/footer.asp" -->