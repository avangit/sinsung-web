<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!-- #include file="./pop_config.asp" -->
<%
pop_idx		= SQL_Injection(Trim(Request.QueryString("idx")))
mode		= SQL_Injection(Trim(Request.QueryString("m")))

If pop_idx = "" Or ISNULL(pop_idx) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","history.back();")
'Else
'	pop_idx = Replace(pop_idx, " ", ",")
End If

Call dbOpen()

'// 삭제 시
If mode = "del" Then
	message = "삭제"

	sql = "SELECT htmlFile FROM popup WHERE pop_idx = " & pop_idx

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then

		Set UploadForm = Server.Createobject("DEXT.FileUpload")
		UploadForm.DefaultPath = Server.MapPath("/upload/popup/")

		'기존파일 삭제
		If rs("htmlFile") <> "" Then
			UploadForm.Deletefile UploadForm.DefaultPath & "\" & rs("htmlFile")
		End If

		Set UploadForm = Nothing
	End If

	dbconn.execute("DELETE FROM popup WHERE pop_idx = " & pop_idx & "")
'		dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES ('"&session("aduserid")&"','"&strId&"','삭제','하위회원삭제','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

	rs.close
	Set rs = Nothing

Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("삭제되었습니다.", "./pop_list.asp?menucode="&menucode)
%>