<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<style>
.hasDatepicker {
	position: relative;
	z-index: 9999;
}
</style>
<article>
<script src="/admin/js/jquery-2.1.1.min.js"></script>
  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>SRM 배너관리</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
Dim intTotalCount, intTotalPage, menucode

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request.QueryString("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= "*"
Dim query_Tablename		: query_Tablename	= "banner"
Dim query_where			: query_where		= " bn_idx > 0 "
Dim query_orderby		: query_orderby		= " ORDER BY bn_idx DESC"
Dim rs, sql, bn_idx, useYN, title, strFile1, regdate

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

'response.Write getQuery

sql = getQuery
Call dbopen
Set rs = dbconn.execute(sql)

menucode = Request.QueryString("menucode")
%>

		<%=intTotalCount%>건
		<footer style="float:right;" class="m_button">
			<a href="./banner_write.asp?menucode=<%=menucode%>" class="btn btn-primary"><i class="fa fa-pencil"></i> 등록</a>
		</footer>

		<form name="list" method="get" action="./banner_exec_get.asp" style="margin:0;">
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="message">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:5%">
			<col style="width:30%">
			<col style="width:*">
			<col style="width:10%">
			<col style="width:5%">
			<col style="width:5%">
		</colgroup>
		<thead>
			<tr>
				<th >번호</th>
				<th>이미지</th>
				<th >제목</th>
				<th >등록일</th>
				<th >상태</th>
				<th >관리</th>
			</tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="6" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	rs.move MoveCount
	Do while Not rs.eof

		bn_idx			= rs("bn_idx")
		useYN 			= rs("useYN")
		title 			= rs("title")
		strFile1 		= rs("strFile1")
		regdate 		= rs("regdate")

		If useYN = "Y" Then
			useYN = "사용"
		Else
			useYN = "미사용"
		End If
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td align="center"><%=intNowNum%></td>
				<td align="center"><img src="/upload/banner/<%=strFile1%>" style="width:300px;height:80px;"></td>
				<td><%=title%></td>
				<td align="center"><%=Left(regdate, 10)%></td>
				<td align="center"><%=useYN%></td>
				<td align="center"><a href="./banner_write.asp?menucode=<%=menucode%>&idx=<%=bn_idx%>" class="btn btn-primary"><i class="fa fa-pencil"></i> 수정</a></td>
			</tr>
<%
		intNowNum = intNowNum - 1
		rs.movenext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
		</tbody>
		</table>

		<!--table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:10 0 0 0;">
			<tr>
				<td>
					<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
				</td>
			</tr>
		</table-->
		</form>

		<%call Paging_list("")%>
	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->