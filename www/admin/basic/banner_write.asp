
		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
<%
Dim menucode, idx, useYN, title, strFile1, strLink, strTarget, regdate
Dim mode, rs, sql

menucode	= Request.QueryString("menucode")
idx			= Request.QueryString("idx")

If idx = "" Or isnull(idx) Then
	mode = "reg"
Else
	mode = "upt"
End If

If mode = "upt" Then
	Call DbOpen()

	sql = "SELECT * FROM banner WHERE bn_idx = " & idx

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		useYN 		= rs("useYN")
		title 		= rs("title")
		strFile1 	= rs("strFile1")
		strLink 	= rs("strLink")
		strTarget 	= rs("strTarget")
		regdate 	= rs("regdate")
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
End If
%>
			<!-- NEW WIDGET START -->
			<article>
			<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>SRM 배너관리</h2>
					</header>

					<!-- widget div-->
					<div class="mt50">

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<form name = "joinform" action = "banner_exec.asp" class="smart-form" method="post" enctype="multipart/form-data">
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<input type="hidden" name="n" value="<%=idx%>">
							<input type="hidden" name="m" value="<%=mode%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>사용 유무 </th>
										<td colspan="2">
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="useYN" value="Y" <% If useYN = "" Or useYN = "Y" Then %> checked<% End If %>>
													<i></i>사용</label>
												<label class="radio">
													<input type="radio" name="useYN" value="N" <% If useYN = "N" Then %> checked<% End If %>>
													<i></i>미사용</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>제목 </th>
										<td colspan="2">
											<label for="title" class="input">
												<input type="text" name="title" id="title" value="<%=title%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>이미지 </th>
										<td>
											<div class="input input-file w30">
												<span class="button"><input type="file" id="strFile1" name="strFile1" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">&nbsp;&nbsp;<%=strFile1%>
											</div>
										</td>
									</tr>
									<tr>
										<th>링크 </th>
										<td colspan="2">
											<label for="strLink" class="input">
												<input type="text" name="strLink" id="strLink" value="<%=strLink%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>이미지 링크 타겟 *</th>
										<td colspan="2">
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="strTarget" value="blank" <% If strTarget = "" Or strTarget = "blank" Then %> checked<% End If %>>
													<i></i> 새 창으로 열림</label>
												<label class="radio">
													<input type="radio" name="strTarget" value="self" <% If strTarget = "self" Then %> checked<% End If %>>
													<i></i> 현재 창으로 열림</label>
											</div>
										</td>
									</tr>
								</tbody>

							</table>

							<footer style="float:right;">
							<% If mode = "reg" Then %>
								<input type="submit" class="btn btn-primary" value="등록">
							<% Else %>
								<input type="button" class="btn btn-primary" value="삭제" onclick="location.href='./banner_exec_get.asp?menucode=<%=menucode%>&n=<%=idx%>&m=del'">
								<input type="submit" class="btn btn-primary" value="수정">
							<% End If %>
								<a href="./banner_list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
							</footer>
							</form>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->
			</article>
			<!-- WIDGET END -->

		<script>
			$(document).ready(function() {
				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			});

		</script>
		<!-- #include file="../inc/footer.asp" -->