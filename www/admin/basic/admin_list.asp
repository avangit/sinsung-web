<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->
<!-- #include file="../member/config.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<style>
.hasDatepicker {
	position: relative;
	z-index: 9999;
}
</style>
<article>
<script src="/admin/js/jquery-2.1.1.min.js"></script>
  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>관리자리스트</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request.QueryString("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= "*"
Dim query_Tablename		: query_Tablename	= TableName
Dim query_where			: query_where		= " intGubun = 9 "
Dim query_orderby		: query_orderby		= " ORDER BY intseq DESC"


Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

'response.Write getQuery

sql = getQuery
Call dbopen
Set rs = dbconn.execute(sql)
%>

		<%=intTotalCount%>건
		<footer style="float:right;" class="m_button">
			<a href="./admin_write.asp?menucode=<%=menucode%>" class="btn btn-primary"><i class="fa fa-pencil"></i> 등록</a>
		</footer>

		<form name="list" method="get" action="./admin_exec_get.asp" style="margin:0;">
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="message">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<col style="width:5%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:10%">
			<col style="width:15%">
			<col style="width:10%">
			<col style="width:10%">
		</colgroup>
		<thead>
			<tr>
				<th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th>
				<th data-hide="phone,tablet">번호</th>
				<th data-class="expand">이름</th>
				<th data-class="expand">아이디</th>
				<th data-hide="phone,tablet">이메일</th>
				<th data-hide="phone,tablet">휴대폰번호</th>
				<th data-hide="phone,tablet">등록일</th>
			</tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="7" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	rs.move MoveCount
	Do while Not rs.eof

		intseq			= rs("intseq")
		strId 			= rs("strId")
		strName 		= rs("strName")
		strPwd 			= rs("strPwd")
		strMobile 		= rs("strMobile")
		dtmInsertDate	= rs("dtmInsertDate")
		intAction 		= rs("intAction")
		stremail 		= rs("stremail")
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td><input type="checkbox" value="<%=intseq%>"></td>
				<td><%=intNowNum%></td>
				<td><a href="admin_write.asp?menucode=<%=menucode%>&intSeq=<%=intseq%>"><%=rs("strName")%></a></td>
				<td><a href="admin_write.asp?menucode=<%=menucode%>&intSeq=<%=intseq%>"><%=rs("strId")%></a></td>
				<td><%=stremail%></td>
				<td><%=strMobile%></td>
				<td><%=Left(dtmInsertDate, 10)%></td>
			</tr>
<%
		intNowNum = intNowNum - 1
		rs.movenext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
		</tbody>
		</table>

		<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:10 0 0 0;">
			<tr>
				<td>
					<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
				</td>
			</tr>
		</table>
		</form>

		<%call Paging_list("")%>
	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->