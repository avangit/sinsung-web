		<!-- #include file="../inc/head.asp" -->
		<!-- #include file="../inc/header.asp" -->
		<!-- #include file="./pop_config.asp" -->
<%
pop_idx = Request.QueryString("idx")

If pop_idx = "" Or isnull(pop_idx) Then
	mode = "reg"
Else
	mode = "upt"
End If

If mode = "upt" Then
	Call DbOpen()

	sql = "SELECT * FROM " & tablename & " WHERE pop_idx = " & pop_idx

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		display 	= rs("display")
		sdate 		= rs("sdate")
		edate 		= rs("edate")
		pop_type 	= rs("pop_type")
		hide_type 	= rs("hide_type")
		scrollYN 	= rs("scrollYN")
		useYN 		= rs("useYN")
		pop_width 	= rs("pop_width")
		pop_height 	= rs("pop_height")
		pop_top 	= rs("pop_top")
		pop_left 	= rs("pop_left")
		title 		= rs("title")
		contents 	= rs("contents")
		regdate 	= rs("regdate")
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
End If
%>
			<!-- NEW WIDGET START -->
			<article>
			<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>팝업정보</h2>
					</header>

					<!-- widget div-->
					<div class="mt50">

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<form name = "joinform" action = "pop_exec.asp" class="smart-form" method="post" <%If mode = "reg" Then %>onsubmit="popup_chk();return false;"<% End If %>>
							<input type="hidden" name="n" value="<%=pop_idx%>">
							<input type="hidden" name="m" value="<%=mode%>">
							<input type="hidden" name="menucode" value="<%=menucode%>">
							<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>출력</th>
										<td colspan="3">
											<label class="select w10">
											<select class="input-sm" name="display">
												<option value="home" <% If display = "home" Then %> selected<% End If %>>홈페이지</option>
												<option value="srm" <% If display = "srm" Then %> selected<% End If %>>SRM</option>
												<option value="both" <% If display = "both" Then %> selected<% End If %>>홈페이지+SRM</option>
											</select> <i></i> </label>
										</td>
									</tr>
									<tr>
										<th>기간 *</th>
										<td>
											<input type="text" name="sdate" id="sdate" value="<%=sdate%>" autocomplete="off"> ~ <input type="text" name="edate" id="edate" value="<%=edate%>" autocomplete="off">
										</td>
										<th>팝업방식 *</th>
										<td>
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="pop_type" value="win" <% If pop_type = "win" Then %> checked<% End If %>>
													<i></i> 브라우저</label>
												<label class="radio">
													<input type="radio" name="pop_type" value="layer" <% If pop_type = "layer" Then %> checked<% End If %>>
													<i></i> 레이어 방식</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>쿠키설정 *</th>
										<td colspan="3">
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="hide_type" value="day" <% If hide_type = "day" Then %> checked<% End If %>>
													<i></i> 하루동안 띄우지 않기</label>
												<label class="radio">
													<input type="radio" name="hide_type" value="none" <% If hide_type = "none" Then %> checked<% End If %>>
													<i></i> 더 이상 띄우지 않기</label>
												<label class="radio">
													<input type="radio" name="hide_type" value="always" <% If hide_type = "always" Then %> checked<% End If %>>
													<i></i> 무조건 띄우기</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>스크롤 유무</th>
										<td>
											<div class="inline-group margin_t">
												<label class="checkbox">
													<input type="checkbox" name="scrollYN" value="yes" <% If scrollYN = "yes" Then %> checked<% End If %>>
													<i></i>스크롤 기능을 사용합니다</label>
											</div>
										</td>
										<th>사용 유무</th>
										<td>
											<div class="inline-group margin_t">
												<label class="radio">
													<input type="radio" name="useYN" value="Y" <% If useYN = "Y" Or useYN = "" Then %> checked<% End If %>>
													<i></i> 사용</label>
												<label class="radio">
													<input type="radio" name="useYN" value="N" <% If useYN = "N" Then %> checked<% End If %>>
													<i></i> 미사용</label>
											</div>
										</td>
									</tr>
									<tr>
										<th>팝업크기 *</th>
										<td>
											가로 <input type="text" name="pop_width" id="pop_width" value="<%=pop_width%>" maxlength="4"> px X&nbsp;&nbsp;
											세로 <input type="text" name="pop_height" id="pop_height" value="<%=pop_height%>" maxlength="4"> px
										</td>
										<th>팝업위치</th>
										<td>
											TOP <input type="text" name="pop_top" id="pop_top" value="<%=pop_top%>" maxlength="4"> px&nbsp;&nbsp;
											LEFT <input type="text" name="pop_left" id="pop_left" value="<%=pop_left%>" maxlength="4"> px
										</td>
									</tr>
									<tr>
										<th>제목</th>
										<td colspan="3">
											<label for="title" class="input">
												<input type="text" name="title" id="title" value="<%=title%>">
											</label>
										</td>
									</tr>
									<tr>
										<th>내용</th>
										<td colspan="3"><%call Editor("contents",contents)%></td>
									</tr>
								</tbody>

							</table>

							<footer style="float:right;">
							<% If mode = "reg" Then %>
								<input type="submit" class="btn btn-primary" value="등록">
							<% Else %>
								<input type="submit" class="btn btn-primary" value="수정">
							<% End If %>
								<a href="./pop_list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-th-list"></i> 리스트</a>
							</footer>
							</form>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->
			</article>
			<!-- WIDGET END -->

		<script>
			$(document).ready(function() {
				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			});

		</script>
		<!-- #include file="../inc/footer.asp" -->