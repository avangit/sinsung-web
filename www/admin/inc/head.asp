<% @CODEPAGE="65001" language="vbscript" %>
<%' option explicit%>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%

	if len(session("aduserid")) = 0 Then
%>
	<script language="javascript">
	<!--
		alert("로그인 하셔야 이용가능합니다.\n\n");
		location.href = "/admin/";
	//-->
	</script>
<%	Response.end
	End If

	'//전역변수 설정
	dim menuTitle, menuCallURL, submenuCode, ThisMenuCode, menuGroup
	submenuCode = 10
	menuTitle = "데쉬보드"
	menuCallURL = "dashboard.asp"

	Sub menu(byval callTitle, byval callURL, byval addurl)
		submenuCode = submenuCode + 1
		'// 메뉴코드만들기
		ThisMenuCode = menuGroup&"s"&submenuCode

		if ThisMenuCode = request.QueryString("menucode") then
			menuTitle = callTitle
			menuCallURL = callURL

			%><li class="active"><a href="/admin/sub.asp?menucode=<%=ThisMenuCode%>&path=<%=callURL%>&<%=addurl%>">>> <%=callTitle%></a></li><%
		else
			%><li><a href="/admin/sub.asp?menucode=<%=ThisMenuCode%>&path=<%=callURL%>&<%=addurl%>">- <%=callTitle%></a></li><%
		end if

	End Sub

	Sub menu2(byval callTitle, byval callURL, byval addurl)
		submenuCode = submenuCode + 1
		'// 메뉴코드만들기
		ThisMenuCode = menuGroup&"s"&submenuCode

		if ThisMenuCode = request.QueryString("menucode") then
			menuTitle = callTitle
			menuCallURL = callURL

			%><li class="active"><a href="<%=callURL%>?menucode=<%=ThisMenuCode%>&<%=addurl%>">>> <%=callTitle%></a></li><%
		else
			%><li><a href="<%=callURL%>?menucode=<%=ThisMenuCode%>&<%=addurl%>">- <%=callTitle%></a></li><%
		end if

	End Sub

%>
<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> 신성CNS 관리자페이지 </title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-skins.min.css">

		<!-- SmartAdmin RTL Support  -->
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-rtl.min.css">

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="/admin/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/admin/img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- Specifying a Webpage Icon for Web Clip
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/admin/img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/admin/img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/admin/img/splash/touch-icon-ipad-retina.png">

		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="/admin/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="/admin/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="/admin/img/splash/iphone.png" media="screen and (max-device-width: 320px)">




		<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/add.css">





		<!-- 추가기능을 위한 스크릅티들 -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="/admin/js/plugin/pace/pace.min.js"></script>

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="/admin/js/libs/jquery-2.1.1.min.js"><\/script>');
			}
		</script>

		<script src="/admin/js/libs/jquery-ui-1.10.3.min.js"></script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="/admin/js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="/admin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

		<!-- BOOTSTRAP JS -->
		<script src="/admin/js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="/admin/js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="/admin/js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="/admin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="/admin/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="/admin/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="/admin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="/admin/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="/admin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="/admin/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="/admin/js/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		<script src="/admin/js/demo.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="/admin/js/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="/admin/js/speech/voicecommand.min.js"></script>

		<!-- SmartChat UI : plugin -->
		<script src="/admin/js/smart-chat-ui/smart.chat.ui.min.js"></script>
		<script src="/admin/js/smart-chat-ui/smart.chat.manager.min.js"></script>

		<!-- PAGE RELATED PLUGIN(S) -->

		<!-- Morris Chart Dependencies -->
		<script src="/admin/js/plugin/morris/raphael.min.js"></script>
		<script src="/admin/js/plugin/morris/morris.min.js"></script>
		<script src="/admin/js/common.js"></script>


	</head>