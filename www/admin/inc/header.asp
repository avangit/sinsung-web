	<%
	If Len(session("aduserid")) < 1 Then
		Response.write "<script>alert('로그인 후 사용가능합니다'); location.href='/admin'</script>"
	End if
	%>
	<!--

	TABLE OF CONTENTS.

	Use search to find needed section.

	===================================================================

	|  01. #CSS Links                |  all CSS links and file paths  |
	|  02. #FAVICONS                 |  Favicon links and file paths  |
	|  03. #GOOGLE FONT              |  Google font link              |
	|  04. #APP SCREEN / ICONS       |  app icons, screen backdrops   |
	|  05. #BODY                     |  body tag                      |
	|  06. #HEADER                   |  header tag                    |
	|  07. #PROJECTS                 |  project lists                 |
	|  08. #TOGGLE LAYOUT BUTTONS    |  layout buttons and actions    |
	|  09. #MOBILE                   |  mobile view dropdown          |
	|  10. #SEARCH                   |  search field                  |
	|  11. #NAVIGATION               |  left panel & navigation       |
	|  12. #RIGHT PANEL              |  right panel userlist          |
	|  13. #MAIN PANEL               |  main panel                    |
	|  14. #MAIN CONTENT             |  content holder                |
	|  15. #PAGE FOOTER              |  page footer                   |
	|  16. #SHORTCUT AREA            |  dropdown shortcuts area       |
	|  17. #PLUGINS                  |  all scripts and plugins       |

	===================================================================

	-->

	<!-- #BODY -->
	<!-- Possible Classes

		* 'smart-style-{SKIN#}'
		* 'smart-rtl'         - Switch theme mode to RTL
		* 'menu-on-top'       - Switch to top navigation (no DOM change required)
		* 'no-menu'			  - Hides the menu completely
		* 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
		* 'fixed-header'      - Fixes the header
		* 'fixed-navigation'  - Fixes the main menu
		* 'fixed-ribbon'      - Fixes breadcrumb
		* 'fixed-page-footer' - Fixes footer
		* 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
	-->
	<body class="">

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">

				<!-- PLACE YOUR LOGO HERE -->
				<span id="logo"><img src="/srm/images/common/logo.png"></span>
				<!-- END LOGO PLACEHOLDER -->

				<!-- Note: The activity badge color changes when clicked and resets the number to 0
				Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->


				<!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
				<div class="ajax-dropdown">

					<!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
					<div class="btn-group btn-group-justified" data-toggle="buttons">
						<label class="btn btn-default">
							<input type="radio" name="activity" id="ajax/notify/mail.html">
							Msgs (14) </label>
						<label class="btn btn-default">
							<input type="radio" name="activity" id="ajax/notify/notifications.html">
							notify (3) </label>
						<label class="btn btn-default">
							<input type="radio" name="activity" id="ajax/notify/tasks.html">
							Tasks (4) </label>
					</div>

					<!-- notification content -->
					<div class="ajax-notifications custom-scroll">

						<div class="alert alert-transparent">
							<h4>Click a button to show messages here</h4>
							This blank page message helps protect your privacy, or you can show the first message here automatically.
						</div>

						<i class="fa fa-lock fa-4x fa-border"></i>

					</div>
					<!-- end notification content -->

					<!-- footer: refresh area -->
					<span> Last updated on: 12/12/2013 9:43AM
						<button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
							<i class="fa fa-refresh"></i>
						</button>
					</span>
					<!-- end footer -->

				</div>
				<!-- END AJAX-DROPDOWN -->
			</div>


			<!-- pulled right: nav area -->
			<div class="pull-right">

				<!-- collapse menu button -->
<!--
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
-->
				<!-- end collapse menu -->

				<!-- #MOBILE -->
				<!-- Top menu profile link : this shows only when top menu is active -->
				<ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
					<li class="">
						<a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
							<img src="/admin/img/avatars/sunny.png" alt="John Doe" class="online" />
						</a>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="login.html" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
							</li>
						</ul>
					</li>
				</ul>

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="/admin/logout.asp" title="Sign Out" data-action="userLogout" ><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->


				<!-- fullscreen button -->
<!--
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
-->
				<!-- end fullscreen button -->

				<!-- #Voice Command: Start Speech -->
				<div id="speech-btn" class="btn-header transparent pull-right hidden-sm hidden-xs">
					<div>
						<div class="popover bottom"><div class="arrow"></div>
							<div class="popover-content">
								<h4 class="vc-title">Voice command activated <br><small>Please speak clearly into the mic</small></h4>
								<h4 class="vc-title-error text-center">
									<i class="fa fa-microphone-slash"></i> Voice command failed
									<br><small class="txt-color-red">Must <strong>"Allow"</strong> Microphone</small>
									<br><small class="txt-color-red">Must have <strong>Internet Connection</strong></small>
								</h4>
								<a href="javascript:void(0);" class="btn btn-success" onclick="commands.help()">See Commands</a>
								<a href="javascript:void(0);" class="btn bg-color-purple txt-color-white" onclick="$('#speech-btn .popover').fadeOut(50);">Close Popup</a>
							</div>
						</div>
					</div>
				</div>
				<!-- end voice command -->

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS variables -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<span style="line-height:26px;">
							<%=session("aduserid")%>
						</span>
						<i></i>
					</a>

				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive-->
			<nav>
				<!--
				NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

				<ul class="left_menu">
					<%menuGroup = "m01"%>
					<li class="active">
						<a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">대시보드</span></a>
						<ul>
							<!--<li class="active"><a href="/admin/main.asp" title="Dashboard"><span class="menu-item-parent">데쉬보드</span></a></li>-->
							<%
							  call menu2("대시보드","/admin/main.asp","")
							%>
						</ul>
					</li>

					<%menuGroup = "m02"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-group"></i> <span class="menu-item-parent">기본관리</span></a>
						<ul>
						<%
							Call menu2("배송비설정","/admin/basic/charge.asp","")
							Call menu2("발신메일설정","/admin/basic/mail.asp","")
							Call menu2("개인정보처리방침","/admin/record3/list.asp","")
							Call menu2("이용약관","/admin/record4/list.asp","")
							Call menu2("팝업관리","/admin/basic/pop_list.asp","")
							Call menu2("SRM 배너관리","/admin/basic/banner_list.asp","")
							Call menu2("관리자관리","/admin/basic/admin_list.asp","")
						%>
						</ul>
					</li>

					<%menuGroup = "m03"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-group"></i> <span class="menu-item-parent">회원관리</span></a>
						<ul>
						<%
							Call menu2("회원관리","/admin/member/list.asp","")
							Call menu2("회원등록","/admin/member/write.asp","")
							Call menu2("탈퇴리스트","/admin/member/leave_list.asp","")
							Call menu2("SMS발송","/admin/sms/form.asp","")
							Call menu2("메일발송","/admin/mail/form.asp","")

'							Call menu2("개인정보접속기록","/admin/record/list.asp","")
'							Call menu2("운영자관리권한기록","/admin/record2/list.asp","")
'							Call menu2("개인정보처리방침","/admin/record3/list.asp","")
'							Call menu2("약관/개인정보","/admin/record4/list.asp","")
						%>
						</ul>
					</li>

					<!--li>
						<a href="#"><i class="fa fa-lg fa-fw fa-group"></i> <span class="menu-item-parent">업체관리</span></a>
						<ul>
							<%
								Call menu2("업체관리","/admin/member/company.asp","")
							%>
						</ul>
					</li-->

					<%menuGroup = "m04"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-sitemap"></i> <span class="menu-item-parent">카테고리관리</span></a>
						<ul>
						<%
							Call menu2("기본형 카테고리관리","/admin/cate/cateAdmin.asp","")
'							Call menu2("맞춤형 카테고리관리","/admin/cate/cateAdmin2.asp","")
							Call menu2("옵션관리","/admin/cate_option/cate_option.asp","")
							Call menu2("옵션그룹관리","/admin/cate_option/cate_option_grouplist.asp","")
						%>
						</ul>
					</li>

					<%menuGroup = "m05"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-shopping-cart"></i> <span class="menu-item-parent">기본형 제품관리</span></a>
						<ul>
						<%
							Call menu2("상품관리","/admin/goods/product.asp","")
							Call menu2("상품등록","/admin/goods/product_write.asp","")
							Call menu2("SRM 회원별 제품","/admin/goods/product_srm_member.asp"," ")
'							Call menu2("1:1문의관리","/admin/qna/list.asp","")
'							Call menu2("상품후기관리","/admin/review/list.asp","")
						%>
						</ul>
					</li>

					<%menuGroup = "m06"%>
					<!--li>
						<a href="#"><i class="fa fa-lg fa-fw fa-shopping-cart"></i> <span class="menu-item-parent">맞춤형 제품관리</span></a>
						<ul>
						<%
							Call menu2("상품관리","/admin/goods2/product2.asp","")
							Call menu2("상품등록","/admin/goods2/product2_write.asp","")
						%>
						</ul>
					</li-->

					<!--li>
						<a href="#"><i class="fa fa-lg fa-fw fa-credit-card"></i> <span class="menu-item-parent">적립금</span></a>
						<ul>
							<li><a href="/admin/point/point.asp">적립금현황</a></li>
						</ul>
					</li-->

					<%menuGroup = "m07"%>
					<!--li>
						<a href="#"><i class="fa fa-lg fa-fw fa-krw"></i> <span class="menu-item-parent">매출통계</span></a>
						<ul>
						<%
							'Call menu2("매출통계","/admin/stats/stats.asp","")
						%>
						</ul>
					</li-->

					<%menuGroup = "m08"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-truck"></i> <span class="menu-item-parent">주문관리</span></a>
						<ul>
						<%
							Call menu2("전자결재","/admin/order/order_list.asp","result=0")
							Call menu2("결제대기","/admin/order/order_list.asp","result=1")
							Call menu2("결제완료","/admin/order/order_list.asp","result=2")
							Call menu2("판매완료","/admin/order/order_list.asp","result=4")
							Call menu2("취소","/admin/order/order_list.asp","result=91")
							Call menu2("교환","/admin/order/order_list.asp","result=92")
							Call menu2("반품","/admin/order/order_list.asp","result=93")
'							Call menu2("전체","/admin/order/order_list.asp","")
'							Call menu2("요약정보","/admin/order/summary.asp","")
'							Call menu2("전체리스트","/admin/order/order_list.asp","")
'							Call menu2("신규[미입금]리스트","/admin/order/order_list.asp","result=1")
'							Call menu2("배송준비리스트","/admin/order/order_list.asp","result=2")
'							Call menu2("배송중 리스트","/admin/order/order_list.asp","result=3")
'							Call menu2("반품 리스트","/admin/order/order_list.asp","result=4")
'							Call menu2("취소 리스트","/admin/order/order_list.asp","result=5")
'							Call menu2("환불 리스트","/admin/order/order_list.asp","result=6")
						%>
						</ul>
					</li>


					<%menuGroup = "m09"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">시리얼관리</span></a>
						<ul>
							<% Call menu2("시리얼관리","/admin/serial/list.asp","") %>
						</ul>
					</li>


					<!--li>
						<a href="#"><i class="fa fa-lg fa-fw fa-bold"></i> <span class="menu-item-parent">기본</span></a>
						<ul>
							<li><a href="/admin/basic/basic_list.asp">기본</a></li>
						</ul>
					</li-->

					<%menuGroup = "m10"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">게시판</span></a>
						<ul>
							<%
							dim boardrs
							set boardrs = dbconn.execute("select * from boardset_v2")

							if not boardrs.eof then
								do while not boardrs.eof
									call menu(boardrs("bs_name"),"/AVANplus/modul/avanboard_v3/call2.asp","bs_code="&boardrs("bs_code"))
								boardrs.movenext
								loop
							end If
							%>
						</ul>
					</li>

					<%menuGroup = "m11"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">온라인문의</span></a>
						<ul>
						<%
							Call menu2("문의내역","/admin/online/online_list.asp","")
						%>
						</ul>
					</li>

					<%menuGroup = "m12"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">구독신청관리</span></a>
						<ul>
						<%
							Call menu2("신청내역","/admin/subscribe/list.asp","")
						%>
						</ul>
					</li>

				<% If Request.ServerVariables("REMOTE_ADDR") = "118.131.143.109" Or Request.ServerVariables("REMOTE_ADDR") = "118.131.143.110" Then %>
					<%menuGroup = "m13"%>
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-gears"></i> <span class="menu-item-parent">홈페이지환경설정</span></a>
						<ul>
						<%
							call menu("게시판관리","/AVANplus/modul/avanboard_v3/calladmin.asp","")

'							call menu2("sms환경설정","/admin/sms/set.asp","")
						%>

							<!--li><a href="/admin/sms/set.asp">sms환경설정</a></li-->
						</ul>
					</li>
				<% End If %>
					<!--li>
						<a href="#"><i class="fa fa-lg fa-fw fa-gears"></i> <span class="menu-item-parent">DB Tool</span></a>
						<ul>
						<%
'							call menu("테이블명세서","/avanplus/_master/table.asp","")
						%>

						</ul>
					</li-->

				</ul>
			</nav>


			<span class="minifyme" data-action="minifyMenu">
				<i class="fa fa-arrow-circle-left hit"></i>
			</span>

		</aside>
		<!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment">
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span>
				</span>

				<!-- breadcrumb -->
				<!--ol class="breadcrumb">
					<li>Home</li><li>Dashboard</li>
				</ol-->
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->



			<!-- MAIN CONTENT -->
			<div id="content">