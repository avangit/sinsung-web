
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->
		<%
			DbOpen()
			
			Dim sYear,sMonth,sDay,sType
			
			sYear	= RequestQ("syear")
			sMonth	= RequestQ("smonth")
			sDay	= RequestQ("sDay")
			sType	= RequestQ("sType")
			
			
			If sYear = "" Then
				sYear	= Year(Date)
			End If
			
			If sMonth = "" Then
				sMonth	= Month(Date)
			End If
			
			If sDay = "" Then
				sDay	= Day(Date)
			End If
			
			If sType = "" Then
				sType	= "m"
			End If
			
			sYear	= cint(sYear)
			sMonth	= cint(sMonth)
			
			Dim rs,sql
			
			
			Select Case sType
				Case "m"
					sql = "SELECT Day(date) AS sDay, Count(*) AS OrderCount, SUM(totalmoney) AS totalMoney, SUM(besongmoney) AS besongMoney "
					sql = sql & " , SUM(point) AS point, SUM(use_point) AS intPoint, SUM(realmoney) AS realMoney "
					sql = sql & " FROM order_regist WHERE status  in('2','e2','3','e3','Y') AND Year(date) = '"&sYear&"'  AND Month(date) = "& sMonth &" GROUP BY Day(date) Order By sDay"
				Case "d"
					sql = "SELECT order_num, name, hand1, totalmoney AS totalMoney, realmoney,point,use_point AS intPoint  "
					sql = sql & "FROM order_regist WHERE status  in('2','e2','3','e3','Y') AND Year(date) = '"&sYear&"'  AND Month(date) = "& sMonth &" AND Day(date) = " & sDay & " Order By order_num"
				Case Else
					sql = "SELECT Month(date) AS sMonth, Count(*) AS OrderCount, SUM(totalmoney) AS totalMoney, SUM(besongmoney) AS besongMoney "
					sql = sql & "  , SUM(point) AS point, SUM(use_point) AS intPoint, SUM(realmoney) AS realMoney  "
					sql = sql & " FROM order_regist WHERE  status  in('2','e2','3','e3','Y')   AND Year(date) = "& sYear &" GROUP BY Month(date) Order By sMonth"
			End Select
			
			
			set rs = dbconn.execute(sql)
		%>
			<!-- NEW WIDGET START -->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>매출통계</h2>
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">
							<form class="smart-form">
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
	
								<tbody>
									<tr>
										<th data-class="expand">날짜</th>
										<td>
											<label class="select bank_num2"  >
											<select class="input-sm">
												<option value="2018">2018</option>
												<option value="2017">2017</option>
												<option value="2016">2016</option>
												<option value="2015">2015</option>
											</select> <i></i> </label>

											
											<label class="select bank_num2" >
											<select class="input-sm">
												<%
												Dim i
												for i=1 To 12%>
												<option value="<%=i%>"><%=i%></option>
												<%next%>
											</select> <i></i> </label>
										</td>
									</tr>
									
								</tbody>
						
							</table>






											<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">

							<div id="year-graph" class="chart no-padding"></div>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->
























							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								 <colgroup>
									<col style="width:10%">
									<col style="width:20%">
									<col style="width:15%">
									<col style="width:15%">
									<col style="width:15%">
									<col style="">
								</colgroup>
								<thead>
									<tr>
										<th data-class="expand">주문수</th>
										<th data-class="expand">상품가격</th>
										<th>배송비</th>
										<th data-hide="phone,tablet">적립금</th>
										<th data-hide="phone,tablet">사용적립금</th>
										<th data-hide="phone,tablet">실결제금액</th>
									</tr>
								</thead>
	
								<tbody>
									<tr>
										<td>35건</td>
										<td>520,000원</td>
										<td>20,000원</td>
										<td>3,000 P</td>
										<td>3,000 P</td>
										<td>50,000원</td>
									</tr>
									
								
								</tbody>
						
							</table>

							
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->



			</div>
			


		</div>
		<!-- END MAIN PANEL -->

		<!-- #include file="../inc/footer.asp" -->


		

		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();


				if ($('#year-graph').length) {
					var day_data = [{
						"period" : "2018-04-01",
						"판매액" : 335100,
						"환불액" : 1500
					}, {
						"period" : "2018-04-02",
						"판매액" : 1651000,
						"환불액" : 0
					}, {
						"period" : "2018-04-03",
						"판매액" : 3500000,
						"환불액" : 60000
					}, {
						"period" : "2018-04-04",
						"판매액" : 3000000,
						"환불액" : 335100
					}, {
						"period" : "2018-04-05",
						"판매액" : 3351,
						"환불액" : 629
					}, {
						"period" : "2018-04-06",
						"판매액" : 3351,
						"환불액" : 629
					}, {
						"period" : "2018-04-07",
						"판매액" : 3351,
						"환불액" : 629
					}, {
						"period" : "2018-04-08",
						"판매액" : 3351,
						"환불액" : 629
					}, {
						"period" : "2018-04-09",
						"판매액" : 3351,
						"환불액" : 629
					}, {
						"period" : "2018-04-10",
						"판매액" : 3351,
						"환불액" : 629
					}];
					Morris.Line({
						element : 'year-graph',
						data : day_data,
						xkey : 'period',
						ykeys : ['판매액', '환불액'],
						labels : ['판매액', '환불액']
						
					})

					
				}


			});

		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

	</body>

</html>