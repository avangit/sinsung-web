
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->
		<%
			DbOpen()
			
			Dim sYear,sMonth,sDay,sType
			
			sYear	= RequestQ("syear")
			sMonth	= RequestQ("smonth")
			sDay	= RequestQ("sDay")
			sType	= RequestQ("sType")
			
			
			If sYear = "" Then
				sYear	= Year(Date)
			End If
			
			If sMonth = "" Then
				sMonth	= Month(Date)
			End If
			
			If sDay = "" Then
				sDay	= Day(Date)
			End If
			
			If sType = "" Then
				sType	= "m"
			End If
			
			sYear	= cint(sYear)
			sMonth	= cint(sMonth)
			
			Dim rs,sql
			
			
			Select Case sType
				Case "m"
					sql = "SELECT Day(date) AS sDay, order_num, Count(*) AS OrderCount, SUM(totalmoney) AS totalMoney, SUM(besongmoney) AS besongMoney "
					sql = sql & " , SUM(point) AS point, SUM(use_point) AS intPoint, SUM(realmoney) AS realMoney "
					sql = sql & " FROM order_regist WHERE status  in('2','e2','3','e3','Y') AND Year(date) = '"&sYear&"'  AND Month(date) = "& sMonth &" GROUP BY Day(date) Order By sDay"
				Case "d"
					sql = "SELECT order_num, name, hand1, totalmoney AS totalMoney, realmoney,point,use_point AS intPoint  "
					sql = sql & "FROM order_regist WHERE status  in('2','e2','3','e3','Y') AND Year(date) = '"&sYear&"'  AND Month(date) = "& sMonth &" AND Day(date) = " & sDay & " Order By order_num"
				Case Else
					sql = "SELECT Month(date) AS sMonth, Count(*) AS OrderCount, SUM(totalmoney) AS totalMoney, SUM(besongmoney) AS besongMoney "
					sql = sql & "  , SUM(point) AS point, SUM(use_point) AS intPoint, SUM(realmoney) AS realMoney  "
					sql = sql & " FROM order_regist WHERE  status  in('2','e2','3','e3','Y')   AND Year(date) = "& sYear &" GROUP BY Month(date) Order By sMonth"
			End Select
			
			
			set rs = dbconn.execute(sql)
		%>
			
			<!-- NEW WIDGET START -->
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
					<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"
	
					-->
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>매출통계</h2>
					</header>
	
					<!-- widget div-->
					<div>
	
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
	
						</div>
						<!-- end widget edit box -->
	
						<!-- widget content -->
						<div class="widget-body no-padding">
							<form class="smart-form">
							<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
								 <colgroup>
									<col style="width:15%">
									<col style="">
								</colgroup>
	
								<tbody>
									<tr>
										<th data-class="expand">년도</th>
										<td>
											<% 
												Dim i
												For i = 2007 To Year(Date) 
													If i = sYear Then
													Else
														response.write "<a href='/admin/stats/stats.asp?syear="& i &"&stype=y' class='btn btn-default'><i></i>"&i&"년</a>"
													End If
												Next 
											%>
											
											
										</td>
									</tr>
									<tr>
										<th data-class="expand">월</th>
										<td>
											<% 
												For i = 1 To 12
													If i = sYear Then
													Else
														response.write "<a href='/admin/stats/stats.asp?syear="& sYear &"&smonth="& i &"&stype=y' class='btn btn-default'><i></i>"&i&"월</a>"
													End If
												Next 
											%>
											
											
										</td>
									</tr>
									
								</tbody>
						
							</table>





						
					
							
							<%If Len(request.querystring("sMonth")) < 1 And Len(request.querystring("sDay")) < 1 then%>
								<!-- Widget ID (each widget will need unique ID)-->
								<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" style="overflow:hidden; padding-right:10px;">
									<!-- widget div-->
									<div style="display:table;width:100%;padding:0px;border:0;">
								
										<!-- widget content -->
										<div class="widget-body no-padding" style="display:table-row;">

											<div id="year-graph" class="chart no-padding" style="display:table-cell;"></div>

										</div>
										<!-- end widget content -->

									</div>
									<!-- end widget div -->

								</div>
								<!-- end widget -->
								
							<%End if%>

							<% If sType = "d" Then%>
								<script language="javascript">
									function ShowList(Var)
									{
										var sImg = document.getElementById("g"+Var);
										
										if(sImg.style.display == "none")
										{
											sImg.style.display = "";
										}
										else
										{
											sImg.style.display = "none";
										}
									}
								</script>
												
								<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
									 <colgroup>
										<col style="width:10%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="">
									</colgroup>
									<thead>
										<tr>
											
											<th data-class="expand">번호</th>
											<th data-class="expand">주문번호</th>
											<th data-class="expand">이름</th>
											<th>휴대폰</th>
											<th data-hide="phone,tablet">결제가격</th>
											<th data-hide="phone,tablet">적립금</th>
										</tr>
									</thead>
									<%
										Dim dIndex
										Dim TotalPrice
										Dim TotalPoint
										dIndex = 1
										TotalPrice = 0
										TotalPoint = 0
										Do Until Rs.EOF
									%>
									<tbody>
										<tr>
											<td><%=dIndex %></td>
											<td><a href="javascript:ShowList('<%= dIndex %>');" onfocus="blur();"><%= Rs("order_num") %></a></td>
											<td><%= Rs("name") %></td>
											<td><%= Rs("hand1") %></td>
											<td><%= FormatW(Rs("realmoney")) %></td>
											<td><%= FormatW(Rs("point")) %></td>
										</tr>
									</tbody>
							
								</table>

								<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%" style="margin-top:20px;">
									 <colgroup>
										<col style="">
										<col style="width:20%">
										<col style="width:10%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											
											<th data-class="expand">상품명</th>
											<th data-class="expand">가격</th>
											<th data-class="expand">수량</th>
											<th>금액</th>
										</tr>
									</thead>
		
									<tbody>
										<%
												Dim strSql,Rs2												
												strSql = "SELECT * FROM OrderDetail WHERE regist_num ='"& Rs("order_num") &"' ORDER BY num DESC"
												set Rs2 = dbconn.execute(strSql)
												
												Do Until Rs2.EOF
													
										%>
										<tr>
											<td><%= Rs2("good_name") %></td>
											<td><%= FormatW(Rs2("money")) %></td>
											<td><%= FormatW(Rs2("ea")) %></td>
											<td><%= FormatW(Rs2("money")*Rs2("ea")) %></td>
										</tr>
										<%
													Rs2.MoveNext
												Loop
												
												Rs2.Close
												SET Rs2 = NOTHING
										%>
										<tr>
											<th colspan="6" class="aling_c">
											상품가격 : <%= FormatW(rs("totalMoney")) %> <br>
											적립금 사용 : <%= FormatW(rs("intPoint")) %><br>
											결제금액 : <%= FormatW(rs("realmoney")) %>   <br>
											</th>
										</tr>
										<%
												Dim TotalPrice2,TotalPoint2
												TotalPrice2 = 0
												TotalPoint2 = 0
												
												TotalPrice = TotalPrice + Rs("realMoney")
												TotalPoint = TotalPoint + Rs("point")
												dIndex = dIndex + 1
												Rs.MoveNext
											Loop
											
											Rs.Close
											SET Rs = NOTHING
										%>
										<tr>
											<td colspan="4">총 가격 : <%= FormatNumber(TotalPrice, 0) %></td>
										</tr>
										<tr>
											<td colspan="4">총 포인트 : <%= FormatNumber(TotalPoint, 0) %></td>
										</tr>
									</tbody>
							
								</table>

							<% Else %>
								<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
									 <colgroup>
										<col style="width:10%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="">
									</colgroup>
									<thead>
										<tr>
											
											<th data-class="expand">날짜</th>
											<th data-class="expand">주문수</th>
											<th data-class="expand">상품가격</th>
											<th>배송비</th>
											<th data-hide="phone,tablet">적립금</th>
											<th data-hide="phone,tablet">사용적립금</th>
											<th data-hide="phone,tablet">실결제금액</th>
										</tr>
									</thead>
		
									<tbody>
										<%
											Dim gPrice,tPrice,tPoint,DisplayDate
											gPrice = 0
											tPrice = 0
											tPoint = 0
											Do Until Rs.EOF
												If sType = "m" Then
													DisplayDate = sYear &"년 "& sMonth &"월 "& Rs("sDay") &"일"
												Else
													DisplayDate = sYear &"년 "& Rs("sMonth") &"월 "
												End If
										%>
										<tr>
											<% If sType = "m" Then %>
											<td><a href="?adminmode=m06s03&sYear=<%= sYear %>&sMonth=<%= sMonth %>&sDay=<%= Rs("sDay") %>&sType=d" onfocus="blur();"><%= DisplayDate %></a></td>
											<% Else %>
											<td><a href="?adminmode=m06s03&sYear=<%= sYear %>&sMonth=<%= Rs("sMonth") %>&sType=m" onfocus="blur();"><%= DisplayDate %></a></td>
											<% End If %>
											<td><%= Rs("OrderCount") %></td>
											<td><%= FormatNumber(Rs("totalMoney"), 0) %>원</td>
											<td><%= FormatNumber(Rs("besongMoney"), 0) %>원</td>
											<td><%= FormatNumber(Rs("point"), 0) %> P</td>
											<td><%= FormatNumber(Rs("intPoint"), 0) %> P</td>
											<td><%= FormatNumber(Rs("realMoney"), 0) %>원</td>
										</tr>
										<%
												gPrice = gPrice + Rs("totalMoney")
												tPrice = tPrice + Rs("realMoney")
												tPoint = tPoint + Rs("point")
												
												Rs.MoveNext
											Loop
											
											Rs.Close
											SET Rs = NOTHING
										%>
										<tr>
											<th colspan="7" class="aling_c">
											총 상품가격 : <%= FormatNumber(gPrice, 0) %> 원 <br>
											총 결제금액 : <%= FormatNumber(tPrice, 0) %> 원 <br>
											총 적립금 : <%= FormatNumber(tPoint, 0) %> point   <br>
											</th>
										</tr>
									
									</tbody>
							
								</table>
							<% End If %>
							
							</form>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->




		<%
		'''''월 매출액
		Dim c_sql, c_rs, mc_money(12), z
		i=0
		c_sql = "select top 12 left(app_time,6) as s_date,sum(realmoney)as s_money from order_regist where status in('2','e2','3','e3','Y') and app_time!='null' group by left(app_time,6) order by left(app_time,6) desc"
		'Response.write c_sql
		Set c_rs = dbconn.execute(c_sql)
		If c_rs.eof Then
		Else
			do while not c_rs.eof

				For z=i To 12
					If Trim(c_rs("s_date")) = Trim(Replace(left(DateAdd("m",-i,now()),7),"-","")) then
						mc_money(i) = c_rs("s_money")
						i= i+1
						Exit For 
					Else
						i= i+1
					End If
				Next
				
			c_rs.movenext
			loop

		end if

		c_rs.close
		set c_rs = Nothing


		Dim c_money(31)
		i=0
		c_sql = "select top 31 left(app_time,8) as s_date,sum(realmoney)as s_money from order_regist where status in('2','e2','3','e3','Y') and app_time!='null' group by left(app_time,8) order by left(app_time,8) desc"
		'Response.write c_sql
		Set c_rs = dbconn.execute(c_sql)
		If c_rs.eof Then
		Else
			do while not c_rs.eof

				For z=i To 31
					If Trim(c_rs("s_date")) = Trim(Replace(Date()-i,"-","")) then
						c_money(i) = c_rs("s_money")
						i= i+1
						Exit For 
					Else
						i= i+1
					End If
				Next
				
			c_rs.movenext
			loop

		end if

		c_rs.close
		set c_rs = Nothing
		
		%>

		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();


				if ($('#year-graph').length) {
					var day_data = [
					<%if request.querystring("sMonth") > 0 and request.querystring("sDay") < 1 then %>
					
						<%for i=0 to 31%>
						{
							"period" : '<%=date()-i%>',
							"판매액" : '<%if len(c_money(i)) > 0 then response.write c_money(i) else response.write 0 end if%>'
						},
						<%next%>
						
					<%else
					for i=0 to 12%>
					{
						"period" : '<%=left(DateAdd("m",-i,now()),7)%>',
						"판매액" : '<%if len(mc_money(i)) > 0 then response.write mc_money(i) else response.write 0 end if%>'
					},
					<%next%>
					
					<%end if%>
					];
					Morris.Line({
						element : 'year-graph',
						data : day_data,
						xkey : 'period',
						ykeys : ['판매액', '환불액'],
						labels : ['판매액', '환불액']
						
					})

					
				}


			});

		</script>

		<!-- #include file="../inc/footer.asp" -->