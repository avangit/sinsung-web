<!-- #include file="../inc/head.asp" -->
<!-- #include file="../inc/header.asp" -->
<!-- include file="./config.asp" -->

<!-- NEW WIDGET START -->
<!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<article>
<script src="/admin/js/jquery-2.1.1.min.js"></script>
  <!-- Widget ID (each widget will need unique ID)-->
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
	  <span class="widget-icon"><i class="fa fa-group"></i></span>
	  <h2>구독신청 리스트</h2>
	</header>
	<!-- widget div-->
	<div>
	  <!-- widget edit box -->
	  <div class="jarviswidget-editbox">
	  <!-- This area used as dropdown edit box -->
	  </div>
	  <!-- end widget edit box -->
  	  <!-- widget content -->
	  <div class="widget-body no-padding">
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(RequestS("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(RequestS("fieldvalue")))
Dim menucode 	: menucode 		=  SQL_Injection(Trim(RequestS("menucode")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request.QueryString("page")))
Dim intPageSize			: intPageSize 		= 30
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= "*"
Dim query_Tablename		: query_Tablename	= "subscribe"
Dim query_where			: query_where		= " s_idx > 0 "
Dim query_orderby		: query_orderby		= " ORDER BY s_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
		<form name = "frm" id="frm" action = "" class="smart-form m10"  method="get" >
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table class="table table-striped table-bordered line-h" width="100%">
			<tr>
				<th>검색</th>
				<td colspan="3">
					<label class="select w10 pro_detail">
					<select class="input-sm h34" name="fieldname" id="fieldname">
						<option value="s_name" <% If fieldname = "s_name" Then %> selected<% End If %>>신청인</option>
						<option value="s_company" <% If fieldname = "s_company" Then %> selected<% End If %>>회사명</option>
						<option value="s_email" <% If fieldname = "s_email" Then %> selected<% End If %>>이메일</option>
					</select>
					</label>
					<div class="icon-addon addon-md col-md-10 col-4">
						<input type="text" name="fieldvalue" id="fieldvalue" class="form-control col-10" value="<%=fieldvalue%>">
						<label for="fieldvalue" class="glyphicon glyphicon-search " rel="tooltip" title="fieldvalue"></label>
					</div>
					<a href="javascript:frm.submit();" class="btn btn-primary"><i class="fa fa-search"></i> 검색</a>
					<a href="./list.asp?menucode=<%=menucode%>" class="btn btn-default"><i class="fa fa-refresh"></i> 검색 초기화</a>
				</td>
			</tr>
		</table>
		</form>

		<%=intTotalCount%>건

		<form name="list" method="get" action="./subscribe_exec.asp" style="margin:0;">
		<input type="hidden" name="n">
		<input type="hidden" name="m">
		<input type="hidden" name="message">
		<input type="hidden" name="menucode" value="<%=menucode%>">
		<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
		<colgroup>
			<!--col style="width:5%"-->
			<col style="width:5%">
			<col style="width:8%">
			<col style="width:15%">
			<col style="width:15%">
			<col style="width:*">
			<col style="width:8%">
		</colgroup>
		<thead>
			<tr>
				<!--th data-class="expand"><input type="checkbox" value="" onclick="checkAll(document.list);"></th-->
				<th data-class="expand">번호</th>
				<th data-hide="phone,tablet">이름</th>
				<th data-class="expand">회사명</th>
				<th data-class="expand">이메일</th>
				<th data-class="expand">주소</th>
				<th data-hide="phone,tablet">신청일</th>
			</tr>
		</thead>
		<tbody>
<%
If rs.eof Then
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<td colspan="7" align="center">데이터가 없습니다.</td>
			</tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof

		s_idx			= rs("s_idx")
		s_name 			= rs("s_name")
		s_company 		= rs("s_company")
		s_email 		= rs("s_email")
		s_addr 			= rs("s_addr")
		regdate 		= rs("regdate")
%>
			<tr style="border-bottom:#dddddd 1px solid;">
				<!--td><input type="checkbox" value="<%=s_idx%>"></td-->
				<td><%=intNowNum%></td>
				<td><%=s_name%></td>
				<td><%=s_company%></td>
				<td><%=s_email%></a></td>
				<td><%=s_addr%></td>
				<td><%=Left(regdate, 10)%></td>
			</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
		</tbody>
		</table>

		<!--table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:10 0 0 0;">
			<tr>
				<td>
					<a href="javascript:GetCheckbox(document.list, 'del', '삭제');" class="btn btn-default"><i class="fa fa-trash-o"></i> 선택삭제</a>
				</td>
			</tr>
		</table-->
		</form>

		<% Call  Paging_list("") %>
	  </div>
	  <!-- end widget content -->
	</div>
	<!-- end widget div -->
  </div>
  <!-- end widget -->
</article>
<!-- WIDGET END -->

<script>
  $(document).ready(function() {
	  // DO NOT REMOVE : GLOBAL FUNCTIONS!
	  pageSetUp();
  });
</script>
<!-- #include file="../inc/footer.asp" -->