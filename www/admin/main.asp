
		<!-- #include file="inc/head.asp" -->
		<!-- #include file="inc/header.asp" -->

				<div class="row">
				<!-- NEW WIDGET START -->
					<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
								<h2>최근 3일이내 주문내역 리스트</h2>
								<!--a class="more_btn" href="javascript:;">더보기</a-->
							</header>
							<!-- widget div-->
							<div>
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
									<colgroup>
										<col style="width:25%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand">주문일시</th>
											<th data-class="expand">주문번호</th>
											<th data-class="expand">주문자/아이디</th>
											<th data-class="expand">주문금액</th>
											<th data-class="expand">주문상태</th>
										</tr>
									</thead>
									<tbody>
<%
sql = "SELECT TOP 5 o_idx, o_num, o_total_price, o_status, strName, strId, regdate FROM orders WHERE o_status IN ('0', '1', '2') AND DATEDIFF(d,getdate(), regdate) > -3"
Set rs = dbconn.execute(sql)

If rs.bof Or rs.eof Then
%>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;">
											<td colspan="6" align="center">3일동안 주문 내역이 없습니다.</td>
										</tr>
<%
Else
	Do While Not rs.eof

		If rs("o_status") = 1 Then
			o_status_txt = "결제대기"
		ElseIf rs("o_status") = 2 Then
			o_status_txt = "결제완료"
		End If
%>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='/admin/order/order_view.asp?idx=<%=rs("o_idx")%>&result=<%=rs("o_status")%>&menucode=<%=menucode%>';">
											<td><%=rs("regdate")%></td>
											<td><%=rs("o_num")%></td>
											<td><%=rs("strName")%>(<%=rs("strId")%>)</td>
											<td><%=FormatNumber(rs("o_total_price"), 0)%>원</td>
											<td><%=o_status_txt%></td>
										</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.close
%>
									</tbody>
								</table>
								<!-- end widget content -->
							</div>
							<!-- end widget div -->
						</div>
						<!-- end widget -->

					</article>
					<!-- WIDGET END -->


					<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
								<h2>최근 3일이내 교환/반품요청 리스트</h2>
								<!--a class="more_btn" href="#">더보기</a-->
							</header>

							<!-- widget div-->
							<div>
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
									<colgroup>
										<col style="width:17%">
										<col style="width:15%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand">요청일</th>
											<th data-class="expand">주문번호</th>
											<th data-class="expand">주문자/아이디</th>
											<th data-class="expand">회사명</th>
											<th data-class="expand">주문금액</th>
											<th data-class="expand">주문상태</th>
										</tr>
									</thead>
									<tbody>
<%
sql = "SELECT TOP 5 o_idx, o_num, o_total_price, o_company, o_status, exchange_date, return_date, strName, strId FROM orders WHERE o_status IN ('92', '93') AND DATEDIFF(d,getdate(), regdate) > -3"
Set rs = dbconn.execute(sql)

If rs.bof Or rs.eof Then
%>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;">
											<td colspan="6" align="center">3일동안 교환/반품 내역이 없습니다.</td>
										</tr>
<%
Else
	Do While Not rs.eof

		If rs("o_status") = 92 Then
			o_status_txt = "교환"
		ElseIf rs("o_status") = 93 Then
			o_status_txt = "반품"
		End If

		If rs("exchange_date") = "" Or ISNULL(rs("exchange_date")) Then
			o_date = rs("return_date")
		Else
			o_date = rs("exchange_date")
		End If
%>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='/admin/order/order_view.asp?idx=<%=rs("o_idx")%>&result=<%=rs("o_status")%>&menucode=<%=menucode%>';">
											<td><%=o_date%></td>
											<td><%=rs("o_num")%></td>
											<td><%=rs("strName")%>(<%=rs("strId")%>)</td>
											<td><%=rs("o_company")%></td>
											<td><%=FormatNumber(rs("o_total_price"), 0)%>원</td>
											<td><%=o_status_txt%></td>
										</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.close
%>
										<!--tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>4</td>
											<td>yyyy-mm-dd</td>
											<td>823456789</td>
											<td>홍길동(sample)</td>
											<td>회사B</td>
											<td>10,000</td>
											<td>반품요청</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>3</td>
											<td>yyyy-mm-dd</td>
											<td>723456789</td>
											<td>홍길동(sample)</td>
											<td>회사C</td>
											<td>10,000</td>
											<td>교환요청</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>2</td>
											<td>yyyy-mm-dd</td>
											<td>623456789</td>
											<td>홍길동(sample)</td>
											<td>회사D</td>
											<td>10,000</td>
											<td>교환요청</td>
										</tr>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='#';">
											<td>1</td>
											<td>yyyy-mm-dd</td>
											<td>523456789</td>
											<td>홍길동(sample)</td>
											<td>회사E</td>
											<td>10,000</td>
											<td>반품요청</td>
										</tr-->

									</tbody>
								</table>
							</div>
							<!-- end widget div -->

						</div>
						<!-- end widget -->
					</article>
				</div>
				<div class="row">
					<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
								<h2>최근 3일이내 회원가입 리스트</h2>
								<a class="more_btn" href="./member/list.asp?menucode=m03s19">더보기</a>
							</header>
							<!-- widget div-->
							<div>
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
									<colgroup>
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:25%">
										<col style="width:20%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand">이름</th>
											<th data-class="expand">아이디</th>
											<th data-class="expand">이메일</th>
											<th data-class="expand">휴대폰번호</th>
											<th data-class="expand">가입일</th>
										</tr>
									</thead>
									<tbody>
<%
intNowPage 		= 1
intPageSize 	= 5
intBlockPage 	= 10

query_filde		= "*"
query_Tablename	= "mtb_member2"
query_where		= " intGubun = 1 AND intAction = 0 AND DATEDIFF(d,getdate(), dtmInsertDate) > -3 "
query_orderby	= " ORDER BY intseq DESC"

Call intTotal

intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery
Set rs = dbconn.execute(sql)

If rs.bof Or rs.eof Then
%>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;">
											<td colspan="5" align="center">3일동안 가입한 회원이 없습니다.</td>
										</tr>
<%
Else
	Do While Not rs.eof
		strId 			= rs("strId")
		strName 		= rs("strName")
		strMobile 		= rs("strMobile")
		strEmail 		= rs("strEmail")
		dtmInsertDate 	= rs("dtmInsertDate")
%>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='./member/list.asp?menucode=s12';">
											<td><%=strName%></td>
											<td><%=strId%></td>
											<td><%=strEmail%></td>
											<td><%=strMobile%></td>
											<td><%=Left(dtmInsertDate, 10)%></td>
										</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

Set rs = Nothing
%>
									</tbody>
								</table>
								<!-- end widget content -->
							</div>
							<!-- end widget div -->
						</div>
						<!-- end widget -->

					</article>
					<!-- WIDGET END -->


					<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
								<h2>최근 3일이내 온라인 문의</h2>
								<a class="more_btn" href="./online/online_list.asp?menucode=m11s52">더보기</a>
							</header>

							<!-- widget div-->
							<div>
								<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
									<colgroup>
										<col style="width:17%">
										<col style="width:15%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
									</colgroup>
									<thead>
										<tr>
											<th data-class="expand">구분</th>
											<th data-class="expand">분류</th>
											<th data-class="expand">제목</th>
											<th data-class="expand">회사명</th>
											<th data-class="expand">작성일</th>
											<th data-class="expand">답변여부</th>
										</tr>
									</thead>
									<tbody>
<%
intNowPage 		= 1
intPageSize 	= 5
intBlockPage 	= 10

query_filde		= "*"
query_Tablename	= "board_online"
query_where		= " DATEDIFF(d,getdate(), regdate) > -3 "
query_orderby	= " ORDER BY on_idx DESC"

Call intTotal

intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery
Set rs = dbconn.execute(sql)

If rs.bof Or rs.eof Then
%>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;">
											<td colspan="6" align="center">3일동안 문의된 내역이 없습니다.</td>
										</tr>
<%
Else
	Do While Not rs.eof
		on_status 	= rs("on_status")
		on_type 	= rs("on_type")
		on_cate 	= rs("on_cate")
		on_company 	= rs("on_company")
		on_title 	= rs("on_title")
		regdate 	= rs("regdate")

		If on_status = "N" Then
			on_status = "접수완료"
		Else
			on_status = "답변완료"
		End If

		If on_type = "estimate1" Then
			on_type = "견적요청(일반)"
		ElseIf on_type = "estimate2" Then
			on_type = "견적요청(솔루션)"
		ElseIf on_type = "tech" Then
			on_type = "기술문의"
		ElseIf on_type = "etc" Then
			on_type = "기타문의"
		End If
%>
										<tr style="border-bottom:#dddddd 1px solid; cursor: pointer;" onclick="location.href='./online/online_list.asp';">
											<td><%=on_type%></td>
											<td><%=on_cate%></td>
											<td><%=Cut(on_title, 10, "...")%></td>
											<td><%=on_company%></td>
											<td><%=Left(regdate, 10)%></td>
											<td><%=on_status%></td>
										</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

Set rs = Nothing
%>
									</tbody>
								</table>
							</div>
							<!-- end widget div -->

						</div>
						<!-- end widget -->
					</article>
				</div>

		<!-- END MAIN PANEL -->


		<%

		'''''' 일 매출액
		Dim c_sql, c_rs, c_money(7), i, z
		i=0
		c_sql = "select top 7 left(app_time,8) as s_date,sum(realmoney)as s_money from order_regist where status = '2' and app_time!='null' group by left(app_time,8) order by left(app_time,8) desc"
		'Response.write c_sql
		Set c_rs = dbconn.execute(c_sql)
		If c_rs.eof Then
			For z=0 To 6
				c_money(z) = 0
			next
		Else
			do while not c_rs.eof

				For z=i To 6
					If Trim(c_rs("s_date")) = Trim(Replace(Date()-i,"-","")) then
						c_money(i) = c_rs("s_money")
						i= i+1
						Exit For
					Else
						i= i+1
					End If
				Next

			c_rs.movenext
			loop

		end if

		c_rs.close
		set c_rs = Nothing



		'''''월 매출액
		Dim mc_money(7)
		i=0
		c_sql = "select top 7 left(app_time,6) as s_date,sum(realmoney)as s_money from order_regist where status = '2' and app_time!='null' group by left(app_time,6) order by left(app_time,6) desc"
		'Response.write c_sql
		Set c_rs = dbconn.execute(c_sql)
		If c_rs.eof Then
			For z=0 To 6
				mc_money(z) = 0
			next
		Else
			do while not c_rs.eof

				For z=i To 6
					If Trim(c_rs("s_date")) = Trim(Replace(left(DateAdd("m",-i,now()),7),"-","")) then
						mc_money(i) = c_rs("s_money")
						i= i+1
						Exit For
					Else
						i= i+1
					End If
				Next

			c_rs.movenext
			loop

		end if

		c_rs.close
		set c_rs = Nothing

		%>

		<!-- #include file="inc/footer.asp" -->
		<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();

				/* data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type */
				if ($('#year-graph').length) {
					var day_data = [
					<%for i=0 to 6%>
					{
						"period" : '<%=date()-i%>',
						"판매액" : '<%if len(c_money(i)) > 0 then response.write c_money(i) else response.write 0 end if%>'
					},
					<%next%>


					];
					Morris.Line({
						element : 'year-graph',
						data : day_data,
						xkey : 'period',
						ykeys : ['판매액', '환불액'],
						labels : ['판매액', '환불액']

					})


				}




				/* data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type */
				if ($('#month-graph').length) {
					var day_data = [

					<%for i=0 to 6%>
					{
						"period" : '<%=left(DateAdd("m",-i,now()),7)%>',
						"판매액" : '<%if len(mc_money(i)) > 0 then response.write mc_money(i) else response.write 0 end if%>'
					},
					<%next%>

					];
					Morris.Line({
						element : 'month-graph',
						data : day_data,
						xkey : 'period',
						ykeys : ['판매액', '환불액'],
						labels : ['판매액', '환불액']
					})
				}

			});

		</script>
<%
Call DbClose()
%>