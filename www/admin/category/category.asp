
		<!-- #include file="../inc/head.asp" -->

		<!-- #include file="../inc/header.asp" -->

			<!-- NEW WIDGET START -->
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
								<!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				
								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"
				
								-->
								<header>
									<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
									<h2>Simple View </h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body">
				
										<div class="tree smart-form">
											<ul>
												<li>
													<span><i class="fa fa-lg fa-folder-open"></i> HOME</span>
													<ul>
														<li>
															<span><i class="fa fa-lg fa-plus-circle"></i>TOP</span>
															<ul>
																<li style="display:none">
																	<span> <label class="checkbox inline-block">
																			<input type="checkbox" name="checkbox-inline">
																			<i></i>셔츠</label> </span>
																</li>
																<li style="display:none">
																	<span> <label class="checkbox inline-block">
																			<input type="checkbox" checked="checked" name="checkbox-inline">
																			<i></i>기본티</label> </span>
																</li>
																<li style="display:none">
																	<span> <label class="checkbox inline-block">
																			<input type="checkbox" checked="checked" name="checkbox-inline">
																			<i></i>니트</label> </span>
																</li>
															</ul>
														</li>
														<li>
															<span><i class="fa fa-lg fa-minus-circle"></i>BOTTOM</span>
															<ul>
																<li>
																	<span><i class="icon-leaf"></i>바지</span>
																</li>
																<li>
																	<span><i class="icon-leaf"></i>신발</span>
																</li>
																<li>
																	<span><i class="fa fa-lg fa-plus-circle"></i>치마</span>
																	<ul>
																		<li style="display:none">
																			<span><i class="fa fa-lg fa-plus-circle"></i> 미니스커트</span>
																			<ul>
																				<li style="display:none">
																					<span><i class="icon-leaf"></i> 원피스</span>
																				</li>
																				<li style="display:none">
																					<span><i class="icon-leaf"></i> Great great Grand Child</span>
																				</li>
																			</ul>
																		</li>
																		<li style="display:none">
																			<span><i class="icon-leaf"></i> Great Grand Child</span>
																		</li>
																		<li style="display:none">
																			<span><i class="icon-leaf"></i> Great Grand Child</span>
																		</li>
																	</ul>
																</li>
															</ul>
														</li>
													</ul>
												</li>
												<li>
													<span><i class="fa fa-lg fa-folder-open"></i> Parent2</span>
													<ul>
														<li>
															<span><i class="icon-leaf"></i> Child</span>
														</li>
													</ul>
												</li>
											</ul>
										</div>
				
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->

			<!-- WIDGET END -->

			</div>

		</div>
		<!-- END MAIN PANEL -->

		<!-- #include file="../inc/footer.asp" -->


		


		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
			
			// PAGE RELATED SCRIPTS
		
			$('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
			$('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
				var children = $(this).parent('li.parent_li').find(' > ul > li');
				if (children.is(':visible')) {
					children.hide('fast');
					$(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
				} else {
					children.show('fast');
					$(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
				}
				e.stopPropagation();
			});			
		
		})

		</script>

	</body>

</html>