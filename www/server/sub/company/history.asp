<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">회사소개</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb7">
					<a href="#;">연혁</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>연혁</h2>
				<!-- container -->

				<article class="history">
					<dl class="first">
							<dt class="whblue">2020</dt>
								<dd class="top_tit"><em class="blue">벤처기업, 기술혁신형 중소기업</em> 인증</dd>
								<dd>
									<ul>
										<li><b>01월</b>제 200102-00091호 기술혁신형 중소기업 선정</li>
										<li><b>01월</b>제 20200100614 호 벤처기업 인증</li>
										<li><b>01월</b>삼성 스타점(3년 연속)</li>
										<li><b>01월</b>제 2020-000057호 성과공유기업</li>
										<li><b>01월</b>제 2020-1063호 청년 친화 강소기업 선정</li>
									</ul>
								</dd>
							</dl>
					<dl>
					<dl>
						<dt>2019</dt>
						<dd class="top_tit"><em class="blue">ISO 9001</em>인증</dd>
						<dd>
							<ul>
								<li><b>01월</b>ITS-KQ-01114 ISO 9001:2015 인증 HPE Gold Parner 삼성 스타점(2년 연속)</li>
								<li><b>03월</b>경영혁신형 중소기업(Main-Biz) 선정 INTEL UNITE(무선회의시스템) IT인프라 국내 최초 구축</li>
								<li><b>04월</b>HPE Server Online Mall 런칭</li>
								<li><b>04월</b>고용노동부 강소기업 선정</li>
								<li><b>07월</b>제 19-S1019호 한국서비스품질우수기업 인증 </li>
								<li><b>11월</b>일·생활 균형 캠페인 참여 기업</li>
								<li><b>11월</b>2019 중소기업 경영혁신 우수사례 장려상 수상</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>2018</dt>
						<dd class="top_tit"><em class="blue">기업부설연구소</em> 설립</dd>
						<dd>
							<ul>
								<li><b>01월</b>제 2018110466호 기업부설연구소 인정</li>
								<li><b>05월</b>HPE Gold Parner</li>
								<li><b>05월</b>삼성 스타점</li>
								<li><b>07월</b>로지텍 골드파트너점</li>
								<li><b>07월</b>서울형 강소기업 선정</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>2017</dt>
						<dd class="top_tit"><em class="blue">HP공인서비스 지정점</em></dd>
						<dd>
							<ul>
								<li><b>04월</b>ATEN 파트너 계약</li>
								<li><b>05월</b>기술지원센터 확장 및 HP 공인서비스 지정점 선정</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>2016</dt>
						<dd class="top_tit">HP 파트너 T1 대리점 계약</dd>
						<dd>
							<ul>
								<li><b>02월</b>경영혁신인증(Mainbiz) 기업 선정</li>
								<li><b>03월</b>HP 파트너 T1 대리점 계약</li>
								<li><b>06월</b>신성 홈페이지 V2.0 구축</li>
								<li><b>07월</b>신성 SRM 전자구매시스템 V1.0 개발</li>
								<li><b>11월</b>(주)한국오키시스템즈 전문총판점 계약</li>
								<li><b>12월</b>고객중심서비스 “MOT아카데미” 강화</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>2015</dt>
						<dd class="top_tit">신성 스마트 리커버리 솔루션 구축</dd>
						<dd>
							<ul>
								<li><b>05월</b>신성 CRM V1.0 구축</li>
								<li><b>07월</b>서울 구로구 디지털로 272 한신IT타워 5층 사옥 확장 이전</li>
								<li><b>10월</b>신성 리커버리 솔루션 구축, 자산관리 솔루션 스타트업</li>
								<li><b class="white">10월</b>삼성전자 금천센터 기술점 [97384] 등록</li>
								<li><b>11월</b>가인지경영 시스템 中 2025년 비전선포</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>2014</dt>
						<dd class="top_tit">CISCO 파트너</dd>
					</dl>
					<dl>
						<dt>2013</dt>
						<dd class="top_tit">신성씨앤에스 사옥 확장 </dd>
						<dd>
							<ul>
								<li><b>01월</b>LG 전자 전문점 계약</li>
								<li><b>02월</b>서울 금천구 가산동 60-24 월드메르디앙 1차 704호 사옥 확장</li>
								<li><b>12월</b>Lenovo Distributor Agreement</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>2011</dt>
						<dd class="top_tit">삼성전자 B2B 솔루션점 대리점 계약 </dd>
						<dd>
							<ul>
								<li><b>03월</b>서울 금천구 가산동 60-24 월드메르디앙 1차 1405호로 확장</li>
								<li><b>05월</b>DELL Indirect Partner 계약</li>
								<li><b>10월</b>삼성전자 B2B 솔루션점 대리점 계약</li>
								<li><b>11월</b>생활용품 ‘마더프렌’ 물티슈 제품 출시</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>2010</dt>
						<dd class="top_tit">HP 파트너 T2 대리점 계약 </dd>
						<dd>
							<ul>
								<li><b>05월</b>HP 파트너 T2 대리점 계약</li>
								<li><b>12월</b>후지제록스 서비스 (Authorized Service Provider) 지정점 계약</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt class="whblue">2009</dt>
						<dd class="top_tit"><em class="blue">신성씨앤에스</em> 설립 </dd>
					</dl>
				</article>		

				<!-- container end -->
			</article>
			<nav id="snb" class="menu7">
				<h2>회사소개</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>