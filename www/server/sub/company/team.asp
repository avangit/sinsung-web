<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">회사소개</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb7">
					<a href="#;">팀을 만나다</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>팀을 만나다</h2>
				<!-- container -->

				<article class="team">

					<!-- titBox -->
					<div class="titBox">
						<dl>
							<dt>Meet<br />
							The Team</dt>
							<dd class="blue_tit blue">Commitment without boundaries</dd>
							<dd>우리는 고객과 동료의 성장을 위해 최선을 다합니다. <br />
							심지어 회계팀도 IT 지식이 있어야 내∙외부적으로 소통이 가능합니다.<br />
							그만큼 우리는 IT 전문지식을 학습하고 이식하는 것에 전력을 다합니다. <br />
							지식은 곧 우리가 고객에게 드릴 수 있는 가장 기본이면서 강력한 본질이기 때문입니다. <br />
							고객과 고객사의 성장은 우리의 심장을 뛰게 하고, 전 직원이 한 마음으로 매우 자랑스럽게 생각합니다.</dd>
						</dl>
					</div>
					<!-- //titBox -->
					<!-- section -->
					<div class="section">
						<p class="team_tit">IT Consultant Team</p>
						<ul>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_1.jpg" alt="" /></p>
								<p class="team_txt"><em>차장</em>&nbsp;송강기</p>
							</li>
							<!--li>
								<p class="team_img"><img src="/server/img/sub/team_i_2.jpg" alt="" /></p>
								<p class="team_txt"><em>대리</em>&nbsp;손민재</p>
							</li-->
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_3.jpg" alt="" /></p>
								<p class="team_txt"><em>과장</em>&nbsp;윤경호 </p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_3new.jpg" alt="" /></p>
								<p class="team_txt"><em>과장</em>&nbsp;김신철 </p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_new1.jpg" alt="" /></p>
								<p class="team_txt"><em>대리</em>&nbsp;최영호</p>
							</li>
				<!-- 					<li> -->
				<!-- 						<p class="team_img"><img src="/server/img/sub/team_i_4new.jpg" alt="" /></p> -->
				<!-- 						<p class="team_txt"><em>대리</em>&nbsp;이태호 </p> -->
				<!-- 					</li> -->
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_7.jpg" alt="" /></p>
								<p class="team_txt"><em>대리</em>&nbsp;김영진</p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_6_old.jpg" alt="" /></p>
								<p class="team_txt"><em>대리</em>&nbsp;이상규</p>
							</li>
				<!-- 			<li> -->
				<!-- 				<p class="team_img"><img src="/server/img/sub/team_i_6.jpg" alt="" /></p> -->
				<!-- 				<p class="team_txt"><em>스타</em>&nbsp;이정환</p> -->
				<!-- 			</li> -->
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_5.jpg" alt="" /></p>
								<p class="team_txt"><em>과장</em>&nbsp;이혜빈</p>
							</li>
				<!-- 			<li> -->
				<!-- 				<p class="team_img"><img src="/server/img/sub/team_i_4.jpg" alt="" /></p> -->
				<!-- 				<p class="team_txt"><em>스타</em>&nbsp;안형철</p> -->
				<!-- 			</li> -->
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_9.jpg" alt="" /></p>
								<p class="team_txt"><em>주임</em>&nbsp;이훈석</p>
							</li>
				<!-- 			 -->
				<!-- 			<li> -->
				<!-- 				<p class="team_img"><img src="/server/img/sub/team_i_new2.jpg" alt="" /></p> -->
				<!-- 				<p class="team_txt"><em>스타</em>&nbsp;나윤주</p> -->
				<!-- 			</li> -->
				<!-- 			<li> -->
				<!-- 				<p class="team_img"><img src="/server/img/sub/team_i_new6.jpg" alt="" /></p> -->
				<!-- 				<p class="team_txt"><em>대리</em>&nbsp;윤영보</p> -->
				<!-- 			</li> -->
						</ul>
						<!-- <p class="img100"><img src="/server/img/sub/team_i_10.jpg" alt="" /></p> -->
					</div>
					<!-- //section -->
					<!-- section -->
					<div class="section">
						<p class="team_tit">IT Technical Service team</p>
						<ul>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_11.jpg" alt="" /></p>
								<p class="team_txt"><em>과장</em>&nbsp;동근도</p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_12.jpg" alt="" /></p>
								<p class="team_txt"><em>과장</em>&nbsp;이주홍</p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_13.jpg" alt="" /></p>
								<p class="team_txt"><em>대리</em>&nbsp;박정현</p>
							</li>
				<!-- 			<li> -->
				<!-- 				<p class="team_img"><img src="/server/img/sub/team_i_14.jpg" alt="" /></p> -->
				<!-- 				<p class="team_txt"><em>대리</em>&nbsp;홍승환</p> -->
				<!-- 			</li> -->
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_17.jpg" alt="" /></p>
								<p class="team_txt"><em>스타</em>&nbsp;서명훈</p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_15new.jpg" alt="" /></p>
								<p class="team_txt"><em>주임</em>&nbsp;남덕우</p>
							</li>
				<!-- 			<li> -->
				<!-- 				<p class="team_img"><img src="/server/img/sub/team_i_16.jpg" alt="" /></p> -->
				<!-- 				<p class="team_txt"><em>스타</em>&nbsp;이재관</p> -->
				<!-- 			</li> -->
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_16new.jpg" alt="" /></p>
								<p class="team_txt"><em>주임</em>&nbsp;김범석</p>
							</li>
				<!-- 			<li> -->
				<!-- 				<p class="team_img"><img src="/server/img/sub/team_i_new3.jpg" alt="" /></p> -->
				<!-- 				<p class="team_txt"><em>스타</em>&nbsp;김태원</p> -->
				<!-- 			</li> -->
						<ul>
						</ul>
						<!-- <p class="img100"><img src="/server/img/sub/team_i_18.jpg" alt="" /></p> -->
					</div>
					<!-- //section -->
					<!-- section -->
					<div class="section">
						<p class="team_tit">Management innovation team</p>
						<ul>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_19.jpg" alt="" /></p>
								<p class="team_txt"><em>COO</em>&nbsp;박세화</p>
							</li>
				<!-- 					<li> -->
				<!-- 						<p class="team_img"><img src="/server/img/sub/team_i_20.jpg" alt="" /></p> -->
				<!-- 						<p class="team_txt"><em>주임</em>&nbsp;서명인</p> -->
				<!-- 					</li> -->
				<!-- 					<li class="last"> -->
				<!-- 						<p class="team_img"><img src="/server/img/sub/team_i_21.jpg" alt="" /></p> -->
				<!-- 						<p class="team_txt"><em>주임</em>&nbsp;윤태훈</p> -->
				<!-- 					</li> -->
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_22.jpg" alt="" /></p>
								<p class="team_txt"><em>과장</em>&nbsp;이보람</p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_23.jpg" alt="" /></p>
								<p class="team_txt"><em>주임</em>&nbsp;황수현</p>
							</li>
							<li class="last">
								<p class="team_img"><img src="/server/img/sub/team_i_24.jpg" alt="" /></p>
								<p class="team_txt"><em>BH컨설턴트</em>&nbsp;박진호</p>
							</li>
							<li class="last">
								<p class="team_img"><img src="/server/img/sub/team_i_new4.jpg" alt="" /></p>
								<p class="team_txt"><em>팀장</em>&nbsp;정우현</p>
							</li>
							<li class="last">
								<p class="team_img"><img src="/server/img/sub/team_i_new5.jpg" alt="" /></p>
								<p class="team_txt"><em>스타</em>&nbsp;김진수</p>
							</li>
						</ul>
				<!-- 				<p><img src="/server/img/sub/team_i_25.jpg" alt="" /></p> -->
					</div>
					<!-- //section -->
					<!-- section -->
					<div class="section">
						<p class="team_tit">Logistics team</p>
						<ul>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_26.jpg" alt="" /></p>
								<p class="team_txt"><em>대리</em>&nbsp;이홍선</p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_28.jpg" alt="" /></p>
								<p class="team_txt"><em>주임</em>&nbsp;정기섭</p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_28new.jpg" alt="" /></p>
								<p class="team_txt"><em>주임</em>&nbsp;이상현</p>
							</li>
							<li>
								<p class="team_img"><img src="/server/img/sub/team_i_29new.jpg" alt="" /></p>
								<p class="team_txt"><em>주임</em>&nbsp;이규황</p>
							</li>
						</ul>
				
						<!--<p><img src="/server/img/sub/team_i_29.jpg" alt="" /></p>-->
					</div>
				
					<div id="content" class="psCon">
						<ul class="img img100">
							<li><img src="/server/img/sub/img_ps1.jpg"/></li>
							<li><img src="/server/img/sub/img_ps2.jpg"/></li>
							<li><img src="/server/img/sub/img_ps3.jpg"/></li>
							<li><img src="/server/img/sub/img_ps4.jpg"/></li>
							<li><img src="/server/img/sub/img_ps5.jpg"/></li>
						</ul>
					</div>
					<!-- //section -->
				</article>				

				<!-- container end -->
			</article>
			<nav id="snb" class="menu7">
				<h2>회사소개</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>