<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">회사소개</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb7">
					<a href="#;">공지사항</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>공지사항</h2>
				<!-- container -->

				<article class="">
	


					<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
						<caption>게시판 내용</caption>
						<colgroup>
							<col width="12%" />
							<col width="" />
							<col width="12%" />
							<col width="15%" />
						</colgroup>
						<thead>
							<tr>
								<th colspan="4" class="r_none"> [안내] 카카오톡 상담서비스 오픈</th>
							</tr>
						</thead>
						<tbody>
						<tr>
							<th>등록일</th>
							<td class="left">2019-10-24</td>
							<th>조회수</th>
							<td>195</td>
						</tr>
						<!--
						<tr>
							<th>E-mail</th>
							<td class="left">psh@sinsungcns.com&nbsp;</td>
							<th>작성자</th>
							<td><span class='bold'>신성씨앤에스</span></td>
						</tr>
						-->
								<tr>
							<th>파일</th>
							<td class="left" colspan="3">
								<img src="/server/img/sub/file.gif" alt="파일">  <a href="#" class="pdR20">상세_플친_신성_20191024.jpg</a>								</td>					
						</tr>				
								
						<tr>
							<td colspan="4" class="init">
							
					
								<div class="content-area">
																																																																					
									
					
									<p><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​안녕하세요.</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">&nbsp;</span></p><p><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">&nbsp;</span></p><p><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">더욱 쉽고 빠른 상담을 위해 실시간 1:1 카카오톡 상담 서비스를 오픈하였습니다.&nbsp;</span></p><p><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">&nbsp;</span></p><p><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">하기 서비스를 편리하게 이용하실 수 있으십니다. 감사합니다.&nbsp;</span></p><p><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">&nbsp;</span></p><p style="line-height: 1.8;"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">▶&nbsp;</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​기술/서비스/문의</span></p><p style="line-height: 1.8;"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;"></span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">▶ 견적문의 (30분 이내)</span></p><p style="line-height: 1.8;"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;"></span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">▶</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​ 서버 라인업</span></p><p style="line-height: 1.8;"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;"></span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">▶</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​ 워크스테이션 라인업</span></p><p style="line-height: 1.8;"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;"></span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">▶</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​ 화상회의 제안서</span></p><p style="line-height: 1.8;"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;"></span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">▶</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​ 맞춤형 전자 발주시스템! 바로가기</span></p><p style="line-height: 1.8;"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;"></span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">▶</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​ 회사소개서 다운받기&nbsp;</span><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">​</span></p><p style="line-height: 1.8;"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">&nbsp;</span></p><p><font face="나눔고딕, NanumGothic, sans-serif"><span style="font-size: 11pt; font-family: 나눔고딕, NanumGothic, sans-serif;">카카오톡 상담 바로가기 :&nbsp;<a href="http://pf.kakao.com/_kxipdT" target="_self"><span style="font-family: 나눔고딕, NanumGothic, sans-serif; font-size: 11pt;">http://pf.kakao.com/_kxipdT</span></a></span></font><span style="font-size: 11pt; font-family: 나눔고딕, NanumGothic, sans-serif;"><a href="http://pf.kakao.com/_kxipdT" target="_self">​</a></span>&nbsp;</p>			</div>
							
							</td>
						</tr>
					
						<!-- 파일첨부 -->
						
						<!-- //파일첨부 -->
					
						</tbody>
					</table>
					
					<!--버튼-->
					<div class="ntb-tb-view-btn" style="width:100%">
						<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='notice.asp'">
						<div class="btnr">
								
										</div>
					</div>
					
					
					
					
						<div class="ntb-tb-view-reply">
					
							<!--이전글 시작-->	
							<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
								<caption>게시판 이전/다음글</caption>
								<colgroup>
									<col width="12%" />
									<col width="" />
								</colgroup>
											<tr> 
									<th width="12%">이전글</th>
									<td width="88%" class="left font_gray">
																						<a href="#">
																	[안내] 신성 Free 케어 7drivers 출시</a>
																				</td>
								</tr>
									
								<!--다음글 시작-->
											<tr> 
									<th width="12%">다음글</th>
									<td width="88%" class="left font_gray">
															다음글이 없습니다.
														</td>
								</tr>
							</table>
						</div>
					
					
					
					
					
					<!-- 코멘트 시작 -->
										
							<!-- 코멘트 종료--></article>
				<!-- container end -->
			</article>
			<nav id="snb" class="menu7">
				<h2>회사소개</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>