<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">회사소개</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb7">
					<a href="#;">찾아오시는 길</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>찾아오시는 길</h2>
				<!-- container -->

				<article class="map">
					<!-- * Daum 지도 - 지도퍼가기 -->
					<!-- 1. 지도 노드 -->
					<div id="daumRoughmapContainer1464588300537" class="root_daum_roughmap root_daum_roughmap_landing"></div>
				
					<!--
						2. 설치 스크립트
						* 지도 퍼가기 서비스를 2개 이상 넣을 경우, 설치 스크립트는 하나만 삽입합니다.
					-->
					<script charset="UTF-8" class="daum_roughmap_loader_script" src="http://dmaps.daum.net/map_js_init/roughmapLoader.js"></script>
				
					<!-- 3. 실행 스크립트 -->
					<script charset="UTF-8">
						new daum.roughmap.Lander({
							"timestamp" : "1464588300537",
							"key" : "bzjg",
							"mapHeight" : "419"
						}).render();
					</script>
					<p class="cont_tit mt30">(주)신성씨엔에스</p>
					<dl style="border-top:2px solid #01a982;">
						<dt>주소</dt>
						<dd>서울 구로구 디지털로 272, 한신IT타워 5층 </dd>
					</dl>
					<dl>
						<dt>Call Us </dt>
						<dd>02-867-2626</dd>
					</dl>
					<dl>
						<dt>Fax </dt>
						<dd>02-867-2621</dd>
					</dl>
					<dl>
						<dt>E-Mail </dt>
						<dd>psh@sinsungcns.com</dd>
					</dl>
					<p class="btn_print fl_right"><a onclick="javascript:print(document.getElementById('content').innerHTML)" style='cursor:pointer'><img src="/server/img/sub/icon_print.png" alt="" />&nbsp;&nbsp;출력하기</a></p>
				</article>
				
				<style>
				@media print {
					#header, #footer, #rightS, .ftLogo, .btn_print { display:none; }
					#content { font-size:16px; }
				}
				</style>

				<!-- container end -->
			</article>
			<nav id="snb" class="menu7">
				<h2>회사소개</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>