<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">회사소개</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb7">
					<a href="#;">공지사항</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>공지사항</h2>
				<!-- container -->

				<article class="">


					<script>
					<!--
					function search_chk(){
						var frm	=	document.search_form;

						if(frm.s_o.value==''){
							alert("검색어를 입력하여 주세요!");
							frm.s_o.focus();
						}else{
							frm.submit();
						}

					}

					function search_chk2(){
						var frm	=	document.sch_Form;

						frm.submit();

					}

					//-->
					</script>

								<table id="empTable">
									<caption class="blind">공지사항 게시판</caption>
									<colgroup>
										<col width="10%">
															<col width="">
										<!--<col width="10%">-->
										<col width="10%">
										<col width="12%">
									</colgroup>
									<tr>
										<th>번호</th>
															<th>제목</th>
										<!--<th>작성자</th>-->
										<th>조회수</th>
										<th>작성일</th>
									</tr>
													<tr>
										<td><img src="/server/img/sub/icon_notice.gif" alt="공지"></td>
															<td class="left"><a href="notice_view.asp">[안내] 카카오톡 상담서비스 오픈</a></td>
										<!--<td><span class='bold'>신성씨앤에스</span></td>-->
										<td>193</td>
										<td>2019/10/24</td>
									</tr>

																	<tr>
										<td>6</td>
															<td class="left">
																								<a href="notice_view.asp">
																	[안내] 신성 Free 케어 7drivers 출시</a>&nbsp;&nbsp;
																</td>
										<!--<td>신성씨앤에스</td>-->
										<td>179</td>
										<td>2019/10/24</td>

									</tr>
													<tr>
										<td>5</td>
															<td class="left">
																								<a href="notice_view.asp">
																	한국서비스품질 우수기업 인증</a>&nbsp;&nbsp;
																</td>
										<!--<td>신성씨앤에스</td>-->
										<td>156</td>
										<td>2019/10/11</td>

									</tr>
													<tr>
										<td>4</td>
															<td class="left">
																								<a href="notice_view.html">
																	워크샵 일정으로 인한 근무일정 안내</a>&nbsp;&nbsp;
																</td>
										<!--<td>신성씨앤에스</td>-->
										<td>262</td>
										<td>2019/05/16</td>

									</tr>
													<tr>
										<td>3</td>
															<td class="left">
																								<a href="notice_view.html">
																	[안내] 거래명세표 공급받는 자가 보이지 않을 때 조치사항입니다.</a>&nbsp;&nbsp;
																</td>
										<!--<td>신성씨앤에스</td>-->
										<td>695</td>
										<td>2016/08/25</td>

									</tr>
													<tr>
										<td>2</td>
															<td class="left">
																								<a href="notice_view.html">
																	[안내] 신성씨앤에스 SRM 전자구매시스템 V1.0 개발이 완료되었습니다..</a>&nbsp;&nbsp;
																</td>
										<!--<td>신성씨앤에스</td>-->
										<td>735</td>
										<td>2016/07/28</td>

									</tr>
													<tr>
										<td>1</td>
															<td class="left">
																								<a href="notice_view.html">
																	신성씨앤에스 홈페이지 버전 2.0 이 오픈되었습니다.</a>&nbsp;&nbsp;
																</td>
										<!--<td>신성씨앤에스</td>-->
										<td>614</td>
										<td>2016/06/22</td>

									</tr>
																</table>
					<!--버튼-->
					<div class="ntb-listbtn-area mgT10"></div>
							<!--검색폼-->
						<div class="ntb-search-area">
							<form name="search_form" action="notice.php" method="post">
								<input type="hidden" name="code" value="notice">
								<input type="hidden" name="cate_idx" value="">
								<select name="s_i" class="AXSelect vmiddle">
									<option value="subject" >제목</option>
									<option value="name" >작성자</option>
									<option value="content" >내용</option>
								</select>
								<input type="text" name="s_o" value="" class="AXInput vmiddle" />
								<input type="submit" value="검색" class="AXButton">
							</form>
						</div>


							<div class="page">
							<ul class="clfix">
								<li><a href='#'>&lt;&lt;</a></li>  <li class='on'><a href='#'>1</a></li> <li><a href='#'>&gt;&gt;</a></li></ul>
						</div>


					</article>

				<!-- container end -->
			</article>
			<nav id="snb" class="menu7">
				<h2>회사소개</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>