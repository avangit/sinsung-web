<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">회사소개</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb7">
					<a href="#;">대표 인사말</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>대표 인사말</h2>
				<!-- container -->

				<article class="greeCon">
					<h4>안녕하십니까?<br/><span>㈜신성씨앤에스</span> 대표 전성우입니다.</h4>
					<ul>
						<li>2009년 7월과 2015년 11월 14일은 우리에게 아주 귀한 날입니다.</li>
						<li>2009년 7월은 우리가 고객사의 올바른 IT 업무환경 구현을 위해<br/>기업이 각社의 구매 기준과 예산 안에서 더 나은 구매 결정을 내릴 수 있도록<br/>제품 전문지식, 솔루션 컨설팅, 기술 지원 서비스를 제공함으로써<br/>고객 섬기기를 시작한 감사한 날입니다.</li>
						<li>2015년 11월 14일은 성장 초기로 신성씨앤에스의 모든 조직원이 하나되어<br/>우리의 비전과 사명을 정의하고 나아가야 할 1년/3년/10년의 Horizon map을<br/>설계한 소중한 날입니다.</li>
						<li>‘고객에게 필요한 가치는 무엇이며, 그 가치는 과연 옳은 것인가?’<br/>또한 ‘더 나은 가치를 어떻게 기여할 것인가’를 우리의 핵심문제로 정의하고<br/>	이에 피드백 시스템을 통하여 끊임없이 학습하고 있습니다.</li>
						<li>그 가치의 첫 번째는 신성 리커버리 솔루션입니다.<br/>2015년부터는 신성 리커버리 솔루션을 통해 고객사의 시스템과 데이터 백업으로<br/>업무의 연속성을 보장하여 생산성을 향상시키고,<br/>10초의 빠른 복구로 비용과 시간을 최대 절감시킬 수 있습니다.<br/>두 번째는 자산관리 솔루션을 통한 IT 장비의 이력 관리를 편하고 쉽게 할 수 있도록<br/>개발하고 있습니다.</li>
						<li>신성의 핵심가치인 신뢰, 열정, 감사정신, 성장으로<br/>고객중심 서비스를 실천하겠습니다.</li>
						<li>감사합니다.</li>
						<!--<li class="sign"><span>(주)신성씨앤에스 대표이사</span>전 성 우</li>-->
					</ul>
					<div>
						<img src="/server/img/sub/img_greeting.jpg" alt="대표이사 이미지" />
						<p><img src="/server/img/sub/sign.jpg" alt="(주)신성씨엔에스 대표이사 전성우"/></p>
					</div>
				</article>				

				<!-- container end -->
			</article>
			<nav id="snb" class="menu7">
				<h2>회사소개</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>