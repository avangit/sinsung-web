<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">회사소개</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb7">
					<a href="#;">회사성장</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>회사성장</h2>
				<!-- container -->

				<article class="growth">
				<h4>신용등급 <span>BB</span> / 현금흐름등급 <span>B</span> / Watch등급 <span>정상</span> <em> (평가기관 : ㈜이크레더블 2017년 재무결산일)</em></h4>
				<p class="img100"><img src="/server/img/sub/growth_server.jpg" ></p>
				</article>

				<!-- container end -->
			</article>
			<nav id="snb" class="menu7">
				<h2>회사소개</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>