<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">구축사례</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb4">
					<a href="#;">구축사례</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>구축사례</h2>
				<!-- container -->
				<script language="javascript" src="/server/common/js/product.js"></script>

				<article class="case">
				<script>
				<!--
				function search_chk(){
					var frm	=	document.search_form;

					if(frm.s_o.value==''){
						alert("검색어를 입력하여 주세요!");
						frm.s_o.focus();
					}else{
						frm.submit();
					}

				}

				function search_chk2(){
					var frm	=	document.sch_Form;

					frm.submit();

				}

				//-->
				</script>
				<script language="javascript" src="/common/js/product_case.js"></script>

				<ul id="tabMenu_s2" class="mb30">
					<li rel="m1" class="active" onClick="location.href='construction_case.asp'">하드웨어</li>
					<li rel="m2"  onClick="location.href='construction_case2.asp'">솔루션</li>
				</ul>


				<div class="sch clfix">
					<form name="sch_Form" method="POST" action="construction_case.asp">
					<input type="hidden" name="s_i" value="all">


									<select name="p1" id="part1_code">
						<option value="">&nbsp;  :::  카테고리 :::  </option>
											<option value="1527743407" >&nbsp; 컴퓨터군</option>
										</select>

									<input type="text" name="s_o" placeholder="검색어를 입력하세요.">
					<a href="javascript:search_chk2()">검색</a>
					</form>
				</div>
				<div class="article">
					<ul>
						<li>
							<table class="article_tbl">
								<caption class="blind">구축사례-하드웨어</caption>
								<colgroup>
									<col width="60px">
									<col width="202px">
									<col width="*">
								</colgroup>
								<tr>
									<td class="taC">59</td>
									<td class="article_img">
										<a href="./construction_case_view.asp"><img src="/server/img/sub/15946367481.jpg" border="0"></a>
									</td>
									<td class="article_con">
										<p class="tit"><a href="./construction_case_view.asp">[대한혈액학회] 화상회의 장비 설치</a> </p>
										<p class="date">등록일 : 2020/07/13 <span class="pdL30">조회수 : 26</span></p>
										<p class="con"><a href="./construction_case_view.asp">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;지난 7월 7일에 종로에 있는 대한혈액학회에&nbsp;화상회의 장비를 구축하였습니다. 설치 모델은 로지텍 Rally입니다.&nbsp;로지텍 Rally는 스튜디오 퀄리티의 비디오 품질과 최고 수준의 음성 선명도를 제공하며 RightSense 회의 자동화 기술을 통해 Google Hangouts Meet, Microsoft Skype® for Business, Microsoft Teams 및 Zoom 등 USB 장치와 호환되는 모든 화상 회의 애플리케이션을 이용한 회의를 원활하게 만들어 드립니다. 모듈식 오디오와 편리한 케..</a></p>
									</td>
								</tr>
							</table>
						</li>
						<li>
							<table class="article_tbl">
								<caption class="blind">구축사례-하드웨어</caption>
								<colgroup>
									<col width="60px">
									<col width="202px">
									<col width="*">
								</colgroup>
								<tr>
									<td class="taC">58</td>
									<td class="article_img">
										<a href="construction_case_view.html"><img src="/server/img/sub/15946365331.jpg" border="0"></a>
									</td>
									<td class="article_con">
										<p class="tit"><a href="construction_case_view.html">[디지털컨버전스] 컴퓨터 설치</a> </p>
										<p class="date">등록일 : 2020/07/13 <span class="pdL30">조회수 : 22</span></p>
										<p class="con"><a href="construction_case_view.html">&nbsp;&nbsp;&nbsp;&nbsp;지난 7월 9일 디지털컨버전스협회 신규 교육장에 교육용&nbsp;PC를 구축하였습니다. ﻿설치 모델은 HP EliteDesk 800 G5 TWR이며 119대를 설치한 대량 건입니다.  압도적인 속도와 안정성을 자랑하는 HP 800 G5은 보안성과&nbsp;관리성을 갖춘 엘리트 PC입니다. 빠른 속도와 안정성을 자랑하는 제품으로 기업에 인기가 많은 제품입니다. 교육장의 특성상 수강이 끝나면 포멧작업이 이루어져야 하므로 신성리커버리를 탑재하였습니다.&nbsp;신성리커버..</a></p>
									</td>
								</tr>
							</table>
						</li>
						<li>
							<table class="article_tbl">
								<caption class="blind">구축사례-하드웨어</caption>
								<colgroup>
									<col width="60px">
									<col width="202px">
									<col width="*">
								</colgroup>
								<tr>
									<td class="taC">57</td>
									<td class="article_img">
										<a href="construction_case_view.html"><img src="/server/img/sub/15946360691.jpg" border="0"></a>
									</td>
									<td class="article_con">
										<p class="tit"><a href="construction_case_view.html">[한국정신분석학회] 화상회의 장비 설치</a> </p>
										<p class="date">등록일 : 2020/07/13 <span class="pdL30">조회수 : 25</span></p>
										<p class="con"><a href="construction_case_view.html">&nbsp;지난 7월 1일 종로에 있는 한국정신분석학회에&nbsp;화상회의 장비를&nbsp;구축하였습니다. 설치 모델은&nbsp;로지텍 Rally Plus모델입니다. 로지텍 Rally는 스튜디도 퀄리티의 비디오 품질과 최고 수준의 음성 선명도를제공하여 RghtSense회의 자동화 기술을 통해&nbsp;Google Hangouts Meet, Microsoft Skype® for Business, Microsoft Teams 및 Zoom 등 USB 장치와 호환되는 모든 화상 회의 애플리케이션을 이용한 회의를 원활하게 만들어 드립니다.모듈식 오디오와 편리한 케이블 관..</a></p>
									</td>
								</tr>
							</table>
						</li>
						<li>
							<table class="article_tbl">
								<caption class="blind">구축사례-하드웨어</caption>
								<colgroup>
									<col width="60px">
									<col width="202px">
									<col width="*">
								</colgroup>
								<tr>
									<td class="taC">56</td>
									<td class="article_img">
										<a href="construction_case_view.html"><img src="/server/img/sub/1594635376.jpg" border="0"></a>
									</td>
									<td class="article_con">
										<p class="tit"><a href="construction_case_view.html">[한국전력공사] 모니터 설치</a> </p>
										<p class="date">등록일 : 2020/07/13 <span class="pdL30">조회수 : 19</span></p>
										<p class="con"><a href="construction_case_view.html">&nbsp;&nbsp;수원시 팔달구에 위치한 한국전력공사에 실시간모니터링용 모니터를 설치하였습니다. KVM을 이용하여 여러대의 모니터에 관제상황을 확인 할 수 있습니다. 설치 모델은 삼성 LS27R650FDKXKR 27인치 모니터 8대와 COMS HDMI KVM분배기 4대입니다.신성씨앤에스는 고객님의 다양한 비즈니스양한 요구에&nbsp;맞춰 제안을 해드리고 무료케어 서비스까지&nbsp;진행하고있습니다.&nbsp;-구축 모델-&nbsp;COMS HDMI KVM 분배기&nbsp;삼성 LS27R650FDKXKR 27인치 모니터 &nbsp;&nbsp;&..</a></p>
									</td>
								</tr>
							</table>
						</li>
						<li>
							<table class="article_tbl">
								<caption class="blind">구축사례-하드웨어</caption>
								<colgroup>
									<col width="60px">
									<col width="202px">
									<col width="*">
								</colgroup>
								<tr>
									<td class="taC">55</td>
									<td class="article_img">
										<a href="./construction_case_view.asp"><img src="/server/img/sub/15935000601.jpg" border="0"></a>
									</td>
									<td class="article_con">
										<p class="tit"><a href="construction_case_view.asp">[CJ CGV] HPE 서버 설치</a> </p>
										<p class="date">등록일 : 2020/06/30 <span class="pdL30">조회수 : 21</span></p>
										<p class="con"><a href="construction_case_view.asp">&nbsp;인천 미추홀구 학익동에 있는 CJ CGV에 서버를 설치 하였습니다. 설치모델은 HPE ProLiant DL380 입니다. 해당 서버는&nbsp;영화 영사를 위해 설치되었습니다.&nbsp;HPE ProLiant DL380서버는 비즈니스요구에 따라 확장할 수 있도록 하여 미래에 대비하여 투자가 가능하며 뛰어난 에너지 효율성을 자랑합니다.지금 신성씨앤에스에서 고객의 필요에&nbsp;맞는 제안부터 무료로 제공되는 케어서비스까지 만나보세요.&nbsp;-구축 모델-HPE DL380 Sever&nbsp;&nbsp;</a></p>
									</td>
								</tr>
							</table>
						</li>

					</ul>
				</div>

				<!--버튼-->
				<div class="ntb-listbtn-area mgT10"></div>
				<div class="page">
					<ul class="clfix">
					<li><a href='/server/sub/experience/construction_case.asp?startPage=0&code=case'>&lt;&lt;</a></li>
					<li class='on'><a href='#'>1</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=5&code=case'>2</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=10&code=case'>3</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=15&code=case'>4</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=20&code=case'>5</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=25&code=case'>6</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=30&code=case'>7</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=35&code=case'>8</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=40&code=case'>9</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=45&code=case'>10</a></li>
					<li><a href='/server/sub/experience/construction_case.asp?startPage=50&code=case'>&gt;</a></li>  <li><a href='/server/sub/experience/construction_case.asp?startPage=55&code=case'>&gt;&gt;</a></li> 		</ul>			</div>


			</article>
			<!-- container end -->
			</article>
			<nav id="snb" class="menu4">
				<h2>구축사례</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>