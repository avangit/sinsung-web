<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">구축사례</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb4">
					<a href="#;">구축사례</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>구축사례</h2>
				<!-- container -->
<script language="javascript" src="/server/common/js/product.js"></script>

<article class="case">
	


<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
	<caption>게시판 내용</caption>
	<colgroup>
		<col width="12%" />
		<col width="" />
		<col width="12%" />
		<col width="15%" />
	</colgroup>
	<thead>
		<tr>
			<th colspan="4" class="r_none"> [대한혈액학회] 화상회의 장비 설치</th>
		</tr>
	</thead>
	<tbody>
	<tr>
		<th>등록일</th>
		<td class="left">2020-07-13</td>
		<th>조회수</th>
		<td>29</td>
	</tr>
	<!--
	<tr>
		<th>E-mail</th>
		<td class="left">psh@sinsungcns.com&nbsp;</td>
		<th>작성자</th>
		<td><span class='bold'>신성씨앤에스</span></td>
	</tr>
	-->
				
	<tr>
		<td colspan="4" class="init">
		

			<div class="content-area">
				
				

				<p><img title="1594636603.jpg" src="http://www.sinsungcns.com/se2/upload/1594636603.jpg"><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></p><p><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></p><p><img title="1594636614.jpg" src="http://www.sinsungcns.com/se2/upload/1594636614.jpg"><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></p><p><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></p><p><img title="1594636625.jpg" src="http://www.sinsungcns.com/se2/upload/1594636625.jpg"><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></p><p><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></p><p style="line-height: 1.5;"><span style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>지난 7월 7일에 종로에 있는 대한혈액학회에&nbsp;화상회의 장비를 구축하였습니다. 설치 모델은 로지텍 Rally입니다.&nbsp;로지텍 Rally는 스튜디오 퀄리티의 비디오 품질과 최고 수준의 음성 선명도를 제공하며 RightSense 회의 자동화 기술을 통해 Google Hangouts Meet, Microsoft Skype® for Business, Microsoft Teams 및 Zoom 등 USB 장치와 호환되는 모든 화상 회의 애플리케이션을 이용한 회의를 원활하게 만들어 드립니다. 모듈식 오디오와 편리한 케이블 관리 및 고급스러운 소재와 디자인이 돋보이는 Rally는 중대형 회의공간을 작은 회의실에 설치하기 좋습니다.&nbsp; 자체 기기에서 사운드와 마이크도 지원하기때문에 깔끔하고 간소하게 설치가 가능합니다.</span></p><p style="line-height: 1.5;"><span class="se-fs- se-ff-nanumbarungothic   " id="SE-d4147ada-d4b2-4f57-8213-1c002df1e821" style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>주식회사 신성씨앤에스는 올해부터 </span><span class="se-fs- se-ff-nanumbarungothic   " id="SE-8b24bf02-5fb0-4c30-a391-93373e69868f" style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><b>로지텍의</b></span><span class="se-fs- se-ff-nanumbarungothic   " id="SE-5c67e1b8-0031-4c8b-92fd-fce2d6a66b36" style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><b> </b></span><span class="se-fs- se-ff-nanumbarungothic   " id="SE-3c4c316d-5acb-4462-b3cd-32b98c5a2248" style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><b>최고 등급인 Premier 파트너로 등급이 선정​</b></span><span class="se-fs- se-ff-nanumbarungothic   " id="SE-2e7531eb-2dc8-4a30-8aa0-d07b5675aae8" style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>이 되어 <span class="se-fs- se-ff-nanumbarungothic   " id="SE-befb67cc-740d-4103-932d-a049a6ffe267" style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>화상회의 장비 및 솔루션의 ‘가격/설치/서비스’ 부문에서<span class="se-fs- se-ff-nanumbarungothic   " id="SE-325d81f9-32cb-4375-885f-27313ae3f73c" style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>국내 최고 수준의 기술력과 경쟁력을 갖추게 되었습니다.</span></span></span></p><p style="line-height: 1.5;"><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></p><p style="line-height: 1.5;">&nbsp;</p><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><p style="line-height: 1.5;"><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'></span></span><p style="line-height: 1.5;"><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'></span></span><p style="line-height: 1.5;"><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(0, 117, 200); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(0, 117, 200); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>-구축 모델-</span></span></span></span></span></span><p><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></span></span></p><p><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;로지텍 Rally</span></span></span></span></p><p style="line-height: 1.5;"><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></p></span><p style="line-height: 1.5;"><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'></span></span></span></span><p style="line-height: 1.5;"><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span class="se-fs- se-ff-nanumbarungothic   " style='color: rgb(70, 70, 70); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1525&amp;part_idx=40&amp;o=1" target="_self"><img width="301" height="286" title="1584508812.png" style="width: 313px; height: 303px;" src="http://www.sinsungcns.com/se2/upload/1584508812.png"></a></span></span></span><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;<span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'><span style='font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 12pt;'>&nbsp;</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>			</div>
		
		</td>
	</tr>

	<!-- 파일첨부 -->
	
	<!-- //파일첨부 -->

	</tbody>
</table>

<!--버튼-->
<div class="ntb-tb-view-btn" style="width:100%">
	<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='/server/sub/experience/construction_case.asp?mode=l&code=case'">
	<div class="btnr">
			
					</div>
</div>




	<div class="ntb-tb-view-reply">

		<!--이전글 시작-->	
		<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
			<caption>게시판 이전/다음글</caption>
			<colgroup>
				<col width="12%" />
				<col width="" />
			</colgroup>
						<tr> 
				<th width="12%">이전글</th>
				<td width="88%" class="left font_gray">
																	<a href="construction_case.asp">
												[디지털컨버전스] 컴퓨터 설치</a>
															</td>
			</tr>
				
			<!--다음글 시작-->
						<tr> 
				<th width="12%">다음글</th>
				<td width="88%" class="left font_gray">
										다음글이 없습니다.
									</td>
			</tr>
		</table>
	</div>





<!-- 코멘트 시작 -->
					
		<!-- 코멘트 종료--></article>
<!-- container end -->

			</article>
			<nav id="snb" class="menu4">
				<h2>구축사례</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>