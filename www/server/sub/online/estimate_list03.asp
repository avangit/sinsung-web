<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">온라인견적</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb5">
					<a href="#;">맞춤사양견적</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>
					<ul class="tabs tab_s1">
						<li id="tab11"><a href="estimate_list.asp">랙서버</a></li>
						<li id="tab12"><a href="estimate_list02.asp">타워서버</a></li>
						<li id="tab13" class="current"><a href="estimate_list03.asp">블레이드서버</a></li>
						<li id="tab13" ><a href="estimate_list04.asp">스토리지</a></li>
						<li id="tab13" ><a href="estimate_list05.asp">워크스테이션</a></li>
					</ul>
				</h2>
				<!-- container -->
				<article class="estCon" id="tabsholder1">
					<div class="contents">
						<div id="content1" class="tabscontent tabCon_est">
						</div>
					</div>
				</article>
				<!-- container end -->
				<script type="text/javascript">
					// 온라인 견적 scroll
					var tab_pos = $(".tab_s1").offset().top;
					var headerWrap = $(".headerWrap");
					
					tab_top(headerWrap.height());
					$(window).resize(function(){
						tab_top(headerWrap.height());
					});

					$(window).scroll(function(){
						var scr_pos = $(this).scrollTop();
						if ( scr_pos >= tab_pos -headerWrap.height()){
							$(".tab_s1").addClass("fixed");
						}else{
							$(".tab_s1").removeClass("fixed");
						};
					});

					function tab_top(num){
						$(".tab_s1").css({
							top:num
						});	
					};
				</script>
			</article>
			<nav id="snb" class="menu5">
				<h2>온라인견적</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>