<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">온라인견적</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb5">
					<a href="#;">맞춤사양견적</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>
					<ul class="tabs tab_s1">
						<li id="tab11" class="current"><a href="estimate_list.asp">랙서버</a></li>
						<li id="tab12" ><a href="estimate_list02.asp">타워서버</a></li>
						<li id="tab13" ><a href="estimate_list03.asp">블레이드서버</a></li>
						<li id="tab13" ><a href="estimate_list04.asp">스토리지</a></li>
						<li id="tab13" ><a href="estimate_list05.asp">워크스테이션</a></li>
					</ul>
				</h2>
				<!-- container -->
				<article class="estCon" id="tabsholder1">
					<div class="contents">
						<div id="content1" class="tabscontent tabCon_est">
							<ul>
								<li>
									<div>
										<a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.asp?idx=1629&part_idx=20&referer=server"><img src="/server/img/sub/15556533541.png" alt=""/></a>
									</div>
									<dl>
										<dt>HPE DL20 Gen10</dt>
											<dd>경제적인 솔루션을 위한 단순한 초소형 디자인</dd>
									</dl>
								</li>
								<li>
									<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
										<dt>HPE DL20 Gen10 Intel Xeon E-2124<br /></dt>
										<dd>HPE DL20 Gen10 Intel Xeon E-2124<br />
										HPE 16GB (1x16GB) Dual Rank x8<br />
										HPE 4TB SATA 6G Midline<br />
										HPE DL20 Gen10 290W<br />
										</dd>
										<dd class="price">￦0<span>(부가세 포함)</span></dd>
										<dd class="btn"><a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.asp?idx=1629&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
									</dl>
								</li>
								<li>
									<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
										<dt>HPE DL20 Gen10 Intel Xeon E-2134</dt>
										<dd>HPE DL20 Gen10 Intel Xeon E-2134<br />
										HPE 16GB (1x16GB) Dual Rank x8<br />
										HPE 4TB SATA 6G Midline<br />
										HPE Smart Array E208i-a SR Gen10</dd>
										<dd class="price">￦0<span>(부가세 포함)</span></dd>
										<dd class="btn"><a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.asp?idx=1629&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
								</li>
							</ul>
							<ul>
								<li>
									<div>
										<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1628&part_idx=20&referer=server"><img src="/server/img/sub/15556492511.png" alt=""/></a>
									</div>
									<dl>
										<dt>DL360 Gen10</dt>
										<dd>유연한 컴퓨팅을 갖춘 업계 최고의 성능</dd>
									</dl>
								</li>
								<li>
									<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
										<dt>HPE DL360 Gen10 Intel Xeon-Bronze 3104</dt>
										<dd>Intel Xeon-Bronze 3104 (1.7GHz/6-core/85W)<br />
										8GB (1x8GB) Single Rank x8 DDR4-2666<br />
										1TB SATA 6G<br />
										500W</dd>
										<dd class="price">￦0<span>(부가세 포함)</span></dd>
										<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1628&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
									</dl>
								</li>
								<li>
									<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
										<dt>HPE DL360 Gen10 Intel Xeon-Bronze 3106</dt>
										<dd>Intel Xeon-Bronze 3106 (1.7GHz/8-core/85W)<br />
										16GB (1x16GB) Single Rank x4 DDR4-2666<br />
										4TB SATA 6G<br />
										800W</dd>
										<dd class="price">￦0<span>(부가세 포함)</span></dd>
										<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1628&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
								</li>
							</ul>
						<ul>
							<li>
								<div>
									<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1086&part_idx=20&referer=server"><img src="/server/img/sub/15295456991.png" alt=""/></a>
								</div>
								<dl>
									<dt>HPE DL380 Gen10</dt>
									<dd>세계적 수준의 성능 및 에너지 효율성 </dd>
								</dl>
							</li>
							<li>
								<p class="new"></p>
								<dl class="estBox">
									<dt>HPE ProLiant DL380 Gen10 3106<br /></dt>
									<dd>Bronze 3106 8-Core 1.70GHz <br />
									16GB DDR4-2666V-R <br />
									Dynamic Smart Array S100i <br />
									331i (4*1GbE) <br />
									8 LFF HDD Bay <br />
									1 x 500W Flex Slot(94%+)</dd>
									<dd class="price">￦0<span>(부가세 포함)</span></dd>
									<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1086&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
								</dl>
							</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL380 Gen10 4114</dt>
							<dd>Xeon-S 4114 10-Core 2.2GHz <br />
32GB DDR4-2666V-R <br />
Dynamic Smart Array P408i-a<br />
331i (4*1GbE) <br />
8 SFF HDD Bay <br />
1 x 500W Flex Slot(94%+)</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1086&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						<ul>
				<li>
					<div>
											<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1085&part_idx=20&referer=server"><img src="/server/img/sub/15121184171.jpg" alt=""/></a>
										</div>
					<dl>
						<dt>HPE DL160 Gen9</dt>
							<dd>가용성, 확장성 및 서비스 용이성 - 최적의 조합</dd>
					</dl>
				</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL160 Gen9 E5-2603v4<br />
</dt>
							<dd>E5-2603v4 LFF Ety Svr<br />
1 x 8GB DDR4-2400T-R<br />
Dynamic Smart Array B140i<br />
LFF HP 4 Bay<br />
361i Adapter (2x1GbE)<br />
1 x 550W NHP (89%)</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1085&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
					</dl>
									</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL160 Gen9 E5-2630v4<br />
</dt>
							<dd>E5-2630v4 SFF AP Svr/Prom<br />
1 x 8GB DDR4-2400T-R<br />
Dynamic Smart Array B140i<br />
SFF HP 8 Bay<br />
361i Adapter (2x1GbE)<br />
1 x 900W Redundant (92%)</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1085&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						<ul>
				<li>
					<div>
											<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1084&part_idx=20&referer=server"><img src="/server/img/sub/15121181201.png" alt=""/></a>
										</div>
					<dl>
						<dt>HPE DL20 Gen9</dt>
							<dd>가용성, 확장성 및 서비스 용이성 - 최적의 조합</dd>
					</dl>
				</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL20 Gen9 E3-1220v5 <br />
</dt>
							<dd>E7-4809v3 2P 64GB Svr<br />
4 x 16GB DDR4-2133R<br />
Smart Array P830i<br />
SFF HP 5 Bay<br />
331FLR Adapter (4x1GbE)<br />
4 x 1500W Common Slot(94%+)</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1084&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
					</dl>
									</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL20 Gen9 E3-1240v5 <br />
</dt>
							<dd>E7-8893v3 4P 256GB Svr<br />
16 x 16GB DDR4-2133R<br />
Smart Array P830i <br />
SFF HP 5 Bay<br />
534FLR Adapter (2x10GbE)<br />
4 x 1500W Common Slot(94%+)</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1084&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						<ul>
				<li>
					<div>
											<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1083&part_idx=20&referer=server"><img src="/server/img/sub/15121170731.png" alt=""/></a>
										</div>
					<dl>
						<dt>HPE DL580 Gen9</dt>
							<dd>가용성, 확장성 및 서비스 용이성 - 최적의 조합</dd>
					</dl>
				</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL580 Gen9 E7-4809v3 <br />
</dt>
							<dd>E7-4809v3 2P 64GB Svr<br />
4 x 16GB DDR4-2133R<br />
Smart Array P830i<br />
SFF HP 5 Bay<br />
331FLR Adapter (4x1GbE)<br />
4 x 1500W Common Slot(94%+)</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1083&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
					</dl>
									</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL580 Gen9 E7-8893v3 <br />
</dt>
							<dd>E7-8893v3 4P 256GB Svr<br />
16 x 16GB DDR4-2133R<br />
Smart Array P830i <br />
SFF HP 5 Bay<br />
534FLR Adapter (2x10GbE)<br />
4 x 1500W Common Slot(94%+)</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1083&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						<ul>
				<li>
					<div>
											<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1080&part_idx=20&referer=server"><img src="/server/img/sub/15121042501.png" alt=""/></a>
										</div>
					<dl>
						<dt>HPE DL180 Gen9</dt>
							<dd>새로운 기준을 충족하기 위한 확장성 및 성능</dd>
					</dl>
				</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL180 Gen9 E5-2603v4<br />
</dt>
							<dd>E5-2603v4 NHP Ety Svr<br />
1 x 4GB DDR4- 2133U<br />
Dynamic Smart Array B140i<br />
LFF HP 4 Bay<br />
Broadcom 5720(2x1GbE)<br />
1 x 550W NHP (89%)<br />
<br />
</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1080&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
					</dl>
									</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>HPE ProLiant DL180 Gen9 E5-2630v4 </dt>
							<dd>E5-2630v4 SFF AP Svr/Prom<br />
1 x 8GB DDR4-2400T-R<br />
Dynamic Smart Array B140i<br />
SFF HP 8 Bay<br />
Broadcom 5720(2x1GbE)<br />
1 x 900W Redundant (92%)<br />
</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1080&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						<ul>
				<li>
					<div>
											<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=812&part_idx=20&referer=server"><img src="/server/img/sub/15120911751.png" alt=""/></a>
										</div>
					<dl>
						<dt>HPE DL120 Gen9</dt>
							<dd>1U/1소켓 폼팩터로 설계된 엔터프라이즈급 서버</dd>
					</dl>
				</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>DL120 Gen9 E5-2603v4 LFF Ety Svr<br />
</dt>
							<dd>E5-2603v4 LFF Ety Svr<br />
1 x 8GB DDR4-2400T-R<br />
Dynamic Smart Array B140i<br />
LFF HP 4 Bay<br />
361i Adapter (2x1GbE)<br />
1 x 550W NHP (89%)<br />
</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=812&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
					</dl>
									</li>
				<li>
										<p class="new"></p>
					<dl class="estBox">
														<dt>DL120 Gen9 E5-2630v4 SFF Ety Svr<br />
</dt>
							<dd>E5-2630v4 SFF Ety Svr<br />
1 x 8GB DDR4-2400T-R<br />
Smart HBA H240<br />
SFF HP 8 Bay<br />
361i Adapter (2x1GbE)<br />
1 x 550W NHP (89%)<br />
</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=812&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						<ul>
				<li>
					<div>
											<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=271&part_idx=20&referer=server"><img src="/server/img/sub/15120914601.png" alt=""/></a>
										</div>
					<dl>
						<dt>HPE DL380 Gen9</dt>
							<dd>데이터 센터의 표준, 전세계 판매 1위에 빛나는 최고의 x86 서버</dd>
					</dl>
				</li>
				<li>
										<p class="new"><span>추천제품</span></p>
					<dl class="estBox">
														<dt>HP DL380 GEN9 E5-2609v4</dt>
							<dd>E5-2609v4<br />
1 x 8GB DDR4-2400T-R<br />
Smart Array P440ar <br />
SFF HP 8 Bay<br />
331i Adapter (4x1GbE)<br />
1 x 500W Flex Slot (94%+)<br />
<br />
</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=271&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
					</dl>
									</li>
				<li>
										<p class="new"><span>추천제품</span></p>
					<dl class="estBox">
														<dt>HP DL380 GEN9 E5-2630v4</dt>
							<dd>E5-2630v4<br />
1 x 16GB DDR4-2400T-R<br />
Dynamic Smart Array B140i<br />
SFF HP 8 Bay<br />
331i Adapter (4x1GbE)<br />
1 x 500W Flex Slot (94%+)<br />
</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=271&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						<ul>
				<li>
					<div>
											<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=84&part_idx=20&referer=server"><img src="/server/img/sub/14660587781.JPG" alt=""/></a>
										</div>
					<dl>
						<dt>HPE  DL320e Gen8</dt>
							<dd>3.1GHz, 4C, 4G MEM, 300W PSU</dd>
					</dl>
				</li>
				<li>
										<p class="new"><span>추천제품</span></p>
					<dl class="estBox">
														<dt>HP DL320e Gen8 v2 E3-1220v3 Base Svr</dt>
							<dd>HP 4GB 2Rx8 PC3-12800E-11 Kit *1<br />
HP 500GB 6G SATA 7.2k 3.5in SC MDL HDD *2<br />
HP 500GB 6G SATA 7.2k 3.5in SC MDL HDD *2<br />
HP 9.5mm SATA DVD ROM Jb Kit<br />
N/A<br />
N/A</dd>
														<dd class="price">￦886,000<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=84&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
					</dl>
									</li>
				<li>
										<p class="new"><span>추천제품</span></p>
					<dl class="estBox">
														<dt>HP DL320e Gen8 v2 E3-1220v3 Base Svr</dt>
							<dd>HP 4GB 2Rx8 PC3-12800E-11 Kit *3<br />
HP 9.5mm SATA DVD ROM Jb Kit<br />
HP 9.5mm SATA DVD ROM Jb Kit<br />
N/A</dd>
														<dd class="price">￦672,000<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=84&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						<ul>
				<li>
					<div>
											<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=221&part_idx=20&referer=server"><img src="/server/img/sub/14688902251.jpg" alt=""/></a>
										</div>
					<dl>
						<dt>HPE ProLiant DL360 Gen9</dt>
							<dd>1U 폼 팩터 최고의 성능과 확장성의 고집적 고성능 서버</dd>
					</dl>
				</li>
				<li>
										<p class="new"><span>추천제품</span></p>
					<dl class="estBox">
														<dt>HP DL360 Gen9 E5-2603v4</dt>
							<dd>E5-2603v4 1P 8G 8SFF Svr<br />
1 x 8GB DDR4-2400T-R<br />
Smart HBA H240ar<br />
SFF HP 8 Bay<br />
331i Adapter (4x1GbE)<br />
1 x 500W Flex Slot (94%+)<br />
</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.asp?idx=221&part_idx=20&recom=1&referer=server" target="_blank">사양변경</a></dd>
					</dl>
									</li>
				<li>
										<p class="new"><span>추천제품</span></p>
					<dl class="estBox">
														<dt>HP DL360 Gen9 E5-2660v4</dt>
							<dd>E5-2660v4 PERF2 WW Svr<br />
4 x 16GB DDR4-2400T-R<br />
Smart Array P440ar <br />
SFF HP 8 Bay<br />
331i Adapter (4x1GbE)<br />
1 x 500W Flex Slot (94%+)<br />
</dd>
														<dd class="price">￦0<span>(부가세 포함)</span></dd>
							<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.asp?idx=221&part_idx=20&recom=2&referer=server" target="_blank">맞춤견적</a></dd>
					</dl>
									</li>
			</ul>
						</div>
					</div>
				</article>
				<!-- container end -->
				<script type="text/javascript">
					// 온라인 견적 scroll
					var tab_pos = $(".tab_s1").offset().top;
					var headerWrap = $(".headerWrap");

					tab_top(headerWrap.height());
					$(window).resize(function(){
						tab_top(headerWrap.height());
					});

					$(window).scroll(function(){
						var scr_pos = $(this).scrollTop();
						if ( scr_pos >= tab_pos -headerWrap.height()){
							$(".tab_s1").addClass("fixed");
						}else{
							$(".tab_s1").removeClass("fixed");
						};
					});

					function tab_top(num){
						$(".tab_s1").css({
							top:num
						});
					};
				</script>
			</article>
			<nav id="snb" class="menu5">
				<h2>온라인견적</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>