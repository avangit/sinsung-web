<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">온라인견적</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb5">
					<a href="#;">프로모션</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>프로모션</h2>
				<!-- container -->
				<article class="">
					<script>
					<!--
					function search_chk(){
						var frm	=	document.search_form;
					
						if(frm.s_o.value==''){
							alert("검색어를 입력하여 주세요!");
							frm.s_o.focus();
						}else{
							frm.submit();
						}
					
					}
					
					function search_chk2(){
						var frm	=	document.sch_Form;
					
						frm.submit();
					
					}
					
					//-->
					</script>
					<div class="article">
						<ul>
							<li>
								<table class="article_tbl">
									<caption class="blind">이벤트</caption>
										<colgroup>
											<col width="60px">
											<col width="202px">
											<col width="*">
										</colgroup>
									<tr>
										<td class="taC">5</td>
										<td class="article_img">
											<a href="promotion_view.asp"><img src="/server/img/sub/list_1587606241.jpg" border="0"></a>
										</td>
										<td class="article_con">
											<p class="tit"><a href="promotion_view.asp">언택트 시대의 필수품, '재택근무 최적의 화상회의 솔루션' Best 4를 소개합..</a> </p>
											<p class="date">등록일 : 2020/04/23 <span class="pdL30">조회수 : 276</span></p>
											<p class="con"><a href="promotion_view.asp">..</a></p>
										</td>
									</tr>
								</table>
							</li>
							<li>
								<table class="article_tbl">
									<caption class="blind">이벤트</caption>
									<colgroup>
										<col width="60px">
										<col width="202px">
										<col width="*">
									</colgroup>
									<tr>
										<td class="taC">4</td>
										<td class="article_img">
											<a href="promotion_view.html"><img src="/server/img/sub/1554776989_list.png" border="0"></a>
										</td>
										<td class="article_con">
											<p class="tit"><a href="promotion_view.html">삼성 정품토너 최대 50% 할인 이벤트</a> </p>
											<p class="date">등록일 : 2019/04/09 <span class="pdL30">조회수 : 251</span></p>
											<p class="con"><a href="promotion_view.html">&nbsp;</a></p>
										</td>
									</tr>
								</table>
							</li>
							<li>
								<table class="article_tbl">
									<caption class="blind">이벤트</caption>
									<colgroup>
										<col width="60px">
										<col width="202px">
										<col width="*">
									</colgroup>
									<tr>
										<td class="taC">3</td>
										<td class="article_img">
											<a href="promotion_view.html"><img src="/server/img/sub/list_1532430884.jpg" border="0"></a>
										</td>
										<td class="article_con">
											<p class="tit"><a href="promotion_view.html">중소기업을 위한 HPE 서버 + 백업솔루션 완벽 패키지</a> </p>
											<p class="date">등록일 : 2018/07/24 <span class="pdL30">조회수 : 589</span></p>
											<p class="con"><a href="promotion_view.html">무제 문서</a></p>
										</td>
									</tr>
								</table>
							</li>
							<li>
								<table class="article_tbl">
									<caption class="blind">이벤트</caption>
									<colgroup>
										<col width="60px">
										<col width="202px">
										<col width="*">
									</colgroup>
									<tr>
										<td class="taC">2</td>
										<td class="article_img">
											<a href="promotion_view.html"><img src="/server/img/sub/1518046789_list.jpg" border="0"></a>
										</td>
										<td class="article_con">
											<p class="tit"><a href="promotion_view.html">[프로모션] HP ProBook 450 G4 특가 프로모션</a> </p>
											<p class="date">등록일 : 2018/02/08 <span class="pdL30">조회수 : 565</span></p>
											<p class="con"><a href="promotion_view.html">무제 문서</a></p>
										</td>
									</tr>
								</table>
							</li>
							<li>
								<table class="article_tbl">
									<caption class="blind">이벤트</caption>
									<colgroup>
										<col width="60px">
										<col width="202px">
										<col width="*">
									</colgroup>
									<tr>
										<td class="taC">1</td>
										<td class="article_img">
											<a href="promotion_view.html"><img src="/server/img/sub/1508298600_list.jpg" border="0"></a>
										</td>
										<td class="article_con">
											<p class="tit"><a href="promotion_view.asp">[신성씨앤에스] HP 워크스테이션 특가 프로모션 제안</a> </p>
											<p class="date">등록일 : 2017/10/18 <span class="pdL30">조회수 : 618</span></p>
											<p class="con"><a href="promotion_view.html">무제 문서</a></p>
										</td>
									</tr>
								</table>
							</li>
						</ul>
					</div>
					<!--버튼-->
					<div class="ntb-listbtn-area mgT10"></div>
					<!--검색폼-->
					<div class="ntb-search-area">
						<form name="search_form" action="/server/sub/b2b/promotion.asp" method="post">
							<input type="hidden" name="code" value="event">
							<input type="hidden" name="cate_idx" value="">
							<select name="s_i" class="AXSelect vmiddle">
								<option value="subject" >제목</option>
								<option value="name" >작성자</option>
								<option value="content" >내용</option>
							</select>
							<input type="text" name="s_o" value="" class="AXInput vmiddle" />
							<input type="submit" value="검색" class="AXButton">
						</form>
					</div>
				

					<div class="page">
						<ul class="clfix">
							<li><a href='/server/sub/b2b/promotion.asp?startPage=0&code=event'>&lt;&lt;</a></li>  <li class='on'><a href='#'>1</a></li> <li><a href='/server/sub/b2b/promotion.asp?startPage=0&code=event'>&gt;&gt;</a></li>
						</ul>
					</div>
				</article>
			<!-- container end -->
			</article>
			<nav id="snb" class="menu5">
				<h2>온라인견적</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>