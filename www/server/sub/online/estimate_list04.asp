<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">온라인견적</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb5">
					<a href="#;">맞춤사양견적</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>
					<ul class="tabs tab_s1">
						<li id="tab11"><a href="estimate_list.asp">랙서버</a></li>
						<li id="tab12"><a href="estimate_list02.asp">타워서버</a></li>
						<li id="tab13"><a href="estimate_list03.asp">블레이드서버</a></li>
						<li id="tab13" class="current"><a href="estimate_list04.asp">스토리지</a></li>
						<li id="tab13" ><a href="estimate_list05.asp">워크스테이션</a></li>
					</ul>
				</h2>
				<!-- container -->
				<article class="estCon" id="tabsholder1">
					<div class="contents">
						<div id="content1" class="tabscontent tabCon_est">
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1635&amp;part_idx=41&amp;referer=server"><img src="/server/img/sub/15608189271.png" alt=""></a>
														</div>
									<dl>
										<dt>HPE SimpliVity 380 Gen10 Node</dt>
											<dd>HPE SimpliVity 380</dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HPE SimpliVity 380 Gen10 Node</dt>
											<dd>DL380 Gen10 CPU<br>
				144G 12 DIMM FIO Kit<br>
				380 for 6000 Series Small Storage<br>
				Smart Array P408i-a SR Gen10<br>
				FlexFabric 10Gb 2-port 534FLR-SFP+ Adapter<br>
				1600W</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1635&amp;part_idx=41&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HPE SimpliVity 380 Gen10 Node</dt>
											<dd>DL380 Gen10 CPU<br>
				192G 12 DIMM FIO Kit<br>
				380 for 6000 Series Medium Storage<br>
				Smart Array P408i-a SR Gen10<br>
				FlexFabric 10Gb 2-port 534FLR-SFP+ Adapter<br>
				1600W</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1635&amp;part_idx=41&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1634&amp;part_idx=41&amp;referer=server"><img src="/server/img/sub/15608195161.png" alt=""></a>
														</div>
									<dl>
										<dt>HPE MSA 2050 SAN Dual Controller</dt>
											<dd>HPE MSA 2050 SAN 스토리지</dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HPE MSA 2050 SAN Dual Controller SFF</dt>
											<dd>HPE MSA 1.2TB 12G SAS 10K SFF(2.5in)<br>
				16Gb Short Wave Fibre Channel SFP+ 4-pack Transceiver</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1634&amp;part_idx=41&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HPE MSA 2050 SAN Dual Controller SFF</dt>
											<dd>HPE MSA 1.2TB 12G SAS 10K SFF(2.5in)<br>
				16Gb Short Wave Fibre Channel SFP+ 4-pack Transceiver</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1634&amp;part_idx=41&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
									</div>
					</div>
				</article>
				<!-- container end -->
				<script type="text/javascript">
					// 온라인 견적 scroll
					var tab_pos = $(".tab_s1").offset().top;
					var headerWrap = $(".headerWrap");
					
					tab_top(headerWrap.height());
					$(window).resize(function(){
						tab_top(headerWrap.height());
					});

					$(window).scroll(function(){
						var scr_pos = $(this).scrollTop();
						if ( scr_pos >= tab_pos -headerWrap.height()){
							$(".tab_s1").addClass("fixed");
						}else{
							$(".tab_s1").removeClass("fixed");
						};
					});

					function tab_top(num){
						$(".tab_s1").css({
							top:num
						});	
					};
				</script>
			</article>
			<nav id="snb" class="menu5">
				<h2>온라인견적</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>