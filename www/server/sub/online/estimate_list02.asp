<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">온라인견적</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb5">
					<a href="#;">맞춤사양견적</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>
					<ul class="tabs tab_s1">
						<li id="tab11"><a href="estimate_list.asp">랙서버</a></li>
						<li id="tab12" class="current"><a href="estimate_list02.asp">타워서버</a></li>
						<li id="tab13" ><a href="estimate_list03.asp">블레이드서버</a></li>
						<li id="tab13" ><a href="estimate_list04.asp">스토리지</a></li>
						<li id="tab13" ><a href="estimate_list05.asp">워크스테이션</a></li>
					</ul>
				</h2>
				<!-- container -->
				<article class="estCon" id="tabsholder1">
					<div class="contents">
						<div id="content1" class="tabscontent tabCon_est">
										<ul>
								<li>
									<div>
															<a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.php?idx=1631&amp;part_idx=20&amp;referer=server"><img src="/server/img/sub/15608217811.png" alt=""></a>
														</div>
									<dl>
										<dt>ML350 Gen10</dt>
											<dd>탁월한 기능 수행 능력</dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>ML30 Gen10 4LFF<br>
				</dt>
											<dd>Intel Xeon-Bronze 3104 (1.7GHz/6-core/85W)<br>
				8GB (1x8GB) Single Rank x8<br>
				1TB SATA 6G Midline 7.2K LFF (3.5in) LP<br>
				500W</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.php?idx=1631&amp;part_idx=20&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>ML30 Gen10 4LFF</dt>
											<dd>Intel Xeon-Bronze 3106 (1.7GHz/8-core/85W)<br>
				16GB (1x16GB) Single Rank x4<br>
				1TB SATA 6G Midline 7.2K SFF (2.5in) SC<br>
				800W Flex</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.php?idx=1631&amp;part_idx=20&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.php?idx=1630&amp;part_idx=20&amp;referer=server"><img src="/server/img/sub/15556549391.png" alt=""></a>
														</div>
									<dl>
										<dt>ML30 Gen10</dt>
											<dd>일일 작업 워크로드를 위한 강력하고 경제적인 서버</dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HPE ML30 Gen10 Intel Xeon E-2124</dt>
											<dd>HPE ML30 Gen10 Intel Xeon E-2124<br>
				HPE 8GB (1x8GB) Single Rank x8<br>
				HPE 1TB SATA 6G Midline<br>
				HPE 500W<br>
				HPE Smart Array P408i-a SR Gen10<br>
				<br>
				<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.php?idx=1630&amp;part_idx=20&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HPE ML30 Gen10 Intel Xeon E-2134</dt>
											<dd>HPE ML30 Gen10 Intel Xeon E-2134<br>
				HPE 16GB (1x16GB) Dual Rank x8<br>
				HPE 4TB SATA 6G Midline<br>
				HPE ML30 Gen9 350W<br>
				HPE Smart Array E208i-a SR Gen10</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1630&amp;part_idx=20&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1082&amp;part_idx=20&amp;referer=server"><img src="/server/img/sub/15121085201.png" alt=""></a>
														</div>
									<dl>
										<dt>HPE ML30 Gen9</dt>
											<dd>가용성, 확장성 및 서비스 용이성 - 최적의 조합</dd>
									</dl>
								</li>
								<li>
														<p class="new"></p>
									<dl class="estBox">
																		<dt>HPE ProLiant ML30 Gen9 E3-1220v5<br>
				</dt>
											<dd>E3-1220v5 Base AP Svr <br>
				1 x 4GB DDR4- 2133U<br>
				Dynamic Smart Array B140i<br>
				LFF HP 4 Bay<br>
				Broadcom 5720(2x1GbE)<br>
				1 x 350W NHP ATX<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1082&amp;part_idx=20&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"></p>
									<dl class="estBox">
																		<dt>HPE ProLiant ML30 Gen9 E3-1220v5 </dt>
											<dd>E3-1220v5 AP LFF Svr/Promo<br>
				1 x 8GB DDR4-2400T-R<br>
				Dynamic Smart Array B140i<br>
				LFF HP 4 Bay<br>
				Broadcom 5720(2x1GbE)<br>
				1 x 350W NHP ATX<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1082&amp;part_idx=20&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1081&amp;part_idx=20&amp;referer=server"><img src="/server/img/sub/15121063301.png" alt=""></a>
														</div>
									<dl>
										<dt>HPE ML350 Gen9</dt>
											<dd>가용성, 확장성 및 서비스 용이성 - 최적의 조합</dd>
									</dl>
								</li>
								<li>
														<p class="new"></p>
									<dl class="estBox">
																		<dt>HPE ProLiant ML350 Gen9 E5-2609v4<br>
				</dt>
											<dd>E5-2609v4 8GB LFF Svr<br>
				1 x 8GB DDR4-2400T-R<br>
				Smart HBA H240ar<br>
				LFF HP 8 Bay<br>
				331i Adapter (4x1GbE)<br>
				1 x 500W Flex Slot (94%+)<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1081&amp;part_idx=20&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"></p>
									<dl class="estBox">
																		<dt>HPE ProLiant DL180 Gen9 E5-2620v4</dt>
											<dd>E5-2620v4 16GB SFF AP Svr<br>
				1 x 16GB DDR4-2400T-R<br>
				Smart Array P440ar <br>
				SFF HP 8 Bay<br>
				331i Adapter (4x1GbE)<br>
				1 x 500W Flex Slot (94%+)<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1081&amp;part_idx=20&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=811&amp;part_idx=20&amp;referer=server"><img src="/server/img/sub/15120950621.png" alt=""></a>
														</div>
									<dl>
										<dt>HPE ML110 Gen9</dt>
											<dd>유연한 디자인으로 비즈니스 요구에 따라 확장 가능</dd>
									</dl>
								</li>
								<li>
														<p class="new"></p>
									<dl class="estBox">
																		<dt>HPE ProLiant ML110 Gen9 E5-2603v4<br>
				</dt>
											<dd>E5-2603v4 LFF Ety Svr<br>
				1 x 8GB DDR4-2400T-R<br>
				Dynamic Smart Array B140i<br>
				LFF HP 4 Bay<br>
				Broadcom 5717 (2x1GbE)<br>
				1 x 350W NHP ATX<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=811&amp;part_idx=20&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"></p>
									<dl class="estBox">
																		<dt>HPE ProLiant ML110 Gen9 E5-2620v4 </dt>
											<dd>E5-2620v4 SFF AP Svr/Prom<br>
				1 x 8GB DDR4-2400T-R<br>
				Dynamic Smart Array B140i<br>
				LFF HP 8 Bay<br>
				Broadcom 5717 (2x1GbE)<br>
				1 x 350W NHP ATX<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=811&amp;part_idx=20&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
									</div>
					</div>
				</article>
				<!-- container end -->
				<script type="text/javascript">
					// 온라인 견적 scroll
					var tab_pos = $(".tab_s1").offset().top;
					var headerWrap = $(".headerWrap");
					
					tab_top(headerWrap.height());
					$(window).resize(function(){
						tab_top(headerWrap.height());
					});

					$(window).scroll(function(){
						var scr_pos = $(this).scrollTop();
						if ( scr_pos >= tab_pos -headerWrap.height()){
							$(".tab_s1").addClass("fixed");
						}else{
							$(".tab_s1").removeClass("fixed");
						};
					});

					function tab_top(num){
						$(".tab_s1").css({
							top:num
						});	
					};
				</script>
			</article>
			<nav id="snb" class="menu5">
				<h2>온라인견적</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>