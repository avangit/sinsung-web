<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server//server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">온라인견적</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb5">
					<a href="#;">맞춤사양견적</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>
					<ul class="tabs tab_s1">
						<li id="tab11"><a href="estimate_list.asp">랙서버</a></li>
						<li id="tab12"><a href="estimate_list02.asp">타워서버</a></li>
						<li id="tab13"><a href="estimate_list03.asp">블레이드서버</a></li>
						<li id="tab13"><a href="estimate_list04.asp">스토리지</a></li>
						<li id="tab13" class="current"><a href="estimate_list05.asp">워크스테이션</a></li>
					</ul>
				</h2>
				<!-- container -->
				<article class="estCon" id="tabsholder1">
					<div class="contents">
						<div id="content1" class="tabscontent tabCon_est">
										<ul>
								<li>
									<div>
															<a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.php?idx=1807&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15680978661.png" alt=""></a>
														</div>
									<dl>
										<dt>HP ZBOOK 15 G6 HIE </dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBOOK 15 G6</dt>
											<dd>Intel® Core™ i9-9880H 2.3GHz (Up to 4.8GHz, 16MB, 8C)<br>
				Windows 10 Pro 64<br>
				512GB SSD NVMe<br>
				N/A<br>
				N/A<br>
				16GB DDR4<br>
				NVIDIA® Quadro® RTX3000 6GB</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.asp?idx=1807&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBOOK 15 G6</dt>
											<dd>Intel® Core™ i5-9400H 2.5GHz (up to 4.3GHz, 8MB, 4C)<br>
				Windows 10 Pro 64<br>
				256GB M.2 NVMe<br>
				N/A<br>
				N/A<br>
				16GB DDR4<br>
				NVIDIA® Quadro® T1000 4GB6GB</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.asp?idx=1807&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.asp?idx=1806&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15680193711.png" alt=""></a>
														</div>
									<dl>
										<dt>HP ZBOOK 15 G6</dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBOOK 15 G6</dt>
											<dd>Intel® Core™ i7-9850H 2.6GHz(up to 4.6GHz, 12MB, 6C)<br>
				Windows 10 Pro 64<br>
				256GB M.2 NVMe + 1TB<br>
				N/A<br>
				16GB DDR4<br>
				NVIDIA® Quadro® T2000 4GB</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.asp?idx=1806&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBOOK 15 G6</dt>
											<dd>Intel® Core™ i7-9850H 2.6GHz(up to 4.6GHz, 12MB, 6C)<br>
				Windows 10 Pro 64<br>
				512GB M.2 NVMe<br>
				N/A<br>
				16GB DDR4<br>
				NVIDIA® Quadro® RTX3000 6GB<br>
				3Y</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1806&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1711&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15591182771.png" alt=""></a>
														</div>
									<dl>
										<dt>HP ZBook 17 G5 Mobile Workstation</dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBook 17 G5 Mobile Workstation</dt>
											<dd>HP ZBook 17 G5 Mobile Workstation<br>
				Intel® Xeon E-2176M<br>
				Windows 10 Pro WKST Plus<br>
				256GB SSD NVMe<br>
				1TB HDD<br>
				N/A<br>
				16GBDDR4-2666 <br>
				NVIDIA Quadro P4200 8GB</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1711&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBook 17 G5 Mobile Workstation</dt>
											<dd>HP ZBook 17 G5 Mobile Workstation<br>
				Intel® Xeon I7-8850H<br>
				Windows 10 Pro WKST Plus<br>
				256GB SSD NVMe<br>
				1TB HDD<br>
				N/A<br>
				16GBDDR4-2666 <br>
				NVIDIA Quadro P4200 8GB</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1711&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1710&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15591176031.png" alt=""></a>
														</div>
									<dl>
										<dt>HP ZBook 15 G5 Mobile Workstation</dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBook 15 G5 Mobile Workstation</dt>
											<dd>HP ZBook 15 G5 Mobile Workstation<br>
				Intel® Core™ i7-8750H 2.2GHz<br>
				Windows 10 Pro (64bit)<br>
				256GB SSD NVMe + 1TB HDD<br>
				N/A<br>
				8GB DDR4-2666 <br>
				NVIDIA® Quadro® P1000 4G</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1710&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBook 15 G5 Mobile Workstation</dt>
											<dd>HP ZBook 15 G5 Mobile Workstation<br>
				Intel® Core™ i7-8850H vPro™<br>
				Windows 10 Pro (64bit)<br>
				256GB SSD NVMe + 1TB HDD<br>
				N/A<br>
				16GB  DDR4-2666 <br>
				NVIDIA® Quadro® P2000 4G</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1710&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1709&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15591151811.png" alt=""></a>
														</div>
									<dl>
										<dt>HP ZBook 15v G5 Mobile Workstation</dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBook 15v G5 Mobile Workstation</dt>
											<dd>Intel® Core™ i7-9750H 2.6GHz(up to 4.5GHz, 12MB, 6C) <br>
				Windows 10 Pro 64<br>
				256GB SSD NVMe + 1TB HDD <br>
				N/A<br>
				16GB DDR4-2666 <br>
				NVIDIA® Quadro® P600 4G</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1709&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP ZBook 15v G5 Mobile Workstation</dt>
											<dd>Intel® Core™ i7-8750H 2.2GHz<br>
				Windows 10 Pro (64bit)<br>
				256GB SSD NVMe + 1TB HDD<br>
				N/A<br>
				16GB  DDR4-2666<br>
				NVIDIA® Quadro® P600 4G</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1709&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1708&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15591140731.png" alt=""></a>
														</div>
									<dl>
										<dt>HP Z2 Mini G4 Workstation</dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP Z2 Mini G4 Workstation</dt>
											<dd>Intel® Xeon® E-2104G 3.20GHz<br>
				Linux-ready<br>
				256G Z Turbo Drive<br>
				1TB 2.5" HDD<br>
				N/A<br>
				8GB (8Gx1) DDR4-2666 NECC SODIMM<br>
				NVIDIA® Quadro® P1000 4GB Graphics</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1708&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP Z2 Mini G4 Workstation</dt>
											<dd>Intel® Xeon® E-2104G 3.20GHz<br>
				Windows10 Professional 64bit<br>
				512G Z Turbo Drive<br>
				N/A<br>
				N/A<br>
				16GB DDR4-2666 NECC SODIMM<br>
				NVIDIA® Quadro® P1000 4GB Graphics</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1708&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1707&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15591119931.png" alt=""></a>
														</div>
									<dl>
										<dt>HP Z2 Tower G4 Workstation</dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP Z2 Tower G4 Workstation</dt>
											<dd>Intel® Xeon® E-2104G 3.20GHz<br>
				Windows 10 Pro for Workstations<br>
				Z Turbo 256GB SSD<br>
				Slim DVDWR <br>
				8GB (8Gx1) DDR4-2666 NECC<br>
				Intel® UHD Graphics P630</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1707&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP Z2 Tower G4 Workstation</dt>
											<dd>Intel® Xeon® E-2144G 3.6GHz<br>
				Windows 10 Pro for Workstations<br>
				Z Turbo 256GB SSD<br>
				1TB HDD (7200rpm) + 2TB HDD<br>
				Slim DVDWR<br>
				16GB (16Gx1) DDR4-2666 ECC<br>
				NVIDIA® Quadro® P1000 4GB Graphics<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1707&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1476&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15474511731.png" alt=""></a>
														</div>
									<dl>
										<dt>HP Z2 G4 Workstation</dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"></p>
									<dl class="estBox">
																		<dt>HP Z2 G4 Workstation </dt>
											<dd>HP Z2 G4 Workstation<br>
				Intel® Xeon® E-2104G<br>
				Windows10 Pro for Workstations<br>
				Z Turbo 256GB SSD<br>
				Slim DVDWR<br>
				8GB DDR4-2666<br>
				Intel® UHD Graphics P630</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1476&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"></p>
									<dl class="estBox">
																		<dt>HP Z2 G4 Workstation </dt>
											<dd>HP Z2 G4 Workstation<br>
				Intel® Xeon® E-2144G<br>
				Windows10 Pro for Workstations<br>
				Z Turbo 256GB SSD<br>
				1TB HDD + TB HDD<br>
				Slim DVDWR<br>
				16GB DDR4-2666<br>
				NVIDIA® Quadro® P1000 4 GB</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1476&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1096&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15125215341.png" alt=""></a>
														</div>
									<dl>
										<dt>HP Zbook x2 G4 Mobile Workstation</dt>
											<dd>언제 어디서나 즐기는 성능</dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP Zbook x2 G4 Mobile Workstation<br>
				</dt>
											<dd>HP Zbook x2 G4 Mobile Workstation<br>
				인텔® 코어™ i7-8550 프로세서<br>
				Win 10 Pro 64 Downgrade Win 7 64<br>
				256GB PCIe® NVMe™<br>
				9.5mm Slim SuperMulti DVDRW 1st ODD<br>
				8GB DDR4-2133<br>
				NVIDIA Quadro M620</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1096&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP Zbook x2 G4 Mobile Workstation<br>
				</dt>
											<dd>HP Zbook x2 G4 Mobile Workstation<br>
				인텔® 코어™ i7-8250 프로세서<br>
				Win 10 Pro 64 Downgrade Win 7 64<br>
				512GB PCIe® NVMe™<br>
				9.5mm Slim SuperMulti DVDRW 1st ODD<br>
				16GB DDR4-2133<br>
				NVIDIA Quadro M620</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1096&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1089&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15475150021.png" alt=""></a>
														</div>
									<dl>
										<dt>HP Z6 G4 WorkStation</dt>
											<dd>HP New Z Workstation</dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>신제품</span></p>
									<dl class="estBox">
																		<dt>HP Z6 G4 WorkStation</dt>
											<dd>HP Z6 G4 WorkStation Series<br>
				인텔® 제온® 프로세서 4114<br>
				Win10 Pro 64 Downgrade Win7 Pro 64<br>
				1TB 7200 SATA HDD<br>
				Z Turbo Drive M.2 256 / 512GB<br>
				9.5mm 슬림 광 디스크 드라이브 베이<br>
				32GB DDR4-2666<br>
				P2000 / P2000 2</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1089&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>신제품</span></p>
									<dl class="estBox">
																		<dt>HP Z6 G4 WorkStation</dt>
											<dd>HP Z6 G4 WorkStation Series<br>
				인텔® 제온® 프로세서 4116<br>
				Win10 Pro 64 Downgrade Win7 Pro 64<br>
				1TB 7200 SATA HDD<br>
				Z Turbo Drive M.2 256 / 512GB<br>
				9.5mm 슬림 광 디스크 드라이브 베이<br>
				32GB DDR4-2666<br>
				P2000 / P2000 2</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1089&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1088&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15475148121.png" alt=""></a>
														</div>
									<dl>
										<dt>HP Z8 G4 WorkStation</dt>
											<dd>HP New Z Workstation</dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>신제품</span></p>
									<dl class="estBox">
																		<dt>HP Z8 G4 WorkStation</dt>
											<dd>HP Z6 G4 WorkStation<br>
				인텔® 제온® 프로세서 5118<br>
				Win10 Pro 64 Downgrade Win7 Pro 64<br>
				Z Turbo Drive M.2 512GB TLC SSD<br>
				9.5mm 슬림 광 디스크 드라이브 베이<br>
				64GB DDR4-2666<br>
				P4000 / P4000 2</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1088&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>신제품</span></p>
									<dl class="estBox">
																		<dt>HP Z8 G4 WorkStation</dt>
											<dd>HP Z6 G4 WorkStation<br>
				인텔® 제온® 프로세서 6130<br>
				Win10 Pro 64 Downgrade Win7 Pro 64<br>
				2TB 7200 SATA HDD<br>
				9.5mm 슬림 광 디스크 드라이브 베이<br>
				32GB DDR4-2666<br>
				P4000 / P4000 2 / P2000 3</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1088&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1087&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/15475118501.png" alt=""></a>
														</div>
									<dl>
										<dt>HP Z4 G4 WorkStation</dt>
											<dd>HP New Z Workstation</dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>신제품</span></p>
									<dl class="estBox">
																		<dt>HP Z4 G4 WorkStation</dt>
											<dd>HP Z4 G4 WorkStation<br>
				인텔® 제온® 프로세서 2125<br>
				Win10 Pro 64 Downgrade Win7 Pro 64<br>
				1TB 7200 SATA HDD<br>
				Z Turbo Drive M.2 512GB TLC SSD<br>
				9.5mm 슬림 광 디스크 드라이브 베이<br>
				64GB DDR4-2666<br>
				P4000<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1087&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>신제품</span></p>
									<dl class="estBox">
																		<dt>HP Z4 G4 WorkStation</dt>
											<dd>HP Z4 G4 WorkStation<br>
				인텔® 제온® 프로세서 2133<br>
				Win10 Pro 64 Downgrade Win7 Pro 64<br>
				1TB 7200 SATA HDD<br>
				Z Turbo Drive M.2 256GB TLC SSD<br>
				9.5mm 슬림 광 디스크 드라이브 베이<br>
				64GB DDR4-2666<br>
				P4000</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=1087&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
										<ul>
								<li>
									<div>
															<a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=81&amp;part_idx=19&amp;referer=server"><img src="/server/img/sub/14660578241.jpg" alt=""></a>
														</div>
									<dl>
										<dt>HP Z240 Workstation</dt>
											<dd></dd>
									</dl>
								</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP Z240 Workstain</dt>
											<dd>HP Z240 Workstain<br>
				인텔® 제온® 프로세서 E3-1225 v5<br>
				Win10 Pro 64 Downgrade Win7 Pro 64<br>
				1TB 7200 RPM SATA 1 st Hard Drive<br>
				Z Turbo Drive M.2 256 / 512<br>
				9.5mm Slim superMulti DVDRW 1st ODD<br>
				8GB DDR4-2133 (1x8GB) Unbuffered<br>
				인텔® HD Graphics P530<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=81&amp;part_idx=19&amp;recom=1&amp;referer=server" target="_blank">사양변경</a></dd>
									</dl>
													</li>
								<li>
														<p class="new"><span>추천제품</span></p>
									<dl class="estBox">
																		<dt>HP Z240 Workstain</dt>
											<dd>HP Z240 Workstain<br>
				인텔® 제온® 프로세서 E3-1280 v5<br>
				Win10 Pro 64 Downgrade Win7 Pro 64<br>
				1TB 7200 RPM SATA 1 st Hard Drive<br>
				Z Turbo Drive M.2 256 / 512<br>
				9.5mm Slim superMulti DVDRW 1st ODD<br>
				8GB DDR4-2133 (1x8GB) Unbuffered<br>
				인텔® HD Graphics P530<br>
				</dd>
																		<dd class="price">￦0<span>(부가세 포함)</span></dd>
											<dd class="btn"><a href="http://www.sinsungcns.com/sub/b2b/estimate_form.php?idx=81&amp;part_idx=19&amp;recom=2&amp;referer=server" target="_blank">맞춤견적</a></dd>
									</dl>
													</li>
							</ul>
									</div>
					</div>
				</article>
				<!-- container end -->
				<script type="text/javascript">
					// 온라인 견적 scroll
					var tab_pos = $(".tab_s1").offset().top;
					var headerWrap = $(".headerWrap");

					tab_top(headerWrap.height());
					$(window).resize(function(){
						tab_top(headerWrap.height());
					});

					$(window).scroll(function(){
						var scr_pos = $(this).scrollTop();
						if ( scr_pos >= tab_pos -headerWrap.height()){
							$(".tab_s1").addClass("fixed");
						}else{
							$(".tab_s1").removeClass("fixed");
						};
					});

					function tab_top(num){
						$(".tab_s1").css({
							top:num
						});
					};
				</script>
			</article>
			<nav id="snb" class="menu5">
				<h2>온라인견적</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>