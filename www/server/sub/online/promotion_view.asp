<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">온라인견적</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb5">
					<a href="#;">프로모션</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>프로모션</h2>
				<!-- container -->
				<article class="">
					<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
						<caption>게시판 내용</caption>
						<colgroup>
							<col width="12%" />
							<col width="" />
							<col width="12%" />
							<col width="15%" />
						</colgroup>
						<thead>
							<tr>
								<th colspan="4" class="r_none"> 언택트 시대의 필수품, '재택근무 최적의 화상회의 솔루션' Best 4를 소개합니다!</th>
							</tr>
						</thead>
						<tbody>
						<tr>
							<th>등록일</th>
							<td class="left">2020-04-23</td>
							<th>조회수</th>
							<td>278</td>
						</tr>
						<!--
						<tr>
							<th>E-mail</th>
							<td class="left">psh@sinsungcns.com&nbsp;</td>
							<th>작성자</th>
							<td><span class='bold'>신성씨앤에스</span></td>
						</tr>
						-->
							<tr>
							<th>이벤트 기간</th>
							<td class="left" colspan="3">2020-04-23 ~ 2020-07-31</td>
						</tr>

						<tr>
							<td colspan="4" class="init">


								<div class="content-area">
					<style type="text/css">img { width:auto; max-width:100%;}
						#content{ position: relative; max-width: 900px; margin: 0 auto; width: 100%;}
						section a{ position: relative; display: block; overflow: hidden;}
						section a img{ transition: all 1s;}
						section a:hover img{ transform: scale(1.1,1.1);}
						.section6:after,
						.section7:after{ display: block; content:''; clear: both;}
						.section6 .box,
						.section7 .box{ float: left; width: 50%;}
						.section7{ background-color: #f2f2f2;}
						.section7 .box{ padding: 40px 0; text-align: center;}
						.section7 .box a{ position: relative; display: block; width:auto; margin: 0 auto;}
						.section7 .box a:hover img{transform: none;}
					</style>
					<div id="content">
					<section class="section1"><a href="http://www.sinsungcns.com/sub/conference/conference.php"><img alt="" src="http://www.sinsungcns.com/img2/top_img.jpg"> </a></section>

					<section class="section2"><a href="http://www.sinsungcns.com/sub/conference/conference.php" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img1.jpg"> </a></section>

					<section class="section2"><a href="http://www.sinsungcns.com/sub/conference/conference.php" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img2.jpg"> </a></section>

					<section class="section3"><a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1911&amp;part_idx=6&amp;s_o=800%20g5&amp;o=1" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img3.jpg"> </a></section>

					<section class="section4"><a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=2038&amp;part_idx=5&amp;s_o=470&amp;o=1" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img4.jpg"> </a></section>

					<section class="section5"><a href="http://www.sinsungcns.com/sub/support/drivers7.php" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img5.jpg"> </a></section>

					<section class="section6">
					<div class="box box1"><a href="https://pf.kakao.com/_kxipdT" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img6.jpg"> </a></div>

					<div class="box box2"><a href="http://www.sinsungcns.com/sub/experience/construction_case.php" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img7.jpg"> </a></div>
					</section>

					<section class="section7">
					<div class="box box1"><a href="http://www.sinsungcns.com/download/Video_conference_simple_proposal.pdf" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img8.jpg"> </a></div>

					<div class="box box2"><a href="http://www.sinsungcns.com/sub/b2b/estimate_list.php" target="_blank"><img alt="" src="http://www.sinsungcns.com/img2/img9.jpg"> </a></div>
					</section>

					<section class="section8"><img alt="" src="http://www.sinsungcns.com/img2/bottom_img.jpg"></section>
					</div>			</div>

							</td>
						</tr>

						<!-- 파일첨부 -->

						<!-- //파일첨부 -->

						</tbody>
					</table>

					<!--버튼-->
					<div class="ntb-tb-view-btn" style="width:100%">
						<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='/server/sub/online/promotion.asp?mode=l&code=event'">
						<div class="btnr">

										</div>
					</div>




					<div class="ntb-tb-view-reply">

						<!--이전글 시작-->
						<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
							<caption>게시판 이전/다음글</caption>
							<colgroup>
								<col width="12%" />
								<col width="" />
							</colgroup>
										<tr>
								<th width="12%">이전글</th>
								<td width="88%" class="left font_gray">
																					<a href="/server/sub/online/promotion.asp?mode=v&idx=202&code=event">
																삼성 정품토너 최대 50% 할인 이벤트</a>
																			</td>
							</tr>

							<!--다음글 시작-->
										<tr>
								<th width="12%">다음글</th>
								<td width="88%" class="left font_gray">
														다음글이 없습니다.
													</td>
							</tr>
						</table>
					</div>
					<!-- 코멘트 시작 -->
					<!-- 코멘트 종료-->
				</article>
				<!-- container end -->
			</article>
			<nav id="snb" class="menu5">
				<h2>온라인견적</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>