<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">기술지원</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb6">
					<a href="#;">IT 자료실</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>IT 자료실</h2>
				<!-- container -->
				<article class="case">
					<ul id="tabMenu_s2" class="mb30">
						<li rel="m1" onClick="location.href='it_data.asp'">제품설명서</li>
						<li rel="m2" class="active">기술지원메뉴얼</li>
					</ul>
				
					<script>
					<!--
					function search_chk2(){
						var frm	=	document.sch_Form;
						frm.submit();
					}
					//-->
					</script>
				
					<div class="sch it_sch clfix">
						<form name="sch_Form" method="POST" action="it_data2.asp">
						<input type="hidden" name="s_i" value="all">
				
						<select name=" p2" id="part2_code">
							<option value="">&nbsp;  ::: 제조사 :::</option>
											<option value="21" >&nbsp; HP</option>
										<option value="62" >&nbsp; 삼성</option>
										<option value="103" >&nbsp; 삼성 단납점 모델</option>
										<option value="23" >&nbsp; LG</option>
										<option value="98" >&nbsp; HPE</option>
										<option value="111" >&nbsp; NETGEAR</option>
										<option value="110" >&nbsp; ATEN</option>
									</select>
				
				
						<select name="p1" id="part1_code">
							<option value="">&nbsp;  ::: 카테고리 ::: </option>
										<option value="1462178498" >&nbsp; 노트북</option>
										<option value="1462178511" >&nbsp; 데스크탑</option>
										<option value="1462178524" >&nbsp; 모니터</option>
										<option value="1462178530" >&nbsp; 서버</option>
										<option value="1462178536" >&nbsp; 워크스테이션</option>
										<option value="1462178544" >&nbsp; 프린터/복합기</option>
										<option value="1462178556" >&nbsp; TV/디스플레이</option>
										<option value="1462178578" >&nbsp; 케어팩 서비스</option>
										<option value="1462178585" >&nbsp; 토너</option>
										<option value="1462178591" >&nbsp; 잉크</option>
										<option value="1475627941" >&nbsp; SSD</option>
										<option value="1476145200" >&nbsp; 전산소모품</option>
										<option value="1512361966" >&nbsp; 화상회의 장비</option>
										<option value="1532681941" >&nbsp; NAS 스토리지</option>
										<option value="1532681957" >&nbsp; 백업솔루션</option>
										<option value="1532682082" >&nbsp; 화상회의 장비</option>
										<option value="1574754886" >&nbsp; 네트워크</option>
									</select>
				
				
						<input type="text" name="s_o" placeholder="검색어를 입력하세요." >
						<a href="javascript:search_chk2()">검색</a>
						</form>
					</div>
				
					
				
				<script>
				<!--
				function search_chk(){
					var frm	=	document.search_form;
				
					if(frm.s_o.value==''){
						alert("검색어를 입력하여 주세요!");
						frm.s_o.focus();
					}else{
						frm.submit();
					}
				
				}
				
				function search_chk2(){
					var frm	=	document.sch_Form;
				
					frm.submit();
				
				}
				
				//-->
				</script>
							<table id="empTable">
								<caption class="blind">IT 자료실-기술지원메뉴얼 게시판</caption>
								<colgroup>
									<col width="10%">
														<col width="">
									<!--<col width="10%">-->
									<col width="10%">
									<col width="12%">
								</colgroup>
								<tr>
									<th>번호</th>
														<th>제목</th>
									<!--<th>작성자</th>-->
									<th>조회수</th>
									<th>작성일</th>
								</tr>
																<tr>
									<td>37</td>
														<td class="left">
																							<a href="it_data2_view.asp">
																그래픽드라이버 설치 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>0</td>
									<td>2020/07/17</td>
				
								</tr>
												<tr>
									<td>36</td>
														<td class="left">
																							<a href="it_data2_view.asp">
																윈도우 설치 USB 만들기 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>17</td>
									<td>2020/07/03</td>
				
								</tr>
												<tr>
									<td>35</td>
														<td class="left">
																							<a href="it_data2_view.asp">
																MS 윈도우 업데이트 도우미 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>6</td>
									<td>2020/06/30</td>
				
								</tr>
												<tr>
									<td>34</td>
														<td class="left">
																							<a href="it_data2_view.html">
																HPE SSP 적용 (ISO) 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>9</td>
									<td>2020/06/10</td>
				
								</tr>
												<tr>
									<td>33</td>
														<td class="left">
																							<a href="it_data2_view.html">
																HPE SSP 다운로드 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>6</td>
									<td>2020/06/10</td>
				
								</tr>
												<tr>
									<td>32</td>
														<td class="left">
																							<a href="it_data2_view.html">
																리눅스 서버 점검 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>19</td>
									<td>2020/06/05</td>
				
								</tr>
												<tr>
									<td>31</td>
														<td class="left">
																							<a href="it_data2_view.html">
																삼성PC 드라이버 설치 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>16</td>
									<td>2020/06/05</td>
				
								</tr>
												<tr>
									<td>30</td>
														<td class="left">
																							<a href="it_data2_view.html">
																절전모드 진입 시 화면 꺼짐 해결 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>44</td>
									<td>2020/04/09</td>
				
								</tr>
												<tr>
									<td>29</td>
														<td class="left">
																							<a href="it_data2_view.html">
																RHEL 풀패키지 업데이트 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>45</td>
									<td>2020/03/10</td>
				
								</tr>
												<tr>
									<td>28</td>
														<td class="left">
																							<a href="it_data2_view.html">
																잔류전원 제거 방법 매뉴얼</a>&nbsp;&nbsp;
															</td>
									<!--<td>신성씨앤에스</td>-->
									<td>37</td>
									<td>2020/03/10</td>
				
								</tr>
				</table>

				<!--버튼-->
				<div class="ntb-listbtn-area mgT10"></div>
				
					<!--검색폼-->
					<div class="ntb-search-area">
						<form name="search_form" action="it_data2.asp" method="post">
							<input type="hidden" name="code" value="it_data">
							<input type="hidden" name="cate_idx" value="">
							<select name="s_i" class="AXSelect vmiddle">
								<option value="subject" >제목</option>
								<option value="name" >작성자</option>
								<option value="content" >내용</option>
							</select>
							<input type="text" name="s_o" value="" class="AXInput vmiddle" />
							<input type="submit" value="검색" class="AXButton">
						</form>
					</div>
					<div class="page">
						<ul class="clfix">
							<li><a href='#'>&lt;&lt;</a></li>   <li class='on'><a href='#'>1</a></li>  <li><a href='#'>2</a></li>  <li><a href='/server/sub/support/it_data2.php?startPage=20&code=it_data'>3</a></li>  <li><a href='/server/sub/support/it_data2.asp?startPage=30&code=it_data'>4</a></li>  <li><a href='/server/sub/support/it_data2.php?startPage=30&code=it_data'>&gt;&gt;</a></li> 		</ul>
					</div>
				</article>
				<!-- container end -->
			</article>
			<nav id="snb" class="menu6">
				<h2>기술지원</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>