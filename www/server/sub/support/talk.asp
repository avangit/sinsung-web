<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">기술지원</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb6">
					<a href="#;">카톡 1:1 서비스</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>카톡 1:1 서비스</h2>
				<!-- container -->
				<article class="serviceCon">
					<div id="content" class="kakao">
						<a href="https://pf.kakao.com/_kxipdT" target="_blank">
							<img src="/server/img/sub/kakao.jpg" alt="카카오톡 연결하기" />
						</a>
					</div>
				</article>

				<!-- container end -->
			</article>
			<nav id="snb" class="menu6">
				<h2>기술지원</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>