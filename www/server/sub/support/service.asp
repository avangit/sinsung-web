<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">기술지원</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb6">
					<a href="#;">맞춤형 세팅 서비스</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>맞춤형 세팅 서비스</h2>
				<!-- container -->
				<article class="serviceCon">
					<dl>
						<dt>고객을 위한 맞춤형 세팅 서비스</dt>
						<dd>
							업무용  PC구입 시 하드웨어 구성과 소프트웨어 설치로 많은 시간을 소비하십니까?<br />
							이에 고객의 시간과 비용을 단축시키고자 (주)신성씨앤에스는 출하 단계에서<br /> 기업이 구입한
							PC를 직원들에게 배포한 즉시 별다른 설정 과정 없이 이용할 수 있는 <br />신성 맞춤형 세팅
							서비스를 제공하고 있습니다.
						</dd>
					</dl>
				
					<ul>
						<li>
							<img src="/server/img/sub/img_service1.jpg" alt="" />
							<p>추가 부품 장착(고객 요청시)</p>
						</li>
						<li class="right">
							<img src="/server/img/sub/img_service2.jpg" alt="" />
							<p>최고수준의 대량 세팅 시스템</p>
						</li>
						<li>
							<img src="/server/img/sub/img_service3.jpg" alt="" />
							<p>BIOS 설정 및 변경</p>
						</li>
						<li class="right">
							<img src="/server/img/sub/img_service4.jpg" alt="" />
							<p>마스터 이미지 로딩(최첨단 장비)</p>
						</li>
						<li>
							<img src="/server/img/sub/img_service5.jpg" alt="" />
							<p>고객사별 시리얼 이력 관리</p>
						</li>
						<li class="right">
							<img src="/server/img/sub/img_service6.jpg" alt="" />
							<p>자산관리  QR코드</p>
						</li>
					</ul>
				</article>
				<!-- container end -->
			</article>
			<nav id="snb" class="menu6">
				<h2>기술지원</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>