<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">기술지원</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb6">
					<a href="#;">IT 자료실</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->
	<article>
		<article class="sub">
			<article class="leftS">
				<h2>IT 자료실</h2>
				<!-- container -->
				<article>
					<article class="case">
						<ul id="tabMenu_s2" class="mb30">
							<li rel="m1" onClick="location.href='it_data.asp'">제품설명서</li>
							<li rel="m2" class="active">기술지원메뉴얼</li>
						</ul>
					
						<script>
						<!--
						function search_chk2(){
							var frm	=	document.sch_Form;
							frm.submit();
						}
						//-->
						</script>
					
						<div class="sch it_sch clfix">
							<form name="sch_Form" method="POST" action="it_data2.asp">
							<input type="hidden" name="s_i" value="all">
					
							<select name=" p2" id="part2_code">
								<option value="">&nbsp;  ::: 제조사 :::</option>
												<option value="21" >&nbsp; HP</option>
											<option value="62" >&nbsp; 삼성</option>
											<option value="103" >&nbsp; 삼성 단납점 모델</option>
											<option value="23" >&nbsp; LG</option>
											<option value="98" >&nbsp; HPE</option>
											<option value="111" >&nbsp; NETGEAR</option>
											<option value="110" >&nbsp; ATEN</option>
										</select>
					
					
							<select name="p1" id="part1_code">
								<option value="">&nbsp;  ::: 카테고리 ::: </option>
											<option value="1462178498" >&nbsp; 노트북</option>
											<option value="1462178511" >&nbsp; 데스크탑</option>
											<option value="1462178524" >&nbsp; 모니터</option>
											<option value="1462178530" >&nbsp; 서버</option>
											<option value="1462178536" >&nbsp; 워크스테이션</option>
											<option value="1462178544" >&nbsp; 프린터/복합기</option>
											<option value="1462178556" >&nbsp; TV/디스플레이</option>
											<option value="1462178578" >&nbsp; 케어팩 서비스</option>
											<option value="1462178585" >&nbsp; 토너</option>
											<option value="1462178591" >&nbsp; 잉크</option>
											<option value="1475627941" >&nbsp; SSD</option>
											<option value="1476145200" >&nbsp; 전산소모품</option>
											<option value="1512361966" >&nbsp; 화상회의 장비</option>
											<option value="1532681941" >&nbsp; NAS 스토리지</option>
											<option value="1532681957" >&nbsp; 백업솔루션</option>
											<option value="1532682082" >&nbsp; 화상회의 장비</option>
											<option value="1574754886" >&nbsp; 네트워크</option>
										</select>
					
					
							<input type="text" name="s_o" placeholder="검색어를 입력하세요." >
							<a href="javascript:search_chk2()">검색</a>
							</form>
						</div>
					
					<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
						<caption>게시판 내용</caption>
						<colgroup>
							<col width="12%" />
							<col width="" />
							<col width="12%" />
							<col width="15%" />
						</colgroup>
						<thead>
							<tr>
								<th colspan="4" class="r_none"> 그래픽드라이버 설치 매뉴얼</th>
							</tr>
						</thead>
						<tbody>
						<tr>
							<th>등록일</th>
							<td class="left">2020-07-17</td>
							<th>조회수</th>
							<td>3</td>
						</tr>
						<!--
						<tr>
							<th>E-mail</th>
							<td class="left">psh@sinsungcns.com&nbsp;</td>
							<th>작성자</th>
							<td><span class='bold'>신성씨앤에스</span></td>
						</tr>
						-->
								<tr>
							<th>파일</th>
							<td class="left" colspan="3">
								<img src="/server/img/sub/file.gif" alt="파일">  <a href="#" class="pdR20">37_그래픽 드라이버 설치 매뉴얼_신성씨앤에스.pdf</a>								</td>					
						</tr>				
								
						<tr>
							<td colspan="4" class="init">
							
					
								<div class="content-area">
																																																																					
									
					
									<p><span style='color: rgb(99, 99, 99); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 11pt;'>안녕하세요. (주)신성씨앤에스입니다.</span><br><span style='color: rgb(99, 99, 99); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 11pt;'>&nbsp;</span><br><span style='color: rgb(99, 99, 99); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 11pt;'>그래픽 드라이버설치 및 삭제 매뉴얼입니다. </span></p><p><span style='color: rgb(99, 99, 99); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 11pt;'>&nbsp;</span></p><p><span style='color: rgb(99, 99, 99); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 11pt;'>감<span style='color: rgb(99, 99, 99); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 11pt;'><span style='color: rgb(99, 99, 99); font-family: "나눔고딕코딩",NanumGothicCoding,Sans-serif; font-size: 11pt;'>사합니다.</span></span></span>&nbsp;&nbsp;&nbsp;</p><p>&nbsp;</p>			</div>
							
							</td>
						</tr>
					
						<!-- 파일첨부 -->
						
						<!-- //파일첨부 -->
					
						</tbody>
					</table>
					
					<!--버튼-->
					<div class="ntb-tb-view-btn" style="width:100%">
						<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='it_data2.asp'">
						<div class="btnr">
								
										</div>
					</div>

						<div class="ntb-tb-view-reply">
					
							<!--이전글 시작-->	
							<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
								<caption>게시판 이전/다음글</caption>
								<colgroup>
									<col width="12%" />
									<col width="" />
								</colgroup>
											<tr> 
									<th width="12%">이전글</th>
									<td width="88%" class="left font_gray">
																						<a href="#">
																	윈도우 설치 USB 만들기 매뉴얼</a>
																				</td>
								</tr>
									
								<!--다음글 시작-->
											<tr> 
									<th width="12%">다음글</th>
									<td width="88%" class="left font_gray">
															다음글이 없습니다.
														</td>
								</tr>
							</table>
						</div>					
					</article>
				</article>
				<!-- container end -->
			</article>
			<nav id="snb" class="menu6">
				<h2>기술지원</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>