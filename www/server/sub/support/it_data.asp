<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">기술지원</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb6">
					<a href="#;">IT 자료실</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2>IT 자료실</h2>
				<!-- container -->
				<article class="case">
					<ul id="tabMenu_s2" class="mb30">
						<li rel="m1" class="active">제품설명서</li>
						<li rel="m2" onClick="location.href='it_data2.asp'">기술지원메뉴얼</li>
					</ul>
					<script>
					<!--
					function search_chk2(){
						var frm	=	document.sch_Form;
						frm.submit();
					}
					//-->
					</script>

					<div class="sch it_sch clfix">
						<form name="sch_Form" method="POST" action="/server/sub/support/it_data.asp">
							<input type="hidden" name="s_i" value="all">

							<select name=" p2" id="part2_code">
								<option value="">&nbsp;  ::: 제조사 :::</option>
								<option value="21" >&nbsp; HP</option>
								<option value="62" >&nbsp; 삼성</option>
								<option value="103" >&nbsp; 삼성 단납점 모델</option>
								<option value="23" >&nbsp; LG</option>
								<option value="98" >&nbsp; HPE</option>
								<option value="111" >&nbsp; NETGEAR</option>
								<option value="110" >&nbsp; ATEN</option>
							</select>

							<select name="p1" id="part1_code">
								<option value="">&nbsp;  ::: 카테고리 ::: </option>
								<option value="1462178498" >&nbsp; 노트북</option>
								<option value="1462178511" >&nbsp; 데스크탑</option>
								<option value="1462178524" >&nbsp; 모니터</option>
								<option value="1462178530" >&nbsp; 서버</option>
								<option value="1462178536" >&nbsp; 워크스테이션</option>
								<option value="1462178544" >&nbsp; 프린터/복합기</option>
								<option value="1462178556" >&nbsp; TV/디스플레이</option>
								<option value="1462178578" >&nbsp; 케어팩 서비스</option>
								<option value="1462178585" >&nbsp; 토너</option>
								<option value="1462178591" >&nbsp; 잉크</option>
								<option value="1475627941" >&nbsp; SSD</option>
								<option value="1476145200" >&nbsp; 전산소모품</option>
								<option value="1512361966" >&nbsp; 화상회의 장비</option>
								<option value="1532681941" >&nbsp; NAS 스토리지</option>
								<option value="1532681957" >&nbsp; 백업솔루션</option>
								<option value="1532682082" >&nbsp; 화상회의 장비</option>
								<option value="1574754886" >&nbsp; 네트워크</option>
							</select>

							<input type="text" name="s_o" placeholder="검색어를 입력하세요." >
							<a href="javascript:search_chk2()">검색</a>
						</form>
					</div>
					<table id="empTable">
						<caption class="blind"> 게시판</caption>
						<colgroup>
							<col width="10%">
							<col width="*">
							<col width="45%">
						</colgroup>
						<tr>
							<th>번호</th>
							<th>제품명</th>
							<th>다운로드</th>
						</tr>
										<tr>
							<td>276</td>
							<td class="left">
								<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1102&part_idx=5">HP EliteBook 840 G4 Notebook PC</a>

							</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-HP EliteBook 840 G3.PDF </a></p>																													</td>

							</tr>
											<tr>
								<td>275</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1104&part_idx=5">삼성전자 노트북9 NT901X3H - i7</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-삼성전자 노트북 NT901X3H Win10.pdf </a></p>																													</td>

							</tr>
											<tr>
								<td>274</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1105&part_idx=5">삼성 NT901X3J - S1</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-삼성전자 노트북 NT901X3J Win10.pdf </a></p>																													</td>

							</tr>
											<tr>
								<td>273</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1106&part_idx=5">삼성전자 노트북9 NT901X5H -  i7</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-삼성전자 노트북9 NT901X5H.pdf </a></p>																													</td>

							</tr>
											<tr>
								<td>272</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1107&part_idx=5">삼성 NT901X5J - S1</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-삼성전자 노트북9 NT901X5J.pdf </a></p>																													</td>

							</tr>
											<tr>
								<td>271</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1354&part_idx=5">HP EliteBook 850 G5</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-HP Elitebook 850 G5.jpg </a></p>																													</td>

							</tr>
											<tr>
								<td>270</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1501&part_idx=5">HP ProBook 440 G6 - S1</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-HP Probook 440 G6.jpg </a></p>																													</td>

							</tr>
											<tr>
								<td>269</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1502&part_idx=5">HP ProBook 440 G6 - S2</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-HP Probook 440 G6.jpg </a></p>																													</td>

							</tr>
											<tr>
								<td>268</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1503&part_idx=5">HP ProBook 450 G6 - S1</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-HP ProBook 450 G6.jpg </a></p>																													</td>

							</tr>
											<tr>
								<td>267</td>
								<td class="left">
									<a href="http://www.sinsungcns.com/sub/b2b/product_view.php?idx=1504&part_idx=5">HP ProBook 450 G6 - S2</a>

								</td>
				<style type="text/css">
				.taL i{background:#2591c3;border:1px solid #0d7bae;border-radius:3px;width:20px;height:20px;line-height:22px;text-align:center;font-size:12px;margin-right:5px;color:#fff;}
				.taL a{font-size:13px;}
				</style>
								<td class="taL">
									<p><a href="#"><i class="fa fa-download" aria-hidden="true"></i> 제품설명서-HP ProBook 450 G6.jpg </a></p>																													</td>

							</tr>
					</table>
					<div class="page">
						<ul class="clfix">
							<li><a href='#'>&lt;&lt;</a></li>   
							<li class='on'><a href='#'>1</a></li>  
							<li><a href='#'>2</a></li>  
							<li><a href='#'>3</a></li>  
							<li><a href='#'>4</a></li> 
							<li><a href='#'>5</a></li>  
							<li><a href='#'>6</a></li>  
							<li><a href='#'>7</a></li>  
							<li><a href='#'>8</a></li>  
							<li><a href='#'>9</a></li>  
							<li><a href='#'>10</a></li>  
							<li><a href='#'>&gt;</a></li>  
							<li><a href='#'>&gt;&gt;</a></li> 
						</ul>
					</div>
				</article>

				<!-- container end -->
			</article>
			<nav id="snb" class="menu6">
				<h2>기술지원</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>