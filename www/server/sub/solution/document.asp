<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">솔루션</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb3">
					<a href="#;">문서중앙화</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article class="sub">
			<article class="leftS">
				<h2><a href="/server/sub/online/onlineReceive_solution.asp" class="btnSD">솔루션 상담요청</a>문서중앙화</h2>
				<!-- container -->
				<article class="doc">
					<dl>
						<dt class="skyblue">문서중앙화 솔루션</dt>
							<dd>내부정보 유출 방지와 문서관리를 한 번에!<br/>사이버다임의 문서중앙화 시스템은 PC 에서 ‘문서 저장’만 클릭하면 자동으로 암호화하여<br/>사내 서버에 저장하고 세분화된 문서 접근 설정과 버전/이력 관리 등을 제공합니다.<br/>DLP, DRM, 매체제어 등 고민하지 마세요. 문서중앙화 하나면 끝!</dd>
							<dd><b>문서중앙화 시스템은 기업의 축적된 기술과 고유의 노하우, 중요한 내부 자료를<br/>중앙 서버에 저장하여 실수로 인한 문서 유실이나 악의적인 유출을 강력 차단합니다.</b></dd>
					</dl>
					<ul>
						<li>
							<img src="/server/img/sub/icon_doc1.gif" alt=""/>
							<h4>자료 유출 원천 차단 </h4>
							<p>사용자 PC에 파일 저장을 제어하고<br/>중앙 서버에서 통합 관리하여<br/>직원이 퇴사하거나 부서 이동시에도<br/>유실 혹은 유출될 걱정이 없습니다.</p>
						</li>
						<li>
							<img src="/server/img/sub/icon_doc2.gif" alt=""/>
							<h4>직원의 자발적 참여를 이끄는 익숙한 I/F</h4>
							<p>기존 PC 환경과 동일한 프로세스를 제공하고,<br/>썸네일, 즐겨찾기, Drag & Drop, 단축키 등<br/>윈도우 탐색기 인터페이스를 사용하여<br/>새로운 시스템에 적응해야 하는<br/>직원들의 불편함을 해소해 드립니다.</p>
						</li>
						<li>
							<img src="/server/img/sub/icon_doc3.gif" alt=""/>
							<h4>다양한 보안 기능으로 업무 자료 보호</h4>
							<p>사전에 허가된 프로그램만이<br/>서버에 접근할 수 있으며,<br/>파일 저장 시 파일, 경로, 파일명<br/> 모두를 암호화하여 안전합니다.</p>
						</li>
						<li>
							<img src="/server/img/sub/icon_doc4.gif" alt=""/>
							<h4>분산된 콘텐츠를 통합 관리</h4>
							<p>문서를 포함한 캐드 파일, 오디오, 비디오,<br/>이미지등 분산 보관되던 콘텐츠를<br/>단일 환경에서 통합 관리할 수 있습니다.<br/>하나의 안전한 장소에 보관하고 항상 최신<br/>상태로 유지하여 필요할 때마다 빠르게<br/>이용할 수 있어 업무 효율성이 향상됩니다.</p>
						</li>
						<li>
							<img src="/server/img/sub/icon_doc5.gif" alt=""/>
							<h4>팀원들과 원활한 협업을 위한 문서 공유</h4>
							<p>문서 접근 권한을 가진 사용자라면 사내 서버에<br/>저장된 문서를 언제든지 조회할 수 있습니다.<br/>수정된 문서를 매번 전달할 필요 없이 가장<br/>최신 파일에 액세스할 수 있어 팀원들 간의<br/>안전하고 원활한 협업이 가능합니다.</p>
						</li>
						<li>
							<img src="/server/img/sub/icon_doc6.gif" alt=""/>
							<h4>밖에서도 사무실처럼 업무가<br/>가능한 모바일 지원</h4>
							<p>중앙 서버에 문서를 저장해 두셨다면,<br/>출장이나 외근, 출퇴근 이동 시간에도<br/>모바일을 이용한 문서 조회가 가능합니다.<br/>물론, 안전한 공유를 위해 기존 문서<br/>접근 권한은 모바일에서도 유지됩니다.</p>
						</li>
					</ul>
					<h4 class="skyblue">시큐어디스크</h4>
					<p class="txt"><em>시큐어디스크는 모든 문서를 원천 차단하고자 하는 기업에 특화된 문서 유출방지 솔루션 입니다.</em>수년에서 많게는 수십 년에 걸쳐 축적된 영업 기밀과 기업 고유의 노하우 등이 유출되지 않도록 강력한 보안 환경을 제공합니다. </p>
					<div class="scBox">
						<h5><span>1</span>이런 기업이라면 시큐어디스크가 필요합니다!</h5>
						<ul class="arrowUl">
							<li>업무 프로세스, 마케팅 등의 영업 노하우를 다루는 기업</li>
							<li>레시피 등의 기업 고유의 노하우 등의 생산제조 기술을 다루는 기업</li>
							<li>기획과 개발 문서, 디자인, 영상, 신제품 아이디어 등의 산업 재산권을 다루는 기업</li>
							<li>그 외의 중요한 문서를 다루는 기업</li>
						</ul>
						<h5><span>2</span>문서보안을 위한 차별화된 환경을 제공합니다.</h5>
						<ul class="imgUl">
							<li>
								<img src="/server/img/sub/img_sc1.gif" />
								<p class="txtq">기업 노하우가 곧 경쟁력!<br/>영업 기밀문서를 원천적으로 차단할 수는 없을까? </p>
								<p class="txta">완벽한 문서유출 차단 시스템</p>
							</li>
							<li>
								<img src="/server/img/sub/img_sc2.gif" />
								<p class="txtq">여기저기 흡어진 유사한 문서들, 한 곳으로 모으는 방안은?</p>
								<p class="txta">체계적인 관리를 위해 모든 문서는 중앙으로 통합 저장</p>
							</li>
							<li>
								<img src="/server/img/sub/img_sc3.gif" />
								<p class="txtq">프로젝트는 혼자 하는 것이 아니다!<br/>최적의 프로젝트 공유 방안은? </p>
								<p class="txta">조직도 연동 기반의 협업 시스템과 세분화된 접근관리</p>
							</li>
							<li>
								<img src="/server/img/sub/img_sc4.gif" />
								<p class="txtq">작업 집중도가 경쟁력의 핵심. 복잡하고 어려운 사용법은 싫다? </p>
								<p class="txta">기존과 동일한 사용자 업무환경 제공</p>
							</li>
						</ul>
						<h5><span>3</span>업무에 필요한 모든 프로그램과 호환됩니다.</h5>
						<ul class="arrowUl2">
							<li>한글과 컴퓨터 2007, 2010</li>
							<li>MS Office 2003, 2007, 2010</li>
							<li>Open Office </li>
							<li>Adobe Photoshop CS4, CS5 </li>
							<li>Adobe Photoshop 7.0 </li>
							<li>Adobe Reader X</li>
							<li>Adobe Acrobat X</li>
							<li>Illustrator 10 </li>
						</ul>
						<p class="txtTip">※ 시큐어디스크는 이 외에도 업무에 필요한 다양한 프로그램과 호환됩니다. </p>
						<h5><span>4</span>도입효과</h5>
						<ul class="iconUl">
							<li>
								<img src="/server/img/sub/icon_sc1.gif" alt=""/>
								<dl>
									<dt>자료유출 원천 차단 체계 확보 </dt>
										<dd>기본적으로 사용자 PC에는 어떠한 업무자료도 존재하지 않기 때문에 내부자의 의도적인 또는 실수로 인한 자료유출이 원천적으로 차단되게 됩니다.</dd>
										<dd>회사의 보안 정책과 상황에 따라 즉각적으로 변경된 보안을 적용할 수 있습니다.</dd>
										<dd>다양한 정책위반 로그 분석을 통해 사용자의 보안의식을 한 단계 높여 드립니다.</dd>
										<dd>외부로 전달하는 자료는 상급자의 내용 확인 후 반출되어 내부자의 실수나 악의적인 의도에 의한 유출이 발생하지 않습니다.</dd>
								</dl>
							</li>
							<li>
								<img src="/server/img/sub/icon_sc2.gif" alt=""/>
								<dl>
									<dt>정보 유실방지 및 통합관리의 실현 </dt>
										<dd>중앙서버에 통합되어 관리됨으로써 담당자 변경 등에 따른 자료의 유실 위험이 사라지고 업무 연속성이 보장됩니다.</dd>
										<dd>관리포인트의 일원화에 따른 체계적인 관리, 자료의 통합 보안이 가능하고 관리 비용 또한 절감됩니다.</dd>
										<dd>강화된 로그 기능으로 사용자는 물론 관리자도 관리할 수 있습니다.</dd>
								</dl>
							</li>
							<li>
								<img src="/server/img/sub/icon_sc3.gif" alt=""/>
								<dl>
									<dt>사용자 업무 효율성 향상 </dt>
										<dd>사용자 인터페이스가 개인 PC환경과 동일하고, 중앙서버의 문서의 본문 내용까지 검색할 수 있어 보안환경 구축에 따른<br/>업무 불편함이 전혀 발생하지 않습니다.</dd>
										<dd>사용자는 철저한 보안 속에 다양한 협업 툴을 제공받아 보다 편리하게 업무를 진행할 수 있습니다.</dd>
										<dd>사용자는 내부는 물론 출장 등 외부에서도 보안이 유지되는 상태에서 업무를 원활히 수행할 수 있습니다.</dd>
										<dd>네트워크 장애 등의 오프라인 상황 발생 시 암호화된 영역에 임시디스크가 생성 되어 사용자의 업무가 중단되지 않으며<br/>작업 중인 파일이 유실되지 않습니다.</dd>
								</dl>
							</li>
							<li>
								<img src="/server/img/sub/icon_sc4.gif" alt=""/>
								<dl>
									<dt>투자비용 절감 </dt>
										<dd>사용자 PC 리소스를 그대로 활용하는 시스템 구조로 인해 성능 저하가 없고 초기 H/W 투자 측면에서 현저한<br/>비용절감이 가능합니다.</dd>
								</dl>
							</li>
						</ul>
					</div>
				</article>
				<!-- container end -->
			</article>
			<nav id="snb" class="menu3">
				<h2>솔루션</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>