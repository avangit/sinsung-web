<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>
<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">스토리지</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb2">
					<a href="#;">스토리지</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article id="subVisual">
			<h2>storage<span>향상된 효율성, 보안 및 데이터 보호 기능 제공</span></h2>
		</article>
		<article class="sub">
			<article class="leftS">
				<h2>스토리지</h2>
				<!-- container -->

				<article class="pdBox storage">
					<h4>HPE 스토리지</h4>
					<ul>
						<li>
							<dl class="tit">
								<dt>소형</dt>
									<dd class="height">HPE 스토리지 포트폴리오 및 현장에서 입증된 HPE ProLiant 호환성 중에서 가장 저렴한 가격으로 소규모 IT작업 부하에 적합한 플랫폼입니다. iSCSI, FC 및 SAS 호스트 인터페이스 연결 기능을 제공합니다.</dd>
							</dl>
							<div class="img"><img src="/server/img/sub/img_hpe1.jpg" alt=""></div>
							<dl class="txt">
								<dt>사용가능한 옵션 :</dt>
								<dd><a href='http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.asp?idx=1635&part_idx=41' target='_blank' data-image='/server/img/sub/15608189271.png'>HPE SimpliVity 380 Gen10 Node</a></dd>
								<dd><a href='http://sinsung.solutionhosting.co.kr/sub/b2b/estimate_form.asp?idx=1634&part_idx=41' target='_blank' data-image='/server/img/sub/15608195161.png'>HPE MSA 2050 SAN Dual Controller</a></dd>
							</dl>
						</li>
						<li>
							<dl class="tit">
								<dt>중*대형</dt>
									<dd class="height">이 솔루션은 가상화 된 작업부하에 대한 인프라 및 고급 데이터 서비스를 시장에서 가장 잘 팔리는 서버 플랫폼에 결합함으로써 IT를 대폭 단순화합니다. 또한 기존의 인프라 구성하는 비용 및 복잡성보다 훨씬 적은 비용으로 가상화된 워크로드의 효율성, 관리, 보호 및 성능을 획기적으로 향상시킬 수 있습니다.</dd>
							</dl>
							<div class="img"><img src="/server/img/main/img_hpe1.jpg" alt=""></div>
							<dl class="txt">
								<dt>사용가능한 옵션 :</dt>
								<dd><a href='javascript:void(0);'>등록된 데이터가 없습니다.</a></dd>
							</dl>
						</li>
					</ul>
				</article>
			<!-- container end -->
			</article>
			<nav id="snb" class="menu2">
				<h2>스토리지</h2>
			</nav>
		</article>
	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>