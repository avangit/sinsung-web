<footer>
    <div class="sitemap">
        <article>
            <dl>
                <dt>서버</dt>
                    <dd><a href="/server/sub/b2b/rack.asp">랙서버</a></dd>
                    <dd><a href="/server/sub/b2b/tower.asp">타워서버</a></dd>
                    <dd><a href="/server/sub/b2b/blade.asp">블레이드서버</a></dd>
            </dl>
            <dl>
                <dt>스토리지</dt>
                    <dd><a href="/server/sub/b2b/storage.asp">스토리지</a></dd>
            </dl>
            <dl>
                <dt>솔루션</dt>
                    <dd><a href="/server/sub/solution/recovery.asp">신성 리커버리</a></dd>
                    <dd><a href="/server/sub/solution/backup.asp">백업 및 복구</a></dd>
                    <dd><a href="/server/sub/solution/double.asp">서버이중화</a></dd>
                    <dd><a href="/server/sub/solution/virtual.asp">가상화</a></dd>
                    <dd><a href="/server/sub/solution/document.asp">문서중앙화</a></dd>
            </dl>
            <dl>
                <dt>구축사례</dt>
                    <dd><a href="/server/sub/experience/construction_case.asp">구축사례</a></dd>
            </dl>
            <dl>
                <dt>온라인 견적</dt>
                    <dd><a href="/server/sub/online/estimate_list.asp">맞춤사양견적</a></dd>
                    <dd><a href="/server/sub/online/onlineReceive_normal.asp">일반견적요청</a></dd>
                    <dd><a href="/server/sub/online/onlineReceive_solution.asp">솔루션상담요청</a></dd>
                    <dd><a href="/server/sub/online/promotion.asp">프로모션</a></dd>
            </dl>
            <dl>
                <dt>기술지원</dt>
                    <dd><a href="/server/sub/support/service.asp">맞춤형 세팅서비스</a></dd>
                    <dd><a href="/server/sub/support/support.asp">IT유지보수</a></dd>
                    <dd><a href="/server/sub/support/why.asp">서비스 차별화</a></dd>
                    <dd><a href="/server/sub/support/restoration.asp">장애복구 절차 및 대책</a></dd>
                    <dd><a href="/server/sub/support/it_data.asp">IT 자료실</a></dd>
                    <dd><a href="/server/sub/support/onlineReceive.asp">온라인 기술지원 문의</a></dd>
            </dl>
            <dl class="last">
                <dt>회사소개</dt>
                    <dd><a href="/server/sub/company/management.asp">경영철학</a></dd>
                    <dd><a href="/server/sub/company/history.asp">연혁</a></dd>
                    <dd><a href="/server/sub/company/growth.asp">회사성장</a></dd>
                    <dd><a href="/server/sub/company/prize.asp">수상실적</a></dd>
                    <dd><a href="/server/sub/company/greeting.asp">대표 인사말</a></dd>
                    <dd><a href="/server/sub/company/notice.asp">공지사항</a></dd>
                    <dd><a href="/server/sub/company/team.asp">팀을 만나다</a></dd>
                    <dd><a href="/server/sub/company/map.asp">찾아오시는 길</a></dd>
            </dl>
        </article>
        <ul>
            <li><a href="http://sinsung.solutionhosting.co.kr/srm/" target="_blank"><img src="img/common/icon_ft1.gif" alt="SRM전자구매시스템"></a></li>
            <li><a href="https://939.co.kr/sinsungcns/" target="_blank"><img src="img/common/icon_ft2.gif" alt="원격 기술지원"></a></li>
        </ul>
    </div>
    <article>
        <address>
            <p>08389 서울시 구로구 디지털로 272 한신 IT타워 5F</p>
            <p><span>Tel : 02-867-3007</span></p>
            <p><span>Fax :02-867-2328</span></p>
            <p><span>Email : sklee@sinsungcns.com</span></p><br/>
            <p>사업자 등록번호 : 119-86-18434</p>
            <p>통신판매업 신고번호 제 2016-서울구로-0842호</p>
            <p>개인정보책임자 : 박세화</p><br/>
            <p class="txt">홈페이지에 게제된 모든 글과 이미지 등의 정보는 저작권법에 따라 보호를 받는 ㈜신성씨앤에스의 저작물이므로 무단 전재나 복제를 금합니다.</p>
        </address>
        <p class="copyright">COPYRIGHT (C)SINSUNGCNS CORP. ALL RIGHTS RESERVED.</p>
        <p class="link"><a href="http://www.sinsungcns.com/sub/member/safeguard.php">개인정보처리방침</a></p>
    </article>
</footer>