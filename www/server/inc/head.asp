<title>SINSUNG CNS</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.5, minimum-scale=1.0, user-scalable=yes,target-densitydpi=medium-dpi">  -->
<meta name="viewport" content="width=420">
<meta name="keywords" content="신성씨앤에스, 삼성전자 B2B 대리점, HP 총판, CISCO 파트너, 레노버 대리점, 리커버리, 복원솔루션, 컴퓨터, 노트북, 모니터, 워크스테이션, 서버, 토너, 케어팩, 컴퓨터 구매 상담" >
<meta name="Description" content="삼성전자 B2B, HP 총판, CISCO 파트너, 레노버 대리점, 기업고객 IT 맞춤제안" >
<meta property="og:type" content="website">
<meta property="og:title" content="신성씨앤에스 IT B2B ">
<meta property="og:description" content="삼성전자 B2B, HP 총판, CISCO 파트너, 레노버 대리점, 기업고객 IT 맞춤제안">
<meta property="og:image" content="http://www.sinsungcns.com/img/common/img_h1.png">
<meta property="og:url" content="http://www.sinsungcns.com">
<link rel="stylesheet" type="text/css" href="/server/css/style.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="/server/js/common.js"></script>
    <!--banner-->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <![endif]-->