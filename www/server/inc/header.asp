<header>
    <h1><a href="/server/"><img src="/server/img/common/logo_h1.gif" alt="신성씨앤에스"></a></h1>
    <nav>
        <ul id="gnb">
            <li class="">
                <a href="/server/sub/b2b/rack.asp">서버</a>
                <ul class="gnb1">
                    <li><a href="/server/sub/b2b/rack.asp">랙서버</a></li>
                    <li><a href="/server/sub/b2b/tower.asp">타워서버</a></li>
                    <li><a href="/server/sub/b2b/blade.asp">블레이드서버</a></li>
                </ul>
            </li>
            <li  class="">
                <a href="/server/sub/b2b/storage.asp">스토리지</a>
                <ul class="gnb2">
                    <li><a href="/server/sub/b2b/storage.asp">스토리지</a></li>
                </ul>
            </li>
            <li class="">
                <a href="/server/sub/solution/recovery.asp">솔루션</a>
                <ul class="gnb3">
                    <li><a href="/server/sub/solution/recovery.asp">신성 리커버리</a></li>
                    <li><a href="/server/sub/solution/backup.asp">백업 및 복구</a></li>
                    <li><a href="/server/sub/solution/double.asp">서버이중화</a></li>
                    <li><a href="/server/sub/solution/virtual.asp">가상화</a></li>
                    <li><a href="/server/sub/solution/document.asp">문서중앙화</a></li>
                </ul>
            </li>
            <li class="">
                <a href="/server/sub/experience/construction_case.asp">구축사례</a>
                <ul class="gnb4">
                    <li ><a href="/server/sub/experience/construction_case.asp">구축사례</a></li>
                </ul>
            </li>
            <li class="">
                <a href="/server/sub/online/estimate_list.asp">온라인견적</a>
                <ul class="gnb5">
                    <li><a href="/server/sub/online/estimate_list.asp">맞춤사양견적</a></li>
                    <li><a href="/server/sub/online/onlineReceive_normal.asp">일반견적요청</a></li>
                    <li><a href="/server/sub/online/onlineReceive_solution.asp">솔루션상담요청</a></li>
                    <li ><a href="/server/sub/online/promotion.asp">프로모션</a></li>
                </ul>
            </li>
            <li class="">
                <a href="/server/sub/support/driver7.asp">기술지원</a>
                <ul class="gnb6">
                    <li><a href="/server/sub/support/driver7.asp">신성케어 7drivers</a></li>
                        <li><a href="/server/sub/support/service.asp">맞춤형 세팅 서비스</a></li>
                        <li><a href="/server/sub/support/it_data2.asp">기술자료 <br />설치매뉴얼</a></li>
                        <li><a href="/server/sub/support/talk.asp">카톡 1:1 서비스</a></li>
                        <li><a href="/server/sub/support/onlineReceive.asp">기술지원 문의</a></li>
                        <li><a href="/server/sub/support/demo.asp">데모장비 신청 <br />및 테스트 의뢰</a></li>
                </ul>
            </li>
            <li>
                <a href="/server/sub/company/management.asp">회사소개</a>
                <ul class="gnb7">
                    <li><a href="/server/sub/company/management.asp">경영철학</a></li>
                    <li><a href="/server/sub/company/growth.asp">회사성장</a></li>
                    <li><a href="/server/sub/company/greeting.asp">대표 인사말</a></li>
                    <li><a href="/server/sub/company/team.asp">팀을 만나다</a></li>
                    <li><a href="/server/sub/company/history.asp">연혁</a></li>
                    <li><a href="/server/sub/company/prize.asp">수상실적</a></li>
                    <li><a href="/server/sub/company/notice.asp">공지사항</a></li>
                    <li><a href="/server/sub/company/map.asp">찾아오시는 길</a></li>
                </ul>
            </li>
        </ul>
        <p class="topBtn"><a href="/server/sub/online/onlineReceive_normal.asp">Contact US</a></p>
    </nav>
    <p class="cateM"><i>-</i><i>-</i><i>-</i></p>
</header>