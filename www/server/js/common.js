$(function() {

	$(".cateM").click(function(){
		$(".mobileNav, .m_bg").addClass("on");
	});

	$(".mobileNav .close").click(function(e){
		e.preventDefault0;
		$(".mobileNav, .m_bg").removeClass("on");
	});

	//lnb	
	$("#gnb").clone().one().appendTo(".menu1").attr("id","#;");
	$("#gnb").clone().one().appendTo(".mobileNav > div").attr("id","gnbM");
	for(var i=0; i <10; i++) {
		$("#gnb .gnb" + i).clone().one().appendTo(".menu2.lnb"+ i).removeClass(".gnb" + i);
		$("#gnb .gnb" + i).clone().one().appendTo("#snb.menu"+ i).removeClass(".gnb" + i);
	}

	$("#gnbM > li.on ul").show();
	$("#gnbM > li > ul > li a br").remove();
	$("#gnbM > li > a").click(function(e){
		if(!$(this).hasClass("target")){
			e.preventDefault();
		}
		var thisList = $(this).parent();
		thisList.siblings().removeClass("on").find("ul").slideUp();
		thisList.addClass("on");
		thisList.find("ul").slideDown();
	});


	if(!$("#mainVisual").length) {
		var str = $(location).attr("href");
		var res = str.split("/");
		var indexArr2 = res[res.length-2];
		var indexArr = res[res.length-1];
		indexArr = indexArr.split(".asp");

		$("#gnbM li a, #snb li a").each(function(){
			menuUrl = $(this).attr("href");
			var idx = $(this).parent().index();
			if (menuUrl.match(indexArr[0])){
				$(this).parent().addClass("on");
			}
		});	
	}

	$("#lnb .menu > div").on({
		click : function(){
			$(this).find("ul").stop().slideToggle();
		},
		mouseleave : function(){
			$(this).find("ul").stop().slideUp();
		}
	});

	//서버	
	$(".pdBox .txt dd a").hover(function(){
		var imgName = $(this).attr("data-image");
		$(this).parents(".txt").prev(".img").find("img").attr("src", imgName);
	}, function(){
		var imgName = $(this).parents(".txt").prev(".img").attr("data-image");
		$(this).parents(".txt").prev(".img").find("img").attr("src", imgName);
	});



	var arrNumber = new Array(); 

	arrNumber[0] = $(".pdBox li:first-child .txt dd").length;
	arrNumber[1] = $(".pdBox li:nth-child(2) .txt dd").length;
	arrNumber[2] = $(".pdBox li:last-child .txt dd").length;

	var max = 0; 

	for(var i = 0; i < arrNumber.length; i++) {
		if(max < arrNumber[i]) {
			max = arrNumber[i]; 
		}
	}
	
	$(".pdBox li .txt").height(40 + (max * 22));

	// gnb add class on
	var subTitle = $('.menu1 > a').text();
	for(var i = 0; i < 8; i ++) {
		var gnbTitle = $("#gnb .gnb" + i).siblings('a').text();
		if($("#gnb .gnb" + i).siblings('a').text() == subTitle) {
			$("#gnb .gnb" + i).parent().addClass('on');
		}
	}

});