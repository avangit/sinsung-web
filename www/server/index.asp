<!DOCTYPE HTML>
<html lang="ko">
<head>
	<!-- #include virtual="/server/inc/head.asp" -->
</head>

<body>



<div id="wrap">
	<div class="headerWrap">
		<!-- #include virtual="/server/inc/header.asp" -->
		<nav id="lnb">
			<div class="menu">
				<p><a href="/server/"><img src="/server/img/common/icon_home.gif" alt="홈"></a></p>
				<div class="menu1">
					<a href="#;">서버</a>
					<!-- gnb copy -->
				</div>
				<div class="menu2 lnb1">
					<a href="#;">랙서버</a>
					<!-- gnb2 copy-->
				</div>
			</div>
			<aside class="tel">
				<i class="fas fa-phone"></i>
				<h3>문의 02 - 867 - 3007</h3>
				<p>영업 전문가가 도와드리겠습니다.</p>
			</aside>
		</nav>
	</div>
	<!--모바일 카테고리-->
	<!-- #include virtual="/server/inc/mobile_category.asp" -->
	<!--//모바일 카테고리-->

	<article>
		<article id="subVisual">
			<h2>Server<span>고객을 위한 맞춤 구성 서비스, 빠른 납기</span></h2>
		</article>
		<!-- container -->

		<article class="product">
			<div class="img"><img src="/server/img/main/img_rack.jpg" alt=""></div>
			<div class="txt">
				<h3>랙서버</h3>
				<p>랙 서버 또는 랙 마운트 서버는 비용이 합리적이고, 안정적입니다.&nbsp;<br/>비즈니스 성장에 따라 필요한 프로세서, 드라이브, 메모리 및 전원 공급 장치를 포함한&nbsp;<br/>인프라를 추가하여 업무 워크로드를 짧은 시간에 효율적으로 최적화시킬 수 있습니다.&nbsp;<br/>귀사의 환경에 적합한 제품을 선택할 수 있도록  최선을 다해 도와 드리겠습니다.&nbsp;<br/>뿐만 아니라 고객을 위한 맞춤 구성 서비스를 제공하고 있습니다.</p>
				<ul class="btn">
					<li><a href="#;"><img src="/server/img/main/icon_info.gif" alt="">구입 가이드 다운로드</a></li>
					<li><a href="#;"><img src="/server/img/main/icon_check.gif" alt="">체크리스트 다운로드</a></li>
				</ul>
			</div>
		</article>
		<div class="gray">

			<article class="pdBox">
				<h4>HPE 프로 라이언트 랙서버</h4>
				<ul>
					<li>
						<dl class="tit">
							<dt>소형</dt>
								<dd>HPE의 보급형 ProLiant 서버는 중소 규모 비즈니스를 위해 설계되었으며 신속하게 운영할 수 있는 통합 도구가 있습니다.</dd>
						</dl>
						<div class="img" data-image="/server/img/main/img_hpe1.jpg"><img src="/server/img/main/img_hpe1.jpg" alt=""></div>
						<dl class="txt">
							<dt>사용가능한 제품 :</dt>
							<dd><a href='sub/b2b/estimate_form.php?idx=1629&part_idx=20' target='_blank' data-image='/server/img/sub/15556533541.png'>HPE DL20 Gen10</a></dd><dd><a href='sub/b2b/estimate_form.php?idx=1085&part_idx=20' target='_blank' data-image='/server/img/sub/15121184171.jpg'>HPE DL160 Gen9</a></dd><dd><a href='/sub/b2b/estimate_form.php?idx=1084&part_idx=20' target='_blank' data-image='/server/img/sub/15121181201.png'>HPE DL20 Gen9</a></dd><dd><a href='/sub/b2b/estimate_form.php?idx=1080&part_idx=20' target='_blank' data-image='/server/img/sub/15121042501.png'>HPE DL180 Gen9</a></dd><dd><a href='/sub/b2b/estimate_form.php?idx=812&part_idx=20' target='_blank' data-image='/server/img/sub/15120911751.png'>HPE DL120 Gen9</a></dd>				</dl>
					</li>

					<li>
						<dl class="tit">
							<dt>중형</dt>
								<dd>HP ProLiant 서버는 안정적인 성능을 위해 최대 2개의 프로세서와 더 많은 메모리 및 스토리지를 지원합니다.</dd>
						</dl>
						<div class="img" data-image="/server/img/main/img_hpe2.jpg"><img src="/server/img/main/img_hpe2.jpg" alt=""></div>
						<dl class="txt">
							<dt>사용가능한 제품 :</dt>
							<dd><a href='sub/b2b/estimate_form.php?idx=1086&part_idx=20' target='_blank' data-image='/server/img/sub/15295456991.png'>HPE DL380 Gen10</a></dd><dd><a href='sub/b2b/estimate_form.php?idx=271&part_idx=20' target='_blank' data-image='/server/img/sub/15120914601.png'>HPE DL380 Gen9</a></dd><dd><a href='/sub/b2b/estimate_form.php?idx=84&part_idx=20' target='_blank' data-image='/server/img/sub/14660587781.JPG'>HPE  DL320e Gen8</a></dd><dd><a href='/sub/b2b/estimate_form.php?idx=221&part_idx=20' target='_blank' data-image='/server/img/sub/14688902251.jpg'>HPE ProLiant DL360 Gen9</a></dd>				</dl>
					</li>

					<li>
						<dl class="tit">
							<dt>대형</dt>
								<dd>최고급 서버로써 중요한 애플리케이션을 지원하고 안정적인 성능을 위해 최대 4개의 프로세서를 지원합니다.</dd>
						</dl>
						<div class="img" data-image="/server/img/main/img_hpe3.jpg"><img src="/server/img/main/img_hpe3.jpg" alt=""></div>
						<dl class="txt">
							<dt>사용가능한 제품 :</dt>
							<dd><a href='sub/b2b/estimate_form.php?idx=1083&part_idx=20' target='_blank' data-image='/server/img/sub/15121170731.png'>HPE DL580 Gen9</a></dd>				</dl>
					</li>
				</ul>
			</article>
		</div>
		<!-- container end -->

	</article>
	<!-- #include virtual="/server/inc/footer.asp" -->
</div>

</body>
</html>