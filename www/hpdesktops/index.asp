
<!DOCTYPE html>
<html lang="kr">
	<head>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-M48Q7D5');</script>
		<!-- End Google Tag Manager -->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110123538-2"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-110123538-2');
		</script>
 		<!-- Google Tag Manager -->

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

		<script type="text/javascript" src="https://www.eventservice.kr/js/jquery.min.js"></script>
		<link rel="stylesheet" href="/hpdesktops/css/base_style.css" type="text/css" media="screen" charset="utf-8" />

		<link rel="stylesheet" href="/hpdesktops/css/style.css" type="text/css" media="screen" charset="utf-8" />

		<!-- 링크 공유 -->
		<meta property="og:type" content="website">
		<meta property="og:title" content="SINSUNG CNS">
		<meta property="og:url" content="https://www.eventservice.kr/2019/hpi/0718_sinsung/index.html">
		<meta property="og:description" content="여기를 눌러 자세한 내용을 확인하세요.">
		<meta property="og:image" content="/hpdesktops/images/sinsung_sumnail_01.jpg">
		<title>SINSUNG CNS</title>

		<style>

		</style>
	</head>


	<body>

         <!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M48Q7D5"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<div id="wrap">
			<div class="header-area">

				<section id="header">

					<div class="neo-inner">
						<div class="logo-area">
							<a href="/main.asp" target="_blank"><img src="/hpdesktops/images/logo-sinsung.png" alt="SINSUNG CNS" /></a><a href="https://www8.hp.com/kr/ko/home.html" target="_blank"><img src="/hpdesktops/images/logo-hpi.png" alt="HPI" /></a>
						</div>

						<ul class="gnb">
							<li><a href="/sub/b2b/product01.asp?c=1001000000" target="_blank">워크스테이션</a></li>
							<li><a href="/sub/b2b/product01.asp?c=1001000000" target="_blank">노트북</a></li>
							<li><a href="/sub/b2b/product_list.asp?c=1002010000" target="_blank">데스크탑</a></li>
							<li><a href="http://sinsungcns.com/sub/b2b/product_list.asp?part_idx=7&op1=21" target="_blank">모니터</a></li>
							<li><a href="/sub/experience/construction_case.asp" target="_blank">구축사례</a></li>
							<li><a href="/sub/company/management.asp" target="_blank">회사소개</a></li>
							<li><a href="/sub/support/onlineReceive.asp" target="_blank">Contact US</a></li>
						</ul>

						<div id="mo-menu-trigger-close" class="mo-menu-trigger">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>

					<div class="m-gnb-bg"></div>
				</section>

				<section id="snb-area">
					<div class="neo-inner">
						<ul class="snb">
							<li><a href="#allinone">올인원</a></li>
							<li><a href="#mini">미니</a></li>
							<li><a href="#small">스몰 폼 팩터</a></li>
							<li><a href="#micro">마이크로타워</a></li>
							<li><a href="#tower">타워</a></li>
						</ul>

						<p class="contact-us"><strong>문의</strong><img src="/hpdesktops/images/icon-call.png" alt="전화번호" />02-867-3007 [영업전문가가 도와드리겠습니다]</p>

					</div>
				</section>
			</div>

			<section id="visual2">
				<div class="neo-inner">
					<img src="/hpdesktops/images/visual-text-2.png" alt="HP EliteDesk 800 G5 TWR & SFF" />

				</div>
			</section>

			<a name="allinone"></a>
			<section id="quarter2">
				<div class="neo-inner">
					<div class="quarter-area">
						<div><img src="/hpdesktops/images/quarter-img.png" alt="quarter mark" /></div>
						<dl>
							<dt><h1><strong>35분기 연속, </strong>HP 최고등급 파트너 인증기업 (주)신성씨앤에스</h1></dt>
							<dd>신성CNS 홈페이지에서 지금 바로 구매하세요!</dd>
						</dl>
					</div>
				</div>
			</section>

			<section id="neo-contents">
				<div class="neo-inner">
					<div class="product">
						<h2><span>올인원</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/hpdesktops/images/allinone.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP ProOne 400 G4 All-in-One</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ProOne 400 G4 All-in-One</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i3-8100T Processor
															3.1GHz (6MB, 4Core, 4Th)
															</span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>SSD</strong><span>128GB NVMe SSD </span></li>
									<li><strong>RAM</strong><span>4GB (4GBx1) DDR4-2666
																(최대 32GB / 2 Slots)
																</span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1746&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP EliteOne 800 G5 All-in-One</h3>
								<ul>
									<li><strong>제품명</strong><span>HP EliteOne 800 G5 All-in-One</span></li>
										<li><strong>CPU</strong><span>Intel® Core™ i3-9100 Processor
														3.6GHz (6MB, 4Core, 4Th)
														 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>SSD</strong><span>256GB NVMe SSD </span></li>
									<li><strong>RAM</strong><span>4GB (4GBx1) DDR4-2666
												(최대 32GB / 2 Slots)
												 </span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<a name="mini"></a>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1991&part_idx=17" target="_blank">맞춤 견적</a></div>

							</div>
						</div>

					</div>
					<div class="product">
						<h2><span>미니</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/hpdesktops/images/mini.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP EliteDesk 800 G3 DM</h3>
								<ul>
									<li><strong>제품명</strong><span>HP EliteDesk 800 G3 DM</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i3-6100 Processor
												(3.7GHz, 3MB, 2C)
												</span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>SSD</strong><span>128GB NVMe SSD </span></li>
									<li><strong>RAM</strong><span>4GB (4GBx1) DDR4-2400
													(최대 32GB/2 Slots)
													</span></li>

									<li><strong>그래픽</strong><span>Intel® HD Graphics 530 </span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1993&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP EliteDesk 800 G5 DM</h3>
								<ul>
									<li><strong>제품명</strong><span>HP EliteDesk 800 G5 DM</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-9500 Processor
												3.0GHz (Up 4.4GHz, 9MB, 6C)
												</span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>SSD</strong><span>256GB SSD M.2, NVMe SSD</span></li>
									<li><strong>RAM</strong><span>8GB (8GBx1) DDR4-2666
												(최대 32GB / 2 Slots)
												 </span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1988&part_idx=17" target="_blank">맞춤 견적</a></div>
								<a name="small"></a>
							</div>

						</div>
					</div>
				</div>
			</section>

			<section id="neo-contents" class="color-gray100">
				<div class="neo-inner">


					<div class="product">
						<h2><span>스몰 폼 팩터</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/hpdesktops/images/small01.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP 280 G3 SFF</h3>
								<ul>
									<li><strong>제품명</strong><span>HP 280 G3 SFF</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i3-9100 Processor
											3.6GHz (Up 4.2GHz, 6MB, 4C)
											</span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit </span></li>

									<li><strong>SSD</strong><span>256GB NVMe SSD </span></li>
									<li><strong>RAM</strong><span style="letter-spacing:-1px;">8GB (8GBx1) DDR4-2666
													(최대 32GB / 2 Slots)
													</span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1994&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP 280 G4 SFF</h3>
								<ul>
									<li><strong>제품명</strong><span>HP 280 G4 SFF</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-9500 Processor
													3.0GHz (Up 4.4GHz, 9MB, 6C, 6th)
													 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>HDD</strong><span>1TB HDD</span></li>
									<li><strong>RAM</strong><span>16GB DDR4-2666 </span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1986&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
						<div class="pro-area " style="padding-top:20px;">
							<div class="pro-img"><img src="/hpdesktops/images/small02.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP ProDesk 400 G6 SFF</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ProDesk 400 G6 SFF</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-9500 Processor
											3.0GHz (Up 4.4GHz, 9MB, 6C)
											</span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>

									<li><strong>SSD</strong><span>256GB NVMe SSD</span></li>
									<li><strong>RAM</strong><span>8GB (8Gx1) DDR4-2666
													(최대32GB / 2 Slots)
													</span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1983&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list-mo">
							    &nbsp;
							</div>
						</div>
						<div class="pro-area" style="padding-top:20px;">
							<div class="pro-img"><img src="/hpdesktops/images/small03.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP ProDesk 600 G4 SFF</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ProDesk 600 G4 SFF</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-8500 Processor
											3.0GHz (Up 4.1GHz, 9MB, 6C)
											 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>

									<li><strong>HDD</strong><span>1TB HDD (7,200rpm)</span></li>					<li><strong>SSD</strong><span>256GB NVMe SSD</span></li>
									<li><strong>RAM</strong><span>8GB (4GBx2) DDR4-2666
											(최대 64GB/4 Slots)
											 </span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1997&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP ProDesk 600 G5 SFF</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ProDesk 600 G5 SFF</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ Pentium® G5420 Processor
													3.8GHz (4MB, 2C)
													</span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>SSD</strong><span>256GB NVMe SSD </span></li>

									<li><strong>RAM</strong><span>4GB (4GBx1) DDR4-2666
											(최대 64GB/ 4 Slots)
											 </span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 610</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1789&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
						<div class="pro-area" style="padding-top:20px;">
							<div class="pro-img"><img src="/hpdesktops/images/small04.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP EliteDesk 800 G4 SFF</h3>
								<ul>
									<li><strong>제품명</strong><span>HP EliteDesk 800 G4 SFF</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i7-8700 Processor
											3.2GHz (Up 4.6GHz, 12MB, 6C)
											</span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit </span></li>

									<li><strong>HDD</strong><span>1TB HDD (7,200rpm, SATA)</span></li>				<li><strong>SSD</strong><span>512GB NVMe SSD </span></li>
									<li><strong>RAM</strong><span>16GB (16GBx1) DDR4-2666
														(최대 64GB / 4 Slots)
														</span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1996&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP EliteDesk 800 G5 SFF</h3>
								<ul>
									<li><strong>제품명</strong><span>HP EliteDesk 800 G5 SFF</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-9500 Processor
													3.0GHz (Up4.4GHz, 9MB, 6C)
													 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>HDD</strong><span>1TB HDD (7,200rpm, SATA)</span></li>				<li><strong>ODD</strong><span>Slim Super Multi</span></li>

									<li><strong>RAM</strong><span>8GB (8GBx1) DDR4-2666
														(최대 64GB / 4 Slots)
														 </span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1989&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
						</div>

					</div>
				</div>
				<a name="micro"></a>
			</section>

			<section id="neo-contents">
				<div class="neo-inner">
					<div class="product">
						<h2><span>마이크로 타워</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/hpdesktops/images/micro.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP 280 G4 MT</h3>
								<ul>
									<li><strong>제품명</strong><span>HP 280 G4 MT</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-9400 Processor
											2.9GHz (Up 4.1GHz, 9MB, 6C)
											 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit </span></li>
									<li><strong>HDD</strong><span>500GB HDD</span></li>
									<li><strong>SSD</strong><span>256GB NVMe  SSD </span></li>
									<li><strong>ODD</strong><span>Slim Super Multi</span></li>
									<li><strong>RAM</strong><span>8GB (8GBx1) DDR4-2666
													(최대 32GB / 2 Slots)
													 </span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630 </span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1721&part_idx=17&recom=1&PHPSESSID=60d34e463a99d388203637ec15b2a930" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP 280 G5 MT</h3>
								<ul>
									<li><strong>제품명</strong><span>HP 280 G5 MT</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-9400 Processor
													2.9GHz (Up 4.1GHz, 9MB, 6C)
													 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>HDD</strong><span>1TB HDD</span></li>
									<li><strong>SSD</strong><span>256GB NVMe SSD</span></li>
									<li><strong>ODD</strong><span>Slim Super Multi</span></li>
									<li><strong>RAM</strong><span>8GB (8GBx1) DDR4-2666
													(최대 32GB / 2 Slots)
													 </span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1987&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
						<div class="pro-area" style="padding-top:20px;">
							<div class="pro-img"><img src="/hpdesktops/images/micro02.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP ProDesk 400 G6 MT</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ProDesk 400 G6 MT</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i3-9100 Processor
													3.6GHz (Up 4.2GHz, 6MB, 4C)
													 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit </span></li>
									<li><strong>SSD</strong><span>256GB NVMe SSD</span></li>
									<li><strong>ODD</strong><span>Slim Super Multi DVD Writer</span></li>
									<li><strong>RAM</strong><span>8GB (8GBx1) DDR4-2666
													(최대 64GB/2 Slots)
													</span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1788&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP ProDesk 600 G5 MT</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ProDesk 600 G5 MT</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-9500 Processor
													3.0GHz (Up 4.4G, 9MB, 6C)
													 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>SSD</strong><span>256GB NVMe SSD</span></li>
									<li><strong>ODD</strong><span>Slim Super Multi DVD Writer</span></li>
									<li><strong>RAM</strong><span>8GB (8GBx1) DDR4-2666
													(최대 64GB/4 Slots)
													</span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics 630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1756&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
						<a name="tower"></a>

					</div>






				</div>
			</section>
			<section id="neo-contents" class="color-gray100">
				<div class="neo-inner">
					<div class="product" >
						<h2><span>타워</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/hpdesktops/images/tower.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP EliteDesk 800 G4 TWR</h3>
								<ul>
									<li><strong>제품명</strong><span>HP EliteDesk 800 G4 TWR</span></li>
									<li><strong>CPU</strong><span>Intel® Pentium® Gold G5400 Processor
															(3.7GHz, 4MB, 2C)
															</span></li>
									<li><strong>OS</strong><span>Free Dos 2.0</span></li>
									<li><strong>HDD</strong><span>1TB 3.5” HDD (7,200rpm, SATA)</span></li>
									<li><strong>ODD</strong><span>Slim Super Multi DVD Writer</span></li>
									<li><strong>RAM</strong><span>4GB (4GBx1) DDR4-2666
													(최대 64GB/4 Slots)
													</span></li>

									<li><strong>그래픽</strong><span>Intel® UHD Graphics 610</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1995&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP EliteDesk 800 G5 TWR</h3>
								<ul>
									<li><strong>제품명</strong><span>HP EliteDesk 800 G5 TWR</span></li>
									<li><strong>CPU</strong><span>Intel® Core™ i5-9500 Processor
													3.0GHz (Up 4.4GHz, 9MB, 6C)
													 </span></li>
									<li><strong>OS</strong><span>Windows 10 Professional 64bit</span></li>
									<li><strong>SSD</strong><span>512G NVMe SSD</span></li>
									<li><strong>ODD</strong><span>Slim Super Multi DVD Writer</span></li>
									<li><strong>RAM</strong><span>8GB (8GBx1) DDR4-2666 3
											(최대 64GB/4 Slots)
											</span></li>

									<li><strong>그래픽</strong><span>NVIDIA® GeForce® GT730 2GB</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.asp?idx=1990&part_idx=17" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
					</div>

				</div>
			</section>


			<!--붙이는곳-->
			<section id="neo-contents" style="padding:80px 0 0 0;" >
				<div class="neo-inner">
					<h2> Only 신성씨앤에스에서만 제공되는 <br/><span>신성 케어 멤버십 인증서</span></h2>

				<style>
					#content { width:100%; overflow:hidden; margin-top:55px; min-height:360px;padding-bottom:40px; }

					#content {letter-spacing:-.25px;}
					#content .cont_tit{background:url(..//hpdesktops/images/cont_tit.gif) no-repeat 0 5px;padding-left:10px;font-size:20px;color:#333;line-height:26px;margin-bottom:20px;}
					#content .cont_btxt{font-size:15px;line-height:26px;letter-spacing:-0.5px}
					#content .cont_txt{font-size:14px;line-height:24px;letter-spacing:-0.5px}
					#content .cobalt{color:#29adc8}
					#content .skyblue{color:#2f7bb5}
					#content .forestgreen{color:#005f4b}
					#content .green{color:#00b336}
					#content .red{color:red}
					#content .whblue{color:#1e65b3}
					#content .blue{color:#0c3993}
					#content .white{color:#fff}
					#content .section{margin-bottom:70px;}
					.greeCon h4, .manaCon h4, .growth h4, .prize h4 { font-size:30px; line-height:45px; color:#333; letter-spacing:-.25px; font-family:'Nanum Square'; }
					#content h4 span { color:#104580; }
					#content .acr3 h4 span { color:#4e3498; }
					#content.case{overflow:hidden;}
					#content.case .article{margin-top:80px;}

					#content.veeam .sol_stit{margin:0px 0 20px 0;}
					#content.veeam .cont_txt{letter-spacing:-.25px;}
					#content.veeam .section{margin-bottom:120px;}
					#content.veeam .sl_bulltt{font-size:16px;color:#333;}
					#content.srmCon .cont_txt{font-size:15px;line-height:26px;}


					.drivers7{margin-top:0 !important;}
					.drivers7 > img{}
					.drivers7 > dl{padding-top:40px;}
					.drivers7 > dl > dt{font-size:20px;color:#3e3935;padding-left:12px;position:relative;}
					.drivers7 > dl > dt:before{display:block;content:"";position:absolute;width:5px;height:18px;background:url("//hpdesktops/images/drivers-before.jpg")no-repeat center top;left:0;top:2px;}
					.drivers7 > dl > dd{padding-top:15px;}
					.drivers7 > dl > dd > p{font-size:15px;color:#666;line-height:26px;}
					.drivers7 > dl > dd > ul{font-size:0;text-align:left;}
					.drivers7 > dl > dd > ul > li{display:inline-block;vertical-align:top;width:50%;text-align:left;font-size:15px;letter-spacing:-0.025em;color:#666;padding-left:35px;position:relative;box-sizing:border-box;margin-top:10px;line-height:26px;}
					.drivers7 > dl > dd > ul > li.check:before{background:url("//hpdesktops/images/li-checkon.jpg")no-repeat center top;}
					.drivers7 > dl > dd > ul > li:before{display:block;content:"";position:absolute;left:0;top:3px;width:23px;height:20px;background:url("//hpdesktops/images/li-checkoff.jpg")no-repeat center top;}
					.drivers7 .inner{margin-top:100px;}
					.content-tab{}
					.content-tab ul{font-size:0;text-align:center;}
					.content-tab ul li.active{}
					.content-tab ul li.active a{border-bottom:0;color:#236bbc;background-color:#fff;font-weight:bold;}

					.content-tab ul li a{display:block;background-color:#eee;text-align:center;font-size:14px;line-height:54px;border:1px solid #d7d7d7;border-right:0;transition:all 0.3s;}
					.content-tab ul li:last-of-type a{border-right:1px solid #d7d7d7;}

					.drivers7 .content-wrap{}
					.drivers7 .content-wrap .content:first-of-type{display:block;}
					.drivers7 .content-wrap .content{display:none;}
					.drivers7 .content-wrap .content .line{border:1px solid #e1e1e1;padding:0 30px 55px;border-top:0;padding-top:80px;}
					.drivers7 .content-wrap .content .line > .tit:before{display:block;content:"";position:absolute;width:48px;height:4px;background-color:#236bbd;top:-25px;left:0;}
					.drivers7 .content-wrap .content .line > .tit{font-size:18px;color:#323230;position:relative;font-weight:bold;}
					.drivers7 .content-wrap .content .line > .sub-txt{font-size:15px;color:#80756f;line-height:26px;padding-top:20px;}
					.drivers7 .content-wrap .content .line > .sub-txt b{color:#402f1d}
					.drivers7 .content-wrap .content .line > ul{font-size:0;text-align:center;margin-top:15px;}
					.drivers7 .content-wrap .content .line > ul > li{display:inline-block;vertical-align:top;position:relative;}
					.drivers7 .content-wrap .content .line > ul.type01 > li:nth-of-type(2n){margin-right:0;}
					.drivers7 .content-wrap .content .line > ul.type01 > li:nth-of-type(-n+2){margin-bottom:18px}
					.drivers7 .content-wrap .content .line > ul.type01 > li{margin-right:18px;}

					.drivers7 .content-wrap .content .line > ul.type02 > li{margin-right:18px;}
					.drivers7 .content-wrap .content .line > ul.type02 > li:last-of-type{margin-right:0;}

					.drivers7 .content-wrap .content .line > ul > li img{width:100%;}
					.drivers7 .content-wrap .content .line > ul > li p{position:absolute;left:0;bottom:0;font-size:15px;color:#fff;text-align:center;background-color:rgba(0,0,0,0.8);width:100%;line-height:40px;}

					.drivers7 .content-wrap .content .line > img{max-width:100%;margin-top:10px;}
				</style>

				<div id="content" class="drivers7">

					<dl>
						<dt>전산 하드웨어 구매 시, 아래 서비스를 제대로 받고 있습니까?</dt>
						<dd>
							<ul>
								<li class="check">사전 부품 추가, OS 탑재 서비스</li>
								<li>자산 이력 관리 및 추적 가능한 시스템</li>
								<li>긴급 기술 문의, 장애 지원 서비스</li>
								<li class="check">무료 택배 발송/회수 서비스</li>
								<li>업무에 필요한 설치 매뉴얼 제공받기 </li>
								<li>구매 전, 데모 장비 신청 서비스</li>
								<li>원클릭으로 발주하는 맞춤형 앱 플랫폼</li>
							</ul>
						</dd>
					</dl>
					<div class="inner">
						<div class="content-tab">
							<ul>
								<li class="active">
									<a href="javascript:;">맞춤형 제품 제공</a>
								</li>
								<li>
									<a href="javascript:;">기술 및 장애 처리</a>
								</li>
								<li>
									<a href="javascript:;">기술자료</a>
								</li>
								<li>
									<a href="javascript:;">맞춤형 구매시스템</a>
								</li>
								<li>
									<a href="javascript:;">자산이력관리</a>
								</li>
								<li>
									<a href="javascript:;">무료배송</a>
								</li>
								<li>
									<a href="javascript:;">데모장비지원</a>
								</li>
							</ul>
						</div>
						<div class="content-wrap">
							<div class="content">
								<div class="line">
									<p class="tit">Driver 1. 맞춤형 제품 제공</p>
									<p class="sub-txt">기업의 요구 사항에 맞는 하드웨어를 구성하고, 필요한 소프트웨어를 설치하는 <b>맞춤형 세팅 서비스</b>를 제공합니다.  <br />고객은 별다른 설정 없이 구입한 장비를 즉시 배포할 수 있어 <b>시간과 비용을 줄일 수 있습니다.</b></p>
									<ul class="type01">
										<li>
											<img src="//hpdesktops/images/driver7-img02.jpg" alt="이미지" />
											<p>추가 부품 장착(고객 요청 시)</p>
										</li>
										<li>
											<img src="//hpdesktops/images/driver7-img03.jpg" alt="이미지" />
											<p>최고 수준의 대량 세팅 시스템</p>
										</li>
										<li>
											<img src="//hpdesktops/images/driver7-img04.jpg" alt="이미지" />
											<p>BIOS 설정 및 변경</p>
										</li>
										<li>
											<img src="//hpdesktops/images/driver7-img05.jpg" alt="이미지" />
											<p>마스터 이미지 로딩(최첨단 장비)</p>
										</li>
									</ul>
								</div>
							</div>
							<div class="content">
								<div class="line">
									<p class="tit">Driver 2. 기술 및 장애 처리</p>
									<p class="sub-txt">제조사의 보증 서비스와는 별개로 신속한 기술 지원 및 긴급 장애 대응으로 업무 손실을 최소화하기 위해<br /><b>전담 엔지니어 콜센터를 운영</b>합니다. 게시판 플랫폼을 통해서 <b>실시간으로 처리 상태</b>를 확인하실 수 있습니다. </p>
									<ul class="type02">
										<li>
											<img src="//hpdesktops/images/driver7-img13.jpg" alt="이미지" />
											<p>원격지원</p>
										</li>
										<li>
											<img src="//hpdesktops/images/driver7-img14.jpg" alt="이미지" />
											<p>카카오톡</p>
										</li>
										<li>
											<img src="//hpdesktops/images/driver7-img15.jpg" alt="이미지" />
											<p>엔지니어 콜센터</p>
										</li>
									</ul>
								</div>
							</div>
							<div class="content">
								<div class="line">
									<p class="tit">Driver 3. 기술자료</p>
									<p class="sub-txt"><b>필요한 설치 매뉴얼을 제공</b>함으로써 기업 전산 운영의 효율성을 높입니다. 다양한 기업 환경에 따른 <b>구축 사례를 제공</b>하고,<br />소프트웨어에 맞는 하드웨어 <b>성능 테스트(BMT),</b> 신제품 등 꼭 필요한 정보를 제공합니다.</p>
									<img src="//hpdesktops/images/driver7-img16.jpg" alt="이미지" />
								</div>
							</div>
							<div class="content">
								<div class="line">
									<p class="tit">Driver 4. 맞춤형 구매시스템</p>
									<p class="sub-txt"><b>고객 맞춤 솔루션과 제품 표준화</b>로 구매 프로세스를 간소화하여 시간과 비용을 절감 시킵니다. 거래명세서, 견적서, 구매 이력 조회,<br />전자 구매 승인 시스템, 여신 결제를 갖춘 <b>원 클릭 구매 시스템</b>입니다.</p>
									<img src="//hpdesktops/images/driver7-img17.jpg" alt="이미지" />
								</div>
							</div>
							<div class="content">
								<div class="line">
									<p class="tit">Driver 5. 자산이력관리</p>
									<p class="sub-txt">효과적인 PC 자산 관리를 위한 <b>QR 라벨</b>을 장비에 부착하여 사양 및 유저 정보를 확인 가능합니다. SRM에서는 고객이<br /><b>별다른 전산 입력 작업없이</b> 주문 제품의 입고일자, 시리얼번호, 사용자 등의 <b>자산관리가 가능</b>합니다.</p>
									<ul class="type01">
										<li>
											<img src="//hpdesktops/images/driver7-img18.jpg" alt="이미지" />
											<p>제품준비</p>
										</li>
										<li>
											<img src="//hpdesktops/images/driver7-img19.jpg" alt="이미지" />
											<p>출고 정보 바코드 생성</p>
										</li>
										<li>
											<img src="//hpdesktops/images/driver7-img20.jpg" alt="이미지" />
											<p>바코드 출력</p>
										</li>
										<li>
											<img src="//hpdesktops/images/driver7-img21.jpg" alt="이미지" />
											<p>바코드 부착 및 정보 대조</p>
										</li>
									</ul>
								</div>
							</div>
							<div class="content">
								<div class="line">
									<p class="tit">Driver 6. 무료배송</p>
									<p class="sub-txt">MOT 교육을 이수한 엔지니어가 직접 납품 및 설치를 합니다. 고객 요구에 따라 택배, 퀵, 화물 등의 다양한 배송 방법으로<br /><b>무료 발송 및 회수 픽업 서비스</b>를 운영 중입니다.</p>
									<img src="//hpdesktops/images/driver7-img22.jpg" alt="이미지" />
								</div>
							</div>
							<div class="content">
								<div class="line">
									<p class="tit">Driver 7. 데모장비지원</p>
									<p class="sub-txt">장비 도입을 위한 테스트 또는 시연이 필요하신 경우에는 <b>데모 장비를 무상으로 지원합니다. 또한 테스트를 의뢰하실 경우<br />지원 가능합니다. 신청 절차는 홈페이지 게시판, 카카오톡 플러스에서 하실 수 있습니다.</b></p>
									<img src="//hpdesktops/images/driver7-img23.jpg" alt="이미지" />
								</div>
							</div>
						</div>

						<!-- 공통 -->

					</div>


				</div>
			</section>


			<script>
				$(function(){
					$(".content-tab ul li a").click(function(){
						var tabIdx = $(this).parent().index();
						$(".content-tab ul li").removeClass("active");
						$(this).parent().addClass("active");
						$(".drivers7 .content-wrap .content").hide();
						$(".drivers7 .content-wrap .content").eq(tabIdx).hide().fadeIn();
					});
				});
			</script>

			<!--붙이는곳-->



			<!--인증현황-->
				<section id="neo-contents" style="padding:0 0 80px 0;" >
			<div class="neo-inner" >
					<h2 class="color-blue" style="padding-top:50px;"><span>인증현황</span></h2>
							<ul class="certi-list">
								<li>
									<a href="/sub/company/prize.asp" target="_blank">
										<img src="/hpdesktops/images/certi-01.png" />
										<dl>
											<dt>Inno Biz</dt>
											<dd>(2020.1.20~2023.1.19)</dd>
										</dl>
									</a>
								</li>
								<li>
									<a href="/sub/company/prize.asp" target="_blank">
										<img src="/hpdesktops/images/certi-02.png" />
										<dl>
											<dt>벤처기업확인서</dt>
											<dd>(2020.1.17~2022.1.16)</dd>
										</dl>
									</a>
								</li>
								<li>
									<a href="/sub/company/prize.asp" target="_blank">
										<img src="/hpdesktops/images/certi-03.png" />
										<dl>
											<dt>서울형 강소기업 선정</dt>
											<dd>(2018.8.10~2020.12.31)</dd>
										</dl>
									</a>
								</li>
								<li>
									<a href="/sub/company/prize.asp" target="_blank">
										<img src="/hpdesktops/images/certi-04.png" />
										<dl>
											<dt>한국서비스품질우수기업</dt>
											<dd>(2019.7.31~2022.7.30)</dd>
										</dl>
									</a>
								</li>
								<li>
									<a href="/sub/company/prize.asp" target="_blank">
										<img src="/hpdesktops/images/certi-05.png" />
										<dl>
											<dt>ISO 9001</dt>
											<dd>(2018.12.24~2021.12.23)
</dd>
										</dl>
									</a>
								</li>
							</ul>
				</div>

			</section>
			<!--인증현황-->



			<section id="footer">
				<div class="neo-inner">
					<div class="foo-btn">
						<a href="/srm/login.asp" target="_blank"><img src="/hpdesktops/images/footer-btn1.jpg" alt="전자구매시스템" /></a>
						<a href="https://939.co.kr/sinsungcns/" target="_blank"><img src="/hpdesktops/images/footer-btn2.jpg" alt="원격 기술지원" /></a>
					</div>
					<div class="foo-txt">
						<p>
							<span>08389 서울시 구로구 디지털로 272 한신 IT타워 5F</span>
							<span>Tel: 02-867-3007</span>
							<span>Fax: 02-867-2328</span>
							<span>Email: <a href="mailto:sklee@sinsungcns.com" target="_blank">sklee@sinsungcns.com</a></span>
						</p>
						<p>
							<span>사업자 등록번호: 119-86-18434</span>
							<span>통신판매업 신고번호 제 2016-서울구로-0842호</span>
							<span>개인정보책임자: 박세화 <a href="/sub/member/safeguard.asp?PHPSESSID=33cf009cb97b8fc97c7581dff61fbac1" target="_blank" class="foo-link">개인정보처리방침</a></span><br />
							홈페이지에 게제된 모든 글과 이미지 등의 정보는 저작권법에 따라 보호를 받는 ㈜신성씨앤에스의 저작물이므로 무단 전재나 복제를 금합니다. <br />COPYRIGHT (C)SINSUNGCNS CORP. ALL RIGHTS RESERVED.
						</p>
						<P>
							인텔, 인텔 로고, Intel Inside, 인텔 코어 및 Core Inside는 미국 및/또는 기타 국가에서 인텔 또는 그 자회사의 상표입니다.
						</P>
					</div>
				</div>
			</section>

		</div>

		<script>

		$(document).ready(function(){
			/* 모바일 메뉴 */
			$('.mo-menu-trigger').click(function(){
				$('.m-gnb-bg').toggleClass('active');
				$(this).toggleClass('active');
				$('.gnb').toggleClass('active');
				$('.tnb ul').toggleClass('active');
			})

			$('.m-gnb-bg').click(function(){
				$('.m-gnb-bg').removeClass('active');
				$('.mo-menu-trigger').removeClass('active');
				$('.gnb').removeClass('active');
				$('.tnb ul').removeClass('active');
			})

			$(window).resize(function(){

				winW = $(window).width()
				winH = $(window).height()

				/* 모바일 메뉴 */
				if(winW > 940){
					$('.gnb').css({
						height: 'auto'
					})

					$('.m-gnb-bg').css({
						height: 'auto'
					})

				}else if(winW < 960){

					$('.gnb').css({
						height: winH
					})

					$('.m-gnb-bg').css({
						height: winH
					})

				}else if(winW < 940){
					$('.gnb').click(function(){
						$('.m-gnb-bg').removeClass('active');
						$('.mo-menu-trigger').removeClass('active');
						$('.gnb').removeClass('active');
					})
				}

			}).resize()

			var naviOffset = $( '#header' ).offset();
			$( window ).scroll( function() {
				if ( $( document ).scrollTop() > naviOffset.top ) {
					$( '.header-area' ).addClass( 'header-fixed' );
				}else {
					$( '.header-area' ).removeClass( 'header-fixed' );
				}
			});

		})
		</script>


	</body>
</html>