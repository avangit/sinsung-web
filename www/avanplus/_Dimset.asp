<%
	'##########################################
	'//상품 경로설정
	'##########################################

	'// 각 함수들이 처리되는 실제 쇼핑몰의 물리적 경로설정
	'// 디자이너가 지정한 경로입니다.
	'// 메인페이지
	dim UserPath_home 		: UserPath_home = "/"

	dim UserPath_sub2		: UserPath_sub2 = "/sub.asp?"

	'// 상품리스트(카테고리) / 상세보기
	dim UserPath_Goods2 		: UserPath_Goods2 	= "/main/goods/view.asp?" 'shop
	'// 상품검색 / 상세보기
	dim UserPath_Search2 	: UserPath_Search2 	= "pagecode=m07s01" 'search
	'// 장바구니
	dim UserPath_cart2 		: UserPath_cart2 	= "pagecode=m03s01" 'cart
	'// 구매페이지
	dim UserPath_order2	 	: UserPath_order2 	= "pagecode=m08s01" 'order
	'// 마이페이지
	dim UserPath_mypage2 	: UserPath_mypage2 	= "pagecode=m06s02" 'mypage
	'// WishList
	dim UserPath_wish2 		: UserPath_wish2 	= "pagecode=m06s03" 'wishlist

	'// 포인트
	dim UserPath_point2 		: UserPath_point2 	= "pagecode=m06s04" 'point
	'// 로그인
	dim UserPath_review2 	: UserPath_review2 	= "pagecode=m05s08" '리뷰

	dim UserPath_qna2		: UserPath_qna2		= "pagecode=m05s09"

	'// 리뷰
	dim UserPath_login 		: UserPath_login 	= "pagecode=m06s01" '로그인

	dim adminOrder			: adminOrder 		= "/admin.asp"

	dim UserPath_contract	: UserPath_contract = "/sub/m05s07.asp"	'약관
	dim UserPath_private	: UserPath_private  = "/sub/m05s10.asp"	'개인보호

	'// 각 페이지에서 execute 시킬 파일 위치임

	'//쇼핑몰 첫 페이지 - 값이 없으면 기본값 사용
	dim url_shopMain 	: url_shopMain = ""

	'//상품상세보기 페이지 - 값이 없으면 기본값 사용
	dim url_GoodsView 	: url_GoodsView = ""

	'//상품검색상세보기 페이지 - 값이 없으면 기본값 사용
	dim url_SearchView 	: url_SearchView = ""

	'//wish list
	dim url_WishList 	: url_WishList = ""

	'//장바구니
	dim url_cartList 	: url_cartList = ""




	'##########################################
	'//업로드 상품 사이즈 설정 - 메세지 출력용
	'##########################################
	'// 큰이미지
	dim bimgW	: bimgW = 280
	dim bimgH	: bimgH = 280
	'// 중간이미지
	dim mimgW	: mimgW = ""
	dim mimgH	: mimgH = ""
	'// 작은이미지
	dim simgW	: simgW = 180
	dim simgH	: simgH = 240

	'dim DefaultGoodsSearch	'//상품검색어 기본글귀
	'DefaultGoodsSearch = ""

	'dim DefaultGoodsSpecial	'//특의사항 기본글귀
	'DefaultGoodsSpecial = ""

	'dim DefaultGoodsshort	'//간략설명 기본글귀
	'DefaultGoodsshort = ""




	'// 메인에 출력되는 옵션 명
	dim moption0	: moption0 = "베스트상품"
	dim moption1	: moption1 = "히트상품"
	dim moption2	: moption2 = "사용안함"
	dim moption3	: moption3 = "사용안함"
	dim moption4	: moption4 = "사용안함"
	dim moption5	: moption5 = "사용안함"
	dim moption6	: moption6 = "사용안함"
	dim moption7	: moption7 = "사용안함"
	dim moption8	: moption8 = "사용안함"
	dim moption9	: moption9 = "사용안함"

	'//
	Dim bank 		 : bank 		= "국민은행 625702-04-006308 예금주 : 오승희 | 기업은행 750000-00-000000 예금주 : (유)아반소프트  | 기업은행 750000-00-000000 예금주 : (유)아반소프트"

	'//배송비 제한 금액
	Dim besong_limit : besong_limit	= "50000"'
	'//배송비
	Dim besong_money : besong_money	= "2700"
	if session("userlevel") = 3 then besong_money	= "0"
	'//관리자 전화번호
	Dim admin_phone  : admin_phone	= "070-8230-9000"
	'//산간지역 배송비
	DIm Export_Money : Export_Money	= "6000"
	'//산간지역 배송비 제한 금액
	DIm Export_Limit : Export_Limit	= "15000"


	'//kcp 사용여부
	Dim UseKCP   			: UseKCP   = True
	Dim UseAccountKCP		: UseAccountKCP = False '//계좌이체 사용여부
	Dim cardEscroFlag		: cardEscroFlag = False '//카드결재 에스크로 사용여부
	Dim UseAccount			: UseAccount = True '//무통장입금 사용여부
	Dim UseVirtual			: UseVirtual = True '//가상계좌 사용여부


	Dim site_cd: site_cd 	= "K4199"
	Dim site_key: site_key	= "2iFzFRgrqY-LJQpxuTQAY4T__"
	Dim site_name : site_name = "RexGirl"

	'//적립금 사용여부
	Dim UsePoint   : UsePoint = True
	Dim pointLimit : pointLimit = 5000 '//적립금 사용할수 있는 금액
	Dim pointMember : pointMember = 1000 '//회원가입시 적립금
	Dim pointRecommender : pointRecommender = 0 '// 추천인한테는 1% 줌~
	Dim pointMemberRecommend : pointMemberRecommend = 2000 '//추천인 넣을시
	Dim pointCourse : pointCourse = 0.003 '// 즐겨찾기,네이버경로로 들어올경우 0.3%추가적립금
	Dim pointReview : pointReview = 200 '// 리뷰
	Dim pointPhotoReview : pointPhotoReview = 500 '//포토리뷰


	'//장바구니 견적서
	Dim strSangho 	: strSangho = "아반소프트"
	Dim strNeName 	: strNeName = "이덕권"
	Dim strNeAddr2 	: strNeAddr2 = ""
	Dim strNeAddr1  : strNeAddr1 = "전북 전주시 덕진구 우아동 2가 909-11 중앙빌딩 3층 "
	Dim strNePhone1 : strNePhone1 = "Tel. 070-8230-9000"
	Dim strNePhone2 : strNePhone2 = "Fax. 050)5566-3000"
	Dim strNeNumber : strNeNumber = "사업자등록번호 : 402-81-59458, 통신판매업신고 106호"

	'//방문아이디
	Dim countSiteId : countSiteId = "rexgirl"

	'//sms및메일
	Dim useSms : useSms = True
	Dim guest_no : guest_no = "035843" 'sms번호
	Dim guest_key : guest_key = "384c9e8790c7d28c74310f23cc148932"	 'sms키
	Dim s_Mail : s_Mail = "love@rexgirl.co.kr" '//관리자 메일
	Dim s_ToPhone : s_ToPhone  = "07082309000"


	Dim strBackUrlLink : strBackUrlLink = Server.URLEncode(Request.ServerVariables("PATH_INFO")&"?"& Request.ServerVariables("QUERY_STRING")& "" )


%>
