<!--#include file = "_header.asp" -->
 <%
	
	dim intseq
	'//검색처리부분
	'//일반과 검색을 위한 설정
	dim where, keyword, keyword_option

	keyword 		= requestS("keyword")
	keyword_option 	= requestS("keyword_option")
	
	'//폼으로 넘어온경우 백로가면 검색화면 페이지표시할수 없음처리
	if len(request.Form("keyword")) > 0 then response.Redirect("?"&GetString("keyword="&keyword&"&keyword_option="&keyword_option))
	

	'//검색일경우 첫페이지로 돌리기 위한 설정
	if len(keyword_option) > 0 then
		where = " intSeq > 0 and "& keyword_option &" like '%"& keyword &"%' "
	else
		where  = " intSeq > 0 "
	end if
	
	
	'//카테고리 값이 넘어오는 경우 처리
	if len(requestQ("mcate")) > 0 then	where = where & " and strCategory = '"& requestQ("mcate") &"' "
	
	
	
	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'## 
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'## 
	dim intTotalCount, intTotalPage
	
	dim intNowPage			: intNowPage 		= Request.QueryString("page")    
    dim intPageSize			: intPageSize 		= 10
    dim intBlockPage		: intBlockPage 		= 10

	dim query_filde			: query_filde		= "*"
	dim query_Tablename		: query_Tablename	= "manager"
	dim query_where			: query_where		= where
	dim query_orderby		: query_orderby		= " order by intSeq DESC "
	call intTotal
	
	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'## 
	'## 
	'#############################################################################

	dim sql, rs
	sql = GetQuery()
	'response.Write(sql)
	call dbopen
	set rs = dbconn.execute(sql)
	Dim UploadFolder
	UploadFolder= "/upload/manager/"
%> 
<!--메인 콘텐츠 시작-->
<div id="container">
<p class="sub_tit">홈 &gt; <span>맛집</span></p>
		<!--#include file = "_search.asp" -->

        
        
         <div id="content">
         <div class="list">
            <ul>
				<%
					dim pagei : pagei = (intTotalCount-MoveCount)
					'// 글이 없을 경우
					if  rs.eof then
				%>
				<li>등록된내용이 없습니다.</li>
				<%

					Else		

				%>
				<%	
					rs.move MoveCount 
					Do while not rs.eof												

				%>
                <li class="ml0">
                    <a href="view.asp?intseq=<%=rs("intseq")%>">
                    <span class="list_img"><%=ImgLmtView(UploadFolder,getFileNumName(rs("img1")),500) %></span>
                    <span class="list_tit"><%=rs("str_Title")%></span>
                    <span class="list_txt"><%=rs("str_category")%><span><%=rs("str_Area2")%></span></span>
                    <span class="call"><img src="image/call2.png" alt="call"/><%=rs("str_Tel")%></span>
                    </a>
                </li>
				<%
							pagei = pagei-1
						rs.movenext
						loop
						rs.close()
						set rs = nothing
					End If


					call DbClose()
				%>
            </ul>    
        </div> 
		<div align="center"><%call Paging("&keyword="&keyword&"&keyword_option="&keyword_option)%></div>          
		 </div><!-- content end-->
</div>  <!--  container end -->
<!--#include file = "_footer.asp" -->