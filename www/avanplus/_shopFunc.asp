<!--#include file="_DimSet.asp"-->
<%

	call dbopen

	'##########################################
	'//사용함수
	'##########################################
	'// 카테고리 타입확인
	if len(request.QueryString("category")) <> 9 and len(request.QueryString("category")) <> 10 and len(request.QueryString("category")) <> 0 then
		response.Write("카테고리 타입이 맞지 않습니다.")
		response.End()
	end if

	'dim catecode : catecode = "8854000000"

	'dim step
	'step = 4

	'Dim stepMax, stepMin
	'stepMax =  10^((5-step)*2)
	'stepMin =  stepMax - 1

	'dim step999Str, step000Str
	'step999Str = Cstr(stepMin)
	'step000Str = replace(stepMin,"9","0")
	Function CateCode()
		dim category
		category = request.QueryString("category")
		if len(category) = 0 then
			CateCode = 0000000000
		else
			CateCode = category
		end if
	End Function


	'// 현제 카테고리의 스텝을 반환
	Function getCateStep(byval category)
		dim CateStep
		CateStep = Cdbl(category)/10000000000
		CateStep = replace(CateStep,".","")
		CateStep = int((Len(CateStep))/2)
		'if CateStep = 0 then CateStep = 1
		if CateStep > 5 then CateStep = 5
		getCateStep = CateStep

	End Function

	'// 해당 코드값의 초대/최소등 원하는 값 리턴
	Function getCateValuse(byval category, byval str)
		dim CateAdd
		CateAdd = 10^(2*(4-getCateStep(category)))
		if int(category) = 0 then CateAdd = 100000000

		dim CateMin
		CateMin = category

		dim CateMax
		CateMax = category + ( (CateAdd*100) - 1 )
		if getCateStep(category) = 0 then CateMax = 9999999999


		if Lcase(str) = "add" then
			getCateValuse = CateAdd
		elseif Lcase(str) = "max" then
			getCateValuse = CateMax
		elseif Lcase(str) = "min" then
			getCateValuse = CateMin
		end if
	End Function

	'// 현재 카테고리의 스텝
	'response.Write("스텝:"&getCateStep(category)&"<br>")
	'// 현재 카테고리의 추가값(인서트시 플러스 값)
	'response.Write("추가값:"&getCateValuse(category,"add")&"<br>")
	'// 현재 카테로기 최소값(카테고리 자신)
	'response.Write("최소값:"&getCateValuse(category,"min")&"<br>")
	'// 현재 카테고리 최대값
	'response.Write("최대값:"&getCateValuse(category,"max")&"<br>")



	'//넘어온 카테고리의 각 스텝의 코드값리턴
	Function upCodeReturn(byval step, byval category)
		upCodeReturn = int( category/(10^((5-step)*2)) ) * (10^((5-step)*2))
		if category = 0 then upCodeReturn = "0000000000"
	End Function

	'response.Write("upCodeReturn:"&upCodeReturn(1,category)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(2,category)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(3,category)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(4,category)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(5,catecode)&"<br>")


	Function CateNextWhere()	'//넘어온 카테고리값의 하위카테고리 생성시 처음서 끝까지
		'// 1122000000 이 넘어오면
		'// 1122       앞부분인 와
				'//       0000 뒷부분이 인 값 알아오기
		dim strF, strR
		dim strS, strE

		strF	= mid(CateCode(),1,getCateStep(catecode)*2)
		if len(strF)>0 then strF = int(strF)

		strR	= 10^((4-getCateStep(catecode))*2)
		strR	= replace(Cstr(strR),"1","")
		strR	= replace(Cstr(strR),"0.0","")

		CateNextWhere = " c_code like '"& strF &"%' and c_code like '%"& strR &"' and c_code > '"& upCodeReturn(getCateStep(catecode),catecode) &"' "
	End Function



	Function CateThisWhere(byval num, byval category)	'//해당스텝의 카테고리 목록 가져오기 쿼리
		'// 1122000000 이 넘어오면
		'// 1122       앞부분인 와
		'//       0000 뒷부분이 인 값 알아오기

		'//	 122000000 이면
		'//  122
		'//       0000 값 알아오기

		dim strF, strR
		dim strS, strE
		dim strnum
		'// 카테고리 맨 앞자리가 2자리수 미만인 경우의 처리

		if int(category) = 0 then
			strnum = 0	'//카테고리 값이 0인경우
		elseif int(category) < 1000000000 then
			strnum = (num*2)-1
			if strnum < 0 then strnum = 0 '//계산식이 -1인경우는 0으로 대체
			'response.Write(strnum)
		else
			strnum = num*2
		end if

		strF	= left(category,strnum)

		if len(strF)>0 then strF = int(strF)

		strR	= 10^((4-num)*2)
		strR	= replace(Cstr(strR),"1","")
		strR	= replace(Cstr(strR),"0.0","")

		'CateThisWhere = " c_code like '"& strF &"%' and c_code like '%"& strR &"' and c_code > "& upCodeReturn(num,category) &" "
		'//썬 수정 (INT 오버플로에러) INT -2^31(-2,147,483,648)에서 2^31 - 1(2,147,483,647)
		'//CateThisWhere = " c_code like '"& strF &"%' and c_code like '%"& strR &"' and c_code > 0"& upCodeReturn(num,category) &""
		CateThisWhere = " c_code like '"& strF &"%' and c_code like '%"& strR &"' and c_code > cast(0"& upCodeReturn(num,category) &" as bigint) "
	End Function



	'Function CateAllWhere()	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식
		'CateAllWhere = " Goods_cate_join.c_code >= "& int(getCateValuse(catecode,"min")) & " and Goods_cate_join.c_code <= "& int(getCateValuse(catecode,"max"))
	'End Function

	'
	'Function CateAllWhere(byval category)	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식 '//2개 테이블
	'	CateAllWhere = " Goods_cate_join.c_code >= "& int(getCateValuse(category,"min")) & " and Goods_cate_join.c_code <= "& int(getCateValuse(category,"max"))
	'End Function

	Function CateAllWhere(byval category)	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식 '//2개 테이블
		CateAllWhere = " Goods_cate_join.c_code >= '"& int(getCateValuse(category,"min")) & "' and Goods_cate_join.c_code <= '"& int(getCateValuse(category,"max"))  &"'"
	End Function

	Function CateAllWhere1(byval category)	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식	'//1개 테이블
		CateAllWhere1 = " c_code >= "& int(getCateValuse(category,"min")) & " and c_code <= cast("& (getCateValuse(category,"max") &" as bigint)" )
	End Function

	Function CateAllWhere2(byval category)	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식 '//2개 테이블
		CateAllWhere2 = " Goods2_cate_join.c_code >= '"& int(getCateValuse(category,"min")) & "' and Goods2_cate_join.c_code <= '"& int(getCateValuse(category,"max"))  &"'"
	End Function

'// 스트링 값을 토대로 카테고리 리스트 출력
'// > 컴퓨터 > 자동차 > 부품
Function getCateColPrint(byval category, ByVal tablename)
	dim TempcateStep, i, sql_func, rs_func
	TempcateStep = getCateStep(category)
	'response.Write getCateStep(category)
	if getCateStep(category) > 5 then TempcateStep = 5
	i = 0
	for i = 0 to TempcateStep

		sql_func = "select * from " & tablename & " where c_code = '"& upCodeReturn(i, category ) &"' "
			'response.Write(sql_func)
		set rs_func = dbconn.execute(sql_func)

			if rs_func.eof then

			else
				do while not rs_func.eof
					getCateColPrint = getCateColPrint & " > "&rs_func("c_name")
				rs_func.movenext
				loop
			end if

	next
End Function





Sub getCateList(byval g_idx)

	'// 해당 상품과 연관된 카테고리
	'// 카테고리를 클릭하면 해당 카테고리 리스트로 이동

	'call dbopen

	dim rs_func, sql_func, UserPath_sub, UserPath_Goods

	sql_func = "select * from GOODS_CATE_JOIN where g_idx = '"& g_idx &"' Order by gcj_idx desc"

	set rs_func = dbconn.execute(sql_func)

		if rs_func.eof then

		else

			Do while not rs_func.eof


				response.Write "<a href='"&UserPath_sub& UserPath_Goods & "?"&getString("category=" & rs_func("c_code") )&"';>"
				'response.Write rs("c_code")

				response.Write "홈"&getCateColPrint(rs_func("c_code"))
				response.Write "</a>"
				response.Write "<BR>"

			rs_func.movenext
			loop

		end if

	rs_func.close
	set rs_func = nothing


	'call dbclose


End Sub



Function getThisNaviname(byval category, byval catestep)
'// 넘어온 카테고리 스텝의 이름 리턴
	dim strR
	strR = left(category,2*catestep)
	'dim sql : sql = "select top 1 c_name from Category where "& CateThisWhere(catestep, category) &" order by c_code asc"
	dim sql : sql = "select top 1 c_name from Category where c_code like '"& strR &"%'  order by c_code asc"

	'response.Write(sql)
	getThisNaviname = getAdoRsScalar(sql)
End Function





'// 셀렉트 리스트 박스로 이어지는 카테고리 선택기
sub navi_select(byval category, ByVal tablename)

	dim i, sql_func, rs_func, temp_selected

%>
<script type="text/JavaScript">
		<!--
			function MM_jumpMenu(targ,selObj,restore){ //v3.0
			  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
			  if (restore) selObj.selectedIndex=0;
		}
		//-->
	</script>
<%
		dim TempcateStep '// 4이상인 경우 무시 4단계때 5단계를 생성하기 때문
		TempcateStep = getCateStep(category)
		if getCateStep(category) > 4 then TempcateStep = 4
		i = 0
		for i = 0 to TempcateStep
			sql_func = "select * from " & tablename & " where "& CateThisWhere(i,request.QueryString("category")) &" order by c_sunse asc "
				'response.Write(sql_func)
			set rs_func = dbconn.execute(sql_func)
%>

					<form name="getCateSelect" id="getCateSelect">


							<%
							if rs_func.eof then
							else
							%>
								<label class="select col-lg-2" >
									<select class="input-sm h34" name="getCateSelectMenu" onchange="MM_jumpMenu('parent',this,0)"  >
										<option value="?<%=getString("")%>"><%=i+1%>차 카테고리 선택</option>
										<%
										do while not rs_func.eof

										'// 각 단계의 값을 리턴(넘어온 카테코드와 디비의 코드가 같으면 셀렉티드)
										temp_selected = chk_selected( upCodeReturn(i+1, category ), upCodeReturn(i+1, rs_func("c_code") ) )
										%>
											<option value="?<%=getString("category="&rs_func("c_code"))%>" <%=temp_selected%>><%=rs_func("c_name")%></option>
										<%
										rs_func.movenext
										loop
										%>
									</select> <i></i>
								</label>


							<%
							end if
							%>

					</form>

		<%
		next
		%>

<% End sub %>





<%

	Sub AUTOcatePage()
		'// 해당 카테고리 페이지에 맞는 페이지로 이동

		dim no, tempName
		no = getCateStep(cateCode)
		'response.write(no&"<<<<<<<<<<<<<")

		tempName = request.ServerVariables("PATH_INFO")


		if  no = 0 then
			'response.Write "카테고리가 없습니다."
		elseif no = 1 and ( lcase(tempName) <> lcase(url_step1) ) then
			response.Redirect(url_step1&"?"&getString(""))
		elseif no = 2 and ( lcase(tempName) <> lcase(url_step2) ) then
			response.Redirect(url_step2&"?"&getString(""))
		elseif no = 3 and ( lcase(tempName) <> lcase(url_step3) ) then
			response.Redirect(url_step3&"?"&getString(""))
		elseif no = 4 and ( lcase(tempName) <> lcase(url_step4) ) then
			response.Redirect(url_step4&"?"&getString(""))
		elseif no = 5 and ( lcase(tempName) <> lcase(url_step5) ) then
			response.Redirect(url_step5&"?"&getString(""))
		end if

	End Sub


%>




<%
'// 카테고리1   : 카테고리2 | 카테고리2 | 카테고리 2
'// 카테고리1_1 : 카테고리2_1 | 카테고리2_1 | 카테고리2_1  call navi_2_3stpe(2,0)
Sub navi_2_3stpe(byval step, byval viewOption)
	'//step 카테고리 스텝이 어디부터 인지? ex. 1 인경우는 1은 세로, 2는 가로에 출력
	'//viewOption	카테고리 보기 세로가 1단인지 전체인지 // 갑이 1이면 1단 0 이면 모두

	'// 카테고리 코드 확인
	if len(request.QueryString("category")) = 0 then
		response.Write("카테고리 값이 없습니다.")
		response.end
	elseif int(request.QueryString("category")) = 0 then
		response.Write("카테고리가 0입니다. 정상적인 카테고리 값을 보내세요.")
		response.end
	end if



	dim sql, rs, i, top, sql2, rs2
	i = ( step - 1 )
		'for i = 0 to TempcateStep
		if viewOption = 1 then top = " top 1 "
		sql = "select "& top &" * from Category where "& CateThisWhere(i,request.QueryString("category")) &" order by c_sunse asc "
		'	response.Write(sql)
		set rs = dbconn.execute(sql)
%>
<table class="cate_table">
<%
	if not rs.eof then
		Do while not rs.eof
%>
  <tr>
    <th><!--<img src="/avanmall/img/bull_nav.gif" >--><a href="?<%=getString("category="&rs("c_code"))%>"><b><%=rs("c_name")%> <span>(<%=getGoodsCount(rs("c_code"))%>)</span></a></th>
    <td>
		<%
			sql2 = "select * from Category where "& CateThisWhere(i+1,rs("c_code")) &" order by c_sunse asc "
				set rs2 = dbconn.execute(sql2)
				'response.Write(rs("c_code"))
				'response.Write(" <font color=#D9D9D9>|</font> ")
					if not rs2.eof then
						Do while not rs2.eof

							response.Write("<a href=?"& getString("category="&rs2("c_code")) & ">" & rs2("c_name") &"("& getGoodsCount(rs2("c_code")) &")"& "</a> ")

						rs2.movenext
						loop
					end if
				rs2.close
				set rs2 = nothing
		%>
	</td>
  </tr>
<%
		rs.movenext
		loop
	end if

	rs.close
	set rs = nothing

%>
</table>
<% End sub %>





<%
	'//카테고리 하위 상품 코드를 리턴
	Function getGoodsCount(Byval category)
		dim rs
		set rs = dbconn.execute("select count(*) from Goods, Goods_cate_join where goods.g_idx = Goods_cate_join.g_idx and g_act = 1 and "& CateAllWhere(category) )
		getGoodsCount = rs(0)
		rs.close
		set rs = nothing
	End Function

%>








<%
Sub navi_row2(byval category)
	'// 상위 줄 출력
	'// 홈 > 두번째 > 세번째
%>

<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
	<td bgcolor="#F2F2F2"><%
		dim TempcateStep, i, sql, rs
		TempcateStep = getCateStep(category)
		'response.Write getCateStep(category)
		if getCateStep(category) > 5 then TempcateStep = 5
		response.Write("<a href=?><b>홈</b></a> ")

		i = 0
		for i = 0 to TempcateStep

			sql = "select * from Category where c_code = '"& upCodeReturn(i, category ) &"' "

			set rs = dbconn.execute(sql)

				if rs.eof then

				else
					do while not rs.eof
						response.Write" > <a href=?"& getString("category="&rs("c_code")) & "><b>" & rs("c_name") & "</b></a> "
						'response.Write(">"&rs("c_name"))
					rs.movenext
					loop
				end if

		next

	%></td>
  </tr>

<%
	'// 상단 카테고리 이하 메뉴출력


	dim sql2, rs2, rs3
	i = getCateStep(CateCode()) - 1
	sql2 = "select * from Category where "& CateThisWhere(i+1,CateCode()) &" order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else
			%>
  <tr>
	<td height="120" bgcolor="#FFFFFF">
			<%
				Do while not rs2.eof

					response.Write "<a href=?"& getString("category="&rs2("c_code")) & ">" & rs2("c_name")

					'// 하위 상품 갯수 나타내는 식
					response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝

					response.Write "</a> <font color=#D9D9D9>|</font> "

				rs2.movenext
				loop
			%>


	</td>
  </tr>
			<%
			end if
		rs2.close
		set rs2 = nothing
%>


</table>
<%
End Sub
%>






<%
	'// 한줄 카테고리 이하 메뉴출력
	'| 하하하 | 호호호 | 히히히 | 케케케| <-이런 형식으로 출력
sub colNaviProduct(byval cateStep)
	dim sql2, rs2, rs3, i, bb, bb2

	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if

	sql2 = "select * from Category where "& CateThisWhere(i+1,CateCode()) &" order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		'response.Write(" <font>|</font> ")
			if rs2.eof then

			else

				Do while not rs2.eof

					bb = ""
					bb2= ""

					if int(CateCode()) = int(rs2("c_code")) then bb = "<b>"
					if int(CateCode()) = int(rs2("c_code")) then bb2 = "</b>"

					response.Write "<a class='cate_list' href=?"& getString("category="&rs2("c_code")) & ">"& bb & rs2("c_name")
					'// 하위 상품 갯수 나타내는 식
					response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝
					response.Write bb2 & "</a> "

				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub
%>


<%
	'// 한줄 카테고리 이하 메뉴출력
	'111
	'222
	'333
	'444
	'이런식으로 세로 출력
sub rowNavi(byval cateStep)
	dim sql2, rs2, rs3, i, bb, bb2

	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if

	sql2 = "select * from Category where "& CateThisWhere(i+1,CateCode()) &" order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		'response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else

				Do while not rs2.eof

					bb = ""
					bb2= ""


					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb = "<b>"
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb2 = "</b>"

					response.Write "<a href=?"& getString("category="&rs2("c_code")) & ">"& bb & rs2("c_name")
					'// 하위 상품 갯수 나타내는 식
					'response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝
					response.Write bb2 & "</a><br>"' <font color=#D9D9D9>|</font> "

				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub
%>


<%
	'// 해당 카테고리 이름출력

sub ThisNavi(byval step, byval category)
	dim sql2, rs2, rs3, i, bb, bb2


	sql2 = "select * from Category where c_code = "& upCodeReturn(step,category) &" order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
			if rs2.eof then

			else

				Do while not rs2.eof

					response.Write "<a href=?"& getString("category="&rs2("c_code")) & ">"& bb & rs2("c_name")

					'// 하위 상품 갯수 나타내는 식
					response.Write getGoodsCount(rs2("c_code"))
					'//-- 하위 갯수 끝

					response.Write bb2 & "</a> <font color=#D9D9D9>|</font> "

				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub
%>


<%
	'// 해당 depth의 카테고리 이름만 출력

sub ThisNaviName(byval step, byval category)
	dim sql2, rs2, rs3, i, bb, bb2


	sql2 = "select * from Category where c_code = "& upCodeReturn(step,category) &" order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
			if rs2.eof then

			else

				Do while not rs2.eof
					response.Write rs2("c_name")
				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub
%>







<%

	'// 돈을 집어넣으면 \100,000형식으로 리턴
	Function FormatW(byval money)
		if len(money) > 0 then
			money = replace(money,",","")
			money = replace(money,"\","")

			money = FormatCurrency(money)
			'FormatW = replace(money,"\","")
			FormatW = mid(money,2,20)
			'FormatW = replace(money,chr(92),"")
		end if
	End Function

	'// 돈을 집어넣으면 100000인트형으로 리턴
	Function FormatINT(byval money)
		if len(money) > 0 then
			'money = mid(money,2,20)
			money = replace(money,",","")
			'money = replace(money,"\","")

			FormatINT = money
		end if

		if len(FormatINT) = 0 then FormatINT = 0
	End Function


	'// 소비자가격과 판배가를 받아 표현
	'// 변경 문자열이 있는경우는 문자열로 표현

	Function MoneyView(byval customerMoney, byval Money, byval changString, byval changOption)

		if len(customerMoney) > 0 then	customerMoney = FormatW(customerMoney)
		if len(Money) > 0 then			Money = FormatW(Money)

		if changOption then
			MoneyView = "<font color=red>"&changString&"</font>"
		else
			'// 각 값이 있고 0보다 큰경우
'			if len(customerMoney) > 0 	and customerMoney > 0 	then MoneyView = "<s><font color=silver>"&customerMoney&"</font></s> "
			if len(Money) > 0 			and Money > 0 			then MoneyView = MoneyView &	" <span id = 'GoodMoney'>"&Money&"</span>"
		end if

		'// 판매가격이 정해지지 않은경우
		if Money = 0 then MoneyView = "<font color=db6f03>금액미정</font>"

	End Function


	Function MoneyView2(byval customerMoney, byval Money, byval changString, byval changOption)

		if len(customerMoney) > 0 then	customerMoney = FormatW(customerMoney)
		if len(Money) > 0 then			Money = FormatW(Money)

		if changOption = 1 then
			MoneyView2 = changString
		else
			MoneyView2 = "<span id = 'GoodMoney'>"&Money&"</span>"
		end if

		if Money = 0 then MoneyView2 = "<span id = 'GoodMoney'>금액미정</span>"

	End Function



	'// 아이콘 보이기
	Function iconView(byval DBstr)
		dim temp, tempstr, i

		if len(trim(DBstr)) > 0 then	'// 값이 있는경우만 처리
			temp = split(DBstr,",")

			for i = 0 to Ubound(temp)
				if len(trim(temp(i))) > 0 then
					tempstr = tempstr & " <img src=/admin/icon/" &trim(temp(i))& ".gif> "
				end if
			next
		end if
		iconView = tempstr
	End Function


'// ######################################
'// 상품리스트 뿌리는 함수
'// ######################################


Sub ProductList(byval category, byval picW, byval picH, byval color, byval talbeW, byval raws, byval cols, byval optionN, byval optionP, byval optionI, byval TempWhere, byval TempOrderby, byval TempPaging, byval mainimgOption)

'category	-카테고리
'picW		-사진가로사이즈
'picH		-사진세로사이즈
'color		-테두리컬러, 없으면 테두리 없음
'talbeW		-전체테이블 가로 사이즈
'raws		-가로 노출 상품수
'cols 		-세로 줄수
'optionN	-상품명 보이기 1 , 보이기 2 , 안보이기 0
'optionP	-상품가격 보이기 1 , 안보이기 0
'optionI	-상품아이콘 보이기 1 , 안보이기 0
'TempWhere	-where 조건
'TempOrderby	-정렬조건
'TempPaging		-페이징 보이기1, 안보이기0
'mainimgOption		-메인이미지 보이기 옵션







	dim tempColor
	'// 테두리 컬러 효과(있는경우와 없는경우)
	dim tempStr

	if len(color) > 0 then
		tempColor	= "cellpadding=2 cellspacing=1 bgcolor="&color
	else
		tempColor	= ""
	end if



	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'##
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'##
	dim intTotalCount, intTotalPage

	dim intNowPage			: intNowPage 		= Request.QueryString("page")
	dim intPageSize			: intPageSize 		= raws * cols
	dim intBlockPage		: intBlockPage 		= 10





	dim g_idx
	dim temTable, temWhere, imgpath

	'// 기본조건지정
	'// 진열상품만 나열
	temWhere = " g_act = 1 "


	'// where조건이 있는경우_함수호출시 where조건을 받는경우
	if len(trim(tempwhere)) > 0 then temWhere = temWhere & " and " & tempwhere



	if int(catecode) > 0 then
		'//카테고리 하위 조인식
		temWhere = temWhere & " and " & CateAllWhere(catecode) & " and  goods.g_idx = Goods_cate_join.g_idx "
		temTable = " Goods,Goods_cate_join "
	elseif int(category) > 0 then
		temWhere = temWhere & " and " & CateAllWhere(category) & " and  goods.g_idx = Goods_cate_join.g_idx "
		temTable = " Goods,Goods_cate_join "
	else
		temTable = " Goods "
	end if


	'//+++++++++++++++++++++++++++
	'// 조건절
	'dim temp
	dim temp : temp = request.QueryString("temp")
	if len(temp) = 0 then temp = TempOrderby

	if lcase(temp) = "new" then
		temp = " Order by g_insertDay Desc,goods.g_idx desc "
	elseif lcase(temp) = "low" then
		temp = " Order by g_Money Asc,goods.g_idx desc "
	elseif lcase(temp) = "high" then
		temp = " Order by g_Money Desc,goods.g_idx desc "
	end if


	dim query_filde			: query_filde		= " * "
	dim query_Tablename		: query_Tablename	= temTable
	dim query_where			: query_where		= temWhere
	dim query_orderby		: query_orderby		= temp
	'call intTotal

	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'##
	'##
	'#############################################################################


	%><Object Runat=Server PROGID=ADODB.Connection ID=FunctionDB></Object><%
		'///디비커넥션 파일의 디비스트링 사용
		'//DBstring = "provider=sqloledb;data source="& DBaddr &" ;initial catalog="& DBname &" ;uid="& DBUserID &"; pwd="& DBUserPW &""



	'Sub intTotal
	'## 조건에 만족하는 카운트 반환
	'// 디비가 연결된 상태이어야함-- 사용자 DB사용 userDB
		If Len(intNowPage) = 0 Then
		intNowPage = 1
		End If


		dim strsql, objRs
		strSQL = "Select Count(*)"
		strSQL = strSQL & ",CEILING(CAST(Count(*) AS FLOAT)/" & intPageSize & ")"
		strSQL = strSQL & " from " & query_Tablename
		if len(query_where) > 0 then
		strSQL = strSQL & " where " & query_where
		end if


		FunctionDB.Open cstConnString
		set objRs = FunctionDB.execute(strsql)


			intTotalCount = objRs(0)
			intTotalPage = objRs(1)

		objRs.close
		set objRs = nothing
		FunctionDB.close
	'End Sub

	dim TopCount
	'## 페이징에서 조건에 맞는 최대 행의수 리턴
		TopCount = "Top " & intNowPage * intPageSize


	dim MoveCount
	'## 설정된 변수값을 토대로 한 현재페이지 레코드 값 반환
		MoveCount = (intNowPage - 1) * intPageSize


	dim NavCount
	'## 전체글 과 현재 페이지 보기
		NavCount = "전체 "&intTotalCount&"개&nbsp;&nbsp;&nbsp;&nbsp;현재페이지 "&intNowPage&"/"&intTotalPage&""


	dim ShopNavCount
	'## 쇼핑몰용 네비게이션
		ShopNavCount = "현재 조건에 맞는 상품이 <font color=red><b>["&intTotalCount&"]</b></font>개 준비되어 있습니다.&nbsp;&nbsp;&nbsp;&nbsp;현재페이지 "&intNowPage&"/"&intTotalPage&""




	dim   sql, rs
	'sql = GetQuery
	'response.Write(sql)
	'Function GetQuery()

	if trim(query_where) <>"" then
		sql = "select distinct  "& TopCount & " " & query_filde &" from "& query_tablename & " where "& query_where & " " & query_orderby
	else
		sql = "select distinct  "& TopCount & " " & query_filde &" from "& query_tablename  & " " & query_orderby
	end if
	'End Function
	'#############################################################################

	'response.Write(sql)
	set rs = dbconn.execute(sql)

%>

  <% if TempPaging = 1 then %>
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	  <td>
		<a href="?<%=getString("")%>temp=new"><img src="/shopimg/arrange_bt_new<%=rw_o("new")%>.gif" border="0" /></a>
		<a href="?<%=getString("")%>temp=low"><img src="/shopimg/arrange_bt_low<%=rw_o("low")%>.gif"  border="0" /></a>
		<a href="?<%=getString("")%>temp=high"><img src="/shopimg/arrange_bt_high<%=rw_o("high")%>.gif"  border="0" /></a>
	  </td>
	  <td><div align="right">
		<%  call PagingFun("", intNowPage, intPageSize, intTotalPage) %>
	  </div></td>
	</tr>
  </table>
<% end if %>
  <table width="<%=talbeW%>" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
	  <%
		'// 루프시작
		dim i, tempi
		if rs.eof then

		else

			rs.move MoveCount
			Do While not rs.eof
	  %>
	  <td width="<%=picW+2%>" valign="top">
	  <!--작음상품-->
	  <table width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top">

			  <!--사진테두리-->
			  <%
				'// 이미지 사용
				Dim arrShopImgSize

				if len(rs("g_simg")) > 0 then
					arrShopImgSize = getImgSize("/upload/"&getFileNumName(rs("g_simg")), picW, picH)
					imgpath = "/upload/"&getFileNumName(rs("g_simg"))
				elseif len(rs("g_bimg1")) > 0 then
					'RESPONSE.WRITE "DFASDf"&picW&picH
					arrShopImgSize = getImgSize("/upload/"&getFileNumName(rs("g_bimg1")), picW, picH)
					imgpath = "/upload/"&getFileNumName(rs("g_bimg1"))
					'RESPONSE.WRITE arrShopImgSize(1)
				else
					arrShopImgSize = getImgSize("/AVANshop_v3/goods/noimg.gif", picW, picH)
					imgpath = "/AVANshop_v3/goods/noimg.gif"
				end if

				'//메인이미지 사용 옵션인경우 처리
				if mainimgOption = "1" and len(rs("g_mainimg")) > 0 then imgpath = "/upload/"&getFileNumName(rs("g_mainimg"))


				'//메인에서 링크를 거는경우는 카테고리 값이 없기에 코드를 생성한다.
				'// 사진 테두리 값이 있는경우와 없는경우
			  %>
			  <table width="<%=picW%>" height="<%=picH%>" border="0" <%=tempColor%>>
				<tr>
				  <td align="center" bgcolor="#FFFFFF"><a href="<%=UserPath_Goods2%><%=getString(getTempStr(requestQ("category"),rs("g_idx")))%>"><img src="<%=imgpath%>" width="<%=arrShopImgSize(1)%>" height="<%=arrShopImgSize(0)%>" border="0" /></a></td>
				</tr>
			  </table>
			  <!--/사진테두리-->

			</td>
		  </tr>
		  <tr>
			<td height="5" valign="top"></td>
		  </tr>
		  <tr>
			<td align="center" valign="top">
				<%










					'optionN	-상품명 보이기 1 , 안보이기 0
					'optionP	-상품가격 보이기 1 , 안보이기 0
					'optionI

					'response.Write rs("g_Money")
					if optionN = 1 then
						'재고량 부족하면 바로 화면에 표시
						If rs("g_use_totalChk")  =  True Then
						Else
						 if rs("total_op") = 0  then
						  response.Write("<font color=red><b>[품절]</b></font>")
						 end if
						End If

						response.Write "<span id = 'goodFont'>"&rs("g_name")&"</span><br>"
					end if
					if optionP = 1 then response.Write MoneyView(rs("g_customerMoney"), rs("g_Money"), rs("g_moneyMemo"), rs("g_moneyMemoOption"))&"<br>"
					if optionP = 2 then response.Write MoneyView2(rs("g_customerMoney"), rs("g_Money"), rs("g_moneyMemo"), rs("g_moneyMemoOption"))&"<br>"
					if optionI = 1 then response.Write iconView(rs("g_icon")) &"<br>"
				%>
			</td>
		  </tr>
	  </table>
	  <!--/작은상품-->	  </td>
	  <%
		'// i

			if tempi = ( cols - 1 ) then
				tempi = tempi - ( cols - 1 )
	  %>
	</tr><tr>
	  <%
			else
				tempi = tempi + 1
	  %>
	  <td>&nbsp;</td>
	  <%
			end if

			rs.movenext
			loop
		end if
	  %>



	<%
	'//빈공백 처리
	'// 4개 출력인경우 3개이면 1개 추가
	IF tempi > 0 THEN
		dim temp2
		for temp2 = tempi  to cols - 1
			response.Write	("<td width="&picW+2&" valign=top>")
			%>

			<table width="<%=picW%>" height="<%=picH%>" border="0" <%'=tempColor%>>
			   <tr>
				  <td align="center" bgcolor="#FFFFFF">
					<!--
					<img src="/upload/noimg.gif%>" width="<%=picW%>" height="<%=picH%>" border="0" />
					-->
				  </td>
			   </tr>
			</table>

			<%
			response.Write	("</td>")
	'response.Write temp2
			if temp2 = cols - 1 then
			else
			'response.Write "77//"
			'response.Write temp2
			'response.Write cols - 1
			response.Write	("<td>&nbsp;</td>")
			end if

		next
		'// 빈공백 처리 끝
	END IF
	%>
	</tr>

  </table>
  <% if TempPaging = 1 then %>
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	  <td>
		<a href="?<%=getString("")%>temp=new"><img src="/shopimg/arrange_bt_new<%=rw_o("new")%>.gif" border="0" /></a>
		<a href="?<%=getString("")%>temp=low"><img src="/shopimg/arrange_bt_low<%=rw_o("low")%>.gif"  border="0" /></a>
		<a href="?<%=getString("")%>temp=high"><img src="/shopimg/arrange_bt_high<%=rw_o("high")%>.gif"  border="0" /></a>
	  </td>
	  <td><div align="right">
		<%  call PagingFun("", intNowPage, intPageSize, intTotalPage) %>
	  </div></td>
	</tr>
  </table>
  <%
	end if

	rs.close
	set rs = nothing
End Sub
Function rw_o(byval strB)
	dim strA
	strA = trim(request.QueryString("temp"))
	if len(strA) = 0 then strA = "new"
	strB = trim(strB)

	if lcase(strA) = lcase(strB) then rw_o = "_o"
End Function

%>






<%
	'//***************************************
	'// 카테고리 상단 이미지 처리
	'//***************************************

	Sub cateVisualimg(byval DBimg)

		dim filepath, fileExe
		filepath = getFileNumName(DBimg)
		'fileExe
		if len(filepath) > 0 then '//파일명 있으면

			if ubound(split(filepath,".")) > 0 then
				fileExe = split(filepath,".")(1)

				'// 파일 확장자로 해당 파일 처리
				if lcase(fileExe) = "gif" or lcase(fileExe) = "jpg" then
					response.Write "<img src=/upload/"& filepath &">"
				elseif lcase(fileExe) = "swf" then
					%>
					<script language="javascript" src = "/AVANshop_v3/upswf.js"--></script>
					<script language="javascript">FlashMainbody('<%=filepath%>',282,598);</script>
					<%
				end if

			end if

		end if

	End Sub
%>






<%

	'// 상품보기 함수
	'// 상품 보기 모드인경우 상품보기로 처리하고 그 이하 처리 멈춤

	'// 경로가 없으면 기본 경로 사용
	'// 상품 리스트 상단에 정의 되어야 합니다.

	FUNCTION exeGoodView()	'//view파일 경로

		exeGoodView = 0

		if lcase(request.QueryString("temp")) = "view" then
			if len(url_GoodsView) = 0 or isnull(url_GoodsView) then	url_GoodsView = "/avanmodule/productlist/w/view.asp"


			dim objScrFso
			Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
				If objScrFso.FileExists(Server.MapPath(url_GoodsView)) Then
					Server.Execute(url_GoodsView)
				Else
					response.Write(url_GoodsView)
					Response.Write " 파일이 존재하지 않습니다. 상품 상세보기 처리 파일의 경로를 확인해 주세요."
				End If

			Set objScrFso = Nothing

			exeGoodView = 1
		end if
		'// 상품 보기 끝
	End FUNCTION




	'//***************************************
	'// 쇼핑몰 상품 보기 리스트
	'//***************************************


	Sub GoodList(byval category)

		'// 상품보기인경우는 리스트 실행하지 않음
		IF exeGoodView = 0 THEN
		'ELSE

			dim url, img
			url 	= getAdoRsScalar("select c_exepath from CATEGORY where c_code = '"& category &"'")
			img 	= getAdoRsScalar("select c_img from CATEGORY where c_code = '"& category &"'")

			'// 경로가 없으면 기본 경로 사용
'			if len(url) = 0 or isnull(url) then	url = "/goods/step"& getCateStep(category) &".asp"
			if len(url) = 0 or isnull(url) then	url = "/avanmodule/productlist/w/step"& getCateStep(category) &".asp"
			if len(img) > 0  then call cateVisualimg(img)

			dim objScrFso
			Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
				If objScrFso.FileExists(Server.MapPath(url)) Then
					Server.Execute(url)
				Else
					response.Write(url)
					Response.Write " 파일이 존재하지 않습니다. 상품 리스트 처리 파일의 경로를 확인해 주세요."
				End If

			Set objScrFso = Nothing
		END IF
	End Sub





	'//***************************************
	'// 검색리스트
	'//***************************************

	Sub exeSearchView()	'//검색view파일 경로

		if len(url_SearchView) = 0 or isnull(url_SearchView) then	url_SearchView = "/default/search.asp"


		dim objScrFso
		Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
			If objScrFso.FileExists(Server.MapPath(url_SearchView)) Then
				Server.Execute(url_SearchView)
			Else
				response.Write(url_SearchView)
				Response.Write " 파일이 존재하지 않습니다. 검색 처리 파일의 경로를 확인해 주세요."
			End If

		Set objScrFso = Nothing

		'// 상품 보기 끝
	End Sub



	'//작은이미지사이즈,파일명,큰이미지사이즈(비율)
	Function getImgSizeView_samll(byval intSizeW,byval intSizeH, byval strFile,byval bigsizeW,byval bigsizeH)

		%>
			<script language="javascript">
				<!--
					function OpenPhotoView(seq){
						window.open("/default/DetailView.asp?photo=" + seq, "", "height=10, width=10, toolbar=0, location=0,directories=0,status=0,menuBar=0,scrollBars=0,resizable=0")
					}
				//-->
			</script>
		<%
		dim filepath,arrShopImgSize,arrBigImgSize,arrNoImgSize,arrNoBigImgSize


		if len(strFile) > 0 then
			filepath = getFileNumName(strFile)
			arrShopImgSize = getImgSize("/upload/"&filepath, intSizeW, intSizeH)
			arrBigImgSize = getImgSize("/upload/"&filepath, bigsizeW, bigsizeH)
			filepath = "<img src=/upload/"& filepath &" width="& arrShopImgSize(1) &" height="& arrShopImgSize(0) &"  onmouseover='ChangeImage(this.src,"&arrBigImgSize(1)&","&arrBigImgSize(0)&");' style='cursor:hand'  onclick='OpenPhotoView(this.src);'>"
		else
			arrNoImgSize = getImgSize("/AVANshop_v3/goods/noimg.gif", intSizeW, intSizeH)
			arrNoBigImgSize = getImgSize("/AVANshop_v3/goods/noimg.gif", bigsizeW, bigsizeH)
			filepath = "<img src=/AVANshop_v3/goods/noimg.gif width="& arrNoImgSize(1) &" height="& arrNoImgSize(0) &" onmouseover='ChangeImage(this.src,"& arrNoBigImgSize(1)&","& arrNoBigImgSize(0)&");' style='cursor:hand'  onclick='OpenPhotoView(this.src);'>"
		end if


		getImgSizeView_samll = getImgSizeView_samll & "<table width="&intSizeW&" border=0 cellpadding=1 cellspacing=1 bgcolor=#CCCCCC><tr>"
		getImgSizeView_samll = getImgSizeView_samll & "<td bgcolor=#FFFFFF>"&filepath&"</td></tr></table>"

	End Function

	'//사진 보이기 가로,세로 이미지할때 (비율)
	Function getImgSizeView(byval intSizeW,byval intSizeH, byval filepath)
		dim arrShop2ImgSize

		if len(filepath) > 0 and getFileSize(filepath) then
			arrShop2ImgSize = getImgSize("/upload/"&filepath, intSizeW, intSizeH)
			filepath = "<img src=/upload/"& filepath &" width="& arrShop2ImgSize(1) &" height="& arrShop2ImgSize(0) &" align = 'absmiddle'>"
		else
			arrShop2ImgSize = getImgSize("/admin/img/images/noimg.gif", intSizeW, intSizeH)
			filepath = "<img src=/admin/img/images/noimg.gif width="& arrShop2ImgSize(1) &" height="& arrShop2ImgSize(0) &" align = 'absmiddle'>"
		end if


		'getImgSizeView = getImgSizeView & "<table width="&intSizeW+5&" height="& intSizeH+5 &" border=0 cellpadding=1 cellspacing=1 bgcolor=#CCCCCC><tr>"
		getImgSizeView = filepath
		'getImgSizeView = getImgSizeView & "<td bgcolor='#FFFFFF' align = 'center'>"&filepath&"</td></tr></table>"

	End Function

	Function getFileSize(byval sFlie)
		Dim fsoObject,objF	,filePath
		If isValue(sFlie) Then
			Set fsoObject = CreateObject("Scripting.FileSystemObject")
			filePath = server.MapPath("/upload/"&sFlie)
			set objF = fsoObject.GetFile(filePath)
			If fsoObject.FileExists(filePath) and objF.Size > 0 Then
				getFileSize = True
			Else
				getFileSize = False
			End If

		End If
	End Function


	'//작은이미지사이즈,파일명,큰이미지사이즈 가로세로
	Function getImgView_samll_both(byval intSize, byval strFile,byval bigsize1,byval bigsize2)
		dim filepath,arrBigImgSize
			filepath = getFileNumName(strFile)
			if len(filepath) > 0 then
				arrBigImgSize = getImgSize("/upload/"&filepath, bigsize1, bigsize2)
				filepath = "<img src=/upload/"& filepath &" width="& intSize &" height="& intSize &"  onclick='ChangeImage(this.src,"&arrBigImgSize(1)&","&arrBigImgSize(0)&");' style='cursor:hand'>"
			else
				arrBigImgSize = getImgSize("/AVANshop_v3/goods/noimg.gif", bigsize1, bigsize2)
				filepath = "<img src=/AVANshop_v3/goods/noimg.gif width="& intSize &" height="& intSize &" onclick='ChangeImage(this.src,"&arrBigImgSize(1)&","&arrBigImgSize(0)&");' style='cursor:hand'>"
			end if


		getImgView_samll_both = getImgView_samll_both & "<table width="&intSize&" border=0 cellpadding=1 cellspacing=1 bgcolor=#CCCCCC><tr>"
		getImgView_samll_both = getImgView_samll_both & "<td bgcolor=#FFFFFF>"&filepath&"</td></tr></table>"
	End Function


	'//사진 보이기(썬) 작은이미지 클릭시 큰 이미지에 보이게.. end


		'//계좌번호
	Sub printBankAccount()
		Dim arrBank : arrBank = split(bank,"|")
		Dim TempNum

		If uBound(arrBank) = 0 Then
		%>

			<select name = "AdminMemo" onChange="DisableName()">
			<option value = "<%=Trim(arrBank(0))%>"><%=arrBank(0)%></option>
			</select>
		<%
		Else
		%>
		<select name = "AdminMemo" onChange="DisableName()">
			<%For TempNum = 0 to uBound(arrBank)%>
			<option value = "<%=Trim(arrBank(TempNum))%>"><%=arrBank(TempNum)%></option>
			<%Next%>
		</select>
		<%
		End If
	End Sub

	'// 상품판매옵션
	'// 판매중 / 판매대기
	Sub GoodAct(byval str)

		if str = true or str = "1" then
			response.Write("<span class='text-primary'>판매중</span>")
		elseif str = false or str = "0" then
			response.Write("<span class='text-danger'>판매대기</span>")
		end if

	End Sub

	'재고량 부족하면 바로 화면에 표시
	Sub printLoseStock(byval bitTotalCheck, byval intTotal)
		If bitTotalCheck  =  True Then
		Else
		 if intTotal = 0  then
		  response.Write("<font color=red><b>[품절]</b></font>")
		 end if
		End If
	End Sub

	'//재고량 표시
	Sub printTotalOp(byval totalchk,byval total)
		if totalchk = "Y" then
			response.Write("<span class='text-primary'>무한</span>")
		elseif total = "N" then
			response.Write("<span class='text-danger'>없음</span>")
		else
			response.Write(rs("total_op"))
		end if
	End Sub

	'//카테고리 링크
	Function getTempStr(byval sCategory,byval sGidx)
		if len(sCategory) = 10 then
			getTempStr = "g_idx="&sGidx
		else
			getTempStr = "category="&getAdoRsScalar("select c_code from Goods_cate_join where g_idx='"&sGidx&"'")&"&g_idx="&sGidx
		end if
	End Function

	'//옵션 사용여부
	Function getOption(byval sOption)
		If isValue(sOption) Then
			getOption = True
		Else
			getOption = False
		End If
	End Function

	Function getSplitOption(byval sOptionName,byval sOptionList)
		If getOption(sOptionName) = True then
			getSplitOption = split(sOptionList,"/")
		End If
	End Function

	'//세션 아이디 where조건
	Function getSessionIdWhere(byval sId,byval sSessionId)

		If isValue(sId) Then
			getSessionIdWhere = " id = '"&sId&"' "
		Else
			getSessionIdWhere = " imsi_id = '"&sSessionId&"' "
		End If

	End Function

	'//세션 아이디
	Function getSessionId()
		If isValue(session("userid")) Then
			getSessionId = session("userid")
		End If
	End Function

	Function getSessionUserId()
		If  isValue(session("sessionid")) Then
			getSessionUserId = session("sessionid")
		Else
			getSessionUserId = session.SessionID
		End If
	End Function

	'//세션이 존재하는지 판단
	Function getSession()
		If isValue(session("userid")) Then
			getSession = True
		Else
			getSession = False
		End If

	End Function

	'//Good 이미지 경로
	Function getGoodImgPath(byval sImg,byval bImg, mImg)
		if isValue(sImg) then
			getGoodImgPath = "/upload/"&getFileNumName(sImg)
		elseif isValue(bImg) then
			getGoodImgPath = "/upload/"&getFileNumName(bImg)
		elseif isValue(mImg) then
			getGoodImgPath = "/upload/"&getFileNumName(mImg)
		else
			getGoodImgPath = "/AVANshop_v3/goods/noimg.gif"
		end if
	End Function

	'재고량 부족하면 바로 화면에 표시
	Sub printOrderLoseStock(byval totalChk,byval total, byval ea)
		If  totalChk =  True Then
		Else
			if total <  ea then
				response.Write("<br><font color=red>재고량이 부족 [현재고량 "& total &"개]</font>")
			end if
		End If
	End Sub

	'//옵션2 머니
	 Function getValueOption2(byval option2)
		Dim stroption
		If isValue(option2) Then
			stroption = split(option2,"\")
			If uBound(stroption) = 1 Then
				getValueOption2 = stroption(1)
			Else
				getValueOption2 = 0
			End If
		End If
	 End Function

	 '//배송비
	Function getBeasongMoney(byval total,byval besong_limit,byval besong)
		If int(total) < int(besong_limit) then
			getBeasongMoney = int(besong_money)
		Else
			getBeasongMoney = 0
		End if
	End Function

	'//배송비 출력
	Sub printBesong(byval total,byval besong_limit,byval besong)
		If besong_limit = 0 Then
			response.write " &nbsp;(배송료착불)"
		ElseIf int(total) < int(besong_limit) then
			response.write "<font color=ea7518>(배송료) "&FormatNumber(besong,0)&" 원</font>"
		Else
		End If
	End Sub

	'//사용자 적립금 잔액

	 Function getPointRemainder()
		Dim p_remainder,pointSQL
		pointSQL = "SELECT TOP 1 p_remainder FROM Point where p_userid = '"&session("userid")&"' ORDER BY p_idx desc"
		p_remainder = getAdoRsScalar(pointSQL)
		If p_remainder <> "" Then
			getPointRemainder = p_remainder
		Else
			getPointRemainder = 0
		End If
	 End Function

	 Function GetLoginShop(ByVal strLoginPagePath)

		Dim strBackUrl

		strBackUrl = Server.URLEncode( "" _
			& Request.ServerVariables("PATH_INFO") _
			&"?"& Request.ServerVariables("QUERY_STRING") _
			& "" )

		Move "", strLoginPagePath & "&strBackUrl=" & strBackUrl
	End Function



	'//포인트 부족일때 나오는 메세지
	Sub PointProcess(byval remainder,byval point,byval realmoney,byval totalMoney)


		If int(point) > int(remainder)  then
			Error("적립금이 부족합니다.")
		ElseIf int(totalMoney) < int(point) Then
			Error("결재액보다 적립금사용이 큽니다")
		ElseIf int(pointLimit) > int(remainder) and int(point) > 0  Then
			Error("적립금은 "&pointLimit&" 이상부터 현금으로 사용하실 수 있습니다.")
		Else

		End If

	End Sub



	'//오늘 본 상품
	Sub toDayGood(byval gIdx)
		If isValue(session.SessionID) Then
			Dim sidx : sidx = getAdoRsScalar("select i_idx from mTb_imsi where i_id = '"&session.SessionID&"' and p_code = '"&gIdx&"'")
			If sidx <> "" Then
				call AdoConnExecute("update mTb_imsi set i_date = getdate() where i_idx = '"&sidx&"'")
			Else
				call AdoConnExecute("insert into mTb_imsi (i_id,p_code) values ('"&session.SessionID&"','"&gIdx&"')")
			End If
		 End If
	End Sub

	Function result_num(byVal resultnum)
		resultnum = trim(resultnum)
		If resultnum = "1" Then
			result_num = "주문완료/입금확인"
		ElseIf resultnum = "2" Then
			result_num = "입금완료/배송준비"
		ElseIf resultnum = "e2" Then
			result_num = "<font color = '009933'>[E]배송준비중</font>"
		ElseIf resultnum = "3" Then
			result_num = "<font color = '009933'>배송중</font>"
		ElseIf resultnum = "e3" Then
			result_num = "<font color = '009933'>[E]배송중</font>"
		ElseIf resultnum = "4" Then
			result_num = "반품요청"
		ElseIf resultnum = "m4" Then
			result_num = "[M]반품요청됨"
		ElseIf resultnum = "e4" Then
			result_num = "[E]반품요청됨"
		ElseIf resultnum = "5" Then
			result_num = "취소요청"
		ElseIf resultnum = "e5" Then
			result_num = "[E]즉시취소됨"
		ElseIf resultnum = "6" Then
			result_num = "<font color = 'blue'>환불중</font>"
		ElseIf resultnum = "e7" Then
			result_num = "<font color = 'gray'>[E]환불완료됨</font>"
		ElseIf resultnum = "e8" Then
			result_num = "<font color = 'gray'>[E]취소접수</font>"
		ElseIf resultnum = "e9" Then
			result_num = "<font color = 'red'>[E]입금계좌해지됨</font>"
		ElseIf resultnum = "Y" Then
			result_num = "정상거래"
		ElseIf resultnum = "N" Then
			result_num = "거래취소"
		End IF
	End Function

	'//카트 전체 카운트
	Function getCartCount()
		Dim totalcount
		totalcount = getAdoRsScalar("select isNull(count(*),0) from cart c, goods g where "&getSessionIdWhere(getSessionId(),getSessionUserId())&" and c.goods_num = g.g_idx")
		If totalcount > 0 Then
			getCartCount = totalcount
		Else
			getCartCount = 0
		End If
	End Function

	'//결재방식,사용포인트,진짜결재액,계좌번호,은행명
	Function pay10(byval pay1,byval use_point1,byval intRealMoney,byval account1,byval bankname1)
		Dim strMethod
		If use_point1 = "" Then use_point1 = 0

		If intRealMoney > 0 and use_point1 > 0 Then strMethod = "적립금 + "
		If intRealMoney = 0 and use_point1 > 0 Then strMethod = "적립금 "

		If isValue(pay1) Then strMethod = strMethod&getPayMethod(pay1)

		If pay1 = "VBank" Then
			strMethod = strMethod&" <br>[계좌번호 : "&account1& " 은행명 : "& bankname1&"]"
		End If

		pay10 = strMethod

	End Function


	'// 에스크로 조건 설정
	dim escro_chk

	sub set_escro_chk(byval pay, byval realmoney)
		escro_chk = false
		if pay ="VBank" and Cdbl(realmoney) > 99999 then escro_chk = true
	End sub

	Function Scent(byval num)

		num = trim(num)
		If num = "1" Then
			Scent = " 주문완료/입금확인중"
		ElseIf num = "2" Then
			Scent = " 입금완료/배송준비"
		ElseIf num = "e2" Then
			Scent = " [E]배송준비중"
		ElseIf num = "3" Then
			Scent = " 배송중"
		ElseIf num = "e3" Then
			Scent = " [E]배송중"
		ElseIf num = "4" Then
			Scent = " 반품요청"
		ElseIf num = "m4" Then
			Scent = " [M]반품요청됨"
		ElseIf num = "e4" Then
			Scent = " [E]반품요청됨"
		ElseIf num = "5" Then
			Scent = " 취소요청"
		ElseIf num = "e5" Then
			Scent = " [E]즉시취소됨"
		ElseIf num = "6" Then
			Scent = " 환불중"
		ElseIf num = "e7" Then
			Scent = " [E]환불완료됨"
		ElseIf num = "e8" Then
			Scent = " [E]취소접수"
		ElseIf num = "e9" Then
			Scent = " [E]입금계좌해지됨"
		ElseIf Ucase(num) = "Y" Then
			Scent = " 정상거래"
		ElseIf Ucase(num) = "N" Then
			Scent = " 거래취소"
		End IF

	End Function






	Function getbankName(byval strBank)
		Dim bank_names
		Set bank_names = Server.CreateObject("Scripting.Dictionary")
		bank_names.add "02","한국산업은행"
		bank_names.add "03","기업은행"
		bank_names.add "04","국민은행"
		bank_names.add "05","하나은행 (구 외환)"
		bank_names.add "06","국민은행 (구 주택)"
		bank_names.add "07","수협중앙회"
		bank_names.add "11","농협중앙회"
		bank_names.add "12","단위농협"
		bank_names.add "16","축협중앙회"
		bank_names.add "20","우리은행"
		bank_names.add "21","구)조흥은행"
		bank_names.add "22","상업은행"
		bank_names.add "23","SC제일은행"
		bank_names.add "24","한일은행"
		bank_names.add "25","서울은행"
		bank_names.add "26","구)신한은행"
		bank_names.add "27","한국씨티은행 (구 한미)"
		bank_names.add "31","대구은행"
		bank_names.add "32","부산은행"
		bank_names.add "34","광주은행"
		bank_names.add "35","제주은행"
		bank_names.add "37","전북은행"
		bank_names.add "38","강원은행"
		bank_names.add "39","경남은행"
		bank_names.add "41","비씨카드"
		bank_names.add "45","새마을금고"
		bank_names.add "48","신용협동조합중앙회"
		bank_names.add "50","상호저축은행"
		bank_names.add "53","한국씨티은행"
		bank_names.add "54","홍콩상하이은행"
		bank_names.add "55","도이치은행"
		bank_names.add "56","ABN 암로"
		bank_names.add "57","JP모건"
		bank_names.add "59","미쓰비시도쿄은행"
		bank_names.add "60","BOA(Bank of America)"
		bank_names.add "64","산림조합"
		bank_names.add "70","신안상호저축은행"
		bank_names.add "71","우체국"
		bank_names.add "81","하나은행"
		bank_names.add "83","평화은행"
		bank_names.add "87","신세계"
		bank_names.add "88","신한(통합)은행"
		bank_names.add "D1","유안타증권(구 동양증권)"
		bank_names.add "D2","현대증권"
		bank_names.add "D3","미래에셋증권"
		bank_names.add "D4","한국투자증권"
		bank_names.add "D5","우리투자증권"
		bank_names.add "D6","하이투자증권"
		bank_names.add "D7","HMC 투자증권"
		bank_names.add "D8","SK 증권"
		bank_names.add "D9","대신증권"
		bank_names.add "DA","하나대투증권"
		bank_names.add "DB","굿모닝신한증권"
		bank_names.add "DC","동부증권"
		bank_names.add "DD","유진투자증권"
		bank_names.add "DE","메리츠증권"
		bank_names.add "DF","신영증권"
		bank_names.add "DG","대우증권"
		bank_names.add "DH","삼성증권"
		bank_names.add "DI","교보증권"
		bank_names.add "DJ","키움증권"
		bank_names.add "DK","이트레이드"
		bank_names.add "DL","솔로몬증권"
		bank_names.add "DM","한화증권"
		bank_names.add "DN","NH증권"
		bank_names.add "DO","부국증권"
		bank_names.add "DP","LIG증권"

		getbankName = bank_names.item(strBank)
	End Function

	Function getcardName(ByVal v)

		Dim bank_names
		Set bank_names = Server.CreateObject("Scripting.Dictionary")
		bank_names.add "01","하나(외환)"
		bank_names.add "03","롯데"
		bank_names.add "04","현대"
		bank_names.add "06","국민"
		bank_names.add "11","BC"
		bank_names.add "12","삼성"
		bank_names.add "14","신한"
		bank_names.add "21","해외 VISA"
		bank_names.add "22","해외마스터"
		bank_names.add "23","해외 JCB"
		bank_names.add "26","중국은련"
		bank_names.add "32","광주"
		bank_names.add "33","전북"
		bank_names.add "34","하나"
		bank_names.add "35","산업카드"
		bank_names.add "41","NH"
		bank_names.add "43","씨티"
		bank_names.add "44","우리"
		bank_names.add "48","신협체크"
		bank_names.add "51","수협"
		bank_names.add "52","제주"
		bank_names.add "54","MG새마을금고체크"
		bank_names.add "71","우체국체크"
		bank_names.add "95","저축은행체크"

		getcardName = bank_names.item(v)
	End function

	Function getPayMethod(byval sValue)
		if trim(sValue)="Card" then
			getPayMethod="신용카드"
		elseif trim(sValue)="DirectBank" then
			getPayMethod="실시간계좌이체"
		elseif trim(sValue)="VBank" then
			getPayMethod="가상계좌"
		elseif trim(sValue)="HPP" then
			getPayMethod="핸드폰"
		elseif trim(sValue)="BankPay" then
			getPayMethod="무통장입금"
		else
			getPayMethod = "Error"
		End If
	End Function

	Sub BackCode(byval bank_code)
%>
<select name='bank_code'>
	<option value="bank_code_not_sel" <%=chk_selected(bank_code,"bank_code_not_sel")%>>선택</option>
	<option value="39" <%=chk_selected(bank_code,"39")%>>경남은행</option>
	<option value="03" <%=chk_selected(bank_code,"03")%>>기업은행</option>
	<option value="32" <%=chk_selected(bank_code,"32")%>>부산은행</option>
	<option value="07" <%=chk_selected(bank_code,"07")%>>수협중앙회</option>
	<option value="48" <%=chk_selected(bank_code,"48")%>>신협</option>
	<option value="71" <%=chk_selected(bank_code,"71")%>>우체국</option>
	<option value="23" <%=chk_selected(bank_code,"23")%>>제일은행</option>
	<option value="06" <%=chk_selected(bank_code,"06")%>>주택은행</option>
	<option value="81" <%=chk_selected(bank_code,"81")%>>하나은행</option>
	<option value="34" <%=chk_selected(bank_code,"34")%>>광주은행</option>
	<option value="11" <%=chk_selected(bank_code,"11")%>>농협중앙회</option>
	<option value="02" <%=chk_selected(bank_code,"02")%>>산업은행</option>
	<option value="53" <%=chk_selected(bank_code,"53")%>>시티은행</option>
	<option value="05" <%=chk_selected(bank_code,"05")%>>외환은행</option>
	<option value="09" <%=chk_selected(bank_code,"09")%>>장기신용</option>
	<option value="35" <%=chk_selected(bank_code,"35")%>>제주은행</option>
	<option value="16" <%=chk_selected(bank_code,"16")%>>축협중앙회</option>
	<option value="27" <%=chk_selected(bank_code,"27")%>>한미은행</option>
	<option value="04" <%=chk_selected(bank_code,"04")%>>국민은행</option>
	<option value="31" <%=chk_selected(bank_code,"31")%>>대구은행</option>
	<option value="25" <%=chk_selected(bank_code,"25")%>>서울은행</option>
	<option value="26" <%=chk_selected(bank_code,"26")%>>신한은행</option>
	<option value="20" <%=chk_selected(bank_code,"20")%>>우리은행</option>
	<option value="37" <%=chk_selected(bank_code,"37")%>>전북은행</option>
	<option value="21" <%=chk_selected(bank_code,"21")%>>조흥은행</option>
	<option value="83" <%=chk_selected(bank_code,"83")%>>평화은행</option>
</select>
<%
End Sub


'//배송중
Function getStatusAfterBasong(byval strStatus)
	If strStatus = "2e" Then
		getStatusAfterBasong = True
	ElseIf strStatus = "3" Then
		getStatusAfterBasong = True
	ElseIf strStatus = "3e" Then
		getStatusAfterBasong = True
	Else
		getStatusAfterBasong = False
	End If
End Function
'//배송전
Function getStatusPreBasong(byval strStatus)
	If strStatus = "1" Then
		getStatusPreBasong = True
	ElseIf strStatus = "2" Then
		getStatusPreBasong = True
	Else
		getStatusPreBasong = False
	End If
End Function

Sub RadioEnable(byval sStatus)
	If sStatus = "1" Then
		response.write "disabled='disabled'"
	ElseIf sStatus = "2" Then
		response.write "disabled='disabled'"
	End If
End Sub
Function getConfirm(byval sValue)
	If sValue = "Y" Then
		getConfirm = True
	ElseIf sValue = "N" Then
		getConfirm = True
	Else
		getConfirm = False
	End If
End Function

Function AdminAccount(byval strStatus,byval ordr_idxx)

	Dim TempScent

	SQL = "SELECT status_scent FROM Order_Regist WHERE order_num = '"&ordr_idxx&"'"

	Dim strScent : strScent = getAdoRsScalar(SQL)

	TempScent = strScent &"<br>"&"[관리자]"&now()&Scent(strStatus)


	AdminAccount = "UPDATE Order_Regist SET status = '"&strStatus&"', status_scent = '"&TempScent&"'  WHERE order_num = '"&ordr_idxx&"' and status not in ('N','Y') "
End Function

Function getMemberName(byval sId)
	getMemberName = getAdoRsScalar("SELECT strName FROM mTb_member2 WHERE strId = '"&sId&"'")
End Function

Function getMyPageResultTitle(byval sValue)

	Select Case sValue
		Case "1"	getMyPageResultTitle = "신규[미입금]리스트"
		Case "2"	getMyPageResultTitle = "배송준비 리스트"
		Case "3"	getMyPageResultTitle = "배송중 리스트"
		Case "4"	getMyPageResultTitle = "반품 리스트"
		Case "5"	getMyPageResultTitle = "취소 리스트"
		Case "6"	getMyPageResultTitle = "환불 리스트"
		Case "07"	getMyPageResultTitle = "입금계좌해지 리스트"
		Case "12"	getMyPageResultTitle = "배송준비리스트"
		Case "15"	getMyPageResultTitle = "주문내역조회"
		Case "30"	getMyPageResultTitle = "거래취소 리스트"
		Case "40"	getMyPageResultTitle = "정상거래 리스트"
		Case "67"	getMyPageResultTitle = "반품/취소내역"
		case Else 	getMyPageResultTitle = "전체 리스트"
	End Select

End Function

'//로그인 title
Function getMyPageMemberTitle(byval sValue)
	If  sValue = "modify" Then
		getMyPageMemberTitle = "회원정보수정"
	ElseIf  sValue = "idsearch" Then
		getMyPageMemberTitle = "아이디 찾기"
	ElseIf sValue = "pwsearch" Then
		getMyPageMemberTitle = "패스워드 찾기"
	ElseIf  sValue = "del" Then
		getMyPageMemberTitle = "회원탈퇴"
	ElseIf sValue = "join" Then
		getMyPageMemberTitle = "회원가입"
	ElseIf sValue = "contract" Then
		getMyPageMemberTitle = "회원가입"
	Else
		getMyPageMemberTitle = "로그인"
	End If
End Function

Function getMemberPoint(byval sId)
	Dim intPoint
	intPoint = getAdoRsScalar("select p_remainder from point where p_userid = '"&sId&"' order by p_idx desc")
	If intPoint > 0 Then
		getMemberPoint = intPoint
	Else
		getMemberPoint = 0
	End If
End Function


Function getOrderPoint(byval sIdx)
	Dim intPoint
	intPoint = getAdoRsScalar("select point from order_regist where order_num = '"&sIdx&"' ")
	If intPoint > 0 Then
		getOrderPoint = intPoint
	Else
		getOrderPoint = 0
	End If
End Function

Sub printCamera(byval sImage)
	If isValue(sImage) Then
		response.write " <img src = '/AVANshop_v3/img/ico_camera.gif'> "
	End If
End Sub



%>






<%
'Function getOrderDetail(byval goodIdx,byval sUserid)
'	If isValue(goodIdx) and isValue(sUserid) Then
'		getOrderDetail = getAdoRsScalar("select isNull(count(*),0) from order_regist o ,orderdetail d where o.order_num = d.regist_num and id = '"&sUserid&"' and  goods_num = '"&goodIdx&"' and  status  in ('2','e2','3','e3','Y')")
'	End If
'End Function

'//상품평은 한번만 작성하겡~
Function getOrdDeNum(byval deIdx)
	If isValue(deIdx)  Then
		getOrdDeNum = getAdoRsScalar("select r_no from Good_Reply where  r_num = '"&deIdx&"'")
	End If
End Function

'// 상품명나올수 있게
Function getGoodName(byval idx)
	If isValue(idx) Then
		getGoodName = getAdoRsScalar("SELECT good_name FROM OrderDetail WHERE num = '"&idx&"'")
	End If
End Function
'// 상품이미지나올수 있게
Function getGoodImg(byval idx)
	If isValue(idx) Then
		getGoodImg = getAdoRsScalar("SELECT good_img FROM OrderDetail WHERE num = '"&idx&"'")
	End If
End Function

'//bit함수 구분
Function getBitGubun(byval sValue)
	If sValue = "0" or sValue = False Then
		getBitGubun = False
	Else
		getBitGubun = True
	End If
End Function

'//비밀번호 체크
Sub passwordCheck(byval sPwd,byval sIdx)

	Dim TempSeq
	TempSeq 	= getAdoRsScalar("select intseq from mTb_Inquiry where strPwd = '"& sPwd &"' and intseq = '"&sIdx&"'")


	'//비밀번호가 맞으면
	If isValue(TempSeq) or admin_chk() Then
	Else
		Error("비밀번호가 틀립니다.")
	End If
End Sub

'//추천아이디
Function memberAgreeId(byval sId)
	If isValue(sId) Then
		memberAgreeId = getAdoRsScalar("select strId from mtb_member2 where strAgreeId = '"&sId&"'")
	End If
End Function

Function getConfirmTitle(byval sValue)
	If ucase(sValue) = "Y" Then
		getConfirmTitle = "정상거래"
	ElseIf ucase(sValue) = "N" Then
		getConfirmTitle = "거래취소"
	End If
End Function
Function getConfirm(byval sValue)
	If  ucase(sValue) = "Y" Then
		getConfirm = True
	ElseIf  ucase(sValue) = "N" Then
		getConfirm = True
	Else
		getConfirm = False
	End If
End Function

Function getPointCancelMoney(byval sIdx,byval sGubun)

	Dim pointSQL,arrListData,p_money,sum_money,intLoop

	pointSQL = "select p_money,p_remainder,p_userid from point where p_num = '"&sIdx&"' and p_gubun = '"&sGubun&"'"

	arrListData = getAdoRsArray(pointSQL)

	If isArray(arrListData) Then

		For intLoop = 0 to UBound(arrListData,2)

			p_money		= arrListData(0,intLoop)

			sum_money = sum_money + -(p_money)

		Next

		getPointCancelMoney = sum_money
	Else
		getPointCancelMoney = 0

	End If

End Function
'//세센 id가 변경될수 있으므로
Sub ApplySession(byval intsession_id)
	If intsession_id <> session.SessionID Then
		session("sessionid") =intsession_id
	End If
End Sub

Function getPointCancelId(byval sIdx,byval sGubun)

	Dim pointSQL,arrListData,p_userid,intLoop

	pointSQL = "select p_userid from point where p_num = '"&sIdx&"' and p_gubun = '"&sGubun&"'"

	arrListData = getAdoRsArray(pointSQL)

	If isArray(arrListData) Then

		For intLoop = 0 to UBound(arrListData,2)

			p_userid	= arrListData(0,intLoop)

		Next

		getPointCancelId = p_userid
	Else
		getPointCancelId = ""

	End If

End Function


Function PointInsertId(byval p_title, byval p_money, byval jumunNo, byval changNo,byval sid,byval sGubun)

	Dim remainderSQL,remainder
	Dim p_idx, p_date, p_remainder, p_userid	,p_num
	Dim strSQL
	Dim jumunID

	if 	len(changNo) = 0 then
		jumunID = sid
	else
	'// 주문번호가 있는경우 해당 주문번호에 맞는 아이디를 가져온다
		jumunID = getAdoRsScalar("SELECT TOP 1 p_userid FROM Point where p_num = '"&changNo&"' and p_gubun = '"&sGubun&"' ORDER BY p_idx desc")
	end if

	remainderSQL = "SELECT TOP 1 p_remainder FROM Point where p_userid = '"&jumunID&"' ORDER BY p_idx desc"


	'//사용자의 잔액을 가져옴

	p_remainder  = getAdoRsScalar(remainderSQL)




	If p_remainder > 0 Then
		remainder = p_remainder+p_money
	Else
		remainder = p_money
	End If

	strSQL = "INSERT"_
			& " INTO Point " _
			& "		(p_date,"_
			& "		p_title,"_
			& "		p_money,"_
			& "		p_remainder,p_userid,p_num,p_gubun) "_
			& "	VALUES "_
			& "			('" & now() & "',"_
			& "			'" & p_title & "',"_
			& "			" & p_money & ","_
			& "			"& remainder &","_
			& "			'"& jumunID &"',"_
			& "			'"& jumunNo &"','"&sGubun&"')"
	'		response.write strsql
	If p_money = 0 or p_money = "" Then
	Else
		Dbconn.execute(strSQL)
	End If

End Function

%>
