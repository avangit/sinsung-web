<%@  codepage="65001" language="VBScript" %>

<% response.charset = "utf-8"%>
<%

	blnUpload = Request.QueryString("blnUpload")

	'If blnUpload = 1 Then

		Const UploadFolder = "/Upload/"

		Set objUpload = Server.CreateObject("Dext.FileUpload")

			objUpload.DefaultPath = Server.MapPath(UploadFolder)

			'inputFile = objUpload("inputFile")
			inputFile = objUpload("Upload")

			If Len(inputFile) > 0 Then

				strFileName = objUpload("Upload").FileName
				strFileName = Replace(strFileName, " ", "")
				strFileName = Replace(strFileName, ",", "")

				strFileExt = Mid(strFileName, InstrRev(strFileName, ".") + 1)

				dtmNowDate = Now()

				strFileNameSecret = "UserFile_" _
					& Replace(Date(),"-", "") _
					& Hour(dtmNowDate) _
					& Minute(dtmNowDate) _
					& Second(dtmNowDate) _
					& Session.SessionID _
					& "." _
					& strFileExt

				strFileSaveFullPath = objUpload.DefaultPath & "\" & strFileNameSecret

				If InStr(",gif,jpg,jpeg,png,", "," & LCase(strFileExt) & ",") > 0 Then
					objUpload("Upload").SaveAs strFileSaveFullPath, False
					strLastSavedFileName =  UploadFolder & objUpload("Upload").LastSavedFileName

				Else
					strErrorMsg = "이미지파일외에는 업로드 하실 수 없습니다."

				End If

			End If

		Set objUpload = Nothing

	'End If
	Dim CKEditorFuncNum
	CKEditorFuncNum =request.QueryString("CKEditorFuncNum")
%>
<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction('<%=CKEditorFuncNum%>', '<%=strLastSavedFileName%>', '전송완료 메시지')</script>
