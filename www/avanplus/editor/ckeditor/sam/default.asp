<!DOCTYPE html>
<!--
Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.md or http://ckeditor.com/license
-->
<html>
<head>
	<!-- meta charset="utf-8" -->
	<meta charset="euc-kr">
	<title>Replace Textareas by Class Name &mdash; CKEditor Sample</title>
	<script src="../ckeditor.js"></script>
	<link rel="stylesheet" href="sam.css">
</head>

	<form action="sample_posteddata.php" method="post">
		<p>
			<label for="editor1">
				Editor 1:
			</label>
			<textarea class="ckeditor" cols="80" id="editor1" name="editor1" rows="10">
				
			</textarea>
		</p>
		<p>
			<input type="submit" value="Submit">
		</p>
	</form>

</body>
</html>
<script>
CKEDITOR.replace('editor1',{
	toolbar:
	 ['Source','-','Save','NewPage','Preview','-','Templates'],
	 ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
	 ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	 ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
	 '/',
	 ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	 ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
	 ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	 ['Link','Unlink','Anchor'],
	 ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
	 '/',
	 ['Styles','Format','Font','FontSize'],
	 ['TextColor','BGColor'],
	 ['Maximize', 'ShowBlocks','-','About']
});

 CKEDITOR.replace( 'editor1',{
   enterMode:'2'
   shiftEnterMode:'3'
 });

</script>
1 = <p></p>

2 = <br />

3 = <div></div>
