<%
	'//=============================================
	'	직접 코딩시 모듈을 호출하는 함수
	'	이 파일을 인클루드하고 모듀을 호출한다.
	'===============================================//
	Function module(byval moduleid, byval modulvalues)
		dim FilePath
		FilePath = "/avanplus/modul/"&moduleid&"/call.asp"
		FilePath = replace(FilePath,"//","/")
		
		
		'//모듈구분값에 값이 있으면 get방식으로 값을 넘긴다.
		IF instr(request.ServerVariables("QUERY_STRING"),modulvalues)=0 THEN	
			'//모듈구분값에 값이 있으면 get방식으로 값을 넘긴다.
			if len(modulvalues) > 0 then response.redirect("?"&getString(modulvalues))
		END IF
		
		dim objScrFso
		Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
			
			if objScrFso.FileExists(Server.MapPath(FilePath)) then
				Server.Execute(FilePath)
			Else
				Response.Write FilePath&"는 존재하지 않는 모듈ID입니다. 모듈ID를 다시한번 확인해주세요."
			End If
	
		Set objScrFso = Nothing
	End Function
	
	
	Function GetString(ByVal addString)
	'##	현재페이지의 스트링값을 받는 함수

      	dim UrlString, split_UrlString, i_UrlString, chkword_UrlString
		dim split_addString, i_addString, chkword_addString
		dim stringEquleChk
		UrlString = request.serverVariables("QUERY_STRING")

		if trim(UrlString)="" or isnull(trim(UrlString)) then
			GetString = addString
		elseif not isnull(trim(urlString)) and isnull(trim(addString)) then
			GetString = urlString
		else
		
			split_UrlString = split(UrlString,"&")
			split_addString = split(addString,"&")
			
		
'response.Write Ubound(split_UrlString) & "/"
'response.Write Ubound(split_addString)
			For i_UrlString = 0 to Ubound(split_UrlString)
				'aaa=123
				if split_UrlString(i_UrlString) <> "" then
					chkword_UrlString = split(split_UrlString(i_UrlString),"=")(0)
					chkword_UrlString = lcase(chkword_UrlString)
					chkword_UrlString = trim(chkword_UrlString)
					stringEquleChk = "n"
					For i_addString = 0 to Ubound(split_addString)
						chkword_addString = split(split_addString(i_addString),"=")(0)
						chkword_addString = lcase(chkword_addString)
						chkword_addString = trim(chkword_addString)
						if chkword_UrlString = chkword_addString then
							stringEquleChk = "y"					
						end if
					'response.Write Ubound(split_addString)
					'response.Write("["&chkword_UrlString&"/"&chkword_addString&"|"&stringEquleChk&"]")
					next
					if stringEquleChk = "n" then

						GetString = GetString & trim(split_UrlString(i_UrlString)) & "&"
						
					end if
					'response.Write("///"&i_UrlString&""&i_addString&stringEquleChk)
				end if
			next
			
			GetString = GetString & addString
			
		end if
		
		
		
		
		'// 2006-05-18 수정
		'// 한번사용하고 소멸하는 Temp 스트링을 없애는 처리 추가
		'// 예를들어 tmep=1234&ttt=1233 인경우 ttt=1233 변환
		
			DIM TEMPI, TEMPI2
			DIM SPLIT_GETSTRING
			DIM TEMPWORD
			DIM GetString2

			
			SPLIT_GETSTRING = SPLIT(GetString,"&")

			
			For TEMPI = 0 to Ubound(SPLIT_GETSTRING)
				'aaa=123
				if SPLIT_GETSTRING(TEMPI) <> "" then
					TEMPWORD = split(SPLIT_GETSTRING(TEMPI),"=")(0)
					TEMPWORD = lcase(TEMPWORD)
					TEMPWORD = trim(TEMPWORD)

					if TEMPWORD = "temp" then
					else
						GetString2 = GetString2 & trim(SPLIT_GETSTRING(TEMPI)) & "&"
					end if
					GetString = GetString2
				end if
			next
			
			'// 2006-05-18 수정끝
		
	End Function
%>