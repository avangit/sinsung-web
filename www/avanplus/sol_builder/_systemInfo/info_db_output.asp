<!-- #include virtual ="/avanplus/inc/incCodepage.asp" -->
<%
	Option Explicit

	if session("userlevel") < 9 then response.End()
	
	Dim strDataBase
	Dim strQueryStatement

	'strDataBase = Request.Form("DataBase")
	strQueryStatement = Request.Form("QueryStatement")
%>

<html>

<head>
<title>결과 창</title>
</head>

<body>

<%
	if ( strQueryStatement <> "" ) then
		Dim strConnect

		' Connection String 을 작성한다.
		'strConnect = "Provider=SQLOLEDB;"
		'strConnect = strConnect & "Data Source=" & Session("IP") & ";"
		'strConnect = strConnect & "Initial Catalog=" & strDataBase & ";"
		'strConnect = strConnect & "User id=" & Session("ID") & "; Password=" & Session("Pwd") & ";"

		%><!--#include virtual = "/dbconn.asp"--><%
		strConnect = cstConnString()
		
		Dim objConn
		Dim objRs
		Dim strSQL

		Set objConn = Server.CreateObject("ADODB.Connection")

		objConn.Open strConnect

		' 쿼리문이 Insert, Update, Delete의 경우에는 결과물이 레코드셋이 아니므로 따로 처리한다.
		if ( Left( LCase( strQueryStatement ), 6 ) = "insert" ) or ( Left( LCase( strQueryStatement ), 6 ) = "update" ) or ( Left( LCase( strQueryStatement ), 6 ) = "delete" ) then
			On Error Resume Next

			objConn.Execute strQueryStatement

			if ( Err.Number <> 0 ) then
				Response.Write "오류 발생 : " & Err.Description
				Response.End
			else
				Response.Write "쿼리문이 제대로 수행 되었습니다."
				Response.End
			end if
		else
			Set objRs = objConn.Execute( strQueryStatement )

			Dim elements
			Dim elem
			Dim intCount
			Dim intLoop
%>

		<table width = 100% cellpadding = 0 cellspacing = 0 border = 1>

<%
			' 쿼리문에 여러 개의 레코드셋을 가져오는 경우도 있으므로
			' 다음과 같이 모든 레코드를 가져올 수 있도록 처리한다.
			Do Until objRs is Nothing
				intCount = objRs.Fields.Count

				if ( intCount <> 0 ) then
					Response.Write "<TR bgcolor = #efefef>"

					' 컬럼의 이름을 출력하는 부분
					For intLoop = 0 to intCount - 1
						Response.Write "<TD>"
						Response.Write objRs.Fields( intLoop ).Name		' 컬럼의 이름을 가져온다.
						Response.Write "</TD>"
					Next

					Response.Write "</TR>"

					' 실제 레코드를 출력하는 부분
					Do Until ( objRs.EOF = True )
						Response.Write "<TR>"

						For intLoop = 0 to intCount - 1
							Response.Write "<TD>"
							Response.Write objRs( intLoop )
							Response.Write "</TD>"
						Next

						Response.Write "</TR>"
						objRs.MoveNext
					Loop
				end if

				Set objRs = objRs.NextRecordSet
			Loop
%>

		</table>

<%
		end if
	end if
%>

</body>

</html>
