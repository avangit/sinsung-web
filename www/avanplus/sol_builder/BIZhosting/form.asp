<!--#include file = "_config.asp"-->
<%
Dim strAction

intSeq = request.QueryString("intSeq")

Dim strPageMode : strPageMode = "update"
	
	Call idxdata(intSeq)
	
	'action처리부분
	
	If Len(intSeq) > 0 Then
		strAction = getstring(Callpage("update",""))	'//수정
	Else			
		strAction = getstring(Callpage("insert",""))	'//등록
	End If


%>
<script language="javascript">
<!--

	function frmRequestForm_Submit(frm){
		if ( frm.strTitle.value.replace(/ /gi, "") == "" ) { alert("상호/단체명을 입력해주세요"); frm.strTitle.focus(); return false; }
		<%If Len(intSeq) = 0 Then%>
		//if ( frm.strimage.value.replace(/ /gi, "") == "" ) { alert("이미지를 입력해주세요"); frm.strimage.focus(); return false; }
		<%End If%>
		//if ( frm.strContent.value.replace(/ /gi, "") == "" ) { alert("의뢰인을 입력해주세요"); frm.strContent.focus(); return false; }
		frm.submit();
	}

//-->
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><form action="?<%=strAction%>" method="post" enctype="multipart/form-data" name="frmRequestForm" id="frmRequestForm" style="margin:0px;" onsubmit="return frmRequestForm_Submit(this);">
      <input type="hidden" name="intseq" value="<%=intseq%>" />
      <table width="100%"  border="1" class="basic">
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 희망 ID/PW </b></td>
          <td class="basic">ID
            <input type="text" name="bizid" value="<%=bizid%>" maxlength="50" style="width:100;" />
            PW
            <input type="text" name="bizpw" value="<%=bizpw%>" maxlength="50" style="width:100;" /> 
            bizid/bizpw
<br />
            <font color="#FF0000">ID의 경우 시스템에 의해 변경 될 수 있습니다. </font></td>
        </tr>
        <tr>
          <td class="basic" width="120" bgcolor="#F5F5F5"><b><font color="#FF0000">+</font> 상호/단체명</b></td>
          <td class="basic"><input type="text" name="strTitle" value="<%=strTitle%>" maxlength="50" style="width:50%;" />
            strTitle</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">+</font> 도메인주소</b></td>
          <td class="basic">http://
            <input type="text" name="strDomain" value="<%=strDomain%>" maxlength="50" style="width:200;" />
            strDomain</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">+</font> 계약자명</b></td>
          <td class="basic"><input type="text" name="strName" value="<%=strName%>" maxlength="50" style="width:100;" />
            연락처
            <input type="text" name="strTel" value="<%=strTel%>" maxlength="50" style="width:150;" />
            strName/strTel</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">+</font> 핸드폰</b></td>
          <td class="basic"><!--include virtual = "/select.asp"-->
              <% Call Form_HandphonePrint("strHp","")%> 
              <font color="#FF0000">*진행상황을 SMS로 알려드립니다. strHP          </font></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">+</font> 이메일</b></td>
          <td class="basic"><!--include virtual = "/select.asp"-->
              <% Call Form_EmailPrint("strEmail","")%>
              strEmail</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 주소</b></td>
          <td class="basic"><input type="text" name="strAddr" value="<%=strAddr%>" maxlength="50" style="width:50%;" />
            strAddr</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ <font color="#0033CC">호스팅상품선택</font>
</b></td>
          <td class="basic"><% Call Form_RadioPrint("strHosting",strHosting, "1,2,3,4,5,6", "원스탑-Basic,원스탑-Silver,원스탑-Gold,원스탑-BusinessA,원스탑-BusinessB,원스탑-Premium", ",", 3, 0) %>
            <font color="#006699">서비스 신청 후 7일이내 이용료 미입금시 BIZ호스팅 서비스는 중지됩니다.strHosting<br />
홈페이지 무료제작등을 이용하시려면 비용납입 후 가능합니다. </font></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 요금담당자</b></td>
          <td class="basic">성함
            <input type="text" name="strPayName" value="<%=strPayName%>" maxlength="50" style="width:100;" />
            전화
              <input type="text" name="strPayTel" value="<%=strPayTel%>" maxlength="50" style="width:100;" />
              strPayName/strPayTel</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 호스팅자동이체</b></td>
          <td class="basic">은행
            <input type="text" name="strBank" value="<%=strBank%>" maxlength="50" style="width:100;" /> 
            계좌
            <input type="text" name="strBankNo" value="<%=strBankNo%>" maxlength="50" style="width:200;" /> 
            예금주 
            <input type="text" name="strBankName" value="<%=strBankName%>" maxlength="50" style="width:100;" />
            strBank/strBankNo/strBankName</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ <font color="#660000">디자인옵션</font></b></td>
          <td class="basic"><% Call Form_RadioPrint("strDesingOpt",strDesingOpt, "", "무료제작요청,심플제작,이코노미,비지니스,프리미엄 ", ",", 5, 0) %>
            <font color="#993300">홈페이지 옵션별 디자인/플래시/코딩등 비용은 별도 견적을 송부해 드립니다. strDesingOpt </font></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 추가요청사항</b></td>
          <td class="basic"><% call setFCKeditor("strcontent", strcontent, "", 200) %>
            strcontent</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 파일첨부</b></td>
          <td class="basic"><input type = "file" name = "strFile" />
            strFile</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 이용약관 </b></td>
          <td class="basic"><textarea name="약관" rows="10" style="width:100%;"><!--#include file="약관.txt"-->
          </textarea></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 약관동의 </b></td>
          <td class="basic">&nbsp;</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic">&nbsp;</td>
          <td class="basic">&nbsp;</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>==관리==</b></td>
          <td class="basic">&nbsp;</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>&gt; 호스팅계약상태</b></td>
          <td class="basic"><% Call Form_RadioPrint("strHostingState",strHostingState, "", "접수/입금대기, 입금확인/세팅중,세팅완료,서비스해지", ",", 4, 0) %>
            strHostingState</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>&gt; 제작 진행상태</b></td>
          <td class="basic"><% Call Form_RadioPrint("strDesignState",strDesignState, "", "작업대기,사이트맵작성중,시안작업중,시안완료/확인중,시안재작업중,컨텐츠/프로그램작업중,오픈테스트,제작완료", ",", 4, 0) %>
            strDesignState</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#0000FF">+ 최종선택상품</font>
</b></td>
          <td class="basic"><% Call Form_RadioPrint("strDesign",strDesign, "", "무료제작요청,심플제작,이코노미,비지니스,프리미엄 ", ",", 5, 0) %>
            strDesign</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 계약내역상세 </b></td>
          <td class="basic"><% call setFCKeditor("strnote", strnote, "", 200) %>
            strnote</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>계약서 첨부 </b></td>
          <td class="basic"><input type = "file" name = "strFileA" />
            strFileA</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>사업자 첨부 </b></td>
          <td class="basic"><input type = "file" name = "strFileB" />
            strFileB</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">제작계약총액</font></b></td>
          <td class="basic"><input type="text" name="strTotalpay" value="<%=strTotalpay%>" maxlength="50" style="width:100;" />
담당자
  <input type="text" name="strDevName" value="<%=strDevName%>" maxlength="50" style="width:100;" />
  회원ID
  <input type="text" name="strMemID" value="<%=strMemID%>" maxlength="50" style="width:100;" />
  strTotalpay/strDevName/strMemID</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>계약일(등록일)</b></td>
          <td class="basic"><input type="text" name="strSdate" value="<%=strSdate%>" maxlength="50" style="width:100;" /> 
            입금일
            <input type="text" name="strPdate" value="<%=strPdate%>" maxlength="50" style="width:100;" /> 
            완료일
            <input type="text" name="strEdate" value="<%=strEdate%>" maxlength="50" style="width:100;" />
            strSdate/strPdate/strEdate</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>메인디자인</b></td>
          <td class="basic"><input type = "file" name = "strMainFile" />
            strMainFile</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>서브디자인</b></td>
          <td class="basic"><input type = "file" name = "strSubFile" />
            strSubFile</td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>메모</b></td>
          <td class="basic"><% call setFCKeditor("strMemo", strMemo, "", 200) %>
            strMemo          </td>
        </tr>
      </table>
      <table width="100%" height="80" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td align="center"><input name="button" type="button" class="btn" onclick="this.form.onsubmit();" value=" 저장 " />
              <input name="button" type="button" class="btn" onclick="self.location.href = '?<%=getString(Callpage("list",""))%>';" value=" 뒤로 " />
          </td>
        </tr>
      </table>
    </form></td>
  </tr>
</table>
