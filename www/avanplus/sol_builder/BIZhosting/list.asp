<!--#include file = "_config.asp"-->
<%
	
	
	'//검색처리부분
	'//일반과 검색을 위한 설정
	dim where, keyword, keyword_option

	keyword 		= requestS("keyword")
	keyword_option 	= requestS("keyword_option")
	
	'//폼으로 넘어온경우 백로가면 검색화면 페이지표시할수 없음처리
	if len(request.Form("keyword")) > 0 then response.Redirect("?"&GetString("keyword="&keyword&"&keyword_option="&keyword_option))
	

	'//검색일경우 첫페이지로 돌리기 위한 설정
	if len(keyword_option) > 0 then
		where = " intSeq > 0 and "& keyword_option &" like '%"& keyword &"%' "
	else
		where  = " intSeq > 0 "
	end if
	
	
	'//카테고리 값이 넘어오는 경우 처리
	if len(requestQ("mcate")) > 0 then	where = where & " and strCategory = '"& requestQ("mcate") &"' "
	
	
	
	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'## 
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'## 
	dim intTotalCount, intTotalPage
	
	dim intNowPage			: intNowPage 		= Request.QueryString("page")    
    dim intPageSize			: intPageSize 		= 10
    dim intBlockPage		: intBlockPage 		= 10

	dim query_filde			: query_filde		= "*"
	dim query_Tablename		: query_Tablename	= Tablename
	dim query_where			: query_where		= where
	dim query_orderby		: query_orderby		= " order by intSeq DESC "
	call intTotal
	
	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'## 
	'## 
	'#############################################################################

	dim sql, rs
	sql = GetQuery()
	'response.Write(sql)
	call dbopen
	set rs = dbconn.execute(sql)
%>

<table width="100%" height="40" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><%=NavCount%></td>
    <form id="searchForm" name="searchForm" method="post" action="?<%=getString("page=1")%>">
	<td align="right">
        <% Call Form_SelectPrint("keyword_option",keyword_option, "", "strContent,strTitle,strName", "내용,제목,글쓴이", ",", "")%>
        <input name="keyword" type="text" id="keyword" value="<%=keyword%>" />
        <input type="submit" name="Search" value="검색" />
     </td>
    </form>
  </tr>
</table>
<table width="100%" border="1" class="basic">
  <%
	dim pagei : pagei = (intTotalCount-MoveCount)
	'// 글이 없을 경우
	if  rs.eof then
	%>
  <%

	Else		

	%>
  <tr class="basic">
    <td align="center" width="50">번호</td>
    <td align="center" width="100">아이디</td>
    <td align="center" >제목</td>
    <td align="center" width="100">진행상태</td>
    <td align="center" width="80">등록일</td>
  </tr>
  <%	
		rs.move MoveCount 
		Do while not rs.eof												
			intseq			= rs("intseq")
			bizID 			= rs("bizID")
			strTitle		= rs("strTitle")
			dtmInsertDate	= rs("dtmInsertDate")
			strContent		= rs("strContent")
			strDesignState	= rs("strDesignState")
			'intSeq, strCategory, strTitle, dtmInsertDate, strImage, strContent

	%>
  <tr>
    <td width="50" align="center" bgcolor="#FFFFFF" class="basic"><%=pagei%></td>
    <td width="100" align="center" bgcolor="#FFFFFF" class="basic"><a href="?<%=getString("mcate="&strCategory)%>">
      <%=bizID%>
    </a></td>
    <td class="basic" bgcolor="#FFFFFF"><a href="?<%=getString(Callpage("view","intseq="&intseq))%>">
    <%=strTitle%>
      </a></td>
    <td width="100" align="center" bgcolor="#FFFFFF" class="basic"><a href="?<%=getString(Callpage("view","intseq="&intseq))%>">
      <%=strDesignState%>
    </a></td>
    <td width="80" align="center" bgcolor="#FFFFFF" class="basic"><%=left(dtmInsertDate, 10)%></td>
  </tr>
  <%
			pagei = pagei-1
		rs.movenext
		loop
		rs.close()
		set rs = nothing
	End If


	call DbClose()
	%>
</table>
<table width="100%" height="80" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><%call Paging("&keyword="&keyword&"&keyword_option="&keyword_option)%></td>
    <td align="right"><input name="button" type="button" class="btn" onclick="self.location.href = '?<%=getString(Callpage("form","intSeq="))%>';" value=" 입력 " />
    </td>
  </tr>
</table>
