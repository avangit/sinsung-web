<% option explicit%>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
	
'======================================================
'// 프로그램 설명 //
'	기본 프로그램 작성 템플릿입니다.
'	mcate를 지원합니다.
'	(xxx.asp?mcate=구분값으로 하나의 프로그램으로 여러페이지에서 사용가능)
'	작성 : 이덕권 2009년 6월 28일
'======================================================
	'[1]프로그램 고유ID명 작성
	Const ModuleID = "BIZhosting"
'======================================================

	'[2] 폴더경로
	Const ModuleUrl = "/avanplus/sol_builder/BIZhosting/"
	call urlNoEXE(ModuleUrl)
	'[3]테이블명
	Const tablename = "BIZhosting"
	
	'[4]업로드폴더
	Const UploadFolder = "/upload/BIZhosting/"

	
	Dim intSeq,strCategory,dtmInsertDate,strUserid,bizid,bizpw,strTitle,strDomain,strName,strTel,strHP,strEmail,strAddr,strHosting,strPayName,strPayTel,strBank,strBankNo,strBankName,strDesingOpt,strcontent,strHostingState,strDesignState,strDesign,strnote,strTotalpay,strDevName,strMemID,strSdate,strPdate,strEdate,strMemo
	'[5] 추가변수선언
	Dim strFile,strFileA,strFileB,strMainFile,strSubFile 
	
	
	
	Dim listField
	listField = "strCategory,dtmInsertDate,strUserid,bizid,bizpw,strTitle,strDomain,strName,strTel,strHP,strEmail,strAddr,strHosting,strPayName,strPayTel,strBank,strBankNo,strBankName,strDesingOpt,strcontent,strHostingState,strDesignState,strDesign,strnote,strTotalpay,strDevName,strMemID,strSdate,strPdate,strEdate,strMemo,strFile,strFileA,strFileB,strMainFile,strSubFile"
	'[6]추가필드

	
	'/--------------------------------
	'	디비처리와 연동이되는 부분
	'--------------------------------/
	'[7]필드중 일반 데이터 업로드 필드
	const setDataField 	= "strCategory,dtmInsertDate,strUserid,bizid,bizpw,strTitle,strDomain,strName,strTel,strHP,strEmail,strAddr,strHosting,strPayName,strPayTel,strBank,strBankNo,strBankName,strDesingOpt,strcontent,strHostingState,strDesignState,strDesign,strnote,strTotalpay,strDevName,strMemID,strSdate,strPdate,strEdate,strMemo"
	
	'[8]필드중 파일업로드 관련 필드
	const setFileField 	= "strFile,strFileA,strFileB,strMainFile,strSubFile"

	'좀더 내려가보면 [10]까지 있음
'=================================================================================


	
	
	
	
	
	
	
	
	Dim strSQL,intLoop,arrData
	
	
		
	Function Callpage(byval pagename, byval pageAdd)
	'				pagename 불러올 페이지명, pageAdd 추가 스트링
		Callpage = ModuleID & "Mode=" & pagename
		if len(pageAdd) > 0 then Callpage = Callpage & "&" & pageAdd
	End Function
	
	
	Sub idxdata(byval intSeq)	'//글수정과 보기에서 함께 쓰는 함수이므로 별도로 뺌.
		If len(intSeq) > 0 then
		
			Call DbOpen()	
				
				
				strSQL = "SELECT "& listField &" FROM "&tablename& " WHERE intSeq = '"&intSeq&"'"				
				'response.write strsql
				
				
				arrData = getAdoRsArray(strSQL)				
				
				If isArray(arrData) Then							
	'=============================================================					
	' 데이터 불러오기
	' [9]아래 주석을 풀고 화면에 나오는 데이터를 붙여넣으세요.
	'	call codeMake_arrData(listField)
	'=============================================================
					intSeq 			= arrData(0,0)
					strCategory= arrData(0,0)
					dtmInsertDate= arrData(1,0)
					strUserid= arrData(2,0)
					bizid= arrData(3,0)
					bizpw= arrData(4,0)
					strTitle= arrData(5,0)
					strDomain= arrData(6,0)
					strName= arrData(7,0)
					strTel= arrData(8,0)
					strHP= arrData(9,0)
					strEmail= arrData(10,0)
					strAddr= arrData(11,0)
					strHosting= arrData(12,0)
					strPayName= arrData(13,0)
					strPayTel= arrData(14,0)
					strBank= arrData(15,0)
					strBankNo= arrData(16,0)
					strBankName= arrData(17,0)
					strDesingOpt= arrData(18,0)
					strcontent= arrData(19,0)
					strHostingState= arrData(20,0)
					strDesignState= arrData(21,0)
					strDesign= arrData(22,0)
					strnote= arrData(23,0)
					strTotalpay= arrData(24,0)
					strDevName= arrData(25,0)
					strMemID= arrData(26,0)
					strSdate= arrData(27,0)
					strPdate= arrData(28,0)
					strEdate= arrData(29,0)
					strMemo= arrData(30,0)
					strFile= arrData(31,0)
					strFileA= arrData(32,0)
					strFileB= arrData(33,0)
					strMainFile= arrData(34,0)
					strSubFile= arrData(35,0)

	'=============================================================
	' [10]위에 풀어두었던 codeMake_arrData() 주석처리하기
	' End	--설정완료--				
	'=============================================================
				else
				
					response.Write("코드에러")
					
				end if
				
			Call DbClose()
		
		End If
	End Sub
		
	
	


%>