<!--#include file="config.asp"-->
<%
	'/// 권한 설정
	if session("userlevel") < 9 then ErrorBack("회원전용입니다. 로그인후 이용해 주세요.")
	
	'//검색처리부분
	'//일반과 검색을 위한 설정
	dim where, keyword, keyword_option

	keyword 		= requestS("keyword")
	keyword_option 	= requestS("keyword_option")


	'//검색일경우 첫페이지로 돌리기 위한 설정
	if len(keyword_option) > 0 then
		where = " intSeq > 0 and "& keyword_option &" like '%"& keyword &"%' "
	else
		where  = " intSeq > 0 "
	end if
	
	
	'//카테고리 값이 넘어오는 경우 처리
	if len(requestQ(ProgramFolderName)) > 0 then	where = where & " and strCategory = '"& requestQ(ProgramFolderName) &"' "
	
	
	
	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'## 
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'## 
	dim intTotalCount, intTotalPage
	
	dim intNowPage			: intNowPage 		= Request.QueryString("page")    
    dim intPageSize			: intPageSize 		= 10
    dim intBlockPage		: intBlockPage 		= 10

	dim query_filde			: query_filde		= "*"
	dim query_Tablename		: query_Tablename	= Tablename
	dim query_where			: query_where		= where
	dim query_orderby		: query_orderby		= " order by intSeq DESC "
	call intTotal
	
	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'## 
	'## 
	'#############################################################################

	dim sql, rs
	sql = GetQuery()
	'response.Write(sql)
	call dbopen
	set rs = dbconn.execute(sql)
%>



<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><%=NavCount%>	 &nbsp;&nbsp; [<a href="?">전체목록보기</a>]</td>
	<td align="right">
	  <form id="searchForm" name="searchForm" method="get" action="?<%=getString(Callpage("list","page=1"))%>">
          <% Call Form_SelectPrint("keyword_option",keyword_option, "", "strContent,strTitle,strName", "내용,제목,글쓴이", ",", "")%>
          <input name="keyword" type="text" id="keyword" value="<%=keyword%>" />
          <input type="submit" name="Submit2" value="검색" />
	  </form>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<%
	dim pagei : pagei = (intTotalCount-MoveCount)
	'// 글이 없을 경우
	if  rs.eof then
	%>
	<tr>
		<td align="center" height="30" colspan="6" style="border-width:1 0px; border-color:#CCCCCC; border-style:solid;">현재 등록된 글이 없습니다.</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td height="1" colspan="6"> </td>
	</tr>
	<tr bgcolor="#333333">
		<td height="1" colspan="6"> </td>
	</tr>
	<%

	Else		

	%>
	<tr bgcolor="#333333">
		<td align="center" height="30" style="border-width:1 0px; border-color:#CCCCCC; border-style:solid; font-weight:bold;color : ffffff" width="50">번호</td>
		<td align="center" height="30" style="border-width:1 0px; border-color:#CCCCCC; border-style:solid; font-weight:bold;color : ffffff" >제목</td>
		<td width="100" height="30" align="center" style="border-width:1 0px; border-color:#CCCCCC; border-style:solid; font-weight:bold;color : ffffff" >글쓴이</td>
		<td align="center" height="30" style="border-width:1 0px; border-color:#CCCCCC; border-style:solid; font-weight:bold;color : ffffff" width="100">등록일</td>
	</tr>
	<%	
		rs.move MoveCount 
		Do while not rs.eof												
			intseq			= rs("intseq")
			strCategory 	= rs("strCategory")
			strTitle		= rs("strTitle")
			dtmInsertDate	= rs("dtmInsertDate")
			strImage		= rs("strImage")
			strContent		= rs("strContent")
			strName			= rs("strName")
			strCompany		= rs("strCompany")
			'intSeq, strCategory, strTitle, dtmInsertDate, strImage, strContent

	%>
		
	<tr>
		<td align="center" height="30" style="border-width:1 0px; border-color:#CCCCCC; "><%=pagei%></td>
	  <td height="30" style="border-width:1 0px; border-color:#CCCCCC; "><a href="?<%=getString(Callpage("view","intseq="&intseq))%>">
	    <b>[<%=strCompany%>]</b><%=strTitle%></a></td>
		<td align="center" height="30" style="border-width:1 0px; border-color:#CCCCCC; "><a href="?<%=getString(Callpage("view","intseq="&intseq))%>"><%=strName%></a></td>
		<td align="center" height="30" style="border-width:1 0px; border-color:#CCCCCC; "><%=left(dtmInsertDate, 10)%></td>
	</tr>
	<tr>
		<td height="1" bgcolor="#CCCCCC" colspan="6"></td>
	</tr>
	<%
			pagei = pagei-1
		rs.movenext
		loop
		rs.close()
		set rs = nothing
	End If


	call DbClose()
	%>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><%call Paging("&keyword="&keyword&"&keyword_option="&keyword_option)%></td>
    <td align="right">
      <input name="button" type="button" style="border:1px solid #CCCCCC;background-color:#F5F5F5;" onclick="self.location.href = '?<%=getString(Callpage("form","intSeq="))%>';" value=" 입력 " />    </td>
  </tr>
</table>
