<% 
	option explicit
	
	'======================================================
	'// 이 프로그램의 폴더명 
	'// 폴더명이 프로그램의 고유식별자로 쓰임
	'======================================================
	dim ProgramFolderName : ProgramFolderName = "HOMEPQA"
	'======================================================
	
	dim ProgramBasePath	'// 폴더경로
	ProgramBasePath = "/avanplus/sol_builder/"&ProgramFolderName&"/"

	
	Sub basicApp(byVal strCategory)
		
		Dim pagemode	'//보여지는 페이지모드 : 서브쿼리명+mode 로 변수설정
		
		pagemode = Request.QueryString(ProgramFolderName&"Mode")
		

		'// 페이지모드값이 없는경우 첫번째 파일
		If Len(pagemode) = 0 Then	pagemode = "form"
		

		'// 특정 페이지로 가야하는 경우가 있는 경우
		if lcase(pagemode) = "form" then
			Server.Execute(ProgramBasePath & "form.asp")	
		
		Elseif lcase(pagemode) = "list" then
			Server.Execute(ProgramBasePath & "list.asp")	
		
		Elseif lcase(pagemode) = "insert" then
			Server.Execute(ProgramBasePath & "insert.asp")	
		Else
			'response.Write(ProgramBasePath & ProgramFolderName &"/" & pagemode & ".asp")
			'Server.Execute(ProgramBasePath & pagemode & ".asp")		
		End If

		
	End Sub

	
	
	Call basicApp("")
%>
