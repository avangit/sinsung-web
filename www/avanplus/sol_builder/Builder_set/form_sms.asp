<!--#include file="_config.asp"-->
<% call dbopen %>


<form name="frmRequestForm" method="post" action="?<%=getString(Callpage("proc",""))%>" enctype="multipart/form-data">

<div class="contents">
   <table class="tableWrite" width="100%"  cellpadding="0" cellspacing="0">

    <colgroup>
      <col style="width:15%">
      <col style="width:85%">
      </colgroup>  
    <tr>
      <th>SMS 사용 </th>
      <td><% Call Form_RadioPrint("str_smsUse",oBprint("str_smsUse"), "1,0", "예(1),아니오(0)", ",", 2, 0) %><span style="float:left;" class="t_blue">SMS는 별도로 신청하여야만 사용이 가능합니다. (문의:1544-7098)</span> str_smsUse</td>
    </tr>
    <tr>
      <th>회신번호</th>
      <td><input name="str_smsReTelNo" type="text" id="str_smsReTelNo" class="w15 input01" value="<%=oBprint("str_smsReTelNo")%>" maxlength="50" />
      str_smsReTelNo</td>
    </tr>
    <tr>
      <th>SMS계정번호</th>
      <td><input name="str_smsID" type="text" id="str_smsID" class="w15 input01" value="<%=oBprint("str_smsID")%>" maxlength="50" />솔루션호스팅 SMS 서비스계정번호를 입력해주세요.str_smsID</td>
    </tr>
    <tr>
      <th>SMS인증키</th>
      <td><input name="str_smsKey" type="text" id="str_smsKey" class="w50 input01" value="<%=oBprint("str_smsKey")%>" maxlength="50" />
      str_smsKey</td>
    </tr>
  </table>
  </div>
  
<ul class="btnWrap">
	<li><input type="submit" name="Submit"  value=" 저장 " class="btn_m c_blue"></li>
</ul>
</form>
<form name="frmRequestForm" method="post" action="?<%=getString(Callpage("sms_send",""))%>">
<div class="contents"><br>
  <table class="tableWrite" width="100%"  cellpadding="0" cellspacing="0">
    <colgroup>
      <col style="width:15%">
      <col style="width:85%">
      </colgroup>
    <tr>
      <th>수신번호</th>
      <td><input name="hp" type="text" id="hp" class="w20 input01" maxlength="50" />번호로 문자발송 테스트를 합니다.</td>
    </tr>
    <tr>
      <th>문자내용</th>
      <td><textarea class="w15  h100px input02" name="msg" >문자발송 테스트입니다.</textarea></td>
    </tr>
    <tr>
      <th>발송테스트</th>
      <td><input type="submit" name="Submit2"  value=" 발송 " class="btn_m c_blue"></td>
    </tr>
  </table>
</div>
</form>
<div class="tip_box">
  <p>
  문자를 발송할 부분에 다음 코드를 삽입하면 됩니다.<br>
  <strong>call sms(&quot;수신자번호&quot;,&quot;발신자번호&quot;,&quot;발송예약시간&quot;,&quot;메세지&quot;)</strong><br>
  발송예약시간이 없으면 즉시 발송됩니다.<br>발신자번호는 발신번호 목록에 등록되어있어야만 정상적으로 문자가 발송됩니다.
</p>
</div>
<% call dbclose %>
