<%@  codepage="65001" language="VBScript" %>
<% option explicit%>
<% response.charset = "utf-8"%>

<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
	
'======================================================
'// 프로그램 설명 //
'	기본 프로그램 작성 템플릿입니다.
'	mcate를 지원합니다.
'	(xxx.asp?mcate=구분값으로 하나의 프로그램으로 여러페이지에서 사용가능)
'	작성 : 이덕권 2009년 6월 28일
'======================================================
	'[1]프로그램 고유ID명 작성
	Const ModuleID = "Bset"
'======================================================

	'[2] 폴더경로
	Const ModuleUrl = "/avanplus/sol_builder/Builder_set/"
	
	'[3]테이블명
	Const tablename = "Builder_set"
	
	'[4]업로드폴더
	Const UploadFolder = "/upload/"

	call urlNoEXE(ModuleUrl)
	
	'Dim intSeq,strCategory,strTitle,strName,strContent,dtmInsertDate,strUserid,strImage		
	'[5] 추가변수선언
	'//추가필드
	'Dim strEmail,strSiteID,strDomein,strLogo,strMailSendName,strCSTel,strSiteActive,strSMSuse,strSMSReTel,strsmsHP,strsmsID,strsmsKey,strSSLuse,strSSL,strSSLcode,strSSLurl,strNameuse,strNameID,strNameKey,textPublicRaw,textPublicInfo,textMemberRaw,textSiteInfo,textMetatag,textCopy,strCopyLogo
	
	
	
	'Dim listField
	'listField = "intSeq,strCategory,strTitle,strName,strContent,dtmInsertDate,strUserid,strImage"
	'[6]추가필드
	'listField = listField & ",strSiteID,strDomein,strLogo,strMailSendName,strCSTel,strSiteActive,strSMSuse,strSMSReTel,strsmsHP,strsmsID,strsmsKey,strSSLuse,strSSL,strSSLcode,strSSLurl,strNameuse,strNameID,strNameKey,textPublicRaw,textPublicInfo,textMemberRaw,textSiteInfo,textMetatag,textCopy,strCopyLogo"
	
	'/--------------------------------
	'	디비처리와 연동이되는 부분
	'--------------------------------/
	'[7]필드중 일반 데이터 업로드 필드
	'const setDataField 	= "strCategory,strTitle,strName,strContent,dtmInsertDate,strUserid,StrViewUrl,strHelp,StrUse,StrTableneme,StrQuery,strSiteID,strDomein,strMailSendName,strCSTel,strSiteActive,strSMSuse,strSMSReTel,strsmsHP,strsmsID,strsmsKey,strSSLuse,strSSL,strSSLcode,strSSLurl,strNameuse,strNameID,strNameKey,textPublicRaw,textPublicInfo,textMemberRaw,textSiteInfo,textMetatag,textCopy"
	
	'[8]필드중 파일업로드 관련 필드
	'const setFileField 	= "strImage,strLogo,strCopyLogo"

	'좀더 내려가보면 [10]까지 있음
'=================================================================================


	
	
	
	
	
	
	
	
	'Dim strSQL,intLoop,arrData
	
	
		
	Function Callpage(byval pagename, byval pageAdd)
	'				pagename 불러올 페이지명, pageAdd 추가 스트링
		Callpage = ModuleID & "Mode=" & pagename
		if len(pageAdd) > 0 then Callpage = Callpage & "&" & pageAdd
	End Function
	
	
	
%>