<!--#include file="_config.asp"-->
<% call dbopen %>


<form name="frmRequestForm" method="post" action="?<%=getString(Callpage("proc",""))%>" style="margin:0px;" enctype="multipart/form-data">

<table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#F5F5F5"><b>V4.xx 환경설정 </b></td>
    <td bgcolor="#FFFFFF">회원관리 4.0버전</td>
  </tr>
  <tr>
    <td width="100" bgcolor="#F5F5F5">회원가입문자 </td>
    <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("str_mem4Addsms",oBprint("str_mem4Addsms"), "1,0", "가입시자동발송(1),발송안함(0)", ",", 3, 0) %>
      str_mem4Addsms</td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5">회원가입시문자 </td>
    <td bgcolor="#FFFFFF"><input type="text" name="str_mem4Addsms_msg" value="<%=oBprint("str_mem4Addsms_msg")%>" maxlength="50" style="width:100%;" />
      빈값인경우 기본값 : 회원가입을 축하드립니다. str_mem4Addsms_msg</td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5">로그인시메세지</td>
    <td bgcolor="#FFFFFF"><input type="text" name="str_mem4login_msg" value="<%=oBprint("str_mem4login_msg")%>" maxlength="50" style="width:100%;" />
      빈값인경우 기본값 : [$username$]님 정상적으로 로그인 되셨습니다. str_mem4login_msg</td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5">접근제한 메세시</td>
    <td bgcolor="#FFFFFF">접근권한이 없는경우 방문자에게 보여지는 메세지 <br />
        <input type="text" name="str_mem4nolevel_msg" value="<%=oBprint("str_mem4nolevel_msg")%>" maxlength="50" style="width:100%;" />
      빈값인경우 기본값 : 회원전용페이지입니다. 로그인후 이용해 주세요. str_mem4nolevel_msg</td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5"><b>V4.1 환경설정 </b></td>
    <td bgcolor="#FFFFFF"><font color="#FF0000">회원관리 4.1버전 사용시 테이블 필드수정작업이 필요하니 고객센터로 문의바랍니다. </font><br />
      (strNic,strHi,strSmsok,strMailok 필드가 추가됩니다.)str_memVer</td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5">회원관리버전<br /></td>
    <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("str_memVer",oBprint("str_memVer"), "400,410", "Ver 4.0,Ver 4.1", ",", 2, 0) %></td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5">회원가입대상<br /></td>
    <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("str_mem4adultAuth",oBprint("str_mem4adultAuth"), "All,19", " 누구나 가입가능(기본),19세이상만 가입가능", ",", 2, 0) %></td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5">필드항목사용<br />
      ON/OFF</td>
    <td bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
      <tr>
        <td bgcolor="#FFFFFF">필드명</td>
        <td align="center" bgcolor="#FFFFFF">On / Off </td>
        <td bgcolor="#FFFFFF">설명</td>
      </tr>
      <tr>
        <td width="100" bgcolor="#FFFFFF">이름</td>
        <td width="100" align="center" bgcolor="#FFFFFF"><font color="#CC3300">필수항목</font></td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><font color="#000000">닉네임</font></td>
        <td align="center" bgcolor="#FFFFFF"><font color="#000000">
          <% Call Form_RadioPrint("str_mem4onoff_nic",oBprint("str_mem4onoff_nic"), "T,F", "On,Off", ",", 2, 0) %>
        </font></td>
        <td bgcolor="#FFFFFF"><font color="#000000"> <font color="#CC3300">기본값 OFF</font><br />
        닉네임 사용을 하면 로그인시 닉네임으로 로그인 됩니다. </font></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><font color="#000000">주민등록번호</font></td>
        <td align="center" bgcolor="#FFFFFF"><font color="#CC3300">필수항목</font></td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF">아이디/비밀번호</td>
        <td align="center" bgcolor="#FFFFFF"><font color="#CC3300">필수항목</font></td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF">상세주소</td>
        <td align="center" bgcolor="#FFFFFF"><font color="#000000">
          <% Call Form_RadioPrint("str_mem4onoff_addr2",oBprint("str_mem4onoff_addr2"), "T,F", "On,Off", ",", 2, 0) %>
        </font></td>
        <td bgcolor="#FFFFFF"><font color="#000000">기본값 On </font></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF">직업</td>
        <td align="center" bgcolor="#FFFFFF"><font color="#000000">
          <% Call Form_RadioPrint("str_mem4onoff_job",oBprint("str_mem4onoff_job"), "T,F", "On,Off", ",", 2, 0) %>
        </font></td>
        <td bgcolor="#FFFFFF"><font color="#CC3300">기본값 OFF </font></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF">가입인사</td>
        <td align="center" bgcolor="#FFFFFF"><font color="#000000">
          <% Call Form_RadioPrint("str_mem4onoff_hi",oBprint("str_mem4onoff_hi"), "T,F", "On,Off", ",", 2, 0) %>
        </font></td>
        <td bgcolor="#FFFFFF"><font color="#CC3300">기본값 OFF </font></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5">관리자글 글쓴이 <br />
      이미지로 보이기</td>
    <td bgcolor="#FFFFFF"><font color="#000000">
      <% Call Form_RadioPrint("str_mem4nameimg_use",oBprint("str_mem4nameimg_use"), "N,Y1,Y2", "미사용,관라자가쓴 모든글(1),특정글쓴이 글만 변경(2)", ",", 3, 0) %>
      <%bPrint("mem4nameimg")%>
      <input name="img_mem4nameimg" type="file" id="img_mem4nameimg" style="width:300" />
&lt;%bPrint(&quot;img_mem4nameimg&quot;)%&gt;<br />
      (1) 게시판에서 관리자가쓴 모든글의 글쓴이가 관리자명 대신 등록하신 이미지가 보여집니다. <br />
      (2) 게시판에서 글쓴이가
      <input type="text" name="str_mem4nameimg_title" value="<%=oBprint("str_mem4nameimg_title")%>" maxlength="50" style="width:70px;" />
      인 글의 글쓴이에 등록하신 이미지가 보여집니다. <br />
    </font></td>
  </tr>
  <tr>
    <td bgcolor="#F5F5F5"><b>개별로그인사용</b></td>
    <td bgcolor="#FFFFFF">
      <% Call Form_RadioPrint("str_userMemberUse",oBprint("str_userMemberUse"), "T,F", "사용자 개별로그인사용,빌더 기본로그인사용", ",", 2, 0) %>
	  - <font color="#FF0000">개별로그인을 사용하면 상위 로그인관련 설정은 모두 무시됩니다.</font><br />
	  - 홈페이지개별 로그인이 비즈빌더용 모듈로 별도 제작된 확장모듈인 경우 이곳에서 설정하시면 됩니다.<br />
	  - 설정된 값은 홈페이지 모든 로그인페이지에 적용되며, 비즈빌더 확장모듈은 자동업데이트에서 제외됩니다.<br />
	  <b>모듈경로</b><input type="text" name="str_userMemberPath" value="<%=oBprint("str_userMemberPath")%>" maxlength="50" style="width:70%;" />
    </td>
  </tr>
</table>

  <br />
  <center>
    <br />
    <input type="submit" value="      저  장      " style="border:1px solid #CCCCCC;background-color:#F5F5F5;">
    
    
  </center>
</form>

<% call dbclose %>