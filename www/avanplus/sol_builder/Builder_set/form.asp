<!--#include file="_config.asp"-->

<%
Dim strAction

intSeq = request.QueryString("intSeq")

Dim strPageMode : strPageMode = "update"
	
	Call idxdata(intSeq)
	
	'action처리부분
	
	If Len(intSeq) > 0 Then
		strAction = getstring(Callpage("update",""))	'//수정
	Else			
		strAction = getstring(Callpage("insert",""))	'//등록
	End If


%>

<form name="frmRequestForm" method="post" onsubmit="return frmRequestForm_Submit(this);" action="?<%=strAction%>" style="margin:0px;" enctype="multipart/form-data">

<input type="hidden" name="intseq" value="<%=intseq%>">

<table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">
	<tr>
		<td width="100" bgcolor="#F5F5F5"><b>홈페이지명</b></td>
	  <td bgcolor="#FFFFFF"><input type="text" name="strName" value="<%=strName%>" maxlength="50" style="width:100;" /></td>
	</tr>
	<tr>
      <td bgcolor="#F5F5F5"><b>사이트ID</b></td>
	  <td bgcolor="#FFFFFF"><input type="text" name="strSiteID" value="<%=strSiteID%>" maxlength="50" style="width:100;" />
	    strSiteID</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5"><b>메모</b></td>
	  <td bgcolor="#FFFFFF"><% call setFCKeditor("strcontent", strcontent, "", 200) %>      </td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">기본환경설정</td>
	  <td bgcolor="#FFFFFF"><table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">
	    <tr>
          <td bgcolor="#F5F5F5">홈페이지 주도메인</td>
	      <td bgcolor="#FFFFFF">http://
	        <input type="text" name="strDomein" value="<%=strDomein%>" maxlength="50" style="width:50%;" />
	        strDomein</td>
	    </tr>
        <tr>
          <td width="100" bgcolor="#F5F5F5">타이틀</td>
          <td bgcolor="#FFFFFF"><input type="text" name="strTitle" value="<%=strTitle%>" maxlength="50" style="width:100%;" /></td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">로고</td>
          <td bgcolor="#FFFFFF"><%=ImgLmtView(UploadFolder,getFileNumName(strLogo),500) %>
            <input type = "file" name = "strLogo" />
            strLogo</td></tr>
        <tr>
          <td bgcolor="#F5F5F5">하단로고</td>
          <td bgcolor="#FFFFFF"><%=ImgLmtView(UploadFolder,getFileNumName(strCopyLogo),500) %>
            <input type = "file" name = "strCopyLogo" />
            strCopyLogo</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">관리자이메일</td>
          <td bgcolor="#FFFFFF"><% Call Form_EmailPrint("strEmail",strEmail)%></td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">메일발송시 이름 </td>
          <td bgcolor="#FFFFFF"><input type="text" name="strMailSendName" value="<%=strMailSendName%>" maxlength="50" style="width:50%;" />
            strMailSendName</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">고객센터전화번호</td>
          <td bgcolor="#FFFFFF"><input type="text" name="strCSTel" value="<%=strCSTel%>" maxlength="50" style="width:50%;" />
            strCSTel</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">홈페이지첫페이지</td>
          <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strSiteActive",strSiteActive, "1,2,3", "공사중,운영중,로그인화면", ",", 3, 0) %>
            strSiteActive<br />
            공사중인경우 http:// <%=strDomein%>/main.asp 로 접속하시면 수정중인 홈페이지를 보실수 있습니다. </td>
        </tr>
      </table></td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">SMS 설정 </td>
	  <td bgcolor="#FFFFFF"><table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">

        <tr>
          <td width="100" bgcolor="#F5F5F5">SMS 사용 </td>
          <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strSMSuse",strSMSuse, "1,0", "예,아니오", ",", 2, 0) %>
            strSMSuse</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">회신번호</td>
          <td bgcolor="#FFFFFF"><input type="text" name="strSMSReTel" value="<%=strSMSReTel%>" maxlength="50" style="width:50%;" />
            strSMSReTel</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">관리자핸드폰</td>
          <td bgcolor="#FFFFFF"><% Call Form_HandphonePrint("strsmsHP",strsmsHP)%>
            strsmsHP</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">SMS인증아이디</td>
          <td bgcolor="#FFFFFF"><input type="text" name="strsmsID" value="<%=strsmsID%>" maxlength="50" style="width:50%;" />
            strsmsID</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">SMS인증키</td>
          <td bgcolor="#FFFFFF"><input type="text" name="strsmsKey" value="<%=strsmsKey%>" maxlength="50" style="width:50%;" />
            strsmsKey</td>
        </tr>
      </table></td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">SSL 설정</td>
	  <td bgcolor="#FFFFFF"><table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">
          <tr>
            <td width="100" bgcolor="#F5F5F5">SSL 사용 </td>
            <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strSSLuse",strSSLuse, "1,0", "예,아니오", ",", 2, 0) %>
              strSSLuse</td>
          </tr>
          <tr>
            <td bgcolor="#F5F5F5">SSL 종류 </td>
            <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strSSL",strSSL, "1,0", "예,아니오", ",", 2, 0) %>
              strSSL</td>
          </tr>
          <tr>
            <td bgcolor="#F5F5F5">인증씰 설치 코드 </td>
            <td bgcolor="#FFFFFF"><textarea name="strSSLcode" rows="5" style="width:100%;"><%=strSSLcode%></textarea>
              strSSLcode</td>
          </tr>
          <tr>
            <td bgcolor="#F5F5F5">SSL경로</td>
            <td bgcolor="#FFFFFF"><input type="text" name="strSSLurl" value="<%=strSSLurl%>" maxlength="50" style="width:50%;" />
              strSSLurl</td>
          </tr>
      </table></td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">실명인증</td>
	  <td bgcolor="#FFFFFF"><table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">
        <tr>
          <td width="100" bgcolor="#F5F5F5">실명인증 사용 </td>
          <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strNameuse",strNameuse, "1,0", "예,아니오", ",", 2, 0) %>
            strNameuse</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">계정번호</td>
          <td bgcolor="#FFFFFF"><input type="text" name="strNameID" value="<%=strNameID%>" maxlength="50" style="width:50%;" />
            strNameID</td>
        </tr>
        <tr>
          <td bgcolor="#F5F5F5">실명인증키</td>
          <td bgcolor="#FFFFFF"><input type="text" name="strNameKey" value="<%=strNameKey%>" maxlength="50" style="width:50%;" />
            strNameKey</td>
        </tr>

      </table></td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">개인정보취급방침</td>
	  <td bgcolor="#FFFFFF"><% call setFCKeditor("textPublicRaw", textPublicRaw, "", 200) %>
	    textPublicRaw</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">개인정보수집 및 안내 </td>
	  <td bgcolor="#FFFFFF"><% call setFCKeditor("textPublicInfo", textPublicInfo, "", 200) %>
	    textPublicInfo</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">회원가입약관</td>
	  <td bgcolor="#FFFFFF"><% call setFCKeditor("textMemberRaw", textMemberRaw, "", 200) %>
	    textMemberRaw</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">홈페이지 이용안내 </td>
	  <td bgcolor="#FFFFFF"><% call setFCKeditor("textSiteInfo", textSiteInfo, "", 200) %>
	    textSiteInfo</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">메타태그설정</td>
	  <td bgcolor="#FFFFFF"><textarea name="textMetatag" rows="5" style="width:100%;"><%=textMetatag%></textarea>
textMetatag</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">카피라이트</td>
	  <td bgcolor="#FFFFFF"><% call setFCKeditor("textCopy", textCopy, "", 200) %>
	    textCopy</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
      <td bgcolor="#F5F5F5">&nbsp;</td>
	  <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
	<tr>
		<td bgcolor="#F5F5F5">&nbsp;</td>
		<td bgcolor="#FFFFFF">&nbsp;</td>
	</tr>	
</table>

<div style="text-align:center; padding:10px;">
  <input type="button" value=" 저장 " onclick="this.form.onsubmit();" style="border:1px solid #CCCCCC;background-color:#F5F5F5;">
	<input type="button" value=" 뒤로 " onclick="self.location.href = '?<%=getString(Callpage("list",""))%>';" style="border:1px solid #CCCCCC;background-color:#F5F5F5;">
</div>

</form>

<script language="javascript">
<!--

	function frmRequestForm_Submit(frm){
		if ( frm.strTitle.value.replace(/ /gi, "") == "" ) { alert("제목을 입력해주세요"); frm.strTitle.focus(); return false; }
		<%If Len(intSeq) = 0 Then%>
		//if ( frm.strimage.value.replace(/ /gi, "") == "" ) { alert("이미지를 입력해주세요"); frm.strimage.focus(); return false; }
		<%End If%>
		//if ( frm.strContent.value.replace(/ /gi, "") == "" ) { alert("의뢰인을 입력해주세요"); frm.strContent.focus(); return false; }
		frm.submit();
	}

//-->
</script>