<% option explicit %>
<!--#include virtual = "/AVANplus/_Function.asp" -->
<% call dbopen %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>★☆ 관리자페이지 ★☆</title>
<link rel='stylesheet' type='text/css' href='_builder.css'>
</head>

<%
	dim pagecode : pagecode = Request.QueryString("pagecode")
	if session("userid") = ""  then 
		server.Execute("/AVANplus/modul/member4/call.asp")
		response.End()
	end if
%>


<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
</head>

<body style="background-color: #f5f1e3;">
<table width="976" height="100" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="9"></td>
    </tr>
    <tr>
        <td align="right"><!-- 탑 --><table width="966" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="198" align="center"><a href = '/avanplus/sol_builder/admin.asp'><img src="/avanplus/modul/customer/img/free_logo2.gif" border="0">
                </a></td>
                <td width="14" rowspan="5" background="/AVANplus/sol_builder/main/T_box_left.gif"></td>
                <td width="740" valign="top" bgcolor="355C87"><table width="740" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="66"><font color="#80A4CC"><b>[Biz호스팅사용권]</b><br>
                        Biz호스팅을 사용하시면 Biz호스팅에서 제공하는 다양한 웹프로그램을 무료로 이용하실 수 있습니다.<br>
                      Biz호스팅 이용중 타 업체 이전시 사용중인 모든 웹프로그램은 이용이 불가능합니다.(데이터자료는 이전가능) </font></td>
                    </tr>
                    
                </table></td>
                <td width="13" rowspan="5" background="/AVANplus/sol_builder/main/T_box_right.gif"></td>
            </tr>
            
            <tr>
                <td rowspan="4"><img src="/AVANplus/sol_builder/main/T_tit.gif" width="199" height="63"></td>
                <td height="22" valign="top" background="/AVANplus/sol_builder/main/T_box_middle1.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><font color="#FFFFFF">관리자페이지 V2.0 Beta</font></td>
                    <td align="right"><font color="#FFFFFF"><%=session("username")%>님[Lv.<%=session("userlevel")%>] 로그인 |</font>
                      <a href="?module=login&member4=logoutok"><font color="#FFFFFF">로그아웃</font></a>
                      <a href="/avanplus/_manager">메니저사이트</a>
					  </td>
                  </tr>
                </table></td>
            </tr>
			<tr>
                <td height="5" valign="bottom"></td>
            </tr>
            <tr>
                <td height="22"><!-- 탑 고정메뉴 --><table width="717" border="0" align="center" cellpadding="0" cellspacing="0">
                   
					<tr>
					  <td><a href="?">관리자메인</a> | <a href="?module=member4">회원관리</a> | <a href="?module=avanboard_v3">게시판관리</a>
				        |
				        <a href="?module=mdl_Popup_v1">
				        팝업관리
				        </a>
				        | 
				        <a href="?module=count_v1">통계</a>
				        | 
					    <a href="http://cafe.naver.com/apphost" target="_blank">
					    무료기술지원
					    </a>
|  
					     <font color="#003366"><b>1:1A/S신청</b></font> |
					     <a href="/avanplus/_Sample/01/" target="_blank">
					     빌더샘플
                        </a> 
				        | 
				        <a href="/" target="_blank">
				        홈페이지 바로가기
				        </a></td>
					  <td>&nbsp;</td>
					</tr>
										
					
                </table></td>
            </tr>
            <tr>
                <td height="14" background="/AVANplus/sol_builder/main/T_box_middle2.gif"></td>
            </tr>
        </table></td>
    </tr>
    <tr>
        <td height="8"></td>
    </tr>
	<tr>
        <td align="right"><table width="961" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="200" valign="top"><!-- left -->
					<table width="194" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td><table width="194" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="8"><img src="/AVANplus/sol_builder/main/L_box_top.gif" width="194" height="8"></td>
								</tr>
								<tr>
									<td height="10" background="/AVANplus/sol_builder/main/L_box_middle.gif"></td>
								</tr>
								<tr>
								  <td background="/AVANplus/sol_builder/main/L_box_middle.gif"><!-- 메뉴 -->
									  <table width="156" border="0" align="center" cellpadding="0" cellspacing="0">
									    <tr>
                                          <td height="20" bgcolor="#284566">&nbsp;</td>
									      <td bgcolor="#284566"><b><font color="#FFFFFF">기본 설정</font></b> </td>
								        </tr>
									    <tr>
                                          <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
									      <td><a href="?module=builderid">
									        빌더ID 설정
									        </a>                                          </td>
								        </tr>
										  <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
										    <td><a href="?module=login&member4=modify">관리자 정보변경 </a></td>
									      </tr>
										  <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
										    <td><a href="?module=sitemenu">
										      <b>홈페이지메뉴설정										      </b>
										    </a></td>
									    </tr>
										  <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
										    <td><a href="?module=sitemap">
										      <b>사이트맵보기</b>
										      </a></td>
									    </tr>
										  <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
										    <td><a href="?module=siteedit">
                                              <b>페이지 내용 수정</b>
                                            </a></td>
									    </tr>
										  <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
										    <td><a href="?module=moduleList">
										    모듈(프로그램)목록
										    </a></td>
									    </tr>
									  </table>
										<table width="156" border="0" align="center" cellpadding="0" cellspacing="0">
										  <tr>
                                            <td height="20" bgcolor="#284566">&nbsp;</td>
										    <td bgcolor="#284566"><b><font color="#FFFFFF">빌더사용설정 </font></b> </td>
									      </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=builderbasic">홈페이지 기본환경 설정 </a></td>
                                          </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=buildermeta">메타테그설정</a></td>
                                          </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=buildersms">
                                              SMS 설정
                                            </a></td>
                                          </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=builderssl">
                                              SSL 설정
                                            </a></td>
                                          </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=builderpublic">
                                              개인정보취급방침
                                            </a></td>
                                          </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=builderjoinraw">
                                              회원가입약관
                                            </a></td>
                                          </tr>
                                        </table>
										<table width="156" border="0" align="center" cellpadding="0" cellspacing="0">
										  <tr>
                                            <td width="12" height="20" bgcolor="#284566">&nbsp;</td>
										    <td bgcolor="#284566"><b><font color="#FFFFFF">세팅된 프로그램 </font></b> </td>
									      </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=moduleList">
                                            모듈(프로그램)관리
                                            </a></td>
                                          </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=siteid">
                                            사이트전체관리
                                            </a></td>
                                          </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td><a href="?module=info_db">
                                            디비관리
                                            </a></td>
                                          </tr>
                                          <tr>
                                            <td height="20"><img src="/AVANplus/sol_builder/main/L_icon.gif" width="12" height="13"></td>
                                            <td>&nbsp;</td>
                                          </tr>
                                        </table>
								  </td>
								</tr>
								<tr>
									<td height="10" background="/AVANplus/sol_builder/main/L_box_middle.gif"></td>
								</tr>
								<tr>
									<td height="8"><img src="/AVANplus/sol_builder/main/L_box_bottom.gif" width="194" height="8"></td>
								</tr>
							</table></td>
						</tr>
						<tr>
                          <td><!-- include virtual = "/gagachatall.asp" --></td>
						</tr>
						<tr>
                          <td></td>
						</tr>
						<tr>
							<td><div align="center"></div></td>
						</tr>												
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
                <td width="761" valign="top">
					<table width="761" height="300" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="7" height="7" background="/AVANplus/sol_builder/main/R_box02.gif"></td>
						</tr>
						<tr>
							<td valign="top" bgcolor="#FFFFFF"><table width="750" border="0" align="center" cellpadding="10">
                              <tr>
                                <td>
								
<%
	'// 프로그램 불러오는 부분
	dim module : module = request.QueryString("module")


	if lcase(module) = "member4"  then server.Execute("/AVANplus/modul/member4/calladmin.asp")
	if lcase(module) = "login"  then server.Execute("/AVANplus/modul/member4/call.asp")
	
	if lcase(module) = "count_v1"  then server.Execute("/AVANplus/modul/count_v1/admincount.asp")
	
	if lcase(module) = "avanboard_v3"  then server.Execute("/AVANplus/modul/avanboard_v3/calladmin.asp")
	if lcase(module) = "board"  then server.Execute("/AVANplus/modul/avanboard_v3/board.asp")
	
	if lcase(module) = "modulelist"  then server.Execute("/AVANplus/sol_builder/_module/call.asp")
	
	if lcase(module) = "sitemenu"  then server.Execute("/AVANplus/sol_builder/CallSitemenu.asp")
	if lcase(module) = "sitemap"  then server.Execute("/AVANplus/sol_builder/Callsitemap.asp")
	if lcase(module) = "siteedit"  then server.Execute("/AVANplus/sol_builder/CallsiteEdit.asp")
	
	if lcase(module) = "help"  then server.Execute("/AVANplus/sol_builder/Callhelp.asp")
	
	if lcase(module) = "mdl_popup_v1"  then server.Execute("/AVANplus/modul/mdl_Popup_v1/Calladmin.asp")
	
	'// 홈페이지 설정호출
	if lcase(module) = "siteid"  		then server.Execute("/AVANplus/sol_builder/builder_set/Calladmin.asp")
	if lcase(module) = "buildersms"  	then server.Execute("/AVANplus/sol_builder/builder_set/Call_sms.asp")
	if lcase(module) = "builderssl"  	then server.Execute("/AVANplus/sol_builder/builder_set/Call_ssl.asp")
	if lcase(module) = "builderpublic"  then server.Execute("/AVANplus/sol_builder/builder_set/Call_public.asp")
	if lcase(module) = "builderjoinraw" then server.Execute("/AVANplus/sol_builder/builder_set/Call_joinraw.asp")
	if lcase(module) = "builderbasic"  	then server.Execute("/AVANplus/sol_builder/builder_set/Call_basic.asp")
	if lcase(module) = "buildermeta"  	then server.Execute("/AVANplus/sol_builder/builder_set/Call_meta.asp")
	if lcase(module) = "builderid"  	then server.Execute("/AVANplus/sol_builder/builder_set/Call_id.asp")
	
	if lcase(module) = "info_db"  	then server.Execute("/AVANplus/sol_builder/_systemInfo/info_db.asp")
	
	
	if lcase(module) = ""  	then server.Execute("adminMain.asp")
%>								
								
								
								
								
								
								
								
								
								
								</td>
                              </tr>
                            </table></td>
						    <td width="7" background="/AVANplus/sol_builder/main/R_box05.gif"></td>
						</tr>
						<tr>
							<td height="7" background="/AVANplus/sol_builder/main/R_box07.gif"></td>
						</tr>
				  </table>
					
				</td>
            </tr>
        </table></td>
    </tr>
    <tr>
        <td height="8"></td>
    </tr>
    <tr>
        <td align="right"><!-- copy --><table width="966" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><img src="/AVANplus/sol_builder/main/Copy.gif" width="966" height="52"></td>
            </tr>
        </table></td>
    </tr>
</table>
</body>
</html>
<% call dbclose %>