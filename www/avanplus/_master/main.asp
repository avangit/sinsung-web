

	<!-- #include file="_inc/head.asp" -->
</head>
<body>
	<div id="A_Wrap">
		<div id="A_Header"> 
			<!-- #include file="_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container_L">
				<!-- #include file="_inc/left_menu.asp" -->
			</div>
			<div id="A_Container_C">
				<p class="main_nav">회원관리 > 회원관리</p>
				<h2 class="main_tit">회원관리</h2>
				<div class="admin">
					<div>
						<form>
							<fieldset>
								<legend>회원관리 검색</legend>
								<table class="table">
									<caption>회원관리 검색</caption>
									<colgroup>
										<col style="width:260px" />
										<col style="width:*" />
										<col style="width:260px" />
										<col style="width:*" />
									</colgroup>
									<tbody>
										<tr>
											<th>검색어</th>
											<td colspan="3" class="left">
												<select>
													<option>이름</option>
													<option>연락처</option>
													<option>상담내역</option>
												</select>
												<input type="text">
												<button type="button" class="btn btn_m bg_gray">검색</button>
											</td>
										</tr>
										<tr>
											<th>회원등급</th>
											<td class="left">
												<select>
													<option>전체</option>
													<option>유료</option>
													<option>미연장</option>
												</select>
											</td>
											<th>회원상태</th>
											<td class="left">
												<select>
													<option>최근접속</option>
													<option>기본</option>
													<option>만족도높음</option>
												</select>
											</td>
										</tr>
									</tbody>
								</table>
							</fieldset>
						</form>
					</div>
					<div class="pt_30">
						<div class="right mb_20">
							<select class="w10">
								<option>최신</option>
								<option>신규</option>
								<option>유료시작일</option>
								<option>종료임박</option>
								<option>가입일</option>
								<option>사용일</option>
							</select>
							<select class="w10">
								<option>25개 보기</option>
								<option>50개 보기</option>
								<option>100개 보기</option>
								<option>250개 보기</option>
								<option>500개 보기</option>
								<option>1000개 보기</option>
								<option>2000개 보기</option>
							</select>
						</div>
						<table class="table">
							<caption>회원관리</caption>
							<colgroup>
								<col style="width:*" />
								<col style="width:*" />
								<col style="width:*" />
								<col style="width:*" />
								<col style="width:*" />
								<col style="width:*" />
								<col style="width:*" />
								<col style="width:*" />
								<col style="width:*" />
								<col style="width:200px" />
							</colgroup>
							<thead>
								<tr>
									<th>
										<input type="checkbox" id="check01">
										<label for="check01">
											<span></span>
										</label>
									</th>
									<th>관리자</th>
									<th>회원명</th>
									<th>연락처</th>
									<th>유료시작일</th>
									<th>유료종료일</th>
									<th>최소등록일</th>
									<th>회원등급</th>
									<th>회원상태</th>
									<th>관리</th>
								</tr>	
							</thead>
							<tbody>
								<tr>
									<td>
										<input type="checkbox" id="check02">
										<label for="check02">
											<span></span>
										</label>
									</td>
									<td>영업사원</td>
									<td>회원명</td>
									<td>010-0000-0000</td>
									<td>YYYY-MM-DD</td>
									<td>YYYY-MM-DD</td>
									<td>YYYY-MM-DD</td>
									<td>유료</td>
									<td>기본</td>
									<td>
										<a href="#" class="btn btn_m bg_gray">관리</a>
										<a href="#" class="btn btn_m bg_gray">발송이력</a>
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" id="check02">
										<label for="check02">
											<span></span>
										</label>
									</td>
									<td>영업사원</td>
									<td>회원명</td>
									<td>010-0000-0000</td>
									<td>YYYY-MM-DD</td>
									<td>YYYY-MM-DD</td>
									<td>YYYY-MM-DD</td>
									<td>미가입</td>
									<td>부재-부재중</td>
									<td>
										<a href="#" class="btn btn_m bg_gray">관리</a>
										<a href="#" class="btn btn_m bg_gray">발송이력</a>
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" id="check02">
										<label for="check02">
											<span></span>
										</label>
									</td>
									<td>영업사원</td>
									<td>회원명</td>
									<td>010-0000-0000</td>
									<td>YYYY-MM-DD</td>
									<td>YYYY-MM-DD</td>
									<td>YYYY-MM-DD</td>
									<td>미연장</td>
									<td>중도해지</td>
									<td>
										<a href="#" class="btn btn_m bg_gray">관리</a>
										<a href="#" class="btn btn_m bg_gray">발송이력</a>
									</td>
								</tr>
								<tr>
									<td colspan="12">
										<div class="f_left">
											<select>
												<option>재접촉 - 통화불가</option>
												<option></option>
												<option></option>
											</select>
											<button type="button" class="btn btn_l bg_gray">상태변경</button>
										</div>
										<div class="f_right">
											검색된 조건에 만족하는 회원에게 전송 <button type="button" class="btn btn_m bg_gray">메시지 전송</button>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="list_page">
							<a href="#" class="first_btn">처음</a><a href="#" class="first2_btn">이전</a>
								<ul>
									<li><a class="on" href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
								</ul>
							<a class="last2_btn" href="#">다음</a><a class="last_btn" href="#">맨뒤</a>
						</div>	
                        
                        
                        <div class="center mt_20">
				<button type="button" class="btn_write">글쓰기</button>  <button type="button" class="btn_list">목록</button>
                <button type="button" class="btn_save">저장</button>  <button type="button" class="btn_edit">수정</button>
                <button type="button" class="btn_del">삭제</button>  <button type="button" class="btn_ok">확인</button>   <button type="button" class="btn_cancel">취소</button>
			</div>
            <br /><br />
            <div class="f_left">
				<button type="button" class="btn_write">글쓰기</button>  <button type="button" class="btn_list">목록</button>
                <button type="button" class="btn_save">저장</button>  <button type="button" class="btn_edit">수정</button>
                <button type="button" class="btn_del">삭제</button>  <button type="button" class="btn_ok">확인</button>   <button type="button" class="btn_cancel">취소</button>
			</div>
            
             <div class="f_right">
				<button type="button" class="btn_write btn_xl bg_orange font_l btn_r">글쓰기</button>  <button type="button" class="btn_list btn_s">목록</button>
                <button type="button" class="btn_save btn_xs bg_white font_xs">저장</button>  <button type="button" class="btn_edit btn_l bg_green">수정</button>
                <button type="button" class="btn_del bg_pink">삭제</button>  <button type="button" class="btn_ok bg_yellow">확인</button>   <button type="button" class="btn_cancel bg_black">취소</button>
			</div>
            
			<br /><br /><br /><br /> 
            <span class="font_red pd_m">font_red</span>  <span class="font_green">font_green</span>  <span class="font_blue">font_blue</span>   <span class="font_yellow">font_yellow</span>   <span class="font_orange">font_orange</span>   <span class="font_black">font_black</span>   <span class="font_gray">font_gray</span>            
			<br /><br /><br /><br /> 
            
            <span class="font_point">font_point</span> <br /><br /><br /><br /> 
            <span class="font_xl">font_xl</span> <span class="font_l">font_l</span> <span class="font_m">font_m</span> <span class="font_s">font_s</span> <span class="font_xs">font_xs</span>
                        
                        
					</div>
				</div>
			</div> 
		</div>  
		<div id="A_Footer"> 
			<!-- #include file="_inc/footer.asp" -->
		</div>
	</div>
</body>
</html>
