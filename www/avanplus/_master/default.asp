<!doctype html>
<html lang="ko">
<head>
<title>홈페이지 관리자</title>
<meta charset="UTF-8">
<meta name="Generator" content="EditPlus®">
<meta name="Author" content="">
<meta name="Keywords" content="">
<meta name="Description" content="">
<link rel="stylesheet" href="_css/default.css" />

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->


<script>
//$(document).ready(function() {

//		$('.loginForm li').click(function(){
//			$(this).find('label').hide();
//		});
//});

function login(){
			if ( document.frmRequestForm.strId.value.replace(/ /gi, "") == "" ) { alert("아이디를 입력해주세요"); document.frmRequestForm.strId.focus(); return; }
			if ( document.frmRequestForm.strPwd.value.replace(/ /gi, "") == "" ) { alert("비밀번호를 입력해주세요"); document.frmRequestForm.strPwd.focus(); return; }
			document.frmRequestForm.submit();
		}
</script>


</head>

<body>
<div id="container" style="background:#f5f6f7">
	<div id="login">
		<h1>ADMINISTRATOR <span>LOGIN</span></h1>
		<div class="loginForm">

            <form name="frmRequestForm" id="frmRequestForm"  method="post"   action="loginOk.asp" onKeydown="javascript:if(event.keyCode == 13) login();" >
            <ul>
				<li><input type="text" name="strId" class="input01" placeholder="관리자아이디"></li>
				<li><input type="password" name="strPwd" class="input01" placeholder="비밀번호"></li>
			</ul>
			<div><a href="javascript:login();"   >로그인</a></div>
			<p>이곳은 관리자 영역입니다. 권한이 없으면 돌아가 주시기 바랍니다.<br>귀하의 접속 아이피는 <font color="red"><%=Request.ServerVariables("REMOTE_ADDR")%></font>입니다.</p>
            </form>

		</div>
	</div>
</div>
</body>
</html>
