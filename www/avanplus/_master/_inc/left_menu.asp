<div class="lnb">
	<ul class="nav">
		<li>
			<a href="#"><em></em><span>대시보드</span></a>
			<ul class="ss_menu">
				<li><a href="#">대시보드</a></li>
			</ul>
		</li>
<%menuGroup = "m01"%>
		<li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>>
			<a href="#"><em></em><span>회원관리</span></a>
			<ul class="ss_menu">
<%				call menu("회원관리","/avanmodule/member4/call.asp","")
				call menu("SMS발송","/avanmodule/sms/call.asp","smsmode=form")
				call menu("메일발송","/avanmodule/mail/call.asp","mailmode=form")
				call menu("개인정보접속기록","/avanmodule/record/call.asp","recordmode=list")
				call menu("운영자관리권한기록","/avanmodule/record/call.asp","recordmode=list2")
				call menu("개인정보처리방침","/avanmodule/record/call.asp","recordmode=list3")
				call menu("약관/개인정보","/avanmodule/record/call.asp","recordmode=list4")
%>
			</ul>
		</li>
<%menuGroup = "m02"%>
		<li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>>
			<a href="#"><em></em><span>제품관리</span></a>
			<ul class="ss_menu">
<%				call menu("상품관리","/avanmodule/goods/call.asp","")
%>
				<li><a href="/new_admin/goods/product_write.asp">상품등록</a></li>
				<li><a href="/new_admin/qna/list.asp">1:1문의관리</a></li>
				<li><a href="/new_admin/review/list.asp">상품후기관리</a></li>
			</ul>
		</li>
		<li>
			<a href="#"><em></em><span>적립금</span></a>
			<ul class="ss_menu">
				<li><a href="/new_admin/point/point.asp">적립금현황</a></li>
			</ul>
		</li>
		<li>
			<a href="#"><em></em><span>매출통계</span></a>
			<ul class="ss_menu">
				<li><a href="/new_admin/stats/stats.asp">매출통계</a></li>
			</ul>
		</li>
		<li>
			<a href="#"><em></em><span>주문관리</span></a>
			<ul class="ss_menu">
				<li><a href="/new_admin/order/summary.asp">요약정보</a></li>
				<li><a href="/new_admin/order/order_list.asp">전체리스트</a></li>
				<li><a href="/new_admin/order/order_list.asp?result=1">신규[미입금]리스트</a></li>
				<li><a href="/new_admin/order/order_list.asp?result=2">배송준비리스트</a></li>
				<li><a href="/new_admin/order/order_list.asp?result=3">배송중 리스트</a></li>
				<li><a href="/new_admin/order/order_list.asp?result=4">반품 리스트</a></li>
				<li><a href="/new_admin/order/order_list.asp?result=5">취소 리스트</a></li>
				<li><a href="/new_admin/order/order_list.asp?result=6">환불 리스트</a></li>
			</ul>
		</li>
		<li>
			<a href="#"><em></em><span>제품카테고리</span></a>
			<ul class="ss_menu">
				<li><a href="/new_admin/cate/cateAdmin.asp">제품카테고리</a></li>
			</ul>
		</li>
		<li>
			<a href="#"><em></em><span>게시판</span></a>
			<ul class="ss_menu">
<%dim boardrs
	set boardrs = dbconn.execute("select * from boardset_v2")

		if not boardrs.eof then
			do while not boardrs.eof
				call menu(boardrs("bs_name"),"/AVANplus/modul/avanboard_v3/call2.asp","bs_code="&boardrs("bs_code"))
			boardrs.movenext
			loop
		end If
%>
			</ul>
		</li>
		<li>
			<a href="#"><em></em><span>홈페이지환경설정</span></a>
			<ul class="ss_menu">
<%
					call menu("게시판관리","/AVANplus/modul/avanboard_v3/calladmin.asp","")

					call menu("sms환경설정","/new_admin/sms/set.asp","")
%>
			</ul>
		</li>
		<!--<li>
			<a href="#"><em></em><span>DB Tool</span></a>
			<ul class="ss_menu">
				<li><a href="#">테이블명세서</a></li>
			</ul>
		</li>-->
	</ul>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$(".nav").navgoco({accordion: true});
});
</script>