<!-- METADATA TYPE="typeLib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->

<Object RUNAT="Server" PROGID="ADODB.Connection" ID="DbConn"></Object><%
Response.CharSet="utf-8"
Session.codepage="65001"
Response.codepage="65001"
Response.ContentType="text/html;charset=utf-8"
Response.buffer=true
Response.Expires = 0

	'--------------------------------------------------------------------------
	'	아반소프트 AVANfuncton v4.0
	'	빌더용 함수모음.

	'	함수ID AVANplus_04_builder

	' 	본 프로그램은 납품용으로 제공된것이며 재판매 될 수 없습니다.
	'	본 프로그램을 재판매 등 상업적으로 사용할 경우 별도 협의가 필요합니다.
	'	문의 1544-7098 / lee@avansoft.co.kr
	'----------------------------------------------------------------------------


	' 캐슁되지 않고 새로 파일을 서버로부터 받음
	Response.Expires = -1
	Response.CacheControl = "no-cache"
	Response.AddHeader "Pragma", "no-cache"
	'Response.Write now()

	' 중국 해커 차단
	if Instr(LCASE(Request.ServerVariables("QUERY_STRING")) , LCase("DECLARE") )> 0 then
		response.end
	end if

%><!-- #include virtual="/DbConn.asp" --><%

	Function ErrorBack(strMsg)
		If DbConn.State = adStateOpen Then
			DbConn.Close()
		End If
		JsAlertAction strMsg, "self.history.back(-1);"
		Response.End()

	End Function


	'// 디비열기
	Function DbOpen()
		'dim cstConnString	'//디비커넥션변수

		If DbConn.State = adStateClosed Then
			DbConn.open cstConnString
		End If
	End Function

	'// 디비닫기
	Function DbClose()
		If DbConn.State = adStateOpen Then
			DbConn.Close()
		End If
	End Function


	'// 쿼리 실행
	Sub AdoConnExecute(ByVal strQuery)

		'Print strQuery

		DbConn.Execute strQuery, , adCmdText + adExecuteNoRecords

	End Sub

	'// 쿼리 실행후 레코드셋 배열로 반환 없음 Null 반환
	Function getAdoRsArray(ByVal strQuery)

		Dim objAdoRs, arrSelectData

		'Print strQuery

		Set objAdoRs = DbConn.Execute(strQuery)'Server.CreateObject("ADODB.RecordSet")

			'objAdoRs.Open strQuery, DbConn, adOpenForwardOnly, adLockReadOnly, adCmdText
				If Not (objAdoRs.EOF And objAdoRs.BOF) Then
					arrSelectData = objAdoRs.GetRows()
				Else
					arrSelectData = Null
				End If
			'objAdoRs.Close

		Set objAdoRs = Nothing

		getAdoRsArray = arrSelectData

	End Function

	'// 쿼리 실행후 스칼라값 반환 없음 Null 반환
	Function getAdoRsScalar(ByVal strQuery)

		Dim objAdoRs, strValue

		'Print strQuery

		Set objAdoRs = DbConn.Execute(strQuery)'Server.CreateObject("ADODB.RecordSet")

			'objAdoRs.Open strQuery, DbConn, adOpenForwardOnly, adLockReadOnly, adCmdText
				If Not (objAdoRs.EOF And objAdoRs.BOF) Then
					strValue = objAdoRs(0)
				Else
					strValue = Null
				End If
			'objAdoRs.Close

		Set objAdoRs = Nothing

		getAdoRsScalar = strValue

	End Function



	Function Word(ByVal Str)
		Str = Request(Str) & ""
		Str = Replace(Str,"'","''")
		Word = Str
	End Function

	Function Error(ByVal Str)
	%>
	<script language="javascript">
	<!--
		alert("<%= Str %>");
		history.back();
	//-->
	</script>
	<%
		If DbConn.State = adStateOpen Then
			DbClose()
		End If
		response.End
	End Function

	Function Alert(ByVal Str)
	%>
	<script language="javascript">
	<!--
		alert("<%= Str %>");
	//-->
	</script>
	<%
	End Function

	Function Move(ByVal Str, ByVal Url)
		If IsValue(str) Then
	%>
	<script language="javascript">
	<!--
		alert("<%= Str %>");
	//-->
	</script>
	<%End If%>
	<meta http-equiv="refresh" content="0;url=<%= Url %>">
	<%
		If DbConn.State = adStateOpen Then
			DbClose()
		End If
		Response.End
	End Function

	Function wClose(ByVal Str)
	%>
	<script language="javascript">
	<!--
		alert("<%= Str %>");
		window.close();
	//-->
	</script>
	<%
	End Function

	Function GetLogin(ByVal strLoginPagePath)

		Dim strBackUrl

		strBackUrl = Server.URLEncode( "" _
			& Request.ServerVariables("PATH_INFO") _
			& Request.ServerVariables("QUERY_STRING") _
			& "" )

		Move "로그인이 필요합니다.", strLoginPagePath & "?strBackUrl=" & strBackUrl
	End Function

	Function GetLogin2(ByVal strLoginPagePath)

		Dim strBackUrl

		strBackUrl = Server.URLEncode( "" _
			& Request.ServerVariables("PATH_INFO") _
			&"?"& Request.ServerVariables("QUERY_STRING") _
			& "" )

		Move "로그인이 필요합니다.", strLoginPagePath & "&strBackUrl=" & strBackUrl
	End Function

	Function LenH(ByVal str)					'한글을 byte단위로 길이구하는 Func
		Dim tlen, tt, k, i
		if isnull(str) then
			LenH = 0
			Exit Function
		end if
		tlen = 0
		tt = Len(str)
		k = 1

		for i=1 to tt
			if Asc(Mid(str, k, 1)) < 0 then
				tlen = tlen + 2
			else
				tlen = tlen + 1
			end if
			k = k + 1
		next
		LenH = tlen
	End Function

	Function LeftH(ByVal str, ByVal strlen)

		if len(str) > 0 then
			Dim nLength, rValue, tmpStr, tmpLen, leng, f

			nLength = 0.00
			rValue = ""

			for f = 1 to len(str)
				tmpStr = MID(str,f,1)
				tmpLen = ASC(tmpStr)
				if (tmpLen = 60) then
					leng = nLength                     '"<"로 태그 열리는 위치 파악
					rValue = rValue & tmpStr
				elseif (tmpLen = 62) then
					nLength = leng                     '">"로 태그 닫히는 위치를 파악하여 위에서 처리한 위치 부터는 문자열 길이에서 제외
					rValue = rValue & tmpStr
				elseif  (tmpLen < 0) then
					nLength = nLength + 1.21           '한글일때 길이값 설정
					rValue = rValue & tmpStr
				elseif (tmpLen >= 97 and tmpLen <= 122) then
					nLength = nLength + 0.71           '영문소문자 길이값 설정
					rValue = rValue & tmpStr
				elseif (tmpLen >= 65 and tmpLen <= 90) then
					nLength = nLength + 0.82           '영문대문자 길이값 설정
					rValue = rValue & tmpStr
				elseif (tmpLen >= 48 and tmpLen <= 57) then
					nLength = nLength + 0.61           '숫자 길이값 설정
					rValue = rValue & tmpStr
				else
					nLength = nLength + 0.71           '특수문자 기호값...
					rValue = rValue & tmpStr
				end if
				If (nLength > strlen) then
					rValue = rValue & ".."
					exit for
				end if
			next

			LeftH = rValue
		end if
	End Function

	function image_size(ByVal image, ByVal size_int)

'		Dim Image_i, image_per, image_h, image_w
'
'		if image <> "" then
'
'			Set Image_i = Server.CreateObject("pakImage.ImageSize")   '이미지 사이즈 컴포넌트를 로딩한다.
'
'			Image_i.ImageSIze Server.MapPath(image)
'
'			if image_i.height <> 0 and image_i.width <> 0 then
'					if image_i.width < size_int then
'						image_per = 1
'					else
'						image_per = size_int / image_i.width
'					end if
'				image_h = image_per * image_i.height
'				image_w = image_per * image_i.width
'			else
'				image_h = 400
'				image_w = size_int
'			end if
'			image_size = split(Image_h &","& Image_w &","& image_i.width ,",")
'			Set Image_i = Nothing '객체 해제
'		else
'			image_size = split("400,400,400",",")
'
'		end if

		image_size = split(""&size_int&","&size_int&","&size_int&"",",")
	end function

	Function GetNumber(ByVal Var)
		GetNumber = Left("0" & Var, 2)
	End Function

	Function GetDate(ByVal Var)
		If len(Var) = 1 Then
			GetDate = "0"&Var
		Else
			GetDate = Var
		End If

	End Function


	Function GetCodeNumber()
		GetCodeNumber = Session.SessionID & year(now)&GetNumber(month(now))&GetNumber(day(now))&GetNumber(hour(now))&GetNumber(minute(now))&GetNumber(second(now))
	End Function

	Function GetGoodsCode() '// 17자리 고유코드 반환
		Dim dtmNow, intYear, intMonth, intDay, intHour, intMinute, intRnd
		dtmNow	= Now()
		intYear		= Left(Year(dtmNow),2)
		intMonth	= Month(dtmNow)
		intDay		= Day(dtmNow)
		intHour		= Hour(dtmNow)
		intMinute	= Minute(dtmNow)
		Randomize
		intRnd		= Int((39 - 0 + 1) * Rnd + 0)
		GetGoodsCode = "" _
				&	Session.SessionID _
				&	GetNumber(intYear + Int((99 - intYear - 0 + 1) * Rnd + 0)) _
				&	GetNumber(intMonth + Int((99 - intMonth - 0 + 1) * Rnd + 0)) _
				&	GetNumber(intDay + Int((99- intDay - 0 + 1) * Rnd + 0)) _
				&	GetNumber(intMinute + Int((99 - intMinute - 0 + 1) * Rnd + 0))
	End Function

	Function IsRegExp(ByVal patrn, ByVal strng)
		Dim regEx, Match, Matches			' 변수를 작성합니다.
		Set regEx = New RegExp				' 정규식을 작성합니다.
		regEx.Pattern = patrn				' 패턴을 설정합니다.
		regEx.IgnoreCase = True				' 대/소문자 구분 안함을 설정합니다.
		regEx.Global = True					' 전역을 설정합니다.
		Set Matches = regEx.Execute(strng)	' 찾기를 실행합니다.
		For Each Match in Matches			' Matches 컬렉션을 반복합니다.
			RetStr = RetStr & "True"
		Next
		IsRegExp = RetStr
	End Function

	'// 도메인 가져오기
	Function SubName
		SubName	= Request.ServerVariables("URL")
	End Function

	'// 쿼리스트링 가져오기
	Function SubString
		SubString	= Request.ServerVariables("QUERY_STRING")
	End Function

	'// 파일웹경로 가졍오기
	Function SubAll
		SubAll		= Request.ServerVariables("PATH_INFO")
	End Function



%>

<%
	'// 민호 추가

	'// 문자열 출력
	Sub Print(ByVal strText)

		Response.Write strText & "<br>"

	End Sub

	'// 폼값가져오기
	Function RequestForm(ByVal strKeyName)

		Dim strReturnValue

		strReturnValue = Request.Form(strKeyName)
		strReturnValue = Trim(strReturnValue) & ""
		RequestForm = Replace(strReturnValue, "'", "''")

	End Function

	'// 자바스크립트 출력
	Sub JsAlertAction(ByVal strAlert, ByVal strAction)

		Dim strText

		If IsValue(strAlert) Then strText = vbTab & "alert('" & strAlert & "');" & vbCrLf
		If IsValue(strAction) Then strText = strText & strAction

		Response.Write "" _
				&		vbCrLf _
				&		"<script language=" & Chr(34) & "javascript" & Chr(34) & ">" & vbCrLf _
				&		strText & vbCrLf _
				&		"</script>" _
				&		vbCrLf

	End Sub

	'// 문자열 텍스트화
	Function ConvertText(ByVal strContent)

		strContent = Replace(strContent, "&", "&amp;")
		strContent = Replace(strContent, "<", "&lt;")
		strContent = Replace(strContent, ">", "&gt;")
		strContent = Replace(strContent, Chr(34), "&quot;")
		strContent = Replace(strContent, Chr(13)&Chr(10), "<br>")
		strContent = Replace(strContent, "  ", "&nbsp;&nbsp;")
		ConvertText = strContent

	End Function

	'// 유효한 값인지 체크
	Function IsValue(ByVal strValue)
		If IsEmpty(strValue) Or IsNull(strValue) Or Trim(strValue) = "" Then
			IsValue = False
		Else
			IsValue = True
		End If

	End Function

	'// 관리자인지 체크
	Function IsTextExist(ByVal strText, ByVal strAllText, ByVal strDelimiter)

		Dim blnChk, arrAllText, intCLoop

		strText = Trim(strText)

		blnChk = False
		'// Null인경우 처리	'//2006/5/16 이덕권
		if len(strAllText) > 0 then
			arrAllText = Split(strAllText, strDelimiter)

			For intCLoop = 0 To UBound(arrAllText, 1)
				If strText = Trim(arrAllText(intCLoop)) Then
					blnChk = True
					Exit For
				End If
			Next
		end if

		IsTextExist = blnChk

	End Function

	'// 이미지파일인지 체크
	Function IsImage(ByVal strFile)

		Dim arrImgFileExt, strFileName, strFileExt, blnExtChk, i

		strFile = LCase(strFile)

		arrImgFileExt = Split("gif.jpg.jpeg.png",".")

		'strFileName = Mid(strFile, 1, InstrRev(strFile, ".") - 1)
		strFileExt = Mid(strFile, InstrRev(strFile, ".") + 1)

		blnExtChk = False

		For i = 0 To Ubound(arrImgFileExt, 1)
			If strFileExt = arrImgFileExt(i) Then
				blnExtChk = True
				Exit For
			End If
		Next

		IsImage = blnExtChk

	End Function

	'// 쿼리스트링 조작
	Function getString3(ByVal strAddQueryString)
		getString(strAddQueryString)
		'Dim strQueryString

		'strQueryString = Request.ServerVariables("Query_String")

	'	If IsValue(strQueryString) = False Then ' 주소창에 쿼리가 없으면 입력받은 쿼리를 반환
		'	getString3 = strAddQueryString
		'ElseIf IsValue(strQueryString) And IsValue(strAddQueryString) = False Then ' 주소창에 쿼리가 있고 입력쿼리가 없을경우
		'	getString3 = strQueryString
		'ElseIf  IsValue(strQueryString) And IsValue(strAddQueryString) Then ' 입력받은 쿼리가 있으면
		'	getString3 = GetReplaceKeyValue(strQueryString, strAddQueryString, "&", "=")
		'End If

	End Function

	'// 쿼리스트링/포스트데이타 조작후 쿼리로 통합
	Function getString2(ByVal strAddQueryString)

		strQueryString1 = Request.Form
		strQueryString2 = Request.QueryString

		If IsValue(strQueryString1) = False AND IsValue(strQueryString2) = False Then ' 주소창에 쿼리가 없으면 입력받은 쿼리를 반환
			tmpString = strAddQueryString
		ElseIf IsValue(strQueryString1) And IsValue(strAddQueryString) = False Then ' 주소창에 쿼리가 있고 입력쿼리가 없을경우
			tmpString = strQueryString1
		ElseIf IsValue(strQueryString2) AND IsValue(strAddQueryString) = False Then
			tmpString = strQueryString2
		ElseIf (IsValue(strQueryString1) OR IsValue(strQueryString2)) And IsValue(strAddQueryString) Then ' 입력받은 쿼리가 있으면
			If IsValue(strQueryString1) AND IsValue(strQueryString2) Then
				tmpString = GetReplaceKeyValue(strQueryString2, strAddQueryString, "&", "=")

				tmpString = GetReplaceKeyValue(tmpString, strQueryString1, "&", "=")
			End If

			If IsValue(strQueryString1) = False AND IsValue(strQueryString2) Then
				tmpString = GetReplaceKeyValue(strQueryString2, strAddQueryString, "&", "=")
			End If

			If IsValue(strQueryString1) AND IsValue(strQueryString2) = False Then
				tmpString = GetReplaceKeyValue(strQueryString1, strAddQueryString, "&", "=")
			End If
		End If

		getString2 = Left(tmpString, 1024)

	End Function

	'// 문자열 서로 치환
	Function getReplaceKeyValue(ByVal strContent1, ByVal strContent2, ByVal strKeyDiv, ByVal strEqualDiv)

		Dim strDiv1Content1, intDiv1Content1, strDiv1Content2, intDiv1Content2, intALoop, intReplaceChk, strtDiv2Content2, strtDiv2Content1, intGLoop, strPlus

		strDiv1Content1 = Split(Lcase(strContent1), strKeyDiv)
		intDiv1Content1 = Ubound(strDiv1Content1)

		strDiv1Content2 = Split(Lcase(strContent2), strKeyDiv)
		intDiv1Content2 = Ubound(strDiv1Content2)

		For intALoop = 0 to intDiv1Content2
			strtDiv2Content2 = Split(strDiv1Content2(intALoop),strEqualDiv)
			intReplaceChk = 0
			For intGLoop = 0 to intDiv1Content1
				strtDiv2Content1 = Split(strDiv1Content1(intGLoop),strEqualDiv)
				If strtDiv2Content2(0) = strtDiv2Content1(0) Then
					strDiv1Content1(intGLoop) = strtDiv2Content1(0) & strEqualDiv & strtDiv2Content2(1)
					intReplaceChk = 1
				End If
			Next
			If intReplaceChk = 0 Then
				strPlus = strPlus & strKeyDiv & strDiv1Content2(intALoop)
			End if
		Next

		For intGLoop = 0 to intDiv1Content1
			GetReplaceKeyValue = GetReplaceKeyValue & strDiv1Content1(intGLoop) & strKeyDiv
		Next

		getReplaceKeyValue = Left(GetReplaceKeyValue ,Len(GetReplaceKeyValue) - 1) & strPlus

	End Function

	'// 무조건 반올림
	Function Ceiling(ByVal intNum1, ByVal intNum2)

		Dim intReturnValue

		intReturnValue = Int(intNum1 / intNum2)
		If intNum1 Mod intNum2 > 0 Then
			intReturnValue = intReturnValue + 1
		End If

		Ceiling = intReturnValue

	End Function

	'// 페이징 출력
	Sub Paging3(ByVal intTotalPage, ByVal intBlockSize, ByVal intNowPage)

		Dim intPageStartNum, intLoopNum, intTmpNum

		If IsValue(intTotalPage) = False Or IsValue(intBlockSize) = False Or IsValue(intNowPage) = False Then Exit Sub

		intTotalPage = Int(Trim(intTotalPage))
		intBlockSize = Int(Trim(intBlockSize))
		intNowPage = Int(Trim(intNowPage))

		If intTotalPage = 0 Then Exit Sub

		intPageStartNum = ((Ceiling(intNowPage, intBlockSize) - 1) * intBlockSize) + 1

		'[1페이지로]
		If intTotalPage > 1 Then
			If intTotalPage > 1 And intNowPage > 1 Then
				Response.Write " <a href='?" & getString("intNowPage=1") & "'>[1]</a> "
			Else
				Response.Write " 1 "
			End If
		End If

		'[이전 10개]
		If intNowPage > intBlockSize Then
			If 1 > intPageStartNum - intBlockSize Then
				intTmpNum = 1
			Else
				intTmpNum = intPageStartNum - intBlockSize
			End If
			Response.Write " <a href='?" & getString("intNowPage=" & intTmpNum ) & "'>[이전 " & intBlockSize & "개]</a> "
		Else
			Response.Write " 이전 " & intBlockSize & "개 "
		End If

		'[숫자리스팅]
		For intLoopNum = intPageStartNum To intPageStartNum + intBlockSize - 1
			If intLoopNum > intTotalPage Then
				Exit For
			Else
				If intLoopNum <> intNowPage Then
					Response.Write " <a href='?" & getString("intNowPage=" & intLoopNum) & "'>" & intLoopNum & "</a> "
				Else
					Response.Write " <span style='font-size:10pt;font-weight:bold;color:#246BA3;'>[" & intLoopNum & "]</span> "
				End If
			End If
		Next

		'[다음 10개]
		If Ceiling(intTotalPage, intBlockSize) > Ceiling(intNowPage, intBlockSize) Then
			If intTotalPage < intPageStartNum + intBlockSize Then
				intTmpNum = intTotalPage
			Else
				intTmpNum = intPageStartNum + intBlockSize
			End If
			Response.Write " <a href='?" & getString("intNowPage=" & intTmpNum ) & "'>[다음 " & intBlockSize & "개]</a> "
		Else
			Response.Write " 다음 " & intBlockSize & "개 "
		End If

		'[라스트페이지로]
		If intTotalPage > 1 Then
			If intTotalPage > 1 And intTotalPage > intNowPage Then
				Response.Write " <a href='?" & getString("intNowPage=" & intTotalPage) & "'>[" & intTotalPage & "]</a> "
			Else
				Response.Write intTotalPage
			End If
		End If

	End Sub
	'//색깔 바꿈

	Sub Paging4(ByVal intTotalPage, ByVal intBlockSize, ByVal intNowPage)

		Dim intPageStartNum, intLoopNum, intTmpNum

		If IsValue(intTotalPage) = False Or IsValue(intBlockSize) = False Or IsValue(intNowPage) = False Then Exit Sub

		intTotalPage = Int(Trim(intTotalPage))
		intBlockSize = Int(Trim(intBlockSize))
		intNowPage = Int(Trim(intNowPage))

		If intTotalPage = 0 Then Exit Sub

		intPageStartNum = ((Ceiling(intNowPage, intBlockSize) - 1) * intBlockSize) + 1

		'[1페이지로]
		If intTotalPage > 1 Then
			If intTotalPage > 1 And intNowPage > 1 Then
				Response.Write " <a href='?" & getString("intNowPage=1") & "'>1</a> "
			Else
				Response.Write " 1 "
			End If
		End If

		'[이전 10개]
		If intNowPage > intBlockSize Then
			If 1 > intPageStartNum - intBlockSize Then
				intTmpNum = 1
			Else
				intTmpNum = intPageStartNum - intBlockSize
			End If
			Response.Write " <a href='?" & getString("intNowPage=" & intTmpNum ) & "'> 이전 " & intBlockSize & "개</a> "
		Else
			Response.Write " 이전 " & intBlockSize & "개 "
		End If

		'[숫자리스팅]
		For intLoopNum = intPageStartNum To intPageStartNum + intBlockSize - 1
			If intLoopNum > intTotalPage Then
				Exit For
			Else
				If intLoopNum <> intNowPage Then
					Response.Write " <a href='?" & getString("intNowPage=" & intLoopNum) & "'>" & intLoopNum & "</a> "
				Else
					Response.Write " <span style='font-size:10pt;font-weight:bold;color:#86C600;'>" & intLoopNum & "</span> "
				End If
			End If
		Next

		'[다음 10개]
		If Ceiling(intTotalPage, intBlockSize) > Ceiling(intNowPage, intBlockSize) Then
			If intTotalPage < intPageStartNum + intBlockSize Then
				intTmpNum = intTotalPage
			Else
				intTmpNum = intPageStartNum + intBlockSize
			End If
			Response.Write " <a href='?" & getString("intNowPage=" & intTmpNum ) & "'> 다음 " & intBlockSize & "개 </a> "
		Else
			Response.Write "다음 " & intBlockSize & "개  "
		End If

		'[라스트페이지로]
		If intTotalPage > 1 Then
			If intTotalPage > 1 And intTotalPage > intNowPage Then
				Response.Write " <a href='?" & getString("intNowPage=" & intTotalPage) & "'>" & intTotalPage & "</a> "
			Else
				Response.Write intTotalPage
			End If
		End If

	End Sub

	'// 간단 페이징 출력
	Sub PagingPreNext(ByVal intTotalPage, ByVal intNowPage)

		If intTotalPage = 0 Then Exit Sub

		'[이전페이지]
		If intTotalPage > 1 And intNowPage <> 1 Then
			Response.Write " [이전 페이지] "
		Else
			Response.Write " 이전 페이지 "
		End If

		'[다음페이지]
		If intTotalPage > 1 And intTotalPage <> intNowPage Then
			Response.Write " [다음 페이지] "
		Else
			Response.Write " 다음 페이지 "
		End If

	End Sub

	'// 이미지 사이즈비율대로 줄여서 가져오기
	function getImgSize(ByVal strImgWebPath, ByVal intWidthMax, ByVal intHeightMax)

		Dim Image_i, image_size, intImgWidth, intImgHeight, intMaxRate, image_per, image_h, image_w, Fso

'		Set Image_i = Server.CreateObject("pakImage.ImageSize")   '이미지 사이즈 컴포넌트를 로딩한다.
'
'			Image_i.ImageSIze Server.MapPath(strImgWebPath)
'
'			intImgWidth = image_i.width
'			intImgHeight = image_i.height
'
'		Set Image_i = Nothing
'
'		If intImgWidth = 0 AND intImgHeight = 0 Then
'
''			Set Fso = Server.CreateObject("SCRIPTING.FileSystemObject")
'			If Fso.FileExists(Server.MapPath(strImgWebPath)) Then
'				Set Image_i = LoadPicture(Server.MapPath(strImgWebPath))
'				intImgWidth = CLng(CDbl(Image_i.Width) * 24 / 635) ' CLng 의 라운드 오프의 기능을 이용하고 있다.
'				intImgHeight = CLng(CDbl(Image_i.Height) * 24 / 635) ' CLng 의 라운드 오프의 기능을 이용하고 있다.
'				Set Image_i = Nothing
'			Else
'				'Response.Write "Path Error!!"
'			End If
'			Set Fso = Nothing
'		End if
'
'		If intImgWidth <> 0 AND intImgHeight <> 0 Then
'
'			intMaxRate = intWidthMax / intHeightMax * intImgHeight
'
'			If intMaxRate <= intImgWidth Then
'				if intImgWidth < intWidthMax then
'					image_per = 1
'				else
'					image_per = intWidthMax / intImgWidth
'				end if
'			Else
'				If intImgHeight <= intHeightMax Then
'					image_per = 1
'				Else
'					image_per = intHeightMax / intImgHeight
'				End If
'			End If
'
'			image_h = Int(image_per * intImgHeight)
'			image_w = Int(image_per * intImgWidth)
'
'			image_size = split(image_h & "," & image_w & "," & intImgHeight & "," & intImgWidth, ",")
'
'		Else
'
'			image_size = split(intImgHeight & "," & intImgWidth & ",0,0",  ",")

'		End If

		image_size = split(intHeightMax & "," & intWidthMax & "," & intHeightMax & "," & intWidthMax, ",")
		getImgSize = image_size

	end function






	' 이전 경로를 리턴함
	' http://도메인/이전파일명.htm?쿼리스트링 형식으로 리턴
	' 쿼리스트링 포함
	Function getBackUrl()
		' 전체 경로 추출
		getBackUrl	= request.serverVariables("HTTP_REFERER")
		getBackUrl	= lcase(getBackUrl)
		'getBackUrl	= replace(getBackUrl,"www.","")
	End Function


	'리턴되는 페이지 이전 파일까지의 경로
	' http://도메인/이전파일명.htm 형식으로 리턴
	Function getBackUrlFile
		if len(getBackUrl) > 0 then
			getBackUrlFile = split(getBackUrl,"?")(0)
		end if
	End Function


	' 현재파일의 루트경로부터 쿼리스트링 전까지를 리턴함
	' /foldername/filename.asp 형식으로 리턴
	Function getThisUrl
		getThisUrl = request.serverVariables("URL")
	End Function


	Function getThisFileName
	'##	현재경로의 파일명을 리턴합니다.
		dim i
		i = split(getThisUrl,"/")
		getThisFileName = i(ubound(i))
	End Function





	'*********** 스트링관련 ************


	Function GetString(ByVal addString)
	'##	현재페이지의 스트링값을 받는 함수

      	dim UrlString, split_UrlString, i_UrlString, chkword_UrlString
		dim split_addString, i_addString, chkword_addString
		dim stringEquleChk
		if instr(request.serverVariables("QUERY_STRING"),"&RD=") > 1 then
			UrlString = split(request.serverVariables("QUERY_STRING"),"&RD=")(0)
		else
			UrlString = request.serverVariables("QUERY_STRING")
		end if

		if trim(UrlString)="" or isnull(trim(UrlString)) then
			GetString = addString
		elseif not isnull(trim(urlString)) and isnull(trim(addString)) then
			GetString = urlString
		else

			split_UrlString = split(UrlString,"&")
			split_addString = split(addString,"&")


'response.Write Ubound(split_UrlString) & "/"
'response.Write Ubound(split_addString)
			For i_UrlString = 0 to Ubound(split_UrlString)
				'aaa=123
				if split_UrlString(i_UrlString) <> "" then
					chkword_UrlString = split(split_UrlString(i_UrlString),"=")(0)
					chkword_UrlString = lcase(chkword_UrlString)
					chkword_UrlString = trim(chkword_UrlString)
					stringEquleChk = "n"
					For i_addString = 0 to Ubound(split_addString)
						chkword_addString = split(split_addString(i_addString),"=")(0)
						chkword_addString = lcase(chkword_addString)
						chkword_addString = trim(chkword_addString)
						if chkword_UrlString = chkword_addString then
							stringEquleChk = "y"
						end if
					'response.Write Ubound(split_addString)
					'response.Write("["&chkword_UrlString&"/"&chkword_addString&"|"&stringEquleChk&"]")
					next
					if stringEquleChk = "n" then

						GetString = GetString & trim(split_UrlString(i_UrlString)) & "&"

					end if
					'response.Write("///"&i_UrlString&""&i_addString&stringEquleChk)
				end if
			next

			GetString = GetString & addString & "&RD=" & random()

		end if




		'// 2006-05-18 수정
		'// 한번사용하고 소멸하는 Temp 스트링을 없애는 처리 추가
		'// 예를들어 tmep=1234&ttt=1233 인경우 ttt=1233 변환

			DIM TEMPI, TEMPI2
			DIM SPLIT_GETSTRING
			DIM TEMPWORD
			DIM GetString2


			SPLIT_GETSTRING = SPLIT(GetString,"&")


			For TEMPI = 0 to Ubound(SPLIT_GETSTRING)
				'aaa=123
				if SPLIT_GETSTRING(TEMPI) <> "" then
					TEMPWORD = split(SPLIT_GETSTRING(TEMPI),"=")(0)
					TEMPWORD = lcase(TEMPWORD)
					TEMPWORD = trim(TEMPWORD)

					if TEMPWORD = "temp" then
					else
						GetString2 = GetString2 & trim(SPLIT_GETSTRING(TEMPI)) & "&"
					end if
					GetString = GetString2
				end if
			next

			'// 2006-05-18 수정끝

	End Function


	Function GetString_norandom(ByVal addString)
	'##	현재페이지의 스트링값을 받는 함수

      	dim UrlString, split_UrlString, i_UrlString, chkword_UrlString
		dim split_addString, i_addString, chkword_addString
		dim stringEquleChk
		if instr(request.serverVariables("QUERY_STRING"),"#") > 1 then
			UrlString = split(request.serverVariables("QUERY_STRING"),"#")(0)
		else
			UrlString = request.serverVariables("QUERY_STRING")
		end if

		if trim(UrlString)="" or isnull(trim(UrlString)) then
			GetString_norandom = addString
		elseif not isnull(trim(urlString)) and isnull(trim(addString)) then
			GetString_norandom = urlString
		else

			split_UrlString = split(UrlString,"&")
			split_addString = split(addString,"&")


'response.Write Ubound(split_UrlString) & "/"
'response.Write Ubound(split_addString)
			For i_UrlString = 0 to Ubound(split_UrlString)
				'aaa=123
				if split_UrlString(i_UrlString) <> "" then
					chkword_UrlString = split(split_UrlString(i_UrlString),"=")(0)
					chkword_UrlString = lcase(chkword_UrlString)
					chkword_UrlString = trim(chkword_UrlString)
					stringEquleChk = "n"
					For i_addString = 0 to Ubound(split_addString)
						chkword_addString = split(split_addString(i_addString),"=")(0)
						chkword_addString = lcase(chkword_addString)
						chkword_addString = trim(chkword_addString)
						if chkword_UrlString = chkword_addString then
							stringEquleChk = "y"
						end if
					'response.Write Ubound(split_addString)
					'response.Write("["&chkword_UrlString&"/"&chkword_addString&"|"&stringEquleChk&"]")
					next
					if stringEquleChk = "n" then

						GetString_norandom = GetString_norandom & trim(split_UrlString(i_UrlString)) & "&"

					end if
					'response.Write("///"&i_UrlString&""&i_addString&stringEquleChk)
				end if
			next

			GetString_norandom = GetString_norandom & addString

		end if




		'// 2006-05-18 수정
		'// 한번사용하고 소멸하는 Temp 스트링을 없애는 처리 추가
		'// 예를들어 tmep=1234&ttt=1233 인경우 ttt=1233 변환

			DIM TEMPI, TEMPI2
			DIM SPLIT_GETSTRING
			DIM TEMPWORD
			DIM GetString2


			SPLIT_GETSTRING = SPLIT(GetString_norandom,"&")


			For TEMPI = 0 to Ubound(SPLIT_GETSTRING)
				'aaa=123
				if SPLIT_GETSTRING(TEMPI) <> "" then
					TEMPWORD = split(SPLIT_GETSTRING(TEMPI),"=")(0)
					TEMPWORD = lcase(TEMPWORD)
					TEMPWORD = trim(TEMPWORD)

					if TEMPWORD = "temp" then
					else
						GetString2 = GetString2 & trim(SPLIT_GETSTRING(TEMPI)) & "&"
					end if
					GetString_norandom = GetString2
				end if
			next

			'// 2006-05-18 수정끝

	End Function








	Function BackGetString(ByVal addString)
	'##	처린이전페이지로 쿼리스트링 문자열 값을 넘기는 함수

		BackGetString = getBackUrlFile & "?" & getstring(addString)

	End Function





	Function ThisGetString(ByVal addString)
	'##	현재페이지로 쿼리스트링 문자열 값을 넘기는 함수

		ThisGetString = "http://"& getThisURL & "?" & getstring(addString)

	End Function



	Sub Furl(byval porgramFolder, byval DefaultUrl)
	'## 해당 파일을 불러오는 함수
		Furl = Request.QueryString("Furl")
		if len(Furl) = 0 then
			Furl = DefaultUrl
		end if

		server.Execute(porgramFolder & Furl &".asp")
	End Sub



	'^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'[텍스트 함수 설정]
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function all_input(ByVal str)
    '##	모든 입력 스트링 문자열에 적용 - 입력시
	  	if not isnull(str) then str = replace(str, "'", "`")
		'if not isnull(str) then str = replace(str, ";", "")
      	all_input = str
	End Function

	Function Use_javaText(ByVal str)
    '##	 모든데이타를 텍스트로 처리 - javascript용
	  	str = replace(str, "''", "")
		str = replace(str, "'", "")
		str = replace(str,chr(13)&chr(10),"\n")
      	Use_javaText = str
	End Function

	Function Use_Text(ByVal str)
	'##	모든데이타를 텍스트로 처리
		str = str & ""
      	str = replace(str, "&", "&amp;")
      	str = replace(str, "<", "&lt;")
      	str = replace(str, ">", "&gt;")
		str = replace(str,chr(13)&chr(10),"<br>")
		str = replace(str, "  ", "&nbsp;&nbsp;")
      	Use_Text = str
	End Function

	Function Use_Text_noblack(ByVal str)
	'##	모든데이타를 연결된 텍스트로 처리
		str = str & ""
		str = replace(str, "&", "&amp;")
      	str = replace(str, "<", "&lt;")
      	str = replace(str, ">", "&gt;")
		str = replace(str,chr(13)&chr(10),"")
		Use_Text_noblack = str
	End Function

	Function Use_HtmlBR(ByVal str)
	'## html이용 <br>태그 자동 삽입
		str = str & ""
		str = replace(str,chr(13)&chr(10),"<br>")
		str = replace(str, "  ", "&nbsp;&nbsp;")
      	Use_HtmlBR = str
	End Function

	Function Use_Html(ByVal str)
	'## 모든데이타를 HTML로 처리
		if not(isnull(str)) then
			str = replace(str, "&lt;", "<")
			str = replace(str, "&gt;", ">")
			str = replace(str, "&quot;", "'")
			str = replace(str, "&amp;", "&")
			Use_Html = str
		end if
	End Function

	Function UnUse_Html(ByVal str)
	'## 모든데이타를 HTML로 처리
		str = str & ""
		str = replace(str, "&", "&amp;")
	  	str = replace(str, "'", "&quot;")
	  	str = replace(str, chr(34), "&quot;")
      	str = replace(str, "<", "&lt;")
      	str = replace(str, ">", "&gt;")
      	UnUse_Html = str
	End Function

	Function MYsqlOut(ByVal str)
		str		= replace(str,chr(13)&chr(10),"<br>")
		str		= replace(str, " < ", "<")
		str		= replace(str, " > ", ">")
		str		= replace(str, "$", "?")
	End Function

	Function MYsqlIn(ByVal str)
		str		= replace(str, "?", "$")
	End Function

	Function chrbyte(byval str,byval bytes)
		dim i, charat, wLen

		if isnull(str) then	'//들어온 스트링이 널인경우
			str = "null"
		end if

		for i=1 to len(str)
		'for i=1 to len(bytes)
			charat=mid(str, i, 1)
			if asc(charat)>0 and asc(charat)<255 then	'//영어이면
				wLen=wLen+1
			else	'//한글이면
				wLen=wLen+2
			end if
		next


		'...의 출력 여부
		dim dot
		if bytes > 12 then	'//5바이트보다 큰 문자열의 경우
			dot = "..."
		end if



		'//문자열출력
		if wLen > Cint(bytes-2) then	'//표현할 바이트보다 문자열이 긴경우
			dim ii : ii = 1
			for i=1 to Cint(bytes-2)
				charat=mid(str, ii, 1)
				if asc(charat)>0 and asc(charat)<255 then	'//영어이면
					wLen=wLen+1
					chrbyte = chrbyte & charat
				else	'//한글이면
					wLen=wLen+2
					i = i + 1
					chrbyte = chrbyte & charat
				end if
				ii = ii + 1
			next
			chrbyte = chrbyte & dot
		else
			chrbyte = str
		end if

	end Function




	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'##
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'##
	'dim intTotalCount, intTotalPage

	'dim intNowPage			: intNowPage 		= Request.QueryString("page")
    'dim intPageSize		: intPageSize 		= 10
    'dim intBlockPage		: intBlockPage 		= 10

	'dim query_filde		: query_filde		= "필드명"
	'dim query_Tablename	: query_Tablename	= "테이블명"
	'dim query_where		: query_where		= "조건"
	'dim query_orderby		: query_orderby		= "정렬값
	'call intTotal

	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. GetQuery() 를 이용하여 쿼리문을 가져욥니다.
	'## 6. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'##
	'##
	'#############################################################################

	%><Object Runat=Server PROGID=ADODB.Connection ID=FunctionDB></Object><%
		'///디비커넥션 파일의 디비스트링 사용
		'//DBstring = "provider=sqloledb;data source="& DBaddr &" ;initial catalog="& DBname &" ;uid="& DBUserID &"; pwd="& DBUserPW &""



	Sub intTotal
	'## 조건에 만족하는 카운트 반환
	'// 디비가 연결된 상태이어야함-- 사용자 DB사용 userDB
		If Len(intNowPage) = 0 Then
        intNowPage = 1
    	End If


		dim strsql, objRs
		strSQL = "Select Count(*)"
		strSQL = strSQL & ",CEILING(CAST(Count(*) AS FLOAT)/" & intPageSize & ")"
		strSQL = strSQL & " from " & query_Tablename
	 	if len(query_where) > 0 then
		strSQL = strSQL & " where " & query_where
		end if


		FunctionDB.Open cstConnString
		set objRs = FunctionDB.execute(strsql)


			intTotalCount = objRs(0)
			intTotalPage = objRs(1)

		objRs.close
		set objRs = nothing
		FunctionDB.close



	End Sub


	Function TopCount
	'## 페이징에서 조건에 맞는 최대 행의수 리턴
		TopCount = "Top " & intNowPage * intPageSize
	End Function

	Function MoveCount
	'## 설정된 변수값을 토대로 한 현재페이지 레코드 값 반환
		MoveCount = (intNowPage - 1) * intPageSize
	End Function

	Function NavCount
	'## 전체글 과 현재 페이지 보기
		NavCount = "전체 "&intTotalCount&"개&nbsp;&nbsp;&nbsp;&nbsp;현재페이지 "&intNowPage&"/"&intTotalPage&""
	End Function

	Function ShopNavCount
	'## 쇼핑몰용 네비게이션
		ShopNavCount = "현재 조건에 맞는 상품이 <font color=red><b>["&intTotalCount&"]</b></font>개 준비되어 있습니다.&nbsp;&nbsp;&nbsp;&nbsp;현재페이지 "&intNowPage&"/"&intTotalPage&""
	End Function

'	Sub Paging(byval plusString)
'	'## 페지징
'
''		dim intTemp, intLoop
'		intTemp = Int((intNowPage - 1) / intBlockPage) * intBlockPage + 1
'			'Response.Write CInt(intNowPage)
'
'	        If intTemp = 1 Then
 '               Response.Write "<img src='/avanplus/img/b_prev_02.png' border='0' align='top'>&nbsp;&nbsp;"
  '          Else
   '             Response.Write"<a href=?"&getstring("page=" & intTemp - intBlockPage)&"><img src='/avanplus/img/b_prev_02.png' border='0' align='top'></a>&nbsp;&nbsp;"
    '        End If
'
 '           intLoop = 1
'
 '           Do Until intLoop > intBlockPage Or intTemp > intTotalPage
  '              If intTemp = CInt(intNowPage) Then
   '                 Response.Write "<span class='paging_on'>" & intTemp &"</span>"
    '            Else
 '                   Response.Write"<a href=?"&getstring("page=" & intTemp & plusString)&"><span>"& intTemp & "</span></a>"
  '              End If
   '             intTemp = intTemp + 1
    '            intLoop = intLoop + 1
 '           Loop
'
'
'
'
 '           If intTemp > intTotalPage Then
  '              Response.Write "&nbsp;&nbsp;<img src='/avanplus/img/b_prev_03.png' border='0' align='top'>"
   '         Else
  '              Response.Write"&nbsp;&nbsp;<a href=?"&getstring("page=" & intTemp)&"><img src='/avanplus/img/b_prev_03.png' border='0' align='top'></a>"
 '           End If
'
'	End Sub


	Sub Paging(byval plusString)
	'## 페지징

		dim intTemp, intLoop
		intTemp = Int((intNowPage - 1) / intBlockPage) * intBlockPage + 1
			'Response.Write CInt(intNowPage)

	        Response.Write("<div class='page_btn'><table><tr><td>")

			If intTemp = 1 Then
                Response.Write "<div class='prev'><a href='#'><img src='/img/news/prev_btn.jpg' alt=''></a></div>"
            Else
                Response.Write "<div class='prev'><a href='?"&getstring("page=" & intTemp - intBlockPage)&"'><img src='/img/news/prev_btn.jpg' alt=''></a></div>"
            End If

			Response.Write("<div class='paging02'><ul>")


            intLoop = 1

            Do Until intLoop > intBlockPage Or intTemp > intTotalPage
                If intTemp = CInt(intNowPage) Then
                    'Response.Write "<span class='paging_on'>" & intTemp &"</span>"
					Response.Write"<li class='on'><a href='#'>"& intTemp & "</a></li>"
                Else
                    'Response.Write"<a href=?"&getstring("page=" & intTemp & plusString)&"><span>"& intTemp & "</span></a>"
					Response.Write"<li><a href='?"&getstring("page=" & intTemp & plusString)&"'>"& intTemp & "</a></li>"
                End If
                intTemp = intTemp + 1
                intLoop = intLoop + 1
            Loop


			Response.Write("</ul></div>")

            If intTemp > intTotalPage Then
                Response.Write "<div class='prev next'><a href='#'><img src='/img/news/next_btn.jpg' alt=''></a></div>"
            Else
                Response.Write "<div class='prev next'><a href='?"&getstring("page=" & intTemp)&"'><img src='/img/news/next_btn.jpg' alt=''></a></div>"
            End If

			Response.Write("</td></tr></table></div>")

	End Sub




	Sub Paging_new(byval plusString)
	'## 페지징

		dim intTemp, intLoop
		intTemp = Int((intNowPage - 1) / intBlockPage) * intBlockPage + 1

			'Response.Write CInt(intBlockPage)

				Response.write "<li class=""b_no""><a href=""?"&getstring("page=1")&"""><img src=""img/aw_09.gif"" alt=""맨앞으로""></a></li>"
                Response.write "<li class=""b_no""><a href=""?"&getstring("page=" & intTemp)&"""><img src=""img/eer_11.gif""alt=""앞으로""></a></li>"

'	        If intTemp = 1 Then
 '               Response.Write "<img src='/avanplus/img/b_prev_02.png' border='0' align='top'>&nbsp;&nbsp;"
  '          Else
  '              Response.Write"<a href=?"&getstring("page=" & intTemp - intBlockPage)&"><img src='/avanplus/img/b_prev_02.png' border='0' align='top'></a>&nbsp;&nbsp;"
  '          End If

            intLoop = 1

            Do Until intLoop > intBlockPage Or intTemp > intTotalPage
                If intTemp = CInt(intNowPage) Then
                    Response.Write "<li>" & intTemp &"</li>"
                Else
                    Response.Write"<li><a href=""?"&getstring("page=" & intTemp & plusString)&""">"& intTemp & "</a></li>"
                End If
                intTemp = intTemp + 1
                intLoop = intLoop + 1
            Loop

		Dim lastPage: lastPage = 0

		If intTotalPage >= lastPage Then lastPage = 1 Else lastPage = intTotalPage End If

			Response.write "<li class=""b_no""><a href=""?"&getstring("page=" & intTemp - 1)&"""><img src=""img/aww_13.gif"" alt=""뒤로""></a></li>"
            Response.write "<li class=""b_no""><a href=""?"&getstring("page=" & lastPage)&"""><img src=""img/arrew7_15.gif"" alt=""맨뒤으로""></a></li>"

'            If intTemp > intTotalPage Then
 '               Response.Write "&nbsp;&nbsp;<img src='/avanplus/img/b_prev_03.png' border='0' align='top'>"
 '           Else
 '               Response.Write"&nbsp;&nbsp;<a href=?"&getstring("page=" & intTemp)&"><img src='/avanplus/img/b_prev_03.png' border='0' align='top'></a>"
 '           End If

	End Sub


Sub Paging_list(byval plusString)
	'## 페지징

		dim intTemp, intLoop
		intTemp = Int((intNowPage - 1) / intBlockPage) * intBlockPage + 1

			Response.Write "<div class='list_page'>"

				Response.write "<a href=""?"&getstring("page=1")&""" class=""first_btn"">처음</a>"
                Response.write "<a href=""?"&getstring("page=" & intTemp)&""" class=""first2_btn"">이전</a>"
				Response.write "<ul>"

'	        If intTemp = 1 Then
 '               Response.Write "<img src='/avanplus/img/b_prev_02.png' border='0' align='top'>&nbsp;&nbsp;"
  '          Else
  '              Response.Write"<a href=?"&getstring("page=" & intTemp - intBlockPage)&"><img src='/avanplus/img/b_prev_02.png' border='0' align='top'></a>&nbsp;&nbsp;"
  '          End If

            intLoop = 1

            Do Until intLoop > intBlockPage Or intTemp > intTotalPage
                If intTemp = CInt(intNowPage) Then
                    Response.Write "<li><a href=""#"" class=""on"">" & intTemp &"</a></li>"
                Else
                    Response.Write "<li><a href=""?"&getstring("page=" & intTemp & plusString)&""">"& intTemp & "</a></li>"
                End If
                intTemp = intTemp + 1
                intLoop = intLoop + 1
            Loop

		Dim lastPage: lastPage = 0

		If intTotalPage >= lastPage Then lastPage = 1 Else lastPage = intTotalPage End If

			Response.write "</ul>"
			Response.write "<a href=""?"&getstring("page=" & intTemp - 1)&""" class=""last2_btn"">다음</a>"
            Response.write "<a href=""?"&getstring("page=" & lastPage)&""" class=""last_btn"">맨뒤</a>"

'            If intTemp > intTotalPage Then
 '               Response.Write "&nbsp;&nbsp;<img src='/avanplus/img/b_prev_03.png' border='0' align='top'>"
 '           Else
 '               Response.Write"&nbsp;&nbsp;<a href=?"&getstring("page=" & intTemp)&"><img src='/avanplus/img/b_prev_03.png' border='0' align='top'></a>"
 '           End If
		Response.Write "</div>"

	End Sub


	Sub Paging_admin(byval plusString)
	'## 페지징

		dim intTemp, intLoop
		intTemp = Int((intNowPage - 1) / intBlockPage) * intBlockPage + 1

			Response.Write "<div class='paging'>"

				Response.write "<a href=""?"&getstring("page=1")&""" class=""imgPage""><img src=""img/imgFirst.png"" alt=""처음""></a>"
                Response.write "<a href=""?"&getstring("page=" & intTemp)&""" class=""imgPage""><img src=""img/imgPrev.png"" alt=""이전"" style=""margin-right:8px""></a>"

'	        If intTemp = 1 Then
 '               Response.Write "<img src='/avanplus/img/b_prev_02.png' border='0' align='top'>&nbsp;&nbsp;"
  '          Else
  '              Response.Write"<a href=?"&getstring("page=" & intTemp - intBlockPage)&"><img src='/avanplus/img/b_prev_02.png' border='0' align='top'></a>&nbsp;&nbsp;"
  '          End If

            intLoop = 1

            Do Until intLoop > intBlockPage Or intTemp > intTotalPage
                If intTemp = CInt(intNowPage) Then
                    Response.Write "<a href=""#"" class=""active"">" & intTemp &"</a>"
                Else
                    Response.Write"<a href=""?"&getstring("page=" & intTemp & plusString)&""">"& intTemp & "</a>"
                End If
                intTemp = intTemp + 1
                intLoop = intLoop + 1
            Loop

		Dim lastPage: lastPage = 0

		If intTotalPage >= lastPage Then lastPage = 1 Else lastPage = intTotalPage End If

			Response.write "<a href=""?"&getstring("page=" & intTemp - 1)&""" class=""imgPage""><img src=""img/imgNext.png"" alt=""다음""  style=""margin-left:8px""></a>"
            Response.write "<a href=""?"&getstring("page=" & lastPage)&""" class=""imgPage""><img src=""img/imgLast.png"" alt=""끝""></a>"

'            If intTemp > intTotalPage Then
 '               Response.Write "&nbsp;&nbsp;<img src='/avanplus/img/b_prev_03.png' border='0' align='top'>"
 '           Else
 '               Response.Write"&nbsp;&nbsp;<a href=?"&getstring("page=" & intTemp)&"><img src='/avanplus/img/b_prev_03.png' border='0' align='top'></a>"
 '           End If
		Response.Write "</div>"

	End Sub




'	Sub Paging(byval plusString)
	'## 페이징 백업 by 연문호 2015-04-06

'		dim intTemp, intLoop
'		intTemp = Int((intNowPage - 1) / intBlockPage) * intBlockPage + 1

'            If intTemp = 1 Then
'                Response.Write "<img src='/avanplus/img/b_prev_02.png' border='0' align='top'>&nbsp;&nbsp;"
'            Else
'                Response.Write"<a href=?"&getstring("page=" & intTemp - intBlockPage)&"><img src='/avanplus/img/b_prev_02.png' border='0' align='top'></a>&nbsp;&nbsp;"
'            End If

'            intLoop = 1

'            Do Until intLoop > intBlockPage Or intTemp > intTotalPage
'                If intTemp = CInt(intNowPage) Then
'                    Response.Write "<font size='2'><b>" & intTemp &"</b></font>&nbsp;"
'                Else
'                    Response.Write"<a href=?"&getstring("page=" & intTemp & plusString)&">" & intTemp & "</a>&nbsp;"
'                End If
'                intTemp = intTemp + 1
'                intLoop = intLoop + 1
'            Loop




'            If intTemp > intTotalPage Then
'                Response.Write "&nbsp;&nbsp;<img src='/avanplus/img/b_prev_03.png' border='0' align='top'>"
'            Else
'                Response.Write"&nbsp;&nbsp;<a href=?"&getstring("page=" & intTemp)&"><img src='/avanplus/img/b_prev_03.png' border='0' align='top'></a>"
'            End If
'	End Sub


	Sub Paging_txt(byval plusString)
	'## 페지징

		dim intTemp, intLoop
		intTemp = Int((intNowPage - 1) / intBlockPage) * intBlockPage + 1

            If intTemp = 1 Then
                Response.Write "< &nbsp;&nbsp;"
            Else
                Response.Write"<a href=?"&getstring("page=" & intTemp - intBlockPage)&"> < </a>&nbsp;&nbsp;"
            End If

            intLoop = 1

            Do Until intLoop > intBlockPage Or intTemp > intTotalPage
                If intTemp = CInt(intNowPage) Then
                    Response.Write "<font size='2'><b>" & intTemp &"</b></font>&nbsp;"
                Else
                    Response.Write"<a href=?"&getstring("page=" & intTemp & plusString)&">" & intTemp & "</a>&nbsp;"
                End If
                intTemp = intTemp + 1
                intLoop = intLoop + 1
            Loop




            If intTemp > intTotalPage Then
                Response.Write "&nbsp;&nbsp; >"
            Else
                Response.Write"&nbsp;&nbsp;<a href=?"&getstring("page=" & intTemp)&"> ></a>"
            End If


	End Sub






	Function GetQuery()
		if trim(query_where) <>"" then
			GetQuery = "select "& TopCount & " " & query_filde &" from "& query_tablename & " where "& query_where & " " & query_orderby
		else
			GetQuery = "select "& TopCount & " " & query_filde &" from "& query_tablename  & " " & query_orderby
		end if
	End Function


	'// 함수용 2005년 5월 16일 이덕권
	Sub PagingFun(byval plusString, byval intNowPage, byval intBlockPage, byval intTotalPage)
	'## 페지징

		dim intTemp, intLoop
		intTemp = Int((intNowPage - 1) / intBlockPage) * intBlockPage + 1

            If intTemp = 1 Then
                Response.Write "<img src='/avanplus/img/b_prev.gif' border='0' align='top'>&nbsp;&nbsp;"
            Else
                Response.Write"<a href=?"&getstring("page=" & intTemp - intBlockPage)&"><img src='/avanplus/img/b_prev.gif' border='0' align='top'></a>&nbsp;&nbsp;"
            End If

            intLoop = 1

            Do Until intLoop > intBlockPage Or intTemp > intTotalPage
                If intTemp = CInt(intNowPage) Then
                    Response.Write "<font size='2'><b>" & intTemp &"</b></font>&nbsp;"
                Else
                    Response.Write"<a href=?"&getstring("page=" & intTemp & plusString)&">" & intTemp & "</a>&nbsp;"
                End If
                intTemp = intTemp + 1
                intLoop = intLoop + 1
            Loop




            If intTemp > intTotalPage Then
                Response.Write "&nbsp;&nbsp;<img src='/avanplus/img/b_next.gif' border='0' align='top'>"
            Else
                Response.Write"&nbsp;&nbsp;<a href=?"&getstring("page=" & intTemp)&"><img src='/avanplus/img/b_next.gif' border='0' align='top'></a>"
            End If


	End Sub

'	Function GetQuery()
'		if trim(query_where) <>"" then
'			GetQuery = "select "& TopCount & " " & query_filde &" from "& query_tablename & " where "& query_where & " " & q'uery_orderby
'		else
'			GetQuery = "select "& TopCount & " " & query_filde &" from "& query_tablename  & " " & query_orderby
'		end if
'	End Function

'####### 추가분 시작

	'##==========================================================================
	'##
	'##	메세지 함수
	'##
	'##==========================================================================




	'//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 메세지 함수 모음 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


    '// 자바스크립트 alert 메세지 출력
    Sub jsAlertMsg(byval msg)

        response.write "<script language=""javascript"">" & vbCrLf
        response.write "<!--" & vbCrLf
        response.write "    window.alert(""" & msg & """);" & vbCrLf
        response.write "// -->" & vbCrLf
        response.write "</script>"

    end Sub

    '// 자바스크립트 alert 메세지 출력 -> url 이동
    Sub jsAlertMsgUrl(byval msg, byval url)

        response.write "<script language=""javascript"">" & vbCrLf
        response.write "<!--" & vbCrLf
        response.write "    window.alert(""" & msg & """);" & vbCrLf

        ' 이동할 url 이 있다면
        if url <> "" then
            response.write "    location.href=""" & url & """;" & vbCrLf
        end if
        response.write "// -->" & vbCrLf
        response.write "</script>"

    end Sub

	'// 자바스크립트 url 이동
    Sub jsAlertUrl(byval url)

        response.write "<script language=""javascript"">" & vbCrLf
        response.write "<!--" & vbCrLf
        response.write "    location.href=""" & url & """;" & vbCrLf
        response.write "// -->" & vbCrLf
        response.write "</script>"

    end Sub

    '// 자바스크립트 alert 메세지 출력   -> history.back()
    Sub jsAlertMsgBack(byval msg)

        response.write "<script language=""javascript"">" & vbCrLf
        response.write "<!--" & vbCrLf
        response.write "    window.alert(""" & msg & """);" & vbCrLf
        response.write "    history.back(-1)" & vbCrLf
        response.write "// -->" & vbCrLf
        response.write "</script>"

    end Sub

	Sub jsAlertMsgClose(byval msg)

        response.write "<script language=""javascript"">" & vbCrLf
        response.write "<!--" & vbCrLf
        response.write "    window.alert(""" & msg & """);" & vbCrLf
        response.write "    window.close()" & vbCrLf
        response.write "// -->" & vbCrLf
        response.write "</script>"

    end Sub


'####### 추가분 끝


	'// 파일의 소스코드를 변수로 반환한다.
	Function GetCodeRead(byval FilePath)

			dim fs, html_str, objFile

			Set fs = Server.CreateObject("Scripting.FileSystemObject")
			Set objFile = fs.OpenTextFile(server.MapPath(FilePath),1)

			 GetCodeRead = objFile.readall

			set objFile = nothing
			set fs = nothing

	End Function



	Function chk_checked(byval a, byval b)
		a = trim(lcase(a))
		b = trim(lcase(b))

		if a = b then
			chk_checked = "checked"
		end if
	End Function

	Function chk_selected(byval a, byval b)
		a = trim(lcase(a))
		b = trim(lcase(b))

		if a = b then
			chk_selected = "selected"
		end if
	End Function





	 Function GetImageSize(ByVal FileSavedFolder, ByVal filename, ByRef x)
	 '       폴더의 경로    파일명

'	  if trim(filename) <> "" then
'
'	   Dim p, f
'	   f = Server.MapPath( FileSavedFolder ) & "\" & filename
'	   'response.Write("<br><br><br>"&f)
'	   'Set p = LoadPicture(f)
'	   'x = CLng(CDbl(p.Width) * 24 / 635) ' CLng 의 라운드 오프의 기능을 이용하고 있다.
'	   'y = CLng(CDbl(p.Height) * 24 / 635) ' CLng 의 라운드 오프의 기능을 이용하고 있다.
'	   'Set p = Nothing
'	   Set p = Server.CreateObject("pakImage.ImageSize")   '이미지 사이즈 컴포넌트를 로딩한다.
'
'	    p.ImageSIze f
'
'	    x = p.width
'
'	   Set p = Nothing
'	   GetImageSize = x

'	  end if

	 End Function

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'	Function Name : ImgLmtView2
	'	설명 : 이미지 보여주기
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function ImgLmtView2(upFolder, filename, MaxWidth)

		'//확장자가 이미지인경우만 처리 if 문 2009년 5 13일 작성
		dim imgfilenametype
		filename = trim(filename)
		if len(filename) > 0 then imgfilenametype = split(filename,".")(1)


		if lcase(imgfilenametype) = "jpg" or lcase(imgfilenametype) = "gif" or lcase(imgfilenametype) = "bmp" or lcase(imgfilenametype) = "png" then
			'최대 크기와의 비교
			if Cint(MaxWidth) < Cint(GetImageSize(upFolder, filename, "x")) then
			ImgLmtView2 = "<img src='"&upFolder&filename&"' >"
			else
			ImgLmtView2 = "<img src='"&upFolder&filename&"' border=0><br>"
			end if

			if len(filename) = 0 or isnull(filename) then ImgLmtView2 = "" end if
		end if
	End Function


	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'	Function Name : ImgLmtView
	'	설명 : 이미지 보여주기
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function ImgLmtView(upFolder, filename, MaxWidth)

		'//확장자가 이미지인경우만 처리 if 문 2009년 5 13일 작성
		dim imgfilenametype
		filename = trim(filename)
		if len(filename) > 0 then imgfilenametype = split(filename,".")(1)


		if lcase(imgfilenametype) = "jpg" or lcase(imgfilenametype) = "gif" or lcase(imgfilenametype) = "bmp" or lcase(imgfilenametype) = "png" then
			'최대 크기와의 비교
			'if Cint(MaxWidth) < Cint(GetImageSize(upFolder, filename, "x")) then
			'ImgLmtView = "<a href='"&upFolder&filename&"' target='_blank'><img src='"&upFolder&filename&"' width='"&MaxWidth&"' border=0></a><br>"
			ImgLmtView = "<a href='/avanplus/imgview.asp?img="&upFolder&filename&"' target='_blank'><img src='"&upFolder&filename&"' width='"&MaxWidth&"' border=0></a><br>"
			'else
			'ImgLmtView = "<img src='"&upFolder&filename&"' border=0><br>"
			'end if

			if len(filename) = 0 or isnull(filename) then ImgLmtView = "" end if
		end if
	End Function



	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'	Function Name : ImgLmtView
	'	설명 : 이미지 보여주기
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function ImgBannerView(upFolder, filename, MaxWidth)

		'//확장자가 이미지인경우만 처리 if 문 2009년 5 13일 작성
		dim imgfilenametype
		dim fname : fname = getFileNumName(filename)
		'response.Write(Cint(GetImageSize(upFolder, filename, "x")))
		if len(filename) > 0 then imgfilenametype = split(fname,".")(1)


		if lcase(imgfilenametype) = "jpg" or lcase(imgfilenametype) = "gif" or lcase(imgfilenametype) = "bmp" or lcase(imgfilenametype) = "png" then
			'최대 크기와의 비교
			if Cint(MaxWidth) < Cint(GetImageSize(upFolder, fname, "x")) then
			'ImgLmtView = "<a href='"&upFolder&filename&"' target='_blank'><img src='"&upFolder&filename&"' width='"&MaxWidth&"' border=0></a><br>"
			ImgBannerView = "<img src='"&upFolder&fname&"' width='"&MaxWidth&"' border=0>"
			else
			ImgBannerView = "<img src='"&upFolder&fname&"' border=0>"
			end if

			if len(fname) = 0 or isnull(fname) then ImgBannerView = "" end if
		end if
	End Function
%>


<%

class DextQueryClass

	'//^^^^^^^^^^^^^^^^^^^^^^^^ 사용자 정의 변수 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    'public tablename
	'public where

    '//^^^^^^^^^^^^^^^^^^^^^^^^ 내부 변수 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    '//
    private Dext
	private RequestData
	private FileSaveMode	'//파일저장모드

	private tablename
	private where




	 '//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ class new, nothing 시에 자동 실행 부분 ^^^^^^^^^^^^^^^^^^^^^

    '// 클래스 new(초기화) 시 자동 실행
    private sub class_initialize()

        '//외부에서 들어오는 글 거르기
		''if split(request.ServerVariables("HTTP_REFERER"),"/")(2) <> request.ServerVariables("HTTP_HOST") then response.End()

		dim Rdomain
		Rdomain = request.ServerVariables("HTTP_REFERER")
		Rdomain = mid(Rdomain,8)
		if Rdomain = "" then Rdomain = "http://"
		Rdomain = left(Rdomain,instr(Rdomain,"/")-1)
		''response.Write "<br>넘어온도메인:"&Rdomain
		''response.Write "<br>현재도메인:"&request.ServerVariables("HTTP_HOST")

		'if Rdomain <> request.ServerVariables("HTTP_HOST") then
		'	Error("HTTP_REFERER값이 실제 도메인주소와 동일하지 않습니다.")
		'	response.End()
		'end if


		set Dext = Server.CreateObject("DEXT.FileUpload") 	'// 덱스트 열기
        set RequestData 	= createObject("Scripting.Dictionary") '//데이타 처리부분

		FileSaveMode = false	'//파일저장모드, 디폴트는 중복저장안되게..
		dext.AutoMakeFolder = true
    end sub

    '// 클래스 nothing (set ntpl = nothing) 시 실행
    private sub class_terminate()

        set Dext      	= nothing
        set RequestData = nothing

    end sub
    '//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ /class new, nothing 시에 자동 실행 부분 ^^^^^^^^^^^^^^^^^^^^^






	'//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 사용자 설정에 관련된 부분 ^^^^^^^^^^^^^^^^^^^^^

	'// 디폴트 업로드 경로 설정
	public Sub setUploadFolder(byval Foldername)
		Dext.DefaultPath = Server.MapPath( Foldername ) & "\"
	End Sub


	'// 넘어온 폼이름 출력
	public Sub ViewFormName()
		dim data

		For Each data In dext.form
			if Ucase(Mid(data.name,1,6)) = "SUBMIT" then	'//submit버튼은 제외

			else
				response.Write data.name & ","
			end if
		Next

	End Sub



	'// 테이블 만들기
	public Sub ViewTbaleGride()
		dim data, i
		i = 1
		response.Write "<table width=600 border=1 >"
		For Each data In dext.form
			if Ucase(Mid(data.name,1,6)) = "SUBMIT" then	'//submit버튼은 제외

			else
				response.Write "<tr>"
				response.Write "<td width=50>" & i & "</td>"
				response.Write "<td width=130>" & data.name & "&nbsp;</td>"
				response.Write "<td width=180>" & data.value & "&nbsp;</td>"
				response.Write "<td width=80>  Varchar(50) </td>"
				response.Write "<td width=80>  NULL </td>"
				response.Write "<td width=80> &nbsp;</td>"
				response.Write "</tr>"
			end if
		i = i + 1
		Next
		response.Write "</table>"
	End Sub




	'// 입력문 만들기
	public Sub ViewFiledInsert()
		dim data, i
		i = 1
		response.Write "<br><br><br>"
		For Each data In dext.form
			if Ucase(Mid(data.name,1,6)) = "SUBMIT" then	'//submit버튼은 제외

			else
				response.Write "Call insert.setdataAdd($$"&data.name&"$$, insert.getFormValue($$"&data.name&"$$))<br>"
			end if
		i = i + 1
		Next
	End Sub
	public Sub ViewFiledUpdate()
		dim data, i
		i = 1
		response.Write "<br><br><br>"
		For Each data In dext.form
			if Ucase(Mid(data.name,1,6)) = "SUBMIT" then	'//submit버튼은 제외

			else
				response.Write "Call update.setdataAdd($$"&data.name&"$$, insert.getFormValue($$"&data.name&"$$))<br>"
			end if
		i = i + 1
		Next
	End Sub



	'// table쿼리식 생성
	public Sub ViewCreateTable()
		dim data
		response.Write "Create Table TABLENAME ("
		For Each data In dext.form
			if Ucase(Mid(data.name,1,6)) = "SUBMIT" then	'//submit버튼은 제외

			else
				response.Write data.name & " Varchar(50) NULL, "
			end if
		Next
		response.Write ")"
	End Sub


	'// 넘어온 값 리턴
	public Function getFormValue(byval str)

		If IsObject(dext) = True Then
			getFormValue = dext(str)
		Else
			getFormValue = Request(str)
		End If

	End Function


	public sub setDefaultData(byval colums)

		dim data
		dim strName
		dim strValue
		dim colums_s
		dim i


		For Each data In dext.form

			strName		= trim(data.name)
			strValue 	= trim(data.value)


			'// 넘어온 컬럼과 같은 경우만 변수에 담기, 값이 없으면 무시
			colums_s = split(colums,",")

			For i = 0 to Ubound(colums_s)
				if Ucase(strName) = Ucase(trim(colums_s(i))) then'and len(strValue) > 0 then
					RequestData.add strName, strValue
				end if
			Next

		Next

	End sub


	public sub setStrData(byval colums)
	'//업로드가 필요없는 일반필드 자동으로 세팅
	'//colums에 있는 필드가 있는경우만 데이터 담기
		dim data
		dim strName
		dim strValue
		dim colums_s
		dim i
		For Each data In dext.form
			strName		= trim(data.name)
			strValue 	= trim(data.value)
			'// 넘어온 컬럼과 같은 경우만 변수에 담기
			colums_s = split(colums,",")
			For i = 0 to Ubound(colums_s)
				if Ucase(strName) = Ucase(trim(colums_s(i))) then' and len(strValue) > 0 then
					RequestData.add strName, strValue
				end If

			Next
		Next
	End Sub

	public sub setStrData2(byval colums)
	'//업로드가 필요없는 일반필드 자동으로 세팅
	'//colums에 있는 필드가 있는경우만 데이터 담기
		dim data
		dim strName
		dim strValue
		dim colums_s
		dim i
		For Each data In dext.form
			strName		= trim(data.name)
			strValue 	= trim(data.value)
			'// 넘어온 컬럼과 같은 경우만 변수에 담기
			colums_s = split(colums,",")
			For i = 0 to Ubound(colums_s)
				if Ucase(strName) = Ucase(trim(colums_s(i))) then' and len(strValue) > 0 then
					RequestData.add strName, strValue
				end If


			Next
			'RequestData.RemoveAll()
		Next
	End sub

	public sub setFileData(byval colums, byval opt)
	'//업로드가 필요없는 파일필드 자동으로 세팅
	'//colums에 있는 필드가 있는경우만 데이터 담기

		dim data
		dim strName
		dim strValue
		dim colums_s
		dim i
		For Each data In dext.form
			strName		= trim(data.name)
			strValue 	= trim(data.value)
			'// 넘어온 컬럼과 같은 경우만 변수에 담기
			'response.Write(strName&" --> ")
			colums_s = split(colums,",")
			For i = 0 to Ubound(colums_s)
				'response.Write(strName&":"&colums_s(i)&" , ")
				if Ucase(strName) = Ucase(trim(colums_s(i))) and len(strValue) > 0 then	'//파일업로드는 값이있는경우만 처리
				'	response.Write(" ==========> "&strName&" , "&strValue)
					if opt = "insert" then setInsertNumFileDataAdd(strName)
					if opt = "update" then setupdateNumFileDataAdd(strName)
					if opt = "delete" then setDeleteNumFileDataAdd(strName)
				end if

			Next
			'response.Write("<br>")
		Next
	End sub



	public sub setDataAdd(byval itemname, byval values)
		If len(values) > 0 Then
			requestdata.add itemname, values
		End If
	End sub

	'업데이트 값이 없다면 무조건 업데이트 //최보순 2015.12.14
	public sub setDataAdd2(byval itemname, byval values)
'		If len(values) > 0 Then
			requestdata.add itemname, values
'		End If
	End sub

	public sub setTablename(byval str)
		Tablename = str
	End sub

	public sub setWhere(byval str)
		Where = str
	End sub



	'=====================================
	'//builder_set에서 사용되는 함수
	'=====================================
	public sub ArrDataAdd()
	'//넘어온 컬럼을 디비에 담는데 앞자리 4자리로 해당 필드에 넘어갈 값 확인
	'//txt_ 	: text필드 - 텍스트 에어리어
	'//str_		: 문자열 필드
	'//img_		: 이미지 - 파일업로드 수행

		dim data
		dim strFName
		dim strUName
		dim strValue
		dim strType
		dim	strNameValue
		dim colums_s
		dim i
		dim tempQuery
		For Each data In dext.form
			strFName	= trim(data.name)
			strValue 	= trim(data.value)
			strValue 	= replace(strValue,"'","`")
			strType		= left(strFName,4)
			strUName	= replace(strFName,strType,"")
			if Ucase(Mid(data.name,1,6)) = "SUBMIT" then	'//submit버튼은 제외
			else
				'// 디비에 해당 값이 있는지 확인 - 있으면 업데이트
				strNameValue = getAdoRsScalar("select count(*) from "& tablename &" where strFName = '"& strFName &"'")
					response.Write "<br><br>"& strNameValue & "-->select count(*) from "& tablename &" where strFName = '"& strFName &"'<br>"
				if strNameValue = 0 then	'//값이 없으면 insert

					'//넘어오는 값이 파일인 경우 strValue값을 파일을 저장한 값으로 대체
					if strType="img_" then strValue = getSaveNumFilename(trim(data.name))

					tempQuery = "insert into "& tablename &" (strFname,strUname,strType,strValue) values (N'"& strFname &"',N'"& strUname &"',N'"& strType &"',N'"& strValue &"')"

					AdoConnExecute(tempQuery)
				'	RESPONSE.Write tempQuery
				else '//값이 있으면 업데이트

					if strType="img_" and strValue <> "" then	'//파일이 첨부되어있으면
						'//기존 업로드된 파일삭제
						call setFileDel(getFileNumName(getAdoRsScalar("SELECT strValue FROM "&tablename&" where strFname = '"& strFname &"'" )))
						'//넘어오는 값이 파일인 경우 strValue값을 파일을 저장한 값으로 대체
						strValue = getSaveNumFilename(trim(data.name))

						tempQuery = "update "& tablename &" set strValue=N'"& strValue &"' where strFname = '"& strFname &"'"
						'// 파일이 있는경우만 디비 업데이트
						if len(strValue) > 0 then AdoConnExecute(tempQuery)
					elseif strType<>"img_" then
						tempQuery = "update "& tablename &" set strValue=N'"& strValue &"' where strFname = '"& strFname &"'"
						'일반필드는 언제나 업데이트
						AdoConnExecute(tempQuery)
					end if
				'	RESPONSE.Write(tempQuery)
				end if
			end if

		Next
	End sub


	'// ArrDataAdd에서만 사용하는 함수
	Private sub ArrDataUpdateFile(byval FormName)
		'response.Write(dext.form(FormName))
		if len(dext.form(FormName)) > 0 then

			call setDataAdd(FormName, getSaveNumFilename(FormName))

			'//기존 파일 삭제
			call setFileDel(getFileNumName(getAdoRsScalar("SELECT "& FormName &" FROM "&tablename&" WHERE "& Where )))
		end if
	End Sub

	'//==================================================





	'//##################################
	'//Dext File 관련 설정파일
	'//##################################

	'//기존의 파일을 삭제하는 함수
	public sub setFileDel(byval filename)
		dim filepath
		filepath = dext.DefaultPath & "\" & filename
			'response.Write(filepath)
		If Dext.FileExists(filepath) Then '//파일이 존재하는지 체크 / 해당파일이 있다면
			Dext.DeleteFile filepath	'//파일삭제
		End If
	end Sub

	'// 파일을 저장하고 저장된 파일이름 리턴
	public Function getSaveFilename(byval FormName)
		'TF : true 중복저장 false 새로운 이름으로 저장
		'FormName : DEXT로 넘어온 값 dext("file_sample")
		'response.Write  dext.form(FormName)
		'FormName		= dext.form(FormName)
		if len(dext.form(FormName)) > 0 then
			getSaveFilename = dext.form(FormName).filename
			response.Write "<br>1->"&getSaveFilename
			getSaveFilename = Dext.SaveAs(Dext.DefaultPath & getSaveFilename, FileSaveMode)
				'response.Write "<br>2 "&getSaveFilename
			getSaveFilename = Dext.LastSavedFileName
		end if
			'response.Write "<br>3 "&getSaveFilename
			'response.Write "<br>3 "&FormName
	End Function

	'// 파일을 저장하고 저장된 파일이름 리턴
	public Function getSaveFilename2(byval FormName)
		'TF : true 중복저장 false 새로운 이름으로 저장
		'FormName : DEXT로 넘어온 값 dext("file_sample")

		'FormName		= dext.form(FormName)
		if len(dext.form(FormName)) > 0 Then
			arrFile=Split(dext.form(FormName),",")
		'response.write arrFile(0)&"//"&arrFile(1)
			For i = 0 To UBound(arrFile, 1)
				If Len(arrFile(i)) > 0 Then
					getSaveFilename = arrFile(i)
					response.Write "<br>1->"&getSaveFilename
					getSaveFilename = Dext.SaveAs(Dext.DefaultPath & getSaveFilename, FileSaveMode)
						'response.Write "<br>2 "&getSaveFilename
					getSaveFilename = Dext.LastSavedFileName
				End if
			Next
		end if
			'response.Write "<br>3 "&getSaveFilename
			'response.Write "<br>3 "&FormName
	End Function

	'// 파일을 저장하고 저장된 파일이름 리턴
	public Function getSaveFilename4(ByVal val, byval FormName)
		'TF : true 중복저장 false 새로운 이름으로 저장
		'FormName : DEXT로 넘어온 값 dext("file_sample")

		'FormName		= dext.form(FormName)
		if len(dext.form(FormName)) > 0 Then
			arrFile=Split(dext.form(FormName),",")
		'response.write arrFile(0)&"//"&arrFile(1)
			For k = 0 To UBound(arrFile, 1)
				If Len(arrFile(k)) > 0 Then

					If k=val Then
					getSaveFilename4 = arrFile(k)
				'	response.Write "<br>1->"&getSaveFilename&"<br>"
					getSaveFilename4 = Dext.SaveAs(Dext.DefaultPath & getSaveFilename4, FileSaveMode)
						'response.Write "<br>2 "&Dext.LastSavedFilePath&"<br>"
					getSaveFilename4 = Dext.LastSavedFilePath
					'response.Write("<img src='/upload/lecture/"&getSaveFilename4&"'>")
					'response.write arrFile(k)&"<br>"
				End If
				End if
			Next
		end if
			'response.Write "<br>3 "&getSaveFilename
			'response.Write "<br>3 "&FormName
	End Function

		'// 파일을 저장하고 저장된 파일이름 리턴
	public Function getSaveFilename3(ByVal val, byval FormName)
		'TF : true 중복저장 false 새로운 이름으로 저장
		'FormName : DEXT로 넘어온 값 dext("file_sample")
	'	response.Write  dext.form(FormName)
		'FormName		= dext.form(FormName)
		if len(FormName) > 0 Then

		For k=1 To dext.form(FormName).count

				If k=val+1 Then
				getSaveFilename3 = dext.form(FormName)(k).filename

				'//파일 확장자 리턴
				dim FileExtens
				FileExtens = dext.form(FormName)(k).FileExtension

				'//유일한 파일이름 만들기
				dim unique_filename
				unique_filename	=	Replace(Date(),"-", "") & Hour(now()) & Minute(now()) & Second(now()) & Session.SessionID &k

				'//변화된 유니크한 파일
				dim unique_filename_up
				'unique_filename_up = unique_filename &"."& FileExtens

				unique_filename_up = Replace(getSaveFilename3," ","")

				getSaveFilename3 = dext(FormName)(k).SaveAs(Dext.DefaultPath & unique_filename_up, FileSaveMode)
				getSaveFilename3=  dext(FormName)(k).LastSavedFileName
				End If

			'	response.Write getSaveFilename3&"<br>"
			next
		end if
	'	response.Write("<img src='/upload/lecture/"&getSaveFilename3&"'>")
	End Function

	'// 파일을 저장한후 정보를 Dic에 저장
	Public sub setSaveFileDataAdd(byval FormName)
		if len(dext.form(FormName)) > 0 then
			call setDataAdd(FormName, getSaveFilename(FormName))
		end if
	End Sub



	'//========================================================

	'	추가사항 2006.5.2 이덕권
	'	년월일시간분초+세션아이디.확장자:실제파일명:확장자:크기
	'	형식으로 리턴

	'========================================================//

	'// 년월일시간분초+세션아이디.확장자:실제파일명:확장자:크기
	public Function getSaveNumFilename(byval FormName)
		'TF : true 중복저장 false 새로운 이름으로 저장
		'FormName : DEXT로 넘어온 값 dext("file_sample")
		'response.Write  dext.form(FormName)
		'FormName		= dext.form(FormName)
		if len(dext.form(FormName)) > 0 then

			'//파일의 본래 이름 받기
			dim original_filename
			original_filename = dext.form(FormName).filename
			'response.Write "<br>0: "&original_filename

			'//업로드 파일명 리턴
			dim FileNameNoExtens
			FileNameNoExtens = dext.form(FormName).FileNameWithoutExt
			'response.Write "<br>0: "&FileNameNoExtens

			'//파일 확장자 리턴
			dim FileExtens
			FileExtens = dext.form(FormName).FileExtension
			'response.Write "<br>0: "&FileExtens

			'//파일 길이 리턴
			dim FileLens
			FileLens = dext.form(FormName).FileLen
			'response.Write "<br>0: "&FileLens

			'//유일한 파일이름 만들기
			dim unique_filename
			unique_filename	=	Replace(Date(),"-", "") & Hour(now()) & Minute(now()) & Second(now()) & Session.SessionID
				'response.Write "<br>1: "&unique_filename

			'//변화된 유니크한 파일
			dim unique_filename_up
			unique_filename_up = unique_filename &"."& FileExtens
				'response.Write "<br5: "&unique_filename_up

			'//파일저장
			call Dext(FormName).SaveAs(Dext.DefaultPath & unique_filename_up , FileSaveMode)
				'response.Write "<br>2: "&getSaveNumFilename


			'getSaveNumFilename = Dext.LastSavedFileName
			dim LastSavedFileName
			LastSavedFileName= Dext(FormName).LastSavedFileName
			getSaveNumFilename = LastSavedFileName &":"& original_filename &":"& FileExtens &":"& FileLens

			'//공백으로 인한 다운로드시 파일에러 처리 2009-05-11


			getSaveNumFilename = Replace(getSaveNumFilename," ","")
				';response.Write "<br>3: "&getSaveNumFilename
		end if
			'response.Write "<br>3 "&getSaveFilename
			'response.Write "<br>3 "&FormName
	End Function


	'// 년월일시간분초+세션아이디.확장자:실제파일명:확장자:크기
	public Function getSaveNumFilename2(byval FormName)
		'TF : true 중복저장 false 새로운 이름으로 저장
		'FormName : DEXT로 넘어온 값 dext("file_sample")
		'response.Write  dext.form(FormName)
		'FormName		= dext.form(FormName)
		if len(dext.form(FormName)) > 0 then
			'response.write dext.form(FormName).count &"<br>"
			For i=1 To dext.form(FormName).count
				'response.write i&"<br>"

				'//파일의 본래 이름 받기
				dim original_filename
				original_filename = dext.form(FormName)(i).filename
				'response.Write "<br>0: "&original_filename

				'//업로드 파일명 리턴
				dim FileNameNoExtens
				FileNameNoExtens = dext.form(FormName)(i).FileNameWithoutExt
				'response.Write "<br>0: "&FileNameNoExtens

				'//파일 확장자 리턴
				dim FileExtens
				FileExtens = dext.form(FormName)(i).FileExtension
				'response.Write "<br>0: "&FileExtens

				'//유일한 파일이름 만들기
				dim unique_filename
				unique_filename	=	Replace(Date(),"-", "") & Hour(now()) & Minute(now()) & Second(now()) & Session.SessionID
					'response.Write "<br>1: "&unique_filename

				'//변화된 유니크한 파일
				dim unique_filename_up
				unique_filename_up = unique_filename &"."& FileExtens
					'response.Write "<br5: "&unique_filename_up

				'//파일저장
				call Dext(FormName(i)).SaveAs(Dext.DefaultPath & unique_filename_up , FileSaveMode)
					'response.Write "<br>2: "&getSaveNumFilename


				'getSaveNumFilename = Dext.LastSavedFileName
				dim LastSavedFileName
				LastSavedFileName= Dext(FormName(i)).LastSavedFileName
				'getSaveNumFilename = LastSavedFileName &":"& original_filename &":"& FileExtens &":"& FileLens
				getSaveNumFilename2 = LastSavedFileName

			'	response.Write LastSavedFileName
				'//공백으로 인한 다운로드시 파일에러 처리 2009-05-11
				getSaveNumFilename2 = Replace(LastSavedFileName," ","")
					'response.Write "<br>3: "&getSaveNumFilename
			next
		end if
			'response.Write "<br>3 "&getSaveFilename
			'response.Write "<br>3 "&FormName
	End Function

	'// 년월일시간분초+세션아이디.확장자:실제파일명:확장자:크기
	public Function getSaveNumFilename3(byval FormName)
		'TF : true 중복저장 false 새로운 이름으로 저장
		'FormName : DEXT로 넘어온 값 dext("file_sample")
	'response.Write dext.form( FormName)&"<br>"
	'	Formname		= dext.form(FormName)
		FormName2="lt_file"
		if len(FormName) > 0 then
			'Dim arrData
			'arrData = Split(Trim(dext.form(FormName)),",")

		'	For i = 0 To UBound(arrData, 1)
				'response.write arrData(i)&"<br>"
				'//파일의 본래 이름 받기
				dim original_filename
				original_filename = FormName
				'response.Write "<br>0: "&original_filename

				'//업로드 파일명 리턴
				dim FileNameNoExtens
				FileNameNoExtens = Mid(FormName, InstrRev(FormName,"\")+1)
				'response.Write "<br>0: "&FileNameNoExtens&"<br>"

				'//파일 확장자 리턴
				dim FileExtens
				FileExtens =Mid(FormName, InstrRev(FormName,".")+1)
				'response.Write "<br>1: "&FileExtens&"<br>"

				'//파일 길이 리턴
				'dim FileLens
			'	FileLens = dext.form(FormName).FileLen
				'response.Write "<br>0: "&FileLens

				'//유일한 파일이름 만들기
				dim unique_filename
				unique_filename	=	Replace(Date(),"-", "") & Hour(now()) & Minute(now()) & Second(now()) & Session.SessionID
					'response.Write "<br>1: "&unique_filename

				'//변화된 유니크한 파일
				dim unique_filename_up
				unique_filename_up = unique_filename &"."& FileExtens
					'response.Write "<br5: "&unique_filename_up

				'//파일저장
				call Dext(FormName2).SaveAs(Dext.DefaultPath & unique_filename_up , FileSaveMode)
					'response.Write "<br>2: "&getSaveNumFilename&"<br>"


				'getSaveNumFilename = Dext.LastSavedFileName
				dim LastSavedFileName
				LastSavedFileName= Dext(FormName2).LastSavedFileName
				'getSaveNumFilename = LastSavedFileName &":"& original_filename &":"& FileExtens &":"& FileLens
				getSaveNumFilename2 = LastSavedFileName

			'	response.Write LastSavedFileName
				'//공백으로 인한 다운로드시 파일에러 처리 2009-05-11
				getSaveNumFilename2 = Replace(LastSavedFileName," ","")
					'response.Write "<br>3: "&getSaveNumFilename2&"<br>"
			'Next
		end if
			'response.Write "<br>3 "&getSaveFilename
			'response.Write "<br>3 "&FormName
	End Function

	'// 파일을 저장한후 정보를 Dic에 저장
	Public sub setSaveNumFileDataAdd(byval FormName)
		if len(dext.form(FormName)) > 0 then
			call setDataAdd(FormName, getSaveNumFilename(FormName))
		end if
	End Sub


	'// 파일을 저장한후 정보를 Dic에 저장 2009-05-11
	Public sub setInsertNumFileDataAdd(byval FormName)
		if len(dext.form(FormName)) > 0 then
			call setDataAdd(FormName, getSaveNumFilename(FormName))
		end if
	End Sub

	'// 파일을 저장및 기존 파일 삭제 후 정보를 Dic에 저장 2009-05-11
	Public sub setUpdateNumFileDataAdd(byval FormName)

		if len(dext.form(FormName)) > 0 then

			call setDataAdd(FormName, getSaveNumFilename(FormName))

			'//기존 파일 삭제
			call setFileDel(getFileNumName(getAdoRsScalar("SELECT "& FormName &" FROM "&tablename&" WHERE "& Where )))
		end if
	End Sub



	'// 기존파일 삭제 2009-05-11
	Public sub setDeleteNumFileDataAdd(byval FormName)
			'//기존 파일 삭제
			call setFileDel(getFileNumName(getAdoRsScalar("SELECT "& FormName &" FROM "&tablename&" WHERE "& Where )))
	End Sub




	Public sub setFileSaveMode(byval TF)
		FileSaveMode = TF
	End Sub






	'// 파일을 저장하고 저장된 파일이름 리턴
	public Function getSaveAsFilename(byval FormName, byval gubun, byval code)
									'//폼값				'//실제저장될 파일명

		dim newname, oldname, savefullpath, ext
		if len(dext.form(FormName)) > 0 then

			newname = getimgName(gubun, code)
			oldname = Dext.form(FormName).filename
			'원래 파일의 확장자를 얻기 위한 부분(파일이름이 바뀌어도 확장자를 유지하기 위함)
			ext = "." & right(oldname,len(oldname)-instr(oldname,"."))
			'response.Write(">>>>>>>>>>>>>>>>>>>>>>>"&ext)
			savefullpath = Dext.DefaultPath & "\"&newname&ext
			getSaveAsFilename = Dext.form(FormName).SaveAs(savefullpath)
			'response.Write savefullpath
			getSaveAsFilename = Dext.form(FormName).LastSavedFileName
			'getSaveAsFilename = newname&ext

		end if

	End Function







	'//################################################
	'//
	'//	쿼리문 생성로직
	'//
	'//################################################


	public Function getQueryInsert
	'##	insert 쿼리생성

	' 형식 insert Table (컬럼1, 컬럼2) VALUES ('a','b')
	dim item, strName, strValue, colume_name, colume_values, colume_count
	colume_count = 0

	colume_name = "insert into " & TableName & " ("
	colume_values = " VALUES ("

	For Each item In RequestData
   	strName = all_input(item)
		strValue = all_input(RequestData(item))
			if colume_count = 0 then
				colume_name 	= colume_name 	& strName
				if left(lcase(strName),3) = "int" then
					colume_values	= colume_values	& "'" & all_input(strValue) & "' "
				else
					colume_values	= colume_values	& "N'" & all_input(strValue) & "' "
				end if
				colume_count = colume_count + 1
			else
				colume_name 	= colume_name 	& "," &strName

				if left(lcase(strName),3) = "int" then
					colume_values	= colume_values	& ",'" & all_input(strValue) & "' "
				else
					colume_values	= colume_values	& ",N'" & all_input(strValue) & "' "
					'response.Write("[[["&colume_values&"]]]")
				end if
			end if
	Next

	colume_name 	= colume_name 	& " ) "
	colume_values	= colume_values	& " ) "
	getQueryInsert = colume_name & colume_values

	End Function


	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'	설명 : update 쿼리문 생성기
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	public Function getQueryUpdate
	'##	update 쿼리생성
	' 형식 insert Table (컬럼1, 컬럼2) VALUES ('a','b')
	dim item, strName, strValue, Uquery, colume_count
	colume_count = 0

	Uquery = "Update " & TableName & " set "

	For Each item In RequestData
  	strName = all_input(item)
		strValue = all_input(RequestData(item))

			if colume_count = 0 then
				if left(lcase(strName),3) = "int" then
					Uquery 	= Uquery 	& strName & "='" & all_input(strValue) &"'"
				else
					Uquery 	= Uquery 	& strName & "=N'" & all_input(strValue) &"'"
				end if

				colume_count = colume_count + 1
			else
				if left(lcase(strName),3) = "int" then
					Uquery 	= Uquery 	& "," & strName & "='" & all_input(strValue) &"'"
				else
					Uquery 	= Uquery 	& "," & strName & "=N'" & all_input(strValue) &"'"
				end if
			end if

	Next


	if trim(where) = "" or isnull(where) then
		Uquery 	= "Errer : where부분 조건이 없습니다. 이경우 모든 데이타를 수정 하기에 실행하지 않습니다."
	else
		Uquery 	= Uquery 	& " where " & where
	end if

	getQueryUpdate = Uquery
	End Function


	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'	설명 : delete 쿼리문 생성기
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	public Function getQueryDelete
	'##	Delete 쿼리생성
	Dim Dquery
	if trim(where) <> "" then
		Dquery 	= "Delete from " & tablename & " where "& where
	else
		Dquery 	= "where부분 조건이 없습니다. 이경우 모든 데이타를 삭제 하기에 실행하지 않습니다."
	end if

	getQueryDelete = Dquery
	End Function


End Class


Sub FileDownLink(DBFileData)
	dim linkurl
	if len(DBFileData) > 0 then
		Response.Write "<a href=/AVANplus/filedownload.asp?o_file="&split(DBFileData,":")(0)&"&u_file="&split(DBFileData,":")(1)&">"
		Response.Write split(DBFileData,":")(1)&"</a>"
	end if
End Sub

Sub FileDownLink_upPath(DBFileData, byval uppath)
	dim linkurl
	if len(DBFileData) > 0 then
		if Ubound(split(DBFileData,":")) > 0 then
			Response.Write "<a href=/AVANplus/filedownload.asp?o_file="&split(DBFileData,":")(0)&"&uppath="&uppath&"&u_file="&split(DBFileData,":")(1)&">"
			Response.Write split(DBFileData,":")(1)&"</a>"
		else
			Response.Write "<a href=/AVANplus/filedownload.asp?o_file="&DBFileData&"&uppath="&uppath&"&u_file="&DBFileData&">"
			Response.Write "파일다운</a>"
		end if
	end if
End Sub



Sub FileDownLink_old(DBFileData, byval uppath)	'//기존게시판용 파일이름 그대로 올라간 경우
	dim linkurl
	if len(DBFileData) > 0 then
		Response.Write "<a href=/AVANplus/filedownload.asp?o_file="&DBFileData&"&uppath="&uppath&"&u_file="&DBFileData&">"
		Response.Write DBFileData&"</a>"
	end if
End Sub

'//2009년 5월 11일 추가/ 폴더가 upload가 아닌경우를 위한 처리
Sub FileDownLink_board(DBFileData)
	dim linkurl
	if len(DBFileData) > 0 then

		Response.Write "<img src = /crm/order/img/disk.gif border=0 align=absmiddle> <a href='/AVANplus/filedownload.asp?o_file="&DBFileData&"&u_file="&DBFileData&"'>"
		Response.Write DBFileData&"</a>"
	end if
End Sub

Sub FileDownLink_AVANboard(DBFileData)
	dim linkurl
	if len(DBFileData) > 0 then

		Response.Write "<img src =/AVANplus/img/disk.gif border=0 align=absmiddle> <a href='/AVANplus/modul/avanboard_v2/filedownload.asp?o_file="&DBFileData&"&u_file="&DBFileData&"'>"
		Response.Write DBFileData&"</a>"
	end if
End Sub

Function getFileNumName(DBFileData)
	if len(DBFileData) > 0 then
		getFileNumName = split(DBFileData,":")(0)
	end if
End Function

Function getFileName(DBFileData)
	If Len(DBFileData) > 0 Then
		If InStr(DBFileData, ":") > 0 Then
			getFileName = Split(DBFileData,":")(1)
		Else
			getFileName = DBFileData
		End If
	End If
End Function

%>



<%

    '****************************************************************************************
    '*
    '*  URLTools.asp 는 공개 Include File 입니다. 상업적, 비상업적인 목적으로 어디서나
    '*  자유롭게 사용하실 수 있습니다.
    '*
    '*  다만, 기능 추가나 에러 수정을 목적으로 Code 를 수정하셨을 때는, 제게 수정된 내용과
    '*  목적에 대하여 적은 메일을 보내주셔서 제가 수정사항을 반영할 수 있도록 해주시면 감사하
    '*  겠습니다. 또한, 이런식으로 수정된 Code 는 그 내용과 이유를 정리하여 제 홈페이지의
    '*  내용에 추가하도록 하겠습니다. 연락처는 다음과 같습니다.
    '*
    '*  Homepage URL : http://www.egocube.pe.kr/
    '*  E-Mail       : songgun@egocube.pe.kr
    '*
    '****************************************************************************************


    '****************************************************************************************
    '*
    '*  형 식 : Function
    '*  정 의 : Public Function URLEncode(URLStr)
    '*  설 명 : URLStr 인자로 입력받은 문자열을 URLEncoding 한다.
    '*  작 성 : 송원석
    '*  날 짜 : 2001.12.03
    '*
    '****************************************************************************************

    Public Function URLEncode(URLStr)

        Dim sURL                '** 입력받은 URL 문자열
        Dim sBuffer             '** Encoding 중의 URL 을 담을 Buffer 문자열
        Dim sTemp               '** 임시 문자열
        Dim cChar               '** URL 문자열 중의 현재 Index 의 문자

        Dim Index


    On Error Resume Next


        Err.Clear
        sURL = Trim(URLStr)     '** URL 문자열을 얻는다.
        sBuffer = ""            '** 임시 Buffer 용 문자열 변수 초기화.


        '******************************************************
        '* URL Encoding 작업
        '******************************************************

        For Index = 1 To Len(sURL)

            '** 현재 Index 의 문자를 얻는다.
            cChar = Mid(sURL, Index, 1)

            If cChar = "0" Or _
               (cChar >= "1" And cChar <= "9") Or _
               (cChar >= "a" And cChar <= "z") Or _
               (cChar >= "A" And cChar <= "Z") Or _
               cChar = "-" Or _
               cChar = "_" Or _
               cChar = "." Or _
               cChar = "*" Then

                '** URL 에 허용되는 문자들 :: Buffer 문자열에 추가한다.
                sBuffer = sBuffer & cChar

            ElseIf cChar = " " Then

                '** 공백 문자 :: + 로 대체하여 Buffer 문자열에 추가한다.
                sBuffer = sBuffer & "+"

            Else

                '** URL 에 허용되지 않는 문자들 :: % 로 Encoding 해서 Buffer 문자열에 추가
                sTemp = CStr(Hex(Asc(cChar)))

                If Len(sTemp) = 4 Then

                    sBuffer = sBuffer & "%" & Left(sTemp, 2) & "%" & Mid(sTemp, 3, 2)

                ElseIf Len(sTemp) = 2 Then

                    sBuffer = sBuffer & "%" & sTemp

                End If

            End If

        Next


        '** Error 처리
        If Err.Number > 0 Then

            URLEncode = ""
            Exit Function

        End If

        '** 결과를 리턴한다.
        URLEncode = sBuffer

        Exit Function


    End Function


    '****************************************************************************************
    '*
    '*  형 식 : Function
    '*  정 의 : Public Function URLDecode(URLStr)
    '*  설 명 : URLStr 인자로 입력받은 문자열을 URLDecoding 한다.
    '*  작 성 : 송원석
    '*  날 짜 : 2001.12.03
    '*
    '****************************************************************************************

    Public Function URLDecode(URLStr)

        Dim sURL                '** 입력받은 URL 문자열
        Dim sBuffer             '** Decoding 중의 URL 을 담을 Buffer 문자열
        Dim cChar               '** URL 문자열 중의 현재 Index 의 문자

        Dim Index


    On Error Resume Next


        Err.Clear
        sURL = Trim(URLStr)     '** URL 문자열을 얻는다.
        sBuffer = ""            '** 임시 Buffer 용 문자열 변수 초기화.


        '******************************************************
        '* URL Decoding 작업
        '******************************************************

        Index = 1

        Do While Index <= Len(sURL)

            cChar = Mid(sURL, Index, 1)

            If cChar = "+" Then

                '** '+' 문자 :: ' ' 로 대체하여 Buffer 문자열에 추가한다.
                sBuffer = sBuffer & " "
                Index = Index + 1

            ElseIf cChar = "%" Then

                '** '%' 문자 :: Decoding 하여 Buffer 문자열에 추가한다.
                cChar = Mid(sURL, Index + 1, 2)

                If CInt("&H" & cChar) < &H80 Then

                    '** 일반 ASCII 문자
                    sBuffer = sBuffer & Chr(CInt("&H" & cChar))
                    Index = Index + 3

                Else

                    '** 2 Byte 한글 문자
                    cChar = Replace(Mid(sURL, Index + 1, 5), "%", "")
                    sBuffer = sBuffer & Chr(CInt("&H" & cChar))
                    Index = Index + 6

                End If

            Else

                '** 그 외의 일반 문자들 :: Buffer 문자열에 추가한다.
                sBuffer = sBuffer & cChar
                Index = Index + 1

            End If

        Loop


        '** Error 처리
        'If Err.Number > 0 Then

           ' URLDecode = ""
           ' Exit Function

       ' End If

        '** 결과를 리턴한다.
        URLDecode = sBuffer

        Exit Function


    End Function


	'*********************************************************
	'  함수명 :	ThumbnailJpg
	'  설  명 :	이미지를 지정한 크기로 썸네일 이미지파일 생성
	'  인  수 :	strImgWebPath		실제 이미지 웹 절대 경로
	'			strSmallImgName		썸네일이미지 파일명에 덧붙일 문자열
	'			blnFactorUse			비율대로 줄일지 유무
	'			intQuality				JPEG 퀄리티
	'			intUserWidth			줄일 가로 크기
	'			intUserHeight			줄일 세로 크기
	'  반환값 :	실제 저장된 썸네일 이미지 웹절대 경로
	'*********************************************************
	Function ThumbnailJpg(ByVal strImgWebPath, ByVal strSmallImgName, ByVal blnFactorUse, ByVal intQuality, ByVal intUserWidth, ByVal intUserHeight)

		Dim strSaveFolderPath, strImgName, arrFile, strFileName, strFileExt
		Dim strSmallImgWebPath, strSmallImgPhysicalPath, strImgPhysicalPath
		Dim strFileType, objCxImage, intImgWidth, intImgHeight, intFactorX, intFactorY, intFactor, intImgWidthS, intImgHeightS

		'// 저장될 경로
		If InStr(strImgWebPath, "/") > 0 Then
			strSaveFolderPath = Left(strImgWebPath, InStrRev(strImgWebPath, "/"))
		Else
			strSaveFolderPath = ""
		End If

		'// 큰이미지 폴더경로 없애구 파일명 구하기
		strImgName = Right(strImgWebPath, Len(strImgWebPath) - InStrRev(strImgWebPath, "/"))

		'// 큰 이미지 파일명과 확장자로 분리
		arrFile = Split(strImgName, ".")
		strFileName = arrFile(0)
		If UBound(arrFile, 1) = 1 Then
			strFileExt = arrFile(1)
		End If

		'// 작은이미지 저장될 경로
		strSmallImgWebPath = strSaveFolderPath & strFileName & strSmallImgName & ".jpg"
		strSmallImgPhysicalPath = Server.MapPath(strSmallImgWebPath)

		'// 큰 이미지 실제경로
		strImgPhysicalPath = Server.MapPath(strImgWebPath)

		Select Case strFileExt
			Case "bmp"		: strFileType = 0
			Case "gif"		: strFileType = 1
			Case "jpg"		: strFileType = 2
			Case "jpeg"		: strFileType = 2
			Case "png"		: strFileType = 3
			Case "ico"		: strFileType = 4
			Case "tif"		: strFileType = 5
			Case "tga"		: strFileType = 6
			Case "pcx"		: strFileType = 7
			Case Else		: strFileType = 2
		End Select

		Set objCxImage = Server.CreateObject("CxImageATL.CxImage")

			objCxImage.Load strImgPhysicalPath, strFileType
			objCxImage.IncreaseBpp(24)

			If blnFactorUse Then
				'// 가로 세로 길이 구하기
				intImgWidth = Int(objCxImage.GetWidth())
				intImgHeight = Int(objCxImage.GetHeight())

				'// 가로 세로 줄일려고 하는 비율
				intFactorX = intImgWidth / intUserWidth
				intFactorY = intImgHeight / intUserHeight

				If intFactorX > intFactorY Then
					intFactor = intFactorX
				Else
					intFactor = intFactorY
				End If

				If intFactor < 1 Then
					intFactor = 1
				End If

				intImgWidthS = intImgWidth / intFactor
				intImgHeightS = intImgHeight / intFactor

			Else
				intImgWidthS = intUserWidth
				intImgHeightS = intUserHeight

			End If

			objCxImage.Resample intImgWidthS, intImgHeightS, 2
			objCxImage.Save strSmallImgPhysicalPath, 2

		Set objCxImage = Nothing

		ThumbnailJpg = strSmallImgWebPath

	End Function



'//2005년 12월

' ===========================
'  Function to GetHTMLBin
' ===========================
Function GetHTMLBin(URLaddress)
	Dim Http
	Set Http = CreateObject("Microsoft.XMLHTTP")
		Http.Open "GET", URLaddress, False
		GetHTMLBin = Http.Send
		GetHTMLBin = Http.responseBody
	Set Http = Nothing
End Function

' ===========================
'  Function to BinToText
' ===========================
Function BinToText(varBinData, intDataSizeInBytes)    ' as String

	dim objRS

	Const adFldLong = &H00000080
	Const adVarChar = 200
	Set objRS = CreateObject("ADODB.Recordset")
		objRS.Fields.Append "txt", adVarChar, intDataSizeInBytes, adFldLong
		objRS.Open
		objRS.AddNew
		objRS.Fields("txt").AppendChunk varBinData
		BinToText = objRS("txt").Value
		objRS.Close
	Set objRS = Nothing
End Function


' ===========================
'  처리된 문서를 html변수로 담는다.
' ===========================
Function HtmlString(byval GetURL)
	dim HTMLBin
	HTMLBin = GetHTMLBin(GetURL)
	HtmlString = BinToText(HTMLBin,32000)
End Function


'response.Write HtmlString("http://search.naver.com/search.naver?where=nexearch&query=ADODB.Recordset+%B0%B4%C3%BC+addnew+%B7%B9%C6%DB%B7%B1%BD%BA&hw=1")
Sub HtmlMailSend(byval strfrom, byval strto, byval strcc, byval strTitle, byval strURL)
	Call HtmlMailSend_2003(strfrom, strto, strcc, strTitle, strURL, "")
End Sub

Sub HtmlMailSend_2003(byval strfrom, byval strto, byval strcc, byval strTitle, byval strURL,byval strAttach)
	'//보내는 사람 / 받는사람 / 참조자 / 제목 / 보낼문서경로
	' 메일 객제 생성
	Dim objMail,objConfig,Flds
	set objMail = server.CreateObject("CDO.Message")
	set objConfig = createobject("CDO.Configuration")

	' SMTP Configuration
	Set Flds = objConfig.Fields

	With Flds
	.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
	' SMTP 발송 서버 설정 (이 부분은 꼭 필요한 부분 입니다.)
	.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
	.update
	End With

	With objMail
	.Configuration = objConfig
	.To = strto '받는 사람 이메일 주소
	.From = strfrom '보내는 사람 이메일 주소
	.Cc = strcc '참조 이메일 주소
	.Bcc = "" '숨은 참조 이메일 주소
	.Subject = strTitle '제목
	' 내용
	if lcase(mid(strURL, 1, 7)) = "http://" then
		.HTMLBody 			=	HtmlString(strURL)
	else
		.HTMLBody 			=	strURL
	end if

	' 원하는 첨부 파일 메일에 첨부
	If len(strAttach) > 0 Then '//첨부파일
		.AddAttachment strAttach'"d:\000xxx\www\test.doc"
	End If

	.fields.update
	.Send
	End With

	set objMail = nothing
	set objConfig = nothing

End Sub

Sub HtmlMailSend_2000(byval strfrom, byval strto, byval strcc, byval strTitle, byval strURL)
	'//윈2000용
	'//보내는 사람 / 받는사람 / 참조자 / 제목 / 보낼문서경로
	dim objMail
	set objMail = server.CreateObject("cdonts.newmail")

		objMail.from 			=	strfrom
		objMail.to 				=	strto
		objMail.cc 				=	strcc
		objMail.subject 		=	strTitle
		'response.Write "<br><br>"&lcase(mid(strURL, 1, 7))&"<br><br>"
		if lcase(mid(strURL, 1, 7)) = "http://" then
			objMail.body 			=	HtmlString(strURL)
		else
			objMail.body 			=	strURL
		end if


		objMail.bodyFormat 		= 0
		objMail.mailFormat 		= 0
		objMail.importance 		= 0 '<--중요도
		objMail.send

	set objMail = nothing
End Sub

'//썬(첨부파일 포함한 메일발송
Sub HtmlMailSend2(byval strfrom, byval strto, byval strcc, byval strTitle, byval strURL,byval strAttach)
	Call HtmlMailSend_2003(strfrom, strto, strcc, strTitle, strURL, "")
End Sub


%>



<%
'// 2006/05/11 추가분


	Sub bitCheckbox(byval Cname, byval str)

		response.write " <input name=" & Cname
		response.write " value=1 "
		if str = "1" or str = true then response.write " checked "
		response.write " type=checkbox > "


	End Sub


'// 민호_넥슨 추가분

'// INSERT 쿼리실행 후 생성된 시퀀스 값 반환
 Function getAdoExecuteID(ByVal strQuery)

	  Dim objAdoRs, strValue

	  'Print strQuery

	  strQuery = strQuery & " ; SELECT @@IDENTITY "

	  Set objAdoRs = Server.CreateObject("ADODB.RecordSet")

		   objAdoRs.Open strQuery, dbconn, adOpenForwardOnly, adLockReadOnly, adCmdText

				Set objAdoRs = objAdoRs.NextRecordSet

					If Not (objAdoRs.EOF And objAdoRs.BOF) Then
					 	strValue = objAdoRs(0)
					Else
					 	strValue = Null
					End If

		   objAdoRs.Close




	  Set objAdoRs = Nothing

	  getAdoExecuteID = strValue

 End Function
%>

<%

	'//2006년 5월 10일 등록내용
	'// 이덕권
	'// sql injection  문제 해결

	Function requestF(byval str)
		str = request.Form(str)
		requestF = all_input(str)
	End Function

	Function requestQ(byval str)
		str = request.QueryString(str)
		requestQ = all_input(str)
	End Function

	Function requestAll(byval str)
		str = request(str)
		requestAll = all_input(str)
	End Function

	FuncTion requestS(byval str) '//검색시 폼값이 있으면 우선 폼을 먼저 받음
		requestS = requestQ(str)
		if len(requestF(str)) > 0  then requestS = requestF(str)
	End Function
'//2006년 6월 13일 등록내용 // 한국 토고전 3:1로 이긴날
	'// 이덕권
	'// 돈을 집어넣으면 \100,000형식으로 리턴
	Function FormatW(byval money)

		if isValue(money) then
			money = replace(money,",","")
			money = replace(money,"\","")

			FormatW = FormatCurrency(money)
		end if
	End Function

	'//이미지 수정할떄 해당 이미지 파일 불러오기
	'//2008년 10월 16일 김선미
	'//Call update.setFileDel(getPhotoDelFileName("p_image1",tablename,"  p_idx = '"&requestQ("p_idx")&"'"))

	Function getPhotoDelFileName(byval sFeild,byval sTable,byval sWhere)
		Dim strDelFile
		If isValue(sFeild) and isValue(sTable) and isValue(sWhere) Then
			strDelFile = getAdoRsScalar("SELECT "&sFeild&" FROM "&sTable&" WHERE "&sWhere)
			If isValue(strDelFile) Then
				Call AdoConnExecute("update  "&sTable&" set "&sFeild&" = ''  WHERE  "&sWhere)
				getPhotoDelFileName = getFileNumName(strDelFile)
			Else
				getPhotoDelFileName = ""
			End If
		End If
	End Function

	'//이미지 사이즈에 맞추기
	Function getImageSizeBi(byval sFile,byval sWidth,byval sHeight)

		Dim arrImgSize,strListImgPrint



		If IsValue(sFile) and isImage(getFileNumName(sFile)) Then
			arrImgSize = getImgSize("/upload/"&getFileNumName(sFile), sWidth, sHeight)
			strListImgPrint = "<img src='/upload/" & getFileNumName(sFile) & "' width='" & arrImgSize(1) & "' height='" & arrImgSize(0) & "' align='middle'>"
		Else
			arrImgSize = getImgSize("/img/noimg.gif", sWidth, sHeight)
			strListImgPrint = "<img src='/img/noimg.gif' width='" & arrImgSize(1) & "' height='" & arrImgSize(0) & "' align='middle'>"
		End If

		getImageSizeBi = strListImgPrint

	End Function

	'//이미지 사이즈에 맞추기
	Function getImageSize1(byval sFile,byval sWidth,byval sHeight)

		Dim arrImgSize,strListImgPrint
		If IsValue(sFile) and isImage(getFileNumName(sFile)) Then
			arrImgSize = getImgSize("/upload/"&getFileNumName(sFile), sWidth, sHeight)
			strListImgPrint = "<img src='/upload/" & getFileNumName(sFile) & "' width='" & sWidth & "' height='" & sHeight & "' border='0' align='middle'>"
		Else
			arrImgSize = getImgSize("/img/noimg.gif", sWidth, sHeight)
			strListImgPrint = "<img src='/img/noimg.gif' width='" & arrImgSize(1) & "' height='" & arrImgSize(0) & "'  border='0' align='middle'>"
		End If

		getImageSize1 = strListImgPrint

	End Function


'// 다이렉트용 sms
Sub smssend(byval sphone,byval callback,byval sdate,byval smsg )
	Dim xmldoc,texts,result,arrPhone,intLoop,sphone1

	if trim(sphone) <> "" then

		arrPhone=split(sphone,"-")
'		alert(sphone)
		For intLoop = 0 to Ubound(arrPhone)
			sphone1 = sphone1&trim(arrPhone(intLoop))
		Next


		Set xmldoc = CreateObject("MSXML2.DOMDocument.4.0")
		xmldoc.async = false
		xmldoc.setProperty("ServerHTTPRequest")=True
		texts = "http://sms.direct.co.kr/link/send.php?" & _
				"stran_phone=" & trim(sphone1) & "&stran_callback=" & callback & _
				"&stran_date=" & sdate & "&stran_msg=" & server.URLEncode(smsg) & _
				"&guest_no=" & guest_no & "&guest_key=" & guest_key
	'	response.write texts

		xmldoc.load texts
		result = xmldoc.text
		set xmldoc = nothing
	end if
End Sub


sub smssend3(byval sphone,byval callback,byval sdate,byval smsg )
	dim mSoapClient,sSendResult
	'3-pod용
	if oBprint("str_smsUse") = "1" then

		  ' Soap 개체를 이용한 웹서비스 접속

		  Set mSoapClient = Server.CreateObject("MSSOAP.SoapClient30")

		  mSoapClient.ClientProperty("ServerHTTPRequest") = True

		  call mSoapClient.mssoapinit("http://websvc.nesolution.com/DpSms/DpSms.asmx?wsdl", "")

		  sSendResult = mSoapClient.SendSms (replace(sphone,"-",""), callback, sdate, smsg, oBprint("str_smsID"), oBprint("str_smsKey"))

		  Set mSoapClient = nothing

	end if

End sub


sub sms(byval sphone,byval callback,byval sdate,byval smsg )

				'받는사람 핸드폰 번호
				dim sTranPhone : sTranPhone = sphone

				'보내는사람 핸드폰 번호
				dim sTranCallback : sTranCallback = callback

				'예약전송 일시(생략시 즉시전송)
				dim sTranDate : sTranDate = sdate

				'전송 메시지
				dim sTranMsg : sTranMsg = smsg

				'계정번호
			'	sGuestNo = Request("txtGuestNo")
				'dim sGuestNo : sGuestNo = oBprint("str_smsID")

				'계정 인증키
				'dim sGuestAuthKey : sGuestAuthKey = oBprint("str_smsKey")

			  %>
              <iframe name="sms" src="/avanplus/sms_euckr.asp?sTranPhone=<%=escape(sTranPhone)%>&sTranCallback=<%=escape(sTranCallback)%>&sTranDate=<%=escape(sTranDate)%>&sTranMsg=<%=escape(sTranMsg)%>&skey=<%=Session.SessionID%>" frameborder="0" width="10" height="10"></iframe>
			  <%
			  

end sub
Function cutstr(input)

  Dim ret(), i, charat, wLen
  Dim rest()
  Dim result(1)

  ReDim ret(len(input))
  ReDim rest(len(input))

  for i = 1 to len(input)
    charat = mid(input, i, 1)

    if asc(charat) > 0 and asc(charat) < 255 then
      wLen = wLen + 1
    else
      wLen = wLen + 2
    end if

    if wLen > 80 then
      rest(i) = charat
    else
      ret(i) = charat
    end if
  next

  result(0) = join(ret, "")
  result(1) = join(rest, "")

  cutstr = result

end function

'//이미지 삭제할때 해당 이미지 파일 불러오기
	'//2007년 12월 7일 김선미
	'//Call update.setFileDel(getDelFileName("p_image1",tablename,"  p_idx = '"&requestQ("p_idx")&"'"))

	Function getDelFileName(byval sFeild,byval sTable,byval sWhere)
		Dim strDelFile
		If isValue(sFeild) and isValue(sTable) and isValue(sWhere) Then
			strDelFile = getAdoRsScalar("SELECT "&sFeild&" FROM "&sTable&" WHERE "&sWhere)
			If isValue(strDelFile) Then
				getDelFileName = getFileNumName(strDelFile)
			Else
				getDelFileName = ""
			End If
		End If
	End Function

	'//이미지 수정할떄 해당 이미지 파일 불러오기
	'//2008년 10월 16일 김선미
	'//Call update.setFileDel(getPhotoDelFileName("p_image1",tablename,"  p_idx = '"&requestQ("p_idx")&"'"))

	Function getPhotoDelFileName(byval sFeild,byval sTable,byval sWhere)
		Dim strDelFile
		If isValue(sFeild) and isValue(sTable) and isValue(sWhere) Then
			strDelFile = getAdoRsScalar("SELECT "&sFeild&" FROM "&sTable&" WHERE "&sWhere)
			If isValue(strDelFile) Then
				Call AdoConnExecute("update  "&sTable&" set "&sFeild&" = ''  WHERE  "&sWhere)
				getPhotoDelFileName = getFileNumName(strDelFile)
			Else
				getPhotoDelFileName = ""
			End If
		End If
	End Function

	'//전화번호 함수()
	'response.Write "021231234 -> "&phone2minus("021231234")&"<br>"
	'response.Write "08012341234 -> "&phone2minus("08012341234")&"<br>"
	'response.Write "0631231234 -> "&phone2minus("0631231234")&"<br>"
	'response.Write "01712341234 -> "&phone2minus("01712341234")&"<br>"
	'response.Write "0111231234 -> "&phone2minus("0111231234")&"<br>"
	'response.Write "15441234 -> "&phone2minus("15441234")&"<br>"

	function phone2minus(str)
		Dim phone(2)
		if len(str) > 8 and len(str) < 12 then
			if Right(Left(str,2),1) = 2 then	'//2번째숫자가
				phone(0) = Left(str,2)
			else								'//2가 아니면
				phone(0) = Left(str,3)
			end if
				phone(2) = Right(str, 4)
				phone(1) = Mid( str, len(phone(0))+1, len(str)-(len(phone(2))+len(phone(0))) )
			phone2minus = Join(phone,"-")
		elseif len(str) = 8 then
			phone2minus = Left(str,4)&"-"&Right(str,4)
		else
			phone2minus = str
		end if
	End function




Function userAgent()
	'익스플로러 버전 체크
	' 작성 : 2014-05-14 by 이용태
	Dim strUserAgent, IE
	strUserAgent = UCase(cstr(request.ServerVariables("HTTP_USER_AGENT")))

	'인터넷 익스플로러
	If InStr(strUserAgent, "MSIE 6.0") Then
		IE = 6
	ElseIf InStr(strUserAgent, "MSIE 6.1") Then
		IE = 6
	ElseIf InStr(strUserAgent, "MSIE 6.2") Then
		IE = 7
	ElseIf InStr(strUserAgent, "MSIE 7") Then
		IE = 7
	ElseIf InStr(strUserAgent, "MSIE 8") Then
		IE = 8
	ElseIf InStr(strUserAgent, "MSIE 9") Then
		IE = 9
	ElseIf InStr(strUserAgent, "MSIE 10") Then
		IE = 10
	Else '기타 (11 버전 이나 또는 크롬 등)
		IE = 99
	End If
	userAgent  = IE
End Function

Sub CkEditor_3_6(byval areaname, byval eValue)
	'에디터 버전  ckeditor 3.6 호출
	'작성 : 2014-05-14 by 이용태
	'/avanplus 디렉토리에 ck_editor 가 존재해야 정상 작동
	%><script type="text/javascript" src="/avanplus/editor/ck_editor/ckeditor.js"></script>
	<textarea class="ckeditor" cols="80" id="<%=areaname%>" name="<%=areaname%>" rows="10"><%=eValue%></textarea>
	<script>
	CKEDITOR.replace('<%=areaname%>',{
		'filebrowserUploadUrl':'/avanplus/editor/ck_editor/FileUpload.asp'
	});
	</script>
	<%
End Sub

Sub CkEditor_4_4(byval areaname, byval eValue)
	'에디터 버전  ckeditor 4.4 호출
	'작성 : 2014-05-14 by 이용태
	'/avanplus 디렉토리에 ckeditor 가 존재해야 정상 작동
	%><script type="text/javascript" src="/avanplus/editor/ckeditor/ckeditor.js"></script>
	<textarea class="ckeditor" id="<%=areaname%>" name="<%=areaname%>"><%=eValue%></textarea>
	<script>
	CKEDITOR.replace('<%=areaname%>',{
		'filebrowserUploadUrl':'/avanplus/editor/ck_editor/FileUpload.asp',
		toolbar: [
					{ name: 'styles', items: [ 'Styles', 'FontSize' ] },
					{ name: 'basicstyles', items: [ 'Table', '-', 'Image', '-', 'TextColor', 'BGColor', '-', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'Link', 'Unlink', '-', 'RemoveFormat'] },
					{ name: 'document', items: [ 'Source', '-', 'Save','-' , 'NewPage', 'Preview', '-', 'Undo', 'Redo', 'Maximize', 'ShowBlocks', '-', 'About'] },
				]
	});
	</script>
	<%
End Sub

Sub Editor(byval areaname, byval eValue)
	'위지윅에디터 호출
	'수정 : 2014-05-14 by 이용태
	'//사용예
	'Call setFCKeditor(areaname,eValue, "y", 400)
	'response.write userAgent()
	If userAgent() <= 9 Then ' 익스 10 미만 버전은 CKeditor3.6
		Call CkEditor_3_6(areaname,eValue)
	Else  '익스 11 이상 및 크롬등 기타 브라우저 버전은 CKeditor4.4
		Call CkEditor_4_4(areaname,eValue)
	End If
end Sub



'//빌더용 함수===============================================================


	Function FilePathTF(byval FilePath)
	'==============================================
	'	해당경로에 파일이 있는지체크 있으면 true반환
	'==============================================
		FilePathTF = false
		dim objScrFso

		Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
			FilePathTF = objScrFso.FileExists(Server.MapPath(FilePath))
		Set objScrFso = Nothing
	End Function

	Sub ExeFile(byval FilePath)
	'==============================================
	'	파일 경로 확인후 execute
	'==============================================
		FilePath = "/"&FilePath
		FilePath = replace(FilePath,"//","/")
		dim objScrFso
		Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
			IF objScrFso.FileExists(Server.MapPath(FilePath)) then
				Server.Execute(FilePath)
			Else
				response.Write("[Err ExeFile] '"&FilePath&"' 파일경로가 정확하지 않습니다.")
			End if
		Set objScrFso = Nothing
	End Sub


	Function ModuleModeSet(byval moduleURL, byval moduleID, byval startMode ,byval validMode)
	'==============================================
	'	모듈의 처음시작지정및 실행가능한 모드지정
	' 	실행시 실행파일 경로를 리턴함.
	'==============================================
		Dim Pmode, Ppath, Rmode
			'//처음 시작 모드
			'startMode = "list"
			'//이 페이지 호출시 처리 가능한 페이지, 없으면 모는 페이지에 대해 처리
			'validMode = ""

		Rmode = lcase(RequestQ(moduleID&"Mode"))
		Pmode = Rmode

		'// 페이지모드값이 없는경우 시작모드
		If Len(Pmode) = 0 Then	Pmode = startMode


		'// 처리시 정해진 파일이 아니면 현재모드리턴 -->문자열 있는지 확인
		'// 관리자는 list 사용자는 list_user를 쓰기에 문자열이 있는지만 검사
		If len(validMode) > 0 and InStr(lcase(validMode),lcase(Rmode)) = 0 then
			Pmode = startMode
		End if


		'// 실행할 전체 경로
		Ppath = "/"&moduleURL& "/" & Pmode & ".asp"
		Ppath = replace(Ppath,"//","/")


		response.Write("<!--'"&moduleURL&"' 실행-->")
		dim objScrFso
		Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
			IF objScrFso.FileExists(Server.MapPath(Ppath)) then
				Server.Execute(Ppath)
			Else
				response.Write("[Err ExeFile] '"&Ppath&"' 파일경로가 정확하지 않습니다.")
			End if
		Set objScrFso = Nothing
		response.Write("<!--'"&moduleURL&"' 종료-->")
		ModuleModeSet = Ppath
	End Function



	Sub codeMake_arrData(Byval strDim)
	'//=========================================
	'	config arr받기 귀찮아서 만든거.
	'//=========================================
		dim i : i = 0
		dim arrDim : arrDim = split(strDim,",")
		for i = 0 To (ubound(arrDim)) Step 1
			'//변수받기
			response.Write("<br>"&trim(arrDim(i))&"= arrData("&i&",0)")
		next
		response.Write("<br><br>")
	End Sub


	Sub urlNoEXE(byval url)	'//해달 url직접실행 금지 '//보안때문에
	'//URL문자열이 들어가 있으면 실행안되게 처리 "/avanlus/modul/_basic" 이면 해당 경로에서 처리안됨.

		if instr(lcase(request.ServerVariables("URL")),lcase(url)) > 0 then
			response.Write("보안상 직접경로는 실행 할 수 없습니다.")
			response.End()
		end if

	End Sub

	'//이미지 저장시 코드번호 만들기
	Function getimgName(byval gubun, byval code)
		if gubun = "visual"			then getimgName = gubun & "_" & left(code,4)
		if gubun = "visual2"		then getimgName = gubun & "_" & left(code,4)
		if gubun = "visualbg"		then getimgName = gubun & "_" & left(code,4)

		if gubun = "title"			then getimgName = gubun & "_" & left(code,6)
		if gubun = "menu"			then getimgName = gubun & "_" & left(code,6)
		if gubun = "menu_over"		then getimgName = gubun & "_" & left(code,6)

		if gubun = "menu2"			then getimgName = gubun & "_" & left(code,8)
		if gubun = "menu2_over"		then getimgName = gubun & "_" & left(code,8)
	End Function


	Function oBprint(byval colum)
	'==============================================
	'	빌더세팅에 관한 스칼라 값 반환 - 가공전 데이터
	'==============================================
		oBprint = getAdoRsScalar("select strValue from Builder_set where strFname = '"&colum&"'")

	End Function

	Function Bprint(byval colum)
	'==============================================
	'	빌더세팅에 관한 스칼라 값 반환
	'==============================================
		dim arr
		arr = getAdoRsArray("select strType, strValue from Builder_set where strUname = '"&colum&"'")

		If isArray(arr) Then
			'이미지나 플래시인 경우 처리가 힘들꺼 같음 / 그래서 주석처리
			if arr(0,0) = "img_" then	'img_, str_, txt_
				response.Write "<img src=/upload/"&getFileNumName(arr(1,0))&">"
			else
				response.Write arr(1,0)
			end if
		end if
	End Function

	Function AduMemberCheck(ByVal id, ByVal tbl, ByVal code)
		Dim strSql, colum, rs_adu, today_date, code_L, code_R, code_T, rs

		today_date = Date
		code_L	   = Left(code,1)

		'종합반 강의시청을 했는지 체크
		Set rs = dbconn.execute("select * from "&tbl&" where s_userId='"&id&"' and s_num='"&code_L&"100'")

		If rs.eof Or rs.bof Then
			code_T = "s_num='"&code&"'"
		Else
			'response.write rs("s_num")
			code_T = "s_num='"&code_L&"001' or s_num='"&code_L&"002' or s_num='"&code_L&"003'"
		End If

		colum = "s_userId='"&id&"' and "&code_T&" and s_StartDate <= '"& today_date &"' and s_EndDate >= '"&today_date&"'"
		strSql = "select * from "&tbl&" where "&colum&" and s_state=1"
		'response.write strSql&"<br>"
		Set rs_adu = dbconn.execute(strSql)

		if rs_adu.eof Then
			AduMemberCheck = "N"
		Else
			AduMemberCheck = "Y"
		end if
	End Function

		'## ASP
	'2014-04-08 by  이용태
function SQL_Injection( get_String )

	'response.write "SQL_Injection : get_String : " & get_String & "<br>"
	get_String = REPLACE( get_String, "'", "''" )
	get_String = REPLACE( get_String, ";", "" )
	get_String = REPLACE( get_String, "--", "" )
	get_String = REPLACE( get_String, "select", "select_", 1, -1, 1 )
	get_String = REPLACE( get_String, "insert", "insert_", 1, -1, 1 )
	get_String = REPLACE( get_String, "update", "update_", 1, -1, 1 )
	get_String = REPLACE( get_String, "delete", "delete_", 1, -1, 1 )
	get_String = REPLACE( get_String, "drop", "drop_", 1, -1, 1 )
	get_String = REPLACE( get_String, "union", "union_", 1, -1, 1 )
	get_String = REPLACE( get_String, "and", "and_", 1, -1, 1 )
	'get_String = REPLACE( get_String, "or", "", 1, -1, 1 )
	get_String = REPLACE( get_String, "1=1", "", 1, -1, 1 )
	get_String = REPLACE( get_String, "sp_", "", 1, -1, 1 )
	get_String = REPLACE( get_String, "xp_", "", 1, -1, 1 )
	get_String = REPLACE( get_String, "@variable", "", 1, -1, 1 )
	get_String = REPLACE( get_String, "@@variable", "", 1, -1, 1 )
	get_String = REPLACE( get_String, "exec", "exec_", 1, -1, 1 )
	get_String = REPLACE( get_String, "sysobject", "", 1, -1, 1 )

	SQL_Injection = get_String

end function



Function random()
  Dim str, strlen, r, i, ds, serialCode '사용되는 변수를 선언

  str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" '랜덤으로 사용될 문자 또는 숫자

   strlen = 10 '랜덤으로 출력될 값의 자릿수 ex)해당 구문에서 10자리의 랜덤 값 출력

   Randomize '랜덤 초기화
   For i = 1 To strlen '위에 선언된 strlen만큼 랜덤 코드 생성
    r = Int((36 - 1 + 1) * Rnd + 1)  ' 36은 str의 문자갯수
    serialCode = serialCode + Mid(str,r,1)
   Next
   random = serialCode
 End Function




Function returnIH(byval aaa)

		dim ih

			ih = aaa
			if ih = "10" then
				ih = "대기"
			elseif ih = "20" then
				ih = "입찰중"
			elseif ih = "30" then
				ih = "입찰중"
			elseif ih = "40" then
				ih = "입찰마감"
			elseif ih = "50" then
				ih = "선정중"
			elseif ih = "60" then
				ih = "낙찰완료"
			elseif ih = "99" then
				ih = "유찰"
			end if
		returnIH = ih

end Function

Sub FileDownLink_upPath2(DBFileData, byval uppath)
	dim linkurl
	if len(DBFileData) > 0 then
		if Ubound(split(DBFileData,":")) > 0 then
			Response.Write "<a href=/AVANplus/filedownload.asp?o_file="&split(DBFileData,":")(0)&"&uppath="&uppath&"&u_file="&split(DBFileData,":")(1)&">"
			Response.Write "<img src=/setImage/icon_file.gif>"&"</a>"
		else
			Response.Write "<a href=/AVANplus/filedownload.asp?o_file="&DBFileData&"&uppath="&uppath&"&u_file="&DBFileData&">"
			Response.Write "파일다운</a>"
		end if
	end if
End Sub

%>
