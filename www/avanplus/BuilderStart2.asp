<%
	option explicit
	%>
<!-- #include file="./inc/incCharset.asp" -->
	<!--#include virtual = "/AVANplus/_Function.asp"-->
	<!--#include file = "CallModule.asp"--><%


	call dbopen

	%><!--#include file = "modul/banner/inc_function.asp"--><%

	'dim siteid
	'siteid = Bprint("strsiteid")


	'if len(siteid) = 0 then
	'	response.Write("사이트 아이디가 지정되지 않았습니다.")
	'	response.End()
	'end if


	dim tablename
	tablename = "BuilderCate"


	call builderData()




	'##########################################
	'//사용함수
	'##########################################
	'// 카테고리 타입확인
	if len(request.QueryString("avan")) <> 9 and len(request.QueryString("avan")) <> 10 and len(request.QueryString("avan")) <> 0 then
		response.Write("카테고리 타입이 맞지 않습니다.")
		response.End()
	end if

	'dim catecode : catecode = "8854000000"

	'dim step
	'step = 4

	'Dim stepMax, stepMin
	'stepMax =  10^((5-step)*2)
	'stepMin =  stepMax - 1

	'dim step999Str, step000Str
	'step999Str = Cstr(stepMin)
	'step000Str = replace(stepMin,"9","0")
	Function CateCode()
		dim HOMEPCATE
		HOMEPCATE = request.QueryString("avan")
		if len(HOMEPCATE) = 0 then
			CateCode = 1000000000
		else
			CateCode = HOMEPCATE
		end if
	End Function


	'// 현제 카테고리의 스텝을 반환
	Function getCateStep(byval HOMEPCATE)
		dim CateStep
		CateStep = Cdbl(HOMEPCATE)/10000000000
		CateStep = replace(CateStep,".","")
		CateStep = int((Len(CateStep))/2)
		'if CateStep = 0 then CateStep = 1
		if CateStep > 5 then CateStep = 5
		getCateStep = CateStep

	End Function

	'// 해당 코드값의 초대/최소등 원하는 값 리턴
	Function getCateValuse(byval HOMEPCATE, byval str)
		dim CateAdd
		CateAdd = 10^(2*(4-getCateStep(HOMEPCATE)))
		if int(HOMEPCATE) = 0 then CateAdd = 100000000

		dim CateMin
		CateMin = HOMEPCATE

		dim CateMax
		CateMax = HOMEPCATE + ( (CateAdd*100) - 1 )
		if getCateStep(HOMEPCATE) = 0 then CateMax = 9999999999


		if Lcase(str) = "add" then
			getCateValuse = CateAdd
		elseif Lcase(str) = "max" then
			getCateValuse = CateMax
		elseif Lcase(str) = "min" then
			getCateValuse = CateMin
		end if
	End Function

	'// 현재 카테고리의 스텝
	'response.Write("스텝:"&getCateStep(HOMEPCATE)&"<br>")
	'// 현재 카테고리의 추가값(인서트시 플러스 값)
	'response.Write("추가값:"&getCateValuse(HOMEPCATE,"add")&"<br>")
	'// 현재 카테로기 최소값(카테고리 자신)
	'response.Write("최소값:"&getCateValuse(HOMEPCATE,"min")&"<br>")
	'// 현재 카테고리 최대값
	'response.Write("최대값:"&getCateValuse(HOMEPCATE,"max")&"<br>")



	'//넘어온 카테고리의 각 스텝의 코드값리턴
	Function upCodeReturn(byval step, byval HOMEPCATE)
		if len(HOMEPCATE) = 0 then
			upCodeReturn = "0000000000"
		elseif HOMEPCATE = 0 then
			upCodeReturn = "0000000000"
		else
			upCodeReturn = int( HOMEPCATE/(10^((5-step)*2)) ) * (10^((5-step)*2))
		end if
	End Function

	'response.Write("upCodeReturn:"&upCodeReturn(1,HOMEPCATE)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(2,HOMEPCATE)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(3,HOMEPCATE)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(4,HOMEPCATE)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(5,catecode)&"<br>")


	Function CateNextWhere()	'//넘어온 카테고리값의 하위카테고리 생성시 처음서 끝까지
		'// 1122000000 이 넘어오면
		'// 1122       앞부분인 와
		'//       0000 뒷부분이 인 값 알아오기
		dim strF, strR
		dim strS, strE

		strF	= mid(CateCode(),1,getCateStep(catecode)*2)
		if len(strF)>0 then strF = int(strF)

		strR	= 10^((4-getCateStep(catecode))*2)
		strR	= replace(Cstr(strR),"1","")
		strR	= replace(Cstr(strR),"0.0","")

		CateNextWhere = " c_code like '"& strF &"%' and c_code like '%"& strR &"' and c_code > "& upCodeReturn(getCateStep(catecode),catecode) &" "
	End Function



	Function CateThisWhere(byval num, byval HOMEPCATE)	'//해당스텝의 카테고리 목록 가져오기 쿼리
		'// 1122000000 이 넘어오면
		'// 1122       앞부분인 와
		'//       0000 뒷부분이 인 값 알아오기

		'//	 122000000 이면
		'//  122
		'//       0000 값 알아오기

		dim strF, strR
		dim strS, strE
		dim strnum
		'// 카테고리 맨 앞자리가 2자리수 미만인 경우의 처리

		if len(HOMEPCATE) = 0 then
			strnum = 0	'//카테고리 값이 0인경우
		elseif int(HOMEPCATE) = 0 then
			strnum = 0	'//카테고리 값이 0인경우
		elseif int(HOMEPCATE) < 1000000000 then
			strnum = (num*2)-1
			if strnum < 0 then strnum = 0 '//계산식이 -1인경우는 0으로 대체
			'response.Write(strnum)
		else
			strnum = num*2
		end if

		strF	= left(HOMEPCATE,strnum)

		if len(strF)>0 then strF = int(strF)

		strR	= 10^((4-num)*2)
		strR	= replace(Cstr(strR),"1","")
		strR	= replace(Cstr(strR),"0.0","")

		CateThisWhere = " c_code like '"& strF &"%' and c_code like '%"& strR &"' and c_code > "& upCodeReturn(num,HOMEPCATE) &" "
	End Function



	'Function CateAllWhere()	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식
		'CateAllWhere = " Goods_cate_join.c_code >= "& int(getCateValuse(catecode,"min")) & " and Goods_cate_join.c_code <= "& int(getCateValuse(catecode,"max"))
	'End Function

	Function CateAllWhere(byval HOMEPCATE)	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식 '//2개 테이블
		CateAllWhere = " Goods_cate_join.c_code >= "& int(getCateValuse(HOMEPCATE,"min")) & " and Goods_cate_join.c_code <= "& int(getCateValuse(HOMEPCATE,"max"))
	End Function

	Function CateAllWhere1(byval HOMEPCATE)	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식	'//1개 테이블
		CateAllWhere1 = " c_code >= "& int(getCateValuse(HOMEPCATE,"min")) & " and c_code <= "& int(getCateValuse(HOMEPCATE,"max"))
	End Function



	'// 스트링 값을 토대로 카테고리 리스트 출력
	'// > 컴퓨터 > 자동차 > 부품
	Function getCateColPrint(byval HOMEPCATE)
		dim TempcateStep, i, sql_func, rs_func
		TempcateStep = getCateStep(HOMEPCATE)
		'response.Write getCateStep(HOMEPCATE)
		if getCateStep(HOMEPCATE) > 5 then TempcateStep = 5
		i = 0
		for i = 0 to TempcateStep

			sql_func = "select * from "& tablename &" where c_code = '"& upCodeReturn(i, HOMEPCATE ) &"'"
				'response.Write(sql_func)
			set rs_func = dbconn.execute(sql_func)

				if rs_func.eof then

				else
					do while not rs_func.eof
						getCateColPrint = getCateColPrint & ">"&rs_func("c_name")
					rs_func.movenext
					loop
				end if

		next
	End Function













'// 셀렉트 리스트 박스로 이어지는 카테고리 선택기
sub navi_select(byval HOMEPCATE)

	dim i, sql_func, rs_func, temp_selected

%>
<script type="text/JavaScript">
		<!--
			function MM_jumpMenu(targ,selObj,restore){ //v3.0
			  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
			  if (restore) selObj.selectedIndex=0;
		}
		//-->
	</script>

	<table width="100" border="0">
		<tr>
<%
		dim TempcateStep
		TempcateStep = getCateStep(HOMEPCATE)

		if len(HOMEPCATE) = 0 then TempcateStep = 0 '//카테고리 값이 없는경우 스텝 0

		if getCateStep(HOMEPCATE) > 4 then TempcateStep = 4 '// 4이상인 경우 무시 4단계때 5단계를 생성하기 때문
		i = 0
		for i = 0 to TempcateStep
			sql_func = "select * from "& tablename &" where "& CateThisWhere(i,request.QueryString("avan")) &" order by c_sunse asc "
				'response.Write(sql_func)
			set rs_func = dbconn.execute(sql_func)
%>


			<td width="100">
				<table width="100" border="0" cellspacing="0" cellpadding="0">
					<form name="getCateSelect" id="getCateSelect">
						<tr>
							<td>

							<%
							if rs_func.eof then
							else
							%>
								<select name="getCateSelectMenu" onChange="MM_jumpMenu('parent',this,0)" style="width:100">
									<option value="?<%=getString("")%>"><%=i+1%>차 카테고리 선택</option>
									<%
									do while not rs_func.eof

									'// 각 단계의 값을 리턴(넘어온 카테코드와 디비의 코드가 같으면 셀렉티드)
									temp_selected = chk_selected( upCodeReturn(i+1, HOMEPCATE ), upCodeReturn(i+1, rs_func("c_code") ) )
									%>
										<option value="?<%=getString("HOMEPCATE="&rs_func("c_code"))%>" <%=temp_selected%>><%=rs_func("c_name")%></option>
									<%
									rs_func.movenext
									loop
									%>
								</select>

							<%
							end if
							%>
							</td>
						</tr>
					</form>
				</table>
			</td>

		<%
		next
		%>
		</tr>
	</table>
<% End sub %>





<%

	Sub AUTOcatePage()
		'// 해당 카테고리 페이지에 맞는 페이지로 이동

		dim no, tempName
		no = getCateStep(cateCode)
		'response.write(no&"<<<<<<<<<<<<<")

		tempName = request.ServerVariables("PATH_INFO")


		if  no = 0 then
			'response.Write "카테고리가 없습니다."
		elseif no = 1 and ( lcase(tempName) <> lcase(url_step1) ) then
			response.Redirect(url_step1&"?"&getString(""))
		elseif no = 2 and ( lcase(tempName) <> lcase(url_step2) ) then
			response.Redirect(url_step2&"?"&getString(""))
		elseif no = 3 and ( lcase(tempName) <> lcase(url_step3) ) then
			response.Redirect(url_step3&"?"&getString(""))
		elseif no = 4 and ( lcase(tempName) <> lcase(url_step4) ) then
			response.Redirect(url_step4&"?"&getString(""))
		elseif no = 5 and ( lcase(tempName) <> lcase(url_step5) ) then
			response.Redirect(url_step5&"?"&getString(""))
		end if

	End Sub


%>




<%
Sub navi_2_3stpe(byval step, byval viewOption)
	'//step 카테고리 스텝이 어디부터 인지? ex. 1 인경우는 1은 세로, 2는 가로에 출력
	'//viewOption	카테고리 보기 세로가 1단인지 전체인지 // 갑이 1이면 1단 0 이면 모두

	'// 카테고리 코드 확인
	if len(request.QueryString("avan")) = 0 then
		response.Write("카테고리 값이 없습니다.")
		response.end
	elseif int(request.QueryString("avan")) = 0 then
		response.Write("카테고리가 0입니다. 정상적인 카테고리 값을 보내세요.")
		response.end
	end if



	dim sql, rs, i, top, sql2, rs2
	i = ( step - 1 )
		'for i = 0 to TempcateStep
		if viewOption = 1 then top = " top 1 "
		sql = "select "& top &" * from "& tablename &" where "& CateThisWhere(i,request.QueryString("avan")) &" order by c_sunse asc "
			response.Write(sql)
		set rs = dbconn.execute(sql)
%>
<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#CCCCCC">
<%
	if not rs.eof then
		Do while not rs.eof
%>
  <tr>
    <td width="120" bgcolor="#F2F2F2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5"><img src="cate/shopimg/s_arrow.gif" width="3" height="5"></td>
          <td><a href="cate/?<%=getString("HOMEPCATE="&rs("c_code"))%>"><b><%=rs("c_name")%>[<%=getGoodsCount(rs("c_code"))%>]</b></a><b></b></td>
        </tr>
      </table></td>
    <td bgcolor="#FFFFFF">
		<%
			sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,rs("c_code")) &" order by c_sunse  "
				set rs2 = dbconn.execute(sql2)
				'response.Write(rs("c_code"))
				response.Write(" <font color=#D9D9D9>|</font> ")
					if not rs2.eof then
						Do while not rs2.eof

							response.Write("<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">" & rs2("c_name") &"["& getGoodsCount(rs2("c_code")) &"]"& "</a> <font color=#D9D9D9>|</font> ")

						rs2.movenext
						loop
					end if
				rs2.close
				set rs2 = nothing
		%>
	</td>
  </tr>
<%
		rs.movenext
		loop
	end if

	rs.close
	set rs = nothing

%>
</table>
<% End sub %>














<%
Sub navi_row2(byval HOMEPCATE)
	'// 상위 줄 출력
	'// 홈 > 두번째 > 세번째
%>

<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#F2F2F2"><%
		dim TempcateStep, i, sql, rs
		TempcateStep = getCateStep(HOMEPCATE)
		'response.Write getCateStep(HOMEPCATE)
		if getCateStep(HOMEPCATE) > 5 then TempcateStep = 5
		response.Write("<a href=?><b>홈</b></a> ")

		i = 0
		for i = 0 to TempcateStep

			sql = "select * from "& tablename &" where c_code = '"& upCodeReturn(i, HOMEPCATE ) &"'"

			set rs = dbconn.execute(sql)

				if rs.eof then

				else
					do while not rs.eof
						response.Write" > <a href=?"& getString("HOMEPCATE="&rs("c_code")) & "><b>" & rs("c_name") & "</b></a> "
						'response.Write(">"&rs("c_name"))
					rs.movenext
					loop
				end if

		next

	%></td>
  </tr>

<%
	'// 상단 카테고리 이하 메뉴출력


	dim sql2, rs2, rs3
	i = getCateStep(CateCode()) - 1
	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &" order by c_sunse  "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else
			%>
  <tr>
    <td height="120" bgcolor="#FFFFFF">
			<%
				Do while not rs2.eof

					response.Write "<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">" & rs2("c_name")

					'// 하위 상품 갯수 나타내는 식
					response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝

					response.Write "</a> <font color=#D9D9D9>|</font> "

				rs2.movenext
				loop
			%>


	</td>
  </tr>
			<%
			end if
		rs2.close
		set rs2 = nothing
%>


</table>
<%
End Sub








	'// 한줄 카테고리 이하 메뉴출력
	'| 하하하 | 호호호 | 히히히 | 케케케| <-이런 형식으로 출력
sub colNavi(byval cateStep)
	dim sql2, rs2, rs3, i, bb, bb2

	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if

	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &" and c_use = '1' order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else

				Do while not rs2.eof

					bb = ""
					bb2= ""
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb = "<b>"
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb2 = "</b>"


					'if int(CateCode()) = int(rs2("c_code")) then bb = "<b>"
					'if int(CateCode()) = int(rs2("c_code")) then bb2 = "</b>"

					'response.Write "<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">"& bb & rs2("c_name")
					if cateStep = "1" then
						response.Write "<a href=main.asp?avan="&rs2("c_code") & ">"& bb & rs2("c_name")
					else
						response.Write "<a href=sub.asp?avan="&rs2("c_code") & ">"& bb & rs2("c_name")
					end if

					'// 하위 상품 갯수 나타내는 식
					'response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝
					response.Write bb2 & "</a> <font color=#D9D9D9>|</font> "

				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub









	'// 한줄 카테고리 이하 메뉴출력
	'111
	'222
	'333
	'444
	'이런식으로 세로 출력
sub rowNavi(byval cateStep)
	dim sql2, rs2, rs3, i, bb, bb2

	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if

	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &" and c_use = '1' order by c_sunse  "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		'response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else

				Do while not rs2.eof

					bb = ""
					bb2= ""


					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb = "<b>"
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb2 = "</b>"

					'response.Write "<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">"& bb & rs2("c_name")
					'response.Write "<a href=?avan="&rs2("c_code") & ">"&

					if ucase(rs2("content_mode")) = "L" then
					'//컨텐츠 새창 띄우기 모드이면
						response.Write	"<a href="&rs2("content_link") & " target=_blank >"
					else
						response.Write	"<a href=?avan="&rs2("c_code") & ">"
					end if

					response.Write bb & rs2("c_name") & bb2 & "</a><br>"' <font color=#D9D9D9>|</font> "

				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub








	'// 한줄 카테고리 이하 메뉴출력
	'111
	'222
	'333
	'444
	'이런식으로 세로 출력
sub rowNavi_sub2(byval cateStep)
	dim sql2, rs2, rs3, i, bb, bb2

	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if

	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &" and c_use = '1' order by c_sunse  "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		'response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else

				Do while not rs2.eof

					bb = ""
					bb2= ""


					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb = "<b>"
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb2 = "</b>"

					'response.Write "<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">"& bb & rs2("c_name")
					response.Write "<a href=?avan="&rs2("c_code") & ">"& bb & rs2("c_name")
					'// 하위 상품 갯수 나타내는 식
					'response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝
					response.Write bb2 & "</a><br>"' <font color=#D9D9D9>|</font> "

					'// 이하 메뉴가 있으면
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then
						response.Write "<table width='170'><tr><td width='20'></td><td>"
						call rowNavi(cateStep + 1)
						response.Write "</td width='150'></tr></table>"
					end if

				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub




	'// 해당 카테고리 이름출력

sub ThisNavi(byval step, byval HOMEPCATE)
	dim sql2, rs2, rs3, i, bb, bb2


	sql2 = "select * from "& tablename &" where c_code = "& upCodeReturn(step,HOMEPCATE) &" order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
			if rs2.eof then

			else

				Do while not rs2.eof

					if getCateStep(HOMEPCATE) >= step then response.Write rs2("c_name")

					'// 하위 상품 갯수 나타내는 식
					'response.Write getGoodsCount(rs2("c_code"))
					'//-- 하위 갯수 끝

					'response.Write bb2 & "</a> <font color=#D9D9D9>|</font> "

				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub



	'//***************************************
	'// 카테고리 상단 이미지 처리
	'//***************************************

	Sub cateVisualimg(byval DBimg)

		dim filepath, fileExe
		filepath = getFileNumName(DBimg)
		'fileExe
		if len(filepath) > 0 then '//파일명 있으면

			if ubound(split(filepath,".")) > 0 then
				fileExe = split(filepath,".")(1)

				'// 파일 확장자로 해당 파일 처리
				if lcase(fileExe) = "gif" or lcase(fileExe) = "jpg" or lcase(fileExe) = "png" then
					response.Write "<img src=/setImage/"& filepath &">"
				elseif lcase(fileExe) = "swf" then
					%>
					<script language="javascript" src = "/shop/upswf.js"--></script>
					<script language="javascript">FlashMainbody('<%=filepath%>',282,598);</script>
					<%
				end if

			end if
		else
			response.Write "<img src=/setImage/ alt='No image'>"
		end if

	End Sub


Function MainImage()
	dim step
	step = getCateStep(CateCode())

	if step = 1 then
		cateVisualimg(c_img)
	end if

End Function

Function loginImage()'MainImage2()
	dim step
	step = getCateStep(CateCode())

	if step = 1 then
		cateVisualimg(menuimg)
	end if

End Function

Function MainImagebg()

	dim step
	step = getCateStep(CateCode())

	if step = 1 then
		'cateVisualimg(menuimg_over)
		'// 백이미지인경우 경로값만 리턴 "예) 000000.asp "
		response.Write "/setImage/"& getFileNumName(menuimg_over)
	end if

End Function


Function SubImage()
	dim step
	step = getCateStep(CateCode())
	if step >= 2 then
		cateVisualimg(getAdoRsScalar("select c_img from "& tablename &" where c_code = '"& upCodeReturn(2,CateCode()) &"'"))
	end if

End Function

Function SubImage2()
	dim step
	step = getCateStep(CateCode())
	if step >= 2 then
		cateVisualimg(getAdoRsScalar("select menuimg from "& tablename &" where c_code = '"& upCodeReturn(2,CateCode()) &"'"))
	end if

End Function

Function SubTitleImage()
	SubImage2()
End Function

Function SubImagebg()
	dim step
	step = getCateStep(CateCode())
	if step >= 2 then
		'cateVisualimg(getAdoRsScalar("select menuimg_over from "& tablename &" where c_code = '"& upCodeReturn(2,CateCode()) &"'"))
		response.Write "/setImage/"& getFileNumName(getAdoRsScalar("select menuimg_over from "& tablename &" where c_code = '"& upCodeReturn(2,CateCode()) &"'"))
	end if
	'response.Write getFileNumName(getAdoRsScalar("select menuimg_over from "& tablename &" where c_code = '"& upCodeReturn(2,CateCode()) &"'"))
End Function

Function SubTitle()
	dim step
	step = getCateStep(CateCode())

	if step > 2 then
		cateVisualimg(getAdoRsScalar("select c_img from "& tablename &" where c_code = '"& upCodeReturn(3,CateCode()) &"'"))

	end if
End Function

Function SubTitleText()
	dim sql2, rs2, rs3, i, bb, bb2


	sql2 = "select * from "& tablename &" where c_code = "& upCodeReturn(3,request.QueryString("avan")) &" order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
			if rs2.eof then
			else

				Do while not rs2.eof
					if getCateStep(request.QueryString("avan")) >= 3 then response.Write rs2("c_name")
				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing
End Function


Function MenuPosition()
		dim TempcateStep, i, sql_func, rs_func, menuPrint, c_code, step
		TempcateStep = getCateStep(request.QueryString("avan"))
		'response.Write getCateStep(HOMEPCATE)
		if getCateStep(request.QueryString("avan")) > 5 then TempcateStep = 5
		i = 0
		menuPrint = ">"
		for i = 0 to TempcateStep

			c_code = int( request.QueryString("avan")/(10^((5-i)*2)) ) * (10^((5-i)*2) )

			sql_func = "select c_name from "& tablename &" where c_code = '"& c_code &"'"

			set rs_func = dbconn.execute(sql_func)
				'response.Write(sql_func&rs_func("c_name"))

				if rs_func.eof then

				else
					do while not rs_func.eof
						menuPrint = menuPrint & ">"&rs_func("c_name")
					rs_func.movenext
					loop
				end if

		next

		menuPrint = replace(menuPrint,">>","")
		response.Write menuPrint
End Function

Function MenuPositionBoot() '부트스트랩용
		dim TempcateStep, i, sql_func, rs_func, menuPrint, c_code, step
		TempcateStep = getCateStep(request.QueryString("avan"))
		'response.Write getCateStep(HOMEPCATE)
		if getCateStep(request.QueryString("avan")) > 5 then TempcateStep = 5
		i = 0
		menuPrint = ">"
		Response.write "<ul class='pull-right breadcrumb'>"
		for i = 0 to TempcateStep

			c_code = int( request.QueryString("avan")/(10^((5-i)*2)) ) * (10^((5-i)*2) )

			sql_func = "select c_name from "& tablename &" where c_code = '"& c_code &"'"

			set rs_func = dbconn.execute(sql_func)
				'response.Write(sql_func&rs_func("c_name"))

				if rs_func.eof then

				else
					do while not rs_func.eof
						'menuPrint = menuPrint & ">"&rs_func("c_name")
						Response.Write " <li>"&rs_func("c_name")&"</li>"
					rs_func.movenext
					loop
				end if

		next

		'menuPrint = replace(menuPrint,">>","")
		'response.Write menuPrint
		Response.Write "</ul>"
End Function


Function submenu()

	dim sql2, rs2, rs3, i, menuname, overmenu, cateStep, linkcode
	dim sql4, rs4
	cateStep = 3

	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if

	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &" and c_use = '1' order by c_sunse  "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		response.Write("<table border=0 cellspacing=0 cellpadding=0>")
			if rs2.eof then

			else

				Do while not rs2.eof

					overmenu = rs2("menuimg_over")
					if len(trim(overmenu)) = 0 or isnull(overmenu) then overmenu = rs2("menuimg")	'//over이미지가 없으면 첫이미지로 처리
					'response.Write len(trim(overmenu))
					menuname = rs2("menuimg")	'// 현재 클린된 페이지 활성화
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then menuname = overmenu


					if ucase(rs2("content_mode")) = "L" then
					'//컨텐츠 새창 띄우기 모드이면
						%>
						<tr>
							<td><a href="<%=rs2("content_link")%>" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('<%=rs2("c_code")%>','','/setImage/<%=overmenu%>',1)" onFocus="blur()"><img src="/setImage/<%=menuname%>" name="<%=rs2("c_code")%>" border="0" alt="<%=rs2("c_name")%>"></a></td>
						</tr>
						<%
					else
						%>
						<tr>
							<td><a href="?avan=<%=rs2("c_code")%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('<%=rs2("c_code")%>','','/setImage/<%=overmenu%>',1)" onFocus="blur()"><img src="/setImage/<%=menuname%>" name="<%=rs2("c_code")%>" border="0" alt="<%=rs2("c_name")%>"></a></td>
						</tr>
						<%
					end if

					'//==== 이하 메뉴가 있으면 4뎁스메뉴
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then


							'call rowNavi(cateStep + 1)
							'call subsubmenu()

						'해당 서브메뉴 쿼리식
						sql4 = "select * from "& tablename &" where "& CateThisWhere(i+2,CateCode()) &" and c_use = '1' order by c_sunse "
						set rs4 = dbconn.execute(sql4)

						if not rs4.eof then

							Do while not rs4.eof

								overmenu = rs4("menuimg_over")
								if len(trim(overmenu)) = 0 or isnull(overmenu) then overmenu = rs4("menuimg")	'//over이미지가 없으면 첫이미지로 처리
								menuname = rs4("menuimg")	'// 현재 클린된 페이지 활성화
								if upCodeReturn(cateStep+1,CateCode) = upCodeReturn(cateStep+1,rs4("c_code")) then menuname = overmenu

								if ucase(rs4("content_mode")) = "L" then
									'//컨텐츠 새창 띄우기 모드이면
									%>
									<tr>
										<td><a href="<%=rs4("content_link")%>" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('<%=rs4("c_code")%>','','/setImage/<%=overmenu%>',1)" onFocus="blur()"><img src="/setImage/<%=menuname%>" name="<%=rs4("c_code")%>" border="0" alt="<%=rs4("c_name")%>"></a></td>
									</tr>
									<%
								else
									%>
									<tr>
										<td><a href="?avan=<%=rs4("c_code")%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('<%=rs4("c_code")%>','','/setImage/<%=overmenu%>',1)" onFocus="blur()"><img src="/setImage/<%=menuname%>" name="<%=rs4("c_code")%>" border="0" alt="<%=rs4("c_name")%>"></a></td>
									</tr>
									<%
								end if

							rs4.movenext
							loop

						end if
						rs4.close
						set rs4 = nothing
					end if
					'//=====4뎁스 메뉴끝

				rs2.movenext
				loop

			end if
		response.Write("</table>")

		rs2.close
		set rs2 = nothing
	%>
	<script type="text/JavaScript">
	<!--
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
	//-->
	</script>
	<script type="text/JavaScript">
	<!--
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	//-->
	</script>
	<%

End Function

Function submenu1()

	dim sql2, rs2, rs3, i, menuname, overmenu, cateStep, linkcode
	dim sql4, rs4
	cateStep = 3

	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if

	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &" and c_use = '1' order by c_sunse  "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		response.Write("<table border=0 cellspacing=0 cellpadding=0>")
			if rs2.eof then

			else

				Do while not rs2.eof

					overmenu = rs2("menuimg_over")
					if len(trim(overmenu)) = 0 or isnull(overmenu) then overmenu = rs2("menuimg")	'//over이미지가 없으면 첫이미지로 처리
					'response.Write len(trim(overmenu))
					menuname = rs2("menuimg")	'// 현재 클린된 페이지 활성화
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then menuname = overmenu


					if ucase(rs2("content_mode")) = "L" then
					'//컨텐츠 새창 띄우기 모드이면
						%>
						<tr>
							<td><a href="<%=rs2("content_link")%>" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('<%=rs2("c_code")%>','','/setImage/<%=overmenu%>',1)" onFocus="blur()"><img src="/setImage/<%=menuname%>" name="<%=rs2("c_code")%>" border="0" alt="<%=rs2("c_name")%>"></a></td>
						</tr>
						<%
					else
						%>
						<tr>
							<td><a href="?avan=<%=rs2("c_code")%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('<%=rs2("c_code")%>','','/setImage/<%=overmenu%>',1)" onFocus="blur()"><img src="/setImage/<%=menuname%>" name="<%=rs2("c_code")%>" border="0" alt="<%=rs2("c_name")%>"></a></td>
						</tr>
						<%
					end if

					'//==== 이하 메뉴가 있으면 4뎁스메뉴 ' 1뎁스만 사용하므로 무조건 실행안함. 2011-12-22 추가
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) and 1 = 2 then


							'call rowNavi(cateStep + 1)
							'call subsubmenu()

						'해당 서브메뉴 쿼리식
						sql4 = "select * from "& tablename &" where "& CateThisWhere(i+2,CateCode()) &" and c_use = '1' order by c_sunse "
						set rs4 = dbconn.execute(sql4)

						if not rs4.eof then

							Do while not rs4.eof

								overmenu = rs4("menuimg_over")
								if len(trim(overmenu)) = 0 or isnull(overmenu) then overmenu = rs4("menuimg")	'//over이미지가 없으면 첫이미지로 처리
								menuname = rs4("menuimg")	'// 현재 클린된 페이지 활성화
								if upCodeReturn(cateStep+1,CateCode) = upCodeReturn(cateStep+1,rs4("c_code")) then menuname = overmenu

								if ucase(rs4("content_mode")) = "L" then
									'//컨텐츠 새창 띄우기 모드이면
									%>
									<tr>
										<td><a href="<%=rs4("content_link")%>" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('<%=rs4("c_code")%>','','/setImage/<%=overmenu%>',1)" onFocus="blur()"><img src="/setImage/<%=menuname%>" name="<%=rs4("c_code")%>" border="0" alt="<%=rs4("c_name")%>"></a></td>
									</tr>
									<%
								else
									%>
									<tr>
										<td><a href="?avan=<%=rs4("c_code")%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('<%=rs4("c_code")%>','','/setImage/<%=overmenu%>',1)" onFocus="blur()"><img src="/setImage/<%=menuname%>" name="<%=rs4("c_code")%>" border="0" alt="<%=rs4("c_name")%>"></a></td>
									</tr>
									<%
								end if

							rs4.movenext
							loop

						end if
						rs4.close
						set rs4 = nothing
					end if
					'//=====4뎁스 메뉴끝

				rs2.movenext
				loop

			end if
		response.Write("</table>")

		rs2.close
		set rs2 = nothing
	%>
	<script type="text/JavaScript">
	<!--
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
	//-->
	</script>
	<script type="text/JavaScript">
	<!--
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	//-->
	</script>
	<%

End Function

Sub ContentsPage(byval code)
'	넘어오는 ?avan= 코드체트해서 해당 대역이 아니면 errer
	if left(request.QueryString("avan"),2) <> code then
		error("카테고리 코드오류!! 잘못된 대역접근 오류!!")
	else
		call ContentPage
	end if
End Sub

Sub ContentPage'//모드,디비저장값,변수값

	'//++++++++++++++++++++++++++++++++++++++++++++
	'// 시뮬레이션 용입 즉 벨루값이 먹지 않음
	'//++++++++++++++++++++++++++++++++++++++++++++
	dim objScrFso, userSessionlevel
	Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")

	'//ValuesSetting()가 선언된 상태에서만 사용가능
	'response.Write(content_mode)
	dim smulmode : smulmode = request.QueryString("avan")


	if isnull(pagelevel) then pagelevel = 0
	userSessionlevel= session("userlevel")
	if userSessionlevel="" then userSessionlevel = 0

	'response.Write pagelevel&"/"&session("userlevel")
	if trim(pagelevel) > trim(userSessionlevel) then
		'//페이지 접근권한이 없으면 로그인 페이지 출력
		server.Execute("/avanplus/modul/member4/call.asp")

	elseif ucase(content_mode) = "D" then		'//Design	디자인 페이지, 즉 html
		'response.Write(dbdata)
		IF LEN(DATA_WDAY) > 0 then
			response.Write data_orignal
		elseif smulmode = "editor" then
			 call setEditor("data_orignal",data_orignal)
		else
			response.Write data_orignal
		end if
	elseif ucase(content_mode) = "M" then	'//Modul 아반모듈
		'response.Write(content_modul)
		'//모듈 보이기
		'//content_modulvalues <--사용자가 지정한 모듈 변수 값
		'if lcase(content_modul) = "board" then
		 	'call board(content_modulvalues)

		'//모듈구분값에 값이 있으면 get방식으로 값을 넘긴다.
		IF instr(lcase(request.ServerVariables("QUERY_STRING")),lcase(content_modulvalues))=0 THEN
			'//모듈구분값에 값이 있으면 get방식으로 값을 넘긴다.
			if len(content_modulvalues) > 0 then
				content_modulvalues = lcase(content_modulvalues)

				if INSTR(content_modulvalues,"=") > 0 then
					response.redirect("?"&getString(content_modulvalues))
				else
					response.redirect("?"&getString("bs_code="&content_modulvalues))
				end if
			end if
		END IF

		content_modul = trim(content_modul)
		content_modul = "/avanplus/modul/"&content_modul&"/call.asp"
		'Response.write content_modul

		If objScrFso.FileExists(Server.MapPath(content_modul)) Then
			Server.Execute(content_modul)
			Response.Write "<!--------------------------"
			Response.Write "모듈ID:"&content_modul
			Response.Write "---------------------------->"
		Else
			Response.Write "모듈ID가 존재하지 않습니다. 다시한번 확인해주세요."
		End If



	elseif ucase(content_mode) = "E" then	'//Execute

		'Response.write instr(lcase(request.ServerVariables("QUERY_STRING")),lcase(content_modulvalues))=0
		'//모듈구분값에 값이 있으면 get방식으로 값을 넘긴다.
		IF instr(lcase(request.ServerVariables("QUERY_STRING")),lcase(content_modulvalues))=0 THEN
			'//모듈구분값에 값이 있으면 get방식으로 값을 넘긴다.
			if len(content_modulvalues) > 0 then
				content_modulvalues = lcase(content_modulvalues)

				if INSTR(content_modulvalues,"=") > 0 then
					response.redirect("?"&getString(content_modulvalues))
				else
					response.redirect("?"&getString("bs_code="&content_modulvalues))
				end if
			end if
		END IF



		If objScrFso.FileExists(Server.MapPath(content_execute)) Then
			Server.Execute(content_execute)
			Response.Write "<!--------------------------"
			Response.Write "물리적경로:"&content_execute
			Response.Write "---------------------------->"
		Else
			Response.Write "path:"&content_execute&"<br>"
			Response.Write "실행하려는 파일이 존재하지 않습니다. 파일의 경로를 확인해 주세요."
		End If

	elseif ucase(content_mode) = "R" then	'//redirect
		response.Redirect(content_redirect)
	elseif ucase(content_mode) = "L" then	'//LINK 새창 띄우기
		'response.Write "해당메뉴를 클릭하면 새창이 뜹니다."

'		response.Write "<body onload='window.open("&content_link&"', '', 'height=600, width=950, left=0, top=0, toolbar=1, location=1,directories=1,status=1,menuBar=1,scrollBars=1,resizable=1');'></body>"
		%>
			<!--
			<script language="javascript">
				window.open('<%=content_link%>', 'a', 'height=600, width=950, left=0, top=0, toolbar=1, location=1,directories=1,status=1,menuBar=1,scrollBars=1,resizable=1');
				history.back();
			</script>
			-->
		<%

	elseif ucase(content_mode) = "S" then	'//sub 서브 첫페이지로 이동
		'response.Write ("select top 1 c_code from HOMEPCATE where "& CateNextWhere() &" and siteid = '"& siteid &"' order by c_sunse  ")
		response.Redirect "?"&getString("avan="&getAdoRsScalar("select top 1 c_code from "& tablename &" where "& CateNextWhere() &" and c_use = '1' order by c_sunse asc "))
	end if


	Set objScrFso = Nothing

End Sub


	'///////////////////////////////////////////
	'
	'	각 페이지에서 사용할 변수를

	'///////////////////////////////////////////


		dim c_name
		dim c_img
		dim menuimg

		dim menuimg_over
		dim menuimg_select
		dim data_orignal
		dim data_memo
		dim data_Wday
		dim data_userip
		dim data_planer
		dim orderMode
		dim content_mode
		dim content_html
		dim content_modul
		dim content_modulvalues
		dim content_execute
		dim content_redirect
		dim content_link
		dim c_use
		dim pagelevel
Sub builderData()
	'// 현재 페이지의 데이터를 불러옵니다.
	dim sql, rs
	sql = "select * from "& tablename &" where c_code = '"& CateCode() &"'"
	set rs = dbconn.execute(sql)

	if not rs.eof then

		c_name 					= rs("c_name")
		c_img 					= rs("c_img")
		menuimg 				= rs("menuimg")
		menuimg_over 			= rs("menuimg_over")
		menuimg_select 			= rs("menuimg_select")
		data_orignal 			= rs("data_orignal")
		data_memo 				= rs("data_memo")
		data_Wday 				= rs("data_Wday")
		data_userip 			= rs("data_userip")
		data_planer 			= rs("data_planer")
		orderMode 				= rs("orderMode")
		content_mode 			= rs("content_mode")
		content_html 			= rs("content_html")
		content_modul 			= rs("content_modul")
		content_modulvalues		= rs("content_modulvalues")
		content_execute 		= rs("content_execute")
		content_redirect		= rs("content_redirect")
		content_link			= rs("content_link")
		c_use					= rs("c_use")
		pagelevel				= rs("pagelevel")


	end if
	rs.close
	set rs = nothing
End Sub




Function BuilderMain()
'==============================================
'	빌더 디폴트 페이지 호출
'==============================================
	server.Execute("/avanplus/CallBuilderDefault.asp")
End Function



Function login()
'==============================================
'	로그인페이지호출
'==============================================
	server.Execute("/avanplus/modul/member4/call.asp")
End Function



Function topMenu()
'==============================================
'	하단 비즈홈피 로고및 링크
'==============================================
	%>
	<table border="0" cellpadding="0" cellspacing="0">
	  <tr align="center">
		<td><a href="/"><img src="/avanplus/images/defaultCode/t_home.gif" width="16" height="15" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<% if session("userid") = "" then %>
		<td><a href="login.asp"><img src="/avanplus/images/defaultCode/t_login.gif" alt="로그인" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<td><a href="login.asp?member4=contract"><img src="/avanplus/images/defaultCode/t_join.gif" alt="회원가입" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<% else %>
		<td><a href="login.asp?member4=logoutok"><img src="/avanplus/images/defaultCode/t_logout.gif" alt="로그아웃" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<td><a href="login.asp?member4=modify"><img src="/avanplus/images/defaultCode/t_myinfo.gif" alt="회원정보" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<% end if %>
		<td><a href="mailto:<%bPrint("adminMail")%>"><img src="/avanplus/images/defaultCode/t_email.gif" alt="이메일" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<td><a href="javascript:window.external.addfavorite('http://<%bPrint("domain")%>', '<%bPrint("title")%>');"><img src="/avanplus/images/defaultCode/t_bookmark.gif" alt="즐겨찾기추가" border="0" /></a></td>
	  </tr>
	</table>
	<%
End Function

Function topMenu01()
'==============================================
'	하단 비즈홈피 로고및 링크 //부트스트랩 형식
'==============================================
	%>
	<!-- table border="0" cellpadding="0" cellspacing="0">
	  <tr align="center">
		<td><a href="/"><img src="/avanplus/images/defaultCode/t_home.gif" width="16" height="15" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<% if session("userid") = "" then %>
		<td><a href="login.asp"><img src="/avanplus/images/defaultCode/t_login.gif" alt="로그인" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<td><a href="login.asp?member4=contract"><img src="/avanplus/images/defaultCode/t_join.gif" alt="회원가입" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<% else %>
		<td><a href="login.asp?member4=logoutok"><img src="/avanplus/images/defaultCode/t_logout.gif" alt="로그아웃" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<td><a href="login.asp?member4=modify"><img src="/avanplus/images/defaultCode/t_myinfo.gif" alt="회원정보" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<% end if %>
		<td><a href="mailto:<%bPrint("adminMail")%>"><img src="/avanplus/images/defaultCode/t_email.gif" alt="이메일" border="0" /></a></td>
		<td width="8px"><img src="/avanplus/images/defaultCode/t_line.gif" /></td>
		<td><a href="javascript:window.external.addfavorite('http://<%bPrint("domain")%>', '<%bPrint("title")%>');"><img src="/avanplus/images/defaultCode/t_bookmark.gif" alt="즐겨찾기추가" border="0" /></a></td>
	  </tr>
	</table -->
	    <ul class="loginbar pull-right">
            <li>
                <i class="icon-globe"></i>
                <a>Languages</a>
                <ul class="lenguages">
                    <li class="active">
                        <a href="#">English <i class="icon-ok"></i></a> 
                    </li>
                    <li><a href="#">Spanish</a></li>
                    <li><a href="#">Russian</a></li>
                    <li><a href="#">German</a></li>
                </ul>
            </li>
            <li class="devider"></li>   
            <li><a href="page_faq.html">Help</a></li>  
            <li class="devider"></li>   
            <li><a href="page_login.html">Login</a></li>   
        </ul>
	<%
End Function

Function topMenuText()
'==============================================
' 상단 로그인/로그아웃 메뉴
' 텍스트 형식의 메뉴
' 로그인 창을 모달로 호출 
' loginModal() 을 페이지 하단에서 불러와야 한다. // BuilderEnd.asp  에서 호출
'==============================================
%>
<style type="text/css">
.topMenu {list-style:none; font-family:'돋움',dotum; background-color: ivory;}
.centered { display: table; margin-left: auto; margin-right: auto; }

 .submenu{height:5px; background:#ffffff; padding:5px 0px; border-bottom:0px solid #f3f3f3;  overflow:hidden}
 .submenu li{ float:left; border:0; padding-right:2px; padding-left:0px;}
 .submenu li a{display:block; color:#969494;padding-right:0; font-size:11px;letter-spacing:-px; padding-left:4px;  no-repeat left 50%;}
 .submenu li:first-child a{ background:none;padding-left:0px; margin-left:0px;}
</style>
	<div class="submenu topMenu centered">
		<ul>
		    <% if session("userid") = "" then %>
			<li><a href="#dialog1" name="modal">로그인</a></li>
            <li><a href="login.asp?member4=contract">회원가입</a></li>
			<% else %>
			<li><a href="login.asp?member4=logoutok">로그아웃</a></li>
            <li><a href="login.asp?member4=modify">회원정보</a></li>
			<% end if %>
			<li><a href="mailto:<%bPrint("adminMail")%>">고객센터</a></li>
			<li><a href="javascript:window.external.addfavorite('http://<%bPrint("domain")%>', '<%bPrint("title")%>');">즐겨찾기</a></li>
		</ul>
	</div>
<%
End Function


Function MadeByBizhomep()
'==============================================
'	하단 비즈홈피 로고및 링크
'==============================================
	response.Write("<a href=http://work.avansoft.co.kr/work/default.asp?pno="&oBPrint("str_systemid")&" target=_blank><img src=/avanplus/images/defaultCode/madebybizhomep.gif border=0></a>")
End Function

Function loginModal()
'===============================================
'작업자 : 이용태 // 2014-05-07 
'로그인 모달 창 // 메인과 서브 페이지 하단에서 호출 해야 한다.
'topMenuText() 에서 링크
'===============================================
%>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<!-- link rel="stylesheet" href="http://mhyjj4.solutionhosting.co.kr/temp01/css/style.css" /-->
<link rel="stylesheet" href="/avanplus/css/contents.css" />
<script src="/avanplus/js/jquery-1.10.2.min.js"></script>  
<script src="/avanplus/js/common.js"></script> 
<script type="text/javascript" src="/avanplus/js/jquery.model.js"></script>

<script src="/avanplus/js/mainbanner.js"></script> 
<!-- script type="text/javascript" src="http://mhyjj4.solutionhosting.co.kr/temp01/js/jquery-1.9.1.min.js"></script -->
<script type="text/javascript" src="/avanplus/js/menu.js"></script>

<style type="text/css">
.avanModal1 b, i, dl, dt, dd, ol, ul, li, fieldset {margin:0; padding:0;  border:0; outline:0;vertical-align:baseline; background:transparent;}
</style>
<script language="javascript">
	<!--
		function login(frm)
		{
			if ( frm.strId.value.replace(/ /gi, "") == "" ) { alert("아이디를 입력해주세요"); frm.strId.focus(); return false; }
			if ( frm.strPwd.value.replace(/ /gi, "") == "" ) { alert("비밀번호를 입력해주세요"); frm.strPwd.focus(); return false; }			
		}
	//-->
</script>

<!-- boxes S -->
<div id="boxes">

	  
	<!-- 로그인 박스 S-->  
	<div id="dialog1" class="window avanModal1">
		<div class="login_form">
			<a href="#" class="close"/><img src="/avanplus/images/common/clear.png" alt="닫기"/></a>
			<form name = "loginform" action = "./login.asp?<%=GetString("member4=loginok")%>" method="post" onSubmit="return login(this)">
				<fieldset class="login_contents">
					<dl>
						<dt><label for="userID">아이디</label></dt>
						<dd><input type="text"  name="strId" id="userID" /></dd>
						<dt><label for="userPW">비밀번호</label></dt>
						<dd><input type="password" name="strPwd" id="userPW" /></dd>
					</dl>
					<button type="button" class="btnLogin" onclick="loginform.submit();">로그인</button>
					<!-- button type="submit" class="btnLogin" >로그인</button -->
				</fieldset>
			</form>
			<div class="smenu">
				<ul>
					<!-- li><a href="#dialog2"  name="modal"  onclick="closePop('dialog1')">회원가입</a></li -->
					<li><a href="login.asp?member4=contract" >회원가입</a></li>
					<!-- li><a href="login.html">로그인</a></li -->
					<li><a href="login.asp?member4=IDsearch">아이디찾기</a></li>
					<li><a href="serch_pw.html">비밀번호 찾기</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- 로그인 박스 E --> 

	<!-- 마스크 영역 - 삭제하면 작동하지 않습니다. -->
	<div id="mask"></div>
</div>
<!-- boxes E -->
<%
End Function
%>
<%
Function Subleft_title()
	dim step
	step = getCateStep(CateCode())
	'//기존소스임.
	if step > 1 then
		'cateVisualimg(getAdoRsScalar("select c_img from "& tablename &" where c_code = '"& upCodeReturn(3,CateCode()) &"'"))
		response.write(getAdoRsScalar("select c_name from "& tablename &" where c_code = '"& upCodeReturn(2,CateCode()) &"'"))
	end if
End Function

Function SubTitle()
	dim step
	step = getCateStep(CateCode())

	if step > 2 then
		'cateVisualimg(getAdoRsScalar("select c_img from "& tablename &" where c_code = '"& upCodeReturn(5,CateCode()) &"'"))
		cateVisualimg(getAdoRsScalar("select c_img from "& tablename &" where c_code = '"& request.QueryString("avan") &"'"))

	end if
End Function

%>