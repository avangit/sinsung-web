<%

	Function IsTextExist(ByVal strText, ByVal strAllText, ByVal strDelimiter)
		
		Dim blnChk, arrAllText, intCLoop

		strText = Trim(strText)

		blnChk = False
		if len(strAllText) > 0 then
			arrAllText = Split(strAllText, strDelimiter)
			
			For intCLoop = 0 To UBound(arrAllText, 1)
				If strText = Trim(arrAllText(intCLoop)) Then
					blnChk = True
					Exit For
				End If
			Next
		end if
		
		IsTextExist = blnChk

	End Function

	Function IsValue(ByVal strValue)

		If IsEmpty(strValue) Or IsNull(strValue) Or Trim(strValue) = "" Then
			IsValue = False
		Else
			IsValue = True
		End If

	End Function

	'*********************************************************
	'  함수명 :	Form_CheckBoxPrint
	'  설  명 :	
	'  인  수 :	strInputName		체크박스 이름
	'			strInputValue		체크박스 값(체크박스는 여러개 체크 가능하므로 아래 예와 값이 축구, 농구 이렇게 들어갈 수도 있음)
	'			strValueALL			체크박스 값종류 전부
	'			strValueALLText		체크박스 출력종류 전부(값종류가 빈 문자열일 경우 이걸루 값 대체)
	'			strDelimiter		체크박스 값과 값종류 각각 구분짓는 문자열(되도록이면 "," 이걸로나누는게...)
	'			intColumnEA			한줄에 출력될 체크박스
	'			intCellPadding		셀간격
	'Call Form_CheckBoxPrint("strHobby", "축구, 농구", "", "축구,농구,기타", ",", 4, 5)
	'Call Form_CheckBoxPrint("strHobby", "01, 02", "01,03,02", "축구,농구,기타", ",", 4, 5)
	'*********************************************************
	Sub Form_CheckBoxPrint(ByVal strInputName, ByVal strInputValue, ByVal strValueAll, ByVal strValueAllText, ByVal strDelimiter, ByVal intColumnEA, ByVal intCellPadding)

		Dim arrValueALL, arrValueALLText
		Dim intCLoop, strThisValue, strThisText, strChecked
		
		If IsValue(strValueAllText) Then 
			arrValueAllText = Split(strValueAllText, strDelimiter)
		End If
		If IsValue(strValueAll) Then 
			arrValueAll = Split(strValueAll, strDelimiter)
		Else
			arrValueAll = arrValueAllText
		End If
		%>
		<!--<table border="0" cellpadding="0" cellspacing="<%=intCellPadding%>">-->
			<%
				If IsArray(arrValueAllText) Then
					For intCLoop = 0 To UBound(arrValueAllText, 1)

						If intCLoop Mod intColumnEA = 0 Then
							%><!--<tr>--><%
						End If
						
						strThisValue = Trim(arrValueAll(intCLoop))
						strThisText = Trim(arrValueAllText(intCLoop))

						If IsTextExist(strThisValue, strInputValue, strDelimiter) = True Then
							strChecked = " checked"
						Else
							strChecked = ""
						End If
			%>
						<!--<td>-->
						<label class="checkbox" style="float:left; width:100px; text-align:left;">
							<input type="checkbox" name="<%=strInputName%>" value="<%=strThisValue%>" style="border:0px;"<%=strChecked%>>
							<i></i>
							<%=strThisText%>
						</label>
						<!--</td>-->
			<%
						If (intCLoop + 1) Mod intColumnEA = 0 And intCLoop > 0 Then
							%><!--</tr>--><%
						End If

					Next
					
					If intCLoop Mod intColumnEA > 0 And intCLoop > 0 And intCLoop <> UBound(arrValueAllText,1) Then
						Dim intRevLoop : intRevLoop = intColumnEA - (intCLoop Mod intColumnEA)
						For intCLoop = 1 To intRevLoop
							'Response.Write "<td width='" & Int(100 / intColumnEA ) & "%'>&nbsp;</td>"
						Next
						%><!--</tr>--><%
					End If
				End If
			%>
		<!--</table>-->
		<%
	End Sub
	
	
	'*********************************************************
	'  함수명 :	Form_RadioPrint
	'  설  명 :	
	'  인  수 :	strInputName		체크박스 이름
	'			strInputValue		체크박스 값(체크박스는 여러개 체크 가능하므로 아래 예와 값이 축구, 농구 이렇게 들어갈 수도 있음)
	'			strValueALL			체크박스 값종류 전부
	'			strValueALLText		체크박스 출력종류 전부(값종류가 빈 문자열일 경우 이걸루 값 대체)
	'			strDelimiter		체크박스 값과 값종류 각각 구분짓는 문자열(되도록이면 "," 이걸로나누는게...)
	'			intColumnEA			한줄에 출력될 체크박스
	'			intCellPadding		셀간격
	'Call Form_RadioPrint("strHobby", "축구, 농구", "", "축구,농구,기타", ",", 4, 5)
	'Call Form_RadioPrint("strHobby", "01, 02", "01,03,02", "축구,농구,기타", ",", 4, 5)
	'*********************************************************
	Sub Form_RadioPrint(ByVal strInputName, ByVal strInputValue, ByVal strValueAll, ByVal strValueAllText, ByVal strDelimiter, ByVal intColumnEA, ByVal intCellPadding)

		Dim arrValueALL, arrValueALLText
		Dim intCLoop, strThisValue, strThisText, strChecked
		
		If IsValue(strValueAllText) Then 
			arrValueAllText = Split(strValueAllText, strDelimiter)
		End If
		If IsValue(strValueAll) Then 
			arrValueAll = Split(strValueAll, strDelimiter)
		Else
			arrValueAll = arrValueAllText
		End If
		%>

			<%
				If IsArray(arrValueAllText) Then
					For intCLoop = 0 To UBound(arrValueAllText, 1)

						If intCLoop Mod intColumnEA = 0 Then
							%><%
						End If
						
						strThisValue = Trim(arrValueAll(intCLoop))
						strThisText = Trim(arrValueAllText(intCLoop))

						If IsTextExist(strThisValue, strInputValue, strDelimiter) = True Then
							strChecked = " checked"
						Else
							strChecked = ""
						End If
			%>

							<input type="radio" name="<%=strInputName%>" value="<%=strThisValue%>" style=""<%=strChecked%>>
							<%=strThisText%>

			<%
						If (intCLoop + 1) Mod intColumnEA = 0 And intCLoop > 0 Then
							%><br><%
						End If

					Next
					
					If intCLoop Mod intColumnEA > 0 And intCLoop > 0 And intCLoop <> UBound(arrValueAllText,1) Then
						Dim intRevLoop : intRevLoop = intColumnEA - (intCLoop Mod intColumnEA)
						For intCLoop = 1 To intRevLoop
							'Response.Write "<td width='" & Int(100 / intColumnEA ) & "%'>&nbsp;</td>"
						Next
						%><%
					End If
				End If
			%>

		<%
	End Sub

	'*********************************************************
	'  함수명 :	Form_SelectPrint
	'  설  명 :	
	'  인  수 :	strSelectName		셀렉트박스 이름
	'			strSelectValue		셀렉트박스 값(체크박스는 여러개 체크 가능하므로 아래 예와 값이 축구, 농구 이렇게 들어갈 수도 있음)
	'			strMsg				체크박스 값종류 전부
	'			strSelectAll		셀렉트박스 출력실제값
	'			strSelectAllText	셀렉트박스 출력텍스트
	'			strDelimiter		체크박스 값종류 전부 각각 구분짓는 문자열(되도록이면 "," 이걸로나누는게...)
	'			strAction			체크박스값바꿀때 실행될 이벤트지정
	'  사용예 : Call Form_SelectPrint("strHobby", "축구", "선택해주세요", "", "축구,농구,기타" "onchange='changeType(this.form,this.value);'")
	'Call Form_SelectPrint("strHobby", "축구", "선택해주세요", "", "축구,농구,기타", ",", "onchange='changeType(this.form,this.value);'")
	'Call Form_SelectPrint("strHobby", "02", "선택해주세요", "01,02,03", "축구,농구,기타", ",", "onchange='changeType(this.form,this.value);'")
	'*********************************************************
	Sub Form_SelectPrint(ByVal strSelectName, ByVal strSelectValue, ByVal strDefaultMsg, ByVal strSelectAll, ByVal strSelectAllText, ByVal strDelimiter, ByVal strAction)
		Dim arrSelectAll, arrSelectAllText, blnValueAll, strSelected, intCLoop, strThisValue, strThisText

		If IsValue(strAction) Then
			strAction = " " & strAction
		End If

		If IsValue(strSelectAllText) Then 
			arrSelectAllText = Split(strSelectAllText, strDelimiter)
		End If

		If IsValue(strSelectAll) Then 
			arrSelectAll = Split(strSelectAll, strDelimiter)
		Else
			arrSelectAll = arrSelectAllText
		End If
		%>
			<select name="<%=strSelectName%>"<%=strAction%> class="input01">
				<%
					If IsValue(strDefaultMsg) Then
				%>
				<option value=""><%=strDefaultMsg%></option>
				<%
					End If
					For intCLoop = 0 To UBound(arrSelectAll, 1)
						
						strThisValue = Trim(arrSelectAll(intCLoop))
						strThisText = Trim(arrSelectAllText(intCLoop))

						If trim(strSelectValue) = trim(strThisValue) Then 
							strSelected = " selected" 
						Else 
							strSelected = "" 
						End If
						%><option value="<%=strThisValue%>"<%=strSelected%>><%=strThisText%></option><%
					Next						
				%>
			</select>
		<%
	End Sub

	'*********************************************************
	'  함수명 :	Form_TelephonePrint
	'  설  명 :	
	'  인  수 :	strInputName		전화번호인풋이름
	'			strValue			전화번호값
	'  사용예 : Call Form_TelephonePrint("strTelephone", strTelephone)
	'Call Form_TelephonePrint("strTelephone", "063-555-3652")
	'*********************************************************	
	Sub Form_TelephonePrint(ByVal strInputName, ByVal strValue)
		
		Dim strInputName1, strInputName2, strInputName3
		Dim strValue1, strValue2, strValue3
		Dim arrValue
		Dim strPhone1ALL

		strInputName1 = strInputName & "1"
		strInputName2 = strInputName & "2"
		strInputName3 = strInputName & "3"
		
		If IsValue(strValue) Then
			arrValue = Split(strValue, "-")
			If IsArray(arrValue) Then
				If UBound(arrValue, 1) >= 0 Then strValue1 = arrValue(0) Else strValue1 = "" End If
				If UBound(arrValue, 1) >= 1 Then strValue2 = arrValue(1) Else strValue2 = "" End If
				If UBound(arrValue, 1) >= 2 Then strValue3 = arrValue(2) Else strValue3 = "" End If
			Else
				strValue1 = ""
				strValue2 = ""
				strValue3 = ""
			End If
		Else
			strValue1 = ""
			strValue2 = ""
			strValue3 = ""
		End If

		strPhone1ALL = "02,031,032,033,041,042,043,051,052,053,054,055,061,062,063,064,070"
		Call Form_SelectPrint(strInputName1, strValue1, "", "", strPhone1ALL, ",", " onchange=""SetTelephone_" & strInputName & "(this.form);""")
		%>
		-
		<input type="text" maxlength="4" size="4" name="<%=strInputName2%>" value="<%=strValue2%>" onkeyup="if(isNaN(this.value))this.value='';SetTelephone_<%=strInputName%>(this.form);" class="input01 w20">
		-
		<input type="text" maxlength="4" size="4" name="<%=strInputName3%>" value="<%=strValue3%>" onkeyup="if(isNaN(this.value))this.value='';SetTelephone_<%=strInputName%>(this.form);" class="input01 w20">
		<input type="hidden" name="<%=strInputName%>" value="<%=strValue%>" class="input01 w20">
		<script language="javascript">
		<!--
			function SetTelephone_<%=strInputName%>(frm){
				frm.<%=strInputName%>.value = frm.<%=strInputName1%>.value + "-" + frm.<%=strInputName2%>.value + "-" + frm.<%=strInputName3%>.value;
			}
		//-->
		</script>
		<%
	End Sub

	'*********************************************************
	'  함수명 :	Form_HandphonePrint
	'  설  명 :	
	'  인  수 :	strInputName		핸드폰인풋이름
	'			strValue			핸드폰값
	'  사용예 : Call Form_HandphonePrint("strTelephone", strTelephone)
	'Call Form_HandphonePrint("strTelephone", "019-612-6024")
	'*********************************************************	
	Sub Form_HandphonePrint(ByVal strInputName, ByVal strValue)
		
		Dim strInputName1, strInputName2, strInputName3
		Dim strValue1, strValue2, strValue3
		Dim arrValue
		Dim strPhone1ALL
		strInputName1 = strInputName & "1"
		strInputName2 = strInputName & "2"
		strInputName3 = strInputName & "3"
		
		If IsValue(strValue) Then
			arrValue = Split(strValue, "-")
			If IsArray(arrValue) Then
				If UBound(arrValue, 1) >= 0 Then strValue1 = arrValue(0) Else strValue1 = "" End If
				If UBound(arrValue, 1) >= 1 Then strValue2 = arrValue(1) Else strValue2 = "" End If
				If UBound(arrValue, 1) >= 2 Then strValue3 = arrValue(2) Else strValue3 = "" End If
			Else
				strValue1 = ""
				strValue2 = ""
				strValue3 = ""
			End If
		Else
			strValue1 = ""
			strValue2 = ""
			strValue3 = ""
		End If

		strPhone1ALL = "010,011,016,017,018,019"
		Call Form_SelectPrint(strInputName1, strValue1, "", "", strPhone1ALL, ",", " onchange=""SetHandPhone_" & strInputName & "(this.form);""")
		%>
		-
		<input type="text" maxlength="4" size="4" name="<%=strInputName2%>" value="<%=strValue2%>" onkeyup="if(isNaN(this.value))this.value='';SetHandPhone_<%=strInputName%>(this.form);" width="50">
		-
		<input type="text" maxlength="4" size="4" name="<%=strInputName3%>" value="<%=strValue3%>" onkeyup="if(isNaN(this.value))this.value='';SetHandPhone_<%=strInputName%>(this.form);">
		<input type="hidden" name="<%=strInputName%>" value="<%=strValue%>">
		<script language="javascript">
		<!--
			function SetHandPhone_<%=strInputName%>(frm){
				frm.<%=strInputName%>.value = frm.<%=strInputName1%>.value + "-" + frm.<%=strInputName2%>.value + "-" + frm.<%=strInputName3%>.value;
			}
		//-->
		</script>
		<%
	End Sub

	'*********************************************************
	'  함수명 :	Form_EmailPrint
	'  설  명 :	
	'  인  수 :	strInputName		이메일인풋이름
	'			strValue			이메일값
	'  사용예 : Call Form_EmailPrint("strTelephone", strTelephone)
	'Call Form_EmailPrint("strTelephone1", "")
	'Call Form_EmailPrint("strTelephone2", "test@hanmir.com")
	'Call Form_EmailPrint("strTelephone3", "test@test.com")
	'*********************************************************	
	Sub Form_EmailPrint(ByVal strInputName, ByVal strValue)

		Dim strInputName1, strInputName2a, strInputName2b
		Dim strValue1, strValue2a, strValue2b
		Dim strMailALL, strMailALLText, blnMailType, arrValue, intLoop, blnSelected

		strInputName1 = strInputName & "1"
		strInputName2a = strInputName & "2a"
		strInputName2b = strInputName & "2b"

		strMailALL = "" _
		& "naver.com" _
		& "," & "daum.net" _
		& "," & "hotmail.com" _
		& "," & "nate.com" _
		& "," & "yahoo.co.kr" _
		& "," & "paran.com" _
		& "," & "empas.com" _
		& "," & "dreamwiz.com" _
		& "," & "freechal.com" _
		& "," & "lycos.co.kr" _
		& "," & "korea.com" _
		& "," & "gmail.com" _
		& "," & "hanmir.com" _
		& "," & "직접입력"

		strMailALLText = "" _
		& "naver.com" _
		& "," & "daum.net" _
		& "," & "hotmail.com" _
		& "," & "nate.com" _
		& "," & "yahoo.co.kr" _
		& "," & "paran.com" _
		& "," & "empas.com" _
		& "," & "dreamwiz.com" _
		& "," & "freechal.com" _
		& "," & "lycos.co.kr" _
		& "," & "korea.com" _
		& "," & "gmail.com" _
		& "," & "hanmir.com" _
		& "," & "직접입력"

		If IsValue(strValue) Then
			arrValue = Split(strValue, "@")
			If IsArray(arrValue) Then
				If UBound(arrValue) >= 0 Then strValue1 = arrValue(0) Else strValue1 = "" End If
				If UBound(arrValue) >= 1 Then 
					blnMailType = IsTextExist(arrValue(1), strMailALL, ",")
					If blnMailType And IsValue(strValue) Then 
						strValue2a = arrValue(1)
						strValue2b = arrValue(1)
					Else
						strValue2a = arrValue(1)
						strValue2b = "직접입력"
					End If
				Else
					strValue2a = ""
					strValue2b = ""	
				End If
			Else
				strValue1 = ""
				strValue2a = ""
				strValue2b = ""
			End If
		Else
			strValue1 = ""
			strValue2a = ""
			strValue2b = ""
		End If
		%>
		<input type="text" width="70" maxlength="25" name="<%=strInputName1%>" value="<%=strValue1%>" onkeyup="SetEmailAddress_<%=strInputName%>(this.form);" class="input_style01" style="width:100px;">
		@
		<input type="text" width="70"maxlength="25" name="<%=strInputName2a%>" value="<%=strValue2a%>" onkeyup="SetEmailAddress_<%=strInputName%>(this.form);" class="input_style01" style="width:100px;" readonly>
		<%Call Form_SelectPrint(strInputName2b, strValue2b, "메일선택", strMailALL, strMailALLText, ",", " onchange=""changeEmailType_" & strInputName & "(this.form, this.value);SetEmailAddress_" & strInputName & "(this.form);""")%>
		<input type="hidden" name="<%=strInputName%>" value="<%=strValue%>">
		<script language="javascript">
		<!--
			function changeEmailType_<%=strInputName%>(frm, emailType){
				frm.<%=strInputName2a%>.value = "";
				if (emailType == "직접입력") {
					frm.<%=strInputName2a%>.readOnly = false;
					frm.<%=strInputName2a%>.focus();
				} else {
					frm.<%=strInputName2a%>.readOnly = true;
					frm.<%=strInputName2a%>.value = emailType;
				}
			}
			function SetEmailAddress_<%=strInputName%>(frm){
				frm.<%=strInputName%>.value = frm.<%=strInputName1%>.value + "@" + frm.<%=strInputName2a%>.value;
			}
		//-->
		</script>
		<%
	End Sub


	'*********************************************************
	'  함수명 :	Form_NumberSelectPrint
	'  설  명 :	
	'  인  수 :	strSelectName		이메일인풋이름
	'			intSelectValue		이메일값
	'			strDefaultMsg		이메일값
	'			intStartNum			이메일값
	'			intEndNum			이메일값
	'			strChangeAction		이메일값
	'  사용예 : Call Form_NumberSelectPrint("strTelephone", strTelephone)
	'Call Form_NumberSelectPrint("strStart", "", "선택해주세요", 0, 100, "")
	'Call Form_NumberSelectPrint("strStart", 20, "선택해주세요", 0, 100, "")
	'*********************************************************
	Sub Form_NumberSelectPrint(ByVal strSelectName, ByVal intSelectValue, ByVal strDefaultMsg, ByVal intStartNum, ByVal intEndNum, ByVal strChangeAction)
		
		If IsValue(strChangeAction) = True Then
			strChangeAction = " onchange=""" & strChangeAction & """"
		End If

		%>
		<select name="<%=strSelectName%>"<%=strChangeAction%>>
			<%
				If IsValue(strDefaultMsg) Then
			%>
			<option value=""><%=strDefaultMsg%></option>
			<%
				End If
				Dim intLoop, strSelected
				If IsValue(intSelectValue) = False Then
					intSelectValue = -1
				End If
				For intLoop = intStartNum To intEndNum
					If Int(intSelectValue) = Int(intLoop) Then
						strSelected = " selected"
					Else
						strSelected = ""
					End If
					%><option value="<%=intLoop%>"<%=strSelected%>><%=intLoop%></option><%
				Next
			%>
		</select>
		<%

	End Sub
	
	
	
	
	'//bit 참거짓을 반환받기 위한 함수
	Sub Form_RadioTF(ByVal strSelectName, ByVal intSelectValue)
		%><input name="<%=strSelectName%>" type="checkbox" value="1" <%if intSelectValue then response.Write("checked") %>/><%
	End Sub
	

%>