<% @CODEPAGE="65001" language="vbscript" %>
<% Option Explicit %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>

<!--#include virtual = "/AVANplus/_Function.asp"-->
<!--#include virtual = "/AVANplus/_FormFunc.asp"-->

<%	
	call dbopen
	
	'//적용할 프로그램 모듈ID 넣기
	'//2011-03-08 양일희 영문게시판 추가함
	const module_name	="게시판V3[M], 영문게시판V3[M]"		
	const module_value	="avanboard_v3, avanboard_v3_eng"
	
	
	
	dim tablename
	tablename = "BuilderCate"


	'##########################################
	'//사용함수
	'##########################################
	'// 카테고리 타입확인
	if len(request.QueryString("avan")) <> 9 and len(request.QueryString("avan")) <> 10 and len(request.QueryString("avan")) <> 0 then
		response.Write("카테고리 타입이 맞지 않습니다.")
		response.End()
	end if
	
	'dim catecode : catecode = "8854000000"
	
	'dim step
	'step = 4
	
	'Dim stepMax, stepMin
	'stepMax =  10^((5-step)*2)
	'stepMin =  stepMax - 1 
	
	'dim step999Str, step000Str
	'step999Str = Cstr(stepMin)
	'step000Str = replace(stepMin,"9","0")
	Function CateCode()
		dim HOMEPCATE
		HOMEPCATE = request.QueryString("HOMEPCATE")
		if len(HOMEPCATE) = 0 then
			CateCode = 0000000000
		else
			CateCode = HOMEPCATE
		end if
	End Function
	
	
	'// 현제 카테고리의 스텝을 반환
	Function getCateStep(byval HOMEPCATE)
		dim CateStep
		CateStep = Cdbl(HOMEPCATE)/10000000000	
		CateStep = replace(CateStep,".","")
		CateStep = int((Len(CateStep))/2)
		'if CateStep = 0 then CateStep = 1
		if CateStep > 5 then CateStep = 5
		getCateStep = CateStep
		
	End Function
	
	'// 해당 코드값의 초대/최소등 원하는 값 리턴
	Function getCateValuse(byval HOMEPCATE, byval str)
		dim CateAdd
		CateAdd = 10^(2*(4-getCateStep(HOMEPCATE)))
		if int(HOMEPCATE) = 0 then CateAdd = 100000000
		
		dim CateMin
		CateMin = HOMEPCATE
		
		dim CateMax
		CateMax = HOMEPCATE + ( (CateAdd*100) - 1 )
		if getCateStep(HOMEPCATE) = 0 then CateMax = 9999999999
		
	
		if Lcase(str) = "add" then
			getCateValuse = CateAdd
		elseif Lcase(str) = "max" then
			getCateValuse = CateMax
		elseif Lcase(str) = "min" then
			getCateValuse = CateMin
		end if						
	End Function
	
	'// 현재 카테고리의 스텝
	'response.Write("스텝:"&getCateStep(HOMEPCATE)&"<br>")
	'// 현재 카테고리의 추가값(인서트시 플러스 값)	
	'response.Write("추가값:"&getCateValuse(HOMEPCATE,"add")&"<br>")
	'// 현재 카테로기 최소값(카테고리 자신)
	'response.Write("최소값:"&getCateValuse(HOMEPCATE,"min")&"<br>")
	'// 현재 카테고리 최대값
	'response.Write("최대값:"&getCateValuse(HOMEPCATE,"max")&"<br>")
	
	
	
	'//넘어온 카테고리의 각 스텝의 코드값리턴
	Function upCodeReturn(byval step, byval HOMEPCATE)
		if len(HOMEPCATE) = 0 then 
			upCodeReturn = "0000000000"
		elseif HOMEPCATE = 0 then 
			upCodeReturn = "0000000000"
		else
			upCodeReturn = int( HOMEPCATE/(10^((5-step)*2)) ) * (10^((5-step)*2))
		end if
	End Function
	
	'response.Write("upCodeReturn:"&upCodeReturn(1,HOMEPCATE)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(2,HOMEPCATE)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(3,HOMEPCATE)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(4,HOMEPCATE)&"<br>")
	'response.Write("upCodeReturn:"&upCodeReturn(5,catecode)&"<br>")
	
	
	Function CateNextWhere()	'//넘어온 카테고리값의 하위카테고리 생성시 처음서 끝까지
		'// 1122000000 이 넘어오면
		'// 1122       앞부분인 와
		'//       0000 뒷부분이 인 값 알아오기
		dim strF, strR
		dim strS, strE
		
		strF	= mid(CateCode(),1,getCateStep(catecode)*2)
		if len(strF)>0 then strF = int(strF)
		
		strR	= 10^((4-getCateStep(catecode))*2)
		strR	= replace(Cstr(strR),"1","")
		strR	= replace(Cstr(strR),"0.0","")
		
		CateNextWhere = " c_code like '"& strF &"%' and c_code like '%"& strR &"' and c_code > "& upCodeReturn(getCateStep(catecode),catecode) &" "
	End Function

	
	
	Function CateThisWhere(byval num, byval HOMEPCATE)	'//해당스텝의 카테고리 목록 가져오기 쿼리
		'// 1122000000 이 넘어오면
		'// 1122       앞부분인 와
		'//       0000 뒷부분이 인 값 알아오기
		
		'//	 122000000 이면
		'//  122	
		'//       0000 값 알아오기
		
		dim strF, strR
		dim strS, strE
		dim strnum
		'// 카테고리 맨 앞자리가 2자리수 미만인 경우의 처리
		
		if len(HOMEPCATE) = 0 then
			strnum = 0	'//카테고리 값이 0인경우
		elseif int(HOMEPCATE) = 0 then
			strnum = 0	'//카테고리 값이 0인경우
		elseif int(HOMEPCATE) < 1000000000 then
			strnum = (num*2)-1
			if strnum < 0 then strnum = 0 '//계산식이 -1인경우는 0으로 대체
			'response.Write(strnum)
		else
			strnum = num*2
		end if

		strF	= left(HOMEPCATE,strnum)
		
		if len(strF)>0 then strF = int(strF)
		
		strR	= 10^((4-num)*2)
		strR	= replace(Cstr(strR),"1","")
		strR	= replace(Cstr(strR),"0.0","")
		
		CateThisWhere = " c_code like '"& strF &"%' and c_code like '%"& strR &"' and c_code > "& upCodeReturn(num,HOMEPCATE) &" "
	End Function
	

	
	'Function CateAllWhere()	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식
		'CateAllWhere = " Goods_cate_join.c_code >= "& int(getCateValuse(catecode,"min")) & " and Goods_cate_join.c_code <= "& int(getCateValuse(catecode,"max"))  
	'End Function
	
	Function CateAllWhere(byval HOMEPCATE)	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식 '//2개 테이블
		CateAllWhere = " Goods_cate_join.c_code >= "& int(getCateValuse(HOMEPCATE,"min")) & " and Goods_cate_join.c_code <= "& int(getCateValuse(HOMEPCATE,"max"))  
	End Function
	
	Function CateAllWhere1(byval HOMEPCATE)	'//해당 카테고리 하위 모든 상품을 가져오는 경우 쿼리식	'//1개 테이블
		CateAllWhere1 = " c_code >= "& int(getCateValuse(HOMEPCATE,"min")) & " and c_code <= "& int(getCateValuse(HOMEPCATE,"max"))  
	End Function
	
	

'// 스트링 값을 토대로 카테고리 리스트 출력
'// > 컴퓨터 > 자동차 > 부품
Function getCateColPrint(byval HOMEPCATE)	
	dim TempcateStep, i, sql_func, rs_func
	TempcateStep = getCateStep(HOMEPCATE)
	'response.Write getCateStep(HOMEPCATE)
	if getCateStep(HOMEPCATE) > 5 then TempcateStep = 5
	i = 0
  	for i = 0 to TempcateStep 
		
		sql_func = "select * from "& tablename &" where c_code = '"& upCodeReturn(i, HOMEPCATE ) &"'  "
			'response.Write(sql_func)
		set rs_func = dbconn.execute(sql_func)
			
			if rs_func.eof then
				
			else
				do while not rs_func.eof
					getCateColPrint = getCateColPrint & ">"&rs_func("c_name")
				rs_func.movenext
				loop
			end if
		
	next
End Function















'// 셀렉트 리스트 박스로 이어지는 카테고리 선택기
sub navi_select(byval HOMEPCATE)

	dim i, sql_func, rs_func, temp_selected
	
%>
<script type="text/JavaScript">
	<!--
	function MM_jumpMenu(targ,selObj,restore){ //v3.0
		  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
		  if (restore) selObj.selectedIndex=0;
	}
	//-->
</script>

	<table width="100" border="0">
		<tr>
<%
		dim TempcateStep 
		TempcateStep = getCateStep(HOMEPCATE) 
		
		if len(HOMEPCATE) = 0 then TempcateStep = 0 '//카테고리 값이 없는경우 스텝 0
		
		if getCateStep(HOMEPCATE) > 4 then TempcateStep = 4 '// 4이상인 경우 무시 4단계때 5단계를 생성하기 때문
		i = 0
		for i = 0 to TempcateStep 
			sql_func = "select * from "& tablename &" where "& CateThisWhere(i,request.QueryString("HOMEPCATE")) &"  order by c_sunse asc "
				'response.Write(sql_func)
			set rs_func = dbconn.execute(sql_func)
%>
		
			
			<td width="100">
				<table width="100" border="0" cellspacing="0" cellpadding="0">
					<form name="getCateSelect" id="getCateSelect">
						<tr>
							<td>
				
							<%
							if rs_func.eof then
							else
							%>
								<select name="getCateSelectMenu" onChange="MM_jumpMenu('self',this,0)" style="width:100">
									<option value="?<%=getString("")%>"><%=i+1%>차 카테고리 선택</option>
									<%			
									do while not rs_func.eof
							
									'// 각 단계의 값을 리턴(넘어온 카테코드와 디비의 코드가 같으면 셀렉티드)
									temp_selected = chk_selected( upCodeReturn(i+1, HOMEPCATE ), upCodeReturn(i+1, rs_func("c_code") ) )
									%>
										<option value="?<%=getString("HOMEPCATE="&rs_func("c_code"))%>" <%=temp_selected%>><%=rs_func("c_name")%></option>
									<%
									rs_func.movenext
									loop
									%>
								</select>
					
							<% 
							end if
							%>
							</td>
						</tr>
					</form>
				</table>
			</td>
			
		<%			
		next 
		%>
		</tr>
	</table>
<% End sub %>





<%

	Sub AUTOcatePage()
		'// 해당 카테고리 페이지에 맞는 페이지로 이동
		
		dim no, tempName
		no = getCateStep(cateCode)
		'response.write(no&"<<<<<<<<<<<<<")

		tempName = request.ServerVariables("PATH_INFO")

		
		if  no = 0 then
			'response.Write "카테고리가 없습니다."
		elseif no = 1 and ( lcase(tempName) <> lcase(url_step1) ) then
			response.Redirect(url_step1&"?"&getString(""))
		elseif no = 2 and ( lcase(tempName) <> lcase(url_step2) ) then
			response.Redirect(url_step2&"?"&getString(""))
		elseif no = 3 and ( lcase(tempName) <> lcase(url_step3) ) then
			response.Redirect(url_step3&"?"&getString(""))
		elseif no = 4 and ( lcase(tempName) <> lcase(url_step4) ) then
			response.Redirect(url_step4&"?"&getString(""))
		elseif no = 5 and ( lcase(tempName) <> lcase(url_step5) ) then
			response.Redirect(url_step5&"?"&getString(""))
		end if
		
	End Sub


%>




<%
Sub navi_2_3stpe(byval step, byval viewOption)
	'//step 카테고리 스텝이 어디부터 인지? ex. 1 인경우는 1은 세로, 2는 가로에 출력
	'//viewOption	카테고리 보기 세로가 1단인지 전체인지 // 갑이 1이면 1단 0 이면 모두
	
	'// 카테고리 코드 확인
	if len(request.QueryString("HOMEPCATE")) = 0 then
		response.Write("카테고리 값이 없습니다.")
		response.end
	elseif int(request.QueryString("HOMEPCATE")) = 0 then 
		response.Write("카테고리가 0입니다. 정상적인 카테고리 값을 보내세요.")
		response.end
	end if

	
	
	dim sql, rs, i, top, sql2, rs2
	i = ( step - 1 )
		'for i = 0 to TempcateStep 
		if viewOption = 1 then top = " top 1 "
		sql = "select "& top &" * from "& tablename &" where "& CateThisWhere(i,request.QueryString("HOMEPCATE")) &"  order by c_sunse asc "
			response.Write(sql)
		set rs = dbconn.execute(sql)
%>
<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#CCCCCC">
<%
	if not rs.eof then
		Do while not rs.eof
%>  
  <tr>
    <td width="120" bgcolor="#F2F2F2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5"><img src="cate/shopimg/s_arrow.gif" width="3" height="5"></td>
          <td><a href="cate/?<%=getString("HOMEPCATE="&rs("c_code"))%>"><b><%=rs("c_name")%>[<%=getGoodsCount(rs("c_code"))%>]</b></a><b></b></td>
        </tr>
      </table></td>
    <td bgcolor="#FFFFFF">
		<%
			sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,rs("c_code")) &"  order by c_sunse  "
				set rs2 = dbconn.execute(sql2)
				'response.Write(rs("c_code"))
				response.Write(" <font color=#D9D9D9>|</font> ")
					if not rs2.eof then
						Do while not rs2.eof
							
							response.Write("<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">" & rs2("c_name") &"["& getGoodsCount(rs2("c_code")) &"]"& "</a> <font color=#D9D9D9>|</font> ")
							
						rs2.movenext
						loop
					end if
				rs2.close
				set rs2 = nothing
		%>
	</td>
  </tr>
<%
		rs.movenext
		loop
	end if
	
	rs.close
	set rs = nothing

%>
</table>
<% End sub %>














<%
Sub navi_row2(byval HOMEPCATE)
	'// 상위 줄 출력 
	'// 홈 > 두번째 > 세번째
%>

<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td bgcolor="#F2F2F2"><%
		dim TempcateStep, i, sql, rs
		TempcateStep = getCateStep(HOMEPCATE)
		'response.Write getCateStep(HOMEPCATE)
		if getCateStep(HOMEPCATE) > 5 then TempcateStep = 5
		response.Write("<a href=?><b>홈</b></a> ")
		
		i = 0
		for i = 0 to TempcateStep 
			
			sql = "select * from "& tablename &" where c_code = '"& upCodeReturn(i, HOMEPCATE ) &"'   "
				
			set rs = dbconn.execute(sql)
				
				if rs.eof then
					
				else
					do while not rs.eof
						response.Write" > <a href=?"& getString("HOMEPCATE="&rs("c_code")) & "><b>" & rs("c_name") & "</b></a> "
						'response.Write(">"&rs("c_name"))
					rs.movenext
					loop
				end if
			
		next
	
	%></td>
  </tr>
  
<%
	'// 상단 카테고리 이하 메뉴출력
	
	
	dim sql2, rs2, rs3
	i = getCateStep(CateCode()) - 1
	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &"   order by c_sunse  "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else
			%>
  <tr>
    <td height="120" bgcolor="#FFFFFF">
			<%			
				Do while not rs2.eof
					
					response.Write "<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">" & rs2("c_name") 	
					
					'// 하위 상품 갯수 나타내는 식	
					response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝
					
					response.Write "</a> <font color=#D9D9D9>|</font> "
					
				rs2.movenext
				loop
			%>	
			
					
	</td>
  </tr>			
			<%
			end if
		rs2.close
		set rs2 = nothing
%>
	

</table>
<% 
End Sub
%>






<%
	'// 한줄 카테고리 이하 메뉴출력
	'| 하하하 | 호호호 | 히히히 | 케케케| <-이런 형식으로 출력
sub colNavi(byval cateStep)	
	dim sql2, rs2, rs3, i, bb, bb2
	
	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if
	
	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &"   and c_use = '1' order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else
		
				Do while not rs2.eof
					
					bb = ""
					bb2= ""
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb = "<b>"
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb2 = "</b>"
					
					
					'if int(CateCode()) = int(rs2("c_code")) then bb = "<b>"
					'if int(CateCode()) = int(rs2("c_code")) then bb2 = "</b>"
					
					'response.Write "<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">"& bb & rs2("c_name") 
					response.Write "<a href=?HOMEPCATE="&rs2("c_code") & ">"& bb & rs2("c_name") 			
					'// 하위 상품 갯수 나타내는 식	
					'response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝
					response.Write bb2 & "</a> <font color=#D9D9D9>|</font> "
					
				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub
%>






<%
	'// 한줄 카테고리 이하 메뉴출력
	'111
	'222
	'333
	'444
	'이런식으로 세로 출력
sub rowNavi(byval cateStep)	
	dim sql2, rs2, rs3, i, bb, bb2
	
	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if
	
	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &"   and c_use = '1' order by c_sunse  "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		'response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else
		
				Do while not rs2.eof
					
					bb = ""
					bb2= ""
					
					
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb = "<b>"
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb2 = "</b>"
					
					'response.Write "<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">"& bb & rs2("c_name")
					response.Write "<a href=?HOMEPCATE="&rs2("c_code") & ">"& bb & rs2("c_name") 			 			
					'// 하위 상품 갯수 나타내는 식	
					'response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝
					response.Write bb2 & "</a><br>"' <font color=#D9D9D9>|</font> "
					
				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub
%>


<%
	'// 한줄 카테고리 이하 메뉴출력
	'111
	'222
	'333
	'444
	'이런식으로 세로 출력
sub rowNavi_sub2(byval cateStep)	
	dim sql2, rs2, rs3, i, bb, bb2
	
	if len(cateStep) = 0 then
		i = getCateStep(CateCode()) - 1
	else
		i = cateStep - 2
	end if
	
	sql2 = "select * from "& tablename &" where "& CateThisWhere(i+1,CateCode()) &"   and c_use = '1' order by c_sunse  "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
		'response.Write(" <font color=#D9D9D9>|</font> ")
			if rs2.eof then

			else
		
				Do while not rs2.eof
					
					bb = ""
					bb2= ""
					
					
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb = "<b>"
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then bb2 = "</b>"
					
					'response.Write "<a href=?"& getString("HOMEPCATE="&rs2("c_code")) & ">"& bb & rs2("c_name")
					response.Write "<a href=?HOMEPCATE="&rs2("c_code") & ">"& bb & rs2("c_name") 			 			
					'// 하위 상품 갯수 나타내는 식	
					'response.Write "["& getGoodsCount(rs2("c_code")) &"]"
					'//-- 하위 갯수 끝
					response.Write bb2 & "</a><br>"' <font color=#D9D9D9>|</font> "
					
					'// 이하 메뉴가 있으면
					if upCodeReturn(cateStep,CateCode) = upCodeReturn(cateStep,rs2("c_code")) then
						response.Write "<table width='170'><tr><td width='20'></td><td>"
						call rowNavi(cateStep + 1)
						response.Write "</td width='150'></tr></table>"
					end if	
					
				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub
%>





<%
	'// 해당 카테고리 이름출력

sub ThisNavi(byval step, byval HOMEPCATE)
	dim sql2, rs2, rs3, i, bb, bb2
	
	
	sql2 = "select * from "& tablename &" where c_code = "& upCodeReturn(step,HOMEPCATE) &"   order by c_sunse asc "
		set rs2 = dbconn.execute(sql2)
		'response.Write(CateCode())
		'response.Write(sql2)
			if rs2.eof then

			else
		
				Do while not rs2.eof
					
					if getCateStep(HOMEPCATE) >= step then response.Write rs2("c_name") 	
					
					'// 하위 상품 갯수 나타내는 식	
					'response.Write getGoodsCount(rs2("c_code"))
					'//-- 하위 갯수 끝
					
					'response.Write bb2 & "</a> <font color=#D9D9D9>|</font> "
					
				rs2.movenext
				loop

			end if
		rs2.close
		set rs2 = nothing

End Sub
%>







<%

	'// 소비자가격과 판배가를 받아 표현
	'// 변경 문자열이 있는경우는 문자열로 표현
	
	Function MoneyView(byval customerMoney, byval Money, byval changString, byval changOption)
	
		if len(customerMoney) > 0 then	customerMoney = FormatCurrency(customerMoney)
		if len(Money) > 0 then			Money = FormatCurrency(Money)
		
		if changOption then
			MoneyView = "<font color=red>"&changString&"</font>"
		else
			MoneyView = "<s>"&customerMoney&"</s> <font color=CCAA00><b>"&Money&"</b></font>"
		end if
	
	End Function
	
	Function MoneyView2(byval customerMoney, byval Money, byval changString, byval changOption)
	
		if len(customerMoney) > 0 then	customerMoney = FormatCurrency(customerMoney)
		if len(Money) > 0 then			Money = FormatCurrency(Money)
		
		if changOption = 1 then
			MoneyView2 = changString
		else
			MoneyView2 = "<font color=CCAA00><b>"&Money&"원</b></font>"
		end if
	
	End Function
	

	
	'// 아이콘 보이기
	Function iconView(byval DBstr)
		dim temp, tempstr, i
		
		if len(trim(DBstr)) > 0 then	'// 값이 있는경우만 처리
			temp = split(DBstr,",")
		
			for i = 0 to Ubound(temp)
				if len(trim(temp(i))) > 0 then
					tempstr = tempstr & " <img src=/shopimg/icon/" &trim(temp(i))& ".gif> "
				end if
			next
		end if
		iconView = tempstr		
	End Function

%>




<%
	'// 상품보기 함수
	'// 상품 보기 모드인경우 상품보기로 처리하고 그 이하 처리 멈춤

	'// 경로가 없으면 기본 경로 사용
	'// 상품 리스트 상단에 정의 되어야 합니다.
	
Sub exeGoodView()	'//view파일 경로
	if request.QueryString("temp") = "view" then
		if len(url_GoodsView) = 0 or isnull(url_GoodsView) then	url_GoodsView = "/default/view.asp"
		

		dim objScrFso
		Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
			If objScrFso.FileExists(Server.MapPath(url_GoodsView)) Then 
				Server.Execute(url_GoodsView)
			Else
				response.Write(url_GoodsView)
				Response.Write " 파일이 존재하지 않습니다. 경로를 확인해 주세요."
			End If

		Set objScrFso = Nothing
		
		response.End()
	end if
	'// 상품 보기 끝
End Sub
%>









<%
	'//***************************************
	'// 카테고리 상단 이미지 처리
	'//***************************************
	
	Sub cateVisualimg(byval DBimg)
		
		dim filepath, fileExe
		filepath = getFileNumName(DBimg)
		'fileExe
		if len(filepath) > 0 then '//파일명 있으면
		
			if ubound(split(filepath,".")) > 0 then
				fileExe = split(filepath,".")(1)
				
				'// 파일 확장자로 해당 파일 처리
				if lcase(fileExe) = "gif" or lcase(fileExe) = "jpg" then
					response.Write "<img src=/upload/"& filepath &">"
				elseif lcase(fileExe) = "swf" then
					%>
					<script language="javascript" src = "/shop/upswf.js"--></script>
					<script language="javascript">FlashMainbody('<%=filepath%>',282,598);</script>
					<%
				end if
				
			end if
		
		end if
		
	End Sub
%>






<%
	'//***************************************
	'// 쇼핑몰 상품 보기 리스트
	'//***************************************
	
	Sub contectView(byval HOMEPCATE)
		
		dim sql, rs, img
		
		img 	= getAdoRsScalar("select c_img from "& tablename &" where c_code = '"& HOMEPCATE &"'  ")
		if len(img) > 0  then call cateVisualimg(img)
		
		
		sql 	= "select * from "& tablename &" where c_code = '"& HOMEPCATE &"'  "
		set rs = dbconn.execute(sql)
		
		if not rs.eof then

			if Ucase(rs("content_mode")) = "D" then '// 디자인페이지 즉 html이면
				response.Write rs("content_html")
			elseif Ucase(rs("content_mode")) = "M" then '//모듈이면
				'// 각 프로그램에 대한 함수 호출
				
			elseif Ucase(rs("content_mode")) = "E" then '//execute 즉 파일 실행이면
				dim objScrFso
				Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
					If objScrFso.FileExists(Server.MapPath(url)) Then 
						Server.Execute(url)
					Else
						response.Write(url)
						Response.Write " 파일이 존재하지 않습니다. 경로를 확인해 주세요."
					End If
		
				Set objScrFso = Nothing
			end if
			
			
		end if
		
		rs.close
		set rs = nothing
	End Sub












	'//사진 보이기
	
	Function getImgView(byval intSize, byval strFile)
		dim filepath
			filepath = getFileNumName(strFile)
			if len(filepath) > 0 then
				filepath = "<img src=/upload/"& filepath &" width="& intSize &" height="& intSize &" >"
			else
				filepath = "<img src=/shop/goods/noimg.gif width="& intSize &" height="& intSize &" >"
			end if
			
	
		getImgView = getImgView & "<table width="&intSize&" border=0 cellpadding=1 cellspacing=1 bgcolor=#CCCCCC><tr>"
		getImgView = getImgView & "<td bgcolor=#FFFFFF>"&filepath&"</td></tr></table>"
	End Function











''// 홈페이지 카테고리 데이터
		dim c_name 				
		dim c_img 				
		dim menuimg 			
		dim menuimg_over 		
		dim menuimg_select 		
		dim data_orignal 		
		dim data_memo 			
		dim data_Wday 			
		dim data_userip 		
		dim data_planer 		
		dim orderMode 			
		dim content_mode 		
		dim content_html 		
		dim content_modul 		
		dim content_modulvalues
		dim content_execute 	
		dim content_redirect	
		dim content_link
		dim c_use
		dim pagelevel
		Dim c_strImg
		
sub ValuesSetting()
	'// 현재 파일을 불러옵니다.
	dim sql, rs
	sql = "select * from "& tablename &" where c_code = '"& CateCode() &"' "
	set rs = dbconn.execute(sql)
	'response.Write(sql)
	if not rs.eof then
		
		c_name 					= rs("c_name")
		c_img 					= rs("c_img")
		menuimg 				= rs("menuimg")
		menuimg_over 			= rs("menuimg_over")
		menuimg_select 			= rs("menuimg_select")
		data_orignal 			= rs("data_orignal")
		data_memo 				= rs("data_memo")
		data_Wday 				= rs("data_Wday")
		data_userip 			= rs("data_userip")
		data_planer 			= rs("data_planer")
		orderMode 				= rs("orderMode")
		content_mode 			= rs("content_mode")
		content_html 			= rs("content_html")
		content_modul 			= rs("content_modul")
		content_modulvalues		= rs("content_modulvalues")
		content_execute 		= rs("content_execute")
		content_redirect		= rs("content_redirect")
		content_link			= rs("content_link")
		c_use					= rs("c_use")
		pagelevel				= rs("pagelevel")
		c_strImg				= rs("strImg")
		
		
		'response.Write("실행됨")
		'response.Write content_html
	end if
	rs.close
	set rs = nothing
end sub





Function b_Checked(byval strA, byval strB)
	if Ucase(Trim(strA)) = Ucase(Trim(strB)) then b_Checked = "Checked"
End Function

Function b_Selected(byval strDB, byval strB)
	if Ucase(strDB) = Ucase(strB) then 
		b_Selected = " value='"& strB &"'  selected='selected' "
	else
		b_Selected = " value='"& strB &"' "
	end if
End Function




Sub ContentPage()	'//모드,디비저장값,변수값
	'//++++++++++++++++++++++++++++++++++++++++++++
	'// 시뮬레이션 용입 즉 벨루값이 먹지 않음
	'//++++++++++++++++++++++++++++++++++++++++++++
	
	
	'//ValuesSetting()가 선언된 상태에서만 사용가능
	'response.Write(content_mode)
	dim smulmode : smulmode = request.QueryString("smulmode")
	if ucase(content_mode) = "D" then		'//Design	디자인 페이지, 즉 html
		'response.Write(dbdata)
		IF LEN(DATA_WDAY) > 0 then
			response.Write data_orignal
		elseif smulmode = "editor" then
			 call setEditor("data_orignal",data_orignal)
		else
			response.Write data_orignal
		end if
	elseif ucase(content_mode) = "M" then	'//Modul 아반모듈
		'response.Write(content_modul)
		'//모듈 보이기
		'//content_modulvalues <--사용자가 지정한 모듈 변수 값
		'if lcase(content_modul) = "board" then
		 	'call board(content_modulvalues)
		
		'//모듈구분값에 값이 있으면 get방식으로 값을 넘긴다.
		IF instr(request.ServerVariables("QUERY_STRING"),content_modulvalues)=0 THEN	
			'//모듈구분값에 값이 있으면 get방식으로 값을 넘긴다.
			if len(content_modulvalues) > 0 then response.redirect("?"&getString(content_modulvalues))
		END IF
		
		content_modul = trim(content_modul)
		content_modul = "/avanplus/modul/"&content_modul&"/call.asp"
		server.Execute(content_modul)
		
		
				
		
	elseif ucase(content_mode) = "E" then	'//Execute	
		dim objScrFso
		Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
			If objScrFso.FileExists(Server.MapPath(content_execute)) Then 
				Server.Execute(content_execute)
			Else
				Response.Write "실행하려는 파일이 존재하지 않습니다. 파일의 경로를 확인해 주세요."
			End If
		Set objScrFso = Nothing
	elseif ucase(content_mode) = "R" then	'//redirect
		response.Redirect(content_redirect)
	elseif ucase(content_mode) = "L" then	'//LINK 새창 띄우기
		response.Write "해당메뉴를 클릭하면 새창이 뜹니다."
		%>
		<BODY onload='window.open("<%=content_link%>", "", "height=600, width=950, left=0, top=0, toolbar=1, location=1,directories=1,status=1,menuBar=1,scrollBars=1,resizable=1");'></BODY>
		<%
	elseif ucase(content_mode) = "S" then	'//sub 서브 첫페이지로 이동
		'response.Write ("select top 1 c_code from "& tablename &" where "& CateNextWhere() &"  order by c_sunse  ")
		response.Redirect "?"&getString("HOMEPCATE="&getAdoRsScalar("select top 1 c_code from "& tablename &" where "& CateNextWhere() &"  order by c_sunse asc "))
	end if

End Sub




Sub ContentPring(byval content_mode, byval data_orignal, byval content_execute, byval content_redirect, byval content_link)	'//모드,디비저장값,변수값
	'//++++++++++++++++++++++++++++++++++++++++++++
	'// 시뮬레이션 용입 즉 벨루값이 먹지 않음
	'//++++++++++++++++++++++++++++++++++++++++++++
	
	
	'//ValuesSetting()가 선언된 상태에서만 사용가능
	'response.Write(content_mode)
	'dim smulmode : smulmode = StrMode
	if ucase(content_mode) = "D" then		'//Design	디자인 페이지, 즉 html
		'response.Write(dbdata)
		response.Write data_orignal

	elseif ucase(content_mode) = "M" then	'//Modul 아반모듈
		response.Write("모듈로 연동되는 프로그램이 들어갑니다.<br>")
		response.Write("모듈로 제공되는 프로그램은 디자인 수정이나 기능 변경이 불가능 합니다.<br>")
		response.Write("별도의 특별한 프로그램이 필요하시면 별도 프로그램 계약을 하셔야 합니다.<br>")
		response.Write("모듈로 제공되는 프로그램은 홈페이지 시뮬레이션에서 그 기능및 형태를 확인하실수 있습니다.")
	elseif ucase(content_mode) = "E" then	'//Execute	
		response.Write(content_execute&"의 해당 경로 파일을 불러옵니다.")
		

	elseif ucase(content_mode) = "R" then	'//redirect
		response.Write(content_redirect&"로 페이지를 이동합니다.")
		
	elseif ucase(content_mode) = "L" then	'//LINK 새창 띄우기
		response.Write(content_link&"의 페이지를 새창으로 띄웁니다.")
	
	elseif ucase(content_mode) = "S" then	'//sub 서브 첫페이지로 이동
		response.Write("현재 페이지는 내용이 없습니다. 하위 첫메뉴로 이동합니다.")
	
	end if

End Sub


'//빌더 설정값 쓰기
Function Bprint(byval colum)
	Bprint = getAdoRsScalar("select "&colum&" from Builder_set where intSeq = 1")
End Function
%>












