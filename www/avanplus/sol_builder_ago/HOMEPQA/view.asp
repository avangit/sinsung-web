<!--#include file="config.asp"-->
<%
	'/// 권한 설정
	if session("userlevel") < 9 then ErrorBack("회원전용입니다. 로그인후 이용해 주세요.")
	
	
intSeq = request.QueryString("intSeq")


Call idxdata(intSeq)
%>
<span id="printArea"></span>
<form action="?<%=getstring(Callpage("update",""))%>" method="post" enctype="multipart/form-data" name="frmRequestForm" id="frmRequestForm" style="margin:0px;" onsubmit="return frmRequestForm_Submit(this);">
  <input type="hidden" name="intseq" value="<%=intseq%>" />
  <table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td width="100" bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>문의제목</b></td>
      <td bgcolor="#FFFFFF"><b><%=strTitle%></b></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>의뢰회사명</b></td>
      <td bgcolor="#FFFFFF"><%=strCompany%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>제작유형</b></td>
      <td bgcolor="#FFFFFF"><%=strSelect%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>제작목적</b></td>
      <td bgcolor="#FFFFFF"><%=strMok%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>제작언어</b></td>
      <td bgcolor="#FFFFFF"><%=strChack%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>프로그램언어</b></td>
      <td bgcolor="#FFFFFF"><%=strRadio%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b>&nbsp;예상페이지수</b></td>
      <td bgcolor="#FFFFFF"><%=strStart%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b>&nbsp;서버유무</b></td>
      <td bgcolor="#FFFFFF"><%=strServer%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b>&nbsp;도메인주소</b></td>
      <td bgcolor="#FFFFFF">http://<%=strDomain%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b>&nbsp;참고사이트</b></td>
      <td bgcolor="#FFFFFF"><%=strDomain2%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b>&nbsp;자료준비</b></td>
      <td bgcolor="#FFFFFF"><%=strData%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>문의내용</b></td>
      <td bgcolor="#FFFFFF"><%=strcontent%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>담당자명/직책</b></td>
      <td bgcolor="#FFFFFF"><%=strName%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>회신연락처</b></td>
      <td bgcolor="#FFFFFF"><b><%=strTel%></b></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>이메일</b></td>
      <td bgcolor="#FFFFFF"><!--include virtual = "/select.asp"-->
      <%=strEmail%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b>&nbsp;핸드폰</b></td>
      <td bgcolor="#FFFFFF"><%=strHp%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000"> &nbsp;</font></strong>의뢰인지역</b></td>
      <td bgcolor="#FFFFFF"><%=strAddr%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>제작예산</b></td>
      <td bgcolor="#FFFFFF"><b><%=strMoney%>
      만원</b></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b><strong><font color="#FF0000">*</font></strong>제작포인트</b></td>
      <td bgcolor="#FFFFFF"><%=strPoint%></td>
    </tr>
    <tr>
      <td bgcolor="#F5F5F5"><b>&nbsp;첨부파일1</b></td>
      <td bgcolor="#FFFFFF"><% call FileDownLink_uppath(strImage,UploadFolder)%>
        <input type = "file" name = "strimage" /></td></tr>
    <tr>
      <td bgcolor="#F5F5F5"><b>&nbsp;첨부파일2</b></td>
      <td bgcolor="#FFFFFF"><% call FileDownLink_uppath(strImage2,UploadFolder)%>
        <input name = "strimage2" type = "file" id="strimage2" /></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFCC"><b>+처리결과</b></td>
      <td bgcolor="#FFFFFF"><% Call Form_CheckBoxPrint("strChack",strChack, "", "확인,부재중,전화상담,방문원함,방문미팅,견적서발송,자료발송,거짓자료", ",", 8, 0) %></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFCC"><b>+알람/기록</b></td>
      <td bgcolor="#FFFFFF"><input type="text" name="strChkDate" value="<%=strChkDate%>" maxlength="10" style="width:80;" />
      <input type="text" name="strChkDateMsg" maxlength="50" style="width:300;" /></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFCC"><b>+알람/기록</b></td>
      <td bgcolor="#FFFFFF"><%=strChkDateMsg%></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFCC"><b>+고객반응</b></td>
      <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strStepRe",strStepRe, "", "상담만 원하는거 같음,차후문의,적극적,계약유력", ",", 5, 0) %></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFCC"><b>+답변</b></td>
      <td bgcolor="#FFFFFF"><% call setFCKeditor("strRe", strRe, "", 200) %></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFCC"><b>+발송</b></td>
      <td bgcolor="#FFFFFF"><input name="sms" type="checkbox" id="sms" value="1" />
        답변내용 SMS발송<br />
        <input name="mail" type="checkbox" id="mail" value="1" />
        답변내용 메일발송[미리보기] </td>
    </tr>
  </table>
  <div style="text-align:center; padding:10px;">
    <input name="button" type="button" style="border:1px solid #CCCCCC;background-color:#F5F5F5;" onclick="this.form.onsubmit();" value=" 업데이트 " />
    <input name="button" type="button" style="border:1px solid #CCCCCC;background-color:#F5F5F5;" onclick="self.location.href = '?<%=getString(Callpage("list",""))%>';" value=" 뒤로 " />
    <span style="text-align:center;padding:10px;">
     <input name="button2" type="button" style="border:1px solid #CCCCCC;background-color:#F5F5F5;" onclick="self.location.href = '?<%=getString(Callpage("form","intSeq="&intSeq))%>';" value=" 수정 " />
    <input name="button2" type="button" style="border:1px solid #CCCCCC;background-color:#F5F5F5;" onclick="return really();" value=" 삭제 " />
    <input name="button2" type="button" style="border:1px solid #CCCCCC;background-color:#F5F5F5;" onclick="self.location.href = '?<%=getString(Callpage("List","intSeq="))%>';" value=" 목록 " />
    <%If Len(Request.QueryString("Print")) = 0 Then%>
    <input name="button2" type="button" style="border:1px solid #CCCCCC;background-color:#F5F5F5;" onclick="PrintContent();" value=" 인쇄하기 " />
    <%Else%>
    <input name="button2" type="button" style="border:1px solid #CCCCCC;background-color:#F5F5F5;" onclick="window.print();" value=" 인쇄하기 " />
    <%End If%>
    </span></div>
</form>


<script language="javascript">
<!--
	function really(){
		if (confirm('\n정말로 삭제하시겠습니까? 삭제는 복구가 불가능합니다.\n')) {
			self.location.href = '?<%=getString(Callpage("Delete","intSeq="&intSeq))%>';	
		}
	}
	
	function OpenWindow(url,w,h,scrollbar) {
		if (scrollbar=="") scrollbar=0;
		//window.open(url,"ICCwindow","width=400,height=400,status=no,scrollbars=no");
		lPos = (screen.width) ? (screen.width-w)/2 : 0; tPos = (screen.height) ? (screen.height-h)/2 : 0;
		
		window.open(url, "", "height="+h+", width="+w+", left="+lPos+", top="+tPos+", toolbar=0, location=0,directories=0,status=0,menuBar=0,scrollBars="+scrollbar+",resizable=0");
	}

	function PrintContent(){	
		var mm = window.open("/AVANplus/Modul/<%=ProgramFolderName%>/view.asp?<%=getString("Print=y")%>","print_win","width=550,height=600");
		var doc = mm.document;		
		doc.focus();
	}	
	
//-->
</script>
<SCRIPT Language="Javascript">
<!--
var NS = 1;
if( document.all) NS = 0;

function PrintOption(type1,type2){
//네스케이프일 경우
        if (NS) {
                window.print();
//익스플로러일 경우
        } else {
                var active = '<OBJECT ID="active1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
                document.body.insertAdjacentHTML('beforeEnd', active);
                active1.ExecWB(type1,type2); 
                active1.outerHTML ="";
        }
}
//-->
</script>


<script language="javascript">
<!--

	function frmRequestForm_Submit(frm){
		//if ( frm.strTitle.value.replace(/ /gi, "") == "" ) { alert("제목을 입력해주세요"); frm.strTitle.focus(); return false; }

		//if ( frm.strContent.value.replace(/ /gi, "") == "" ) { alert("의뢰인을 입력해주세요"); frm.strContent.focus(); return false; }
		frm.submit();
	}

//-->
</script>