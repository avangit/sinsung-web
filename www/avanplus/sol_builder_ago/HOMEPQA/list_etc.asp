<!--#include file="config.asp"-->
<%
	
	
	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'## 
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'## 
	
	dim intTotalCount, intTotalPage
	
	dim intNowPage			: intNowPage 		= Request.QueryString("page")    
    dim intPageSize			: intPageSize 		= 6
    dim intBlockPage		: intBlockPage 		= 10

	dim query_filde			: query_filde		= "*"
	dim query_Tablename		: query_Tablename	= " mTb_Product_v1"
	dim query_where			: query_where		= ""
	dim query_orderby		: query_orderby		= " order by intSeq DESC "
	
	strCategory = request.QueryString("strCategory")
	
	
	If IsValue(strCategory) Then
		query_where = " strCategory = '" & strCategory & "' "		
	End If

	call intTotal

	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'## 
	'## 
	'#############################################################################

	dim sql, rs
	sql = GetQuery()
	
	call dbopen
	set rs = dbconn.execute(sql)
	
	dim pagei : pagei = (intTotalCount-MoveCount)
	'response.Write(sql)
	'response.end
%>
<script language="javascript">
<!--
	//작은 이미지 클릭헀을때 큰이미지로 ..
	//큰이미지 밑에 타이틀까지..나오기..
	function ChangeImage(src, w, h,title)
	{
		document.getElementById("BigPhotoImg").src = src;
		
		img = new Image();
		img.src = document.getElementById("BigPhotoImg").src;
		
		document.getElementById("BigPhotoImg").style.width = w;
		document.getElementById("BigPhotoImg").style.height = h;
		document.getElementById('strTitle').innerHTML = title;
	}
//-->
</script>

<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td valign="top" width = "373">
			<!--큰 이미지 start-->
				<table border="0" cellspacing="0" cellpadding="0"  >
				<tr>
					<td background = "/AVANplus/Modul/Product_v1/img/big_bg.gif" width = "373" height = "305" align="center">
						<%
						if  rs.eof then					
								response.write "등록된 이미지가 없습니다."				
						Else
							Dim STRSQL2,intcount,strsql1
							
							strsql = "select top 1 a.strimage,a.strTitle,a.intseq  from (SELECT top "&pagei&" strimage,strTitle,intseq FROM mTb_Product_v1 where  "&query_where&" order by intseq asc ) a  order by intseq desc"
							
							ArrData =getAdoRsArray(strsql)
							'response.write STRSQL2
							If MoveCount >  0 Then			
								strimage =	ArrData(0,0)
								strTitle =  ArrData(1,0)
								intseq   =  ArrData(2,0)								
							
						%>
							<img src="<%=UploadFolder&strimage%>" width="350" height="250" ID ="BigPhotoImg">							
							<div ID ="strTitle"><%=strTitle%></div>
							<%Else%>
							<img src="<%=UploadFolder&rs("strimage")%>" width="350" height="250" ID ="BigPhotoImg">							
							<div ID ="strTitle"><%=rs("strTitle")%></div>
							<%End IF%>
						<%End If%>				
					</td>
				</tr>
			</table>
			<!--큰 이미지 end-->
		</td>
		<td width="2"></td>
		<td valign="top">
			<!--작은 이미지 start-->
			<table border="0" cellspacing="0" cellpadding="0" width="100%"><tr>
				<tr>	
					<td valign="top" height="300">
						<table border="0" cellspacing="0" cellpadding="0">						
							<tr>
							<%
							'Dim pagei : pagei = (intTotalCount-MoveCount)
							'// 글이 없을 경우
							If  rs.eof Then					
								response.write "<td background = '/AVANplus/Modul/Product_v1/img/small_bg.gif' width = '106' height = '86' align='center'>No Image</td></tr>"			
							'// 글이 있는 경우	
							Else
								rs.move MoveCount 
								Dim i : i = 0
								Do Until rs.eof 
								%>
									<td background = "/AVANplus/Modul/Product_v1/img/small_bg.gif" width = "106" height = "86" align="center">
									<img src = "<%=UploadFolder&rs("strimage")%>" width = "88" height = "60" align="center" onclick="ChangeImage(this.src,'350','250','<%=rs("strtitle")%>');" style="cursor:hand">
									<%
									'//한줄에 2개가 출력되므로 
									If i < 1  and i <> "" Then		
										i = i + 1			
										response.write "</td><TD WIDTH = '1'></TD>"									
									Elseif i = 1 and i <> "" Then										
										response.write "</td><TD WIDTH = '1'></TD></tr><tr><td colspan='4' height='10'></td></tr><tr>"										
										i = 0
									End If
									rs.movenext
								Loop
							End If
							%>								
							</tr>
							<tr>
								<td height="3"></td>
							</tr>
							
						</table>
					</td>
				</tr>		
				<tr>
					<td align="center"><%Paging("")%></td>
				</tr>
				<%
				rs.close()
				set rs = nothing
				Call Dbclose()%>		
			</table>			
			<!--작은 이미지 end-->
		</td>
	</tr>
</table>
