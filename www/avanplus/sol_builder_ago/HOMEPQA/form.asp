<!--#include file="Config.asp"-->

<%
Dim strAction

intSeq = request.QueryString("intSeq")

Dim strPageMode : strPageMode = "update"
	
	Call idxdata(intSeq)
	
	'action처리부분
	
	If Len(intSeq) > 0 Then
		strAction = getstring(Callpage("update",""))	'//수정
	Else			
		strAction = getstring(Callpage("insert",""))	'//등록
	End If


%>

<form name="frmRequestForm" method="post" onsubmit="return frmRequestForm_Submit(this);" action="?<%=strAction%>" style="margin:0px;" enctype="multipart/form-data">

<input type="hidden" name="intseq" value="<%=intseq%>">

<table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">
	<tr>
      <td width="100" bgcolor="#ECFFF1"><b><strong><font color="#FF0000">*</font></strong>문의제목</b></td>
	  <td bgcolor="#F7FFFF"><input type="text" name="strTitle" value="<%=strTitle%>" maxlength="50" style="width:50%;" /></td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b><strong><font color="#FF0000">*</font></strong>의뢰회사명</b></td>
	  <td bgcolor="#F7FFFF"><input type="text" name="strCompany" value="<%=strCompany%>" maxlength="50" style="width:200;" /></td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b>&nbsp;도메인주소</b></td>
	  <td bgcolor="#F7FFFF">http://
	    <input type="text" name="strDomain" value="<%=strDomain%>" maxlength="50" style="width:150;" />
	    *도메인이 있으시면 입력해주세요. </td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b><strong><font color="#FF0000">*</font></strong>문의내용</b></td>
	  <td bgcolor="#F7FFFF"><% call setFCKeditor("strcontent", strcontent, "", 300) %></td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b>&nbsp;첨부파일</b></td>
	  <td bgcolor="#F7FFFF">#1
	    <input type = "file" name = "strimage" />
	    #2
        <input type = "file" name = "strimage2" /></td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b><strong><font color="#FF0000">*</font></strong>담당자명/직책</b></td>
	  <td bgcolor="#F7FFFF"><input type="text" name="strName" value="<%=strName%>" maxlength="50" style="width:100;" /></td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b><strong><font color="#FF0000">*</font></strong>회신연락처</b></td>
	  <td bgcolor="#F7FFFF"><input type="text" name="strTel" value="<%=strTel%>" maxlength="50" style="width:200;" /></td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b><strong><font color="#FF0000">*</font></strong>이메일</b></td>
	  <td bgcolor="#F7FFFF"><!--include virtual = "/select.asp"-->
          <% Call Form_EmailPrint("strEmail",strEmail)%></td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b><strong><font color="#FF0000">*</font></strong>제작예산</b></td>
	  <td bgcolor="#F7FFFF"><input type="text" name="strMoney" value="<%=strMoney%>" maxlength="50" style="width:100;" />
      만원</td>
    </tr>
	<tr>
      <td bgcolor="#ECFFF1"><b>&nbsp;핸드폰</b></td>
	  <td bgcolor="#F7FFFF"><% Call Form_HandphonePrint("strHp",strHp)%>
          <br />
          <font color="#FF0000">+ 핸드폰을 입력하시면 처리상황이 문자로 발송됩니다.</font></td>
    </tr>
</table>
<div align="center"><br />
    <span style="text-align:center; padding:10px;">
    <input name="button" type="button" style="border:1px solid #000000; color:#FFFFFF; width:120; height:30; background-color:#336699;" onclick="this.form.onsubmit();" value="제 작 문 의" />
    </span><br />
  <br />
</div>
<table width="100%"  border="0" cellpadding="7" cellspacing="1" bgcolor="#CCCCCC">
	<tr>
      <td bgcolor="#FFFFE6"><b><strong><font color="#FF0000"> &nbsp;</font></strong><font color="#990000">추가정보</font></b></td>
	  <td bgcolor="#FFFFFF"><font color="#0000FF">* 아래 내용을 등록하시면 더욱 상세한 상담및 견적이 가능합니다. </font></td>
	</tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;</strong>제작포인트</b></td>
	  <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strPoint",strPoint, "", "최저가제작,예산범위안에서제작,일반수준 퀄리티,고퀄리티작업-디자인중요", ",", 4, 0) %></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;</strong>제작유형</b></td>
	  <td bgcolor="#FFFFFF"><% Call Form_SelectPrint("strSelect",strSelect, "선택해주세요", "", "홈페이지,쇼핑몰,리뉴얼,프로그램,시안작업,디자인개발,프로그램개발", ",", "")%></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;</strong>제작목적</b></td>
	  <td bgcolor="#FFFFFF"><% Call Form_CheckBoxPrint("strMok",strMok, "", "온라인 마케팅강화,고객과의 커뮤니케이션,신규시장진입,사이트 리뉴얼,운영관리 편의,해외시장진출,정보제공,제품판매(전자상거래)", ",", 4, 0) %></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;</strong>제작언어</b></td>
	  <td bgcolor="#FFFFFF"><% Call Form_CheckBoxPrint("strChack",strChack, "", "국문,영문,일문,중문,기타언어", ",", 5, 0) %></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;</strong>프로그램언어</b></td>
	  <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strRadio",strRadio, "", "무관,ASP,JSP,PHP,.net,기타개발언어", ",", 6, 0) %></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;참고사이트</b></td>
	  <td bgcolor="#FFFFFF"><input type="text" name="strDomain2" value="<%=strDomain2%>" maxlength="50" style="width:100%;" /></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;예상페이지수</b></td>
	  <td bgcolor="#FFFFFF"><% Call Form_NumberSelectPrint("strStart",strStart, "선택해주세요", 0, 100, "") %>
	    Page</td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;서버유무</b></td>
	  <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strServer",strServer, "", "없음,윈도우서버,리눅스서버,기타서버", ",", 4, 0) %></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b>&nbsp;자료준비</b></td>
	  <td bgcolor="#FFFFFF"><% Call Form_RadioPrint("strData",strData, "", "없음,자료모으는중,자료만있음,기획자료있음", ",", 4, 0) %></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFE6"><b><strong><font color="#FF0000"> &nbsp;</font></strong>의뢰인지역</b></td>
	  <td bgcolor="#FFFFFF"><input type="text" name="strAddr" value="<%=strAddr%>" maxlength="50" style="width:50%;" /></td>
    </tr>	
</table>

<div style="text-align:center; padding:10px;">
  <input name="button2" type="button" style="border:1px solid #000000; color:#FFFFFF; width:120; height:30; background-color:#336699;" onclick="this.form.onsubmit();" value="제 작 문 의" />
   <input type="button" value=" 뒤로 " onclick="self.location.href = '?<%=getString(Callpage("list",""))%>';" style="border:1px solid #000000; color:#000000; width:120; height:30; background-color:#CCCCCC;">
</div>

</form>

<script language="javascript">
<!--

	function frmRequestForm_Submit(frm){
		if ( frm.strTitle.value.replace(/ /gi, "") == "" ) { alert("제목을 입력해주세요"); frm.strTitle.focus(); return false; }
		if ( frm.strName.value.replace(/ /gi, "") == "" ) { alert("담당자명/직책을 입력해주세요"); frm.strName.focus(); return false; }
		if ( frm.strTel.value.replace(/ /gi, "") == "" ) { alert("연락받을 회신연락처를 입력해주세요"); frm.strTel.focus(); return false; }
		if ( frm.strMoney.value.replace(/ /gi, "") == "" ) { alert("제작예산을 입력해주세요"); frm.strMoney.focus(); return false; }
		
		frm.submit();
	}

//-->
</script>