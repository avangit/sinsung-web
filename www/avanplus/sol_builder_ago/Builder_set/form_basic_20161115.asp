<!--#include file="_config.asp"-->
<% call dbopen %>


<form name="frmRequestForm" method="post" action="?<%=getString(Callpage("proc",""))%>"  enctype="multipart/form-data">

<div class="contents">
   <table class="tableWrite" width="100%"  cellpadding="0" cellspacing="0">

    <colgroup>
      <col style="width:150">
      <col style="width:85%">
      </colgroup> 
  <tr>
    <th class="w_weight">일반정보세팅</th>
    <td class="t_blue">기본정보를 등록해 주세요.</td>
  </tr>
  <tr>
    <th>홈페이지명</th>
    <td><input type="text" name="str_SiteName" value="<%=oBprint("str_SiteName")%>" maxlength="50" class="w20 input01" />
      &lt;%bPrint(&quot;sitename&quot;)%&gt;</td>
  </tr>
  <tr>
    <th>홈페이지 주도메인</th>
    <td>http://
      <input type="text" name="str_Domain" value="<%=oBprint("str_Domain")%>" maxlength="50" class="w20 input01"/>
      &lt;%bPrint(&quot;domain&quot;)%&gt;</td>
  </tr>
  <tr>
    <th>타이틀</th>
    <td><input type="text" name="str_Title" value="<%=oBprint("str_Title")%>" maxlength="50" class="w40 input01" />&lt;%bPrint(&quot;title&quot;)%&gt;</td>
  </tr>
  <tr>
    <th>카피라이트</th>
    <td><input type="text" name="textCopy" value="<%=oBprint("textCopy")%>" maxlength="50"  class="w100 input01"/>
  </td>
  </tr>
  <tr>
    <th>홈페이지상태</th>
    <td><% Call Form_RadioPrint("str_SiteActive",oBprint("str_SiteActive"), "1,2,3", "공사중,운영중,로그인화면", ",", 3, 0) %></td>
  </tr>
</table>
</div>  





  <br />
  
  
<div class="contents">
   <table class="tableWrite" width="100%"  cellpadding="0" cellspacing="0">

    <colgroup>
      <col style="width:150">
      <col style="width:85%">
      </colgroup> 

    <tr>
      <th>메일환경세팅</th>
      <td class="t_blue">시스템에서 자동 발송되는 메일 설정 값</td>
    </tr>
    <tr>
      <th>관리자이메일</th>
      <td><% Call Form_EmailPrint("str_adminMail",oBprint("str_adminMail"))%>
        &lt;%bPrint(&quot;adminMail&quot;)%&gt;</td>
    </tr>
    <tr>
      <th>메일발송시 이름 </th>
      <td><input type="text" name="str_MailSendName" value="<%=oBprint("str_MailSendName")%>" maxlength="50" class="w20 input01" />
        &lt;%bPrint(&quot;MailSendName&quot;)%&gt;</td>
    </tr>
    <tr>
      <th>고객센터전화번호</th>
      <td><input type="text" name="str_CSTel" value="<%=oBprint("str_CSTel")%>" maxlength="50" class="w20 input01" />
        &lt;%bPrint(&quot;CSTel&quot;)%&gt;</td>
    </tr>
  </table>
  </div> 
  <br />
  
  
  
<div class="contents">
   <table class="tableWrite" width="100%"  cellpadding="0" cellspacing="0">

    <colgroup>
      <col style="width:150">
      <col style="width:85%">
      </colgroup> 

    <th colspan="8" class="w_weight"> **관리자 메모** adminMemo </th>
  </tr>
  
   <tr>
      <td class="pr20">
       <textarea class="w100  h300px input02" name="str_adminMemo"><%=oBprint("str_adminMemo")%></textarea>
      </td>
  </tr> 
  </table>
</div>

  
  
  <center class="btnWrap">
    <br />
    <input type="submit" value=" 저 장 " class="btnDB btnS">
    
    
  </center>
</form>

<% call dbclose %>