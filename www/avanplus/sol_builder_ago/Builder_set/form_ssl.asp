<!--#include file="_config.asp"-->
<% call dbopen %>


<form name="frmRequestForm" method="post" action="?<%=getString(Callpage("proc",""))%>" style="margin:0px;" enctype="multipart/form-data">

<div class="contents">
   <table class="tableWrite" width="100%"  cellpadding="0" cellspacing="0">

    <colgroup>
      <col style="width:15%">
      <col style="width:85%">
      </colgroup>  
  
    <tr>
      <th>SSL 사용 </td>
      <td><% Call Form_RadioPrint("str_sslUse",oBprint("str_sslUse"), "1,0", "예(1),아니오(0)", ",", 2, 0) %>
       <span class="t_blue"> SSL은 별도로 신청하여야만 사용이 가능합니다. (문의:1544-7098) str_sslUse</span></td>
    </tr>
    <tr>
      <th>SSL 종류/메모 </th>
      <td><input name="str_sslType" type="text" id="str_sslType" class="w50 input01" value="<%=oBprint("str_sslType")%>" maxlength="50" />
      str_sslType</td>
    </tr>
    <tr>
      <th>인증씰 설치 코드 </th>
      <td><textarea name="str_sslCode" rows="5" id="str_sslCode" class="w50 input02"><%=oBprint("str_sslCode")%></textarea> str_sslCode
     </td>
    </tr>
    <tr>
      <th>SSL경로</th>
      <td>https://
        <input name="str_sslUrl" type="text" id="str_sslUrl" class="w40 input01" value="<%=oBprint("str_sslUrl")%>" maxlength="50" />
      str_sslUrl</td>
    </tr>
  </table>
  
</div>  

<ul class="btnWrap">
	<li><input type="submit" name="Submit"  value=" 저장 " class="btn_m c_blue"></li>
</ul>
</form>

<% call dbclose %>
