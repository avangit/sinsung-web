<!--#include file = "_config.asp"-->
<%
Dim strAction

intSeq = request.QueryString("intSeq")

Dim strPageMode : strPageMode = "update"
	
	Call idxdata(intSeq)
	
	'action처리부분
	
	If Len(intSeq) > 0 Then
		strAction = getstring(Callpage("update",""))	'//수정
	Else			
		strAction = getstring(Callpage("insert",""))	'//등록
	End If


%>
<script language="javascript">
<!--

	function frmRequestForm_Submit(frm){
		if ( frm.strTitle.value.replace(/ /gi, "") == "" ) { alert("상호/단체명을 입력해주세요"); frm.strTitle.focus(); return false; }
		<%If Len(intSeq) = 0 Then%>
		//if ( frm.strimage.value.replace(/ /gi, "") == "" ) { alert("이미지를 입력해주세요"); frm.strimage.focus(); return false; }
		<%End If%>
		if ( frm.chk.checked == false ) { alert("약관에 동의하셔야 이용이 가능 합니다."); frm.chk.focus(); return false; }
		frm.submit();
	}

//-->
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><form action="?<%=strAction%>" method="post" enctype="multipart/form-data" name="frmRequestForm" id="frmRequestForm" style="margin:0px;" onsubmit="return frmRequestForm_Submit(this);">
      <input type="hidden" name="intseq" value="<%=intseq%>" />
      <table width="100%"  border="1" class="basic">
        <tr>
          <td class="basic" width="120" bgcolor="#F5F5F5"><b><font color="#FF0000">+</font> 상호/단체명</b></td>
          <td class="basic"><input type="text" name="strTitle" value="<%=strTitle%>" maxlength="50" style="width:50%;" /></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">+</font> 도메인주소</b></td>
          <td class="basic">http://
            <input type="text" name="strDomain" value="<%=strDomain%>" maxlength="50" style="width:200;" /></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">+</font> 계약자명</b></td>
          <td class="basic"><input type="text" name="strName" value="<%=strName%>" maxlength="50" style="width:100;" />
            연락처
            <input type="text" name="strTel" value="<%=strTel%>" maxlength="50" style="width:150;" /></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">+</font> 핸드폰</b></td>
          <td class="basic"><!--include virtual = "/select.asp"-->
              <% Call Form_HandphonePrint("strHp","")%> 
              <font color="#FF0000">*진행상황을 SMS로 알려드립니다.(필수)</font></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b><font color="#FF0000">+</font> 이메일</b></td>
          <td class="basic"><!--include virtual = "/select.asp"-->
              <% Call Form_EmailPrint("strEmail","")%></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 주소</b></td>
          <td class="basic"><input type="text" name="strAddr" value="<%=strAddr%>" maxlength="50" style="width:50%;" /></td>
        </tr>
        <tr>
          <td bgcolor="#F6F6F6" class="basic"><b>+ <font color="#0033CC">호스팅상품선택</font>
          </b></td>
          <td class="basic"><% Call Form_RadioPrint("strHosting",strHosting, "B;S;G10;G20;G50;M", "원스탑 Basic (기본1G / 매월33,000원)<br>;원스탑 Silver (기본3G / 매월55,000원);원스탑Gold10(기본10G / 매월110,000원);원스탑Gold20(기본20G / 매월165,000원);원스탑Gold50(기본50G / 매월330,000원);기존고객이전", ";", 1, 0) %></td>
        </tr>
        <tr>
          <td bgcolor="#F6F6F6" class="basic"><b>+ 월 이용료 </b></td>
          <td height="25" class="basic"><!--include virtual = "/select.asp"-->
              <a href="?avan=1002020000" target="_blank"><font color="#0000FF">[서비스 이용요금]</font></a>
            참조<br />
            월 이용료는 서비스 추가 및 용량 증설을 하시면 자동으로 변경되며 비용납부는 계정관리사이트를 통해서만 결재하실 수 있습니다.(카드,가상계좌)<br />
            초기 세팅시 시스템 설정비용 110,000원은 지정계좌로 송금해 주셔야 합니다. </td>
        </tr>
        <tr>
          <td bgcolor="#F6F6F6" class="basic"><b>+ 희망 ID/PW </b></td>
          <td class="basic">계정 ID <input type="text" name="bizid" value="<%=bizid%>" maxlength="50" style="width:100;" /> PW <input type="text" name="bizpw" value="<%=bizpw%>" maxlength="50" style="width:100;" />
              <br />
              <font color="#FF0000">FTP및 DB계정아이디 비밀번호 생성 정보로 사용됩니다.<br />
                ID의 경우 시스템 상황에 따라 변경 될 수 있습니다. </font></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 요금담당자</b></td>
          <td class="basic">성함
            <input type="text" name="strPayName" value="<%=strPayName%>" maxlength="50" style="width:100;" />
            연락처
              <input type="text" name="strPayTel" value="<%=strPayTel%>" maxlength="50" style="width:150;" /></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 이용약관 </b></td>
          <td class="basic"><textarea name="약관" rows="10" style="width:100%;"><!--#include file="약관.txt"-->
          </textarea></td>
        </tr>
        <tr>
          <td width="120" bgcolor="#F6F6F6" class="basic"><b>+ 약관동의 </b></td>
          <td class="basic"><input type="checkbox" name="chk" value="" />
서비스 이용약관에 동의합니다.</td>
        </tr>
      </table>
      <table width="100%" height="80" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="70"><span class="basic"><font color="#006699">&nbsp;</font></span>
            <table width="100%" border="0" cellspacing="10" cellpadding="0">
              <tr>
                <td><span class="basic"><font color="#006699">서비스 신청 후 초기 세팅비용(110,000-vat포함)은 지정계좌로 입금해주시기 바랍니다. <br />
홈페이지 무료제작등 서비스를 이용하시려면 입금확인 후 세팅됩니다. </font><br />
<b><font color="#FF0000"> 본 서비스 신청은 홈페이지 제작신청이 아닙니다.</font></b> 홈페이지 운영을 위한 <b>원스탑 Biz호스팅 신청서</b> 입니다.<br />
홈페이지 제작은 원스탑 Biz호스팅과는 별도로 제작계약이 이루어지며, 계약시 계약서는 별도  문서로 송부해드립니다.</span></td>
              </tr>
            </table>
            </td>
        </tr>
        <tr>
          <td align="center"><input name="button" type="button" class="btn" onclick="this.form.onsubmit();" value=" 저장 " />
            <input name="button" type="button" class="btn" onclick="self.location.href = '?<%=getString(Callpage("list",""))%>';" value=" 뒤로 " /></td>
        </tr>
      </table>
    </form></td>
  </tr>
</table>
