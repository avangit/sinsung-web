<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
+ 실제 코딩하는 디자이너를 위한 설명서<br />
<table width="700" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td width="150">함수명</td>
    <td>함수설명</td>
  </tr>
  <tr>
    <td>&lt;%=Title%&gt;</td>
    <td>관리자페이지에서 입력한 홈페이지 타이틀을 불러옵니다. </td>
  </tr>
  <tr>
    <td>&lt;%=Logo%&gt;</td>
    <td>관리자페이지에서 입력한 홈페이지 로고를 불러옵니다. </td>
  </tr>
  <tr>
    <td>&lt;%=MetaTag%&gt;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&lt;%=MainMenu%&gt;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&lt;%=SubMenu%&gt;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&lt;%=MenuPath%&gt;</td>
    <td>&quot;한국어&gt;메인메뉴&gt;서브메뉴&quot; 식으로 메뉴 경로를 보여줍니다. </td>
  </tr>
  <tr>
    <td>&lt;%=CopyRight%&gt;</td>
    <td>&nbsp;</td>
  </tr>
</table> 
</body>
</html>
