<%
	Option Explicit

	if session("userlevel") < 9 then response.End()
	
	Dim chrObjectType
	Dim strObjectName
	Dim strDataBase

	chrObjectType = Request("ObjectType")	' u : 테이블, p : 스토어드 프로시저
	strObjectName = Request("ObjectName")	' 오브젝트의 이름
	'strDataBase = Request("DataBase")		' 데이터베이스의 이름

	if ( chrObjectType = "" ) then	chrObjectType = "u"		' 값이 없으면 테이블로 설정
	'if ( strDataBase = "" ) then	strDataBase = "bizhomp"	' 데이터베이스가 입력되지 않았으면
															' pubs 디비로 설정
%>

<html>

<head>
<title>입력 창</title>
<base target="output">


<SCRIPT LANGUAGE="JavaScript">
<!--
	function ChangeObject( s )
	{
		location.href = '?DataBase=<%=strDataBase%>&ObjectType=' + s.options[s.selectedIndex].value;
	}

	function SetSelectStatement()
	{
		var f = document.QueryForm;
		f.QueryStatement.value = "Select * From " + f.ObjectName.options[f.ObjectName.selectedIndex].value;
	}

	function SetInsertStatement()
	{
		var f = document.QueryForm;
		f.QueryStatement.value = "Insert Into " + f.ObjectName.options[f.ObjectName.selectedIndex].value + "(  )\nvalues (  )";
	}

	function SetUpdateStatement()
	{
		var f = document.QueryForm;
		f.QueryStatement.value = "Update " + f.ObjectName.options[f.ObjectName.selectedIndex].value + "\nSet Column1 = Value1 \nWhere WhereStatement";
	}

	function SetDeleteStatement()
	{
		var f = document.QueryForm;
		f.QueryStatement.value = "Delete From " + f.ObjectName.options[f.ObjectName.selectedIndex].value + "\nWhere WhereStatement";
	}

	function SetHelpStatement()
	{
		var f = document.QueryForm;
		f.QueryStatement.value = "sp_Help " + f.ObjectName.options[f.ObjectName.selectedIndex].value;
	}
//-->
</SCRIPT>

</head>
<!--#include virtual = "/dbconn.asp"-->
<body>

	<form name = QueryForm method="POST" action="info_db_output.asp">
		<table width = 100% cellpadding = 0 cellspacing = 10 border = 0>
			<tr>
				<td colspan = 2>
				  <font size="2"><b><font color="#FF0000">데이터베이스 작업시 복구가 불가능하므로 신중하게 작업하시기 바랍니다.</font> <br>
					</b><font color="#003333">프로시저 또는 테이블 목록이 나오지 않으면 dbo 권한이 없어서 입니다. 호스팅관리&gt;DB서버정보에서  DB개체소유자변경을 dbo로 변경요청하시면 이용가능합니다. </font></font></td>
			</tr>
			<tr>
				<td colspan = 2>
					<select name="ObjectType" OnChange="javascript:ChangeObject( this );">
						<option value = p <% if ( chrObjectType = "p" ) then Response.Write "Selected" end if %>>Stored Procedure</option>
						<option value = u <% if ( chrObjectType = "u" ) then Response.Write "Selected" end if %>>Table</option>
					</select>
					<select name="ObjectName">
<%
	Dim strConnect

	' Connection String 을 작성한다.
	'strConnect = "Provider=SQLOLEDB;"
	'strConnect = strConnect & "Data Source=" & Session("IP") & ";"
	'strConnect = strConnect & "Initial Catalog=" & strDataBase & ";"
	'strConnect = strConnect & "User id=" & Session("ID") & "; Password=" & Session("Pwd") & ";"

	strConnect = cstConnString()
	
	
	Dim objConn
	Dim objRs
	Dim strSQL

	Set objConn = Server.CreateObject("ADODB.Connection")

	' 시스템 테이블에서 원하는 오브젝트의 이름을 찾아온다.
	strSQL = "Select Name From SysObjects Where Type = '" & chrObjectType & "'Order By Name"

	objConn.Open strConnect
	Set objRs = objConn.Execute( strSQL )

	if not ( ( objRs.BOF = True ) or ( objRs.EOF = True ) ) then
		Do Until ( objRs.EOF = True )
%>
						<option value = <%=objRs(0)%>><%=objRs(0)%></option>
<%
			objRs.MoveNext
		Loop
	end if

	objRs.Close
	Set objRs = Nothing
	objConn.Close
	Set objConn = Nothing
%>
					</select>
				</td>
			</tr>
			<tr>
				<td>

<%	if ( chrObjectType = "u" ) then	%>

					<input type="button" value="Select" style = "background-color:#EFEFEF;width:80; height:21; border:1 solid buttonface" OnClick = "javascript: SetSelectStatement();">
					<input type="button" value="Insert" style = "background-color:#EFEFEF;width:80; height:21; border:1 solid buttonface" OnClick = "javascript: SetInsertStatement();">
					<input type="button" value="Update" style = "background-color:#EFEFEF;width:80; height:21; border:1 solid buttonface" OnClick = "javascript: SetUpdateStatement();">
					<input type="button" value="Delete" style = "background-color:#EFEFEF;width:80; height:21; border:1 solid buttonface" OnClick = "javascript: SetDeleteStatement();">
<%	end if	%>
					<input type="button" value="정보보기" style = "background-color:#EFEFEF;width:80; height:21; border:1 solid buttonface" OnClick = "javascript: SetHelpStatement();">
					<!--input type="button" value="단추" name="B6"-->
				</td>
				<td align = right width = 105>
					<input type="submit" value="  쿼리 실행 >>> " style = "background-color:#EFEFEF;width:100; height:21; border:1 solid buttonface">
				</td>
			</tr>
			<tr>
				<td colspan = 2>
					<textarea rows="11" name="QueryStatement" style="width:100%"></textarea>
				</td>
			</tr>
	  </table>
		<input type = hidden name = DataBase value = "<%=strDataBase%>">
	</form>

</body>

</html>
