<!--#include file = "_BuilderConfig.asp" -->


<% 
	call urlNoEXE("/avanplus/sol_builder/")
	
	if catecode() = 0 then response.Redirect "?"&getString("HOMEPCATE=1000000000")
	
	
	'//내용확인 체크
	Function data_chack(byval DATA_WDAY, byval content_mode)
		data_chack = "<font color=red>확인중</font>"
		if len(DATA_WDAY) > 0 then data_chack = "<font color=blue>확인됨</font>"
		
		'//서브메뉴인경우
		if Ucase(content_mode) = "S" then data_chack = "<font color=silver>내용없음</font>"
	End Function
	
	'//구현방식z
	Function data_ordermode(byval ordermode, byval content_mode)
		data_ordermode = "<font color=red>미정</font>"'"<font color=red>미정</font>["&content_mode&"]"
		if content_mode = "D" then 
			data_ordermode = "<font color=blue>디자인페이지</font>"
		
		elseif content_mode = "M" then 
			data_ordermode = "<font color=blue>프로그램[모듈]</font>"
		
		elseif content_mode = "E" then 
			data_ordermode = "<font color=blue>실행파일연결</font>"
			
		elseif content_mode = "R" then 
			data_ordermode = "<font color=blue>페이지이동</font>"
			
		elseif content_mode = "L" then 
			data_ordermode = "<font color=blue>페이지링크[새창]</font>"
			
		elseif content_mode = "S" then 
			data_ordermode = "<font color=blue>서브 첫페이지</font>"
			
		end if
		
		'//서브메뉴인경우
		if Ucase(content_mode) = "S" then data_ordermode = "<font color=silver>내용없음</font>"
	End Function
	
%>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>홈페이지 사이트맵</title>
</HEAD>



<BODY>
<br>
<p class="title3 t_blue">게시판관리</p>
  <br>
  <br>
  <%
  	dim imgSql, imgRs
  %>
  
  
<table class="tableList image_table" cellpadding="0" cellspacing="0">
    <colgroup>    
        <col style="width:">
        <col style="width:8%">
    </colgroup>
    
    <tr>
        <th colspan="2">메인/로그인페이지 비주얼이미지 관리</th>
    </tr>
		<%
            imgSql = "select * from "& tablename &" where c_code='1000000000'  order by c_sunse asc "
            set imgRs = dbconn.execute(imgSql)
            if not imgRs.eof then
            do while not imgRs.eof
        %>
    <form name="imgModify<%=imgRs("c_code")%>" enctype="multipart/form-data" method="post" action="/avanplus/sol_builder/modify_catedesign.asp?<%=getString("HOMEPCATE="&imgRs("c_code"))%>">
    <tr>
        <td>		
            <b class="t_blue">메인페이지</b> 비주얼이미지
            <input name="visual" type="file" id="visual"> &lt;%MainImage%&gt;
            <br><%=ImgLmtView("/setImage/",imgRs("c_img"),600) %>
            <hr style="border:dotted silver; height:1px"><br>
        </td>  
        <td rowspan="3" ><input name="m"  class="btnDB btnS" type="submit" id="m" value="적용"></td>     
    </tr>
    
    
    <tr>          
        <td>		
            <b><font color="gray">메인페이지</font></b> 비주얼백그라운드 이미지
            <input name="visualbg" type="file" id="visualbg">&lt;%MainImageBg%&gt;
            <br><%=ImgLmtView("/setImage/",imgRs("menuimg_over"),600) %>
            <hr style="border:dotted silver; height:1px"><br>
        </td>        
    </tr> 
    <tr> 
        <td>		
            <b class="t_blue">로그인페이지</b> 비주얼이미지
            <input name="visual2" type="file" id="visual2">&lt;%loginImage%&gt;
            <br><%=ImgLmtView("/setImage/",imgRs("menuimg"),600) %>
        </td>
    </tr>
    </form>
    <%
		imgRs.movenext
		loop
		end if
		imgRs.close
		set imgRs = nothing
    %>
</table>
  
<br>
<br>





  
 <table class="tableList image_table" cellpadding="0" cellspacing="0">
    <colgroup>    
        <col style="width:">
        <col style="width:8%">
    </colgroup>
    <tr>
        <th colspan="2">서브 비주얼이미지 관리</th>
    </tr>
        <%
			imgSql = "select * from "& tablename &" where c_code like '%000000' and c_code > '1000000000' and c_code < '1100000000'  order by c_sunse asc "
			'response.Write(imgSql)
			set imgRs = dbconn.execute(imgSql)
			if not imgRs.eof then
			do while not imgRs.eof
        %>
        <form name="imgModify<%=imgRs("c_code")%>" enctype="multipart/form-data" method="post" action="/avanplus/sol_builder/modify_catedesign.asp?<%=getString("HOMEPCATE="&imgRs("c_code"))%>">
    <tr>  
        <th colspan="2"><%=imgRs("c_name")%></th>
    </tr>
    <tr>
        <td>	
            <b class="t_blue"><%=imgRs("c_name")%></b> 서브 주메뉴타이틀
            <input name="visual2" type="file" id="visual2">&lt;%SubTitleImage%&gt;
            <br><%=ImgLmtView("/setImage/",imgRs("menuimg"),600) %>
            <hr style="border:dotted silver; height:1px"><br>
        </td>
        <td rowspan="3"><input name="m" class="btnDB btnS" type="submit" id="m" value="적용"></td>
    </tr>
    <tr>
        <td>    		
            <b class="t_blue"><%=imgRs("c_name")%></b> 서브 비주얼이미지
            <input name="visual" type="file" id="visual">&lt;%SubImage%&gt;
            <br><%=ImgLmtView("/setImage/",imgRs("c_img"),600) %>
            <hr style="border:dotted silver; height:1px"><br>
        </td>
    </tr>	
    <tr>
        <td>    		
            <b class="t_blue"><%=imgRs("c_name")%></b> 서브 비주얼백이미지
            <input name="visualbg" type="file" id="visualbg">&lt;%SubImageBg%&gt;
            <br><%=ImgLmtView("/setImage/",imgRs("menuimg_over"),600) %>
        </td>
    </tr>
    </form>
    <%
		imgRs.movenext
		loop
		end if
		imgRs.close
		set imgRs = nothing
    %>
</table>
  <br>
  <br>
* 해당 업로드된 이미지들은 HTML 소스 적용여부에 따라 적용되지 않을 수 있습니다. 
</BODY>
</HTML>
<% call dbclose %>