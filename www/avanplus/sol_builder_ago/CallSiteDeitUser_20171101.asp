<!--#include file = "_BuilderConfig.asp" -->



<%
	call urlNoEXE("/avanplus/sol_builder/")
	
	if catecode() = 0 then response.Redirect "?"&getString("HOMEPCATE=1000000000")
	
	call ValuesSetting()

%>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>카테고리 및 디자인</title>
<style type="text/css">
.left_td { text-align:left; padding-left:10px; }
</style>
</HEAD>

<BODY>

    <table class="tableList" cellpadding="0" cellspacing="0">
        <colgroup>    
            <col style="width:16%">
            <col style="width:"">
        </colgroup>
	    
		<tr>
            <th>메뉴선택</th>
            <td class="tleft pl20" ><% navi_select(cateCode) %></td>
	    </tr>
		  <%     
			   if len(request.QueryString("HOMEPCATE")) = 0 then
					response.Write "카테고리를 선택해 주세요."
					response.end
				end if
		  %>
	   <form name="catemodify" enctype="multipart/form-data" method="post"  action="/avanplus/sol_builder/modify_catedesign.asp?<%=getString("HOMEPCATE="&request.QueryString("HOMEPCATE"))%>">
		<tr>
            <th>카테고리</th>
            <td  class="tleft pl20">홈 <%=getCateColPrint(cateCode)%></td>
		</tr>
		<tr>
            <th>메뉴명</th>
            <td class="tleft pl20"><input name="c_name" type="text" id="c_name" class="input01 w30" value="<%=c_name%>"></td>
		</tr>
		<tr>
            <th>메뉴설명</th>
            <td class="tleft pl20"><input name="data_memo" type="text" id="data_memo" class="input01 w30" value="<%=data_memo%>"><br></td>
        <!--tr>
            <td bgcolor="#FFFFFF" colspan="2">
            + 메뉴에 들어갈 이미지(1depth면 메인비주얼이미지, 2depth면 서브비주얼이미지, 3depth면 컨텐츠타이틀로 보여짐)<br>
            <%=ImgLmtView("/upload/",getFileNumName(c_img),550) %>
            
            <input name="c_img" type="file" id="c_img">
            <input name="imgcheck" type="checkbox" id="imgcheck" value="1">
            이미지삭제
            </td>
        </tr-->
        </tr>
		<tr>
          	<th class="left_td"><input name="content_mode" type="radio" value="M" <%=b_Checked(content_mode,"M")%>> 프로그램[모듈]</th>
		  	<td class="tleft pl20">
                <span class="basic"><% Call Form_SelectPrint("content_modul",content_modul, "사용할모듈ID선택", module_value, module_name, ",", "")%></span> 
                <!--<input name="content_modul" type="text" id="content_modul" value="<%=content_modul%>" style="width:200px">-->
                모듈구분값 <input name="content_modulvalues" type="text" id="content_modulvalues" class="input01 " value="<%=content_modulvalues%>">
            </td>
	     </tr>
        <tr>
            <th class="left_td"><input name="content_mode" type="radio" value="E" <%=b_Checked(content_mode,"E")%>> 실행파일절대경로</th>
            <td class="pr20 pl20"><input name="content_execute" type="text" id="content_execute" class="input01 w100" value="<%=content_execute%>"></td>
        </tr>
        <tr>
            <th class="left_td"><input name="content_mode" type="radio" value="R" <%=b_Checked(content_mode,"R")%>> 페이지이동 </th>
            <td class="pr20 pl20"><input name="content_redirect" type="text" id="content_redirect" class="input01 w100" value="<%=content_redirect%>"></td>
        </tr>
        <tr>
            <th class="left_td"><input name="content_mode" type="radio" value="L" <%=b_Checked(content_mode,"L")%>> 페이지링크[새창] </th>
            <td class="pr20 pl20"><input name="content_link" type="text" id="content_link" class="input01 w100" value="<%=content_link%>"></td>
        </tr>
        <tr>
            <th class="left_td"><input name="content_mode" type="radio" value="S" <%=b_Checked(content_mode,"S")%>> 서브 첫페이지 </th>
            <td class="tleft pl20">하위메뉴의 첫 페이지로 이동합니다. </td>
        </tr>
		<tr>
            <th colspan="3" class="left_td"><input name="content_mode" type="radio" value="D" <%=b_Checked(content_mode,"D")%>> 디자인페이지
				<%' call setFCKeditor("data_orignal", data_orignal, "1", 500) %><%' call SetEditor_v3("data_orignal",data_orignal) %>
                <% call editor("data_orignal",data_orignal)%>
<!--                <center>** 에디터에 삽입할 이미지가 있으시면 디자인파일 업로드를 이용하세요. <input type="button"  class="btnDB btnS w20" value="이미지파일업로드"  onClick="window.open('/AVANplus/LIB/KNEditor_FIle/FileUpload.asp','imgfileupload','width=500,height=500,left=200,top=150,scrollbars=no');"></center>
-->            </th>
		</tr> 
		<tr>
            <th class="t_blue">페이지보안레벨</th>
            <td class="tleft pl20"><% Call Form_NumberSelectPrint("pagelevel",pagelevel, "", 0, 9, "") %>레벨포함 접근가능 (0:비로그인, 1:로그인 ~ 9:관리자) </td>
	   	</tr>
		<tr>
            <th>메뉴노출 설정 </th>
            <td class="tleft pl20">
                <input name="c_use" type="radio" value="1" <% if c_use then response.Write("checked") %>> 메뉴보이기 
                <input name="c_use" type="radio" value="0" <% if not(c_use) then response.Write("checked") %>> 메뉴숨기기		    
            </td>
	    </tr>
		<tr>
            <th>메뉴 메모 </th>
            <td class="pr20 pl20 "><textarea name="data_planer" rows="5" id="data_planer" class="input02 w100 h100px"><%=data_planer%></textarea></td>
	    </tr>
	</table>
    
    <ul class="btnWrap">
        <li><input type="submit" name="Submit"  class="btn_m c_blue" value="저장"></li>
    </ul>
    </form>	
</BODY>
</HTML>
<% call dbclose %>