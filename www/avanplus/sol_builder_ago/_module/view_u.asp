<!--#include file = "_config.asp"-->
<%

intSeq = request.QueryString("intSeq")


Call idxdata(intSeq)
%>

<table width="100%"  border="1" class="basic">
  <tr>
    <td width="100" bgcolor="#F6F6F6" class="basic"><b>모듈명</b></td>
    <td class="basic"><b><%=strTitle%></b></td>
  </tr>
  <tr>
    <td class="basic" bgcolor="#F6F6F6"><b>모듈ID</b></td>
    <td class="basic"><font color="#990000"><%=strName%></font></td>
  </tr>
  <tr>
    <td class="basic" bgcolor="#F6F6F6"><b>모듈설명</b></td>
    <td class="basic"><%=strcontent%></td>
  </tr>
  <tr>
    <td class="basic" bgcolor="#F6F6F6"><b><font color="#0000FF">미리보기</font></b></td>
    <td class="basic"><%=StrViewUrl%></td>
  </tr>
  <tr>
    <td class="basic" bgcolor="#F6F6F6"><b>이용요금</b></td>
    <td class="basic">사용료 <font color="#FF0000"><b>무료</b></font>(BIZ호스팅 이용시 무료, 타사이전 불가)</td>
  </tr>
  <tr>
    <td class="basic" bgcolor="#F6F6F6"><b>세팅비용</b></td>
    <td class="basic">모듈 및 데이터베이스 세팅비용 1회 30,000원</td>
  </tr>
  <tr>
    <td class="basic" bgcolor="#F6F6F6"><b>신청방법</b></td>
    <td class="basic">마이페이지 <b>MY BIZ호스팅 &gt; 1:1A/S문의</b> 에서<br />
      &quot;모듈아이디<font color="#990000"><%=strName%></font> 세팅요청바랍니다. &quot;라고 요청하시면 1영업일 이내에 세팅해드립니다. </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="80" align="center"><input name="button2" type="button" class="btn" onclick="self.location.href = '?<%=getString(Callpage("List_u","intSeq="))%>';" value="목  록"></td>
  </tr>
</table>
<script language="javascript">
<!--
	function really(){
		if (confirm('\n정말로 삭제하시겠습니까? 삭제는 복구가 불가능합니다.\n')) {
			self.location.href = '?<%=getString(Callpage("Delete","intSeq="&intSeq))%>';	
		}
	}
	
	function OpenWindow(url,w,h,scrollbar) {
		if (scrollbar=="") scrollbar=0;
		//window.open(url,"ICCwindow","width=400,height=400,status=no,scrollbars=no");
		lPos = (screen.width) ? (screen.width-w)/2 : 0; tPos = (screen.height) ? (screen.height-h)/2 : 0;
		
		window.open(url, "", "height="+h+", width="+w+", left="+lPos+", top="+tPos+", toolbar=0, location=0,directories=0,status=0,menuBar=0,scrollBars="+scrollbar+",resizable=0");
	}

	function PrintContent(){	
		var mm = window.open("?<%=getString("Print=y")%>","print_win","width=550,height=600");
		var doc = mm.document;		
		doc.focus();
	}	
	
//-->
</script>
<SCRIPT Language="Javascript">
<!--
var NS = 1;
if( document.all) NS = 0;

function PrintOption(type1,type2){
//네스케이프일 경우
        if (NS) {
                window.print();
//익스플로러일 경우
        } else {
                var active = '<OBJECT ID="active1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
                document.body.insertAdjacentHTML('beforeEnd', active);
                active1.ExecWB(type1,type2); 
                active1.outerHTML ="";
        }
}
//-->
</script>
