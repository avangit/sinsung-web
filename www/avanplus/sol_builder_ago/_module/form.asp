<!--#include file = "_config.asp"-->
<%
Dim strAction

intSeq = request.QueryString("intSeq")

Dim strPageMode : strPageMode = "update"
	
	Call idxdata(intSeq)
	
	'action처리부분
	
	If Len(intSeq) > 0 Then
		strAction = getstring(Callpage("update",""))	'//수정
	Else			
		strAction = getstring(Callpage("insert",""))	'//등록
	End If


%>
<script language="javascript">
<!--

	function frmRequestForm_Submit(frm){
		if ( frm.strTitle.value.replace(/ /gi, "") == "" ) { alert("제목을 입력해주세요"); frm.strTitle.focus(); return false; }
		<%If Len(intSeq) = 0 Then%>
		//if ( frm.strimage.value.replace(/ /gi, "") == "" ) { alert("이미지를 입력해주세요"); frm.strimage.focus(); return false; }
		<%End If%>
		//if ( frm.strContent.value.replace(/ /gi, "") == "" ) { alert("의뢰인을 입력해주세요"); frm.strContent.focus(); return false; }
		frm.submit();
	}

//-->
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><form action="?<%=strAction%>" method="post" enctype="multipart/form-data" name="frmRequestForm" id="frmRequestForm" style="margin:0px;" onsubmit="return frmRequestForm_Submit(this);">
      <input type="hidden" name="intseq" value="<%=intseq%>" />
      <table width="100%"  border="1" class="basic">
        <tr>
          <td class="basic" width="100" bgcolor="#F5F5F5"><b>모듈명</b></td>
          <td class="basic"><input type="text" name="strTitle" value="<%=strTitle%>" maxlength="50" style="width:50%;" /></td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6"><b>모듈ID</b></td>
          <td class="basic"><input type="text" name="strName" value="<%=strName%>" maxlength="50" style="width:100;" /></td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6"><b>설명</b></td>
          <td class="basic"><% call setFCKeditor("strcontent", strcontent, "", 200) %>          </td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6"><b>이미지</b></td>
          <td class="basic"><input type = "file" name = "strimage" /></td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6"><b>미리보기</b></td>
          <td class="basic"><!--include virtual = "/select.asp"-->
              <input type="text" name="StrViewUrl" value="<%=StrViewUrl%>" maxlength="50" style="width:50%;" />
            StrViewUrl</td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6"><b>사용방법</b></td>
          <td class="basic"><% call setFCKeditor("strHelp", strHelp, "", 200) %>
          strHelp</td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6"><b>용도</b></td>
          <td class="basic"><!--include virtual = "/select.asp"-->
            <% Call Form_RadioPrint("StrUse",StrUse, "1,2", "고객서비스용,내부개발", ",", 2, 0) %>
            StrUse</td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6"><b>테이블명</b></td>
          <td class="basic"><!--include virtual = "/select.asp"-->
            <input type="text" name="StrTableneme" value="<%=StrTableneme%>" maxlength="50" style="width:50%;" />
            StrTableneme</td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6"><b>쿼리</b></td>
          <td class="basic"><!--include virtual = "/select.asp"-->
            <textarea name="StrQuery" rows="20" style="width:100%;"><%=StrQuery%></textarea>
            StrQuery</td>
        </tr>
        <tr>
          <td class="basic" bgcolor="#F6F6F6">&nbsp;</td>
          <td class="basic"><!--include virtual = "/select.asp"--></td>
        </tr>
      </table>
      <table width="100%" height="80" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td align="center"><input name="button" type="button" class="btn" onclick="this.form.onsubmit();" value=" 저장 " />
              <input name="button" type="button" class="btn" onclick="self.location.href = '?<%=getString(Callpage("list",""))%>';" value=" 뒤로 " />
          </td>
        </tr>
      </table>
    </form></td>
  </tr>
</table>
