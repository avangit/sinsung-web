<!--#include file = "_config.asp"-->
<%
'+++++++[0] 입력은 insert 수정은 update 삭제는 delete 로 변경한다.
Dim insert

Call DbOpen()

set insert =  new DextQueryClass
	
	Call insert.setUploadFolder(UploadFolder)	'//업로드 폴더 세팅
	Call insert.settablename(tablename)			'//테이블명 세팅	
	
	'+++++++[1] 넘어오는 값들을 확인해 본다.넘어오는 값확인후 반드시 주석처리로 바꾼다
	'삭제시에는 아래 3값이 활성화 되어있으면 에러남
	'=====================================================================
	'Call insert.ViewFormName()  	'[주석]폼네임명을 볼수있는 함수
	'Call insert.ViewTbaleGride()	'[주석]테이블로 보기
	'Call insert.ViewCreateTable()	'[주석]테이블쿼리
	'=====================================================================
	'+++++++[0]_End 



	'+++++++[2] 수정과 삭제의 경우만 활설화 된다. 입력(insert)때는 비활성화
	'Call insert.setwhere(" intSeq = '"& request.QueryString("intSeq") &"' ")	'//수정과 삭제에서만 사용
	'+++++++[2]_End


	'// 기본으로 넘어오는 값 세팅
	'Call insert.setDefaultData("")


	'+++++++[3] 입력페이지 이외에는 [주석]처리한다.
	'기본 저장 필드 '_config파일에서 setDataField또는 setFileField 에 지정되지 않은 필드들
	Call insert.setdataAdd("strCategory", requestQ("mcate"))	'//글등록 구분값
	Call insert.setdataAdd("strUserid", session("userid"))		'//글등록자 아이디
	Call insert.setdataAdd("dtmInsertDate", date())				'//최초글등록일
	'+++++++[3]_End


	'+++++++[4] form에서 넘어오는 값 세팅 / 파일로 넘어오는 값 제외 / delete에서는 사용하지 않음[4번은 삭제]
	'config에 정의된 데이터 필드 넣기
	call insert.setStrData(setDataField)
		'dim i, arr
		'i=""
		'arr=""
		'arr = split(setDataField,",")
		'For i = 0 To (ubound(arr)) Step 1
		'	response.Write arr(i)
		'	Call insert.setdataAdd(arr(i), insert.getFormValue(arr(i)))
		'next

	'+++++++[5] form에서 넘어오는 업로드될 파일 값 세팅 [함수명 바뀜insert-update-delete]
	'config에 정의된 파일 필드 넣기
	call insert.setFileData(setFileField,"insert")
		'i=""
		'arr=""
		'arr = split(setFileField,",")
		'For i = 0 To (ubound(arr)) Step 1
			'response.Write arr(i)
		'	Call insert.setinsertNumFileDataAdd(arr(i))
		'next

	'+++++++[5]_End


	'쿼리생성
	strSQL = insert.getqueryinsert	'[함수명 바뀜insert-update-delete]			
	'response.Write("<br><br>"&strSQL)
	Call AdoConnExecute(strSQL)
	
Call DbClose()
set insert = nothing


'+++++++[6] 메세지를 바꿔주고 처리후 이동할 페이지를 정해준다.
Call jsAlertMsgUrl("등록되었습니다", BackGetString(Callpage("list","keyword=&keyword_option=&page=")))
'+++++++[6]_End
%>
