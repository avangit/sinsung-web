<!--#include file = "_BuilderConfig.asp" -->
<!--include virtual = "/AVANplus/BuilderStart.asp" -->

<%	

'call urlNoEXE("/avanplus/sol_builder/")
if session("userlevel") < 9 then response.End()

Sub inputForm(byval num)

	'response.Write getCateStep(CateCode) &"/"& int(num)
	if int(getCateStep(CateCode)) = int(num) then
	  %>
	  <tr>
        <form name="HOMEPCATEAdd" method="post" enctype="multipart/form-data" action="/avanplus/sol_builder/insert.asp?<%=getString("")%>">
		<td bgcolor="#666666">
			<b><font color="#FFFFFF"><%=i+1%>단 카테고리 등록</b></font><br>
			<input name="c_name" type="text" size="19">	
            <input type="submit" name="Submit" value="추가">
		</td>
		</form>
      </tr>
	  <%
	end if
End Sub


Function bgColor(byval code, byval step)
	bgColor = "#F5F5F5"
	dim HOMEPCATE
	if left(code,2*step) = left(request.QueryString("HOMEPCATE"),2*step) then bgColor = "#0099CC"
End Function


%>



<b><font size="4">홈페이지 사이트맵 작성</font></b><br><br>
<b><font color=red><a href="?<%=getString("HOMEPCATE=")%>">처음</a><%=getCateColPrint(cateCode)%></font></b>

<br>
<table width="<%=200*getCateStep(CateCode)%>" border="0">
 <tr valign="top">
 <% 
 	
	'response.Write getCateStep(CateCode)
	dim i, sql_list, rs_list
 	
	
	
	dim TempcateStep '// 4이상인 경우 무시 4단계때 5단계를 생성하기 때문
	TempcateStep = getCateStep(CateCode) 
	if getCateStep(CateCode) > 4 then TempcateStep = 4
	
  	for i = 0 to TempcateStep 
%>
  
    <td>
		<table class="tableList" cellpadding="0" cellspacing="0">
		  <tr bgcolor="#000000">
			<td>
				<font color="#FFFFFF"><b><%=i+1%> depth </b></font>
			</td>
		  </tr>
		</table>
		<table width="200" border="0" cellpadding="3" cellspacing="1" bgcolor="#000000">
			
			<% 
				call inputForm(i) 
				'response.Write("a"&i)
			%>
			
			<%
				sql_list = "select * from "&tablename&" where "& CateThisWhere(i,cateCode) &"  order by c_sunse asc "
				'response.Write(sql_list)
				set rs_list = dbconn.execute(sql_list)
				
				if not rs_list.eof then
					do while not rs_list.eof
			%>
			<tr>
				<form name="catemodify" enctype="multipart/form-data" method="post" action="/avanplus/sol_builder/modify.asp?<%=getString("c_idx="&rs_list("c_idx"))%>">
				  <td bgcolor="#CCCCCC">
				  	<table width="200" border="0" cellpadding="0" cellspacing="0" bgcolor="<%=bgColor(rs_list("c_code"), i+1)%>">
					  <tr>
						<td>&nbsp;메뉴사용<input name="c_use" type="checkbox"  style="border:0;background-color:transparent" value="1" <% if rs_list("c_use") then response.Write("checked") %> >
							<a href="?<%=getString("HOMEPCATE="&rs_list("c_code"))%>"><%=rs_list("c_code")%></a>
							<font color="#0000FF">&nbsp;
							</font> <input name="imageField" type="image" src="/avanplus/sol_builder/btn_edit.gif"  width="11" height="11" border="0">
							<a href="/avanplus/sol_builder/delete.asp?<%=getString("HOMEPCATE="&rs_list("c_code"))%>"><img src="/avanplus/sol_builder/btn_del.gif" width="11" height="11" border="0"></a></td>
					  </tr>
					  <tr>
						<td>
							<input name="c_name" type="text" size="20"  class="input01" value="<%=rs_list("c_name")%>">
							<input name="c_sunse" type="text" size="3"  class="input01" value="<%=rs_list("c_sunse")%>"><br>
						</td>
					  </tr>
				  	</table>
				  </td>
				</form>		
			</tr>

			<%
					rs_list.movenext
					loop
					'call inputForm(i) 
					'response.Write("b"&i)
				end if
			%>
		</table>
	</td>
  
  <% next %>
  </tr>
</table>

<br>
<br>



<div class="tip_box">
	<p>[간략설명]<br>
    1 depth는 시스템에서 사용합니다. 추가 하거나 변경하지 마세요.<br>
    2 depth는 주메뉴, 3 depth는 서브메뉴를 나타냅니다. <br>
     (메뉴는 4depth까지 생성가능하나 빌더연동은 3depth 이후는 사용되지 않습니다.)<br>
		  10자리 숫자를 클릭하면 해당 카테고리가 파란색으로 활성화 되며 하위메뉴를 입력하실수 있습니다.<br>
		  메뉴사용 체크를 해제하시면 디비에 자료는 지워지지 않고 홈페이지에 메뉴를 보이지 않게 됩니다.<br>
		  두번째 입력필드에 숫자는 노출순서입니다. 숫자를 조절하면 메뉴의 위치가 위/아래로 변경됩니다.<br>
</div>





</table>

<% call dbclose %>