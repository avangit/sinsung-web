<%
  Response.Expires = 0
  Response.AddHeader "pragma","no-cache"
  Response.AddHeader "cache-control","private"
  Response.CacheControl = "no-cache"

  Server.ScriptTimeout = 300
  Response.Buffer = TRue


if session("userlevel") < 9 then response.End()  
%><!--#include virtual = "/dbconn.asp"--><%  
  
  Function getAdoRsScalar(ByVal strQuery)

		Dim objAdoRs, strValue

		'Print strQuery

		Set objAdoRs = Dbcon.Execute(strQuery)'Server.CreateObject("ADODB.RecordSet")

			'objAdoRs.Open strQuery, DbConn, adOpenForwardOnly, adLockReadOnly, adCmdText
				If Not (objAdoRs.EOF And objAdoRs.BOF) Then
					strValue = objAdoRs(0)
				Else
					strValue = Null
				End If
			'objAdoRs.Close

		Set objAdoRs = Nothing

		getAdoRsScalar = strValue

	End Function
  
  
  
  
  
  
  
  
  Dim Dbcon

  Dim serverip, tablename,userid,password


  serserverip   = ""
  tablename     = ""
  userid        = ""
  password      = ""

  Sub Db_Connect  'Connection Database

    Set Dbcon = Server.CreateObject("ADODB.CONNECTION")
    'Dbcon.Open "Provider=SQLOLEDB;Data Source=211.253.25.100;Initial Catalog=lawbid;user ID=lawbid;password=avandb#0701;Persist Security Info=True;Network Library=dbmssocn"
    Dbcon.Open cstConnString

  End Sub

  Sub Db_DisConnect 'Disconnection Database

      Dbcon.Close
    Set Dbcon = Nothing

  End Sub

  Call Db_Connect()	'-- DataBase Connection
  Call MainProcess()
  Call Db_DisConnect() '-- DataBase DisConnection

  Sub MainProcess

    SQL =  "  select  " &_
           "      t_columns.table_catalog,  " &_
           "      t_columns.table_name,  " &_
           "      t_columns.column_name,  " &_
           "      t_columns.ordinal_position,  " &_
           "      t_columns.column_default,  " &_
           "      t_columns.is_nullable,  " &_
           "      t_columns.data_type,  " &_
           "      t_columns.character_maximum_length,  " &_
           "      t_columns.is_nullable,  " &_
		   "	  (select TD.columnDescription from TableDescription TD where TD.tableName = t_columns.table_name and TD.columnName = t_columns.column_name ) CD,  " &_
           "      case when t_columns.column_name=isnull(t_column_usage.column_name,'') and t_table_const.constraint_type='PRIMARY KEY' then 'Y' else ' ' end as PK,  " &_
           "      case when t_columns.column_name=isnull(t_column_usage.column_name,'') and t_table_const.constraint_type='FOREIGN KEY' then 'Y' else ' ' end as FK,  " &_
           "      case when t_columns.column_name=isnull(t_column_usage.column_name,'') and t_table_const.constraint_type='FOREIGN KEY' then  " &_
           "      (SELECT  " &_
           "        PK_Table  = PK.TABLE_NAME  " &_
           "      FROM  " &_      
		   "			INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C  " &_
           "        INNER JOIN  " &_
           "        INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK  " &_
           "          ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME  " &_
           "        INNER JOIN  " &_
           "        INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK  " &_
           "          ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME  " &_
           "        INNER JOIN  " &_
           "        INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU  " &_
           "          ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME  " &_
           "        INNER JOIN  " &_
           "        (  " &_
           "          SELECT  " &_
           "            i1.TABLE_NAME, i2.COLUMN_NAME  " &_
           "          FROM  " &_
           "            INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1  " &_
           "            INNER JOIN  " &_
           "            INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2  " &_
           "            ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME  " &_
           "            WHERE i1.CONSTRAINT_TYPE = 'PRIMARY KEY'  " &_
           "        ) PT  " &_
           "        ON PT.TABLE_NAME = PK.TABLE_NAME and C.CONSTRAINT_NAME=t_table_const.CONSTRAINT_NAME  " &_
           "      )  " &_
           "      else ' ' end as FK_table ,  " &_
           "      case when t_columns.column_name=isnull(t_column_usage.column_name,'') and t_table_const.constraint_type='FOREIGN KEY' then  " &_
           "      (SELECT  " &_
           "        PK_Column = PT.COLUMN_NAME  " &_
           "      FROM  " &_
           "        INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C  " &_
           "        INNER JOIN  " &_
           "        INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK  " &_
           "          ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME  " &_
           "        INNER JOIN  " &_
           "        INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK  " &_
           "          ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME  " &_
           "        INNER JOIN  " &_
           "        INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU  " &_
           "          ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME  " &_
           "        INNER JOIN  " &_
           "        (  " &_
           "          SELECT  " &_
           "            i1.TABLE_NAME, i2.COLUMN_NAME  " &_
           "          FROM  " &_
           "            INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1  " &_
           "            INNER JOIN  " &_
           "            INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2  " &_
           "            ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME  " &_
           "            WHERE i1.CONSTRAINT_TYPE = 'PRIMARY KEY'  " &_
           "        ) PT  " &_
           "        ON PT.TABLE_NAME = PK.TABLE_NAME and C.CONSTRAINT_NAME=t_table_const.CONSTRAINT_NAME  " &_
           "      )  " &_
           "      else ' ' end as FK_column  " &_
           "  " &_
           "    FROM  " &_
           "      INFORMATION_SCHEMA.COLUMNS t_columns left outer join INFORMATION_SCHEMA.KEY_COLUMN_USAGE t_column_usage  " &_
           "        on t_columns.TABLE_NAME+t_columns.column_name=t_column_usage.TABLE_NAME+t_column_usage.column_name  " &_
           "      left outer join INFORMATION_SCHEMA.TABLE_CONSTRAINTS t_table_const  " &_
           "        on t_column_usage.constraint_name=t_table_const.constraint_name  " &_
           "    order by  " &_
           "       t_columns.table_name,t_columns.ordinal_position  "

    Set Rs = dbcon.execute(SQL)
	'response.Write(sql)

'  Response.ContentType = "application/msword"
'  Response.CacheControl = "public"
'  Response.AddHeader "Content-Disposition", "attachment;filename=DB_"&tablename&".doc"




%>
<html xmlns:v=urn:schemas-microsoft-com:vml xmlns:o=urn:schemas-microsoft-com:office:office xmlns:w=urn:schemas-microsoft-com:office:word xmlns:m=http://schemas.microsoft.com/office/2004/12/omml xmlns:st1=urn:schemas-microsoft-com:office:smarttags xmlns=http://www.w3.org/TR/REC-html40>
  <head>
  <meta http-equiv=Content-Type content=text/html; charset=utf-8>
  <meta name=ProgId content=Word.Document>
  <meta name=Generator content=Microsoft Word 12>
  <meta name=Originator content=Microsoft Word 12>
  <o:SmartTagType namespaceuri=urn:schemas-microsoft-com:office:smarttags
   name=place/>
  <o:SmartTagType namespaceuri=urn:schemas-microsoft-com:office:smarttags
   name=country-region/>
  <!--[if gte mso 9]><xml>
   <o:DocumentProperties>
    <o:Author>kelvin</o:Author>
    <o:LastAuthor>kelvin</o:LastAuthor>
    <o:Revision>1</o:Revision>
    <o:TotalTime>11</o:TotalTime>
    <o:Created>2012-01-06T02:28:00Z</o:Created>
    <o:LastSaved>2012-01-06T02:39:00Z</o:LastSaved>
    <o:Pages>1</o:Pages>
    <o:Words>102</o:Words>
    <o:Characters>586</o:Characters>
    <o:Company>adnstyle</o:Company>
    <o:Lines>4</o:Lines>
    <o:Paragraphs>1</o:Paragraphs>
    <o:CharactersWithSpaces>687</o:CharactersWithSpaces>
    <o:Version>12.00</o:Version>
   </o:DocumentProperties>
  </xml><![endif]-->
  <!--[if gte mso 9]><xml>
   <w:WordDocument>
    <w:SpellingState>Clean</w:SpellingState>
    <w:GrammarState>Clean</w:GrammarState>
    <w:TrackMoves>false</w:TrackMoves>
    <w:TrackFormatting/>
    <w:DrawingGridHorizontalSpacing>5.5 pt</w:DrawingGridHorizontalSpacing>
    <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
    <w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery>
    <w:ValidateAgainstSchemas/>
    <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
    <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
    <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
    <w:DoNotPromoteQF/>
    <w:LidThemeOther>EN-US</w:LidThemeOther>
    <w:LidThemeAsian>KO</w:LidThemeAsian>
    <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
    <w:Compatibility>
     <w:SpaceForUL/>
     <w:BalanceSingleByteDoubleByteWidth/>
     <w:DoNotLeaveBackslashAlone/>
     <w:ULTrailSpace/>
     <w:DoNotExpandShiftReturn/>
     <w:AdjustLineHeightInTable/>
     <w:BreakWrappedTables/>
     <w:SnapToGridInCell/>
     <w:WrapTextWithPunct/>
     <w:UseAsianBreakRules/>
     <w:DontGrowAutofit/>
     <w:SplitPgBreakAndParaMark/>
     <w:DontVertAlignCellWithSp/>
     <w:DontBreakConstrainedForcedTables/>
     <w:DontVertAlignInTxbx/>2018-04-30
     <w:Word11KerningPairs/>
     <w:CachedColBalance/>
     <w:UseFELayout/>
    </w:Compatibility>
    <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
    <m:mathPr>
     <m:mathFont m:val=Cambria Math/>
     <m:brkBin m:val=before/>
     <m:brkBinSub m:val=&#45;-/>
     <m:smallFrac m:val=off/>
     <m:dispDef/>
     <m:lMargin m:val=0/>
     <m:rMargin m:val=0/>
     <m:defJc m:val=centerGroup/>
     <m:wrapIndent m:val=1440/>
     <m:intLim m:val=subSup/>
     <m:naryLim m:val=undOvr/>
    </m:mathPr></w:WordDocument>
  </xml><![endif]--><!--[if gte mso 9]><xml>
   <w:LatentStyles DefLockedState=false DefUnhideWhenUsed=true
    DefSemiHidden=true DefQFormat=false DefPriority=99
    LatentStyleCount=267>
    <w:LsdException Locked=false Priority=0 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Normal/>
    <w:LsdException Locked=false Priority=9 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=heading 1/>
    <w:LsdException Locked=false Priority=9 QFormat=true Name=heading 2/>
    <w:LsdException Locked=false Priority=9 QFormat=true Name=heading 3/>
    <w:LsdException Locked=false Priority=9 QFormat=true Name=heading 4/>
    <w:LsdException Locked=false Priority=9 QFormat=true Name=heading 5/>
    <w:LsdException Locked=false Priority=9 QFormat=true Name=heading 6/>
    <w:LsdException Locked=false Priority=9 QFormat=true Name=heading 7/>
    <w:LsdException Locked=false Priority=9 QFormat=true Name=heading 8/>
    <w:LsdException Locked=false Priority=9 QFormat=true Name=heading 9/>
    <w:LsdException Locked=false Priority=39 Name=toc 1/>
    <w:LsdException Locked=false Priority=39 Name=toc 2/>
    <w:LsdException Locked=false Priority=39 Name=toc 3/>
    <w:LsdException Locked=false Priority=39 Name=toc 4/>
    <w:LsdException Locked=false Priority=39 Name=toc 5/>
    <w:LsdException Locked=false Priority=39 Name=toc 6/>
    <w:LsdException Locked=false Priority=39 Name=toc 7/>
    <w:LsdException Locked=false Priority=39 Name=toc 8/>
    <w:LsdException Locked=false Priority=39 Name=toc 9/>
    <w:LsdException Locked=false Priority=35 QFormat=true Name=caption/>
    <w:LsdException Locked=false Priority=10 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Title/>
    <w:LsdException Locked=false Priority=1 Name=Default Paragraph Font/>
    <w:LsdException Locked=false Priority=11 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Subtitle/>
    <w:LsdException Locked=false Priority=22 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Strong/>
    <w:LsdException Locked=false Priority=20 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Emphasis/>
    <w:LsdException Locked=false Priority=59 SemiHidden=false
     UnhideWhenUsed=false Name=Table Grid/>
    <w:LsdException Locked=false UnhideWhenUsed=false Name=Placeholder Text/>
    <w:LsdException Locked=false Priority=1 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=No Spacing/>
    <w:LsdException Locked=false Priority=60 SemiHidden=false
     UnhideWhenUsed=false Name=Light Shading/>
    <w:LsdException Locked=false Priority=61 SemiHidden=false
     UnhideWhenUsed=false Name=Light List/>
    <w:LsdException Locked=false Priority=62 SemiHidden=false
     UnhideWhenUsed=false Name=Light Grid/>
    <w:LsdException Locked=false Priority=63 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 1/>
    <w:LsdException Locked=false Priority=64 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 2/>
    <w:LsdException Locked=false Priority=65 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 1/>
    <w:LsdException Locked=false Priority=66 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 2/>
    <w:LsdException Locked=false Priority=67 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 1/>
    <w:LsdException Locked=false Priority=68 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 2/>
    <w:LsdException Locked=false Priority=69 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 3/>
    <w:LsdException Locked=false Priority=70 SemiHidden=false
     UnhideWhenUsed=false Name=Dark List/>
    <w:LsdException Locked=false Priority=71 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Shading/>
    <w:LsdException Locked=false Priority=72 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful List/>
    <w:LsdException Locked=false Priority=73 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Grid/>
    <w:LsdException Locked=false Priority=60 SemiHidden=false
     UnhideWhenUsed=false Name=Light Shading Accent 1/>
    <w:LsdException Locked=false Priority=61 SemiHidden=false
     UnhideWhenUsed=false Name=Light List Accent 1/>
    <w:LsdException Locked=false Priority=62 SemiHidden=false
     UnhideWhenUsed=false Name=Light Grid Accent 1/>
    <w:LsdException Locked=false Priority=63 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 1 Accent 1/>
    <w:LsdException Locked=false Priority=64 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 2 Accent 1/>
    <w:LsdException Locked=false Priority=65 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 1 Accent 1/>
    <w:LsdException Locked=false UnhideWhenUsed=false Name=Revision/>
    <w:LsdException Locked=false Priority=34 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=List Paragraph/>
    <w:LsdException Locked=false Priority=29 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Quote/>
    <w:LsdException Locked=false Priority=30 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Intense Quote/>
    <w:LsdException Locked=false Priority=66 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 2 Accent 1/>
    <w:LsdException Locked=false Priority=67 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 1 Accent 1/>
    <w:LsdException Locked=false Priority=68 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 2 Accent 1/>
    <w:LsdException Locked=false Priority=69 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 3 Accent 1/>
    <w:LsdException Locked=false Priority=70 SemiHidden=false
     UnhideWhenUsed=false Name=Dark List Accent 1/>
    <w:LsdException Locked=false Priority=71 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Shading Accent 1/>
    <w:LsdException Locked=false Priority=72 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful List Accent 1/>
    <w:LsdException Locked=false Priority=73 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Grid Accent 1/>
    <w:LsdException Locked=false Priority=60 SemiHidden=false
     UnhideWhenUsed=false Name=Light Shading Accent 2/>
    <w:LsdException Locked=false Priority=61 SemiHidden=false
     UnhideWhenUsed=false Name=Light List Accent 2/>
    <w:LsdException Locked=false Priority=62 SemiHidden=false
     UnhideWhenUsed=false Name=Light Grid Accent 2/>
    <w:LsdException Locked=false Priority=63 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 1 Accent 2/>
    <w:LsdException Locked=false Priority=64 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 2 Accent 2/>
    <w:LsdException Locked=false Priority=65 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 1 Accent 2/>
    <w:LsdException Locked=false Priority=66 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 2 Accent 2/>
    <w:LsdException Locked=false Priority=67 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 1 Accent 2/>
    <w:LsdException Locked=false Priority=68 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 2 Accent 2/>
    <w:LsdException Locked=false Priority=69 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 3 Accent 2/>
    <w:LsdException Locked=false Priority=70 SemiHidden=false
     UnhideWhenUsed=false Name=Dark List Accent 2/>
    <w:LsdException Locked=false Priority=71 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Shading Accent 2/>
    <w:LsdException Locked=false Priority=72 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful List Accent 2/>
    <w:LsdException Locked=false Priority=73 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Grid Accent 2/>
    <w:LsdException Locked=false Priority=60 SemiHidden=false
     UnhideWhenUsed=false Name=Light Shading Accent 3/>
    <w:LsdException Locked=false Priority=61 SemiHidden=false
     UnhideWhenUsed=false Name=Light List Accent 3/>
    <w:LsdException Locked=false Priority=62 SemiHidden=false
     UnhideWhenUsed=false Name=Light Grid Accent 3/>
    <w:LsdException Locked=false Priority=63 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 1 Accent 3/>
    <w:LsdException Locked=false Priority=64 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 2 Accent 3/>
    <w:LsdException Locked=false Priority=65 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 1 Accent 3/>
    <w:LsdException Locked=false Priority=66 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 2 Accent 3/>
    <w:LsdException Locked=false Priority=67 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 1 Accent 3/>
    <w:LsdException Locked=false Priority=68 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 2 Accent 3/>
    <w:LsdException Locked=false Priority=69 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 3 Accent 3/>
    <w:LsdException Locked=false Priority=70 SemiHidden=false
     UnhideWhenUsed=false Name=Dark List Accent 3/>
    <w:LsdException Locked=false Priority=71 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Shading Accent 3/>
    <w:LsdException Locked=false Priority=72 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful List Accent 3/>
    <w:LsdException Locked=false Priority=73 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Grid Accent 3/>
    <w:LsdException Locked=false Priority=60 SemiHidden=false
     UnhideWhenUsed=false Name=Light Shading Accent 4/>
    <w:LsdException Locked=false Priority=61 SemiHidden=false
     UnhideWhenUsed=false Name=Light List Accent 4/>
    <w:LsdException Locked=false Priority=62 SemiHidden=false
     UnhideWhenUsed=false Name=Light Grid Accent 4/>
    <w:LsdException Locked=false Priority=63 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 1 Accent 4/>
    <w:LsdException Locked=false Priority=64 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 2 Accent 4/>
    <w:LsdException Locked=false Priority=65 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 1 Accent 4/>
    <w:LsdException Locked=false Priority=66 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 2 Accent 4/>
    <w:LsdException Locked=false Priority=67 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 1 Accent 4/>
    <w:LsdException Locked=false Priority=68 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 2 Accent 4/>
    <w:LsdException Locked=false Priority=69 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 3 Accent 4/>
    <w:LsdException Locked=false Priority=70 SemiHidden=false
     UnhideWhenUsed=false Name=Dark List Accent 4/>
    <w:LsdException Locked=false Priority=71 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Shading Accent 4/>
    <w:LsdException Locked=false Priority=72 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful List Accent 4/>
    <w:LsdException Locked=false Priority=73 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Grid Accent 4/>
    <w:LsdException Locked=false Priority=60 SemiHidden=false
     UnhideWhenUsed=false Name=Light Shading Accent 5/>
    <w:LsdException Locked=false Priority=61 SemiHidden=false
     UnhideWhenUsed=false Name=Light List Accent 5/>
    <w:LsdException Locked=false Priority=62 SemiHidden=false
     UnhideWhenUsed=false Name=Light Grid Accent 5/>
    <w:LsdException Locked=false Priority=63 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 1 Accent 5/>
    <w:LsdException Locked=false Priority=64 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 2 Accent 5/>
    <w:LsdException Locked=false Priority=65 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 1 Accent 5/>
    <w:LsdException Locked=false Priority=66 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 2 Accent 5/>
    <w:LsdException Locked=false Priority=67 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 1 Accent 5/>
    <w:LsdException Locked=false Priority=68 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 2 Accent 5/>
    <w:LsdException Locked=false Priority=69 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 3 Accent 5/>
    <w:LsdException Locked=false Priority=70 SemiHidden=false
     UnhideWhenUsed=false Name=Dark List Accent 5/>
    <w:LsdException Locked=false Priority=71 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Shading Accent 5/>
    <w:LsdException Locked=false Priority=72 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful List Accent 5/>
    <w:LsdException Locked=false Priority=73 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Grid Accent 5/>
    <w:LsdException Locked=false Priority=60 SemiHidden=false
     UnhideWhenUsed=false Name=Light Shading Accent 6/>
    <w:LsdException Locked=false Priority=61 SemiHidden=false
     UnhideWhenUsed=false Name=Light List Accent 6/>
    <w:LsdException Locked=false Priority=62 SemiHidden=false
     UnhideWhenUsed=false Name=Light Grid Accent 6/>
    <w:LsdException Locked=false Priority=63 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 1 Accent 6/>
    <w:LsdException Locked=false Priority=64 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Shading 2 Accent 6/>
    <w:LsdException Locked=false Priority=65 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 1 Accent 6/>
    <w:LsdException Locked=false Priority=66 SemiHidden=false
     UnhideWhenUsed=false Name=Medium List 2 Accent 6/>
    <w:LsdException Locked=false Priority=67 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 1 Accent 6/>
    <w:LsdException Locked=false Priority=68 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 2 Accent 6/>
    <w:LsdException Locked=false Priority=69 SemiHidden=false
     UnhideWhenUsed=false Name=Medium Grid 3 Accent 6/>
    <w:LsdException Locked=false Priority=70 SemiHidden=false
     UnhideWhenUsed=false Name=Dark List Accent 6/>
    <w:LsdException Locked=false Priority=71 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Shading Accent 6/>
    <w:LsdException Locked=false Priority=72 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful List Accent 6/>
    <w:LsdException Locked=false Priority=73 SemiHidden=false
     UnhideWhenUsed=false Name=Colorful Grid Accent 6/>
    <w:LsdException Locked=false Priority=19 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Subtle Emphasis/>
    <w:LsdException Locked=false Priority=21 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Intense Emphasis/>
    <w:LsdException Locked=false Priority=31 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Subtle Reference/>
    <w:LsdException Locked=false Priority=32 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Intense Reference/>
    <w:LsdException Locked=false Priority=33 SemiHidden=false
     UnhideWhenUsed=false QFormat=true Name=Book Title/>
    <w:LsdException Locked=false Priority=37 Name=Bibliography/>
    <w:LsdException Locked=false Priority=39 QFormat=true Name=TOC Heading/>
   </w:LatentStyles>
  </xml><![endif]--><!--[if !mso]><object
   classid=clsid:38481807-CA0E-42D2-BF39-B33AF135CC4D id=ieooui></object>
  <style>
  st1\:*{behavior:url(#ieooui) }
  </style>
  <![endif]-->
  <style>
  <!--
   /* Font Definitions */
   @font-face
    {font-family:바탕;
    panose-1:2 3 6 0 0 1 1 1 1 1;
    mso-font-alt:Batang;
    mso-font-charset:129;
    mso-generic-font-family:roman;
    mso-font-pitch:variable;
    mso-font-signature:-1342176593 1775729915 48 0 524447 0;}
  @font-face
    {font-family:Cambria Math;
    panose-1:2 4 5 3 5 4 6 3 2 4;
    mso-font-charset:0;
    mso-generic-font-family:roman;
    mso-font-pitch:variable;
    mso-font-signature:-1610611985 1107304683 0 0 159 0;}
  @font-face
    {font-family:돋움체;
    panose-1:2 11 6 9 0 1 1 1 1 1;
    mso-font-charset:129;
    mso-generic-font-family:modern;
    mso-font-pitch:fixed;
    mso-font-signature:-1342176593 1775729915 48 0 524447 0;}
  @font-face
    {font-family:바탕체;
    panose-1:2 3 6 9 0 1 1 1 1 1;
    mso-font-charset:129;
    mso-generic-font-family:roman;
    mso-font-pitch:fixed;
    mso-font-signature:-1342176593 1775729915 48 0 524447 0;}
  @font-face
    {font-family:\@돋움체;
    panose-1:2 11 6 9 0 1 1 1 1 1;
    mso-font-charset:129;
    mso-generic-font-family:modern;
    mso-font-pitch:fixed;
    mso-font-signature:-1342176593 1775729915 48 0 524447 0;}
  @font-face
    {font-family:\@바탕;
    panose-1:2 3 6 0 0 1 1 1 1 1;
    mso-font-charset:129;
    mso-generic-font-family:roman;
    mso-font-pitch:variable;
    mso-font-signature:-1342176593 1775729915 48 0 524447 0;}
  @font-face
    {font-family:\@바탕체;
    panose-1:2 3 6 9 0 1 1 1 1 1;
    mso-font-charset:129;
    mso-generic-font-family:roman;
    mso-font-pitch:fixed;
    mso-font-signature:-1342176593 1775729915 48 0 524447 0;}
   /* Style Definitions */
   p.MsoNormal, li.MsoNormal, div.MsoNormal
    {mso-style-name:표준\,표준\(양끝\);
    mso-style-unhide:no;
    mso-style-qformat:yes;
    mso-style-parent:;
    margin:0cm;
    margin-bottom:.0001pt;
    text-align:justify;
    text-justify:inter-ideograph;
    line-height:14.0pt;
    mso-pagination:widow-orphan;
    mso-layout-grid-align:none;
    font-size:11.0pt;
    font-family:바탕체,serif;
    mso-hansi-font-family:Times New Roman;
    mso-bidi-font-family:바탕체;}
  span.SpellE
    {mso-style-name:;
    mso-spl-e:yes;}
  .MsoChpDefault
    {mso-style-type:export-only;
    mso-default-props:yes;
    mso-ascii-font-family:맑은 고딕;
    mso-ascii-theme-font:minor-latin;
    mso-bidi-font-family:Times New Roman;
    mso-bidi-theme-font:minor-bidi;}
   /* Page Definitions */
   @page
    {mso-page-border-surround-header:no;
    mso-page-border-surround-footer:no;}
  @page WordSection1
    {size:841.9pt 595.3pt;
    mso-page-orientation:landscape;
    margin:72.0pt 3.0cm 72.0pt 72.0pt;
    mso-header-margin:42.55pt;
    mso-footer-margin:49.6pt;
    mso-paper-source:0;}
  div.WordSection1
    {page:WordSection1;}
  -->
  </style>
  <!--[if gte mso 10]>
  <style>
   /* Style Definitions */
   table.MsoNormalTable
    {mso-style-name:표준 표;
    mso-tstyle-rowband-size:0;
    mso-tstyle-colband-size:0;
    mso-style-noshow:yes;
    mso-style-priority:99;
    mso-style-qformat:yes;
    mso-style-parent:;
    mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
    mso-para-margin:0cm;
    mso-para-margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:10.0pt;
    mso-bidi-font-size:11.0pt;
    font-family:맑은 고딕;
    mso-ascii-font-family:맑은 고딕;
    mso-ascii-theme-font:minor-latin;
    mso-fareast-font-family:맑은 고딕;
    mso-fareast-theme-font:minor-fareast;
    mso-hansi-font-family:맑은 고딕;
    mso-hansi-theme-font:minor-latin;
    mso-font-kerning:1.0pt;}
  </style>
  <![endif]--><!--[if gte mso 9]><xml>
   <o:shapedefaults v:ext=edit spidmax=2050/>
  </xml><![endif]--><!--[if gte mso 9]><xml>
   <o:shapelayout v:ext=edit>
    <o:idmap v:ext=edit data=1/>
   </o:shapelayout></xml><![endif]-->
  </head>

    <body lang=KO style='tab-interval:40.0pt'>
<form name="discription" action="/avanplus/_master/table_ok.asp" method="post">
<%
    If Not rs.eof And Not rs.bof Then
      tablename = ""
      tablecnt = 1
      Do Until Rs.Eof
		
		Response.Flush
		
		
      If tablename <> Rs("table_name") Then

        If tablecnt <> 1 Then

%>
             </table>
           <p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:돋움체'><o:p>&nbsp;</o:p></span></p>
           <p class=MsoNormal><span lang=EN-US style='font-family:돋움체'><o:p>&nbsp;</o:p></span></p>
         </div>
<%
        End if

%>
       <div class=WordSection1>
       <p class=MsoNormal><b><span lang=EN-US style='font-size:12.0pt;font-family:돋움체;mso-bidi-font-family:바탕'><%=tablecnt%>.  <%=rs("table_name")%> <input type="text" name="<%=Rs("table_name")%>#" style="width:50%" value="<%=rs("CD")%>"><input type="submit" value="update" class="update_btn"> <input type="button" value="테이블디자인" class="update_btn" onClick="location.href='?menucode=<%=request.querystring("menucode")%>&path=/avanplus/_master/table.asp&table=<%=rs("table_name")%>'"><o:p></o:p></span></b></p>
       
       <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=870 style='width:853.3pt;margin-left:12.05pt;border-collapse:collapse;mso-padding-alt:0cm 4.95pt 0cm 4.95pt'>
         <% if rs("table_name")=request.QueryString("table") then %>
         <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:17.0pt'>
           <td width=38 nowrap style='width:1.0cm;border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center;line-height:normal;mso-layout-grid-align:auto;mso-vertical-align-alt:auto'><span lang=EN-US style='font-family:돋움체;mso-bidi-font-family:바탕'>No</span><span lang=EN-US style='font-family:돋움체'><o:p></o:p></span></p>
           </td>
           <td width=157 style='width:117.55pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center;line-height:normal;mso-layout-grid-align:auto;mso-vertical-align-alt:auto'><span class=SpellE><span style='font-family:돋움체;mso-bidi-font-family:바탕'>컬럼명</span></span><span lang=EN-US style='font-family:돋움체'><o:p></o:p></span></p>
           </td>
           <td width=156 style='width:200.0pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span style='font-family:돋움체;mso-bidi-font-family:바탕'>설명</span><span lang=EN-US style='font-family:돋움체'><o:p></o:p></span></p>
           </td>
           <td width=108 style='width:81.0pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span style='font-family:돋움체;mso-bidi-font-family:바탕'>타입</span><span lang=EN-US style='font-family:돋움체'><o:p></o:p></span></p>
           </td>
           <td width=48 style='width:36.0pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span style='font-family:돋움체;mso-bidi-font-family:바탕'>길이</span><span lang=EN-US style='font-family:돋움체'><o:p></o:p></span></p>
           </td>
           <td width=48 style='width:70.0pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span style='font-family:돋움체;mso-bidi-font-family:바탕'>기본값</span><span lang=EN-US style='font-family:돋움체'><o:p></o:p></span></p>
           </td>
           <td width=36 style='width:27.0pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-family:돋움체;mso-bidi-font-family:바탕'>PK<o:p></o:p></span></p>
           </td>
           <td width=36 style='width:27.0pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-family:돋움체;mso-bidi-font-family:바탕'>FK<o:p></o:p></span></p>
           </td>
           <td width=36 style='width:27.0pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><st1:place w:st='on'><st1:country-region w:st='on'><span lang=EN-US style='font-family:돋움체;mso-bidi-font-family:바탕'>UK</span></st1:country-region></st1:place><span lang=EN-US style='font-family:돋움체;mso-bidi-font-family:바탕'><o:p></o:p></span></p>
           </td>
           <td width=36 style='width:27.0pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-family:돋움체;mso-bidi-font-family:바탕'>NN<o:p></o:p></span></p>
           </td>
           <td width=70 style='width:93.3pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-family:돋움체;mso-bidi-font-family:바탕'>FK </span><span class=SpellE><span style='font-family:돋움체;mso-bidi-font-family:바탕'>테이블명</span></span><span lang=EN-US style='font-family:돋움체'><o:p></o:p></span></p>
           </td>
           <td width=70 style='width:92.1pt;border:solid windowtext 1.0pt;border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;background:#E6E6E6;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-family:돋움체;mso-bidi-font-family:바탕'>FK </span><span class=SpellE><span style='font-family:돋움체;mso-bidi-font-family:바탕'>컬럼명</span></span><span lang=EN-US style='font-family:돋움체'><o:p></o:p></span></p>
           </td>
         </tr>
         <% end if %>
         <%
      End If
        %>
		 <% if rs("table_name")=request.QueryString("table") then %>
         <tr style='mso-yfti-irow:1;height:17.0pt'>
           <td width=38 nowrap style='width:1.0cm;border:solid windowtext 1.0pt;border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체;mso-bidi-font-family:바탕'><%=Rs("ordinal_position")%><o:p></o:p></span></p>
           </td>
           <td width=157 style='width:117.55pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=left style='text-align:left'><span lang=EN-US style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:돋움체'><%=rs("column_name")%><o:p></o:p></span></p>
           </td>
           <td width=156 style='width:200.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <input type="text" name="<%=Rs("table_name")%>#<%=rs("column_name")%>" style="width:100%" value="<%=rs("CD")%>"><p class=MsoNormal align=left style='text-align:left'><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:돋움체'><span lang=EN-US><o:p></o:p></span></span></p>
           </td>
           <td width=108 style='width:81.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-size:10.0pt;mso-bidi-font-size:9.0pt;font-family:돋움체'><%=rs("data_type")%><o:p></o:p></span></p>
           </td>
           <td width=48 style='width:36.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체'><% If rs("data_type") = "image" Or rs("data_type") = "text" Then response.write "" Else response.write rs("character_maximum_length") End If %><o:p></o:p></span></p>
           </td>
           <td width=36 style='width:70.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체'><%=rs("column_default")%><o:p></o:p></span></p>
           </td>
           <td width=36 style='width:27.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체'><%=rs("PK")%><o:p></o:p></span></p>
           </td>
           <td width=36 style='width:27.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체'><%=rs("FK")%><o:p>&nbsp;</o:p></span></p>
           </td>
           <td width=36 style='width:27.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체'><o:p>&nbsp;</o:p></span></p>
           </td>
           <td width=36 style='width:27.0pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체'><%If rs("is_nullable") = "NO" Then response.write "Y" End if%><o:p>&nbsp;</o:p></span></p>
           </td>
           <td width=70 style='width:93.3pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=left style='text-align:left'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체'><%=rs("FK_table")%><o:p>&nbsp;</o:p></span></p>
           </td>
           <td width=70 style='width:92.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;padding:0cm 4.95pt 0cm 4.95pt;height:17.0pt'>
             <p class=MsoNormal align=left style='text-align:left'><span lang=EN-US style='font-size:10.0pt;font-family:돋움체'><%=rs("FK_column")%><o:p>&nbsp;</o:p></span></p>
           </td>
         </tr>
         <% end if %>
        <%
      If tablename <> Rs("table_name") Then
        tablename = Rs("table_name")
        tablecnt  = tablecnt + 1
      End If

      response.write html
      response.flush

      Rs.MoveNext
      Loop
    End if


  End Sub
%>

</form>
