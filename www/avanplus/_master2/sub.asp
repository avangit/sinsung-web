<!--#include virtual = "/AVANplus/BuilderStart.asp" -->
<%

	if len(session("userid")) = 0 then response.Redirect("default.asp")


	'//전역변수 설정
	dim menuTitle, menuCallURL, submenuCode, ThisMenuCode, menuGroup
	submenuCode = 10
	menuTitle = "데쉬보드"
	menuCallURL = "dashboard.asp"

	Sub menu(byval callTitle, byval callURL, byval addurl)
		submenuCode = submenuCode + 1
		'// 메뉴코드만들기
		ThisMenuCode = menuGroup&"s"&submenuCode

		if ThisMenuCode = request.QueryString("menucode") then
			menuTitle = callTitle
			menuCallURL = callURL

			%><li class="active"><a href="?menucode=<%=ThisMenuCode%>&path=<%=callURL%>&<%=addurl%>">>> <%=callTitle%></a></li><%
		else
			%><li><a href="?menucode=<%=ThisMenuCode%>&path=<%=callURL%>&<%=addurl%>">- <%=callTitle%></a></li><%
		end if

	End Sub

%>
<!doctype html>
<html lang="ko">
<head>
<title>홈페이지 관리자</title>
<meta charset="UTF-8">
<meta name="Author" content="">
<meta name="Keywords" content="">
<meta name="Description" content="">
<link rel="stylesheet" href="css/style.css" />
<script type="text/javascript" src="js/jquery-limenu.min.js"></script>
<script type="text/javascript" src="js/jquery.navgoco.js"></script>
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
<div id="container">
<!-- s: header -->
<div id="header">
	<div class="logo">
		<h1 class="mainfont"><a href="http://demo.avansoft.co.kr/avanplus/_master/sub.asp">Administrator System</a></h1>
	</div>
	<div class="menu">
		<div class="area">
			<div class="search"><input type="text" placeholder="게시물검색"><a href="#"><img src="img/imgSearch.png" alt="" /></a></div>
			<ul class="topMenu">					
				<li><span>관리자님 반갑습니다.</span></li>
				<li><a href="http://sadmin.co.kr" target="_blank" class="btnW">나의 계정관리</a></li>
				<li><a href="/" target="_blank" class="btnB">나의 홈페이지</a></li>
				<li><a href="logOut.asp" class="btnW logout"><img src="img/bgLogout.png" alt="">로그아웃</a></li>
				<li><a href="?menucode=m00s00&	member4=modify" class="btnW logout">내정보수정</a></li>
			</ul>
		</div>	
		<nav class="area_bottom">
		  <ul class="topQuick">
			  <li class="first-child"><a href="?" class="active">데쉬보드</a></li>
			  <li><a href="http://work.avansoft.co.kr" target="_blank">작업게시판(유지보수)</a></li>
			  <li><a href="http://sadmin.co.kr" target="_blank">계정관리시스템(호스팅)</a></li>
			  <li><a href="http://www.avansoft.co.kr" target="_blank">(주)아반소프트</a></li>
			  <li><a href="/" target="_blank">내홈페이지 바로가기</a></li>
		</ul>
		</nav>
	</div><!-- menu End -->
</div>
<!-- e: header -->

<!-- s: body -->
<div id="body">	
	<div class="leftWrap">
		<div class="lnb">
			<h2>ID : <%=split(split(cstConnString,"=")(3),";")(0)%></h2>
			<ul class="nav">


				<!-- <% menuGroup = "m02" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">레이아웃작업중..</a>
					<ul>
                        <%
							call menu("베이직0","/avanmodule/basic0/call.asp","")
							call menu("베이직2","/avanmodule/basic2/call.asp","")
							call menu("베이직3","/avanmodule/basic3/call.asp","")
						%>
					</ul>
				</li> -->


				<% menuGroup = "m03" %>
								<li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">운영관리</a>
					<ul>
						   <%

							'call menu("팝업관리","/AVANplus/modul/mdl_Popup_v1/Calladmin.asp","")
                        	'						dim i
							'For i = 1 to 100
							'	if len(oBprint("str_Bmenu0"&i&"_url")) > 0 then

							'		call menu(oBprint("str_Bmenu0"&i),oBprint("str_Bmenu0"&i&"_url"),"")

							'	end if
							'Next
							
							
								call menu("우리는 운동중","/avanmodule/tbl_gallery1/call.asp","")
								call menu("지점관리","/avanmodule/tbl_store/call.asp","")
								call menu("메인배너","/avanmodule/tbl_main/call.asp","")
								call menu("메인동영상","/avanmodule/tbl_video/call.asp","")
								call menu("자주묻는 질문","/avanmodule/faq/call.asp","")
						  %>
					</ul>
				</li>

                <% menuGroup = "m72" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">게시판관리</a>
					<ul>
                        <%
							dim boardrs
							set boardrs = dbconn.execute("select * from boardset_v2")
							
							if not boardrs.eof then
								do while not boardrs.eof
									call menu(boardrs("bs_name"),"/AVANplus/modul/avanboard_v3/call2.asp","bs_code="&boardrs("bs_code"))
								boardrs.movenext
								loop
							end If
							
							
							call menu("비포에프터","/avanmodule/tbl_before/call.asp","")
							call menu("무료 수업 신청","/avanmodule/tbl_request/call.asp","")
						%>
					</ul>
				</li>
		
				<% menuGroup = "m73" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">메뉴구성관리</a>
					<ul>
                        <%
							'call menu("사이트맵","/AVANplus/sol_builder/CallsitemapUser.asp","")
							call menu("메뉴관리","/avanplus/sol_builder/callSiteMenu.asp","")
							call menu("메뉴 내용(콘텐츠)관리","/AVANplus/sol_builder/CallSiteEdituser.asp","HOMEPCATE=1000000000")
							'call menu("메인/서브 이미지관리","/AVANplus/sol_builder/CallSiteImage.asp","")
							'call menu("롤오버 이미지관리","/AVANplus/sol_builder/CallSiteTitleimg.asp","")
							'call menu("베너그룹관리","/AVANplus/modul/banner/Calladmin.asp","")
						%>
					</ul>
				</li>
					<!--
				<% menuGroup = "m74" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">홈페이지 환경설정</a>
					<ul>
                        <%
							call menu("기본환경설정","/avanplus/sol_builder/builder_set/call_basic.asp","")
							call menu("회원가입약관","/avanplus/sol_builder/builder_set/Call_joinraw.asp","")
							call menu("메타태그설정","/avanplus/sol_builder/builder_set/call_meta.asp","")
							call menu("SMS설정","/avanplus/sol_builder/builder_set/call_sms.asp","")
							call menu("SSL설정","/avanplus/sol_builder/builder_set/call_ssl.asp","")
							call menu("시스템ID","/avanplus/sol_builder/builder_set/call_id.asp","")
							call menu("추가모듈 관리","/avanplus/sol_builder/builder_set/call_addModule.asp","")
							call menu("게시판관리","/AVANplus/modul/avanboard_v3/calladmin.asp","")
						%>
					</ul>
				</li>

				<% menuGroup = "m80" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">쇼핑몰관리</a>
					<ul>
                        <%
							call menu("적립금현황","/avanmall/point/call.asp","")
							call menu("매출통계","/avanmall/sale/call.asp","")
							call menu("1:1문의관리","/avanmall/Inquiry/call.asp","")
							call menu("상품후기관리","/avanmall/good_reply/call.asp","")
							call menu("판매상품관리","/avanmall/goods/call.asp","")
							call menu("신규상품등록","/avanmall/goods/call.asp","strGoodsMode=GoodsAdd")
							call menu("쇼핑몰 카테고리관리","/avanmall/cate/cateadmin.asp","")
						%>
					</ul>
				</li>
                <% menuGroup = "m81" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">주문관리</a>
					<ul>
                        <%
							call menu("요약정보","/avanmall/order/call.asp","strOrderMode=toplist")
							call menu("전체리스트","/avanmall/order/call.asp","strOrderMode=list&&result=all")
							call menu("신규[미입금]리스트","/avanmall/order/call.asp","strOrderMode=list&result=1")
							call menu("배송준비 리스트","/avanmall/order/call.asp","strOrderMode=list&result=2")
							call menu("배송중 리스트","/avanmall/order/call.asp","strOrderMode=list&result=3")
							call menu("반품 리스트","/avanmall/order/call.asp","strOrderMode=list&result=4")
							call menu("취소 리스트","/avanmall/order/call.asp","strOrderMode=list&result=5")
							call menu("환불 리스트","/avanmall/order/call.asp","strOrderMode=list&result=6")
							call menu("거래취소 리스트","/avanmall/order/call.asp","strOrderMode=list&result=30")
							call menu("정상완료 리스트","/avanmall/order/call.asp","strOrderMode=list&result=40")
						%>
					</ul>
				</li>

                <% menuGroup = "m89" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">DB Tool</a>
					<ul>
                        <%
							call menu("bSET(홈페이지변수설정)","/avanmodule/_bSET/call.asp","")
							call menu("코드관리","/avanmodule/tbl_code/call.asp","")
							call menu("테이블명세서","/avanplus/_master/table.asp","")
							call menu("테이블레이아웃 베이직0","/avanmodule/basic0/call.asp","")
							call menu("테이블레이아웃 베이직2","/avanmodule/basic2/call.asp","")
							call menu("테이블레이아웃 베이직3","/avanmodule/basic3/call.asp","")
						%>
					</ul>
				</li>
				-->
                <% menuGroup = "m99" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">고객센터</a>
					<ul>
                        <li><a href="http://work.avansoft.co.kr" target="_blank">- 작업게시판(유지보수)</a></li>
                        <li><a href="http://sadmin.co.kr" target="_blank">- 계정관리시스템</a></li>
                        <li><a href="http://avansoft.co.kr" target="_blank">- 아반소프트 홈페이지</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<script type="text/javascript">
		$(document).ready(function() {
			$(".nav").navgoco({accordion: true});
		});
		</script>
	</div>	

	<div class="rightWrap" id="contents">
		<h2><%=menuTitle%></h2>
		<div class="contents">

			<%server.Execute(menuCallURL)%>
            <% if request.QueryString("menucode") = "m00s00" then server.Execute("/AVANplus/modul/member4/calladmin.asp")%>

		</div>
         <div class="tip_box">
        <p>설명입니다.</p></div>
	</div>
</div>
<!-- e: body -->

<!-- s: footer -->
<div id="footer">
	<div class="copyright">COPYRIGHT (C) A2014 APP.PEOPLE (주)앱피플. ALL RIGHTS RESERVED.</div>
</div>
<!-- e: footer -->
</div>
</body>
</html>
<!--#include virtual = "/AVANplus/BuilderEnd.asp" -->
