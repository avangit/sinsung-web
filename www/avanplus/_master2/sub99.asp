<!--#include virtual = "/AVANplus/BuilderStart.asp" -->
<%

	if len(session("userid")) = 0 then response.Redirect("default.asp")


	'//전역변수 설정
	dim menuTitle, menuCallURL, submenuCode, ThisMenuCode, menuGroup
	submenuCode = 10
	menuTitle = "데쉬보드"
	menuCallURL = "dashboard.asp"

	Sub menu(byval callTitle, byval callURL, byval addurl)
		submenuCode = submenuCode + 1
		'// 메뉴코드만들기
		ThisMenuCode = menuGroup&"s"&submenuCode

		if ThisMenuCode = request.QueryString("menucode") then
			menuTitle = callTitle
			menuCallURL = callURL

			%><li class="active"><a href="?menucode=<%=ThisMenuCode%>&path=<%=callURL%>&<%=addurl%>">>> <%=callTitle%></a></li><%
		else
			%><li><a href="?menucode=<%=ThisMenuCode%>&path=<%=callURL%>&<%=addurl%>">- <%=callTitle%></a></li><%
		end if

	End Sub

%>
<!doctype html>
<html lang="ko">
<head>
<title>홈페이지 관리자</title>
<meta charset="UTF-8">
<meta name="Author" content="">
<meta name="Keywords" content="">
<meta name="Description" content="">
<link rel="stylesheet" href="css/style.css" />
<script type="text/javascript" src="js/jquery-limenu.min.js"></script>
<script type="text/javascript" src="js/jquery.navgoco.js"></script>
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
<div id="container">
<!-- s: header -->
<div id="header">
	<ul class="topMenu">
		<li><span>관리자</span>님 반갑습니다.</li>
		<li><a href="http://sadmin.co.kr" target="_blank" class="btnW">나의 계정관리</a></li>
		<li><a href="/" target="_blank" class="btnB">나의 홈페이지</a></li>
        <li><a href="logOut.asp" class="btnW logout"><img src="img/bgLogout.png" alt="">로그아웃</a></li>
        <li><a href="?menucode=m00s00&	member4=modify" class="btnW logout">내정보수정</a></li>
	</ul>
	<h1>Administrator System</h1>



    <div class="search"><input type="text" placeholder="게시물검색"><a href="#"><img src="img/imgSearch.png" alt="" /></a></div>
</div>
<!-- e: header -->

<!-- s: body -->
<div id="body">
	<div class="leftWrap">
		<div class="lnb">
			<h2>ID : <%=split(split(cstConnString,"=")(3),";")(0)%></h2>
			<ul class="nav">


				<!-- <% menuGroup = "m02" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">레이아웃작업중..</a>
					<ul>
                        <%
							call menu("베이직0","/avanmodule/basic0/call.asp","")
							call menu("베이직2","/avanmodule/basic2/call.asp","")
							call menu("베이직3","/avanmodule/basic3/call.asp","")
						%>
					</ul>
				</li> -->


				<% menuGroup = "m73" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">세미나</a>
					<ul>
                        <%
							call menu("세미나등록","/avanmodule/seminar/call.asp","")
							call menu("세미나신청관리","/avanmodule/seminar/allcall.asp","")
						%>
					</ul>
				</li>



                <% menuGroup = "m99" %>
                <li <%if left(request.QueryString("menucode"),3)=menuGroup then %>class="open"<%end if%>><a href="#">고객센터</a>
					<ul>
                        <li><a href="http://work.avansoft.co.kr" target="_blank">- 작업게시판(유지보수)</a></li>
                        <li><a href="http://sadmin.co.kr" target="_blank">- 계정관리시스템</a></li>
                        <li><a href="http://avansoft.co.kr" target="_blank">- 아반소프트 홈페이지</a></li>
					</ul>
				</li>

			</ul>
		</div>
		<script type="text/javascript">
		$(document).ready(function() {
			$(".nav").navgoco({accordion: true});
		});
		</script>
	</div>

	<div class="rightWrap" id="contents">
		<h2><%=menuTitle%></h2>
		<div class="contents">

			<%server.Execute(menuCallURL)%>
            <% if request.QueryString("menucode") = "m00s00" then server.Execute("/AVANplus/modul/member4/calladmin.asp")%>

		</div>
         <div class="tip_box">
        <p>설명입니다.</p></div>
	</div>
</div>
<!-- e: body -->

<!-- s: footer -->
<div id="footer">
	<div class="copyright">COPYRIGHT (C) A2014 APP.PEOPLE (주)앱피플. ALL RIGHTS RESERVED.</div>
</div>
<!-- e: footer -->
</div>
</body>
</html>
<!--#include virtual = "/AVANplus/BuilderEnd.asp" -->
