<!--#include file ="dbconn.asp"-->
<% 
	'#############################################################################
	'
	'	프로그램 라이브러리 파일 v1.0
	'
	'	작성자 : 이덕권 (leedk7@msn.com)
	'
	'#############################################################################
	'[중요]dim strConnect DB연결 변수 페이지 상단에 설정 할것.
	
	
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' 내    용 : 클라이언트에서 파일을 항상 서버에서 가져오게 한다.
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	Response.Expires = -1500
	Response.AddHeader "Pragma", "No-Cache"
	Response.AddHeader "Cache-Control", "No-Store"
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	''	경로 변수의 선언
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	dim backURL, pageURL, Physical_PATH
	' 전체 경로 추출
	backURL	= request.serverVariables("HTTP_REFERER")
	backURL	= replace(backURL,"www.","")
	backURL	= replace(backURL,"WWW.","")
	backURL	= lcase(backURL)

	pageURL	= lcase(request.serverVariables("URL"))
	
	Physical_PATH = request.serverVariables("PATH_TRANSLATED")
	
	
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
	'[텍스트 함수 설정]
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' 모든 입력 스트링 문자열에 적용 - 입력시
	function all_input(str)
      str = replace(str, "'", "''")
      'str = replace(str, "  ", "&nbsp;&nbsp;")
      all_input = str
	End Function
	
	function Use_Text(str)
      str = replace(str, "&", "&amp;")
      str = replace(str, "<", "&lt;")
      str = replace(str, ">", "&gt;")
			str = replace(str,chr(13)&chr(10),"<br>")
			str = replace(str, "  ", "&nbsp;&nbsp;")
      Use_Text = str
	End Function
	
	function Use_Html(str)
      str = replace(str, "&lt;", "<")
      str = replace(str, "&gt;", ">")
	  	str = replace(str, "&quot;", "'")
	  	str = replace(str, "&amp;", "&")
      Use_Html = str
	End Function

	Function Use_HtmlBR(ByVal str)	
	'## html이용 <br>태그 자동 삽입
		str = replace(str,chr(13)&chr(10),"<br>")
		str = replace(str, "  ", "&nbsp;&nbsp;")
      	Use_HtmlBR = str
	End Function


Function GetString(ByVal addString)
	'##	현재페이지의 스트링값을 받는 함수

      	dim UrlString, split_UrlString, i_UrlString, chkword_UrlString
		dim split_addString, i_addString, chkword_addString
		dim stringEquleChk
		UrlString = request.serverVariables("QUERY_STRING")

		if trim(UrlString)="" or isnull(trim(UrlString)) then
			GetString = addString
		elseif not isnull(trim(urlString)) and isnull(trim(addString)) then
			GetString = urlString
		else
		
			split_UrlString = split(UrlString,"&")
			split_addString = split(addString,"&")
			
		
'response.Write Ubound(split_UrlString) & "/"
'response.Write Ubound(split_addString)
			For i_UrlString = 0 to Ubound(split_UrlString)
				'aaa=123
				if split_UrlString(i_UrlString) <> "" then
					chkword_UrlString = split(split_UrlString(i_UrlString),"=")(0)
					chkword_UrlString = lcase(chkword_UrlString)
					chkword_UrlString = trim(chkword_UrlString)
					stringEquleChk = "n"
					For i_addString = 0 to Ubound(split_addString)
						chkword_addString = split(split_addString(i_addString),"=")(0)
						chkword_addString = lcase(chkword_addString)
						chkword_addString = trim(chkword_addString)
						if chkword_UrlString = chkword_addString then
							stringEquleChk = "y"					
						end if
					'response.Write Ubound(split_addString)
					'response.Write("["&chkword_UrlString&"/"&chkword_addString&"|"&stringEquleChk&"]")
					next
					if stringEquleChk = "n" then
						GetString = GetString & split_UrlString(i_UrlString) & "&"
						
					end if
					'response.Write("///"&i_UrlString&""&i_addString&stringEquleChk)
				end if
			next
			
			GetString = GetString & addString & "#avan"
		end if
		'GetString = addString
	End Function
	
	
	
	


	Function BackGetString(ByVal addString)
	'##	처린이전페이지로 쿼리스트링 문자열 값을 넘기는 함수
		dim ReturnUrlPath
			'리턴되는 페이지 이전 파일
			if request.ServerVariables("HTTP_REFERER") <> "" then
				ReturnUrlPath = split( request.ServerVariables("HTTP_REFERER"),"?")(0)
			end if
		
		BackGetString = ReturnUrlPath & "?" & getstring(addString)
		
	End Function





	
	'################################################################
	'	Function Name : FileSave,  FileInfo
	'	설명 : 파일저장 관련 설정값
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' 본 함수는 하일 UploadForm(file1)을 저장하고
	'	파일의 이름과 크기와 type을 리턴한다. 
	' 파일의 크기를 제한 하려면 MaxSize값을 지정하고
	'	파일의 확장자 업로드 제한을 하려면 FileTypeChk에 저장가능 확장자를 명기한다.
	'################################################################
	dim SaveFolder, DirPath
	SaveFolder	= "../../UpLoad/"

	
	'DirPath = server.MapPath( SaveFolder ) 서버에서 mappath를 인식하지 못해서 다음으로 설정
	DirPath = "E:\newhome\upload\"
	
	
	dim filename			'파일 이름 리턴
	dim filetype			'파일 타입 리턴
	dim filesize			'파일 용량 리턴
	
	dim MaxSize 			'저장 가능 최대 용량 M단위
	dim FileTypeChk		'저장 가능한 파일 타입

	dim filepath
	dim FileSave_Errer
	dim LastSavedFileName '마지막으로 파일이 저장될 이름
	
	Function FileSave() '최대 업로드 용량, 업로드 가능한 파일타입 - 업로드파일은 반드시uploadform(file1)으로 기술

	filename	= uploadform("file1").filename
	filesize	= uploadform("file1").filelen
	filetype	= split(filename,".")(1)

	' ##### 파일 유효성 체크
	' ##크기 확인
	'if MaxSize = "" then
	'	FileSave_Errer = "" 
	'elseif FileSize < MaxSize*1024*1024 then
	'	FileSave_Errer = "" 
	'elseif FileSize > MaxSize*1024*1024 then
	'	FileSave_Errer = MaxSize & "M 이상의 파일은 업로드 할수 없습니다."
	'end if
	
	' ##확장자 확인

	'if FileTypeChk = "" then
	'	FileSave_Errer = ""
	'elseif FileTypeChk <> "" then
	'	FileSave_Errer = "파일 확장자 "& FileType & "은 제한된 업로드 확장자 입니다."
 	' 파일을 나눈다
	'	FileTypeChk	= split(FileTypeChk,",")
	'	dim i
	'	For i = 0 to UBound(FileTypeChk)		
	'		if Ucase(FileTypeChk(i)) = Ucase(FileType) then
	'			FileSave_Errer = ""
	'		end if
	'	Next
	'end if
	
	
	' ##### 파일 저장 - 유효성 조건 만족시
	if FileSave_Errer = "" then
		FileSave = uploadform.DefaultPath & "\" & filename
		uploadform("file1").SaveAs FileSave, False		'고유이름으로 저장
	end if
	
	LastSavedFileName = uploadform("file1").LastSavedFileName ' 마지막으로 저장된 이름
	End Function
	
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
	'	Function Name : query
	'	설명 : 간단한 쿼리문 생성기
	'	주의
	'	- 자료는 테이블 명과 함께 컬렉션으로 넘길것
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'set query_str = CreateObject("Scripting.Dictionary")
	dim key, query_str, Query_Fild_name, Query_fild_Values
	Function query(tablename, ioud ,query_str, Data_Array, where, targetURL) ' (테이블이름, 입력/출력/업데이트등..선택 ,쿼리문-컬렉션으로 입력, 정렬, where
		' 문자열을 변수와 변수값으로 분류.. 코드 작성
		'-----------------------------------------------------	
		'### insert
		'-----------------------------------------------------
		if Ucase(ioud) = "INSERT" then
			Query_Fild_name = "("	
			Query_Fild_Values =  "("
			
			For Each key In query_str
				if Query_Fild_name = "(" then
				Query_Fild_name = Query_Fild_name & key 
				else
					if Ucase(key) = "FILE1" and query_str(key) <> "" then
						Query_Fild_name = Query_Fild_name & ",filename"
						Query_Fild_name = Query_Fild_name & ",filetype"
						Query_Fild_name = Query_Fild_name & ",fileSize"
					elseif Ucase(key) <> "FILE1" then
						Query_Fild_name = Query_Fild_name & "," & key 
					end if
				end if

				query_str(key) = all_input(query_str(key)) '문자열에서 ' --> '' 로 치환
				if Query_fild_Values =  "(" then
				Query_Fild_Values = Query_Fild_Values & "'" & query_str(key)  & "'"
				else
					if Ucase(key) = "FILE1" and query_str(key) <> "" then
						FileSave()	'넘어온 값이 파일인 경우에만 처리 - 파일을 저장하고 값 리턴
						Query_Fild_Values = Query_Fild_Values & ",'" & LastSavedFileName  & "'"
						Query_Fild_Values = Query_Fild_Values & ",'" & filetype  & "'"
						Query_Fild_Values = Query_Fild_Values & ",'" & fileSize  & "'"
					elseif Ucase(key) <> "FILE1" then
						Query_Fild_Values = Query_Fild_Values & ",'" & query_str(key)  & "'"
					end if
				end if
			Next				

			Query_Fild_name = Query_Fild_name & ")"	
			Query_fild_Values = Query_Fild_Values & ")"
						
			query = "insert into " & tablename & Query_Fild_name & " Values" & Query_fild_Values
			
			' db 에저장
			' 파일이 있는경우가 있기 때문에 FileSave_Errer = ""경우만 디비에 저장
			if FileSave_Errer = "" then
			call DBopen
			DBconn.execute(query)
			DBconn.close
			end if
			
		'-----------------------------------------------------	
		'### select
		'-----------------------------------------------------
		elseif Ucase(ioud) = "SELECT" then
		
		response.Write("select 문은 아직 지원하지 않습니다.")
		
		
		'-----------------------------------------------------
		'### update
		'-----------------------------------------------------	
		elseif Ucase(ioud) = "UPDATE" then
			dim Query_update
			For Each key In query_str
				query_str(key) = all_input(query_str(key)) '문자열에서 ' --> '' 로 치환
				if Query_update =  "" then
				Query_update = Query_update & key  & "='" & query_str(key)  & "'"
				else
					if Ucase(key) = "FILE1" and query_str(key) <> "" then
						FileSave()	'넘어온 값이 파일인 경우에만 처리 - 파일을 저장하고 값 리턴
						Query_update = Query_update & ", filename ='" & LastSavedFileName  & "'"
						Query_update = Query_update & ", filetype ='" & filetype  & "'"
						Query_update = Query_update & ", filesize ='" & fileSize  & "'"
					elseif Ucase(key) <> "FILE1" then
						Query_update = Query_update & ","& key  & "='" & query_str(key)  & "'"
					end if
				
				end if
			Next		
		
			if where = "" then
				'where 구문이 없으면 모든 행이 적용되므로 처리를 하지 않음
				'query = "update " & tablename & " set " & Query_update 
			else 
				query = "update " & tablename & " set " & Query_update & " where "& where
			end if
			' db 에저장
			' 파일이 있는경우가 있기 때문에 FileSave_Errer = ""경우만 디비에 저장
			if FileSave_Errer = "" then
			call DBopen
			DBconn.execute(query)
			DBconn.close
			end if
			
		'-----------------------------------------------------	
		'### delete
		'-----------------------------------------------------	
		elseif Ucase(ioud) = "DELETE" then
			if where = "" then
				'where 구문이 없으면 모든 행이 적용되므로 처리를 하지 않음
				'query = "delete from " & tablename 
			else 
				query = "delete from " & tablename & " where "& where 
			end if
			
			
			' db 에저장
			' 파일이 있는경우가 있기 때문에 FileSave_Errer = ""경우만 디비에 저장
			if FileSave_Errer = "" then
			calldbopen
			DBconn.execute(query)
			DBconn.close
			end if
		
		
		end if
	End Function











	'#############################################################################
	'
	'	업로드된 이지지 파일등의 정보를 리턴하는 함수모음
	'
	'#############################################################################

	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
	'	Function Name : GetImageSize
	'	설명 : 이미지 파일의 넓이값을 리턴한다.
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	Function GetImageSize(ByVal FileSavedFolder, ByVal filename, ByRef x, ByRef y)
	'							폴더의 경로				파일명		
		
		if trim(filename) <> "" then
			
			Dim p, f
			f = Server.MapPath( FileSavedFolder ) & "\" & filename
			'response.Write("<br><br><br>"&f)
			Set p = LoadPicture(f)
			x = CLng(CDbl(p.Width) * 24 / 635) ' CLng 의 라운드 오프의 기능을 이용하고 있다.
			y = CLng(CDbl(p.Height) * 24 / 635) ' CLng 의 라운드 오프의 기능을 이용하고 있다.
			Set p = Nothing
			GetImageSize = x
		
		end if
	End Function
	
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
	'	Function Name : ImgLmtView
	'	설명 : 이미지 보여주기
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function ImgLmtView(FileSavedFolder, imgFileName, MaxWidth)

		
		'최대 크기와의 비교
		if MaxWidth < GetImageSize(FileSavedFolder, imgFileName, "","") then
		ImgLmtView = "<a href='"&FileSavedFolder&imgFileName&"' target='_blank'><img src='"&FileSavedFolder&imgFileName&"' width='"&MaxWidth&"' border=0></a><br>"
		else
		ImgLmtView = "<a href='"&FileSavedFolder&imgFileName&"' target='_blank'><img src='"&FileSavedFolder&imgFileName&"' border=0></a><br>"
		end if
		
		if imgFileName = "" or isnull(imgFileName) then ImgLmtView = "" end if
		
	End Function
	
	
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
	'	Function Name : FileDown
	'	설명 : 파일의 다운로드
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	Function FileDown(FilePath, filename)
		'Response.Buffer = False 
		'filepath = Request.QueryString("file") 
		FilePath = Server.MapPath( FilePath ) & "\" & filename
		'filename = Mid(filepath, InStrRev("\")+1) 
		
		Response.AddHeader "Content-Disposition","inline;filename=" & filename 
		
		set objFS = Server.CreateObject("Scripting.FileSystemObject") 
		set objF = objFS.GetFile(filepath) 
		Response.AddHeader "Content-Length", objF.Size 
		set objF = nothing 
		set objFS = nothing 
		
		Response.ContentType = "application/x-msdownload" 
		Response.CacheControl = "public" 
		
		Set objDownload = Server.CreateObject("DEXT.FileDownload") 
		objDownload.Download filepath 
		Set uploadform = Nothing 

	
	'다운로드할 파일의 전체경로와 함께 상기 ASP페이지를 실행하면 해당 파일을 다운로드 받을 수 있게 된다. 페이지 
	'상단을 보면 Response.Buffer = False 로 지정하여 버퍼링 없이 즉시 파일을 다운로드 받을 수 있게 했고, Content- 
	'Disposition, Content-Length, ContentType 등의 헤드정보를 지정한 후 Download Method를 호출하고 있다. 사용자는 
	'웹브라우저에 나타나는 대화상자에 다운로드할 경로와 파일명을 지정하여 다운로드 받을 수 있다. 
	
	End Function


	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
	'	Function Name : FileView
	'	설명 : 파일의 처리
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	Function FileView(FileSavedFolder, filename)
		
		filename = FileSavedFolder & "/" & filename
		
		dim FileTypeView
		if trim(fileneme) <> "" then
			FileTypeView = split(filename,".")(1)
			FileTypeView = Ucase(FileTypeView)
			
			if FileTypeView = "GIF" or FileTypeView = "JPG" then
			elseif FileTypeView = "TXT" or FileTypeView = "HTM" or FileTypeView = "HTML" then
			elseif FileTypeView = "" then	'미디어(영상) 파일의 경우
			elseif FileTypeView = "" then	'미디어(음악) 파일의 경우
			end if
		end if
	End Function
%>