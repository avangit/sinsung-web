

<%	
Sub CountView2(byval siteid)
	siteid = lcase(siteid)
	'// 사이트별 뷰어수 설정
	dim SearchString, nosite, naver, daum, yahoo, empas, siteetc
	nosite	= 0
	naver	= 0
	daum	= 0
	yahoo	= 0
	empas	= 0
	siteetc	= 0
	
	



	' 금일 날짜에서 조회하고자 하는 날짜를 변수에 등록
	
	dim i				'전/후
	dim CountViewMode		'년/월/일
	
	dim yy
	dim mm
	dim dd
	dim today_date
	dim quest_date
	dim pageurl
	
	pageurl = lcase(request.ServerVariables("PATH_INFO"))

	i		= request.QueryString("i")

	
	if len(session("CountViewMode")) = 0 then 
		session("CountViewMode") = "dd"
	end if
	
	if len(request.QueryString("CountViewMode")) > 0 then 
		session("CountViewMode") = request.QueryString("CountViewMode")
	end if
	
	
	'response.Write(session("CountViewMode"))
	
	' 관리자가 원하는 날짜 출력
	if session("CountViewMode")="dd" then
		quest_date = DateAdd("d",i,date)
		yy = datepart("yyyy",quest_date)
		mm = datepart("m",quest_date)
		dd = datepart("d",quest_date)
		quest_date = yy & "년 " & mm &"월 " & dd & "일"
	elseif session("CountViewMode")="mm" then
		quest_date = DateAdd("m",i,date)
		yy = datepart("yyyy",quest_date)
		mm = datepart("m",quest_date)
		dd = datepart("d",quest_date)
		quest_date = yy & "년 " & mm &"월 "
	elseif session("CountViewMode")="yy" then
		quest_date = DateAdd("yyyy",i,date)
		yy = datepart("yyyy",quest_date)
		mm = datepart("m",quest_date)
		dd = datepart("d",quest_date)
		quest_date = yy & "년 "
	end if

	
	
	call dbopen
	
	
	
	'// siteid 받아오기

	'//-----------------------------------
	
	
	
	
	
	
	' ##### 전체 방문자
		dim Totalno,Total_rs, Total_sql
		Total_sql = "select count(Distinct userip) from Count_data where pageURL like '%/%' and siteid = '"&siteid&"'"
		'response.write Total_sql
		
		set Total_rs = dbconn.execute(Total_sql)
		TotalNo = Total_rs(0)
		Total_rs.close
		set Total_rs = nothing
		
	' ##### 전체 페이지뷰
		dim TotalViewno,TotalView_rs, TotalView_sql
		TotalView_sql = "select count(*) from Count_data where siteid = '"&siteid&"'"
		set TotalView_rs = dbconn.execute(TotalView_sql)
		TotalViewNo = TotalView_rs(0)
		TotalView_rs.close
		set TotalView_rs = nothing
	
	' ##### 기본 정보 - 관련 전체 방문자 / 관련 전체 페이지수 'pageURL='"&pageurl&"' and <--삭제
	dim T_main, T_main_sql, T_main_rs
	if session("CountViewMode")="dd" then
		T_main_sql = "select count(Distinct userip) from Count_data where yyyy= " & yy &" and mm= " & mm & " and dd= " & dd & " and siteid = '"&siteid&"'"
		set T_main_rs = dbconn.execute(T_main_sql)
		T_main = T_main_rs(0)
		
	elseif session("CountViewMode")="mm" then
		T_main_sql = "select count(Distinct userip) from Count_data where yyyy= " & yy &" and mm= " & mm & " and siteid = '"&siteid&"'"
		set T_main_rs = dbconn.execute(T_main_sql)
		T_main = T_main_rs(0)

	elseif session("CountViewMode")="yy" then
		T_main_sql = "select count(Distinct userip) from Count_data where yyyy= " & yy &" and siteid = '"&siteid&"'"
		set T_main_rs = dbconn.execute(T_main_sql)
		T_main = T_main_rs(0)

	end if
	'response.write T_main_sql
		T_main_rs.close
		set T_main_rs = nothing
	
	
	' ##### 기본 정보 - 관련 전체 방문자 / 관련 전체 페이지뷰수
	dim T_page, T_page_sql, T_page_rs
	if session("CountViewMode")="dd" then
		T_page_sql = "select count(*) from Count_data where yyyy= " & yy &" and mm= " & mm & " and dd= " & dd &" and siteid = '"&siteid&"'"
		set T_page_rs = dbconn.execute(T_page_sql)
		T_page = T_page_rs(0)
		
	elseif session("CountViewMode")="mm" then
		T_page_sql = "select count(*) from Count_data where yyyy= " & yy &" and mm= " & mm &" and siteid = '"&siteid&"'"
		set T_page_rs = dbconn.execute(T_page_sql)
		T_page = T_page_rs(0)

	elseif session("CountViewMode")="yy" then
		T_page_sql = "select count(*) from Count_data where yyyy= " & yy &" and siteid = '"&siteid&"'"
		set T_page_rs = dbconn.execute(T_page_sql)
		T_page = T_page_rs(0)

	end if
		T_page_rs.close
		set T_page_rs = nothing
	
	' ##### 페이지별 조회수
	dim sql_pageURL, rs_pageURL
	sql_pageURL = "Select pageURL, Count(PageURL) As 'ViewNo' From Count_data Where yyyy = "
	sql_pageURL = sql_pageURL & yy
	if session("CountViewMode") = "mm" or session("CountViewMode") = "dd" then
	sql_pageURL = sql_pageURL & " and mm = " & mm
	end if
	if session("CountViewMode") = "dd" then
	sql_pageURL = sql_pageURL & " and dd = " & dd
	end if
	sql_pageURL = sql_pageURL & " and  siteid = '"&siteid&"' Group By pageURL Order By ViewNo Desc"


	set rs_pageURL = dbconn.execute(sql_pageURL)
	
	' ##### 메인페이지 접근경로
	dim sql_backURL, rs_backURL
	sql_backURL = "Select Top 100 backURL, Count(backURL) As 'ViewNo' From Count_data Where yyyy = "
	sql_backURL = sql_backURL & yy
	if session("CountViewMode") = "mm" or session("CountViewMode") = "dd" then
	sql_backURL = sql_backURL & " and mm = " & mm
	end if
	if session("CountViewMode") = "dd" then
	sql_backURL = sql_backURL & " and dd = " & dd
	end if
	sql_backURL = sql_backURL & " and siteid = '"&siteid&"' Group By backURL Order By ViewNo Desc"


	set rs_backURL = dbconn.execute(sql_backURL)

	
	
'response.Write(sql_backURL)
%>

<HTML>
<HEAD>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link href="../../prog_inc/css.css" rel="stylesheet" type="text/css">
</HEAD>

<BODY>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr> 
    <td width="50"><div align="center"><a href="?<%=getString("CountViewMode=dd&i=0")%>">날짜별</a></div></td>
    <td width="50"><div align="center"><a href="?<%=getString("CountViewMode=mm&i=0")%>">월별</a></div></td>
    <td width="50"><div align="center"><a href="?<%=getString("CountViewMode=yy&i=0")%>">년도별</a></div></td>
    <td><div align="right">총 <%=TotalNo%>명 방문/ 총 <%=TotalviewNo%> Page 클릭</div></td>
    <td width="120"><div align="right"><strong>Today</strong> <%=Date()%></div></td>
  </tr>
  <tr bgcolor="#3399CC"> 
    <td height="2"></td>
    <td height="2"></td>
    <td height="2"></td>
    <td height="2"></td>
    <td height="2"></td>
  </tr>
</table>
<br>
* <span class="blue"><strong><%=quest_date%></strong></span><font size="2">조회[<a href='?<%=getString("i=" & i-1)%>'>이전 
조회 </a>&lt;&gt;<a href='?<%=getString("i=" & i+1)%>'>이후 
조회</a>]<br>
</font><br>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr bgcolor="#FFFFFF"> 
    <td width="150"><strong>* 기본 정보</strong></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td><%=quest_date%> 전체 방문자 [ <%=T_main%> 명 ] <!--/ <%'=quest_date%> 전체 페이지뷰 [ <%'=T_page%> 회 ] --></td>
  </tr>
</table>

<br>
<!--
<p><br>
  날짜별일경우 시간대별 그래프<br>
  월별일 경우 날짜별 그래프<br>
  년도별일 경우 월별 그래프<br>
</p>
-->
<table width="100%" border="0" cellspacing="1" cellpadding="3">
  <tr> 
    <td><strong>* 페이지 조회수 </strong> </td>
  </tr>
  <tr> 
    <td>
		<%
			if not rs_pageURL.eof then 
			 	Do While not rs_pageURL.eof
				
				
		%>   <%=rs_pageURL("pageURL")%> (<a href='?<%=getString("siteid="&rs_pageURL("pageURL"))%>'><%=rs_pageURL("ViewNo")%></a>)<br>
		<% 
			rs_pageURL.movenext
				loop 
				end if
			
				'리소스 반환
				rs_pageURL.close
				set rs_pageURL = nothing
			
		%>
		
	</td>
  </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="3">
   <form name="form1" method="post" action="?<%=getString("")%>">
  <tr> 
    <td><strong>* 페이지별 접근경로</strong>[전체통계] (상위 100열만 출력됩니다.)
    </td>
  </tr>
  </form>
  <tr> 
    <td>
		<%
			dim backurl_name
			if not rs_backURL.eof then 
				Do While not rs_backURL.eof
				
				backurl_name = rs_backURL("backURL")
				
				if len(backurl_name) = 0 then
					%><%="직접경로입력 혹은 즐겨찾기로 접속"%> (<%=rs_backURL("ViewNo")%>)<br> <%
				else
					%><a href='<%=rs_backURL("backURL")%>' target="_blank"><%=URLDecode(backurl_name)%></a> (<%=rs_backURL("ViewNo")%>)<br> <%
				end if
				
				
				
				
				'// 사이트별 접속현황
				'//SearchString, nosite, naver, daum, yahoo, empas, siteetc
				SearchString = backurl_name
				if len(trim(backurl_name)) = 0 then
					nosite = nosite + rs_backURL("ViewNo")
				else
					if Instr(lcase(backurl_name), "naver") > 0 then
						naver = naver + rs_backURL("ViewNo")
					elseif Instr(lcase(backurl_name), "daum") > 0 then
						daum = daum + rs_backURL("ViewNo")
					elseif Instr(lcase(backurl_name), "yahoo") > 0 then
						yahoo = yahoo + rs_backURL("ViewNo")
					elseif Instr(lcase(backurl_name), "empas") > 0 then
						empas = empas + rs_backURL("ViewNo")
					else
						siteetc = siteetc + 1
					end if				
				end if
				'____________________________// 사이트별 접속현황 끝

			rs_backURL.movenext
				loop 
				end if
			
				'리소스 반환
				rs_backURL.close
				set rs_backURL = nothing
				call dbclose()
			
		%>
			
	</td>
  </tr>
</table><br>
<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td width="70">검색사이트</td>
    <td bgcolor="#FFFFFF">즐겨찾기(<%=nosite%>) <b>네이버</b>(<%=naver%>) <b>다음</b>(<%=daum%>) <b>야후</b>(<%=yahoo%>) <b>엡파스</b>(<%=empas%>) 기타(<%=siteetc%>) </td>
  </tr>
</table>
</BODY>
</HTML>
<%
end sub
%>