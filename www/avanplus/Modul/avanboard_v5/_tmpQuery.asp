<%
	'//*------------------------------------------------------------
	' 소  속 : 아반소프트 avansoft.co.kr
	' 작성자 : 이덕권 (leedk7@msn.com)
	' 작성일 : 2004년 1월 29일
	' ***[설명]***
	' 
	'-------------------------------------------------------------*//

	Function QueryInsert(byval TableName,byval RequestData)
	'##	insert 쿼리생성
	
	' 형식 insert Table (컬럼1, 컬럼2) VALUES ('a','b')
	dim item, strName, strValue, colume_name, colume_values, colume_count
	colume_count = 0
	
	colume_name = "insert into " & TableName & " ("		
	colume_values = " VALUES ("
	
	For Each item In RequestData
   	strName = all_input(item)
		strValue = all_input(RequestData(item))		
			if colume_count = 0 then
				colume_name 	= colume_name 	& strName
				colume_values	= colume_values	& "N'" & all_input(strValue) & "' "
				colume_count = colume_count + 1
			else
				colume_name 	= colume_name 	& "," &strName
				colume_values	= colume_values	& ",N'" & all_input(strValue) & "' "
			end if
	Next
	
	colume_name 	= colume_name 	& " ) " 
	colume_values	= colume_values	& " ) "
	QueryInsert = colume_name & colume_values 
		
	End Function
	
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
	'	설명 : update 쿼리문 생성기
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function QueryUpdate(byval TableName,byval RequestData,byval Where)
	'##	update 쿼리생성
	' 형식 insert Table (컬럼1, 컬럼2) VALUES ('a','b')
	dim item, strName, strValue, Uquery, colume_count
	colume_count = 0
	
	Uquery = "Update " & TableName & " set "		
	
	For Each item In RequestData
  	strName = all_input(item)
		strValue = all_input(RequestData(item)) 
		
			if colume_count = 0 then	
				Uquery 	= Uquery 	& strName & "=N'" & all_input(strValue) &"'"
				colume_count = colume_count + 1
			else
				Uquery 	= Uquery 	& "," & strName & "=N'" & all_input(strValue) &"'"
			end if

	Next
	
	
	if trim(where) = "" or isnull(where) then
		Uquery 	= "Errer : where부분 조건이 없습니다. 이경우 모든 데이타를 수정 하기에 실행하지 않습니다."
	else
		Uquery 	= Uquery 	& " where " & where
	end if
	
	QueryUpdate = Uquery
	End Function
	
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
	'	설명 : delete 쿼리문 생성기
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function QueryDelete(byval TableName,byval Where)
	'##	Delete 쿼리생성
	Dim Dquery
	if trim(where) <> "" then
		Dquery 	= "Delete from " & tablename & " where "& where 
	else
		Dquery 	= "where부분 조건이 없습니다. 이경우 모든 데이타를 삭제 하기에 실행하지 않습니다."
	end if
	
	QueryDelete = Dquery
	End Function
%>
