<!--#include file="../../_Config.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
<%
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'## dim intNowPage,intPageSize,intBlockPage,intTotalCount,intTotalPage
	'## intNowPage = Request.QueryString("page")    
    '## intPageSize = 10
    '## intBlockPage = 10
	'## call intTotal(Tablename,where)

	'## 1. intTotal(Tablename,where) : call intTotal(Tablename,where)
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Pasing 하단의 네비게이션 바 call Pasing
	'## 

	DBopen
	dim intNowPage,intPageSize,intBlockPage,intTotalCount,intTotalPage
	intNowPage = Request.QueryString("page")
	intPageSize = 15
	intBlockPage = 10

	
	dim query_filde		: query_filde		= "*"
	dim query_Tablename	: query_Tablename	= "boardset_v2"
	dim query_where		: query_where		= ""
	dim query_orderby		: query_orderby		= ""
	call intTotal
	
	
	dim sql, rs
	sql = "select  " & TopCount & " * from boardset_v2 order by bs_idx desc"
	set rs = dbconn.execute(sql)
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><font color="#639CCF" style="font-size:14px; font-face:돋움"><b>게시판 관리</b></font> (ver.3.20161218)</td>
	</tr>
	<tr>
		<td style="padding-bottom:10px;"><%=NavCount%></td>
	</tr>
</table>
<table class="tableList" width="100%"  border="0" cellspacing="0" cellpadding="2">
  <tr>
    <th>아이디</th>
    <th>게시판 이름 </th>
    <th>쓰/읽/답/리/첨 </th>
    <th>관리자ID</th>
    <th>게시판용도</th>
	<th>그룹핑코드</th>
    <th>세부설정</th>
  </tr>
  <%
  	if not rs.eof then
	rs.move MoveCount
	Do while not rs.eof
  %>
  <tr>
	<td class="tcenter"><%=rs("bs_code")%></td>
    <td class="tcenter"><font color="#006699"><a href="/avanplus/modul/avanboard_v3/call2.asp?<%=getString("bs_code="&rs("bs_code")&"&vmode=")%>" target="_blank"><b><%=rs("bs_name")%></b></a></font></td>
	<td class="tcenter"><%=rs("bs_writeLaw")&" / "&rs("bs_readLaw")&" / "&left(rs("bs_rewrite"),1)&" / "&left(rs("bs_reply"),1)&" / "&left(rs("bs_file"),1)%></td>
	<td class="tcenter"><%=rs("bs_admin")%></td>
    <td class="tcenter"><%=rs("bs_use")%></td>
    <td class="tcenter"><%=rs("bs_grouping")%></td>
    <td class="tcenter"><a href='?<%=getString("vmode=modify&bs_code="&rs("bs_code"))%>'>세부설정</a></td>
  </tr>
  
  <%
  	rs.movenext
	loop
  	end if
	
	rs.close
	set rs = nothing
	DBclose
  %>
</table>
<br>
<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td><%call paging("")%></td>
  </tr>
</table>

<ul class="btnWrap">
    <li><input type="button"  class="btn_m c_blue" value="신규게시판등록" onclick="self.location.href = '?<%=getString("vmode=boardadd")%>';"></li>
</ul>
</body>
</html>
