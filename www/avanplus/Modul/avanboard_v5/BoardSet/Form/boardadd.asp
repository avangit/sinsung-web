<!-- include virtual ="/avanplus/inc/incCodepage.asp" -->
<!--#include file="../../_Config.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><font color="#639CCF" style="font-size:14px; font-face:돋움"><b>신규 게시판 등록</b></font></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
</table>
<table class="tableWrite" width="100%"  border="0" cellpadding="0" cellspacing="0" >
	<colgroup>
    	<col style="width:15%">
        <col style="width:85%">
    </colgroup>
	<form action="?<%=getString("vmode=boardadd_ok")%>" method="post" enctype="multipart/form-data" name="insert" >
	<tr>
		<th>게시판 아이디</th>
		<td class="pl20">
			<input name="bs_code" type="text" id="bs_code" size="15" maxlength="20">영문자20자 이내로 <font color="#FF0000">* 세팅후 변경 불가 </font>
		</td>
	</tr>
	<tr>
		<th>게시판 이름</th>
		<td class="pl20"><input name="bs_name" type="text" id="bs_name" size="30" maxlength="20"></td>
	</tr>
	<tr>
		<th>관리 패스워드</th>
		<td class="pl20">
			<b>게시판관리자</b>
			<input name="bs_admin" type="text" id="bs_admin" value="admin" size="15" maxlength="20">&nbsp;&nbsp;&nbsp;
			<b>관리용비밀번호</b>
			<input name="bs_pw" type="text" id="bs_pw" value="avansoft" size="15" maxlength="20">
		</td>
	</tr>
	<tr>
		<th>게시판 안내글</th>
		<td class="pl20"><textarea name="bs_message" rows="5" id="bs_message" style="width:100% "></textarea></td>
	</tr>
	<tr>
		<th>게시판 구분</th>
	  <td class="pl20">
			<input name="bs_use" type="radio"  style="border:0px;background-color:transparent" value="board" checked>일반게시판
			<input type="radio"  style="border:0px;background-color:transparent" name="bs_use" value="pic">	 
			사진게시판
		    <input type="radio"  style="border:0px;background-color:transparent" name="bs_use" value="gallery" >
사진갤러리</td>
	</tr>
	<tr>
		<th>게시물 제목길이</th>
		<td class="pl20">
			<input name="bs_titleLen" type="text" id="bs_titleLen" value="50" size="10" maxlength="5">byte
		</td>
	</tr>
	<tr>
		<th>페이지당 목록수</th>
		<td class="pl20">
			<input name="bs_pageRow" type="text" id="bs_pageRow" value="10" size="10" maxlength="5">개
		</td>
	</tr>
	<tr>
		<th>페이지 이동단위</th>
		<td class="pl20">
			<input name="bs_pageBlock" type="text" id="bs_pageBlock" value="10" size="10" maxlength="5">개
		</td>
	</tr>
	<tr>
		<th>쓰기권한</th>
		<td class="pl20">
			<!--<input type="radio"  style="border:0px;background-color:transparent" name="bs_writeLaw" value="9">관리자
			<input type="radio"  style="border:0px;background-color:transparent" name="bs_writeLaw" value="1">회원
			<input name="bs_writeLaw" type="radio"  style="border:0px;background-color:transparent" value="0" checked>비회원-->
			<select name = "bs_writeLaw" style="width:50">
				<option value = "0">0</option>
				<option value = "1">1</option>
				<option value = "2">2</option>
				<option value = "3">3</option>
				<option value = "4">4</option>
				<option value = "5">5</option>
				<option value = "6">6</option>
				<option value = "7">7</option>
				<option value = "8">8</option>
				<option value = "9" selected>9</option>
			</select> Level 0 : 비회원
		</td>
	</tr>
	<tr>
		<th>읽기권한</th>
		<td class="pl20">
			<select name = "bs_readLaw" style="width:50">
				<option value = "0">0</option>
				<option value = "1">1</option>
				<option value = "2">2</option>
				<option value = "3">3</option>
				<option value = "4">4</option>
				<option value = "5">5</option>
				<option value = "6">6</option>
				<option value = "7">7</option>
				<option value = "8">8</option>
				<option value = "9" selected>9</option>
			</select>
			<!--<input type="radio"  style="border:0px;background-color:transparent" name="bs_readLaw" value="9">관리자
			<input type="radio"  style="border:0px;background-color:transparent" name="bs_readLaw" value="1">회원
			<input name="bs_readLaw" type="radio"  style="border:0px;background-color:transparent" value="0" checked>비회원-->
			[<b>비밀글기능</b>
			<input type="radio"  style="border:0px;background-color:transparent" name="bs_bimilLaw" value="1" >사용
			<input name="bs_bimilLaw" type="radio"  style="border:0px;background-color:transparent" value="0" checked >사용안함 ]
		</td>
	</tr>
	<tr>
		<th>답글쓰기</th>
		<td class="pl20">
			<input name="bs_rewrite" type="radio"  style="border:0px;background-color:transparent" value="1" checked>사용
			<input name="bs_rewrite" type="radio"  style="border:0px;background-color:transparent" value="0">사용안함&nbsp;&nbsp; 
			[<b> 권한</b>:
			<!--<input type="radio"  style="border:0px;background-color:transparent" name="bs_rewriteLaw" value="9">관리자
			<input name="bs_rewriteLaw" type="radio"  style="border:0px;background-color:transparent" value="1">회원
			<input name="bs_rewriteLaw" type="radio"  style="border:0px;background-color:transparent" value="0" checked>비회원-->
			<select name = "bs_rewriteLaw" style="width:50">
				<option value = "0" selected>0</option>
				<option value = "1">1</option>
				<option value = "2">2</option>
				<option value = "3">3</option>
				<option value = "4">4</option>
				<option value = "5">5</option>
				<option value = "6">6</option>
				<option value = "7">7</option>
				<option value = "8">8</option>
				<option value = "9" selected>9</option>
			</select>]
		</td>
	</tr>
	<tr>
		<th>리플기능</th>
		<td class="pl20">
			<input name="bs_reply" type="radio"  style="border:0px;background-color:transparent" value="1" checked>사용
			<input name="bs_reply" type="radio"  style="border:0px;background-color:transparent" value="0">사용안함&nbsp;&nbsp; 
			[<font color="#0000FF"> 회원만 사용가능 </font>]
		</td>
	</tr>
	<tr>
		<th>파일첨부</th>
		<td class="pl20">
			<input type="radio"  style="border:0px;background-color:transparent" name="bs_file" value="1">사용
			<input name="bs_file" type="radio"  style="border:0px;background-color:transparent" value="0" checked>사용안함&nbsp;&nbsp; 
			[<b> 업로드 용량제한 </b><input name="bs_filesize" type="text" id="bs_filesize" value="1" size="5" maxlength="3"> MB ]
			
		</td>
	</tr>
	<!--
	<tr>
		<th>업로드금지<br>파일명설정</td>
		<td class="pl20"><textarea name="bs_badFiletype" rows="5" id="bs_badFiletype" style="width:100% "></textarea></td>
	</tr>
	-->
	<tr>
		<th>IP표시</th>
		<td class="pl20">
			<input name="bs_ipview" type="radio"  style="border:0px;background-color:transparent" value="1" checked>사용
			<input type="radio"  style="border:0px;background-color:transparent" name="bs_ipview" value="0">사용안함
		</td>
	</tr>
	<tr>
		<th>주민번호인증</th>
		<td class="pl20">
			<input type="radio"  style="border:0px;background-color:transparent" name="bs_jumin" value="1">사용
			<input name="bs_jumin" type="radio"  style="border:0px;background-color:transparent" value="0" checked>
			사용안함 [글등록시 주민번호를 입력]
		</td>
	</tr>
	<tr>
		<th>게시판 색지정</th>
		<td class="pl20"><input name="bs_color" type="text" id="bs_color" size="10" maxlength="8" value="#666666"></td>
	</tr>
	<!--
	<tr>
		<th>불량단어 필터링</th>
		<td class="pl20"><textarea name="bs_badWord" rows="5" id="bs_badWord" style="width:100% ">광고;[광고];씨발;좆도;개새끼;섹스;니미뽕</textarea></td>
	</tr>
	<tr>
		<th>불량IP 필터링</th>
		<td class="pl20"><textarea name="bs_badip" rows="5" id="bs_badip" style="width:100% "></textarea></td>
	</tr>
	-->
	<tr>
		<th>타이틀 이미지</th>
		<td class="pl20">
			<input type="radio"  style="border:0px;background-color:transparent" name="bs_titleimg" value="1">사용
			<input name="bs_titleimg" type="radio"  style="border:0px;background-color:transparent" value="0" checked>사용안함
			<input name="file_titleimg" type="file" id="file_titleimg">(jpg,gif만 가능)
		</td>
	</tr>
	<tr>
		<th>게시글 그룹핑</th>
		<td class="pl20"><input name="bs_grouping" type="text" id="bs_grouping" size="10" maxlength="10">10자 이내
	</td>
	</tr>
</table>
<ul class="btnWrap">
	<li><input type="submit"name="Submit"  value=" 신규게시판 등록 " class="btn_m c_blue"></li>
	<li><input type="button" value=" 목록 "  class="btn_m c_gray"  onclick="self.location.href = '?<%=getString("vmode=boardlist")%>';"></li>
</ul>
</form>

</body>
</html>
