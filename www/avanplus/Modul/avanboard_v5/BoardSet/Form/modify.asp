<!--#include file="../../_Config.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>무제 문서</title>
</head>
<%
	

  	Function radio_checked(byval val, byval rs_val)'//두 값이 같으면 checked출력
		if val = rs_val then
			response.Write("checked")
		end if
		
	end Function
	
	Function radio_checked_bit(byval TF, byval rs_val)'//두 값이 같으면 checked출력'////비트연산
		if TF = false and not rs_val then
			response.Write("checked")
		elseif TF and rs_val then
			response.Write("checked")
		end if
	end Function

	
	
	dim sql, rs, bs_code
	bs_code = request.QueryString("bs_code")
	DBopen
	set rs = DBconn.execute("select * from boardset_v2 where bs_code='"& bs_code &"'")

%>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><font color="#639CCF" style="font-size:14px; font-face:돋움"><b>게시판 수정</b></font></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
</table>
<table class="tableWrite" width="100%"  border="0" cellpadding="0" cellspacing="0">
	<colgroup>
    	<col style="width:15%">
        <col style="width:85%">
    </colgroup>
	<form action='?<%=getString("vmode=modify_ok&bs_code="&rs("bs_code"))%>' method="post" enctype="multipart/form-data" name="update" >
    <tr>
      <th>게시판 아이디</th>
      <td class="pl20"><font color="#0000FF"><a href="/avanplus/modul/avanboard_v3/board.asp?bs_code=<%=rs("bs_code")%>" target="_blank"><b><%=rs("bs_code")%></b></a></font><font color="#FF0000">&nbsp;&nbsp; [게시판 아이디는 변경하실수 없습니다.]</font></td>
    </tr>
    <tr>
      <th>게시판 이름</th>
      <td class="pl20"><input name="bs_name" type="text" id="bs_name" value="<%=rs("bs_name")%>" size="30" maxlength="20"></td>
    </tr>
    <tr>
      <th>게시판관리</th>
      <td class="pl20"><b>게시판관리자ID</b>        <input name="bs_admin" type="text" id="bs_admin" value="<%=rs("bs_admin")%>" size="15" maxlength="20">
        등록된 ID user는 게시판 관리자 권한을 가집니다. <br>
        <b>관리용비밀번호</b>      <input name="bs_pw" type="password" id="bs_pw" value="<%=rs("bs_pw")%>" size="15" maxlength="20" style="height:28px;border: 1px solid #ddd; margin-top:4px;">
      관리용비밀번호를 입력하면 게시글을 수정,삭제할 수 있습니다. </td>
    </tr>
    <tr>
      <th>게시판 안내글</th>
      <td class="pl20"><textarea name="bs_message" rows="5" id="bs_message" style="width:100% "><%=rs("bs_message")%></textarea></td>
    </tr>
    <tr>
      <th>게시판 구분</th>
	  <td class="pl20"><input type="radio"  style="border:0px;background-color:transparent" name="bs_use" value="board" <%call radio_checked("board",rs("bs_use"))%>>
        일반게시판
        <input type="radio"  style="border:0px;background-color:transparent" name="bs_use" value="pic" <%call radio_checked("pic",rs("bs_use"))%>>
        사진게시판 
        <input type="radio"  style="border:0px;background-color:transparent" name="bs_use" value="gallery" <%call radio_checked("gallery",rs("bs_use"))%>>
        사진갤러리</td>
    </tr>
    <tr>
      <th>게시물 제목길이</th>
      <td class="pl20"><input name="bs_titleLen" type="text" id="bs_titleLen" value="<%=rs("bs_titlelen")%>" size="10" maxlength="5">
        byte</td>
    </tr>
    <tr>
      <th>페이지당 목록수</th>
      <td class="pl20"><input name="bs_pageRow" type="text" id="bs_pageRow" value="<%=rs("bs_pagerow")%>" size="10" maxlength="5">
        개</td>
    </tr>
    <tr>
      <th>페이지 이동단위</th>
      <td class="pl20"><input name="bs_pageBlock" type="text" id="bs_pageBlock" value="<%=rs("bs_pageblock")%>" size="10" maxlength="5">
        개</td>
    </tr>
    <tr>
      <th>쓰기권한</th>
      <td class="pl20"><!--<input type="radio"  style="border:0px;background-color:transparent" name="bs_writeLaw" value="9" <%'call radio_checked("9",rs("bs_writeLaw"))%>>
        관리자
          <input type="radio"  style="border:0px;background-color:transparent" name="bs_writeLaw" value="1" <%'call radio_checked("1",rs("bs_writeLaw"))%>>
        회원
        <input name="bs_writeLaw" type="radio"  style="border:0px;background-color:transparent" value="0"  <%'call radio_checked("0",rs("bs_writeLaw"))%>>
        비회원 -->
		<select name = "bs_writeLaw" style="width:50">
			<option value = "0" <% = chk_selected(rs("bs_writeLaw"), "0")%>>0</option>
			<option value = "1" <% = chk_selected(rs("bs_writeLaw"), "1")%>>1</option>
			<option value = "2" <% = chk_selected(rs("bs_writeLaw"), "2")%>>2</option>
			<option value = "3" <% = chk_selected(rs("bs_writeLaw"), "3")%>>3</option>
			<option value = "4" <% = chk_selected(rs("bs_writeLaw"), "4")%>>4</option>
			<option value = "5" <% = chk_selected(rs("bs_writeLaw"), "5")%>>5</option>
			<option value = "6" <% = chk_selected(rs("bs_writeLaw"), "6")%>>6</option>
			<option value = "7" <% = chk_selected(rs("bs_writeLaw"), "7")%>>7</option>
			<option value = "8" <% = chk_selected(rs("bs_writeLaw"), "8")%>>8</option>
			<option value = "9" <% = chk_selected(rs("bs_writeLaw"), "9")%>>9</option>
		</select> Level 0 : 비회원</td>
    </tr>
    <tr>
      <th>읽기권한</th>
	  <td class="pl20"><!--<input type="radio"  style="border:0px;background-color:transparent" name="bs_readLaw" value="9" <%'call radio_checked("9",rs("bs_readLaw"))%>>
        관리자
          <input type="radio"  style="border:0px;background-color:transparent" name="bs_readLaw" value="1" <%'call radio_checked("1",rs("bs_readLaw"))%>>
        회원
        <input name="bs_readLaw" type="radio"  style="border:0px;background-color:transparent" value="0" <%'call radio_checked("0",rs("bs_readLaw"))%>>
        비회원-->
		<select name = "bs_readLaw" style="width:50">
			<option value = "0" <% = chk_selected(rs("bs_readLaw"), "0")%>>0</option>
			<option value = "1" <% = chk_selected(rs("bs_readLaw"), "1")%>>1</option>
			<option value = "2" <% = chk_selected(rs("bs_readLaw"), "2")%>>2</option>
			<option value = "3" <% = chk_selected(rs("bs_readLaw"), "3")%>>3</option>
			<option value = "4" <% = chk_selected(rs("bs_readLaw"), "4")%>>4</option>
			<option value = "5" <% = chk_selected(rs("bs_readLaw"), "5")%>>5</option>
			<option value = "6" <% = chk_selected(rs("bs_readLaw"), "6")%>>6</option>
			<option value = "7" <% = chk_selected(rs("bs_readLaw"), "7")%>>7</option>
			<option value = "8" <% = chk_selected(rs("bs_readLaw"), "8")%>>8</option>
			<option value = "9" <% = chk_selected(rs("bs_readLaw"), "9")%>>9</option>
		</select>        
       &nbsp; [ <b>비밀글기능</b>
	    <input type="radio"  style="border:0px;background-color:transparent" name="bs_bimilLaw" value="1" <%call radio_checked_bit(true,rs("bs_bimilLaw"))%>>사용
        <input type="radio"  style="border:0px;background-color:transparent" name="bs_bimilLaw" value="0" <%call radio_checked_bit(false,rs("bs_bimilLaw"))%>>사용안함
        ]</td>
    </tr>
    <tr>
      <th>답글쓰기</th>
      <td class="pl20"><input name="bs_rewrite" type="radio"  style="border:0px;background-color:transparent" value="1" <%call radio_checked_bit(true,rs("bs_rewrite"))%>>
        사용
          <input name="bs_rewrite" type="radio"  style="border:0px;background-color:transparent" value="0" <%call radio_checked_bit(false,rs("bs_rewrite"))%>>
        사용안함&nbsp;&nbsp; [<b> 권한</b>:
        <!--<input type="radio"  style="border:0px;background-color:transparent" name="bs_rewriteLaw" value="9" <%'call radio_checked("9",rs("bs_rewriteLaw"))%>>
        관리자
        <input name="bs_rewriteLaw" type="radio"  style="border:0px;background-color:transparent" value="1" <%'call radio_checked("1",rs("bs_rewriteLaw"))%>>
        회원
        <input name="bs_rewriteLaw" type="radio"  style="border:0px;background-color:transparent" value="0" <%'call radio_checked("0",rs("bs_rewriteLaw"))%>>
        비회원-->
		<select name = "bs_rewriteLaw" style="width:50">
			<option value = "0" <% = chk_selected(rs("bs_rewriteLaw"), "0")%>>0</option>
			<option value = "1" <% = chk_selected(rs("bs_rewriteLaw"), "1")%>>1</option>
			<option value = "2" <% = chk_selected(rs("bs_rewriteLaw"), "2")%>>2</option>
			<option value = "3" <% = chk_selected(rs("bs_rewriteLaw"), "3")%>>3</option>
			<option value = "4" <% = chk_selected(rs("bs_rewriteLaw"), "4")%>>4</option>
			<option value = "5" <% = chk_selected(rs("bs_rewriteLaw"), "5")%>>5</option>
			<option value = "6" <% = chk_selected(rs("bs_rewriteLaw"), "6")%>>6</option>
			<option value = "7" <% = chk_selected(rs("bs_rewriteLaw"), "7")%>>7</option>
			<option value = "8" <% = chk_selected(rs("bs_rewriteLaw"), "8")%>>8</option>
			<option value = "9" <% = chk_selected(rs("bs_rewriteLaw"), "9")%>>9</option>
		</select>] </td>
    </tr>
    <tr>
      <th>리플기능</th>
      <td class="pl20"><input name="bs_reply" type="radio"  style="border:0px;background-color:transparent" value="1" <%call radio_checked_bit(true,rs("bs_reply"))%>>
        사용
          <input name="bs_reply" type="radio"  style="border:0px;background-color:transparent" value="0" <%call radio_checked_bit(false,rs("bs_reply"))%>>
        사용안함&nbsp;&nbsp; [<font color="#0000FF"> 회원만 사용가능</font> ] </td>
    </tr>
    <tr>
      <th>파일첨부</th>
      <td class="pl20"><input type="radio"  style="border:0px;background-color:transparent" name="bs_file" value="1" <%call radio_checked_bit(true,rs("bs_file"))%>>
        사용
          <input name="bs_file" type="radio"  style="border:0px;background-color:transparent" value="0" <%call radio_checked_bit(false,rs("bs_file"))%>>
        사용안함&nbsp;&nbsp; [ <b>업로드 용량제한</b>
        <input name="bs_filesize" type="text" id="bs_filesize" value="<%=rs("bs_filesize")%>" size="5" maxlength="3">
        MB ]
		 </td>
    </tr>
	<!--
    <tr>
      <th>업로드금지<br>
        파일명설정<br>
        <font color="#FF0000">[미완]</font></th>
      <td class="pl20"><textarea name="bs_badFiletype" rows="5" id="bs_badFiletype" style="width:100% "><%=rs("bs_badfiletype")%></textarea></td>
    </tr>
	-->
    <tr>
      <th>IP표시</th>
      <td class="pl20"><input name="bs_ipview" type="radio"  style="border:0px;background-color:transparent" value="1" <%call radio_checked_bit(true,rs("bs_ipview"))%>>
        사용
          <input type="radio"  style="border:0px;background-color:transparent" name="bs_ipview" value="0" <%call radio_checked_bit(false,rs("bs_ipview"))%>>
        사용안함</td>
    </tr>
    <tr>
      <th>주민번호인증</th>
      <td class="pl20"><input type="radio"  style="border:0px;background-color:transparent" name="bs_jumin" value="1" <%call radio_checked_bit(true,rs("bs_jumin"))%>>
        사용
          <input name="bs_jumin" type="radio"  style="border:0px;background-color:transparent" value="0" <%call radio_checked_bit(false,rs("bs_jumin"))%>>
        사용안함 [글등록시 주빈번호를 입력]</td>
    </tr>
    <tr>
      <th>게시판 색지정 </th>
      <td class="pl20"><input name="bs_color" type="text" id="bs_color" value="<%=rs("bs_color")%>" size="10" maxlength="8"></td>
    </tr>
    <!--
	<tr>
      <th>불량단어 필터링<br>
      <font color="#FF0000">[미완]</font></th>
      <td class="pl20"><textarea name="bs_badWord" rows="5" id="bs_badWord" style="width:100% "><%=rs("bs_badword")%></textarea></td>
    </tr>
    <tr>
      <th>불량IP 필터링<br>
      <font color="#FF0000">[미완]</font></th>
      <td class="pl20"><textarea name="bs_badip" rows="5" id="bs_badip" style="width:100% "><%=rs("bs_badip")%></textarea></td>
    </tr>
	-->
    <tr>
      <th>타이틀 이미지</th>
      <td class="pl20">
	  <% if len(rs("file_titleimg")) > 0 then %>
	  <table width="100%"  border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
        <tr>
          <td width="50"><div align="center">등록된<br>
            이미지</div></td>
          <td><img src='/upload/<%=rs("file_titleimg")%>'></td>
        </tr>
      </table>        
      <% end if %>
	    <input type="radio"  style="border:0px;background-color:transparent" name="bs_titleimg" value="1" <%call radio_checked_bit(true,rs("bs_titleimg"))%>>
          사용
            <input name="bs_titleimg" type="radio"  style="border:0px;background-color:transparent" value="0" <%call radio_checked_bit(false,rs("bs_titleimg"))%>>
          사용안함
          <input name="file_titleimg" type="file" id="file_titleimg">
      (jpg,gif만 가능)</td>
    </tr>
    <tr>
      <th>게시글 그룹핑</td>
      <td><input name="bs_grouping" type="text" id="bs_grouping" value="<%=rs("bs_grouping")%>" size="10" maxlength="20">
      10자 이내 </td>
    </tr>
</table>

<ul class="btnWrap">
	<li><input type="submit" name="Submit"  value=" 게시판 수정 " class="btn_m c_blue"></li>
	<li><input type="button" value=" 목록 "  class="btn_m c_gray"  onclick="self.location.href = '?<%=getString("vmode=boardlist")%>';"></li>
</ul>
</form>

</body>
<%
	rs.close
	set rs = nothing
	DBclose
%>
</html>
