<!--#include file="../_config.asp"-->
<!--#include file="../BoardSet/_BoardsettingInfo.asp"-->
<html>
<head>
<!--#include file="form_chk.js"-->
<style>
.cssAvanJoinFormTrBar {background-color:#DEDEDE;}
.cssAvanJoinFormTrHeight {height:35px;}
#divTableForm td {vertical-align:middle;}
.board_form { font-size:14px; color:#666666; }
.board_form td {  }
.board_form input[type=text] { height:24px; line-height: 28px; border: 1px solid #ddd; font-size: 14px; padding-left:5px; }
</style>
</head>

<body>
<!-- 게시판 상단 타이틀 이미지 -->
<!--#include file="board_title.asp"-->
<%
	'response.write bs_writelaw
	'response.write session("userlevel")
if Authority(bs_writelaw) then%>
<div id="divTableForm">
<table class="board_form" width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="<%=bs_color%>" style="background-color:<%=bs_color%>;">
	<colgroup>
    	<col style="width:15%">
        <col style="width:85%">
    </colgroup>
	<form name="form1"  method="post" enctype="multipart/form-data" action='?<%=getString("vmode=exe_insert&b_idx=")%>'>
	<tr class="cssAvanJoinFormTrHeight">
		<td height="25" valign="middle" align="center" ><font color="#FFFFFF" ><B>새 글 등 록</B></font></td>
		<td bgcolor="#FFFFFF" align="right" style="background-color:#FFFFFF;"><!--<a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a> --></td>
	</tr>
	<tr>
		<td colspan="2" height="4"></td>
	</tr>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td  height="41" style="padding:6 0 2 40;"><img src=<%=Img_point%> border="0"> <b>제목</b></td>
		<td style="padding:6 0 2 0;">
			<input name="b_title" type="text" id="b_title" size="50" maxlength="50" required>		</td>
	</tr>
	<tr bgcolor="#FFFFFF"  style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>글쓴이</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
			<input name="b_name" type="text" id="b_name" value='<%=session("username")%>' size="20" maxlength="20" required <%if bs_Writelaw > 0 and session("userlevel")<9 then%>readonly<% end if %>>		</td>
	</tr>

	<% if bs_jumin then %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>주민번호</td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0">
			<input name="b_jumin1" type="text" id="b_jumin1" size="10" maxlength="6"> -
			<input name="b_jumin2" type="password" id="b_jumin2" size="10" maxlength="7">		</td>
	</tr>
	<% end if %>

	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>이메일</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
			<input name="b_email" type="text" id="b_email" size="50" maxlength="50" required>		</td>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;">
	  <td colspan="2">
			<%call Editor("b_text","")%>
			<!-- input type="button" value="이미지파일업로드" style="width:100px;height:30px;padding-top:1px;font-size:11px;" onClick="window.open('/AVANplus/LIB/KNEditor_FIle/FileUpload.asp','imgfileupload','width=500,height=500,left=200,top=150,scrollbars=no');">
		&lt;**** 에디터에 삽입할 이미지가 있으시면 이미지파일 업로드를 이용하세요. -->		</td>
	  </tr>

	<% if bs_bimilLaw then %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>비밀글 설정</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
			<font color="#003366">
				<input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="1" checked >비밀글
				<input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="0" >일반글			</font>		</td>
	</tr>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td background="/avanplus/modul/avanboard_v3//img/bg1000.gif" style="padding:6 0 2 40; background-image:url('/avanplus/modul/avanboard_v3//img/bg1000.gif');"><img src=/avanplus/img/avanboard/b_prepoint.gif border="0"> 비밀번호</td>
		<td background="/avanplus/modul/avanboard_v3//img/bg1000.gif" style="padding:6 0 2 0; background-image:url('/avanplus/modul/avanboard_v3//img/bg1000.gif');">
			<input name="b_pass" type="password" id="b_pass" size="20" maxlength="20" required>		</td>
	</tr>
	<% end if %>

	<% if bs_file then %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>첨부파일</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
			<input name="file_1" type="file" id="file_1" size="40"> [최대 <%=bs_filesize%>MB]		</td>
	</tr>
	<% end if %>

	<% if bs_writelaw = 0 then '//비회원 글쓰기 가능이면 %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>비밀번호</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
			<input name="b_pass" type="password" id="b_pass" size="20" maxlength="20" required>		</td>
	</tr>
	<% end if %>

	<% if admin_chk() then %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>공지글등록</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
            <input type="radio"  style="border:0px;background-color:transparent" name="option_notice" value="1">공지글 &nbsp;&nbsp;
            <input name="option_notice" type="radio"  style="border:0px;background-color:transparent" value="0" checked>일반글
        </td>
	</tr>
	<% end if %>

	<tr>
		<td height="2" colspan="2"></td>
	</tr>

	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td colspan="2" align="right" style="padding-top:20px;">
			<input  TYPE="IMAGE" src="<%=Img_entry%>" name="Submit" value="Submit">
			<img src=<%=Img_cancel%> align="top" alt=" 다 시 " border="0" onClick="document.form1.reset();">
			<img src=<%=Img_back%> align="top" alt=" 뒤 로 " border="0" onClick="history.back(-1);">
            <a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a>
		</td>
	</tr>
	<input name="b_part" type="hidden" id="b_part" value="<%=bs_code%>">
	<input name="b_writeday" type="hidden" id="b_writeday" value="<%=date()%>">
	<input name="b_ip" type="hidden" id="b_ip" value="<%=request.ServerVariables("remote_addr")%>">
	<input name="userid" type="hidden" id="userid" value="<%=session("userid")%>">
	</form>
</table>
</div>
<% else %>

<br>
<br>
<br>
<br>
<center>
<table width="400" border="0" align="center" cellpadding="3" cellspacing="0" bgcolor="<%=bs_color%>" style="background-color:<%=bs_color%>;">
	<tr>
		<td height="30" style="vertical-align:middle;"><div align="center" class="WHITE"><font color="#FFFFFF"><strong>글등록 권한이 없습니다. </strong></font></div></td>
	</tr>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;">
		<td height="50" background="<%=Programpath%>/img/bg1000.gif" style="background-image:url('<%=Programpath%>/img/bg1000.gif');vertical-align:middle;">
			* <font color="#FF0000">회원을 위한 공간입니다. </font><br>
			* 권한이 있는 사용자로 로그인 후 이용해 주세요.<%=session("userlevel")%></td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td height="50" align="center" background="<%=Programpath%>/img/bg1000.gif" style="background-image:url('<%=Programpath%>/img/bg1000.gif');vertical-align:middle;" >
			<img src=<%=Img_back%> alt=" 뒤 로 " border="0" align="top" onClick="history.back(-1);">
		</td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
</table>
</center>
<% end if %>
<!-- <br>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
	<tr>
		<td align="right"><a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a></td>
	</tr>
</table> -->
</body>
</html>
