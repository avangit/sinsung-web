
<!--#include file="../_config.asp"-->
<!--#include file="../BoardSet/_BoardsettingInfo.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--#include file="form_chk.js"-->
<style type="text/css">
.cssAvanBdViewCenter {display:table; margin-left:auto; margin-right:auto;}
.cssAvanBdViewTable {background:<%=bs_color%>;line-height:30px; border-left:1px solid <%=bs_color%>; border-right:1px solid <%=bs_color%>; border-bottom:1px solid <%=bs_color%>;}
.cssAvanBdViewTableTr {background:#fff;}
.cssAvanJoinFormTrHeight {height:35px;}
.cssAvanBdViewTableTrLine {background:#CCCCCC;}
.board_form { font-size:14px; color:#666666; }
.board_form td {  }
.board_form input[type=text] { height:24px; line-height: 28px; border: 1px solid #ddd; font-size: 14px; padding-left:5px !important; }
.rewrite_msg .fsize { font-size:14px; text-align:center; padding:20px; color:#666666; }
</style>
</head>

<body>
<%
	DBopen
	dim sql, rs
	dim b_idx : b_idx = request.QueryString("b_idx")
	sql = "select * from board_v1 where b_idx = '" & b_idx & "'"
	set rs = DBconn.execute(sql)
%>
<!-- 게시판 상단 타이틀 이미지 -->
<!--#include file="board_title.asp"-->
<!-- 답글등록 권한 체크 -->
<% if Authority(bs_rewritelaw) then%>
<table class="board_form" width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="<%=bs_color%>">
	<colgroup>
    	<col style="width:15%">
        <col style="width:85%">
    </colgroup>
	<form name="form1" method="post" enctype="multipart/form-data" action='?<%=getString("vmode=exe_insert&ref="&rs("ref"))%>'>
	<tr class="cssAvanJoinFormTrHeight">
		<td height="25" valign="middle" align="center">
			<font color="#FFFFFF"><B>답변</B></font>
		</td>
		<td bgcolor="#FFFFFF" align="right">
			<!-- <a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a> -->
		</td>
	</tr>
	<tr>
		<td height="4"></td>
		<td height="4"></td>
	</tr>
	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td height="41" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>제목</b></td>
		<td style="padding:6 0 2 0">
			<input name="b_title" type="text" id="b_title" size="50" maxlength="50" required>
			<input name="b_part" type="hidden" id="b_part" value="<%=bs_code%>">
			<input name="b_writeday" type="hidden" id="b_writeday" value="<%=date()%>">
			<input name="b_ip" type="hidden" id="b_ip" value="<%=request.ServerVariables("remote_addr")%>">
			<input name="userid" type="hidden" id="userid" value="<%=session("userid")%>">
		</td>
	</tr>
	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>글쓴이</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0">
			<input name="b_name" type="text" id="b_name" value='<%=session("username")%>' size="20" maxlength="20" required <%if bs_rewriteLaw > 0 then%>readonly<% end if %>>
		</td>
	</tr>

	<% if bs_jumin then %>
	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>주민번호</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0">
			<input name="b_jumin1" type="text" id="b_jumin1" size="10" maxlength="6"> -
			<input name="b_jumin2" type="password" id="b_jumin2" size="10" maxlength="7">
		</td>
	</tr>
	<% end if %>

	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>이메일</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0">
			<input name="b_email" type="text" id="b_email" size="50" maxlength="20" required>
		</td>
	</tr>
	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
	  <td colspan="2" style="padding-bottom:10px;">
		<%
			dim b_text
			b_text = "<br><br><br><br><br><br><br>---------------------------------------------------------------<br><br>"
			b_text = b_text & rs("b_text")
			call Editor("b_text",b_text)
		%></td>
	  </tr>

	<% if bs_bimilLaw then %>
	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>비밀글 설정</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0">
			<font color="#003366">
				<input name="option_bimil" type="radio" value="1" checked >비밀글
				<input name="option_bimil" type="radio" value="0" >일반글
			</font>
		</td>
	</tr>
	<% end if %>

	<% if bs_file then %>
	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>첨부파일</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0">
			<input name="file_1" type="file" id="file_1" size="40"> [최대 <%=bs_filesize%>MB]
		</td>
	</tr>
	<% end if %>

	<% if bs_writelaw = 0 then '//비회원 글쓰기 가능이면 %>
	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>비밀번호</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0">
			<input name="b_pass" type="password" id="b_pass" size="20" maxlength="20" required>
		</td>
	</tr>
	<% end if %>

	<% if admin_chk() then %>
	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40"><img src=<%=Img_point%> border="0"> <b>공지글등록</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0">
            <input type="radio" name="option_notice" value="1">공지글
            <input name="option_notice" type="radio" value="0" checked>일반글
		</td>
	</tr>
	<% end if %>

	<tr>
		<td colspan="2" height="2"></td>
	</tr>

	<tr bgcolor="#FFFFFF" class="cssAvanJoinFormTrHeight">
		<td colspan="2" align="right" style="padding-top:20px;">
			<input  TYPE="IMAGE" src="<%=Img_entry%>" name="Submit" value="Submit">
			<!-- <img src=<%=Img_entry%> align="top" alt=" 등 록 " border="0" onClick="return check();"> -->
			<img src=<%=Img_cancel%> align="top" alt=" 다 시 " border="0" onClick="document.form1.reset();">
			<a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a>
<!-- 			<img src=<%=Img_back%> align="top" alt=" 뒤 로 " border="0" onClick="history.back(-1);"> -->
		</td>
	</tr>
	</form>
</table>
<%else%>
<br>
<br>
<br>
<br>
<div class="cssAvanBdViewCenter">
<table width="400" border="0" align="center" cellpadding="3" cellspacing="0" bgcolor="<%=bs_color%>" class="cssAvanBdViewTable rewrite_msg">
	<tr>
		<td height="50" align="center" class="WHITE"><font color="#FFFFFF"><B>답글등록 권한이 없습니다.</B></font></td>
	</tr>
	<tr bgcolor="#CCCCCC" class="cssAvanBdViewTableTrLine">
		<td height="1"></td>
	</tr>
	<tr bgcolor="#FFFFFF" class="cssAvanBdViewTableTr">
		<td height="50" class="fsize">
			<font color="#bf141d">회원제 게시판 입니다. </font><br>
			권한을 가진 사용자로 로그인후 이용해 주세요.
		</td>
	</tr>
	<tr bgcolor="#FFFFFF" class="cssAvanBdViewTableTr">
		<td height="50" align="center">
			<img src=<%=Img_back%> alt=" 뒤 로 " border="0" align="top" onClick="history.back(-1);">
		</td>
	</tr>
</table>
</div>
<% end if %>

<!-- <table width="100%" border="0" cellspacing="0" cellpadding="10">
	<tr>
		<td align="right">
			<a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a>
		</td>
	</tr>
</table> -->
<%
	rs.close
	set rs = nothing
	DBclose
%>
</body>
</html>
