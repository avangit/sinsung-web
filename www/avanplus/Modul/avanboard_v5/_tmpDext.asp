
<%
	'//*------------------------------------------------------------
	' 소  속 : 아반소프트 avansoft.co.kr
	' 작성자 : 이덕권 (leedk7@msn.com)
	' 작성일 : 2004년 1월 29일
	' ***[설명]***
	' 덱트트 업로드 관련 함수모음
	'-------------------------------------------------------------*//

	'//사용법
	'DextOpen(byval UpFolder)
	'DicOpen
	'	QueryInsert(byval TableName,byval RequestData)
	'	QueryUpdate(byval TableName,byval RequestData,byval Where)
	'	QueryDelete(byval TableName,byval Where)
	'	dbconn.execute(쿼리문)
	'DicClose
	'DextClose

	dim RequestData '폼으로부터 넘어온 값을 받는 변수

	' 덱스트 열기
	dim Dext
	Sub DextOpen(byval UpFolder)
		Set Dext = Server.CreateObject("DEXT.FileUpload")
		Dext.AutoMakeFolder = true
		Dext.DefaultPath = Server.MapPath( UpFolder ) & "\"
		Dext.CodePage = 65001 '<--추가 ( utf-8 일경우)
	End Sub


	' 덱스트 닫기
	Sub DextClose
		set Dext = nothing
	End Sub



	'//기존의 파일을 삭제하는 함수
	sub Filedel(byval tablename,byval filename,byval where)
		dim filepath
		'// 파일의 이름을 가져와 해당파일이 존재하는지 검사
		dim rs, sql
		sql = "select "& filename &" from "& tablename &" where "& where
		set rs = dbconn.execute(sql)

		if len(rs(0)) > 0 then	'//레코드에 파일명이 입력되어있으면
			filepath = dext.DefaultPath & "\" & rs(0)
			'response.Write(filepath)
			If Dext.FileExists(filepath) Then '//파일이 존재하는지 체크 / 해당파일이 있다면
				Dext.DeleteFile filepath	'//파일삭제
			End If

			dbconn.execute("update "& tablename &" set "& filename &"='' where "&where)	'//레코드 삭제
		end if
	End Sub


	' dic 열기
	Sub DicOpen
		'데이타를 Dictionary 컬랙션에 저장
		dim data
		dim strName
		dim strValue
		dim upfile

		set RequestData = CreateObject("Scripting.Dictionary")


		'//외부에서 들어오는 글 거르기
		if split(request.ServerVariables("HTTP_REFERER"),"/")(2) <> request.ServerVariables("HTTP_HOST") then response.End()
		'Response.Write(dext.defaultpath)

		For Each data In Dext.form
				strName = data.name
				strValue = data.value

				'//필요없는 부분들을 삭제
				if len(strValue) = 0 or Ucase(Mid(strName,1,6)) = "SUBMIT" then

				elseif Ucase(Mid(strName,1,5)) = "FILE_" then

					strvalue = Dext(data.name).filename
					strValue = Dext(data.name).SaveAs(Dext.DefaultPath & strValue, False)
					strValue = Dext(data.name).LastSavedFileName

					RequestData.add strName, strValue

				else
					RequestData.add strName, strValue
					'response.Write strName & ":" & strValue & "<br>"
				end if
		Next
	End Sub


	' dic 닫기
	Sub DicClose
		set RequestData = nothing
	End Sub



	Function FileSave(byval TF, byval FormName )
		'TF : true 중복저장 false 새로운 이름으로 저장
		'FormName : DEXT로 넘어온 값 dext("file_sample")

		FileSave = FormName.filename
			'response.Write "<br>1 "&FileSave
		FileSave = FormName.SaveAs(Dext.DefaultPath & FileSave, TF)
			'response.Write "<br>2 "&FileSave
		FileSave = FormName.LastSavedFileName
			'response.Write "<br>3 "&FileSave
			'response.Write "<br>3 "&FormName
	End Function



	Function FileDelete(byval tablename, byref FormName )

	End Function





	' 파일 다운로드
	Function FileDown(ByVal FilePath, ByVal filename)

		'Response.Buffer = False
		'filepath = Request.QueryString("file")
		FilePath = Server.MapPath( FilePath ) & "\" & filename
		'filename = Mid(filepath, InStrRev("\")+1)

		Response.AddHeader "Content-Disposition","inline;filename=" & filename

		set objFS = Server.CreateObject("Scripting.FileSystemObject")
		set objF = objFS.GetFile(filepath)
		Response.AddHeader "Content-Length", objF.Size
		set objF = nothing
		set objFS = nothing

		Response.ContentType = "application/x-msdownload"
		Response.CacheControl = "public"

		Set objDownload = Server.CreateObject("DEXT.FileDownload")
		objDownload.Download filepath
		Set uploadform = Nothing


	'## 다운로드할 파일의 전체경로와 함께 상기 ASP페이지를 실행하면 해당 파일을 다운로드 받을 수 있게 된다.
	'## 페이지 상단을 보면 Response.Buffer = False 로 지정하여 버퍼링 없이 즉시 파일을 다운로드 받을 수 있게 했고,
	'## Content-Disposition, Content-Length, ContentType 등의 헤드정보를 지정한 후 Download Method를 호출하고 있다.
	'## 사용자는 웹브라우저에 나타나는 대화상자에 다운로드할 경로와 파일명을 지정하여 다운로드 받을 수 있다.

	End Function


%>
