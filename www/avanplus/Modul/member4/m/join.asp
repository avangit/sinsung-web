<!--#include file= "config.asp"-->
<link href="/avanplus/modul/member4/m/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script> 
 <script type="text/javascript">
	function ckId(){
		var x  ; 
		x = document.getElementById("strId").value;
		if( x == 0){
			alert("사용하실ID를 입력하세요");
		}
		else{
			$.ajax({   
				type: "POST", 
				url: "/avanplus/modul/member4/m/ajaxIdcheck.asp",   
				data: "ckid="+x,   //&a=xxx 식으로 뒤에 더 붙이면 됨
				success: function (data){
					var msg;
					msg = data;
					if (msg == 0)
					{
						var idck_value = 1;
						alert("사용가능합니다.");
						document.joinform.idck.value = idck_value
						document.joinform.strPwd.focus(); return false;
					}
					else{
						alert(msg);
						document.joinform.strId.value = "";
						document.joinform.strId.focus(); return false;
					}	
				}
			});
		}
	}

</script> 

<form name = "joinform" action = "?member4=joinok" method = "post" enctype="multipart/form-data" onSubmit="return checkjoin(this)">
	<input type="hidden" name="idck" value="0">
	<table class="com_m_input">
		<colgroup>
			<col width="20%" /><col />
		</colgroup>
		<tbody>    
			<tr>
    			<th>아이디</th> 
				<td>
					<input type="text" name="strId" id="strId" class="com_m_w50" onKeyup="document.joinform.idck.value == 0;"/ >
					<!--button class="com_m_btn_S01" type="button" onClick="checkid()">중복확인	</button-->
					<button class="com_m_btn_S01" type="button" onClick="ckId()">중복확인</button>
					4~12자 미만의 영문
				</td>
			</tr>
    
			<tr>
				<th>비밀번호</th>
				<td>
					<input type="password" name="strPwd" id="strPwd"/> 
					4~12자 미만의 영문 / 숫자
				</td>
			  </tr>
			
			<tr>
				<th>비밀번호 확인</th>
				<td>
					<input type="password" name="strPwd2" id="strPwd2" />
				</td>
			 </tr>
			
			<tr>
				<th>이름</th>
				<td>
					<input type="text" name="strName" id="strName"  />
				</td>
			  </tr>
			<tr>
				<th>휴대번호</th>
				<td>
					<% Call Form_HandphonePrint("strMobil","")%>
				</td>
			</tr>
			<tr>
				<th>이메일</th>
				<td>
					<% Call Form_EmailPrint("strEmail","")%>
				</td>
			</tr>
		    <tr>
				<th>사업신청</th>
				<td>본사이트 무료 회원가입 하신후 등업신청을 반드시 해주시기 바랍니다.</td>
			</tr>
			<tr>
				<th>소개받은 <br />
			    사이트 *</th>
				<td>
					<input type="radio" id="Facebook" class="com_m_ridio" checked="checked"/>페이스북 
					<input type="radio" id="cafe" class="com_m_ridio"/>카페 
					<input type="radio" id="naver" class="com_m_ridio"/>네이버 검색 
					<input type="radio" id="not" class="com_m_ridio"/>해당없음
				</td>
			</tr>    
			<tr>
				<th colspan="2"><p class="com_m_line"></p></th>
			</tr>


		</tbody>    
	</table>
</form>	
<div class="com_m_btn">
	<button class="com_m_btn_SR01" type="button" onclick="checkjoin()">가입</button>
	<button class="com_m_btn_SR02"onclick="loginback()">취소</button>
</div>

    
			<script language="javascript">
				<!--
					//영문자 숫자체크
					function check_char(input_str) {
						var alpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						var numeric = '1234567890';
						var nonkorean = alpha+numeric;
					
						for (var i=0; i < input_str.length; i++ ) {
							if( nonkorean.indexOf(input_str.substring(i,i+1)) < 0) {
								break ;
							}
						}
						
						if ( i != input_str.length ) {
							return false ; 
						} else{
							return true ;
						}
						return true;
					}
					frm=document.joinform
					function checkjoin()
					{
						if ( frm.strId.value.replace(/ /gi, "") == "" ) { alert("아이디를 입력해주세요"); frm.strId.focus(); return false; }
						
						if ( frm.strPwd.value.replace(/ /gi, "") == "" ) { alert("패스워드를 입력해주세요"); frm.strPwd.focus(); return false; }
						if ( (frm.strPwd.value.length < 4) || (frm.strPwd.value.length > 12)) { alert("패스워드를 4~12자 미만으로 입력해주세요."); frm.strPwd.focus(); return false; }
						if (!check_char(frm.strPwd.value)){alert("비밀번호는 영문자,숫자만 사용하실수 있습니다.");frm.strPwd.focus();return false;}
						if ( frm.strPwd.value != frm.strPwd2.value ) { alert("비밀번호가 일치하지 않습니다..\비밀번호를 다시 입력해주세요."); frm.strPwd2.focus(); return false; }

						if ( frm.strName.value.replace(/ /gi, "") == "" ) { alert("이름을 입력해주세요"); frm.strName.focus(); return false; }
						if ((frm.strId.value.length < 4) || (frm.strId.value.length > 12)) { alert("아이디를 4~12자 미만으로 입력해주세요."); frm.strId.focus(); return false; }

						if ( frm.strMobil.value.replace(/ /gi, "") == "" ) { alert("핸드폰번호를 입력해주세요"); frm.strMobil1.focus(); return false; }
						if ( frm.strEmail1.value.replace(/ /gi, "") == "" ) { alert("이메일을 입력해주세요"); frm.strEmail1.focus(); return false; }
						if ( frm.idck.value != 1) { alert("아이디 중복체크를 해주세요."); return false; }
						document.joinform.submit()
					}
					//function checkid()
					//{
					//	if ( (document.joinform.strId.value.length < 4) || (document.joinform.strId.value.length > 12)) { alert("아이디를 4~12자 미만으로 입력해주세요.");document.joinform.strId.focus(); return false; }
					//	else {
					//	var newwindow="toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=324,height=197";
					//	window.open('/AVANplus/Modul/member4/checkId.asp?checkid='+document.joinform.strId.value,'checkid',newwindow);}
					//}
					function searchpost()
					{
						var newwindow="toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=344,height=230";
						window.open('/AVANplus/Modul/member4/post/searchpost.asp','post',newwindow);
					}
					function loginback()
					{
						location.replace('login.asp');

					}
				//-->
			</script>
   
    
