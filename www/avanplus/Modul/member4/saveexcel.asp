<%@  codepage="65001" language="VBScript" %>

<!-- METADATA TYPE="typeLib"  NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<Object RUNAT="Server" PROGID="ADODB.Connection" ID="DbConn"></Object>
<%
	
	Response.Buffer=true
	Response.ContentType = "application/vnd.ms-excel; name='My_Excel'"
	Response.CacheControl = "public"
	Response.AddHeader "Content-Disposition","attachment;filename=MemberList.xls"
	%><!-- #include virtual="/DbConn.asp" --><%
	
	'// 디비열기
	Function DbOpen()
		'dim cstConnString	'//디비커넥션변수
		
		If DbConn.State = adStateClosed Then
			DbConn.open cstConnString
		End If
	End Function
	
	'// 디비닫기
	Function DbClose()
		If DbConn.State = adStateOpen Then
			DbConn.Close()
		End If
	End Function


	'// 쿼리 실행후 레코드셋 배열로 반환 없음 Null 반환
	Function getAdoRsArray(ByVal strQuery)
		
		Dim objAdoRs, arrSelectData
		
		'Print strQuery

		Set objAdoRs = Server.CreateObject("ADODB.RecordSet")

			objAdoRs.Open strQuery, DbConn, adOpenForwardOnly, adLockReadOnly, adCmdText
				If Not (objAdoRs.EOF And objAdoRs.BOF) Then 
					arrSelectData = objAdoRs.GetRows()
				Else
					arrSelectData = Null
				End If
			objAdoRs.Close

		Set objAdoRs = Nothing

		getAdoRsArray = arrSelectData

	End Function

	DbOpen()
	Dim ir,arr,intAction,intGubun,strSQL,strWhere
	
	strid 		= request.QueryString("strid")
	strName 	= request.QueryString("strName")
	intGubun 	= request.QueryString("intGubun")
	strStart 	= request.QueryString("strStart")
	strEnd	 	= request.QueryString("strEnd")

	'If strid <> "" Then
	'	strWhere  = " and strid like '"&strid&"%'"
	'End If
	'If strName <> "" Then
	'	strWhere  = " and strName like '"&strName&"%'"
	'End If
	
	'If strStart <> "" and strEnd <> "" Then
	'	strWhere = strWhere &  " and intGubun between '" & strStart & "' and '"&strEnd&"'"
	'ElseIf strStart <> "" Then
	'	strWhere = strWhere &  " and intGubun >= '" & strStart  &"'"
	'ElseIf strEnd <> "" Then
	'	strWhere = strWhere &  " and intGubun <= '" & strEnd &"'"
	'End If
	
	strWhere = strWhere &  request.form("quy")
	strWhere = strWhere &  " and not intAction in (1,2,3) "
	
	strSQL = " SELECT intSeq, strName, strJumin1, strJumin2, strId, strPwd, strZip, strAddr1, strAddr2,strPhone, strMobil, strEmail, strJob, dtmInsertDate, strMemo, intGubun,intAction,strActionMessage FROM mtb_member2  where "&strWhere&" ORDER BY intseq ASC "
	'response.write strsql
	
	
	
	arr = getAdoRsArray(strSQL)
	
	
	DbClose()

%>
<html>
<head>
<title>회원정보</title>
</head>
<body>
<table cellpadding="0" cellspacing="0" border="1">
	<tr>
		<td>번호</td>
		<td>이름</td>
		<!--td>주민번호</td-->
		<td>아이디</td>

		<td>우편번호</td>
		<td>주소</td>
		<td>전화번호</td>
		<td>핸드폰번호</td>
		<td>이메일</td>
		<td>직업</td>

		<td>구분</td>
		<td>상태</td>
	</tr>
	<%

		If IsArray(arr) Then
			For ir = 0 To UBound(arr, 2)				
	%>
	<tr>

		<td><%=ir+1%></td>
		<td><%=arr(1, ir)%></td>
		<!--td><%=arr(2, ir)%>-<%=left(arr(3, ir),1)%>******</td-->
		<td><%=arr(4, ir)%></td>

		<td><%=arr(6, ir)%></td>
		<td><%=arr(7, ir)%> <%=arr(8, ir)%></td>
		<td><%=arr(9, ir)%></td>
		<td><%=arr(10, ir)%></td>
		<td><%=arr(11, ir)%></td>
		<td><%=arr(12, ir)%></td>

		<td><%response.write "Lv."&arr(15, ir)%></td>
		<td>
			<%
			if Len(arr(16, ir)) = 0 or isnull(arr(16, ir)) or arr(16, ir) = "0" then
				intAction = "정상"
			elseif arr(16, ir) = "1" then
				Active = "<font color=#FF9900>강퇴</font>"
			elseif arr(16, ir) = "2" then
				intAction = "<font color=red>탈퇴</font>"
			elseif arr(16, ir) = "3" then
				intAction = "<font color=red>임시제한</font>"
			end if
			response.write intAction%>		
		</td>

	</tr>
	<%
			Next
		End If
	%>
</table>
</body>
</html>