<!--#include file = "config.asp"-->
<%
	dim fieldname 	: fieldname 	= Request.querystring("fieldname")
	dim fieldvalue 	: fieldvalue 	= Request.querystring("fieldvalue")
	dim strStart 	: strStart 		= Request.querystring("strStart")
	dim strEnd 		: strEnd 		= Request.querystring("strEnd")
	dim mcate 		: mcate 		= Request.querystring("mcate")

	dim intTotalCount, intTotalPage, strnic

	dim intNowPage			: intNowPage 		= Request.QueryString("page")
	dim intPageSize			: intPageSize 		= 30
	dim intBlockPage		: intBlockPage 		= 30

	dim query_filde			: query_filde		= "*"
	dim query_Tablename		: query_Tablename	= TableName
	dim query_where			: query_where		= " intSeq > 0 "
	dim query_orderby		: query_orderby		= "order by intseq desc"


	if Len(fieldname) > 0 then
		query_where = query_where &" and "& fieldname & " like '%" & fieldvalue & "%' "
	end if

	if request.querystring("sms") = "Y" then
		query_where = query_where &" and strsmsok='Y'"
	end if
	if request.querystring("mail") = "Y" then
		query_where = query_where &" and strmailok='Y'"
	end if

	if len(request.querystring("area")) > 0 then
		query_where = query_where &" and strAddr1 like '%"& request.querystring("area") &"%'"
	end if

	If strStart <> "" and strEnd <> "" Then
		query_where = query_where &  " and intGubun between '" & strStart & "' and '"&strEnd&"'"
	ElseIf strStart <> "" Then
		query_where = query_where &  " and intGubun >= '" & strStart  &"'"
	ElseIf strEnd <> "" Then
		query_where = query_where &  " and intGubun <= '" & strEnd &"'"
	End If

	If mcate <> "" Then
		query_where = query_where &  " and storeCheck <> N''"
	End If

	call intTotal

	dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

	dim sql, rs
	sql = getQuery
	call dbopen
	set rs = dbconn.execute(sql)
%>

<script language="JavaScript">
<!--
function really(){
if (confirm('\n정말로 삭제하시겠습니까? 삭제는 복구가 불가능합니다.\n')) return true;
return false;
}
function winopen1(str){
if (str ==""){
	window.open('/avanplus/modul/member4/mail_content.asp','','width=650,height=550,scrollbars=yes,left=100,top=100')
}
else if(str != ""){
	window.open('/avanplus/modul/member4/mail_content.asp?useremail='+str,'','width=650,height=570,scrollbars=yes') }

}

function openwin(theURL,winName,features){
	window.open(theURL,winName,features);
}


//-->
</script>

<div>
<table class="tableList left_table" cellpadding="0" cellspacing="0">
	<colgroup>    
        <col style="width:15%">
        <col style="width:85%">
    </colgroup>
	<tr>
		<form name="form1" method="get" action="?<%=getstring("")%>">
		<th rowspan="2">검색조건</th>
		<td>
			<%Call Form_NumberSelectPrint("strStart", strStart, "레벨", 1, 9, "")%> ~ <%Call Form_NumberSelectPrint("strEnd", strEnd, "레벨", 1, 9, "")%>&nbsp;&nbsp;
            <input name="sms" type="checkbox" id="sms" value="Y" <%if request.querystring("sms") = "Y" then response.Write("checked")%>/> 문자Y&nbsp;&nbsp;
            <input name="mail" type="checkbox" id="mail" value="Y" <%if request.querystring("mail") = "Y" then response.Write("checked")%> /> 메일Y&nbsp;&nbsp;
            <select name="fieldname">
                <option value="strId" <%if request.querystring("fieldname") = "strId" then response.Write("selected")%>>아이디</option>
                <option value="strName" <%if request.querystring("fieldname") = "strName" then response.Write("selected")%>>성명</option>
				<% if oBprint("str_memVer") = "410" then %>
                <option value="strNic" <%if request.querystring("fieldname") = "strNic" then response.Write("selected")%>>닉네임</option>
				<% end if %>
            </select>
            <input name="fieldvalue" type="text" Class="w20" value="<%=request.querystring("fieldvalue")%>" size="10">
            <input name="avan" type="hidden" id="avan" value="<%=request.QueryString("avan")%>" /><br>
        </td>
	  	</form>
	</tr>
	<tr>
		<td>
            <form name="form1" method="get" action="?<%=getstring("")%>">
                지역조건
                <input name="area" type="text" class="input01 w30" id="area" value="<%=request.querystring("area")%>" size="10" />
                (지역조건은'서울','강남구'등이 들어간 주소를 검색함) 
                <input type = "image" src="/AVANplus/modul/member4/img/btn_postsearch.gif"  style="cursor:hand;border:0px; height:28px;" align="absmiddle">
            </form>
            <form action="/AVANplus/modul/member4/saveexcel.asp" method="post" name="saveExcel" target="_blank" id="saveExcel" >
                <!--a href="/AVANplus/modul/member4/saveexcel.asp?<%=fieldname%>=<%=fieldvalue%>&strStart=<%=strStart%>&strEnd=<%=strEnd%>">검색조건회원 엑셀다운로드</a-->
                <input name="quy" type="hidden" value="<%=query_where%>" />
                <input type="submit" name="Submit" class="btnDB btnS w20" value="검색된회원 엑셀다운로드" />
            </form>
        </td>
	</tr>    
</Table>
<br>
  
<table class="w100 tableSearch"  height="40" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="tleft w20"><span class="t_red">회원리스트 :</span>  (Total : <%=intTotalCount%>명)</td>
        <td class="tright w50">검색된 조건에 만족하는 회원에게 SMS및 메일 발송 --&gt;&nbsp;     
            <form action="?<%=getstring("member4=mail")%>" method="post" name="form1" id="form1" class="display">
                <input type = "hidden" name = "qry" value = "<%=query_where%>" />
                <input name="submit" type = "submit"  class="btnS btnG" value="메일" src = "/AVANplus/modul/member4/img/btn_mail.gif" />
            </form>
            <form action="?<%=getstring("member4=sms")%>" method="post" name="form1" id="form1" class="display">
                <input type = "hidden" name = "qry" value = "<%=query_where%>" />
                <%if oBprint("str_smsUse") = "1" then%>
                <input name="submit" type = "submit" class="btnS btnG" value="SMS" src = "/AVANplus/modul/member4/img/btn_sms.gif">
                <%Else%>
                <img src = "/AVANplus/modul/member4/img/btn_sms.gif" onclick="alert('SMS설정후 사용해주세요. ')" />
                <%End If%> 
            </form>
        </td>
	</tr>
</table>  

<table class="tableList center_table" cellpadding="0" cellspacing="0">
    <colgroup> 
        <col style="width:8%">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:">
        <col style="width:10%">
        <col style="width:8%">
        <col style="width:8%">
        <col style="width:8%">            
    </colgroup>
	<tr>
		<th>번호</th>
		<th>아이디</th>
		<th>성명<% if oBprint("str_memVer") = "410" then %><font color="#FFFFFF">(닉네임)</font><% end if %></th>
		<th>이메일</th>
		<th>가입일</th>
		<th>상태</th>
		<th>구분</th>
		<th>삭제</th>
    </tr>
<%
	if not rs.eof then
	rs.move MoveCount
	Do while not rs.eof
	
	intseq			 = rs("intseq")
	strId 			 = rs("strId")
	strName 		 = rs("strName")
	strJumin1 		 = rs("strJumin1")
	strJumin2 		 = rs("strJumin2")
	strPwd 			 = rs("strPwd")
	dtmInsertDate    = rs("dtmInsertDate")
	intAction 		 = rs("intAction")
	intGubun 		 = rs("intGubun")
	stremail 		 = rs("stremail")
	'storeCheck		 = rs("storeCheck")
	
	if oBprint("str_memVer") = "410" then
	strnic 		 	= rs("strnic")
	end if
	
	if Len(intAction) = 0 or isnull(intAction) or intAction = "0" then
		intAction = "정상"
	elseif intAction = "1" then
		Active = "<font color=#FF9900>강퇴</font>"
	elseif intAction = "2" then
		intAction = "<font color=red>탈퇴</font>"
	elseif intAction = "3" then
		intAction = "<font color=red>임시제한</font>"
	end if
%>
    <tr>
    	<td><%=intNowNum%></td>
        <td><a href="?<%=getstring("member4=memberview&intseq="&intseq)%>"><%=strId%></a></td>
		<td><a href="?<%=getstring("member4=memberview&intseq="&intseq)%>"><%=strName%></a><% if oBprint("str_memVer") = "410" then %>(<a href="?<%=getstring("member4=memberview&intseq="&intseq)%>"><%=strNic%></a><a href="?<%=getstring("member4=memberview&intseq="&intseq)%>">)</a><% end if %></td>
		<td><%=stremail%></td>
		<td><%=left(dtmInsertDate,10)%></td>
		<td><%=intAction%></td>
		<td>Level.<%=intGubun%></td>
		<td><a href="?<%=getstring("member4=memberdel&intseq="&intseq)%>" onClick="return really();">삭제</a></td>
	</tr>
<%
	intNowNum = intNowNum - 1
	rs.movenext
	loop
	end if
	
	rs.close
	set rs = nothing
%>
</table>


<!--<iframe width="0" height="0" frameborder="1" src="about:blank" name="memdel"></iframe>-->
<%call Paging_admin("")%>


<SCRIPT Language="Javascript">
<!--
var NS = 1;
if( document.all) NS = 0;

function PrintOption(type1,type2){
//네스케이프일 경우
		if (NS) {
				window.print();
//익스플로러일 경우
		} else {
				var active = '<OBJECT ID="active1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
				document.body.insertAdjacentHTML('beforeEnd', active);
				active1.ExecWB(type1,type2);
				active1.outerHTML ="";
		}
}
//-->
</script>
<% call dbclose %>
<br>



<table class="tableList" cellpadding="0" cellspacing="0">
    <colgroup>    
        <col style="width:10%">
        <col style="width:50%">
    </colgroup>
    <tr>
        <th rowspan="2">사용자ID 즉시등록</th>
        <td class="tleft pl20 ">
            <form name = "joinform" action = "?<%=getstring("member4=joinok")%>" method = "post" enctype="multipart/form-data" onSubmit="return checkjoin(this)">
            	회원명
                <input name="strname" type="text" size="15" maxlength="20" class="input01" />
                &nbsp;&nbsp;&nbsp;ID
                <input name="strId" type="text" size="15" maxlength="20" class="input01" onkeyup="document.joinform.idck.value == 0;" />
                <input type="hidden" name="idck" value="0">
                <img src="/AVANplus/modul/member4/img/btn_idsearch.gif" style="cursor:hand; vertical-align: middle;" onclick="checkid()" /> &nbsp;&nbsp;&nbsp; PW
                <input name="strPwd" type="text" size="15" maxlength="20" class="input01"/>
            </form>
		</td>
	</tr>
	<tr>
		<td class="tleft pl20 ">   
        	<form name = "joinform" action = "?<%=getstring("member4=joinok")%>" method = "post" enctype="multipart/form-data" onSubmit="return checkjoin(this)">
                이메일</b><span style="padding-top:5px">
                <%Call Form_EmailPrint("strEmail", "")%>
                </span><!--b>주민</b>
                <input name="strJumin1" type="text" size="7" maxlength="6" class="input01" />
                -
                <input name="strJumin2" type="text" size="7" maxlength="7" class="input01" /-->
                <p class="t_red"> * 즉시등록을 하시면 회원가입절차 없이 즉시 로그인이 가능합니다. </p>
        	</form>
      	</td>
    </tr>
</table>

<div  class="tcenter  pt10pb10"><input name="image2" type= "image" src="/AVANplus/modul/member4/img/bt1.gif" /></div>

<script language="javascript">
	<!--
		//영문자 숫자체크
		function check_char(input_str) {
			var alpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			var numeric = '1234567890';
			var nonkorean = alpha+numeric;

			for (var i=0; i < input_str.length; i++ ) {
				if( nonkorean.indexOf(input_str.substring(i,i+1)) < 0) {
					break ;
				}
			}

			if ( i != input_str.length ) {
				return false ;
			} else{
				return true ;
			}
			return true;
		}
		function checkjoin(frm)
		{
			if ( frm.strname.value.replace(/ /gi, "") == "" ) { alert("회원명을 입력해주세요"); frm.strname.focus(); return false; }
			if ( frm.strId.value.replace(/ /gi, "") == "" ) { alert("아이디를 입력해주세요"); frm.strId.focus(); return false; }
			if ((frm.strId.value.length < 4) || (frm.strId.value.length > 12)) { alert("아이디를 4~12자 미만으로 입력해주세요."); frm.strId.focus(); return false; }
			if ( frm.strPwd.value.replace(/ /gi, "") == "" ) { alert("패스워드를 입력해주세요"); frm.strPwd.focus(); return false; }
			if ( (frm.strPwd.value.length < 4) || (frm.strPwd.value.length > 12)) { alert("패스워드를 4~12자 미만으로 입력해주세요."); frm.strPwd.focus(); return false; }
			if (!check_char(frm.strPwd.value)){alert("비밀번호는 영문자,숫자만 사용하실수 있습니다.");frm.strPwd.focus();return false;}
			if ( frm.strPwd.value != frm.strPwd2.value ) { alert("비밀번호가 일치하지 않습니다..\비밀번호를 다시 입력해주세요."); frm.strPwd2.focus(); return false; }
			//if ( frm.strZip.value.replace(/ /gi, "") == "" ) { alert("우편번호 찾기를 클릭해주세요"); frm.strZip.focus(); return false; }
			//if ( frm.strAddr2.value.replace(/ /gi, "") == "" ) { alert("세부주소를 입력해주세요"); frm.strAddr2.focus(); return false; }
			//if ( frm.strPhone.value.replace(/ /gi, "") == "" ) { alert("전화번호를 입력해주세요"); frm.strPhone1.focus(); return false; }
			//if ( frm.strMobil.value.replace(/ /gi, "") == "" ) { alert("핸드폰번호를 입력해주세요"); frm.strMobil1.focus(); return false; }
			if ( frm.strEmail1.value.replace(/ /gi, "") == "" ) { alert("이메일을 입력해주세요"); frm.strEmail1.focus(); return false; }
			//if ( frm.strJob.value.replace(/ /gi, "") == "" ) { alert("직업을 입력해주세요"); frm.strJob.focus(); return false; }
			if ( frm.idck.value != 1) { alert("아이디 중복체크를 해주세요."); return false; }
		}
		function checkid()
		{
			if ( (document.joinform.strId.value.length < 4) || (document.joinform.strId.value.length > 12)) { alert("아이디를 4~12자 미만으로 입력해주세요.");document.joinform.strId.focus(); return false; }
			else {
			var newwindow="toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=324,height=197";
			window.open('/AVANplus/Modul/member4/checkId.asp?checkid='+document.joinform.strId.value,'checkid',newwindow);}
		}
		function searchpost()
		{
			var newwindow="toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=344,height=230";
			window.open('/AVANplus/Modul/member4/post/searchpost.asp','post',newwindow);
		}
	//-->
</script>