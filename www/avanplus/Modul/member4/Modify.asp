<!--#include file= "config.asp"-->

<%
dim strNic,strSmsok,strmailok,strHi
If session("userid")  = "" then
	Call jsAlertUrl(BackGetString("member4=login"))
End If
Call DbOpen()

	if oBprint("str_memVer") = "410" then
		strSQL = "SELECT strName, strJumin1, strJumin2, strId, strPwd, strZip, strAddr1, strAddr2,strPhone, strMobil, strEmail, strJob,strNic,strSmsok,strmailok,strHi FROM "&tablename& " WHERE strId = '"&session("userid")&"'"
	else
		strSQL = "SELECT strName, strJumin1, strJumin2, strId, strPwd, strZip, strAddr1, strAddr2,strPhone, strMobil, strEmail, strJob FROM "&tablename& " WHERE strId = '"&session("userid")&"'"
	end if
arrData = getAdoRsArray(strSQL)

If isArray(arrData) Then


	strName 		= arrData(0,0)
	strJumin1 		= arrData(1,0)
	strJumin2 		= arrData(2,0)
	strId 			= arrData(3,0)
	strPwd 			= arrData(4,0)
	strZip 			= arrData(5,0)
	strAddr1 		= arrData(6,0)
	strAddr2 		= arrData(7,0)
	strPhone 		= arrData(8,0)
	strMobil		= arrData(9,0)
	strEmail		= arrData(10,0)
	strJob 			= arrData(11,0)

	if oBprint("str_memVer") = "410" then
	'//4.1버전이면 불러오기가 다름
	strNic			= arrData(12,0)
	strSmsok		= arrData(13,0)
	strMailok		= arrData(14,0)
	strHi			= arrData(15,0)
	end if
else

	response.Write("코드에러")

end if

%>
<form name = "joinform" action = "?<%=getstring("member4=modifyok")%>" method = "post" enctype="multipart/form-data" onSubmit="return checkjoin(this)">
<table class="tableList" cellpadding="0" cellspacing="0">
    <colgroup>
        <col style="width:15%">
        <col style="width:85%">
    </colgroup>

    <tr >
        <th>회원이름</th>
        <td class=" tleft pl20"><input name="strName" type="text" class="w50 input01" value="<%=strName%>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
        <th>회원아이디</th>
        <td class=" tleft pl20"><%=strId%></td>
    </tr>
		<tr>
        <th>비밀번호</th>
        <td class=" tleft pl20"><input name="strPwd" class="input01 w20" type="password" size="15" maxlength="20">
					4~12자 미만의 영문 / 숫자 </td>
    </tr>
		<tr>
        <th>비밀번호확인</th>
        <td class=" tleft pl20"><input name="strPwd2" class="input01 w20" type="password" size="15" maxlength="20">
					4~12자 미만의 영문 / 숫자 </td>
    </tr>
    <tr>
        <th>우편번호</th>
        <td class=" tleft pl20"><input type="text" name="strZip" class="input01 w10"  readonly value = "<%=strZip%>" onclick ="searchpost()"></td>
    </tr>

    <tr>
        <th>주소</th>
        <td class=" tleft pl20"><input type="text" name="strAddr1" class="input01" style="width:300px;" maxlength="200" value = "<%=strAddr1%>">&nbsp;
				<input type="text" name="strAddr2" style="width:300px;" maxlength="200" value = "<%=strAddr2%>"></td>
    </tr>

    <tr>
        <th>전화번호</th>
        <td class=" tleft pl20"><%Call Form_TelephonePrint("strPhone", strPhone)%></td>
    </tr>

    <tr>
        <th>핸드폰</th>
        <td class=" tleft pl20"><%call Form_HandphonePrint("strMobil", strMobil)%>&nbsp;&nbsp;
					※ SMS 수신여부 <% if strsmsok <> "N" then strsmsok = "Y" %>
<input name="strsmsok" type="radio"  value="Y" <% if strsmsok="Y" then response.Write("checked") %>/>Y
<input type="radio" name="strsmsok"  value="N" <% if strsmsok="N" then response.Write("checked") %>/>N
					</td>
    </tr>

    <tr>
        <th>이메일주소</th>
        <td class=" tleft pl20"><%Call Form_EmailPrint("strEmail", strEmail)%>&nbsp;
					※ 메일수신여부 <% if strMailok <> "N" then strMailok = "Y" %>
<input name="strMailok" type="radio"  value="Y" <% if strMailok="Y" then response.Write("checked") %>/>Y
<input type="radio" name="strMailok"  value="N" <% if strMailok="N" then response.Write("checked") %>/>N</td>
    </tr>
</table>
<ul class="btnWrap">
	<li><input type="button" class="btn_m c_blue" value=" 수정 " onclick="this.form.onsubmit();"></li>
</ul>
</form>



<script language="javascript">
	<!--
		function checkjoin(frm)
		{
			if ( frm.strPwd.value.replace(/ /gi, "") == "" ) { alert("패스워드를 입력해주세요"); frm.strPwd.focus(); return false; }
			if ( (frm.strPwd.value.length < 4) || (frm.strPwd.value.length > 12)) { alert("패스워드를 4~12자 미만으로 입력해주세요."); frm.strPwd.focus(); return false; }
			if ( frm.strPwd.value != frm.strPwd2.value ) { alert("패스워드를 잘못입력하셨습니다.\n다시입력해주세요."); frm.strPwd2.focus(); return false; }
			//if ( frm.strZip.value.replace(/ /gi, "") == "" ) { alert("우편번호 찾기를 클릭해주세요"); frm.strZip.focus(); return false; }
			<% if not oBprint("str_mem4onoff_addr2") = "F" then %>
			if ( frm.strAddr2.value.replace(/ /gi, "") == "" ) { alert("세부주소를 입력해주세요"); frm.strAddr2.focus(); return false; }
			<% end if %>
			if ( frm.strPhone.value.replace(/ /gi, "") == "" ) { alert("전화번호를 입력해주세요"); frm.strPhone1.focus(); return false; }
			if ( frm.strMobil.value.replace(/ /gi, "") == "" ) { alert("핸드폰번호를 입력해주세요"); frm.strMobil1.focus(); return false; }
			if ( frm.strEmail1.value.replace(/ /gi, "") == "" ) { alert("이메일을 입력해주세요"); frm.strEmail1.focus(); return false; }
			<%' if oBprint("str_mem4onoff_job") = "T" then %>
			//if ( frm.strJob.value.replace(/ /gi, "") == "" ) { alert("직업을 입력해주세요"); frm.strJob.focus(); return false; }
			<%' end if %>
			frm.submit();
		}
		function searchpost()
		{
			var newwindow="toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=344,height=230";
			window.open('/AVANplus/Modul/member4/post/searchpost.asp','post',newwindow);
		}
	//-->
</script>
<%
Call DbClose()
%>
