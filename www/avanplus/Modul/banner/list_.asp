<!--#include file = "_config.asp"-->

<%
	
	
	'//검색처리부분
	'//일반과 검색을 위한 설정
	dim where, keyword, keyword_option

	keyword 		= requestS("keyword")
	keyword_option 	= requestS("keyword_option")
	
	'//폼으로 넘어온경우 백로가면 검색화면 페이지표시할수 없음처리
	if len(request.Form("keyword")) > 0 then response.Redirect("?"&GetString("keyword="&keyword&"&keyword_option="&keyword_option))
	

	'//검색일경우 첫페이지로 돌리기 위한 설정
	if len(keyword_option) > 0 then
		where = " intSeq > 0 and "& keyword_option &" like '%"& keyword &"%' "
	else
		where  = " intSeq > 0 "
	end if
	
	
	'//카테고리 값이 넘어오는 경우 처리
	if len(requestQ("mcate")) > 0 then	where = where & " and strCategory = '"& requestQ("mcate") &"' "
	
	
	
	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'## 
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'## 
	dim intTotalCount, intTotalPage
	
	dim intNowPage			: intNowPage 		= Request.QueryString("page")    
    dim intPageSize			: intPageSize 		= 20
    dim intBlockPage		: intBlockPage 		= 10

	dim query_filde			: query_filde		= " Distinct intseq, strcategory "
	dim query_Tablename		: query_Tablename	= Tablename
	dim query_where			: query_where		= where
	dim query_orderby		: query_orderby		= " order by intSeq DESC "
	
	'//카테고리 값이 넘어오면 정렬은 정렬순서기준
	if len(requestQ("mcate")) > 0 then	query_orderby = " order by strsun asc, intSeq asc "
	
	call intTotal
	
	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'## 
	'## 
	'#############################################################################

	dim sql, rs
	sql = GetQuery()
	response.Write(sql)
	call dbopen
	set rs = dbconn.execute(sql)
%>

<table width="100%" border="1" cellpadding="10" class="basic">
  <form action="?<%=getstring(Callpage("insert",""))%>" method="post" name="frmRequestForm" id="frmRequestForm" style="margin:0px;" >
  <tr>
    <td><b>새 베너그룹ID</b> 등록 <input type="text" name="strCategory" maxlength="50" style="width:100;" /> 베너설명 <input type="text" name="strTitle" maxlength="50" style="width:200;" /><input type="submit" value="등록" /></td>
  </tr>
  </form>
</table>
<br />
<table width="100%" height="40" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><%=NavCount%> [<a href="?<%=getString("mcate=&intseq=")%>"><b>전체베너</b></a>] </td>
    <form id="searchForm" name="searchForm" method="post" action="?<%=getString("page=1")%>">
	<td align="right">
        <% Call Form_SelectPrint("keyword_option",keyword_option, "", "strCategory,strTitle", "베너그룹ID,베너제목", ",", "")%>
        <input name="keyword" type="text" id="keyword" value="<%=keyword%>" />
        <input type="submit" name="Search" value="검색" />
     </td>
    </form>
  </tr>
</table>
<table width="100%" border="1" class="basic">
  <%
	dim tempFileExe
	dim pagei : pagei = (intTotalCount-MoveCount)
	'// 글이 없을 경우
	if  rs.eof then
	%>
  <%

	Else		

	%>
  <tr class="basic">
    <td width="80" align="center">베너그룹ID</td>
    <td width="50" align="center">정렬순</td>
    <td width="300" align="center">이미지</td>
    <td align="center" >베너위치설명</td>
    <td align="center" width="50">수정</td>
  </tr>
  <%	
		rs.move MoveCount 
		Do while not rs.eof												
			intseq			= rs("intseq")
			strCategory 	= rs("strCategory")
			strTitle		= rs("strTitle")
			dtmInsertDate	= rs("dtmInsertDate")
			strImage		= rs("strImage")
			strContent		= rs("strContent")
			strName			= rs("strName")
			strsun			= rs("strsun")
			'intSeq, strCategory, strTitle, dtmInsertDate, strImage, strContent

	%>
  <tr>
    <td width="80" align="center" bgcolor="#FFFFFF"><a href="?<%=getString("mcate="&strCategory&"&page=1&keyword=&keyword_option=")%>">
      <%=strCategory%>
    </a></td>
    <td width="50" align="center" bgcolor="#FFFFFF"><a href="?<%=getString(Callpage("list","intseq="&intseq))%>"><%=strsun%></a></td>
    <td width="300" bgcolor="#FFFFFF">
	<%
			
			if rs("strRadio") = "e" and len(rs("strurl")) > 0 then	'//파일읽기이면
				dim objScrFso
				Set objScrFso = Server.CreateObject("SCRIPTING.FileSystemObject")
					If objScrFso.FileExists(Server.MapPath(rs("strurl"))) Then 
						'Server.Execute(rs("strurl"))
						Response.Write "'"& rs("strurl") &"'파일을 읽어옵니다."
					Else
						Response.Write "경로가 틀립니다. 다시한번 확인해주세요."
					End If
				Set objScrFso = Nothing
			elseif rs("strRadio") = "t" then	'//텍스트면
				response.Write rs("strcontent")
			elseif rs("strRadio") = "b" then	'//공란이면
				response.Write "베너 사이에 높이 "&rs("strH")&"px만큼 빈칸을 만듭니다."
			elseif rs("strRadio") = "i" then
			
				if len(rs("strimage")) > 0 then	

					tempFileExe = rs("strimage")
					tempFileExe = split(tempFileExe,":")(0)
					tempFileExe = split(tempFileExe,".")(1)
					tempFileExe = lcase(tempFileExe)
					'response.Write tempFileExe
					if tempFileExe = "swf" then	'//플래시이면
					%>
					<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="<%=rs("strW")%>" height="<%=rs("strH")%>">
					  <param name="flash_component" value="ImageViewer.swc" />
					  <param name="movie" value="/upload/banner/<%=split(rs("strimage"),":")(0)%>" />
					  <param name="quality" value="high" />
					  <param name="FlashVars" value="flashlet={imageLinkTarget:'_blank',captionFont:'Verdana',titleFont:'Verdana',showControls:true,frameShow:false,slideDelay:5,captionSize:10,captionColor:#333333,titleSize:10,transitionsType:'Random',titleColor:#333333,slideAutoPlay:false,imageURLs:['img1.jpg','img2.jpg','img3.jpg'],slideLoop:false,frameThickness:2,imageLinks:['http://macromedia.com/','http://macromedia.com/','http://macromedia.com/'],frameColor:#333333,bgColor:#FFFFFF,imageCaptions:[]}" />
					  <embed src="/upload/banner/<%=split(rs("strimage"),":")(0)%>" quality="high" flashvars="flashlet={imageLinkTarget:'_blank',captionFont:'Verdana',titleFont:'Verdana',showControls:true,frameShow:false,slideDelay:5,captionSize:10,captionColor:#333333,titleSize:10,transitionsType:'Random',titleColor:#333333,slideAutoPlay:false,imageURLs:['img1.jpg','img2.jpg','img3.jpg'],slideLoop:false,frameThickness:2,imageLinks:['http://macromedia.com/','http://macromedia.com/','http://macromedia.com/'],frameColor:#333333,bgColor:#FFFFFF,imageCaptions:[]}" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="<%=rs("strW")%>" height="<%=rs("strH")%>">  </embed>
					</object>
					<%
					else
						'if len(rs("strurl")) > 0 then response.Write("<a href="&rs("strurl")&" target='"&rs("strtarget")&"' title="&rs("strurl")&">")
						response.Write("<img src=/upload/banner/"&split(rs("strimage"),":")(0))
						if len(rs("strW")) > 0 then response.Write(" width="&rs("strW")&" ")
						if len(rs("strH")) > 0 then response.Write(" height="&rs("strH")&" ")
						response.Write(" border=0>")
						'if len(rs("strurl")) > 0 then response.Write("</a>")
					end if
				end if
			end if
	%>	</td>
    <td bgcolor="#FFFFFF"><%=strTitle%></td>
    <td width="50" align="center" bgcolor="#FFFFFF" class="basic"><a href="?<%=getString("mcate="&strCategory&"&page=1&keyword=&keyword_option=&intseq="&intseq)%>">수정</a></td>
  </tr>
  <%
			pagei = pagei-1
		rs.movenext
		loop
		rs.close()
		set rs = nothing
	End If


	call DbClose()
	%>
</table>
<table width="100%" height="80" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><%call Paging("&keyword="&keyword&"&keyword_option="&keyword_option)%></td>
    <td align="right"><%if len(requestQ("mcate")) > 0 then%><input name="button" type="button" class="btn" onclick="self.location.href = '?<%=getString(Callpage("insert","intseq=&mcate="&request.QueryString("mcate")))%>';" value="베너추가 " /><%end if%>
    </td>
  </tr>
</table>
<% if len(requestQ("mcate")) > 0 and len(request.QueryString("intseq")) > 0 then server.Execute("/avanplus/modul/banner/form.asp") %> 
[이용방법]<br /> 
베너그룹추가 - 새 베너그룹 아이디를 등록합니다. 필요한 갯수만큼 베너추가를 클릭하시면 베너 행이 추가됩니다.
<br />
베너수정 - 베너그룹ID를 먼저 선택합니다. 그리고 수정할 베너위치설명 또는 번호를 클릭하면 해당 자료를 수정 할 수 있습니다. 