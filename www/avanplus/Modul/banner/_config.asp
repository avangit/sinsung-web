<%@  codepage="65001" language="VBScript" %>
<% option explicit%>
<% response.charset = "utf-8"%>

<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
	
'======================================================
'// 프로그램 설명 //
'	기본 프로그램 작성 템플릿입니다.
'	mcate를 지원합니다.
'	(xxx.asp?mcate=구분값으로 하나의 프로그램으로 여러페이지에서 사용가능)
'	작성 : 이덕권 2009년 6월 28일
'======================================================
	'[1]프로그램 고유ID명 작성
	Const ModuleID = "banner"
'======================================================

	'[2] 폴더경로
	Const ModuleUrl = "/avanplus/modul/banner/"
	call urlNoEXE(ModuleUrl)
	'[3]테이블명
	Const tablename = "BANNER"
	
	'[4]업로드폴더
	Const UploadFolder = "/upload/banner/"

	
	Dim intSeq,strCategory,strTitle,strName,strContent,dtmInsertDate,strUserid,strImage, strW, strH, strURL, strSUN	,strTarget, strradio
	'[5] 추가변수선언
	
	
	
	
	Dim listField
	listField = "intSeq,strCategory,strTitle,strName,strContent,dtmInsertDate,strUserid,strImage, strW, strH, strURL, strSUN, strTarget, strradio"
	'[6]추가필드

	
	'/--------------------------------
	'	디비처리와 연동이되는 부분
	'--------------------------------/
	'[7]필드중 일반 데이터 업로드 필드
	const setDataField 	= "strCategory,strTitle,strName,strContent,dtmInsertDate,strUserid, strW, strH, strURL, strSUN, strTarget, strradio"
	
	'[8]필드중 파일업로드 관련 필드
	const setFileField 	= "strImage"

	'좀더 내려가보면 [10]까지 있음
'=================================================================================


	
	
	
	
	
	
	
	
	Dim strSQL,intLoop,arrData
	
	
		
	Function Callpage(byval pagename, byval pageAdd)
	'				pagename 불러올 페이지명, pageAdd 추가 스트링
		Callpage = ModuleID & "Mode=" & pagename
		if len(pageAdd) > 0 then Callpage = Callpage & "&" & pageAdd
	End Function
	
	
	Sub idxdata(byval intSeq)	'//글수정과 보기에서 함께 쓰는 함수이므로 별도로 뺌.
		If len(intSeq) > 0 then
		
			Call DbOpen()	
				
				
				strSQL = "SELECT "& listField &" FROM "&tablename& " WHERE intSeq = '"&intSeq&"'"				
				'response.write strsql
				
				
				arrData = getAdoRsArray(strSQL)				
				
				If isArray(arrData) Then							
	'=============================================================					
	' 데이터 불러오기
	' [9]아래 주석을 풀고 화면에 나오는 데이터를 붙여넣으세요.
	'	call codeMake_arrData(listField)
	'=============================================================
					intSeq 			= arrData(0,0)
					strCategory 	= arrData(1,0)
					strTitle 		= arrData(2,0)					
					strName 		= arrData(3,0)			
					strContent		= arrData(4,0)				
					dtmInsertDate	= arrData(5,0)
					strUserid		= arrData(6,0)
					
					'// 이미지필드
					strImage		= arrData(7,0)
					strW			= arrData(8,0)
					strH			= arrData(9,0)
					strURL			= arrData(10,0)
					strSUN			= arrData(11,0)
					strTarget		= arrData(12,0)
					strradio		= arrData(13,0)
	'=============================================================
	' [10]위에 풀어두었던 codeMake_arrData() 주석처리하기
	' End	--설정완료--				
	'=============================================================
				else
				
					response.Write("코드에러")
					
				end if
				
			Call DbClose()
		
		End If
	End Sub
		
	
	


%>