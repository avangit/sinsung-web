<!--#include file = "_config.asp"-->
<!--#include file = "inc_function.asp"-->
<%
	
	
	'//검색처리부분
	'//일반과 검색을 위한 설정
	dim where, keyword, keyword_option

	keyword 		= requestS("keyword")
	keyword_option 	= requestS("keyword_option")
	
	'//폼으로 넘어온경우 백로가면 검색화면 페이지표시할수 없음처리
	if len(request.Form("keyword")) > 0 then response.Redirect("?"&GetString("keyword="&keyword&"&keyword_option="&keyword_option))
	

	'//검색일경우 첫페이지로 돌리기 위한 설정
	if len(keyword_option) > 0 then
		where = " intSeq > 0 and "& keyword_option &" like '%"& keyword &"%' "
	else
		where  = " intSeq > 0 "
	end if
	
	
	'//카테고리 값이 넘어오는 경우 처리
	if len(requestQ("mcate")) > 0 then	where = where & " and strCategory = '"& requestQ("mcate") &"' "
	
	
	
	'##==========================================================================
	'##
	'##	페이징 관련 함수 - 게시판등...
	'##
	'##==========================================================================
	'## 
	'## [설정법]
	'## 다음 코드가 상단에 위치 하여야 함
	'## 
	dim intTotalCount, intTotalPage
	
	'dim intNowPage			: intNowPage 		= Request.QueryString("page")    
   ' dim intPageSize			: intPageSize 		= 20
   ' dim intBlockPage		: intBlockPage 		= 10

	'dim query_filde			: query_filde		= " Distinct intseq, strcategory "
	'dim query_Tablename		: query_Tablename	= Tablename
	'dim query_where			: query_where		= where
	'dim query_orderby		: query_orderby		= " order by intSeq DESC "
	
	'//카테고리 값이 넘어오면 정렬은 정렬순서기준
	'if len(requestQ("mcate")) > 0 then	query_orderby = " order by strsun asc, intSeq asc "
	
	'call intTotal
	
	'##
	'## 1. intTotal : call intTotal
	'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
	'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
	'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
	'## 5. Paging(byval plusString) 하단의 네비게이션 바 call Paging(추가스트링)
	'## 
	'## 
	'#############################################################################

	dim sql, rs
	'sql = GetQuery()
	'response.Write(sql)
	call dbopen
	set rs = dbconn.execute("select Distinct strcategory from BANNER order by strcategory ASC")
%>
<%=getstring(Callpage("insert",""))%>
<table class="tableList" cellpadding="0" cellspacing="0">
    <colgroup>    
        <col style="width:15%">
        <col style="width:">
    </colgroup>
    <form action="?<%=getstring(Callpage("insert",""))%>" method="post" name="frmRequestForm" id="frmRequestForm"  >
    <tr>
        <th>새 배너그룹ID</th>
        <td class="tleft pl20"><input type="text" name="strCategory"  class="input01 w40" maxlength="50" /> </td>
    </tr>
    <tr>
        <th>배너설명 </th>
        <td class="tleft pl20"><input type="text" name="strTitle" maxlength="50" class="input01 w40"/> <input type="submit"  class="btnDB btnS" value="등록" /></td>
    </tr>
    </form>
</table>



<br />
<table width="100%" height="40" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><%'=NavCount%> [<a href="?<%=getString("mcate=&intseq=")%>">전체배너</a>] </td>
    <form id="searchForm" name="searchForm" method="post" action="?<%=getString("page=1")%>">
	<td align="right">
        <% Call Form_SelectPrint("keyword_option",keyword_option, "", "strCategory,strTitle", "배너그룹ID,배너제목", ",", "")%>
        <input name="keyword" type="text" id="keyword" class="input01" value="<%=keyword%>" />
        <input type="submit" name="Search" class="btnDB btnS" value="검색" />
     </td>
    </form>
  </tr>
</table>


<table class="tableList center_table" cellpadding="0" cellspacing="0">
    <colgroup>    
        <col style="width:15%">
        <col style="width:">
        <col style="width:8%">
    </colgroup>
  	<%
	dim tempFileExe
	'dim pagei : pagei = (intTotalCount-MoveCount)
	'// 글이 없을 경우
	if  rs.eof then
	%>
  	<%

	Else		

	%>
  <tr class="basic">
    <th>배너그룹ID</th>
    <th>배너미리보기 &lt;%banner(&quot;배너그룹ID&quot;)%&gt;</th>
    <th>편집</th>
  </tr>
  <%	
		'rs.move MoveCount 
		Do while not rs.eof												
			'intseq			= rs("intseq")
			strCategory 	= rs("strCategory")
			'strTitle		= rs("strTitle")
			'dtmInsertDate	= rs("dtmInsertDate")
			'strImage		= rs("strImage")
			'strContent		= rs("strContent")
			'strName			= rs("strName")
			'strsun			= rs("strsun")
			'intSeq, strCategory, strTitle, dtmInsertDate, strImage, strContent

	%>
  <tr>
    <td><a href="?<%=getString("mcate="&strCategory&"&page=1&keyword=&keyword_option=")%>"> <%=strCategory%> </a></td>
    <td><%banner(strCategory)%></td>
    <td><a href="?<%=getString(Callpage("list2","mcate="&strCategory&"&page=1&keyword=&keyword_option=&intseq="&intseq))%>">편집</a></td>
  </tr>
  <%
			'pagei = pagei-1
		rs.movenext
		loop
		rs.close()
		set rs = nothing
	End If


	call DbClose()
	%>
</table>



<div class="tip_box">
	<p>[이용방법]<br>
    배너그룹추가 - 새 배너그룹 아이디를 등록합니다. 필요한 갯수만큼 배너추가를 클릭하시면 배너 행이 추가됩니다.<br>
   배너수정 - 배너그룹ID를 먼저 선택합니다. 그리고 수정할 배너위치설명 또는 번호를 클릭하면 해당 자료를 수정 할 수 있습니다. <br>
</div>
 