<!--#include file="../../_Config.asp"-->

<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<%
		'## [설정법]
		'## 다음 코드가 상단에 위치 하여야 함
		'## dim intNowPage,intPageSize,intBlockPage,intTotalCount,intTotalPage
		'## intNowPage = Request.QueryString("page")    
		'## intPageSize = 10
		'## intBlockPage = 10
		'## call intTotal(Tablename,where)

		'## 1. intTotal(Tablename,where) : call intTotal(Tablename,where)
		'## 2. TopCount 를 불러오는 쿼리문 에 삽입한다.
		'## 3. MoveCount 를 Do while문 상단에 rs.move MoveCount 형식으로 삽입한다.
		'## 4. NavCount 현재페이지의 정보를 보여주는 함수 response.Write(NavCount) 형식으로 삽입
		'## 5. Pasing 하단의 네비게이션 바 call Pasing
		'## 

		DBopen
		dim intNowPage,intPageSize,intBlockPage,intTotalCount,intTotalPage
		intNowPage = Request.QueryString("page")
		intPageSize = 15
		intBlockPage = 10

		
		dim query_filde		: query_filde		= "*"
		dim query_Tablename	: query_Tablename	= "boardset_v2"
		dim query_where		: query_where		= ""
		dim query_orderby		: query_orderby		= ""
		call intTotal
		
		
		dim sql, rs
		sql = "select  " & TopCount & " * from boardset_v2 order by bs_idx desc"
		set rs = dbconn.execute(sql)
	%>
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i> </span>
		<h2>게시판관리</h2>
	</header>
		<div>
			<div class="widget-body no-padding">

				<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
				  <tr>
					<th data-class="expand">아이디</th>
					<th data-class="expand">게시판 이름</th>
					<th>쓰/읽/답/리/첨</th>
					<th data-hide="phone,tablet">관리자ID</th>
					<th data-hide="phone,tablet">게시판용도</th>
					<th data-hide="phone,tablet">그룹핑코드</th>
					<th data-hide="phone,tablet">세부설정</th>
				  </tr>
				  <%
					if not rs.eof then
					rs.move MoveCount
					Do while not rs.eof
				  %>
				  <tr>
					<td class="tcenter"><%=rs("bs_code")%></td>
					<td class="tcenter"><font color="#006699"><a href="/avanplus/modul/avanboard_v3/call2.asp?<%=getString("bs_code="&rs("bs_code")&"&vmode=")%>" target="_blank"><b><%=rs("bs_name")%></b></a></font></td>
					<td class="tcenter"><%=rs("bs_writeLaw")&" / "&rs("bs_readLaw")&" / "&left(rs("bs_rewrite"),1)&" / "&left(rs("bs_reply"),1)&" / "&left(rs("bs_file"),1)%></td>
					<td class="tcenter"><%=rs("bs_admin")%></td>
					<td class="tcenter"><%=rs("bs_use")%></td>
					<td class="tcenter"><%=rs("bs_grouping")%></td>
					<td class="tcenter"><a href='?<%=getString("vmode=modify&bs_code="&rs("bs_code"))%>'>세부설정</a></td>
				  </tr>
				  
				  <%
					rs.movenext
					loop
					end if
					
					rs.close
					set rs = nothing
					DBclose
				  %>
				</table>
				<%call paging("")%>
				<footer style="float:right;" class="m_button">
					<button type="submit" class="btn btn-primary" onclick="self.location.href = '?<%=getString("vmode=boardadd")%>';">신규게시판등록</button>
				</footer>
			</div>
		</div>

</div>