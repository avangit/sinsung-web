<!-- include virtual ="/avanplus/inc/incCodepage.asp" -->
<!--#include file="../../_Config.asp"-->
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i> </span>
		<h2>신규 게시판 등록</h2>
	</header>
	<div>
		<form action="?<%=getString("vmode=boardadd_ok")%>" method="post"  class="smart-form"enctype="multipart/form-data" name="insert" >
		<div class="widget-body no-padding">
			<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
				<colgroup>
					<col style="width:15%">
					<col style="width:85%">
				</colgroup>
				<tr>
					<th>게시판 아이디</th>
					<td class="pl20">
						<label for="bs_code" class="input col-2 ib" >
							<input type="text" name="bs_code" id="bs_code" value=""  maxlength="20">
						</label>
						영문자20자 이내로 <font color="#FF0000">* 세팅후 변경 불가 </font>
					</td>
				</tr>
				<tr>
					<th>게시판 이름</th>
					<td class="pl20">
					<label for="bs_name" class="input col-2" >
						<input type="text" name="bs_name" id="bs_name" value="">
					</label>
					</td>
				</tr>
				<tr>
					<th>관리 패스워드</th>
					<td class="pl20">
						<b>게시판관리자</b>
						<label for="bs_admin" class="input col-1 ib" >
							<input type="text" name="bs_admin" id="bs_admin" >
						</label>
						&nbsp;&nbsp;&nbsp;
						<b>관리용비밀번호</b>
						<label for="bs_pw" class="input col-1 ib" >
						  <input type="password" name="bs_pw" id="bs_pw" value="avansoft" maxlength="20">
					    </label>
					</td>
				</tr>
				<tr>
					<th>게시판 안내글</th>
					<td class="pl20"><textarea name="bs_message" rows="5" id="bs_message" style="width:100% "></textarea></td>
				</tr>
				<tr>
					<th>게시판 구분</th>
				  <td class="pl20">
						<label class="radio ib">
							<input type="radio" name="bs_use" value="simple" >
							<i></i>심플게시판</label>
						<label class="radio ib">
							<input type="radio" name="bs_use" value="board" checked >
							<i></i>일반게시판</label>
						<label class="radio ib">
							<input type="radio" name="bs_use" value="pic" >
							<i></i>사진게시판</label>
						<label class="radio ib">
							<input type="radio" name="bs_use" value="gallery"  >
							<i></i>사진갤러리</label>
						<label class="radio ib">
							<input type="radio" name="bs_use" value="video" >
							<i></i>동영상게시판</label>
					</td>
				</tr>
				<tr>
				  <th>게시물 제목길이</th>
				  <td class="pl20">
					<label for="bs_titlelen" class="input col-1 ib" >
						<input type="text" name="bs_titlelen" id="bs_titlelen" value="50">
					</label>byte
				  </td>
				</tr>
				<tr>
				  <th>페이지당 목록수</th>
				  <td class="pl20">
					<label for="bs_pagerow" class="input col-1 ib" >
						<input type="text" name="bs_pagerow" id="bs_pagerow" value="10">
					</label>개
				  </td>
				</tr>
				<tr>
				  <th>페이지 이동단위</th>
				  <td class="pl20">
					<label for="bs_pageblock" class="input col-1 ib" >
						<input type="text" name="bs_pageblock" id="bs_pageblock"value="10">
					</label>개
				  </td>
				</tr>
				<tr>
				  <th>쓰기권한</th>
				  <td class="pl20">
					<label class="select state-disabled col-1 ib">
					<select name = "bs_writeLaw"  class="input-sm">
						<option value = "0">0</option>
						<option value = "1">1</option>
						<option value = "2">2</option>
						<option value = "3">3</option>
						<option value = "4">4</option>
						<option value = "5">5</option>
						<option value = "6">6</option>
						<option value = "7">7</option>
						<option value = "8">8</option>
						<option value = "9" selected>9</option>
					</select><i></i> 
					</label>
					Level 0 : 비회원</td>
				</tr>
				<tr>
				  <th>읽기권한</th>
				  <td class="pl20">
					<label class="select state-disabled col-1 ib">
						<select name = "bs_readLaw"  class="input-sm">
							<option value = "0">0</option>
							<option value = "1">1</option>
							<option value = "2">2</option>
							<option value = "3">3</option>
							<option value = "4">4</option>
							<option value = "5">5</option>
							<option value = "6">6</option>
							<option value = "7">7</option>
							<option value = "8">8</option>
							<option value = "9" selected>9</option>
						</select><i></i> 
					</label>
				   &nbsp; [ <b>비밀글기능</b>
					<label class="radio ib">
						<input type="radio" name="bs_bimilLaw" value="1" >
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_bimilLaw" value="0" checked >
						<i></i>사용안함</label>
					]</td>
				</tr>
				<tr>
				  <th>답글쓰기</th>
				  <td class="pl20">
				    <label class="radio ib">
						<input type="radio" name="bs_rewrite" value="1" >
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_rewrite" value="0" checked>
						<i></i>사용안함</label>
					&nbsp;&nbsp; [<b> 권한</b>:
					<label class="select state-disabled col-1 ib">
						<select name = "bs_rewriteLaw" class="input-sm">
							<option value = "0">0</option>
							<option value = "1">1</option>
							<option value = "2">2</option>
							<option value = "3">3</option>
							<option value = "4">4</option>
							<option value = "5">5</option>
							<option value = "6">6</option>
							<option value = "7">7</option>
							<option value = "8">8</option>
							<option value = "9" selected>9</option>
						</select> <i></i>
					</label>]
					</td>
				</tr>
				<tr>
				  <th>리플기능</th>
				  <td class="pl20">
					<label class="radio ib">
						<input type="radio" name="bs_reply" value="1" >
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_reply" value="0"  checked>
						<i></i>사용안함</label>
					 &nbsp;&nbsp; [<font color="#0000FF"> 회원만 사용가능</font> ] </td>
				</tr>


				<tr>
				  <th>추천기능</th>
				  <td class="pl20">
				  
					<label class="radio ib">
						<input type="radio" name="recommend" value="1" >
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="recommend" value="0" checked>
						<i></i>사용안함</label>
					
					&nbsp;&nbsp; [<font color="#0000FF"> 회원만 사용가능</font> ] </td>
				</tr>

				<tr>
				  <th>파일첨부</th>
				  <td class="pl20">
					
					<label class="radio ib">
						<input type="radio" name="bs_file" value="1" >
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_file" value="0" checked>
						<i></i>사용안함</label>
					
					&nbsp;&nbsp; [ <b>업로드 용량제한</b>
					<label for="bs_filesize" class="input col-1 ib" >
						<input type="text" name="bs_filesize" id="bs_filesize"value="10">
					</label>
					MB ]
					 </td>
				</tr>
				<tr>
				  <th>IP표시</th>
				  <td class="pl20">
				  	<label class="radio ib">
						<input type="radio" name="bs_ipview" value="1" >
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_ipview" value="0" checked>
						<i></i>사용안함</label>
				  
				  
				</tr>
				<tr>
				  <th>게시판 색지정 </th>
				  <td class="pl20">
				  <label for="bs_color" class="input col-1 ib" >
						<input type="text" name="bs_color" id="bs_color">
					</label>
				  </td>
				</tr>
				<tr>
				  <th>타이틀 이미지</th>
				  <td class="pl20">
					<label class="radio ib">
						<input type="radio" name="bs_titleimg" value="1" >
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_titleimg" value="0" checked>
						<i></i>사용안함</label>

						<div class="input input-file w20 ib">
							<span class="button"><input type="file" id="file" name="file_titleimg" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="" >
						</div>
				  (jpg,gif만 가능)</td>
				</tr>
				<tr>
				  <th>게시글 그룹핑</td>
				  <td>
				    <label for="bs_grouping" class="input col-1 ib" >
						<input type="text" name="bs_grouping" id="bs_grouping" maxlength="10">
					</label>
				  
				  10자 이내 </td>
				</tr>
			</table>

			
			<footer style="float:right;">
				<input type="submit" class="btn btn-primary" onclick="this.form.onsubmit();" value="신규게시판 등록">
				<input type="button" class="btn btn-default"  onclick="window.history.back();" value="리스트">
			</footer>
			</form>
		</div>
	</div>
</div>