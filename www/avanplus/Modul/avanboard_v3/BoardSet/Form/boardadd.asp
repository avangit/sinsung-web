<!-- include virtual ="/avanplus/inc/incCodepage.asp" -->
<!--#include file="../../_Config.asp"-->
				<p class="main_nav">홈페이지환경설정 > 게시판관리</p>
				<h2 class="main_tit">게시판관리</h2>
				<div class="admin">
<form action="?<%=getString("vmode=boardadd_ok")%>" method="post"  class="smart-form"enctype="multipart/form-data" name="insert" >
						<fieldset>
							<legend>신규 게시판 등록</legend>
							<table class="table">
								<caption>신규 게시판 등록</caption>
								<colgroup>
									<col style="width:260px">
									<col style="">
								</colgroup>
								<tbody>
									<tr>
										<th>게시판 아이디</th>
										<td class="left">
											<input type="text" name="bs_code" id="bs_code" value=""  maxlength="20" class="w20" /> 영문자20자 이내로 <span class="font_red">* 세팅후 변경 불가</span>
										</td>
									</tr>
									<tr>
										<th>게시판 이름</th>
										<td class="left">
											<input type="text" name="bs_name" id="bs_name" value="" class="w20" />
										</td>
									</tr>
									<tr>
										<th>관리 패스워드</th>
										<td class="left">
											<b>게시판관리자</b> <input type="password" name="bs_admin" id="bs_admin" maxlength="20" class="w10 mr_10" />
											<b>관리용비밀번호</b> <input type="password" name="bs_pw" id="bs_pw" maxlength="20" class="w10" />
										</td>
									</tr>
									<tr>
										<th>게시판 안내글</th>
										<td class="left">
											<textarea name="bs_message" rows="5" id="bs_message" class="w100 h200"></textarea>
										</td>
									</tr>
									<tr>
										<th>게시판 구분</th>
										<td class="left">
											<label><input type="radio" name="bs_use" value="simple" /> 심플게시판</label>
											<label><input type="radio" name="bs_use" value="board" checked /> 일반게시판</label>
											<label><input type="radio" name="bs_use" value="pic" /> 사진게시판</label>
											<label><input type="radio" name="bs_use" value="gallery" /> 사진갤러리</label>
											<label><input type="radio" name="bs_use" value="video" /> 동영상게시판</label>
										</td>
									</tr>
									<tr>
										<th>게시물 제목길이</th>
										<td class="left">
											<input type="text" name="bs_titlelen" id="bs_titlelen" value="50" class="w10"/> byte
										</td>
									</tr>
									<tr>
										<th>페이지당 목록수</th>
										<td class="left">
											<input type="text" name="bs_pagerow" id="bs_pagerow" value="10" class="w10"/> 개
										</td>
									</tr>
									<tr>
										<th>페이지 이동단위</th>
										<td class="left">
											<input type="text" name="bs_pageblock" id="bs_pageblock"value="10" class="w10"/> 개
										</td>
									</tr>
									<tr>
										<th>쓰기권한</th>
										<td class="left">
											<select name="bs_writeLaw" >
												<option value = "0">0</option>
												<option value = "1">1</option>
												<option value = "2">2</option>
												<option value = "3">3</option>
												<option value = "4">4</option>
												<option value = "5">5</option>
												<option value = "6">6</option>
												<option value = "7">7</option>
												<option value = "8">8</option>
												<option value = "9" selected>9</option>
											</select> Level 0 : 비회원
										</td>
									</tr>
									<tr>
										<th>읽기권한</th>
										<td class="left">
											<select name="bs_readLaw">
												<option value = "0">0</option>
												<option value = "1">1</option>
												<option value = "2">2</option>
												<option value = "3">3</option>
												<option value = "4">4</option>
												<option value = "5">5</option>
												<option value = "6">6</option>
												<option value = "7">7</option>
												<option value = "8">8</option>
												<option value = "9" selected>9</option>
											</select>
											[<b>비밀글기능</b> <label><input type="radio" name="bs_bimilLaw" value="1" /> 사용</label> <label><input type="radio" name="bs_bimilLaw" value="0" checked /> 사용안함</label>]
										</td>
									</tr>
									<tr>
										<th>답글쓰기</th>
										<td class="left">
											<label><input type="radio" name="bs_rewrite" value="1" /> 사용</label>
											<label><input type="radio" name="bs_rewrite" value="0" checked /> 사용안함</label>
											[<b>권한</b> <select name="bs_rewriteLaw">
												<option value = "0">0</option>
												<option value = "1">1</option>
												<option value = "2">2</option>
												<option value = "3">3</option>
												<option value = "4">4</option>
												<option value = "5">5</option>
												<option value = "6">6</option>
												<option value = "7">7</option>
												<option value = "8">8</option>
												<option value = "9" selected>9</option>
											</select>]
										</td>
									</tr>
									<tr>
										<th>리플기능</th>
										<td class="left">
											<label><input type="radio" name="bs_reply" value="1" /> 사용</label>
											<label><input type="radio" name="bs_reply" value="0"  checked /> 사용안함</label>
											[<span class="font_blue">회원만 사용가능</span>]
										</td>
									</tr>
									<tr>
										<th>추천기능</th>
										<td class="left">
											<label><input type="radio" name="recommend" value="1" /> 사용</label>
											<label><input type="radio" name="recommend" value="0" checked /> 사용안함</label>
											[<span class="font_blue">회원만 사용가능</span>]
										</td>
									</tr>
									<tr>
										<th>파일첨부</th>
										<td class="left">
											<label><input type="radio" name="bs_file" value="1" /> 사용</label>
											<label><input type="radio" name="bs_file" value="0" checked /> 사용안함</label>
											[<b>업로드 용량제한</b><input type="text" name="bs_filesize" id="bs_filesize"value="10" class="w10" /> MB]
										</td>
									</tr>
									<tr>
										<th>IP표시</th>
										<td class="left">
											<label><input type="radio" name="bs_ipview" value="1" /> 사용</label>
											<label><input type="radio" name="bs_ipview" value="0" checked /> 사용안함</label>
										</td>
									</tr>
									<tr>
										<th>게시판 색지정</th>
										<td class="left">
											<input type="text" name="bs_color" id="bs_color" class="w10" />
										</td>
									</tr>
									<tr>
										<th>타이틀 이미지</th>
										<td class="left">
											<label><input type="radio" name="bs_titleimg" value="1" /> 사용</label>
											<label><input type="radio" name="bs_titleimg" value="0" checked /> 사용안함</label>
											<input type="file" name="file_titleimg" /> (jpg,gif만 가능)
										</td>
									</tr>
									<tr>
										<th>게시글 그룹핑</th>
										<td class="left">
											<input type="text" class="w10" name="bs_grouping" id="bs_grouping" maxlength="10" /> 10자 이내
										</td>
									</tr>
								</tbody>
							</table>
							<div class="f_right mt_10">
								<button type="button" class="btn_list" onclick="window.history.back();" value="리스트">리스트</button>
								<button type="submit" class="btn_save" value="신규게시판 등록">신규게시판 등록</button>
							</div>
						</fieldset>
</form>
		</div>