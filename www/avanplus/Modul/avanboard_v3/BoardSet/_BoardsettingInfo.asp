<%

	dim adminid
	adminid = "admin"



	'// 각 모듈에서 보드세팅 값을 불러 쓰는 파일
	'// bs_code 값이 넘어와야함



	dim sql_info, rs_info, bs_code
	bs_code = request.QueryString("bs_code")

	if len(bs_code) = 0 then
		response.Write("게시물 모듈구분값이 없습니다. <br>본 페이지 링크를 거실경우 'xxx.asp?bs_code=게시판ID' 형식으로 게시판ID를 지정해 주셔야 합니다. ")
		response.Write("<br>빌더에서 등록하신 경우는 게시판 관리자페이지에서 게시판 아이디를 확인하셔서 관리자모드에 모듈구분값에 'bs_code=아이디' 형식으로 값을 넣어주세요.")
		response.end
	else

		DBopen
		set rs_info = DBconn.execute("select * from boardset_v2 where bs_code='"& bs_code &"'")

		dim bs_name,bs_admin,bs_pw,bs_message,bs_use,bs_titleLen,bs_pageRow,bs_pageBlock,bs_writeLaw,bs_readLaw,bs_bimilLaw,bs_rewrite,bs_rewriteLaw,bs_reply,bs_replyLaw,bs_file,bs_filesize,bs_badFiletype,bs_ipview,bs_jumin,bs_color,bs_badWord,bs_badip,bs_titleimg,file_titleimg,bs_grouping,bs_fileLaw, recommend

		if rs_info.eof then
			response.Write("게시판 아이디(bs_code)값이 잘못되었습니다.")
			response.End()
		else
			bs_code			= rs_info("bs_code")			'// 게시판아이디
			bs_name			= rs_info("bs_name")			'// 게시판명
			bs_admin		= rs_info("bs_admin")			'// 게시판 관리자 아이디
			bs_pw			= rs_info("bs_pw")				'// 게시판 관리용비번
			bs_message		= rs_info("bs_message")			'// 게시판안내글
			bs_use			= rs_info("bs_use")				'// 게시판 구분(자료실/게시판/사진첩/공지사항)
			bs_titleLen		= rs_info("bs_titleLen")		'// 게시물 길이
			bs_pageRow		= rs_info("bs_pageRow")			'// 페이지당 목록수
			bs_pageBlock	= rs_info("bs_pageBlock")		'// 페이지 이동단위
			bs_writeLaw		= rs_info("bs_writeLaw")		'// 쓰기권한
			bs_readLaw		= rs_info("bs_readLaw")			'// 읽기권한
			bs_bimilLaw		= rs_info("bs_bimilLaw")		'// 비밀글 기능
			bs_rewrite		= rs_info("bs_rewrite")			'// 답글기능
			bs_rewriteLaw	= rs_info("bs_rewriteLaw")		'// 답글권한
			bs_reply		= rs_info("bs_reply")			'// 리플기능
			bs_replyLaw		= rs_info("bs_replyLaw")		'// 리플권한
			bs_file			= rs_info("bs_file")			'// 파일첨부
			bs_filesize		= rs_info("bs_filesize")		'// 파일용량
			bs_badFiletype	= rs_info("bs_badFiletype")		'// 업로드금지파일명
			bs_ipview		= rs_info("bs_ipview")			'// IP표시
			bs_jumin		= rs_info("bs_jumin")			'// 주민인증
			bs_color		= rs_info("bs_color")			'// 게시판 칼라
			bs_badWord		= rs_info("bs_badWord")			'// 불량단어
			bs_badip		= rs_info("bs_badip")			'// 불량아이피
			bs_titleimg		= rs_info("bs_titleimg")		'// 타이틀이미지사용
			file_titleimg	= rs_info("file_titleimg")		'// 이미지명
			bs_grouping		= rs_info("bs_grouping")		'// 게시글그룹핑코드(board_code)
			recommend		= rs_info("recommend")		'// 게시글그룹핑코드(board_code)
		End if


		rs_info.close
		set rs_info = nothing
		DBclose

	end if

	'//썬이 주석
	'if session("userid") <> "" then
		'if bs_admin = session("userid") then
		'	session("userlevel") = "9"
		'else
			'session("userlevel") = "1"
		'end if
	'else

	'end if

	'if trim(session("userid")) = "" then
	'	session("userlevel") = "0"
	'End If
	'//썬이 주석


	dim Img_write, Img_list, Img_entry, Img_amend, Img_back, Img_cancel, Img_delete, Img_first, Img_reply, Img_search, Img_re, Img_new, Img_key, Img_dot, Img_point, Img_ok
	Img_write		= "/avanplus/img/avanboard/b_write.gif"
	Img_list		= "/avanplus/img/avanboard/b_list.gif"
	Img_entry		= "/avanplus/img/avanboard/b_entry.gif"
	Img_amend		= "/avanplus/img/avanboard/b_amend.gif"
	Img_back		= "/avanplus/img/avanboard/b_back.gif"
	Img_cancel		= "/avanplus/img/avanboard/b_cancel.gif"
	Img_delete		= "/avanplus/img/avanboard/b_delete.gif"
	Img_first		= "/avanplus/img/avanboard/b_first.gif"
	Img_reply		= "/avanplus/img/avanboard/b_reply.gif"
	Img_search		= "/avanplus/img/avanboard/b_search.gif"
	Img_re			= "/avanplus/img/avanboard/b_re.gif"
	Img_new			= "/avanplus/img/avanboard/b_new.gif"
	Img_key			= "/avanplus/img/avanboard/b_key.gif"
	Img_dot			= "/avanplus/img/avanboard/b_dot.gif"
	Img_point		= "/avanplus/img/avanboard/b_prepoint.gif"
	Img_ok			= "/avanplus/img/avanboard/ok.gif"




	'//**********************************
	'	권한의 체크
	'	사용자 권한과 해당 권한값 체크함수
	'***********************************//
	Function Authority(byval row)
	'response.Write (session("userlevel")&" | "&int(row))&" | "

	'response.write session("userid")
	'//session("userlevel") : 로그인한 사람의 레벨
	'//int(row)				: 게시판 권한 레벨
	'response.write int(session("userlevel"))

	if int(session("aduserlevel")) >= int(row) or int(session("aduserlevel")) => 9 then

			Authority = true
		end if

	end Function


	'//**********************************
	'	게시물 수정과 삭제권한 - 덱스트 사용
	'	디비 연결상태에서 사용해야함
	'***********************************//
	function delok_chk(byval b_idx)

			'//게시판글의 사용자 아이디와 비번 불러오기
			dim rs_pass
			set rs_pass = DBconn.execute("select userid, b_pass from board_v1 where b_idx='"& b_idx &"'")



				if lcase(session("aduserlevel")) = "9" then	'//관리자인경우
					delok_chk = true
				elseif lcase(session("aduserid")) = lcase(bs_admin) then	'//관리자 인 경우
					delok_chk = true
				elseif lcase(session("aduserid")) = rs_pass(0) then	'//글주인인경우
					delok_chk = true
				elseif dext("b_pass") = rs_pass(1) then	'//게시판 관리용 비번과 같은경우
					delok_chk = True
				elseif dext("b_pass") = rs_pass(1) and len(rs_pass(1)) > 0 then	'//해당글의 비번과 같은경우
					delok_chk = true
				end if


			rs_pass.close
			set rs_pass = nothing
	end function


	'//**********************************
	'	관리자 권한 - 일반
	'***********************************//
	function admin_chk()

		if lcase(session("aduserid")) = "admin" then	'//관리자인경우
			admin_chk = true
		elseif lcase(session("aduserid")) = lcase(bs_admin) then	'//게시판 관리자의 경우
			admin_chk = true
		end if

	end function

	'//**********************************
	'	소유자 권한 - 일반
	'	디비 연결상태에서 사용해야함
	'***********************************//
	function myline_chk(byval b_idx)
		dim rs_pass
		set rs_pass = DBconn.execute("select userid from board_v1 where b_idx='"& b_idx &"'")
			if len(session("aduserlevel")) = 0 then session("aduserlevel") = 0
			if lcase(session("aduserlevel")) >= 9 then	'//관리자인경우
				myline_chk = true
			elseif lcase(session("aduserid")) = lcase(bs_admin) then	'//게시판 관리자의 경우
				myline_chk = true
			elseif lcase(session("aduserid")) = rs_pass(0) then	'//글주인인경우
				myline_chk = true
			end if

		rs_pass.close
		set rs_pass = nothing
	end function




	'//**********************************
	'	체크함수
	'
	'***********************************//
	Function radio_checked(byval val, byval rs_val)'//두 값이 같으면 checked출력
		if val = rs_val then
			response.Write("checked")
		end if

	end Function

	Function radio_checked_bit(byval TF, byval rs_val)'//두 값이 같으면 checked출력'////비트연산
		if TF = false and not rs_val then
			response.Write("checked")
		elseif TF and rs_val then
			response.Write("checked")
		end if
	end Function



	'//**********************************
	'	필터
	'***********************************//

	'// 파일용량 점검 (DEXT안에서 사용)
	'// baord_v1에서만 사용가능함

	Function FileSize_chk(byval Kb)

		FileSize_chk = true

		if len(dext("file_1")) > 0 then

			dext.MaxFileLen = int(Kb)*1024*1024	'//MByte단위로 환산
			If dext("file_1").FileLen > dext.MaxFileLen Then '//제공값보다 파일 크기가 크면
				FileSize_chk = false
			end if
		end if

	End Function

%>
<link rel='stylesheet' type='text/css' href='/style.css'>
