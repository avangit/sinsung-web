<!--#include file="../../_Config.asp"-->


<%


  	Function radio_checked(byval val, byval rs_val)'//두 값이 같으면 checked출력
		if val = rs_val then
			response.Write("checked")
		end if

	end Function

	Function radio_checked_bit(byval TF, byval rs_val)'//두 값이 같으면 checked출력'////비트연산
		if TF = false and not rs_val then
			response.Write("checked")
		elseif TF and rs_val then
			response.Write("checked")
		end if
	end Function



	dim sql, rs, bs_code
	bs_code = request.QueryString("bs_code")
	DBopen
	set rs = DBconn.execute("select * from boardset_v2 where bs_code='"& bs_code &"'")

%>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i> </span>
		<h2>게시판 수정</h2>
	</header>
	<div>
		<div class="widget-body no-padding">
			<form action='?<%=getString("vmode=modify_ok&bs_code="&rs("bs_code"))%>' method="post" class="smart-form" enctype="multipart/form-data" name="update" >

			<table id="datatable_fixed_column" class="table table-striped table-bordered line-h" width="100%">
				<colgroup>
					<col style="width:15%">
					<col style="width:85%">
				</colgroup>
				<tr>
				  <th>게시판 아이디</th>
				  <td class="pl20"><font color="#0000FF"><a href="/avanplus/modul/avanboard_v3/board.asp?bs_code=<%=rs("bs_code")%>" target="_blank"><b><%=rs("bs_code")%></b></a></font><font color="#FF0000">&nbsp;&nbsp; [게시판 아이디는 변경하실수 없습니다.]</font></td>
				</tr>
				<tr>
				  <th>게시판 이름</th>
				  <td class="pl20">
					<label for="bs_name" class="input col-2" >
						<input type="text" name="bs_name" id="bs_name" placeholder="" value="<%=rs("bs_name")%>">
					</label>
				  </td>
				</tr>
				<tr>
				  <th>게시판관리</th>
				  <td class="pl20">
					  <b>게시판관리자ID</b>        
					  <label for="bs_admin" class="input col-1 ib" >
							<input type="text" name="bs_admin" id="bs_admin" placeholder="" value="<%=rs("bs_admin")%>">
					  </label>
					  등록된 ID user는 게시판 관리자 권한을 가집니다. <br>
					  <b>관리용비밀번호</b>      
					  <label for="bs_pw" class="input col-1 ib" >
						  <input type="password" name="bs_pw" id="bs_pw" placeholder="" value="<%=rs("bs_pw")%>" maxlength="20">
					  </label>
				      관리용비밀번호를 입력하면 게시글을 수정,삭제할 수 있습니다. </td>
				</tr>
				<tr>
				  <th>게시판 안내글</th>
				  <td class="pl20"><textarea name="bs_message" rows="5" id="bs_message" style="width:100% "><%=rs("bs_message")%></textarea></td>
				</tr>
				<tr>
				  <th>게시판 구분</th>
					<td class="pl20">
						<label class="radio ib">
							<input type="radio" name="bs_use" value="simple" <%If Trim(rs("bs_use")) =  "simple" then%>checked <%End if%>>
							<i></i>심플게시판</label>
						<label class="radio ib">
							<input type="radio" name="bs_use" value="board"  <%If Trim(rs("bs_use")) =  "board" then%>checked <%End if%> >
							<i></i>일반게시판</label>
						<label class="radio ib">
							<input type="radio" name="bs_use" value="pic" <%If Trim(rs("bs_use")) =  "pic" then%>checked <%End if%>>
							<i></i>사진게시판</label>
						<label class="radio ib">
							<input type="radio" name="bs_use" value="gallery"  <%If Trim(rs("bs_use")) =  "gallery" then%>checked <%End if%> >
							<i></i>사진갤러리</label>
						<label class="radio ib">
							<input type="radio" name="bs_use" value="video" <%If Trim(rs("bs_use")) =  "video" then%>checked <%End if%>>
							<i></i>동영상게시판</label>
				
					</td>
				</tr>
				<tr>
				  <th>게시물 제목길이</th>
				  <td class="pl20">
					<label for="bs_titlelen" class="input col-1 ib" >
						<input type="text" name="bs_titlelen" id="bs_titlelen" placeholder="" value="<%=rs("bs_titlelen")%>">
					</label>byte
				  </td>
				</tr>
				<tr>
				  <th>페이지당 목록수</th>
				  <td class="pl20">
					<label for="bs_pagerow" class="input col-1 ib" >
						<input type="text" name="bs_pagerow" id="bs_pagerow" placeholder="" value="<%=rs("bs_pagerow")%>">
					</label>개
				  </td>
				</tr>
				<tr>
				  <th>페이지 이동단위</th>
				  <td class="pl20">
					<label for="bs_pageblock" class="input col-1 ib" >
						<input type="text" name="bs_pageblock" id="bs_pageblock" placeholder="" value="<%=rs("bs_pageblock")%>">
					</label>개
				  </td>
				</tr>
				<tr>
				  <th>쓰기권한</th>
				  <td class="pl20">
					<label class="select state-disabled col-1 ib">
					<select name = "bs_writeLaw"  class="input-sm">
						<option value = "0" <% = chk_selected(rs("bs_writeLaw"), "0")%>>0</option>
						<option value = "1" <% = chk_selected(rs("bs_writeLaw"), "1")%>>1</option>
						<option value = "2" <% = chk_selected(rs("bs_writeLaw"), "2")%>>2</option>
						<option value = "3" <% = chk_selected(rs("bs_writeLaw"), "3")%>>3</option>
						<option value = "4" <% = chk_selected(rs("bs_writeLaw"), "4")%>>4</option>
						<option value = "5" <% = chk_selected(rs("bs_writeLaw"), "5")%>>5</option>
						<option value = "6" <% = chk_selected(rs("bs_writeLaw"), "6")%>>6</option>
						<option value = "7" <% = chk_selected(rs("bs_writeLaw"), "7")%>>7</option>
						<option value = "8" <% = chk_selected(rs("bs_writeLaw"), "8")%>>8</option>
						<option value = "9" <% = chk_selected(rs("bs_writeLaw"), "9")%>>9</option>
					</select><i></i> 
					</label>
					Level 0 : 비회원</td>
				</tr>
				<tr>
				  <th>읽기권한</th>
				  <td class="pl20">
					<label class="select state-disabled col-1 ib">
						<select name = "bs_readLaw"  class="input-sm">
							<option value = "0" <% = chk_selected(rs("bs_readLaw"), "0")%>>0</option>
							<option value = "1" <% = chk_selected(rs("bs_readLaw"), "1")%>>1</option>
							<option value = "2" <% = chk_selected(rs("bs_readLaw"), "2")%>>2</option>
							<option value = "3" <% = chk_selected(rs("bs_readLaw"), "3")%>>3</option>
							<option value = "4" <% = chk_selected(rs("bs_readLaw"), "4")%>>4</option>
							<option value = "5" <% = chk_selected(rs("bs_readLaw"), "5")%>>5</option>
							<option value = "6" <% = chk_selected(rs("bs_readLaw"), "6")%>>6</option>
							<option value = "7" <% = chk_selected(rs("bs_readLaw"), "7")%>>7</option>
							<option value = "8" <% = chk_selected(rs("bs_readLaw"), "8")%>>8</option>
							<option value = "9" <% = chk_selected(rs("bs_readLaw"), "9")%>>9</option>
						</select><i></i> 
					</label>
				   &nbsp; [ <b>비밀글기능</b>
					<label class="radio ib">
						<input type="radio" name="bs_bimilLaw" value="1" <%If Trim(rs("bs_bimilLaw")) =  "1" then%>checked <%End if%>>
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_bimilLaw" value="0"  <%If Trim(rs("bs_bimilLaw")) =  "0" then%>checked <%End if%> >
						<i></i>사용안함</label>
					]</td>
				</tr>
				<tr>
				  <th>답글쓰기</th>
				  <td class="pl20">
				    <label class="radio ib">
						<input type="radio" name="bs_rewrite" value="1" <%call radio_checked_bit(true,rs("bs_rewrite"))%>>
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_rewrite" value="0"  <%call radio_checked_bit(false,rs("bs_rewrite"))%>>
						<i></i>사용안함</label>
					&nbsp;&nbsp; [<b> 권한</b>:
					<label class="select state-disabled col-1 ib">
						<select name = "bs_rewriteLaw" class="input-sm">
							<option value = "0" <% = chk_selected(rs("bs_rewriteLaw"), "0")%>>0</option>
							<option value = "1" <% = chk_selected(rs("bs_rewriteLaw"), "1")%>>1</option>
							<option value = "2" <% = chk_selected(rs("bs_rewriteLaw"), "2")%>>2</option>
							<option value = "3" <% = chk_selected(rs("bs_rewriteLaw"), "3")%>>3</option>
							<option value = "4" <% = chk_selected(rs("bs_rewriteLaw"), "4")%>>4</option>
							<option value = "5" <% = chk_selected(rs("bs_rewriteLaw"), "5")%>>5</option>
							<option value = "6" <% = chk_selected(rs("bs_rewriteLaw"), "6")%>>6</option>
							<option value = "7" <% = chk_selected(rs("bs_rewriteLaw"), "7")%>>7</option>
							<option value = "8" <% = chk_selected(rs("bs_rewriteLaw"), "8")%>>8</option>
							<option value = "9" <% = chk_selected(rs("bs_rewriteLaw"), "9")%>>9</option>
						</select> <i></i>
					</label>]
					</td>
				</tr>
				<tr>
				  <th>리플기능</th>
				  <td class="pl20">
					<label class="radio ib">
						<input type="radio" name="bs_reply" value="1" <%call radio_checked_bit(true,rs("bs_reply"))%>>
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_reply" value="0"  <%call radio_checked_bit(false,rs("bs_reply"))%>>
						<i></i>사용안함</label>
					 &nbsp;&nbsp; [<font color="#0000FF"> 회원만 사용가능</font> ] </td>
				</tr>


				<tr>
				  <th>추천기능</th>
				  <td class="pl20">
				  
					<label class="radio ib">
						<input type="radio" name="recommend" value="1" <%call radio_checked_bit(true,rs("recommend"))%>>
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="recommend" value="0"  <%call radio_checked_bit(false,rs("recommend"))%>>
						<i></i>사용안함</label>
					
					&nbsp;&nbsp; [<font color="#0000FF"> 회원만 사용가능</font> ] </td>
				</tr>

				<tr>
				  <th>파일첨부</th>
				  <td class="pl20">
					
					<label class="radio ib">
						<input type="radio" name="bs_file" value="1" <%call radio_checked_bit(true,rs("bs_file"))%>>
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_file" value="0"  <%call radio_checked_bit(false,rs("bs_file"))%>>
						<i></i>사용안함</label>
					
					&nbsp;&nbsp; [ <b>업로드 용량제한</b>
					<label for="bs_filesize" class="input col-1 ib" >
						<input type="text" name="bs_filesize" id="bs_filesize" placeholder="" value="<%=rs("bs_filesize")%>">
					</label>
					MB ]
					 </td>
				</tr>
				<tr>
				  <th>IP표시</th>
				  <td class="pl20">
				  	<label class="radio ib">
						<input type="radio" name="bs_ipview" value="1" <%call radio_checked_bit(true,rs("bs_ipview"))%>>
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_ipview" value="0"  <%call radio_checked_bit(false,rs("bs_ipview"))%>>
						<i></i>사용안함</label>
				  
				  
				</tr>
				<tr>
				  <th>게시판 색지정 </th>
				  <td class="pl20">
				  <label for="bs_color" class="input col-1 ib" >
						<input type="text" name="bs_color" id="bs_color" placeholder="" value="<%=rs("bs_color")%>">
					</label>
				  </td>
				</tr>
				<tr>
				  <th>타이틀 이미지</th>
				  <td class="pl20">
				  <% if len(rs("file_titleimg")) > 0 then %>
				  <table width="100%"  border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
					<tr>
					  <td width="50"><div align="center">등록된<br>
						이미지</div></td>
					  <td><img src='/upload/<%=rs("file_titleimg")%>'></td>
					</tr>
				  </table>
				  <% end if %>
					<label class="radio ib">
						<input type="radio" name="bs_titleimg" value="1" <%call radio_checked_bit(true,rs("bs_titleimg"))%>>
						<i></i>사용</label>
					<label class="radio ib">
						<input type="radio" name="bs_titleimg" value="0" <%call radio_checked_bit(true,rs("bs_titleimg"))%>>
						<i></i>사용안함</label>

						<div class="input input-file w20 ib">
							<span class="button"><input type="file" id="file" name="file_titleimg" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="" value="<%=getFileNumName(rs("file_titleimg"))%>">
						</div>
						<% call FileDownLink_uppath(rs("file_titleimg"),"/upload/basic/")%>
				  (jpg,gif만 가능)</td>
				</tr>
				<tr>
				  <th>게시글 그룹핑</td>
				  <td>
				    <label for="bs_grouping" class="input col-1 ib" >
						<input type="text" name="bs_grouping" id="bs_grouping" maxlength="10" value="<%=rs("bs_grouping")%>">
					</label>
				  
				  10자 이내 </td>
				</tr>
			</table>

	
			<footer style="float:right;">
				<input type="submit" class="btn btn-primary" onclick="this.form.onsubmit();" value="수정하기">
				<input type="button" class="btn btn-default"  onclick="window.history.back();" value="리스트">
			</footer>
			</form>
		</div>
	</div>
</div>
	<%
		rs.close
		set rs = nothing
		DBclose
	%>