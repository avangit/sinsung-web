<!--#include file="../_config.asp"-->
<!--#include file="../BoardSet/_BoardsettingInfo.asp"-->
<html>
<head>
<!--#include file="form_chk.js"-->
<link href="/avanplus/Modul/avanboard_v3/Form_m/module.css" rel="stylesheet" />

</head>

<body>
<!-- 게시판 상단 타이틀 이미지 -->
<!--#include file="board_title.asp"-->
<%
	'response.write bs_writelaw
	'response.write session("userlevel")
if Authority(bs_writelaw) then%>
<div class="avanBoardForm">
	<form name="frmRequestForm"  method="post"  onsubmit="return frmRequestForm_Submit(this);" enctype="multipart/form-data" action='?<%=getString("vmode=exe_insert&b_idx=")%>'>



		<table class="write_list">
			<colspan>
				<col style="width:20%">
				<col>
			</colspan>
			<tr>
				<th>제목</th>
				<td><input type="text" name="b_title" id="b_title" class="input01" required>
					<% if admin_chk() then %>
					<span class="choice"><input type="checkbox" name="option_notice" value="1">공지</span>
					<% end if%>
				</td>
			</tr>
			<tr>
				<th>작성자</th>
				<td>
				   <input name="b_name" type="text" id="b_name" value='<%=session("username")%>' maxlength="20"  <%if len(session("userid")) > 0 and session("userlevel")<9 then%>readonly<% end if %>>
				</td>
			</tr>
			
			<%
			If request.querystring("bs_code") = "board3" then
			%>
			<tr>
				<th>전화번호</th>
				<td>
				<select class="select02 mobile1">
					<option>010</option>
					<option>011</option>
					<option>017</option>
					<option>018</option>
				</select>
				<input type="text" class="input02 mobile2" maxlength="4" ><input type="text" class="input02 mobile3" maxlength="4" >
				<input type="hidden" name="b_mobile">
				</td>
			</tr>
			<%
			End If 
			%>
			<% if bs_file then %>
			<tr>
				<th>첨부파일</th>
				<td>
					<input type="file" size="30" id="file_1" name="file_1" style="display: none;" onChange="document.getElementById('here').value=this.value;">
					<input type='text' id='here' readonly class="fileInput">
					<a class='file' href="javascript:document.getElementById('file_1').click();">찾기</a>

				</td>
			</tr>
			<% end if %>
			<tr>
				<th height="300px">내용입력</th>
				<td><textarea name="b_text"></textarea></td>
			</tr>
			<% if len(session("userid"))=0 or bs_bimilLaw then '//비회원 글쓰기 가능이면 %>
			<tr>
				<th>비밀번호</th>
				<td><input type="password" name="b_pass" class="input01"style="width:30%"></td>
			</tr>
			<% end if %>
			<% if bs_bimilLaw then %>
			<tr>
				<th>비밀글</th>
				<td><input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="1" checked >비밀글
					<input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="0" >일반글
				</td>
			</tr>
			<% end if%>

			<input name="b_part" type="hidden" id="b_part" value="<%=bs_code%>">
			<input name="b_writeday" type="hidden" id="b_writeday" value="<%=date()%>">
			<input name="b_ip" type="hidden" id="b_ip" value="<%=request.ServerVariables("remote_addr")%>">
			<input name="userid" type="hidden" id="userid" value="<%=session("userid")%>">

		</table>
		
		<div class="list_btn">
			<input type="button" value="취소하기" onClick="window.location='?<%=getString("vmode=list")%>'"><input type="button" value="등록하기" onclick="this.form.onsubmit();">
		</div>




	</form>
<% else %>

<br>
<br>
<br>
<br>
<center>
<table width="400" border="0" align="center" cellpadding="3" cellspacing="0" bgcolor="<%=bs_color%>" style="background-color:<%=bs_color%>;">
	<tr>
		<td height="30" style="vertical-align:middle;"><div align="center" class="WHITE"><font color="#FFFFFF"><strong>글등록 권한이 없습니다. </strong></font></div></td>
	</tr>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;">
		<td height="50" background="<%=Programpath%>/img/bg1000.gif" style="background-image:url('<%=Programpath%>/img/bg1000.gif');vertical-align:middle;">
			* <font color="#FF0000">회원을 위한 공간입니다. </font><br>
			* 권한이 있는 사용자로 로그인 후 이용해 주세요.<%=session("userlevel")%></td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td height="50" align="center" background="<%=Programpath%>/img/bg1000.gif" style="background-image:url('<%=Programpath%>/img/bg1000.gif');vertical-align:middle;" >
			<img src=<%=Img_back%> alt=" 뒤 로 " border="0" align="top" onClick="history.back(-1);">
		</td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
</table>
</center>
<% end if %>
<!-- <br>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
	<tr>
		<td align="right"><a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a></td>
	</tr>
</table> -->
</body>
</html>
<script language="javascript">
   function frmRequestForm_Submit(frm){

			if ( frm.b_title.value == "" ) { alert("제목을 입력해주세요"); frm.b_title.focus(); return false; }

			if ( frm.b_name.value == "" ) { alert("작성자를 입력해주세요"); frm.b_name.focus(); return false; }

			<%
			if (request.querystring("bs_code")="board2" or request.querystring("bs_code")="board3") and session("userlevel") <> "9" then
			%>
			if ( frm.b_pass.value == "" ) { alert("비밀번호를 입력해주세요"); frm.b_pass.focus(); return false; }
			<%
			end if 
			%>

			<%
			If request.querystring("bs_code") = "board3" then
			%>
			if ($(".mobile2").val() =="")
			{
				alert("전화번호를 입력해주세요")
				return false;
			}
			
			if ($(".mobile3").val() =="")
			{
				alert("전화번호를 입력해주세요")
				return false;
			}

			mobile1 = $(".mobile1").val()
			mobile2 = $(".mobile2").val()
			mobile3 = $(".mobile3").val()

			frm.b_mobile.value = mobile1+"-"+mobile2+"-"+mobile3
			<%
			end if
			%>
	   	frm.submit();
   }
</script>
