
<!--#include file="../_config.asp"-->
<!--#include file="../BoardSet/_BoardsettingInfo.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--#include file="form_chk.js"-->
<link href="/avanplus/Modul/avanboard_v3/Form/module.css" rel="stylesheet" />
</head>

<body>
<%
	DBopen
	dim sql, rs
	dim b_idx : b_idx = request.QueryString("b_idx")
	sql = "select * from board_v1 where b_idx = '" & b_idx & "'"
	set rs = DBconn.execute(sql)
%>
<!-- 게시판 상단 타이틀 이미지 -->
<!--#include file="board_title.asp"-->
<!-- 답글등록 권한 체크 -->
<% if Authority(bs_rewritelaw) then%>
<form name="frmRequestForm"  method="post"  onsubmit="return frmRequestForm_Submit(this);" enctype="multipart/form-data" action='?<%=getString("vmode=exe_insert&ref="&rs("ref"))%>'>
	<table class="write_list">
		<colspan>
			<col style="width:15%">
			<col>
		</colspan>
		<tr>
			<th>제목</th>
			<td>
				<input name="b_part" type="hidden" id="b_part" value="<%=bs_code%>">
				<input name="b_writeday" type="hidden" id="b_writeday" value="<%=date()%>">
				<input name="b_ip" type="hidden" id="b_ip" value="<%=request.ServerVariables("remote_addr")%>">
				<input name="userid" type="hidden" id="userid" value="<%=session("userid")%>">
				<input type="text" name="b_title" id="b_title" class="input01" required>
				<% if admin_chk() then %>
				<span class="choice"><input type="checkbox" name="option_notice" value="1">공지</span>
				<% end if%>
			</td>
		</tr>
		<tr>
			<th>작성자</th>
			<td>
			   <input name="b_name" type="text" id="b_name" value='<%=session("username")%>' maxlength="20"  <%if len(session("userid")) > 0 and session("userlevel")<9 then%>readonly<% end if %>>
			</td>
		</tr>
		
		
		<% if bs_file then %>
		<tr>
			<th>첨부파일</th>
			<td>
				<input type="file" size="30" id="file_1" name="file_1" style="display: none;" onChange="document.getElementById('here').value=this.value;">
				<input type='text' id='here' readonly class="fileInput">
				<a class='file' href="javascript:document.getElementById('file_1').click();">찾기</a>

			</td>
		</tr>
		<% end if %>
		<tr>
			<th>문의내역</th>
			<td><%=rs("b_text")%></td>
		</tr>
		<tr>
			<th height="300px">내용입력</th>
			<td><%call Editor("b_text","")%></td>
		</tr>
		<tr>
			<th>비밀번호</th>
			<td><input type="text" value="<%=rs("b_pass")%>" name="b_pass" class="input01"style="width:30%" readonly></td>
		</tr>
		<% if bs_bimilLaw then %>
		<tr>
			<th>비밀글</th>
			<td><input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="1" checked >비밀글
				<input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="0" >일반글
			</td>
		</tr>
		<% end if%>

		<input name="b_part" type="hidden" id="b_part" value="<%=bs_code%>">
		<input name="b_writeday" type="hidden" id="b_writeday" value="<%=date()%>">
		<input name="b_ip" type="hidden" id="b_ip" value="<%=request.ServerVariables("remote_addr")%>">
		<input name="userid" type="hidden" id="userid" value="<%=session("userid")%>">

	</table>
	
	<div class="list_btn">
		<input type="button" value="취소하기" onClick="window.location='?<%=getString("vmode=list")%>'"><input type="button" value="등록하기" onclick="this.form.onsubmit();">
	</div>



















</form>
<script language="javascript">
  function frmRequestForm_Submit(frm){

	   if ( frm.b_title.value == "" ) { alert("제목을 입력해주세요"); frm.b_title.focus(); return false; }
	   <%if request.QueryString("bs_code")<>"board1" then%>
	   if ( frm.b_name.value == "" ) { alert("작성자를 입력해주세요"); frm.b_name.focus(); return false; }
	   <%end if%>
	   <% if len(session("userid"))=0 or bs_bimilLaw then '//비회원 글쓰기 가능이면 %>
	   if ( frm.b_pass.value == "" ) { alert("비밀번호를 입력해주세요"); frm.b_pass.focus(); return false; }
	   <%end if%>

	   	frm.submit();
   }
</script>
<%else%>
<br>
<br>
<br>
<br>
<div class="cssAvanBdViewCenter">
<table width="400" border="0" align="center" cellpadding="3" cellspacing="0" bgcolor="<%=bs_color%>" class="cssAvanBdViewTable rewrite_msg">
	<tr>
		<td height="50" align="center" class="WHITE"><font color="#FFFFFF"><B>답글등록 권한이 없습니다.</B></font></td>
	</tr>
	<tr bgcolor="#CCCCCC" class="cssAvanBdViewTableTrLine">
		<td height="1"></td>
	</tr>
	<tr bgcolor="#FFFFFF" class="cssAvanBdViewTableTr">
		<td height="50" class="fsize">
			<font color="#bf141d">회원제 게시판 입니다. </font><br>
			권한을 가진 사용자로 로그인후 이용해 주세요.
		</td>
	</tr>
	<tr bgcolor="#FFFFFF" class="cssAvanBdViewTableTr">
		<td height="50" align="center">
			<img src=<%=Img_back%> alt=" 뒤 로 " border="0" align="top" onClick="history.back(-1);">
		</td>
	</tr>
</table>
</div>
<% end if %>

<!-- <table width="100%" border="0" cellspacing="0" cellpadding="10">
	<tr>
		<td align="right">
			<a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a>
		</td>
	</tr>
</table> -->
<%
	rs.close
	set rs = nothing
	DBclose
%>

</body>
</html>
