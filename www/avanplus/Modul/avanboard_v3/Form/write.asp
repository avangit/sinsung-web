<!--#include file="../_config.asp"-->
<!--#include file="../BoardSet/_BoardsettingInfo.asp"-->
<html>
<head>
<!--#include file="form_chk.js"-->
<link href="/avanplus/Modul/avanboard_v3/Form/module.css" rel="stylesheet" />
<script src="/admin/js/jquery-2.1.1.min.js"></script>
</head>

<body>
<!-- 게시판 상단 타이틀 이미지 -->
<!--#include file="board_title.asp"-->
<%
	'response.write bs_writelaw
	'response.write session("userlevel")
if Authority(bs_writelaw) then%>
<div class="avanBoardForm">
	<form name="frmRequestForm"  method="post"  onsubmit="return frmRequestForm_Submit(this);" enctype="multipart/form-data" action='?<%=getString("vmode=exe_insert&b_idx=")%>'>



		<table class="write_list">
			<colspan>
				<col style="width:15%">
				<col>
			</colspan>
			<tr>
				<th><% If request.querystring("bs_code") = "board11" Then %>회사명<% Else %>작성자<% End If %></th>
				<td>
				   <input name="b_name" type="text" id="b_name" value='<%=session("adusername")%>' maxlength="20"  <%if len(session("aduserid")) > 0 and session("aduserlevel")<9 then%>readonly<% end if %>>
				</td>
			</tr>
		<% If request.querystring("bs_code") = "board01" Then %>
			<tr>
				<th>홈페이지/SRM 출력</th>
				<td>
					<select name="display_mode" id="display_mode">
						<option value="both">홈페이지 + SRM</option>
						<option value="home">홈페이지</option>
						<option value="srm">SRM</option>
					</select>
				</td>
			</tr>
		<% ElseIf request.querystring("bs_code") = "board04" Or bs_code = "board05" Then %>
			<tr>
				<th>상태</th>
				<td>
					<select name="b_addtext2" id="b_addtext2">
						<option value="진행중">진행중</option>
						<option value="마감">마감</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>기간</th>
				<td>
					<input type="text" name="b_addtext3" id="b_addtext3" autocomplete="off"> ~ <input type="text" name="b_addtext4" id="b_addtext4" autocomplete="off">
				</td>
			</tr>
		<% ElseIf request.querystring("bs_code") = "board06" Or request.querystring("bs_code") = "board07" Or request.querystring("bs_code") = "board08" Then %>
			<tr>
				<th>제조사</th>
				<td>
					<select name="b_addtext5" id="b_addtext5">
						<option value="HP">HP</option>
						<option value="삼성">삼성</option>
						<option value="삼성 단납점 모델">삼성 단납점 모델</option>
						<option value="LG">LG</option>
						<option value="HPE">HPE</option>
						<option value="NETGEAR">NETGEAR</option>
						<option value="ATEN">ATEN</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>카테고리</th>
				<td>
					<select name="b_addtext6" id="b_addtext6">
						<option value="데스크탑/서버">데스크탑/서버</option>
						<option value="디스플레이/TV">디스플레이/TV</option>
						<option value="화상회의">화상회의</option>
						<option value="복합기/프린터">복합기/프린터</option>
						<option value="네트워크">네트워크</option>
						<option value="태블릿/모바일">태블릿/모바일</option>
						<option value="부품/소프트웨어">부품/소프트웨어</option>
						<option value="가전">가전</option>
					</select>
				</td>
			</tr>
		<% ElseIf request.querystring("bs_code") = "board09" Then %>
			<tr>
				<th>분류</th>
				<td>
					<select name="b_cate" id="b_cate">
						<option value="하드웨어">하드웨어</option>
						<option value="솔루션">솔루션</option>
					</select>
				</td>
			</tr>
		<% End If %>
			<tr>
				<th>제목</th>
				<td><input type="text" name="b_title" id="b_title" class="input01" required>
					<% if admin_chk() And request.querystring("bs_code") <> "board11" then %>
					<span class="choice"><input type="checkbox" name="option_notice" value="1">공지</span>
					<% end if%>
				</td>
			</tr>

		<% If request.querystring("bs_code") = "board03" Or request.querystring("bs_code") = "board08" Then %>
			<tr>
				<th>동영상 코드</th>
				<td>
					<!--input type="text" name="b_addtext1" id="b_addtext1" class="input01"-->
					<textarea name="b_addtext1" id="b_addtext1"></textarea>
				</td>
			</tr>
		<% End If %>
			<tr>
				<th height="300px">내용입력</th>
				<td><%call Editor("b_text","")%></td>
			</tr>
		<% if bs_file then %>
			<tr>
				<th <% If bs_code = "board01" Or bs_code = "board04" Or bs_code = "board05" Or bs_code = "board06" Or bs_code = "board07" Then %>rowspan="3"<% End If %>>첨부파일</th>
				<td>
					<input type="file" size="60" id="file_1" name="file_1" style="display: none;" onChange="document.getElementById('here').value=this.value;">
					<input type='text' id='here' readonly class="fileInput">
					<a class='file' href="javascript:document.getElementById('file_1').click();">찾기</a> *권장 이미지 사이즈 500*375
				</td>
			</tr>
			<% If bs_code = "board01" Or bs_code = "board04" Or bs_code = "board05" Or bs_code = "board06" Or bs_code = "board07" Then %>
			<tr>
				<td>
					<input type="file" size="60" id="file_2" name="file_2" style="display: none;" onChange="document.getElementById('here2').value=this.value;">
					<input type='text' id='here2' readonly class="fileInput">
					<a class='file' href="javascript:document.getElementById('file_2').click();">찾기</a> *권장 이미지 사이즈 500*375
				</td>
			</tr>
			<tr>
				<td>
					<input type="file" size="60" id="file_3" name="file_3" style="display: none;" onChange="document.getElementById('here3').value=this.value;">
					<input type='text' id='here3' readonly class="fileInput">
					<a class='file' href="javascript:document.getElementById('file_3').click();">찾기</a> *권장 이미지 사이즈 500*375
				</td>
			</tr>
			<% End If %>
		<% end if %>
			<% if len(session("aduserid"))=0 or bs_bimilLaw then '//비회원 글쓰기 가능이면 %>
			<tr>
				<th>비밀번호</th>
				<td><input type="password" name="b_pass" class="input01"style="width:30%"></td>
			</tr>
			<% end if %>
			<% if bs_bimilLaw then %>
			<tr>
				<th>비밀글</th>
				<td><input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="1" checked >비밀글
					<input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="0" >일반글
				</td>
			</tr>
			<% end if%>

			<input name="b_part" type="hidden" id="b_part" value="<%=bs_code%>">
			<input name="b_writeday" type="hidden" id="b_writeday" value="<%=date()%>">
			<input name="b_ip" type="hidden" id="b_ip" value="<%=request.ServerVariables("remote_addr")%>">
			<input name="userid" type="hidden" id="userid" value="<%=session("aduserid")%>">

		</table>

		<div class="list_btn">
			<input type="button" value="취소하기" onClick="window.location='?<%=getString("vmode=list")%>'"><input type="button" value="등록하기" onclick="this.form.onsubmit();">
		</div>




	</form>
<% else %>

<br>
<br>
<br>
<br>
<center>
<table width="400" border="0" align="center" cellpadding="3" cellspacing="0" bgcolor="<%=bs_color%>" style="background-color:<%=bs_color%>;">
	<tr>
		<td height="30" style="vertical-align:middle;"><div align="center" class="WHITE"><font color="#FFFFFF"><strong>글등록 권한이 없습니다. </strong></font></div></td>
	</tr>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;">
		<td height="50" background="<%=Programpath%>/img/bg1000.gif" style="background-image:url('<%=Programpath%>/img/bg1000.gif');vertical-align:middle;">
			* <font color="#FF0000">회원을 위한 공간입니다. </font><br>
			* 권한이 있는 사용자로 로그인 후 이용해 주세요.<%=session("userlevel")%></td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td height="50" align="center" background="<%=Programpath%>/img/bg1000.gif" style="background-image:url('<%=Programpath%>/img/bg1000.gif');vertical-align:middle;" >
			<img src=<%=Img_back%> alt=" 뒤 로 " border="0" align="top" onClick="history.back(-1);">
		</td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
</table>
</center>
<% end if %>
<!-- <br>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
	<tr>
		<td align="right"><a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a></td>
	</tr>
</table> -->
</body>
</html>
<script language="javascript">
   function frmRequestForm_Submit(frm){

			if ( frm.b_title.value == "" ) { alert("제목을 입력해주세요"); frm.b_title.focus(); return false; }

			if ( frm.b_name.value == "" ) { alert("작성자를 입력해주세요"); frm.b_name.focus(); return false; }

			<%
			if (request.querystring("bs_code")="board2" or request.querystring("bs_code")="board3") and session("aduserlevel") <> "9" then
			%>
			if ( frm.b_pass.value == "" ) { alert("비밀번호를 입력해주세요"); frm.b_pass.focus(); return false; }
			<%
			end if
			%>

			<%
			If request.querystring("bs_code") = "board3" then
			%>
			if ($(".mobile2").val() =="")
			{
				alert("전화번호를 입력해주세요")
				return false;
			}

			if ($(".mobile3").val() =="")
			{
				alert("전화번호를 입력해주세요")
				return false;
			}

			mobile1 = $(".mobile1").val()
			mobile2 = $(".mobile2").val()
			mobile3 = $(".mobile3").val()

			frm.b_mobile.value = mobile1+"-"+mobile2+"-"+mobile3
			<%
			end if
			%>
	   	frm.submit();
   }

$(document).ready(function () {
	$.datepicker.setDefaults($.datepicker.regional['ko']);

	$( "#b_addtext3" ).datepicker({
		changeMonth: true,  // 월을 바꿀수 있는 셀렉트 박스를 표시한다.
		changeYear: true,	// 년을 바꿀 수 있는 셀렉트 박스를 표시한다.
		showMonthAfterYear: true,	// 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다
		nextText: '다음 달',
		prevText: '이전 달',
		dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dateFormat: "yy-mm-dd",
//					minDate: 0,		// 선택할수있는 최소날짜, ( 0 : 오늘 이전 날짜 선택 불가)
		onClose: function( selectedDate ) {
			//시작일(startDate) datepicker가 닫힐때 종료일(endDate)의 선택할수있는 최소 날짜(minDate)를 선택한 시작일로 지정
			$("#b_addtext4").datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#b_addtext4" ).datepicker({
		changeMonth: true,
		changeYear: true,
		showMonthAfterYear: true,
		nextText: '다음 달',
		prevText: '이전 달',
		dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dateFormat: "yy-mm-dd",
//				onClose: function( selectedDate ) {
			// 종료일(endDate) datepicker가 닫힐때 시작일(startDate)의 선택할수있는 최대 날짜(maxDate)를 선택한 시작일로 지정
//					$("#sdate").datepicker( "option", "maxDate", selectedDate );
//				}

	});
});
</script>