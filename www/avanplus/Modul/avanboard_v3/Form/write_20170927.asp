<!--#include file="../_config.asp"-->
<!--#include file="../BoardSet/_BoardsettingInfo.asp"-->
<html>
<head>
<!--#include file="form_chk.js"-->
<link href="/avanplus/Modul/avanboard_v3/Form/module.css" rel="stylesheet" />

</head>

<body>
<!-- 게시판 상단 타이틀 이미지 -->
<!--#include file="board_title.asp"-->
<%
	'response.write bs_writelaw
	'response.write session("userlevel")
if Authority(bs_writelaw) then%>
<div class="avanBoardForm">
<table class="avanBoard1_form">
	<colgroup>
    	<col style="width:15%">
        <col style="width:85%">
    </colgroup>
	<form name="frmRequestForm"  method="post"  onsubmit="return frmRequestForm_Submit(this);" enctype="multipart/form-data" action='?<%=getString("vmode=exe_insert&b_idx=")%>'>

	
    <tr>
        <th>제목</th>
        <td><input type="text" name="b_title" id="b_title" style="width:85%" required>
        	<% if admin_chk() then %>
            <span class="choice"><input type="checkbox" name="option_notice" value="1">공지</span>
            <% end if%>
        </td>
    </tr>
    <%if request.QueryString("bs_code") <> "board1" then%>
    <tr>
        <th>작성자</th>
        <td>
           <input name="b_name" type="text" id="b_name" value='<%=session("username")%>' maxlength="20"  <%if len(session("userid")) > 0 and session("userlevel")<9 then%>readonly<% end if %>>
        </td>
    </tr>
    <%end if%>
    <% if bs_file then %>
    <tr>
        <th>첨부파일</th>
        <td>
            <input type="file" size="30" id="file_1" name="file_1" style="display: none;" onChange="document.getElementById('here').value=this.value;">
            <input type='text' id='here' readonly class="fileInput"> 
            <a class='file' href="javascript:document.getElementById('file_1').click();">찾기</a> <span class="imgDelete"><input type="checkbox">이미지삭제</span>
           
        </td>
    </tr>
    <% end if %>
 
	 <tr>
          <th height="300px">내용입력</th>
          <td><%call Editor("b_text","")%></td>
      </tr>
      <% if len(session("userid"))=0 and bs_bimilLaw then '//비회원 글쓰기 가능이면 %>
      <tr>
        	<th>비밀번호</th>
            <td><input type="text" name="b_pass" style="width:30%"></td>
      </tr>
      <% end if %>
      <% if bs_bimilLaw then %>
      <tr>
        	<th>비밀글</th>
            <td><input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="1" checked >비밀글
				<input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="0" >일반글
            </td>
      </tr>
      <% end if%>
      
<!--
	<% if bs_bimilLaw then %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>비밀글 설정</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
			<font color="#003366">
				<input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="1" checked >비밀글
				<input name="option_bimil" type="radio"  style="border:0px;background-color:transparent" value="0" >일반글			</font>		</td>
	</tr>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td background="/avanplus/modul/avanboard_v3//img/bg1000.gif" style="padding:6 0 2 40; background-image:url('/avanplus/modul/avanboard_v3//img/bg1000.gif');"><img src=/avanplus/img/avanboard/b_prepoint.gif border="0"> 비밀번호</td>
		<td background="/avanplus/modul/avanboard_v3//img/bg1000.gif" style="padding:6 0 2 0; background-image:url('/avanplus/modul/avanboard_v3//img/bg1000.gif');">
			<input name="b_pass" type="password" id="b_pass" size="20" maxlength="20" required>		</td>
	</tr>
	<% end if %>

	<!--<% if bs_file then %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>첨부파일</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
			<input name="file_1" type="file" id="file_1" size="40"> [최대 <%=bs_filesize%>MB]		</td>
	</tr>
	<% end if %>-->

	<!--<% if bs_writelaw = 0 then '//비회원 글쓰기 가능이면 %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>비밀번호</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
			<input name="b_pass" type="password" id="b_pass" size="20" maxlength="20" required>		</td>
	</tr>
	<% end if %>-->

	<!--<% if admin_chk() then %>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td height="41" background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 40; background-image:url('<%=Programpath%>/img/bg1000.gif');"><img src=<%=Img_point%> border="0"> <b>공지글등록</b></td>
		<td background="<%=Programpath%>/img/bg1000.gif" style="padding:6 0 2 0; background-image:url('<%=Programpath%>/img/bg1000.gif');">
            <input type="radio"  style="border:0px;background-color:transparent" name="option_notice" value="1">공지글 &nbsp;&nbsp;
            <input name="option_notice" type="radio"  style="border:0px;background-color:transparent" value="0" checked>일반글
        </td>
	</tr>
	<% end if %>-->

	<tr>
		<td height="2" colspan="2"></td>
	</tr>

	<!--<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;" class="cssAvanJoinFormTrHeight">
		<td colspan="2" align="right" style="padding-top:20px;">
			<input  TYPE="IMAGE" src="<%=Img_entry%>" name="Submit" value="Submit">
			<img src=<%=Img_cancel%> align="top" alt=" 다 시 " border="0" onClick="document.form1.reset();">
			<img src=<%=Img_back%> align="top" alt=" 뒤 로 " border="0" onClick="history.back(-1);">
            <a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a>
		</td>
	</tr>-->
	<input name="b_part" type="hidden" id="b_part" value="<%=bs_code%>">
	<input name="b_writeday" type="hidden" id="b_writeday" value="<%=date()%>">
	<input name="b_ip" type="hidden" id="b_ip" value="<%=request.ServerVariables("remote_addr")%>">
	<input name="userid" type="hidden" id="userid" value="<%=session("userid")%>">

</table>
</div>

    <ul class="avanBoard1_btn">
        <li><input type="button" value="등록"  onclick="this.form.onsubmit();"></li>
        <li><input type="button" value="목록"  onClick="history.back(-1);"></li>
    </ul>
    	</form>
<% else %>

<br>
<br>
<br>
<br>
<center>
<table width="400" border="0" align="center" cellpadding="3" cellspacing="0" bgcolor="<%=bs_color%>" style="background-color:<%=bs_color%>;">
	<tr>
		<td height="30" style="vertical-align:middle;"><div align="center" class="WHITE"><font color="#FFFFFF"><strong>글등록 권한이 없습니다. </strong></font></div></td>
	</tr>
	<tr bgcolor="#FFFFFF" style="background-color:#FFFFFF;">
		<td height="50" background="<%=Programpath%>/img/bg1000.gif" style="background-image:url('<%=Programpath%>/img/bg1000.gif');vertical-align:middle;">
			* <font color="#FF0000">회원을 위한 공간입니다. </font><br>
			* 권한이 있는 사용자로 로그인 후 이용해 주세요.<%=session("userlevel")%></td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td height="50" align="center" background="<%=Programpath%>/img/bg1000.gif" style="background-image:url('<%=Programpath%>/img/bg1000.gif');vertical-align:middle;" >
			<img src=<%=Img_back%> alt=" 뒤 로 " border="0" align="top" onClick="history.back(-1);">
		</td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
</table>
</center>
<% end if %>
<!-- <br>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
	<tr>
		<td align="right"><a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a></td>
	</tr>
</table> -->
</body>
</html>
<script language="javascript">
   function frmRequestForm_Submit(frm){
	   
	   if ( frm.b_title.value == "" ) { alert("제목을 입력해주세요"); frm.b_title.focus(); return false; }
	   <%if request.QueryString("bs_code")<>"board1" then%>
	   if ( frm.b_name.value == "" ) { alert("작성자를 입력해주세요"); frm.b_name.focus(); return false; }
	   <%end if%>
	   
	   	frm.submit();
   }
</script>