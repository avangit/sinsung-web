<!--#include file="../_config.asp"-->
<!--#include file="../BoardSet/_BoardsettingInfo.asp"-->
<link href="/avanplus/Modul/avanboard_v3/Form/module.css" rel="stylesheet">
<script src="/admin/js/jquery-2.1.1.min.js"></script>
<!--#include file="form_chk.js"-->
<body>
<%
	Function radio_checked_bit(byval TF, byval rs_val)'//두 값이 같으면 checked출력'////비트연산
		if TF = false and not rs_val then
			response.Write(" checked ")
		elseif TF and rs_val then
			response.Write(" checked ")
		end if
	end Function

	DBopen
	dim sql, rs
	Dim mobile
	dim b_idx : b_idx = request.QueryString("b_idx")

	sql = "select * from board_v1 where b_idx = '" & b_idx & "'"
	set rs = DBconn.execute(sql)

	'//비밀번호와 게시글 비번 체크
	dim passok
	passok = 0
	if(isnull(rs("b_pass"))) then
		if not myline_chk(b_idx) then
			call jsAlertMsgUrl("게시글 수정 권한이 없습니다.", BackGetString("vmode=view"))
		end if
	else
		if trim(rs("b_pass")) = trim(request.form("pw")) then
			passok = 1
		elseif request.form("pw") <> "" and bs_writelaw = 0 then
			error("패스워드가 일치하지 않습니다.")
		end if
	End If


	if not (myline_chk(b_idx) or passok = 1) then '비회원 등록글이면
			%>
             <div class="md_secret_check">
                <form name="pw" class="" action="?<%=getString("vmode=modify")%>" method="post">
                    <h3>비밀글입니다.</h3>
                    <p>아래 등록 시 입력하신 비밀번호 입력해 주세요.</p>
                    <table class="secret_box" cellpadding="0" cellspacing="0">
                        <tr>
                          <th>비밀번호</th>
                          <td>
                          <%
							if bs_writelaw = 0 then
							%>
                          <input type="password" name="pw" >
                          <%end if%>
                          </td>
                        </tr>
                    </table>
                    <input type="image" src="<%=Img_amend%>" alt=" 수 정 " align="absmiddle" style="border:0px;">
                    <img src="<%=Img_back%>" align="absmiddle" alt=" 뒤 로 " border="0" onClick="history.back(-1);">
                </form>
            </div>




<%
	else	'//해당글의 소유자이거나

%>
<!-- 게시판 상단 타이틀 이미지 -->
<!--#include file="board_title.asp"-->
<div class="avanBoardForm">

	<form name="frmRequestForm"  method="post"  onsubmit="return frmRequestForm_Submit(this);" enctype="multipart/form-data" action='?<%=getString("vmode=Exe_modify")%>'>

		<table class="write_list">
			<colspan>
				<col style="width:15%">
				<col>
			</colspan>
		<% If request.querystring("bs_code") = "board01" Then %>
			<tr>
				<th>홈페이지/SRM 출력</th>
				<td>
					<select name="display_mode" id="display_mode">
						<option value="both" <% If rs("display_mode") = "both" Then %> selected<% End If %>>홈페이지 + SRM</option>
						<option value="home" <% If rs("display_mode") = "home" Then %> selected<% End If %>>홈페이지</option>
						<option value="srm" <% If rs("display_mode") = "srm" Then %> selected<% End If %>>SRM</option>
					</select>
				</td>
			</tr>
		<% ElseIf request.querystring("bs_code") = "board04" Then %>
			<tr>
				<th>상태</th>
				<td>
					<select name="b_addtext2" id="b_addtext2">
						<option value="진행중" <% If rs("b_addtext2") = "진행중" Then %> selected<% End If %>>진행중</option>
						<option value="마감" <% If rs("b_addtext2") = "마감" Then %> selected<% End If %>>마감</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>기간</th>
				<td>
					<input type="text" name="b_addtext3" id="b_addtext3" value="<%=rs("b_addtext3")%>" autocomplete="off"> ~ <input type="text" name="b_addtext4" id="b_addtext4" value="<%=rs("b_addtext4")%>" autocomplete="off">
				</td>
			</tr>
		<% ElseIf request.querystring("bs_code") = "board06" Or request.querystring("bs_code") = "board07" Or request.querystring("bs_code") = "board08" Then %>
			<tr>
				<th>제조사</th>
				<td>
					<select name="b_addtext5" id="b_addtext5">
						<option value="HP" <% If rs("b_addtext5") = "HP" Then %> selected<% End If %>>HP</option>
						<option value="삼성" <% If rs("b_addtext5") = "삼성" Then %> selected<% End If %>>삼성</option>
						<option value="삼성 단납점 모델" <% If rs("b_addtext5") = "삼성 단납점 모델" Then %> selected<% End If %>>삼성 단납점 모델</option>
						<option value="LG" <% If rs("b_addtext5") = "LG" Then %> selected<% End If %>>LG</option>
						<option value="HPE" <% If rs("b_addtext5") = "HPE" Then %> selected<% End If %>>HPE</option>
						<option value="NETGEAR" <% If rs("b_addtext5") = "NETGEAR" Then %> selected<% End If %>>NETGEAR</option>
						<option value="ATEN" <% If rs("b_addtext5") = "ATEN" Then %> selected<% End If %>>ATEN</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>카테고리</th>
				<td>
					<select name="b_addtext6" id="b_addtext6">
						<option value="데스크탑/서버" <% If rs("b_addtext6") = "데스크탑/서버" Then %> selected<% End If %>>데스크탑/서버</option>
						<option value="디스플레이/TV" <% If rs("b_addtext6") = "디스플레이/TV" Then %> selected<% End If %>>디스플레이/TV</option>
						<option value="화상회의" <% If rs("b_addtext6") = "화상회의" Then %> selected<% End If %>>화상회의</option>
						<option value="복합기/프린터" <% If rs("b_addtext6") = "복합기/프린터" Then %> selected<% End If %>>복합기/프린터</option>
						<option value="네트워크" <% If rs("b_addtext6") = "네트워크" Then %> selected<% End If %>>네트워크</option>
						<option value="태블릿/모바일" <% If rs("b_addtext6") = "태블릿/모바일" Then %> selected<% End If %>>태블릿/모바일</option>
						<option value="부품/소프트웨어" <% If rs("b_addtext6") = "부품/소프트웨어" Then %> selected<% End If %>>부품/소프트웨어</option>
						<option value="가전" <% If rs("b_addtext6") = "가전" Then %> selected<% End If %>>가전</option>
					</select>
				</td>
			</tr>
		<% ElseIf request.querystring("bs_code") = "board09" Then %>
			<tr>
				<th>분류</th>
				<td>
					<select name="b_cate" id="b_cate">
						<option value="하드웨어" <% If rs("b_cate") = "하드웨어" Then %> selected<% End If %>>하드웨어</option>
						<option value="솔루션" <% If rs("b_cate") = "솔루션" Then %> selected<% End If %>>솔루션</option>
					</select>
				</td>
			</tr>
		<% End If %>
			<tr>
				<th>제목</th>
				<td><input type="text" name="b_title" id="b_title" class="input01" value="<%=rs("b_title")%>" required>
					<% if admin_chk() then %>
					<span class="choice"><input type="checkbox" name="option_notice" value="1" <%if rs("option_notice")="True" then%> checked <%end if%>>공지</span></td>
					<% end if%>
				</td>
			</tr>
			<tr>
				<th>작성자</th>
				<td>
				   <input name="b_name" type="text" id="b_name" value='<%=rs("b_name")%>' maxlength="20"  <%if len(session("userid")) > 0 and session("userlevel")<9 then%>readonly<% end if %>>
				</td>
			</tr>
		<% If request.querystring("bs_code") = "board03" Or request.querystring("bs_code") = "board08" Then %>
			<tr>
				<th>동영상 코드</th>
				<td>
					<!--input type="text" name="b_addtext1" id="b_addtext1" class="input01"-->
					<textarea name="b_addtext1" id="b_addtext1"><%=rs("b_addtext1")%></textarea>
				</td>
			</tr>
		<% End If %>
		<% if bs_file then %>
			<tr>
				<th <% If bs_code = "board01" Or bs_code = "board04" Or bs_code = "board06" Or bs_code = "board07" Or bs_code = "board08" Then %>rowspan="3"<% End If %>>첨부파일</th>
				<td>
					<input type="file" size="30" id="file_1" name="file_1" style="display: none;" onChange="document.getElementById('here').value=this.value;">
					<input type='text' id='here' readonly class="fileInput" value="<%=rs("file_1")%>">
					<a class='file' href="javascript:document.getElementById('file_1').click();">찾기</a>

				</td>
			</tr>
			<% If bs_code = "board01" Or bs_code = "board04" Or bs_code = "board06" Or bs_code = "board07" Or bs_code = "board08" Then %>
			<tr>
				<td>
					<input type="file" size="30" id="file_2" name="file_2" style="display: none;" onChange="document.getElementById('here2').value=this.value;">
					<input type='text' id='here2' readonly class="fileInput" value="<%=rs("file_2")%>">
					<a class='file' href="javascript:document.getElementById('file_2').click();">찾기</a>

				</td>
			</tr>
			<tr>
				<td>
					<input type="file" size="30" id="file_3" name="file_3" style="display: none;" onChange="document.getElementById('here3').value=this.value;">
					<input type='text' id='here3' readonly class="fileInput" value="<%=rs("file_3")%>">
					<a class='file' href="javascript:document.getElementById('file_3').click();">찾기</a>

				</td>
			</tr>
			<% End If %>
		<% end if %>
			<% If rs("re_level") > 0 then%>
			<tr>
				<th>문의내용</th>
				<td>
					<%=getAdoRsScalar("select b_text from BOARD_v1 where ref = '"&rs("ref")&"' and re_level = '0' ")%>
				</td>
			</tr>
			<%
			End if
			%>
			<tr>
				<th height="300px">내용입력</th>
				<td><%call Editor("b_text",rs("b_text"))%></td>
			</tr>


		</table>

		<div class="list_btn">
			<input type="button" value="수정" onclick="this.form.onsubmit();"><input type="button" value="취소" onClick="window.location='?<%=getString("vmode=list")%>'">
		</div>




    </form>

</div>
<script language="javascript">
   function frmRequestForm_Submit(frm){

		if ( frm.b_title.value == "" ) { alert("제목을 입력해주세요"); frm.b_title.focus(); return false; }
		if ( frm.b_name.value == "" ) { alert("작성자를 입력해주세요"); frm.b_name.focus(); return false; }


		<%
		If request.querystring("bs_code") = "board3" and rs("re_level") = "0" then
		%>
		if ($(".mobile2").val() =="")
		{
			alert("전화번호를 입력해주세요")
			return false;
		}

		if ($(".mobile3").val() =="")
		{
			alert("전화번호를 입력해주세요")
			return false;
		}

		mobile1 = $(".mobile1").val()
		mobile2 = $(".mobile2").val()
		mobile3 = $(".mobile3").val()

		frm.b_mobile.value = mobile1+"-"+mobile2+"-"+mobile3
		<%
		end if
		%>

	   	frm.submit();
   }

$(document).ready(function () {
	$.datepicker.setDefaults($.datepicker.regional['ko']);

	$( "#b_addtext3" ).datepicker({
		changeMonth: true,  // 월을 바꿀수 있는 셀렉트 박스를 표시한다.
		changeYear: true,	// 년을 바꿀 수 있는 셀렉트 박스를 표시한다.
		showMonthAfterYear: true,	// 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다
		nextText: '다음 달',
		prevText: '이전 달',
		dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dateFormat: "yy-mm-dd",
//					minDate: 0,		// 선택할수있는 최소날짜, ( 0 : 오늘 이전 날짜 선택 불가)
		onClose: function( selectedDate ) {
			//시작일(startDate) datepicker가 닫힐때 종료일(endDate)의 선택할수있는 최소 날짜(minDate)를 선택한 시작일로 지정
			$("#b_addtext4").datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#b_addtext4" ).datepicker({
		changeMonth: true,
		changeYear: true,
		showMonthAfterYear: true,
		nextText: '다음 달',
		prevText: '이전 달',
		dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dateFormat: "yy-mm-dd",
//				onClose: function( selectedDate ) {
			// 종료일(endDate) datepicker가 닫힐때 시작일(startDate)의 선택할수있는 최대 날짜(maxDate)를 선택한 시작일로 지정
//					$("#sdate").datepicker( "option", "maxDate", selectedDate );
//				}

	});
});
</script>
<!-- <table width="100%" border="0" cellspacing="0" cellpadding="10">
	<tr>
		<td align="right">
			<a href="?<%=getString("vmode=list")%>"><img src=<%=Img_list%> border="0"></a>
		</td>
	</tr>
</table> -->
<%


	rs.close
	set rs = nothing
	DBclose

	'else

	'call jsAlertMsgUrl("게시글 수정 권한이 없습니다.", BackGetString("vmode=view"))

	end if
%>
