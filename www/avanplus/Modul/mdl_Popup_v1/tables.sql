CREATE TABLE [dbo].[mTb_PopupInfo] (
	[intPopupSeq] [int] IDENTITY (1, 1) NOT NULL ,
	[dtmStartDate] [smalldatetime] NOT NULL ,
	[dtmEndDate] [smalldatetime] NOT NULL ,
	[intWidth] [smallint] NOT NULL ,
	[intHeight] [smallint] NOT NULL ,
	[blnCenter] [bit] NOT NULL ,
	[intTop] [smallint] NOT NULL ,
	[intLeft] [smallint] NOT NULL ,
	[blnScroll] [bit] NOT NULL ,
	[strTitle] [varchar] (100) COLLATE Korean_Wansung_CI_AS NOT NULL ,
	[intType] [tinyint] NOT NULL ,
	[intOpen] [tinyint] NOT NULL ,
	[blnUse] [bit] NOT NULL ,
	[strContent] [text] COLLATE Korean_Wansung_CI_AS NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[mTb_PopupInfo] WITH NOCHECK ADD 
	CONSTRAINT [PK_mTb_PopupInfo] PRIMARY KEY  CLUSTERED 
	(
		[intPopupSeq]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[mTb_PopupInfo] ADD 
	CONSTRAINT [DF_mTb_PopupInfo_intWidth] DEFAULT (0) FOR [intWidth],
	CONSTRAINT [DF_mTb_PopupInfo_intHeight] DEFAULT (0) FOR [intHeight],
	CONSTRAINT [DF_mTb_PopupInfo_blnCenter] DEFAULT (0) FOR [blnCenter],
	CONSTRAINT [DF_mTb_PopupInfo_intTop] DEFAULT (0) FOR [intTop],
	CONSTRAINT [DF_mTb_PopupInfo_intLeft] DEFAULT (0) FOR [intLeft],
	CONSTRAINT [DF_mTb_PopupInfo_blnScroll] DEFAULT (0) FOR [blnScroll],
	CONSTRAINT [DF_mTb_PopupInfo_strTitle] DEFAULT ('') FOR [strTitle],
	CONSTRAINT [DF_mTb_PopupInfo_intType] DEFAULT (0) FOR [intType],
	CONSTRAINT [DF_mTb_PopupInfo_intOpen] DEFAULT (0) FOR [intOpen],
	CONSTRAINT [DF_mTb_PopupInfo_blnUse] DEFAULT (0) FOR [blnUse],
	CONSTRAINT [DF_mTb_PopupInfo_strContent] DEFAULT ('') FOR [strContent]
GO

