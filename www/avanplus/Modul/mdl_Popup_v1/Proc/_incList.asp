<%

	strTableName = " mTb_PopupInfo " '// 테이블 명
	strSelectField = " intPopupSeq, dtmStartDate, dtmEndDate, intWidth, intHeight, blnCenter, intTop, intLeft, blnScroll, strTitle, intType, intOpen, blnUse, strContent " '// 선택해서 가져올 필드명
	strWhereBody = " intPopupSeq > 0  " '// 검색조건
	strOrderByIn = " intPopupSeq DESC  " '// 정렬조건
	strOrderByOut = " intPopupSeq ASC  " '// 정렬조건역순

	intPageSize = 15 '// 출력될 최대레코드 숫자
	intBlockSize = 10 '// 출력될 페이징 숫자

	intNowPage = Request.QueryString("intNowPage")

	strSQL = " Select Count(*) FROM " & strTableName & " WHERE " & strWhereBody

	intTotalCount = getAdoRsScalar(strSQL)
	intTotalPage = Ceiling(intTotalCount, intPageSize)

	If Int(intTotalPage) < Int(intNowPage) Or IsValue(intNowPage) = False Then 
		intNowPage = 1
	End If

	intSelectRecordNum = intPageSize
	intLastRecordNum = intTotalCount Mod intPageSize

	If Int(intTotalPage) = Int(intNowPage) And intTotalCount Mod intPageSize > 0 Then
		intSelectRecordNum = intLastRecordNum
	End If

	strSQL = "" _
	&	" SELECT " _
	&	"			* " _
	&	" FROM " _
	&	"			( " _
	&	"			SELECT TOP " & intSelectRecordNum _
	&	"					* " _
	&	"			FROM " _
	&	"					( " _
	&	"					SELECT Top " & intPageSize * intNowPage _
	&	"							" & strSelectField  _
	&	"					FROM " _ 
	&	"							" & strTableName  _
	&	"					WHERE " _ 
	&	"							" & strWhereBody _
	&	"					ORDER BY " _ 
	&	"							" & strOrderByIn & " " _
	&	"					) a " _ 
	&	"			ORDER BY " _
	&	"					" & strOrderByOut & " " _
	&	"			) b " _
	&	" ORDER BY " _
	&	"			" & strOrderByIn & " "

	arrListData = getAdoRsArray(strSQL)

%>