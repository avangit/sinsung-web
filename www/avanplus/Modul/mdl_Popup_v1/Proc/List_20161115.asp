<!--#include file="../_Header.asp"-->
<!--#include file="_incList.asp"-->


<style type="text/css">
<!--

#popup_listContent {  }

#popup_listContent .listHeader { text-align:center; height:30px; background-color:#E4E4E4; border-width:2px 0px 1px; border-style:solid; border-color:#CCCCCC; font-weight:bold; font-size:12px; color:#666666; }
#popup_listContent .noRecordMsg { text-align:center; border-bottom:1px solid #CCCCCC; height:300px; font-weight:bold; font-size:12px; color:#666666; }
#popup_listContent .listContent { text-align:center; height:30px; border-bottom:1px solid #CCCCCC; font-size:12px; color:#666666; }
#popup_listContent .noUse { background-color:#F3F3F3; }

#popup_listPaging { text-align:center; padding-top:10px; }
#popup_listButton { text-align:right; padding:10px;  }

-->
</style>



	
	<table class="tableList center_table" cellpadding="0" cellspacing="0">
        <colgroup>    
            <col style="width:3%">
            <col style="width:20%">
            <col style="width:10%">
            <col style="width:6%">
            <col style="width:6%">
        </colgroup>
		<tr>
			<th>번호</th>
			<th>팝업 기간</th>
			<th>타이틀</th>
			<th>사용유무</th>
			<th>비고</th>
		</tr>
		<%
			'// 글이 없을 경우
			If Not IsArray(arrListData) Then
		%>
		<tr>
			<td colspan="5" class="noRecordMsg">데이타가 존재하지 않습니다.</td>
		</tr>
		<%
			'// 글이 있는 경우
			Else

				intNum = intTotalCount - ( ( intNowPage - 1 ) * intPageSize )
				For intRowNum = 0 To UBound(arrListData, 2)
					intColumnNum = 0

					intPopupSeq = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					dtmStartDate = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					dtmEndDate = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					intWidth = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					intHeight = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					blnCenter = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					intTop = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					intLeft = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					blnScroll = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					strTitle = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					intType = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					intOpen = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					blnUse = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1
					strContent = arrListData(intColumnNum, intRowNum) : intColumnNum = intColumnNum + 1

					If blnUse = True Then
						strTrClass = ""
						blnUse = "사용함"
					Else
						strTrClass = " class='noUse'"
						blnUse = "사용안함"
					End If
		%>
		<tr<%=strTrClass%>>
			<td class="listContent"><%=intNum%></td>
			<td class="listContent">
				<a href="javascript:popupOpen('<%=intPopupSeq%>', '<%=intWidth%>', '<%=intHeight%>', '<%=intLeft%>', '<%=intTop%>', '<%=blnScroll%>', '<%=blnCenter%>');">
				<%=dtmStartDate%> ~ <%=dtmEndDate%>
				</a>
			</td>
			<td class="listContent" style="text-align:left;">
				<a href="javascript:popupOpen('<%=intPopupSeq%>', '<%=intWidth%>', '<%=intHeight%>', '<%=intLeft%>', '<%=intTop%>', '<%=blnScroll%>', '<%=blnCenter%>');">
				&nbsp;&nbsp;&nbsp;&nbsp;<%=left(strTitle,30)%>
				</a>
			</td>
			<td class="listContent"><%=blnUse%></td>
			<td class="listContent">
				<input type="button" value=" 수정 " class="btnDB btnS" onclick="self.location.href = '?<%=getString("strPopupMode=Update&intPopupSeq="&intPopupSeq)%>';" >
				<input type="button" value=" 삭제 " class="btnDB btnS c_gray" onclick="self.location.href = '?<%=getString("strPopupMode=Delete&intPopupSeq="&intPopupSeq)%>';" >
			</td>
		</tr>
		<%			
					intNum = intNum - 1
				Next
			End If
		%>
	</table>


<div id="popup_listPaging">
	<%Call Paging3(intTotalPage, intBlockSize, intNowPage)%>
</div>

<div id="popup_listButton">
	<input type="button" value=" 입력 " class="btnDB btnS" onclick="self.location.href = '?<%=getString("strPopupMode=Insert")%>';" >
</div>

<script language="javascript">
<!--
	function popupOpen(seq, wWidth, wHeight, leftPos, topPos, scrollYes, centerYes){
		var popupoption = "";
		popupoption += ("width=" + wWidth);
		popupoption += (", height=" + wHeight);
		
		if (centerYes.toLowerCase() == "true"){
			var cleftPos = (screen.width) ? (screen.width-wWidth)/2 : 0;
			var ctopPos = (screen.height) ? (screen.height-wHeight)/2 : 0;
			popupoption += (", left=" + cleftPos);
			popupoption += (", top=" + ctopPos);
		} else {
			if ( leftPos != "") popupoption += ("left=" + leftPos);
			if ( topPos != "") popupoption += ("top=" + topPos);
		}
		
		if (scrollYes.toLowerCase() == "true"){
			var wScroll = 1;
		} else {
			var wScroll = 0;
		}
		popupoption += (", scrollbars=" + wScroll);
		window.open('/AVANplus/Modul/mdl_Popup_v1/popupin.asp?intPopupSeq=' + seq, '', popupoption);
	}
//-->
</script>

<!--#include file="../_Footer.asp"-->