<%
	If IsValue(intPopupSeq) = False Then
		strFormMode = "InsertProc"
	Else
		strFormMode = "UpdateProc"
	End If
%>

<script language="javascript">
<!--

	function frmSubmit(frm){
		if ( frm.dtmStartDate1.value.replace(/ /gi, '') == '' ) { alert('팝업시작일을 입력해주세요'); frm.dtmStartDate1.focus(); return false; }
		if ( frm.dtmStartDate2.value.replace(/ /gi, '') == '' ) { alert('팝업시작일을 입력해주세요'); frm.dtmStartDate2.focus(); return false; }
		if ( frm.dtmStartDate3.value.replace(/ /gi, '') == '' ) { alert('팝업시작일을 입력해주세요'); frm.dtmStartDate3.focus(); return false; }

		if ( isNaN(frm.dtmStartDate2.value) == true ) { alert("팝업시작일은 숫자만 가능합니다."); frm.dtmStartDate2.value = ""; frm.dtmStartDate2.focus(); return false; }
		if ( parseInt(frm.dtmStartDate2.value) >= 24 ) { alert("팝업시작일은 23이하로 입력해주세요"); frm.dtmStartDate2.value = ""; frm.dtmStartDate2.focus(); return false; }

		if ( isNaN(frm.dtmStartDate3.value) == true ) { alert("팝업시작일은 숫자만 가능합니다."); frm.dtmStartDate2.value = ""; frm.dtmStartDate2.focus(); return false; }
		if ( parseInt(frm.dtmStartDate3.value) >= 60 ) { alert("팝업시작일은 59이하로 입력해주세요"); frm.dtmStartDate3.value = ""; frm.dtmStartDate3.focus(); return false; }

		if ( frm.dtmEndDate1.value.replace(/ /gi, '') == '' ) { alert('팝업종료일을 입력해주세요'); frm.dtmEndDate1.focus(); return false; }
		if ( frm.dtmEndDate2.value.replace(/ /gi, '') == '' ) { alert('팝업종료일을 입력해주세요'); frm.dtmEndDate2.focus(); return false; }
		if ( frm.dtmEndDate3.value.replace(/ /gi, '') == '' ) { alert('팝업종료일을 입력해주세요'); frm.dtmEndDate3.focus(); return false; }

		if ( isNaN(frm.dtmEndDate2.value) == true ) { alert("팝업시작일은 숫자만 가능합니다."); frm.dtmEndDate2.value = ""; frm.dtmEndDate2.focus(); return false; }
		if ( parseInt(frm.dtmEndDate2.value) >= 24 ) { alert("팝업시작일은 23이하로 입력해주세요"); frm.dtmEndDate2.value = ""; frm.dtmEndDate2.focus(); return false; }

		if ( isNaN(frm.dtmEndDate3.value) == true ) { alert("팝업시작일은 숫자만 가능합니다."); frm.dtmEndDate2.value = ""; frm.dtmEndDate2.focus(); return false; }
		if ( parseInt(frm.dtmEndDate3.value) >= 60 ) { alert("팝업시작일은 59이하로 입력해주세요"); frm.dtmEndDate3.value = ""; frm.dtmEndDate3.focus(); return false; }

		if ( frm.intWidth.value.replace(/ /gi, '') == '' ) { alert('팝업크기를 입력해주세요'); frm.intWidth.focus(); return false; }
		if ( isNaN(frm.intWidth.value) == true ) { alert("팝업크기는 숫자만 가능합니다."); frm.intWidth.value = ""; frm.intWidth.focus(); return false; }
		if ( parseInt(frm.intWidth.value) == 0 ) { alert("팝업크기는 0이상으로 입력해주세요"); frm.intWidth.value = ""; frm.intWidth.focus(); return false; }

		if ( frm.intHeight.value.replace(/ /gi, '') == '' ) { alert('팝업크기를 입력해주세요'); frm.intHeight.focus(); return false; }
		if ( isNaN(frm.intHeight.value) == true ) { alert("팝업크기는 숫자만 가능합니다."); frm.intHeight.value = ""; frm.intHeight.focus(); return false; }
		if ( parseInt(frm.intHeight.value) == 0 ) { alert("팝업크기는 0이상으로 입력해주세요"); frm.intHeight.value = ""; frm.intHeight.focus(); return false; }

		if ( frm.blnCenter.checked == true ) {
			frm.intTop.value = "";
			frm.intLeft.value = "";
		} else {
			if ( frm.intTop.value.replace(/ /gi, '') == '' ) {
				if ( isNaN(frm.intTop.value) == true ) { alert("팝업위치는 숫자만 가능합니다."); frm.intTop.value = ""; frm.intTop.focus(); return false; }
			}
			if ( frm.intLeft.value.replace(/ /gi, '') == '' ) {
				if ( isNaN(frm.intLeft.value) == true ) { alert("팝업위치는 숫자만 가능합니다."); frm.intLeft.value = ""; frm.intLeft.focus(); return false; }
			}
		}

		if ( frm.strTitle.value.replace(/ /gi, '') == '' ) { alert('팝업타이틀을 입력해주세요'); frm.strTitle.focus(); return false; }

		//if ( frm.strContent.value.replace(/ /gi, '') == '' ) { alert('팝업내용을 입력해주세요'); return false; }

		frm.submit();
	}

//-->
</script>




<form name="frmPopUp" method="post" onsubmit="return frmSubmit(this);" action="?<%=getString("strPopupMode=" & strFormMode)%>">



	<input type="hidden" name="intPopupSeq" value="<%=intPopupSeq%>">


	<table class="tableList" cellpadding="0" cellspacing="0">

    <colgroup>
          <col style="width:15%">
          <col style="width:""">
    </colgroup>
		<tr>
			<th>팝업시작일</th>
			<td class="tleft pl20">
				<input type="text" name="dtmStartDate1" class="input01 w20 datepicker" value="<%=dtmStartDate1%>" size="10" maxlength="10"  readonly>
				<input type="text" name="dtmStartDate2" class="input01 w5" value="<%=dtmStartDate2%>" size="2" maxlength="2" onkeydown="return showKeyCode(event)">시
				<input type="text" name="dtmStartDate3" class="input01 w5" value="<%=dtmStartDate3%>" size="2" maxlength="2" onkeydown="return showKeyCode(event)">분
			</td>
		</tr>
		<tr>
			<th>팝업종료일</th>
			<td class="tleft pl20">
				<input type="text" name="dtmEndDate1" class="input01 w20 datepicker" value="<%=dtmEndDate1%>" size="10" maxlength="10" readonly>
				<input type="text" name="dtmEndDate2"  class="input01 w5" value="<%=dtmEndDate2%>" size="2" maxlength="2" onkeydown="return showKeyCode(event)">시
				<input type="text" name="dtmEndDate3" class="input01 w5"  value="<%=dtmEndDate3%>" size="2" maxlength="2" onkeydown="return showKeyCode(event)">분
			</td>
		</tr>
		<tr>
			<th>팝업크기</th>
			<td class="tleft pl20">
				가로 :  <input type="text" name="intWidth" class="input01 w5" value="<%=intWidth%>" size="5" maxlength="4" onkeydown="return showKeyCode(event)"> px
				*
				세로 : <input type="text" name="intHeight" class="input01 w5" value="<%=intHeight%>" size="5" maxlength="4" onkeydown="return showKeyCode(event)"> px
				<br>
				(세로크기가 출력시 자동으로 20px 늘어납니다.)
			</td>
		</tr>
		<tr>
			<th>팝업위치</th>
			<td class="tleft pl20">
				화면중앙 : <input type="checkbox" name="blnCenter" value="1"<%If blnCenter = True Then Response.Write " checked"%> class="brnone"> (화면 정중앙에 출력됩니다.)
				<br>
				지정위치 :
				위에서 <input type="text" name="intTop" class="input01 w5" value="<%=intTop%>" size="5" maxlength="4" onkeydown="return showKeyCode(event)"> px
				왼쪽에서 <input type="text" name="intLeft" class="input01 w5" value="<%=intLeft%>" size="5" maxlength="4" onkeydown="return showKeyCode(event)"> px
			</td>
		</tr>
		<tr>
			<th>팝업기간</th>
			<td class="tleft pl20">
				<input type="radio" name="intOpen" value="1"<%If intOpen = "1" Or IsValue(intOpen) = False Then Response.Write " checked"%> class="brnone"> 하루동안 띄우지 않기
				<br>
				<input type="radio" name="intOpen" value="2"<%If intOpen = "2" Then Response.Write " checked"%> class="brnone"> 더이상 띄우지 않기
				<br>
				<input type="radio" name="intOpen" value="0"<%If intOpen = "0" Then Response.Write " checked"%> class="brnone"> 무조건 띄우기
			</td>
		</tr>
		<tr>
		  <th>팝업방식</th>
		  <td class="tleft pl20"><input type="radio" name="intType" value="0"<%If intType = "0" Or IsValue(intType) = False Then Response.Write " checked"%> class="brnone">
새로운 브라우저방식
  <input type="radio" name="intType" value="1"<%If intType = "1" Then Response.Write " checked"%> class="brnone">
페이지내 레이어방식 </td>
	  </tr>
		<tr>
			<th>스크롤유무</th>
			<td class="tleft pl20">
				<input type="checkbox" name="blnScroll" value="1"<%If blnScroll = True Then Response.Write " checked"%> class="brnone"> 스크롤기능을 사용합니다.
			</td>
		</tr>
		<tr>
			<th>팝업사용유무</th>
			<td class="tleft pl20">
				<input type="radio" name="blnUse" value="0"<%If blnUse = False Then Response.Write " checked"%> class="brnone"> 팝업을 띄우지 않습니다.
				<input type="radio" name="blnUse" value="1"<%If blnUse = True Then Response.Write " checked"%> class="brnone"> 팝업을 띄웁니다.
			</td>
		</tr>
		<tr>
			<th>팝업타이틀</th>
			<td class="pr20 pl20">
				<input type="text" name="strTitle" class="input01 w100" value="<%=strTitle%>" size="50" maxlength="50">
				<br>
				(브라우저의 타이틀에 출력될 내용을 입력해주세요)
			</td>
		</tr>
		<tr>
			<th>팝업내용</th>
			<td class="formContent">
				<%Call Editor("strContent", strContent)%>
			</td>
		</tr>
	</table>



<div class="btnWrap">
	<input type="button" value=" 저장 " onclick="this.form.onsubmit();" class="btnDB btnS">
	<input type="button" value=" 목록 " onclick="self.location.href = '?<%=getString("strPopupMode=List")%>';"   class="btnDB btnS c_gray">
</div>

</form>




























































<script language="javascript">
<!--

var target;																	// 호출한 Object의 저장
var stime;
document.write("<div id=minical oncontextmenu='return false' ondragstart='return false' onselectstart='return false' style=\"background:buttonface; margin:5; padding:5;margin-top:2;border-top:1 solid buttonshadow;border-left: 1 solid buttonshadow;border-right: 1 solid buttonshadow;border-bottom:1 solid buttonshadow;width:160;display:none;position: absolute; z-index: 99\"></div>");

function Calendar(obj) {														// jucke
	var now = obj.value.split("-");
	var x, y;

	target = obj;																// Object 저장;

	x = (document.layers) ? loc.pageX : event.clientX;
	y = (document.layers) ? loc.pageY : event.clientY;

	minical.style.pixelTop	= y+5 + document.body.scrollTop;
	minical.style.pixelLeft	= x-50 + document.body.scrollLeft;
	if ( minical.style.display == "block" ) {
		minical.style.display = "none";
		selectbox_visible();

	} else {
		minical.style.display = "block";
		selectbox_hidden("minical");

	}

	if (now.length == 3) {														// 정확한지 검사
		Show_cal(now[0],now[1],now[2]);											// 넘어온 값을 년월일로 분리
	} else {
		now = new Date();
		Show_cal(now.getFullYear(), now.getMonth()+1, now.getDate());			// 현재 년/월/일을 설정하여 넘김.
	}


}

function doOver() {																// 마우스가 칼렌다위에 있으면
	var el = window.event.srcElement;
	cal_Day = el.title;

	if (cal_Day.length > 7) {													// 날자 값이 있으면.
		el.style.borderTopColor = el.style.borderLeftColor = "buttonhighlight";
		el.style.borderRightColor = el.style.borderBottomColor = "buttonshadow";
	}
	window.clearTimeout(stime);													// Clear
}

function doClick() {															// 날자를 선택하였을 경우
	cal_Day = window.event.srcElement.title;
	window.event.srcElement.style.borderColor = "red";							// 테두리 색을 빨간색으로
	if (cal_Day.length > 7) {													// 날자 값이있으면
		target.value=cal_Day													// 값 설정
	}
	minical.style.display='none';												// 화면에서 지움

	selectbox_visible();

}

function doOut() {
	var el = window.event.fromElement;
	cal_Day = el.title;

	if (cal_Day.length > 7) {
		el.style.borderColor = "white";
	}
	//stime=window.setTimeout("minical.style.display='none';", 200);
}

function day2(d) {																// 2자리 숫자료 변경
	var str = new String();

	if (parseInt(d) < 10) {
		str = "0" + parseInt(d);
	} else {
		str = "" + parseInt(d);
	}
	return str;
}

function Show_cal(sYear, sMonth, sDay) {
	var Months_day = new Array(0,31,28,31,30,31,30,31,31,30,31,30,31)
	var Weekday_name = new Array("일", "월", "화", "수", "목", "금", "토");
	var intThisYear = new Number(), intThisMonth = new Number(), intThisDay = new Number();
	document.all.minical.innerHTML = "";
	datToday = new Date();													// 현재 날자 설정

	intThisYear  = parseInt(sYear);
	intThisMonth = parseInt(sMonth);
	intThisDay   = parseInt(sDay);

	intThisMonth = __getNumWithoutZero( sMonth );
	intThisDay   = __getNumWithoutZero( sDay );

	if (intThisYear == 0) intThisYear = datToday.getFullYear();				// 값이 없을 경우
	if (intThisMonth == 0) intThisMonth = parseInt(datToday.getMonth())+1;	// 월 값은 실제값 보다 -1 한 값이 돼돌려 진다.
	if (intThisDay == 0) intThisDay = datToday.getDate();

	switch(intThisMonth) {
		case 1:
				intPrevYear = intThisYear -1;
				intPrevMonth = 12;
				intNextYear = intThisYear;
				intNextMonth = 2;
				break;
		case 12:
				intPrevYear = intThisYear;
				intPrevMonth = 11;
				intNextYear = intThisYear + 1;
				intNextMonth = 1;
				break;
		default:
				intPrevYear = intThisYear;
				intPrevMonth = parseInt(intThisMonth) - 1;
				intNextYear = intThisYear;
				intNextMonth = parseInt(intThisMonth) + 1;
				break;
	}

	NowThisYear = datToday.getFullYear();										// 현재 년
	NowThisMonth = datToday.getMonth()+1;										// 현재 월
	NowThisDay = datToday.getDate();											// 현재 일

	datFirstDay = new Date(intThisYear, intThisMonth-1, 1);						// 현재 달의 1일로 날자 객체 생성(월은 0부터 11까지의 정수(1월부터 12월))
	intFirstWeekday = datFirstDay.getDay();										// 현재 달 1일의 요일을 구함 (0:일요일, 1:월요일)

	intSecondWeekday = intFirstWeekday;
	intThirdWeekday = intFirstWeekday;

	datThisDay = new Date(intThisYear, intThisMonth, intThisDay);				// 넘어온 값의 날자 생성
	intThisWeekday = datThisDay.getDay();										// 넘어온 날자의 주 요일

	varThisWeekday = Weekday_name[intThisWeekday];								// 현재 요일 저장

	intPrintDay = 1																// 달의 시작 일자
	secondPrintDay = 1
	thirdPrintDay = 1

	Stop_Flag = 0

	if ((intThisYear % 4)==0) {													// 4년마다 1번이면 (사로나누어 떨어지면)
		if ((intThisYear % 100) == 0) {
			if ((intThisYear % 400) == 0) {
				Months_day[2] = 29;
			}
		} else {
			Months_day[2] = 29;
		}
	}
	intLastDay = Months_day[intThisMonth];										// 마지막 일자 구함
	Stop_flag = 0

	Cal_HTML = "<TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0 ONMOUSEOVER=doOver(); ONMOUSEOUT=doOut(); STYLE='font-size:8pt;font-family:Tahoma;'>"
			+ "<TR ALIGN=CENTER><TD COLSPAN=7 nowrap=nowrap ALIGN=CENTER><SPAN TITLE='이전달' STYLE=cursor:hand; onClick='Show_cal("+intPrevYear+","+intPrevMonth+",1);'><FONT COLOR=Navy>◀</FONT></SPAN> "
			+ "<B STYLE=color:red>"+get_Yearinfo(intThisYear,intThisMonth,intThisDay)+"년"+get_Monthinfo(intThisYear,intThisMonth,intThisDay)+"월</B>"
			+ " <SPAN TITLE='다음달' STYLE=cursor:hand; onClick='Show_cal("+intNextYear+","+intNextMonth+",1);'><FONT COLOR=Navy>▶</FONT></SPAN></TD></TR>"
			+ "<TR ALIGN=CENTER BGCOLOR=ThreedFace STYLE='color:#555555;font-weight:bold;'><TD>일</TD><TD>월</TD><TD>화</TD><TD>수</TD><TD>목</TD><TD>금</TD><TD>토</TD></TR>";

	for (intLoopWeek=1; intLoopWeek < 7; intLoopWeek++) {						// 주단위 루프 시작, 최대 6주
		Cal_HTML += "<TR ALIGN=RIGHT BGCOLOR=WHITE>"
		for (intLoopDay=1; intLoopDay <= 7; intLoopDay++) {						// 요일단위 루프 시작, 일요일 부터
			if (intThirdWeekday > 0) {											// 첫주 시작일이 1보다 크면
				Cal_HTML += "<TD onClick=doClick();>";
				intThirdWeekday--;
			} else {
				if (thirdPrintDay > intLastDay) {								// 입력 날짝 월말보다 크다면
					Cal_HTML += "<TD onClick=doClick();>";
				} else {														// 입력날짜가 현재월에 해당 되면
					Cal_HTML += "<TD onClick=doClick(); title="+intThisYear+"-"+day2(intThisMonth).toString()+"-"+day2(thirdPrintDay).toString()+" STYLE=\"cursor:Hand;border:1px solid white;";
					if (intThisYear == NowThisYear && intThisMonth==NowThisMonth && thirdPrintDay==intThisDay) {
						Cal_HTML += "background-color:cyan;";
					}

					switch(intLoopDay) {
						case 1:													// 일요일이면 빨간 색으로
							Cal_HTML += "color:red;"
							break;
						case 7:
							Cal_HTML += "color:blue;"
							break;
						default:
							Cal_HTML += "color:black;"
							break;
					}

					Cal_HTML += "\">"+thirdPrintDay;

				}
				thirdPrintDay++;

				if (thirdPrintDay > intLastDay) {								// 만약 날짜 값이 월말 값보다 크면 루프문 탈출
					Stop_Flag = 1;
				}
			}
			Cal_HTML += "</TD>";
		}
		Cal_HTML += "</TR>";
		if (Stop_Flag==1) break;
	}
	Cal_HTML += "</TABLE>";

	document.all.minical.innerHTML = Cal_HTML;
}

function get_Yearinfo(year,month,day) {											// 년 정보를 콤보 박스로 표시

	//var min = parseInt(year) - 100;
	//var max = parseInt(year) + 10;
	var min = 1999;
	var max = parseInt(year) + 10;
	var i = new Number();
	var str = new String();

	str = "<SELECT onChange='Show_cal(this.value,"+month+","+day+");' ONMOUSEOVER=doOver();>";
	for (i=min; i<=max; i++) {
		if (i == parseInt(year)) {
			str += "<OPTION VALUE="+i+" selected ONMOUSEOVER=doOver();>"+i+"</OPTION>";
		} else {
			str += "<OPTION VALUE="+i+" ONMOUSEOVER=doOver();>"+i+"</OPTION>";
		}
	}
	str += "</SELECT>";
	return str;
}


function get_Monthinfo(year,month,day) {										// 월 정보를 콤보 박스로 표시
	var i = new Number();
	var str = new String();

	str = "<SELECT onChange='Show_cal("+year+",this.value,"+day+");' ONMOUSEOVER=doOver();>";
	for (i=1; i<=12; i++) {
		if (i == parseInt(month)) {
			str += "<OPTION VALUE="+i+" selected ONMOUSEOVER=doOver();>"+i+"</OPTION>";
		} else {
			str += "<OPTION VALUE="+i+" ONMOUSEOVER=doOver();>"+i+"</OPTION>";
		}
	}
	str += "</SELECT>";
	return str;
}

// ----------------------------------------------
// 숫자만 반환한다.
// ----------------------------------------------
function __getNumWithoutZero( szVal ) {
	var val = new String( szVal );

	if ( val.length < 1 )
		return parseInt( val );

	if ( val.charAt(0) == 0 ) {
		return parseInt(val.charAt(1));
	}

	return parseInt( val );
}

// Internet Explorer에서 셀렉트박스와 레이어가 겹칠시 레이어가 셀렉트 박스 뒤로 숨는 현상을 해결하는 함수
// 레이어가 셀렉트 박스를 침범하면 셀렉트 박스를 hidden 시킴
// 사용법 :
// <div id=LayerID style="display:none; position:absolute;" onpropertychange="selectbox_hidden('LayerID')">
function selectbox_hidden(layer_id)
{
	var ly = eval(layer_id);

	// 레이어 좌표
	var ly_left  = ly.offsetLeft;
	var ly_top    = ly.offsetTop;
	var ly_right  = ly.offsetLeft + ly.offsetWidth;
	var ly_bottom = ly.offsetTop + ly.offsetHeight;

	// 셀렉트박스의 좌표
	var el;

	for (i=0; i<document.forms.length; i++) {
		for (k=0; k<document.forms[i].length; k++) {
			el = document.forms[i].elements[k];
			if (el.type == "select-one") {
				var el_left = el_top = 0;
				var obj = el;
				if (obj.offsetParent) {
					while (obj.offsetParent) {
						el_left += obj.offsetLeft;
						el_top  += obj.offsetTop;
						obj = obj.offsetParent;
					}
				}
				el_left  += el.clientLeft;
				el_top    += el.clientTop;
				el_right  = el_left + el.clientWidth;
				el_bottom = el_top + el.clientHeight;

				// 좌표를 따져 레이어가 셀렉트 박스를 침범했으면 셀렉트 박스를 hidden 시킴
				if ( (el_left >= ly_left && el_top >= ly_top && el_left <= ly_right && el_top <= ly_bottom) ||
					(el_right >= ly_left && el_right <= ly_right && el_top >= ly_top && el_top <= ly_bottom) ||
					(el_left >= ly_left && el_bottom >= ly_top && el_right <= ly_right && el_bottom <= ly_bottom) ||
					(el_left >= ly_left && el_left <= ly_right && el_bottom >= ly_top && el_bottom <= ly_bottom) )
					el.style.visibility = 'hidden';
			}
		}
	}
}

// 감추어진 셀렉트 박스를 모두 보이게 함
function selectbox_visible()
{
	for (i=0; i<document.forms.length; i++) {
		for (k=0; k<document.forms[i].length; k++) {
			el = document.forms[i].elements[k];
			if (el.type == "select-one" && el.style.visibility == 'hidden')
				el.style.visibility = 'visible';
		}
	}
}

$(function() {
 $(".datepicker").datepicker({
	 dateFormat: 'yy-mm-dd',
	 changeMonth:'true',
	 changeYear:'true'
	 });
 })

 function showKeyCode(event) {
		 event = event || window.event;
		 var keyID = (event.which) ? event.which : event.keyCode;
			 if( ( keyID >=48 && keyID <= 57 ) || ( keyID >=96 && keyID <= 105 ) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 )
		 {
			 return;
		 }
		 else
		 {
			 return false;
		 }
	 }

//-->
</script>
<link rel="stylesheet" href="jquery-ui.css" media="screen" title="no title">
<script src="jquery-ui.js"></script>
