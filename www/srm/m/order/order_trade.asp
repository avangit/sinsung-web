<!-- #include virtual="/srm/usercheck.asp" -->
<%
goods_list		= SQL_Injection(Trim(Request.Form("goods_list")))
goods_price		= SQL_Injection(Trim(Request.Form("goods_price")))
goods_cnt		= SQL_Injection(Trim(Request.Form("goods_cnt")))
cost			= SQL_Injection(Trim(Request.Form("cost")))
price			= SQL_Injection(Trim(Request.Form("price")))
arr_g_name		= SQL_Injection(Trim(Request.Form("arr_g_name")))

o_company		= SQL_Injection(Trim(Request.Form("o_company")))
o_name			= SQL_Injection(Trim(Request.Form("o_name")))
o_department	= SQL_Injection(Trim(Request.Form("o_department")))
o_mobile1		= SQL_Injection(Trim(Request.Form("o_mobile1")))
o_mobile2		= SQL_Injection(Trim(Request.Form("o_mobile2")))
o_mobile3		= SQL_Injection(Trim(Request.Form("o_mobile3")))
o_email			= SQL_Injection(Trim(Request.Form("o_email")))
s_name			= SQL_Injection(Trim(Request.Form("s_name")))
s_phone1		= SQL_Injection(Trim(Request.Form("s_phone1")))
s_phone2		= SQL_Injection(Trim(Request.Form("s_phone2")))
s_phone3		= SQL_Injection(Trim(Request.Form("s_phone3")))
s_mobile1		= SQL_Injection(Trim(Request.Form("s_mobile1")))
s_mobile2		= SQL_Injection(Trim(Request.Form("s_mobile2")))
s_mobile3		= SQL_Injection(Trim(Request.Form("s_mobile3")))
s_email			= SQL_Injection(Trim(Request.Form("s_email")))
s_zip			= SQL_Injection(Trim(Request.Form("s_zip")))
s_addr1			= SQL_Injection(Trim(Request.Form("s_addr1")))
s_addr2			= SQL_Injection(Trim(Request.Form("s_addr2")))
s_request		= SQL_Injection(Trim(Request.Form("s_request")))
payment			= SQL_Injection(Trim(Request.Form("payment")))

o_mobile	= o_mobile1 & "-" & o_mobile2 & "-" & o_mobile3
s_phone		= s_phone1 & "-" & s_phone2 & "-" & s_phone3
s_mobile	= s_mobile1 & "-" & s_mobile2 & "-" & s_mobile3

If InStr(arr_g_name, ",") > 0 Then
	g_cnt = Ubound(Split(arr_g_name, ",")) + 1

	If g_cnt > 1 Then
		add_g_name = Split(arr_g_name, ",")(0)
		add_g_name2 = "외 " & g_cnt - 1 & "개"
	End If
Else
	add_g_name = arr_g_name
	g_cnt = 1
End If

If payment = "card" Then
	payment_txt = "카드결제"
End If

Call DbOpen()


	goodSQL = "SELECT g_name FROM GOODS WHERE g_idx = '" & g_idx & "'"
	Set goodRS = dbconn.execute(goodSQL)

If Not (goodRS.eof) Then
	EP_product_nm = goodRS("g_name")
End If

goodRS.close
Set goodRS = Nothing
%>
<!doctype html>
<html>

<head>
    <!-- #include virtual="/srm/m/_inc/head.asp" -->
    <!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script type="text/javascript" src="https://testpg.easypay.co.kr/webpay/EasypayCard_Web.js"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {
//            $("#A_Header .gnb ul.nav li:nth-child(2)").addClass("active");
        });


    </script>
	<script type="text/javascript">

		/* 입력 자동 Setting */
		function f_init()
		{
			var frm_pay = document.frm_pay;

			var today = new Date();
			var year  = today.getFullYear();
			var month = today.getMonth() + 1;
			var date  = today.getDate();
			var time  = today.getTime();

			if(parseInt(month) < 10)
			{
				month = "0" + month;
			}

			if(parseInt(date) < 10)
			{
				date = "0" + date;
			}


			/*--공통--*/
			frm_pay.EP_mall_id.value        = "T0001997";                              //가맹점 ID
			frm_pay.EP_mall_nm.value        = encodeURIComponent("주식회사신성씨앤에스");                    //가맹점명
			frm_pay.EP_order_no.value       = "ORDER_" + year + month + date + time;   //가맹점 주문번호
			frm_pay.EP_currency.value       = "00";                                    //통화코드 : 00-원
			frm_pay.EP_product_nm.value     = encodeURIComponent("<%=add_g_name&add_g_name2%>");                            //상품명
			frm_pay.EP_product_amt.value    = "<%=price%>";							 //상품금액
																					   //가맹점 return_url(윈도우 타입 선택 시, 분기)
			frm_pay.EP_lang_flag.value      = "KOR"                                    //언어: KOR / ENG
			frm_pay.EP_charset.value        = "UTF-8"                                 //가맹점 Charset: EUC-KR(default) / UTF-8
			frm_pay.EP_user_id.value        = "psj1988";                               //가맹점 고객 ID
			frm_pay.EP_memb_user_no.value   = "15123485756";                           //가맹점 고객 일련번호
			frm_pay.EP_user_nm.value        = encodeURIComponent("<%=o_name%>");                                //가맹점 고객명
			//frm_pay.EP_user_mail.value      = "kildong@kicc.co.kr";                    //가맹점 고객 이메일
			//frm_pay.EP_user_phone1.value    = "0221471111";                            //가맹점 고객 번호1
			//frm_pay.EP_user_phone2.value    = "01012345679";                           //가맹점 고객 번호2
			frm_pay.EP_user_addr.value      = encodeURIComponent("서울시 금천구 가산동");                  //가맹점 고객 주소
			frm_pay.EP_product_type.value   = "0";                                     //상품정보구분 : 0-실물, 1-서비스
			frm_pay.EP_product_expr.value   = "20501231";                              //서비스기간 : YYYYMMDD
			frm_pay.EP_return_url.value     = "http://sinsung.solutionhosting.co.kr/easypay/web/normal/order_res.asp";      // Return 받을 URL (HTTP부터 입력)


			/*--신용카드--*/
			frm_pay.EP_usedcard_code.value  = "";                                      //사용가능한 카드 LIST
			frm_pay.EP_quota.value          = "";                                      //할부개월

																					   //무이자 여부(Y/N) (select)
			frm_pay.EP_noinst_term.value    = "029-02:03";                             //무이자기간
																					   //카드사포인트 사용여부(select)
			frm_pay.EP_point_card.value     = "029-40";                                //포인트카드 LIST
																					   //조인코드(select)
																					   //국민 앱카드 사용(select)

			/*--가상계좌--*/
			frm_pay.EP_vacct_bank.value     = "";                                      //가상계좌 사용가능한 은행 LIST
			frm_pay.EP_vacct_end_date.value = "20591231";                              //입금 만료 날짜
			frm_pay.EP_vacct_end_time.value = "153025";                                //입금 만료 시간



		}

		/* 인증창 호출, 인증 요청 */
		function f_cert()
		{
			var frm_pay = document.frm_pay;

			/*  주문정보 확인 */
//			if( !frm_pay.EP_order_no.value )
//			{
//				alert("가맹점주문번호를 입력하세요!!");
//				frm_pay.EP_order_no.focus();
//				return;
//			}
//
//			if( !frm_pay.EP_product_amt.value )
//			{
//				alert("상품금액을 입력하세요!!");
//				frm_pay.EP_product_amt.focus();
//				return;
//			}

			/* 가맹점에서 원하는 인증창 호출 방법을 선택 */

			if( frm_pay.EP_window_type.value == "iframe" )
			{
				easypay_webpay(frm_pay,"/easypay/web/normal/iframe_req.asp","hiddenifr","0","0","iframe","");

//				if( frm_pay.EP_charset.value == "UTF-8" )
//				{
//					// encoding 된 값은 모두 decoding 필수.
//					frm_pay.EP_mall_nm.value        = decodeURIComponent( frm_pay.EP_mall_nm.value );
//					frm_pay.EP_product_nm.value     = decodeURIComponent( frm_pay.EP_product_nm.value );
//					frm_pay.EP_user_nm.value        = decodeURIComponent( frm_pay.EP_user_nm.value );
//					frm_pay.EP_user_addr.value      = decodeURIComponent( frm_pay.EP_user_addr.value );
//				}
			}
			else if( frm_pay.EP_window_type.value == "popup" )
			{
				easypay_webpay(frm_pay,"/easypay/web/normal/popup_req.asp","hiddenifr","300","300","popup","");

//				if( frm_pay.EP_charset.value == "UTF-8" )
//				{
//					// encoding 된 값은 모두 decoding 필수.
//					frm_pay.EP_mall_nm.value        = decodeURIComponent( frm_pay.EP_mall_nm.value );
//					frm_pay.EP_product_nm.value     = decodeURIComponent( frm_pay.EP_product_nm.value );
//					frm_pay.EP_user_nm.value        = decodeURIComponent( frm_pay.EP_user_nm.value );
//					frm_pay.EP_user_addr.value      = decodeURIComponent( frm_pay.EP_user_addr.value );
//				}
			}
		}

		function f_submit()
		{
			var frm_pay = document.frm_pay;

			frm_pay.target = "_self";
			frm_pay.action = "./order_card_ok.asp";
			frm_pay.submit();
		}

	</script>
</head>

<body onload="f_init();">
    <div id="A_Wrap">
        <div id="A_Header">
            <!-- #include virtual="/srm/m/_inc/header.asp" -->
        </div>
		<form name="frm_pay" method="post">

		<!--------------------------->
		<!-- ::: 공통 인증 요청 값 -->
		<!--------------------------->

		<input type="hidden" id="EP_mall_id"        name="EP_mall_id"           value="T0001997">         <!-- 가맹점명-->
		<input type="hidden" id="EP_mall_nm"        name="EP_mall_nm"           value="">         <!-- 가맹점명-->
		<input type="hidden" id="EP_order_no"       name="EP_order_no"          value="">         <!-- 가맹점명-->
		<input type="hidden" id="EP_product_nm"     name="EP_product_nm"        value="<%=EP_product_nm%>">         <!-- 가맹점명-->
		<input type="hidden" id="EP_product_amt"    name="EP_product_amt"       value="<%=price%>">         <!-- 가맹점명-->
		<input type="hidden" id="EP_window_type"    name="EP_window_type"       value="popup">    <!-- 가맹점명-->
		<input type="hidden" id="EP_currency"       name="EP_currency"          value="00">       <!-- 통화코드 // 00 : 원화-->
		<input type="hidden" id="EP_return_url"     name="EP_return_url"        value="http://sinsung.solutionhosting.co.kr/easypay/web/normal/order_res.asp">         <!-- 가맹점 CALLBACK URL // -->
		<input type="hidden" id="EP_ci_url"         name="EP_ci_url"            value="">         <!-- CI LOGO URL // -->
		<input type="hidden" id="EP_lang_flag"      name="EP_lang_flag"         value="">         <!-- 언어 // -->
		<input type="hidden" id="EP_charset"        name="EP_charset"           value="UTF-8">    <!-- 가맹점 CharSet // EUC-KR,UTF-8 사용시 대문자 이용-->
		<input type="hidden" id="EP_user_id"        name="EP_user_id"           value="">         <!-- 가맹점 고객ID // -->
		<input type="hidden" id="EP_memb_user_no"   name="EP_memb_user_no"      value="">         <!-- 가맹점 고객일련번호 // -->
		<input type="hidden" id="EP_user_nm"        name="EP_user_nm"           value="">         <!-- 가맹점 고객명 // -->
		<input type="hidden" id="EP_user_mail"      name="EP_user_mail"         value="<%=o_email%>">         <!-- 가맹점 고객 E-mail // -->
		<input type="hidden" id="EP_user_phone1"    name="EP_user_phone1"       value="<%=o_mobile%>">         <!-- 가맹점 고객 연락처1 // -->
		<input type="hidden" id="EP_user_phone2"    name="EP_user_phone2"       value="">         <!-- 가맹점 고객 연락처2 // -->
		<input type="hidden" id="EP_user_addr"      name="EP_user_addr"         value="">         <!-- 가맹점 고객 주소 // -->
		<input type="hidden" id="EP_user_define1"   name="EP_user_define1"      value="">         <!-- 가맹점 필드1 // -->
		<input type="hidden" id="EP_user_define2"   name="EP_user_define2"      value="">         <!-- 가맹점 필드2 // -->
		<input type="hidden" id="EP_user_define3"   name="EP_user_define3"      value="">         <!-- 가맹점 필드3 // -->
		<input type="hidden" id="EP_user_define4"   name="EP_user_define4"      value="">         <!-- 가맹점 필드4 // -->
		<input type="hidden" id="EP_user_define5"   name="EP_user_define5"      value="">         <!-- 가맹점 필드5 // -->
		<input type="hidden" id="EP_user_define6"   name="EP_user_define6"      value="">         <!-- 가맹점 필드6 // -->
		<input type="hidden" id="EP_product_type"   name="EP_product_type"      value="">         <!-- 상품정보구분 // -->
		<input type="hidden" id="EP_product_expr"   name="EP_product_expr"      value="">         <!-- 서비스 기간 // (YYYYMMDD) -->
		<input type="hidden" id="EP_diEP_cash_yn"   name="EP_diEP_cash_yn"      value="">         <!-- 현금영수증 화면표시여부 //미표시 : "N", 그외: DB조회 -->


		<!--------------------------->
		<!-- ::: 카드 인증 요청 값 -->
		<!--------------------------->

		<input type="hidden" id="EP_usedcard_code"      name="EP_usedcard_code"     value="">      <!-- 사용가능한 카드 LIST // FORMAT->카드코드:카드코드: ... :카드코드 EXAMPLE->029:027:031 // 빈값 : DB조회-->
		<input type="hidden" id="EP_quota"              name="EP_quota"             value="">      <!-- 할부개월 (카드코드-할부개월) -->
		<input type="hidden" id="EP_os_cert_flag"       name="EP_os_cert_flag"      value="2">     <!-- 해외안심클릭 사용여부(변경불가) // -->
		<input type="hidden" id="EP_noinst_flag"        name="EP_noinst_flag"       value="">      <!-- 무이자 여부 (Y/N) // -->
		<input type="hidden" id="EP_noinst_term"        name="EP_noinst_term"       value="">      <!-- 무이자 기간 (카드코드-더할할부개월) // -->
		<input type="hidden" id="EP_set_point_card_yn"  name="EP_set_point_card_yn" value="">      <!-- 카드사포인트 사용여부 (Y/N) // -->
		<input type="hidden" id="EP_point_card"         name="EP_point_card"        value="">      <!-- 포인트카드 LIST  // -->
		<input type="hidden" id="EP_join_cd"            name="EP_join_cd"           value="">      <!-- 조인코드 // -->
		<input type="hidden" id="EP_kmotion_useyn"      name="EP_kmotion_useyn"     value="Y">     <!-- 국민앱카드 사용유무 (Y/N)// -->

		<!------------------------------->
		<!-- ::: 가상계좌 인증 요청 값 -->
		<!------------------------------->

		<input type="hidden" id="EP_vacct_bank"         name="EP_vacct_bank"        value="">      <!-- 가상계좌 사용가능한 은행 LIST // -->
		<input type="hidden" id="EP_vacct_end_date"     name="EP_vacct_end_date"    value="">      <!-- 입금 만료 날짜 // -->
		<input type="hidden" id="EP_vacct_end_time"     name="EP_vacct_end_time"    value="">      <!-- 입금 만료 시간 // -->

		<!------------------------------->
		<!-- ::: 선불카드 인증 요청 값 -->
		<!------------------------------->

		<input type="hidden" id="EP_prepaid_cp"         name="EP_prepaid_cp"        value="">      <!-- 선불카드 CP // FORMAT->코드:코드: ... :코드 EXAMPLE->CCB:ECB // 빈값 : DB조회-->

		<!--------------------------------->
		<!-- ::: 인증응답용 인증 요청 값 -->
		<!--------------------------------->

		<input type="hidden" id="EP_res_cd"             name="EP_res_cd"            value="">      <!--  응답코드 // -->
		<input type="hidden" id="EP_res_msg"            name="EP_res_msg"           value="">      <!--  응답메세지 // -->
		<input type="hidden" id="EP_tr_cd"              name="EP_tr_cd"             value="">      <!--  결제창 요청구분 // -->
		<input type="hidden" id="EP_pay_type"       name="EP_pay_type"      value="11">      <!--  결제수단 // -->
		<input type="hidden" id="EP_ret_pay_type"       name="EP_ret_pay_type"      value="">      <!--  결제수단 // -->
		<input type="hidden" id="EP_ret_complex_yn"     name="EP_ret_complex_yn"    value="">      <!--  복합결제 여부 (Y/N) // -->
		<input type="hidden" id="EP_card_code"          name="EP_card_code"         value="">      <!--  카드코드 (ISP:KVP카드코드 MPI:카드코드) // -->
		<input type="hidden" id="EP_eci_code"           name="EP_eci_code"          value="">      <!--  MPI인 경우 ECI코드 // -->
		<input type="hidden" id="EP_card_req_type"      name="EP_card_req_type"     value="">      <!--  거래구분 // -->
		<input type="hidden" id="EP_save_useyn"         name="EP_save_useyn"        value="">      <!--  카드사 세이브 여부 (Y/N) // -->
		<input type="hidden" id="EP_trace_no"           name="EP_trace_no"          value="">      <!--  추적번호 // -->
		<input type="hidden" id="EP_sessionkey"         name="EP_sessionkey"        value="">      <!--  세션키 // -->
		<input type="hidden" id="EP_encrypt_data"       name="EP_encrypt_data"      value="">      <!--  암호화전문 // -->
		<input type="hidden" id="EP_spay_cp"            name="EP_spay_cp"           value="">      <!--  간편결제 CP 코드 // -->
		<input type="hidden" id="EP_card_prefix"        name="EP_card_prefix"       value="">      <!--  신용카드prefix // -->
		<input type="hidden" id="EP_card_no_7"          name="EP_card_no_7"         value="">      <!--  신용카드번호 앞7자리 // -->
        <div id="A_Container_Wrap">
            <div id="A_Container" class="main">
                <div id="sub_contents" class="order_finish">
                    <div class="inner">
                        <div class="sub_tit">
                            <h2>주문결제</h2>
                        </div>
                        <div class="progress_box">
                            <ul>
                                <li>1</li>
                                <li class="on">2</li>
                                <li>3</li>
                            </ul>
                        </div>
                        <table class="table table_type03">
                            <colgroup>
                                <col style="width:600px">
                                <col style="width:200px">
                                <col style="width:200px">

                            </colgroup>
                            <thead>
                                <tr>
                                    <th>상품정보</th>
                                    <th>상품금액</th>
                                    <th>수량</th>
                                </tr>
                            </thead>
                            <tbody>
<%
	If Request.Cookies("SRM_LEVEL") = 2 Then
		query_where = " AND c.mem_idx = " & intSeq
	ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
		query_where = " AND c.mem_idx = " & masterIDX
	End If

	If Request.Cookies("SRM_LEVEL") = 9 Then
		sql = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_simg, B.g_cnt FROM GOODS A INNER JOIN cart B ON A.g_idx = B.g_idx WHERE B.g_idx IN (" & goods_list & ") AND B.mem_id = '" & Request.Cookies("SRM_ID") & "' " & query_where & " ORDER BY b.c_idx DESC"
	Else
		sql = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_simg, B.g_cnt, ISNULL(C.mem_price, 0) AS mem_price FROM GOODS A INNER JOIN cart B ON A.g_idx = B.g_idx LEFT JOIN goods_srm_join C ON B.g_idx = C.g_idx WHERE B.g_idx IN (" & goods_list & ") AND B.mem_id = '" & Request.Cookies("SRM_ID") & "' " & query_where & " ORDER BY b.c_idx DESC"
	End If
	Set rs = Server.CreateObject("ADODB.RecordSet")

	rs.CursorType = 3
	rs.CursorLocation = 3
	rs.LockType = 3
	rs.Open sql, dbconn

	If Not (rs.eof) Then
		Totalcnt = 0
		Do While Not rs.eof
%>
                                <tr>
                                    <td>
                                        <div class="cart_div">
                                            <div class="img_box">
                                                <a href="#"><img src="/upload/goods/<%=rs("g_simg")%>"></a>
                                            </div>
                                            <div class="txt_box">
                                                <a href="#" class="pro_code">제품코드 : #<%=rs("g_idx")%></a>
                                                <p class="pro_name"><%=rs("g_name")%></p><br>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="price"><% If rs("mem_price") = 0 Then %><%=FormatNumber(rs("g_Money"),0)%><% Else %><%=FormatNumber(rs("mem_price"),0)%><% End If %>원</td>
                                    <td><%=rs("g_cnt")%>개</td>
                                </tr>
<%
			Totalcnt = Totalcnt + rs("g_cnt")
			rs.MoveNext
		Loop

		rs.MoveFirst
	End If
%>
                            </tbody>
                        </table>
<%

	If Not (rs.eof) Then
		i = 1
		Do While Not rs.eof
			If rs("mem_price") = 0 Then
				FinalPrice = rs("g_Money")
			Else
				FinalPrice = rs("mem_price")
			End If

			TotalPrice = TotalPrice + (FinalPrice * rs("g_cnt"))

			g_idx = rs("g_idx")

			If i = rs.RecordCount Then
				comma = ""
			Else
				comma = ","
			End If

			If rs("g_idx") <> "" Then
				arr_g_idx = arr_g_idx & rs("g_idx") & comma
			End If

			i = i + 1
			rs.MoveNext
		Loop

'		rs.MoveFirst
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
						<input type="hidden" name="arr_g_idx" value="<%=arr_g_idx%>">
						<input type="hidden" name="c_idx" value="<%=goods_list%>">
						<input type="hidden" name="g_price" value="<%=goods_price%>">
						<input type="hidden" name="arr_g_name" value="<%=escape(add_g_name&add_g_name2)%>">
						<input type="hidden" name="g_cnt" value="<%=goods_cnt%>">
						<input type="hidden" name="o_total_mount" value="<%=g_cnt%>">
						<input type="hidden" name="o_total_price" value="<%=TotalPrice%>">
						<input type="hidden" name="o_company" value="<%=escape(o_company)%>">
						<input type="hidden" name="o_name" value="<%=escape(o_name)%>">
						<input type="hidden" name="o_department" value="<%=escape(o_department)%>">
						<input type="hidden" name="o_mobile" value="<%=o_mobile%>">
						<input type="hidden" name="o_email" value="<%=o_email%>">
						<input type="hidden" name="s_name" value="<%=escape(s_name)%>">
						<input type="hidden" name="s_phone" value="<%=s_phone%>">
						<input type="hidden" name="s_mobile" value="<%=s_mobile%>">
						<input type="hidden" name="s_email" value="<%=s_email%>">
						<input type="hidden" name="s_zip" value="<%=s_zip%>">
						<input type="hidden" name="s_addr1" value="<%=escape(s_addr1)%>">
						<input type="hidden" name="s_addr2" value="<%=escape(s_addr2)%>">
						<input type="hidden" name="s_request" value="<%=escape(s_request)%>">
						<input type="hidden" name="o_payment" value="card">
                        <div class="cart_price_box2">
                            <div class="price">
                                <p>
                                    <span>구매금액</span>
                                    <%=FormatNumber(TotalPrice, 0)%>원
                                </p>
                                <img src="/srm/m/image/sub/icon_plus.png" alt="">
                                <p>
                                    <span>배송비</span>
                                    <%=FormatNumber(cost, 0)%>원
                                </p>
                                <img src="/srm/m/image/sub/icon_total.png" alt="">
                                <p>
                                    <span>총 결제금액</span>
                                    <%=FormatNumber(TotalPrice+cost, 0)%>원
                                </p>
                            </div>
                        </div>
                        <div class="order_middle">
                            <div class="section_tit">
                                <h4>주문자 정보</h4>
                            </div>
                            <table class="table table_type02">
                                <colgroup>
                                    <col width="200px">
                                    <col width="500px">
                                    <col width="200px">
                                    <col width="500px">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th>회사명</th>
                                        <td><%=o_company%></td>
                                        <th>주문자</th>
                                        <td><%=o_name%></td>
                                    </tr>
                                    <tr>
                                        <th>주문부서</th>
                                        <td colspan="3"><%=o_department%></td>
                                    </tr>
                                    <tr>
                                        <th>휴대폰번호</th>
                                        <td><%=o_mobile%></td>
                                        <th>이메일 주소</th>
                                        <td><%=o_email%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="order_middle">
                            <div class="section_tit">
                                <h4>배송지 정보</h4>
                            </div>
                            <table class="table table_type02">
                                <colgroup>
                                    <col width="200px">
                                    <col width="500px">
                                    <col width="200px">
                                    <col width="500px">
                                </colgroup>
                                <tbody>
                                    <tr>
										<th>수령인</th>
										<td><%=s_name%></td>
										<th>연락처</th>
										<td><%=s_phone%></td>
                                    </tr>
                                    <tr>
										<th>이메일 주소</th>
										<td><%=s_email%></td>
										<th>휴대폰번호</th>
										<td><%=s_mobile%></td>
                                    </tr>
                                    <tr>
                                        <th>주소</th>
                                       <td colspan="3">[<%=s_zip%>] <%=s_addr1%>&nbsp;&nbsp;<%=s_addr2%></td>
                                    </tr>
                                    <tr>
                                        <th>배송 시 요청사항</th>
                                        <td colspan="3"><%=s_request%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="order_middle">
                            <div class="section_tit">
                                <h4>결제하기</h4>
                            </div>
                            <table class="table table_type02">
                                <colgroup>
                                    <col width="200px">
                                    <col width="500px">
                                    <col width="200px">
                                    <col width="500px">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th>결제방법선택</th>
                                        <td colspan="3"><%=payment_txt%></td>

                                    </tr>
                                    <tr>
										<th>구매금액</th>
										<td><%=FormatNumber(TotalPrice, 0)%>원</td>
										<th>배송비</th>
										<td><%=FormatNumber(cost, 0)%>원</td>
                                    </tr>
                                    <tr>
                                        <th>총결제금액</th>
                                       <td colspan="3"><%=FormatNumber(TotalPrice+cost, 0)%>원</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="order_btn">
                            <button class="btn_blue" onclick="javascript:f_cert();">결제하기</button>
                        </div>
                    </div>
                </div>
            </div>
			</form>
            <div id="A_Footer">
               <!-- #include virtual="/srm/m/_inc/footer.asp" -->
            </div>
        </div>

    </div>

</body>

</html>