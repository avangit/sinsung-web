<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="/srm/m/js/jquery.navgoco.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#sdate").datepicker({
				showOn: "both",
				buttonImageOnly: true,
				buttonImage: "/srm/m/image/common/ic_date.png",
				dateFormat: "yy-mm-dd"
			});
			$("#edate").datepicker({
				showOn: "both",
				buttonImageOnly: true,
				buttonImage: "/srm/m/image/common/ic_date.png",
				dateFormat: "yy-mm-dd"
			});
		});

		$(document).ready(function() {
			var posY;

			function bodyFreezeScroll() {
				posY = $(window).scrollTop();
				$("html").addClass('fix');
				$("html").css("top", -posY);
			}

			function bodyUnfreezeScroll() {
				$("html").removeAttr('class');
				$("html").removeAttr('style');
				posY = $(window).scrollTop(posY);
			}
			$('.thumb_img a, .video .video_area ul li a').click(function() {
				$('.popup').fadeIn();
				bodyFreezeScroll();

			});
			$('.popup .btn_close').click(function() {
				$('.popup').fadeOut();
				bodyUnfreezeScroll()

			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="pd0">
				<div id="sub_contents" class="serial order_list">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>주문배송조회</h2>
					</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim dayday 		: dayday 		=  SQL_Injection(Trim(Request("d")))
Dim sdate 		: sdate 		=  SQL_Injection(Trim(Request("sdate")))
Dim edate 		: edate 		=  SQL_Injection(Trim(Request("edate")))
Dim orderstep	: orderstep		=  SQL_Injection(Trim(Request("orderstep")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "orders"
Dim query_where			: query_where		= " o_payment <> 'yeosin'"
Dim query_orderby		: query_orderby		= " ORDER BY o_idx DESC"

If Request.Cookies("SRM_LEVEL") <> 9 Then
	query_where = query_where & " AND intSeq = " & intSeq
End If

If dayday = "" Then
'	sdate = ""
'	edate = ""
ElseIf dayday = "7" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -7, Date)
	edate = Date
ElseIf dayday = "30" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -30, Date)
	edate = Date
ElseIf dayday = "365" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -365, Date)
	edate = Date
End If

If sdate <> "" And edate <> "" Then
	query_where = query_where & " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day, 1, '" & edate & "')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where & " AND regdate >= '" & sdate & "'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where & " AND regdate <= DATEADD(day, 1, '" & edate & "')"
End If

If orderstep <> "" Then
	query_where = query_where &" AND o_status = '" & orderstep & "' "
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
					<div class="search_box2">
							<div class="tab_btn">
								<button onclick="location.href='./order_list2.asp'" <% If dayday = "" Then %>class="on"<% End If %>>전체</button>
								<button onclick="location.href='./order_list2.asp?d=7'" <% If dayday = "7" Then %>class="on"<% End If %>>1주일</button>
								<button onclick="location.href='./order_list2.asp?d=30'" <% If dayday = "30" Then %>class="on"<% End If %>>1개월</button>
								<button onclick="location.href='./order_list2.asp?d=365'" <% If dayday = "365" Then %>class="on"<% End If %>>1년</button>
							</div>
							<form name="sch_Form" method="post" action="">
							<div>
								<div class="date_box"><input type="text" id="sdate" name="sdate" value="<%=sdate%>" autocomplete="off">
								</div>

								<div class="date_box">
									<input type="text" id="edate" name="edate" value="<%=edate%>" autocomplete="off" class="date">
								</div>
							</div>
					</div>

					<div class="inner">
						<div class="search_box">
							<input type="text" name="fieldvalue" id="fieldvalue" value="<%=fieldvalue%>" placeholder="상품명을 검색해주세요">
							<button class="btn_search" onclick="document.sch_Form.submit();">검색</button>
						</div>
						</form>
						<form name="s_Form" method="post">
						<div class="middle_box">
							<p class="total_t">전체검색</p>
							<div class="a_option">
								<select name="orderstep" onchange="document.s_Form.submit();">
									<option value="">주문상태</option>
									<option value="1" <% If orderstep = "1" Then %> selected<% End If %>>결제대기</option>
									<option value="2" <% If orderstep = "2" Then %> selected<% End If %>>결제확인</option>
									<option value="3" <% If orderstep = "3" Then %> selected<% End If %>>상품배송</option>
									<option value="4" <% If orderstep = "4" Then %> selected<% End If %>>판매완료</option>
									<option value="91" <% If orderstep = "91" Then %> selected<% End If %>>취소</option>
									<option value="92" <% If orderstep = "92" Then %> selected<% End If %>>교환</option>
									<option value="93" <% If orderstep = "93" Then %> selected<% End If %>>반품</option>
								</select>
							</div>
						</div>
						</form>
						<div id="slide_tab">
<%
If rs.bof Or rs.eof Then
%>
								<div class="lnb">등록된 주문 내역이 없습니다.</div>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
		status = rs("o_status")

		If rs("o_status") = "1" Then
			status_txt = "결제대기"
		ElseIf rs("o_status") = "2" Then
			status_txt = "결제확인"
		ElseIf rs("o_status") = "3" Then
			status_txt = "상품배송"
		ElseIf rs("o_status") = "4" Then
			status_txt = "판매완료"
		ElseIf rs("o_status") = "91" Then
			status_txt = "취소"
		ElseIf rs("o_status") = "92" Then
			status_txt = "교환"
		ElseIf rs("o_status") = "93" Then
			status_txt = "반품"
		End If

		If InStr(rs("g_idx"), ",") > 0 Then
			totalCnt = Ubound(Split(rs("g_idx"), ",")) + 1
		Else
			totalCnt = 1
		End If
%>
							<div class="lnb">
								<ul class="nav">
									<li>
										<a href="./order_view2.asp?idx=<%=rs("o_idx")%>">
										<div class="box">
											<span><%=Left(rs("regdate"), 10)%></span>
											<span class="code"><%=rs("o_num")%></span>
										</div>
											<div class="pro_name">
												<div class="l_box">
													<p><%=totalCnt%>건</p>
													<p><%=FormatNumber(rs("o_total_price"), 0)%>원</p>
													<span class="custom custom2"><%=status_txt%></span>
												</div>
											</div>
										</a>
									</li>

								</ul>
							</div>
<%
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>

						</div>


						<% Call Paging_user_srm("") %>

					</div>
				</div>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>