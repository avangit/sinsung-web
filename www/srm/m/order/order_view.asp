<!-- #include virtual="/srm/usercheck.asp" -->
<%
	If Request.Cookies("SRM_LEVEL") < 9 Then
	'	Call jsAlertMsgBack("접근 권한이 없습니다.")
	End If

	sql = "SELECT * FROM Record_views3"
	Set rs = dbconn.execute(sql)

	If Not rs.eof Then
		cost = rs("cost")
	End If

	rs.close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container">
				<div id="sub_contents" class="order_view">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>구매 전자결재</h2>
					</div>
<%
	o_idx = SQL_Injection(Trim(Request.QueryString("idx")))

	If Request.Cookies("SRM_LEVEL") <> 9 Then
'		query_where = " AND strId = '" & Request.Cookies("SRM_ID") & "'"
	End If

	sql = "SELECT * FROM orders WHERE o_payment = 'yeosin' AND o_idx = " & o_idx & query_where
	Set rs = dbconn.execute(sql)

	If rs.eof Or rs.bof Then
		Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","/srm/")
	Else

		If InStr(rs("g_idx"), ",") > 0 Then
			arr_g_cnt = Split(rs("g_cnt"), ",")
			arr_g_price = Split(rs("g_price"), ",")
		Else
			arr_g_cnt = rs("g_cnt")
			arr_g_price = rs("g_price")
		End If
%>
					<div class="inner">

						<section class="order_section">

							<h5 class="section_tit">주문상품</h5>
							<div class="bg_wh">
<%
		strSQL = "SELECT g_idx,g_name,g_money,g_simg FROM GOODS WHERE g_idx IN (" & rs("g_idx") & ") ORDER BY g_idx DESC"
		Set rs2 = dbconn.execute(strSQL)

			i = 0
			Do While Not rs2.eof

				If rs("o_status") = "0" Then
					status_txt = "구매요청"
				ElseIf rs("o_status") = "15" Then
					status_txt = "구매반려"
				ElseIf rs("o_status") = "20" Then
					status_txt = "구매승인"
				End If
%>
								<div class="cart_div">
									<div class="img_box">
										<a href="#"><img src="/upload/goods/<%=rs2("g_simg")%>"></a>
									</div>
									<div class="txt_box">

										<p class="pro_name"><%=rs2("g_name")%></p>
										<a href="#" class="pro_code">제품코드 : #<%=rs2("g_idx")%></a>
									<% If InStr(rs("g_idx"), ",") > 0 Then %>
										<strong><%=FormatNumber(rs("o_total_price")-cost, 0)%>원</strong>
										<span><%=arr_g_cnt(i)%>개</span>
									<% Else %>
										<strong><%=FormatNumber(rs("o_total_price")-cost, 0)%>원</strong>
										<span><%=arr_g_cnt%>개</span>
									<% End If %>
									</div>
								</div>
<%
				i = i + 1
				rs2.MoveNext
			Loop

		rs2.close
		Set rs2 = Nothing
%>
								<!--div class="cart_div">
									<div class="img_box">
										<a href="#"><img src="/srm/m/image/sub/cart_img02.jpg"></a>
									</div>
									<div class="txt_box">

										<p class="pro_name">제품명이 노출됩니다</p>
										<a href="#" class="pro_code">제품코드 : #123456789</a>
										<strong>100,000원</strong>
										<span>1개</span>
									</div>
								</div-->
							</div>
							<div class="cart_div">
								<table class="m_table bg_none">

									<tbody>
										<tr>
											<th>상품금액</th>
											<td><%=FormatNumber(rs("o_total_price")-cost, 0)%>원</td>
										</tr>
										<tr>
											<th>배송비</th>
											<td><%=FormatNumber(cost, 0)%>원</td>
										</tr>

									</tbody>
								</table>
							</div>
							<div class="total">
								<strong>총 결제금액</strong>
								<strong class="price"><%=FormatNumber(rs("o_total_price"), 0)%><span>원</span></strong>
							</div>
						</section>



						<div class="order_section">
							<h5 class="section_tit">주문/결제정보</h5>

							<table class="m_table">
								<tbody>
									<tr>
										<th>주문상태</th>
										<td><%=status_txt%></td>
									</tr>
									<tr>
										<th>주문번호</th>
										<td><%=rs("o_num")%></td>
									</tr>
									<tr>
										<th>결제금액</th>
										<td><%=FormatNumber(rs("o_total_price"), 0)%>원</td>
									</tr>
									<tr>
										<th>주문일시</th>
										<td><%=rs("regdate")%></td>
									</tr>

								</tbody>
							</table>


						</div>

						<div class="order_section">
							<h5 class="section_tit">주문하시는 분</h5>

							<table class="m_table">

								<tbody>
									<tr>
										<th>회사명</th>
										<td><%=rs("o_company")%></td>
									</tr>
									<tr>
										<th>주문자</th>
										<td><%=rs("o_name")%></td>
									</tr>
									<tr>
										<th>주문부서</th>
										<td><%=rs("o_department")%></td>
									</tr>
									<tr>
										<th>휴대폰 번호</th>
										<td><%=rs("o_mobile")%></td>
									</tr>
									<tr>
										<th>이메일주소</th>
										<td><%=rs("o_email")%></td>
									</tr>

									<tr class="full">
										<th colspan="2">주소</th>
									</tr>
									<tr class="full">
										<td colspan="2">[<%=rs("s_zip")%>] <%=rs("s_addr1")%> <br><%=rs("s_addr2")%></td>
									</tr>
								</tbody>
							</table>

						</div>
						<div class="order_section">
							<h5 class="section_tit">받는 분</h5>
							<table class="m_table">

								<tbody>

									<tr>
										<th>수령인</th>
										<td><%=rs("s_name")%></td>
									</tr>
									<tr>
										<th>연락처</th>
										<td><%=rs("s_phone")%></td>
									</tr>
									<tr>
										<th>이메일주소</th>
										<td><%=rs("s_email")%></td>

									</tr>
									<tr>
										<th>휴대폰 번호</th>
										<td><%=rs("s_mobile")%></td>
									</tr>
									<tr class="full">
										<th colspan="2">주소</th>

									</tr>
									<tr class="full">
										<td colspan="2">[<%=rs("s_zip")%>] <%=rs("s_addr1")%> <br><%=rs("s_addr2")%></td>
									</tr>
									<tr class="full">
										<th colspan="2">배송 시 요청사항</th>

									</tr>
									<tr class="full">
										<td colspan="2"><%=rs("s_request")%></td>
									</tr>
								</tbody>
							</table>
						</div>



						<div class="btn_order_box btn_order_box2">
							<button class="btn_f6" onclick="location.href='order_list.asp'">주문취소</button>
							<button class="btn_navy" onclick="location.href='order_list.asp'">목록</button>
						</div>



					</div>
				</div>
			</div>
		</div>
<%
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
		<div id="A_Footer">
			<!-- #include virtual="/srm/m/_inc/footer.asp" -->
		</div>
	</div>


</body>

</html>