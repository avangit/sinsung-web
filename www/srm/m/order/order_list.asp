<!-- #include virtual="/srm/usercheck.asp" -->
<%
If Request.Cookies("SRM_LEVEL") < 9 Then
'	Call jsAlertMsgBack("접근 권한이 없습니다.")
End If
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="/srm/m/js/jquery.navgoco.js"></script>
	<script src="/srm/js/order.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#sdate").datepicker({
				showOn: "both",
				buttonImageOnly: true,
				buttonImage: "/srm/m/image/common/ic_date.png",
				dateFormat: "yy-mm-dd"
			});
			$("#edate").datepicker({
				showOn: "both",
				buttonImageOnly: true,
				buttonImage: "/srm/m/image/common/ic_date.png",
				dateFormat: "yy-mm-dd"
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="pd0">
				<div id="sub_contents" class="serial order_list">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>구매 전자결재</h2>
					</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim dayday 		: dayday 		=  SQL_Injection(Trim(Request("d")))
Dim sdate 		: sdate 		=  SQL_Injection(Trim(Request("sdate")))
Dim edate 		: edate 		=  SQL_Injection(Trim(Request("edate")))
Dim orderstep	: orderstep		=  SQL_Injection(Trim(Request("orderstep")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "orders"
Dim query_where			: query_where		= " o_payment = 'yeosin'"
Dim query_orderby		: query_orderby		= " ORDER BY o_idx DESC"

If dayday = "" Then
'	sdate = ""
'	edate = ""
ElseIf dayday = "7" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -7, Date)
	edate = Date
ElseIf dayday = "30" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -30, Date)
	edate = Date
ElseIf dayday = "365" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -365, Date)
	edate = Date
End If

If sdate <> "" And edate <> "" Then
	query_where = query_where & " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day, 1, '" & edate & "')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where & " AND regdate >= '" & sdate & "'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where & " AND regdate <= DATEADD(day, 1, '" & edate & "')"
End If

If orderstep <> "" Then
	query_where = query_where &" AND o_status = '" & orderstep & "' "
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
					<div class="search_box2">
							<div class="tab_btn">
								<button onclick="location.href='./order_list.asp'" <% If dayday = "" Then %> class="on"<% End If %>>전체</button>
								<button onclick="location.href='./order_list.asp?d=7'" <% If dayday = "7" Then %> class="on"<% End If %>>1주일</button>
								<button onclick="location.href='./order_list.asp?d=30'" <% If dayday = "30" Then %> class="on"<% End If %>>1개월</button>
								<button onclick="location.href='./order_list.asp?d=365'" <% If dayday = "365" Then %> class="on"<% End If %>>1년</button>
							</div>
							<form name="sch_Form" method="post" action="">
							<div>
								<div class="date_box"><input type="text" id="sdate" name="sdate" value="<%=sdate%>" autocomplete="off"></div>
								<div class="date_box"><input type="text" id="edate" name="edate" class="date" value="<%=edate%>" autocomplete="off"></div>
							</div>
					</div>

					<div class="inner">
						<div class="search_box">
							<input type="text" name="fieldvalue" id="fieldvalue" value="<%=fieldvalue%>" placeholder="상품명을 검색해주세요">
							<button class="btn_search" onclick="document.sch_Form.submit();">검색</button>
						</div>
						</form>
						<form name="orderForm0" method="post" action="./order0_status_ok.asp">
						<input type="hidden" name="n">
						<input type="hidden" name="m">
						<input type="hidden" name="message">
						<% If Request.Cookies("SRM_LEVEL") = 9 Then %>
						<div class="middle_box">
							<p class="total_t">전체검색</p>
							<div class="btn_small">
								<button class="btn_gray" onclick="javascript:GetCheckbox(document.orderForm0, 'chg15', '반려');return false;">반려</button>
								<button class="btn_blue" onclick="javascript:GetCheckbox(document.orderForm0, 'chg20', '승인');return false;">승인</button>
							</div>
						</div>
						<% End If %>


						<div id="slide_tab">
<%
If rs.bof Or rs.eof Then
%>
								<div class="lnb">등록된 전재결재 내역이 없습니다.</div>
<%
Else
	i = 1
	rs.move MoveCount
	Do While Not rs.eof

		If rs("o_status") = "0" Then
			status_txt = "구매요청"
		ElseIf rs("o_status") = "15" Then
			status_txt = "구매반려"
		ElseIf rs("o_status") = "20" Then
			status_txt = "구매승인"
		End If

		strSQL = "SELECT strName FROM mTb_Member2 WHERE intSeq = " & rs("intSeq")
		Set rs2 = dbconn.execute(strSQL)

		If Not(rs2.bof Or rs2.eof) Then
			order_name = rs2("strName")
		End If

		rs2.close
		Set rs2 = Nothing
%>
							<div class="lnb">
								<ul class="nav">
									<li>
										<a href="./order_view.asp?idx=<%=rs("o_idx")%>">
											<div class="chk_box">
												<input id="order_chk<%=i%>" type="checkbox" value="<%=rs("o_idx")%>">
												<label for="order_chk<%=i%>"><span></span></label>
											</div>
											<span><%=Left(rs("regdate"), 10)%></span>
											<span class="code"><%=rs("o_num")%></span>
											<div class="pro_name">
												<div class="l_box">
													<p><%=rs("o_total_mount")%>건</p>
													<p class="color9"><%=order_name%> / <%=rs("o_department")%></p>
													<strong class="price"><%=FormatNumber(rs("o_total_price"), 0)%><span>원</span></strong>

													<span class="custom"><%=status_txt%></span>
												</div>
											</div>
										</a>
									</li>

								</ul>
							</div>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
							<!--div class="lnb">
								<ul class="nav">
									<li>
										<a href="order_view.asp">
											<div class="chk_box">
												<input id="or02" type="checkbox">
												<label for="or02"><span></span></label>
											</div>
											<span>2020 .06. 19</span>
											<span class="code">1234567890</span>
											<div class="pro_name">
												<div class="l_box">
													<p>제품명 외 3건</p>
													<p class="color9">홍길동 / 부서A</p>
													<strong class="price">100,000<span>원</span></strong>

													<span class="custom">전자결재 반려</span>
												</div>
											</div>
										</a>
									</li>

								</ul>
							</div-->

						</div>
						</form>


						<% Call Paging_user_srm("") %>

					</div>
				</div>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>