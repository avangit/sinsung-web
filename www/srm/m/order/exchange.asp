<!-- #include virtual="/srm/usercheck.asp" -->
<%
	sql = "SELECT * FROM Record_views3"
	Set rs = dbconn.execute(sql)

	If Not rs.eof Then
		cost = rs("cost")
	End If

	rs.close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script src="/srm/js/order.js"></script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
	o_idx = SQL_Injection(Trim(Request.QueryString("idx")))

	sql = "SELECT * FROM orders WHERE o_idx = " & o_idx & " AND strId = '" & strId & "'"
	Set rs = dbconn.execute(sql)

	If rs.eof Or rs.bof Then
		Call jsAlertMsgBack("일치하는 정보가 없습니다.")
	Else
		If rs("o_status") = "1" Then
			status_txt = "결제대기"
		ElseIf rs("o_status") = "2" Then
			status_txt = "결제확인"
		End If

		If rs("o_payment") = "card" Then
			payment_txt = "카드결제"
		ElseIf rs("o_payment") = "account" Then
			payment_txt = "무통장입금"
		ElseIf rs("o_payment") = "yeosin" Then
			payment_txt = "여신결제"
		End If
	End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container">
				<div id="sub_contents" class="order_view return">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>교환요청</h2>
					</div>

					<div class="inner">

						<section class="order_section">

							<h5 class="section_tit">주문상품</h5>
							<div class="bd1">
<%
	If InStr(rs("g_idx"), ",") > 0 Then
		arr_g_idx = Split(rs("g_idx"), ",")
		arr_g_price = Split(rs("g_price"), ",")
		arr_g_cnt = Split(rs("g_cnt"), ",")

		NextArr = Ubound(arr_g_idx)
	Else
		arr_g_idx = rs("g_idx")
		arr_g_price = rs("g_price")
		arr_g_cnt = rs("g_cnt")

		NextArr = 0
	End If
		For i = 0 To NextArr
			If NextArr > 0 Then
				arr_idx = arr_g_idx(i)
				arr_price = arr_g_price(i)
				arr_cnt = arr_g_cnt(i)
			Else
				arr_idx = arr_g_idx
				arr_price = arr_g_price
				arr_cnt = arr_g_cnt
			End If
			sql2 = "SELECT g_name, g_simg FROM GOODS WHERE g_idx = '" & arr_idx & "'"
			Set rs_g_name = dbconn.execute(sql2)
%>
								<div class="cart_div">
									<div class="img_box">
										<a href="#"><img src="/upload/goods/<%=rs_g_name("g_simg")%>"></a>
									</div>
									<div class="txt_box">
										<p class="pro_name"><%=rs_g_name("g_name")%></p>
										<a href="#" class="pro_code">제품코드 : #<%=arr_idx%></a>
										<strong><%=FormatNumber(arr_price, 0)%>원</strong>
										<span><%=arr_cnt%>개</span>
									</div>
								</div>
<%
			rs_g_name.close
			Set rs_g_name = Nothing
		Next
%>
								<!--div class="cart_div">
									<div class="img_box">
										<a href="#"><img src="image/sub/cart_img02.jpg"></a>
									</div>
									<div class="txt_box">
								
										<p class="pro_name">제품명이 노출됩니다</p>
										<a href="#" class="pro_code">제품코드 : #123456789</a>
										<strong>100,000원</strong>
										<span>1개</span>
									</div>
								</div-->
							</div>
							<div class="cart_div">
								<table class="m_table">

									<tbody>
										<tr>
											<th>상품금액</th>
											<td><%=FormatNumber(rs("o_total_price")-cost, 0)%>원</td>
										</tr>
										<tr>
											<th>배송비</th>
											<td><%=FormatNumber(cost, 0)%>원</td>
										</tr>

									</tbody>
								</table>
							</div>
							<div class="total">
								<strong>총 결제금액</strong>
								<strong class="price"><%=FormatNumber(rs("o_total_price"), 0)%><span>원</span></strong>
							</div>
						</section>



						<div class="order_section">
							<h5 class="section_tit">주문/결제정보</h5>

							<table class="m_table">
								<tbody>
									<tr>
										<th>주문상태</th>
										<td><%=status_txt%></td>
									</tr>
									<tr>
										<th>주문번호</th>
										<td><%=rs("o_num")%></td>
									</tr>
									
									<tr>
										<th>주문일시</th>
										<td><%=rs("regdate")%></td>
									</tr>
									<tr>
										<th>결제금액</th>
										<td><%=FormatNumber(rs("o_total_price"), 0)%>원</td>
									</tr>
									<tr>
										<th>결제수단</th>
										<td><%=payment_txt%></td>
									</tr>

								</tbody>
							</table>


						</div>

						<div class="order_section">
							<h5 class="section_tit">교환요청</h5>
							<form name="exchangeForm" method="post" action="exchange_ok.asp">
							<input type="hidden" name="idx" value="<%=o_idx%>">
							<table class="m_table">

								<tbody>
									<tr class="full">
										<th colspan="2">교환요청 사유 <span>*</span></th>
										
									</tr>
									<tr class="full">
										<td colspan="2"><textarea name="reason" placeholder="교환요청 내용을 입력해주세요"></textarea></td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="btn_order_box btn_order_box2">
							<button class="btn_f6" onclick="location.href = 'order_list2.asp'">취소</button>
							<button class="btn_navy" onclick="exchange_write();return false;">확인</button>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div id="A_Footer">
			<!-- #include virtual="/srm/m/_inc/footer.asp" -->
		</div>
	</div>


</body>

</html>