<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
	o_idx = SQL_Injection(Trim(Request.QueryString("idx")))

	sql = "SELECT * FROM orders WHERE o_idx = '" & o_idx & "' AND strId = '" & strId & "'"
	Set rs = dbconn.execute(sql)

	If rs.eof Or rs.bof Then
		Call jsAlertMsgBack("일치하는 정보가 없습니다.")
	Else

		If rs("o_payment") = "card" Then
			o_payment = "카드결제"
		ElseIf rs("o_payment") = "account" Then
			o_payment = "무통장입금"
		ElseIf rs("o_payment") = "yeosin" Then
			o_payment = "여신결제"
		End If

		If InStr(rs("g_idx"), ",") > 0 Then
			totalCnt = Ubound(Split(rs("g_idx"), ",")) + 1
		Else
			totalCnt = 1
		End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container">
				<div id="sub_contents" class="order_finish">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>주문완료</h2>
					</div>

					<div class="inner">
						<div class="progress_box">
							<ul>
								<li>1</li>
								<li>2</li>
								<li class="on">3</li>
							</ul>
						</div>

						<section class="order_section">

							 <div class="order_top">
                            <img src="/srm/m/image/sub/icon_order_finish.png">
                            <p><!--strong>제품명</strong> 외--> <span><%=totalCnt%>건</span>의 주문이 정상적으로 접수되었습니다.</p>
                        </div>
						</section>

						<div class="order_section">
							<h5 class="section_tit">주문/결제정보</h5>

							<table class="m_table">
								<tbody>

									<tr>
										<th>주문번호</th>
										<td><%=rs("o_num")%></td>
									</tr>
									<tr>
										<th>주문일시</th>
										<td><%=Left(rs("regdate"), 10)%></td>
									</tr>
									<tr>
										<th>결제금액</th>
										<td><%=FormatNumber(rs("o_total_price"), 0)%>원</td>
									</tr>

									<tr>
										<th>결제수단</th>
										<td><%=o_payment%></td>
									</tr>

								</tbody>
							</table>


						</div>

						<div class="order_section">
							<h5 class="section_tit">주문자 정보</h5>

							<table class="m_table">

								<tbody>
									<tr>
										<th>회사명</th>
										<td><%=rs("o_company")%></td>
									</tr>
									<tr>
										<th>주문자</th>
										<td><%=rs("o_name")%></td>
									</tr>
									<tr>
										<th>주문부서</th>
										<td><%=rs("o_department")%></td>
									</tr>
									<tr>
										<th>휴대폰 번호</th>
										<td><%=rs("o_mobile")%></td>
									</tr>
									<tr>

										<th>이메일주소</th>
										<td><%=rs("o_email")%></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="order_section">
							<h5 class="section_tit">배송지 정보</h5>
							<table class="m_table">

								<tbody>
									<tr>
										<th>수령인</th>
										<td><%=rs("s_name")%></td>
									</tr>
									<tr>
										<th>연락처</th>
										<td><%=rs("s_phone")%></td>
									</tr>
									<tr>
										<th>이메일주소</th>
										<td><%=rs("s_email")%></td>

									</tr>
									<tr>
										<th>휴대폰 번호</th>
										<td><%=rs("s_mobile")%></td>
									</tr>
									<tr class="full">
										<th colspan="2">주소</th>

									</tr>
									<tr class="full">
										<td colspan="2">[<%=rs("s_zip")%>] <%=rs("s_addr1")%> <br>
											<%=rs("s_addr2")%></td>
									</tr>
									<tr class="full">
										<th colspan="2">배송 시 요청사항</th>

									</tr>
									<tr class="full">
										<td colspan="2"><%=rs("s_request")%></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="order_btn">
                            <button class="btn_wh" onclick="location.href='/srm/m/'">메인으로</button>
						<% If rs("o_payment") <> "yeosin" Then %>
                            <button class="btn_blue" onclick="location.href='order_list2.asp'">주문배송조회</button>
						<% End If %>
                        </div>
					</div>
				</div>
			</div>
		</div>
<%
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
		<div id="A_Footer">
			<!-- #include virtual="/srm/m/_inc/footer.asp" -->
		</div>
	</div>


</body>

</html>