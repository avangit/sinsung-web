<!-- #include virtual="/srm/usercheck.asp" -->
<%
goods_list = Request("goods_list")
idx = Request("idx")
'goods_price = Request.Form("goods_price")
'goods_cnt = Request.Form("goods_cnt")

If idx <> "" Then
	goods_list = idx
End If
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script type="text/javascript" src="/srm/js/product.js"></script>
	<script type="text/javascript">
		function goBack() {
			window.history.back();
		}
		$(document).on('ready', function() {
			// 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function() {
				if (window.FileReader) {
					var filename = $(this)[0].files[0].name;
				} else {
					var filename = $(this).val().split('/').pop().split('\\').pop();
				}
				$(this).siblings('.upload_name').val(filename);
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container">
				<div id="sub_contents" class="ask">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>견적요청</h2>
					</div>
					<div class="inner">
					<section class="order_section">

							<h5 class="section_tit">상품정보</h5>
							<div class="bd1">
<%
If Request.Cookies("SRM_LEVEL") = 2 Then
	query_where = query_where & " AND b.mem_idx = " & intSeq
ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
	query_where = query_where & " AND b.mem_idx = " & masterIDX
End If

sql = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_simg, ISNULL(b.mem_price, 0) AS mem_price FROM GOODS A INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx WHERE a.g_idx IN (" & goods_list & ")" & query_where
Set rs = dbconn.execute(sql)

If Not (rs.eof) Then
	Do While Not rs.eof
%>
								<div class="cart_div">
									<div class="img_box">
										<a href="#"><img src="/upload/goods/<%=rs("g_simg")%>"></a>
									</div>
									<div class="txt_box">

										<p class="pro_name"><%=rs("g_name")%></p>
										<a href="#" class="pro_code">제품코드 : #<%=rs("g_idx")%></a>
										<strong><% If rs("mem_price") = 0 Then %><%=FormatNumber(rs("g_Money"),0)%><% Else %><%=FormatNumber(rs("mem_price"),0)%><% End If %>원</strong>
										<span>1개</span>
									</div>
								</div>
<%
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
								<!--div class="cart_div">
									<div class="img_box">
										<a href="#"><img src="/srm/m/image/sub/cart_img02.jpg"></a>
									</div>
									<div class="txt_box">

										<p class="pro_name">제품명이 노출됩니다</p>
										<a href="#" class="pro_code">제품코드 : #123456789</a>
										<strong>100,000원</strong>
										<span>1개</span>
									</div>
								</div-->
							</div>
						</section>

					<h5 class="section_tit">견적신청내역</h5>
						<form name="onlineform" method="post" action="./estimate_ok.asp" enctype="multipart/form-data">
						<input type="hidden" name="goods_list" value="<%=idx%>">
						<table class="table table_type02">
							<colgroup>
								<col width="145px">
								<col width="*">
							</colgroup>

							<tbody>

								<tr>
									<th>회사명 <span>*</span></th>
									<td><input type="text" name="on_company" value="<%=cName%>"></td>
								</tr>
								<tr>
									<th>담당자 <span>*</span></th>
									<td><input type="text" name="on_name" value="<%=strName%>"></td>
								</tr>
								<tr class="phone">
									<th>휴대폰 번호 <span>*</span></th>
									<td>
										<input type="text" name="on_mobile1" maxlength="4" value="<%=strMobile1%>" style="width:70px"> -
										<input type="text" name="on_mobile2" maxlength="4" value="<%=strMobile2%>" style="width:70px"> -
										<input type="text" name="on_mobile3" maxlength="4" value="<%=strMobile3%>" style="width:70px">
									</td>
								</tr>
								<tr class="mail">
									<th>이메일 주소 <span>*</span></th>
									<td>
										<input type="text" name="on_email" value="<%=strEmail%>">

									</td>
								</tr>
								<tr>
									<th>제목 <span>*</span></th>
									<td><input type="text" name="on_title"></td>
								</tr>
								<tr>
									<th>내용 <span>*</span></th>
									<td><textarea name="on_content"></textarea></td>
								</tr>
								<tr>
									<th>첨부파일</th>
									<td>
										<div class="file_box">

											<label for="on_file1">파일첨부</label>
											<input type="file" id="on_file1" name="on_file1" class="upload_hidden">
											<input class="upload_name" disabled="disabled">
											<span>(10M 이하)</span>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="btn_box ask_btn_box">
							<button class="btn_wh" onclick="goBack()">취소</button>
							<button class="btn_blue" onclick="ask_chk_m();return false;">확인</button>
						</div>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>