 <!-- #include virtual="/srm/usercheck.asp" -->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/srm/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<link rel="stylesheet" href="/srm/m/_css/sub.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/srm/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
<!--
 		<dl class="sub_menu">
 			<dt class="tit">우리성장</dt>
 			<dd>
 				<ul>
 					<li><a href="plan.asp">우리성장</a></li>
 					<li><a href="culture.asp">우리문화</a></li>
 					<li><a href="video.asp">동영상</a></li>
 					<li><a href="person.asp">인재상</a></li>
 					<li><a href="welfare.asp">복지제도</a></li>
 					<li><a href="recruit.asp">인재채용</a></li>
 				</ul>
 			</dd>
 		</dl>
-->
 		<div id="A_Container" class="pdt110">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>제품비교</h3>
			</div>
					<div id="cpCon">
 					<div class="blueBarTit">
 						<p>Highlight : </p>
 						<ul class="chk">
 							<li><input type="radio" name="highlightrad" id="None_radio" value="None" checked=""> <label for="None_radio">Off</label></li>
 							<li><input type="radio" name="highlightrad" id="Similar_radio" value="Similar"> <label for="Similar_radio">Similarities</label></li>
 							<li><input type="radio" name="highlightrad" id="Differences_radio" value="Differences"> <label for="Differences_radio">Differences</label></li>
 						</ul>
 					</div>
 					<table class="cpTable" id="compareTbl">
 						<colgroup>
 							<col style="width:14%">
 							<col style="width:14%">
 							<col style="width:14%">
 						</colgroup>
 						<form name="remove_Form" method="POST" action="/sub/b2b/product_compare.php"></form>
 						<input type="hidden" name="arr_goods_list" value="@2238@2236@2235@2234@">
 						<tbody>
 							<tr class="pdThum">
 								<th>Product Thumbnail</th>
 								<td>
 									<p><a href="product_view.asp"><img src="/srm/m/images/sub/product_img.jpg" alt=""></a></p>
 									<a href="#;" class="reBtn">REMOVE</a>
 									<input type="hidden">
 								</td>
 								<td>
 									<p><a href="product_view.asp"><img src="images/sub/product_img.jpg" alt=""></a></p>
 									<a href="#;" class="reBtn" rel="2236">REMOVE</a>
 									<input type="hidden">
 								</td>
 							</tr>

 							<tr class="pdName">
 								<th>Product Name</th>
 								<td>
 									<a href="product_view.asp">
 										<em>삼성 노트북 NT931QCG-K582D</em>
 										CPU Intel® Core™ i5-1035G4 1.10G(up 3.70 G.. </a>
 								</td>
 								<td>
 								<a href="product_view.asp">
 										<em>삼성 노트북 NT951XCJ-K582S</em>
 										CPU Intel® Core™ i5-10210U 1.60 G(Up to 4.. </a>
 								</td>
 							</tr>

 							<tr class="subheader">
 								<th class="depth">옵션1</th>
 								<td></td>
 								<td></td>
 							</tr>
 							<tr class="subheader">
 								<th class="depth">옵션2</th>
 								<td></td>
 								<td></td>
 							</tr>
 							<tr class="subheader">
 								<th class="depth">옵션3</th>
 								<td></td>
 								<td></td>
 							</tr>

 						</tbody>
 					</table>
 					 					<table class="cpTable" id="compareTbl">
 						<colgroup>
 							<col style="width:14%">
 							<col style="width:14%">
 							<col style="width:14%">
 						</colgroup>
 						<form name="remove_Form" method="POST" action="/sub/b2b/product_compare.php"></form>
 						<input type="hidden" name="arr_goods_list" value="@2238@2236@2235@2234@">
 						<tbody>
 							<tr class="pdThum">
 								<th>Product Thumbnail</th>
 								<td>
 									<p><a href="product_view.asp"><img src="images/sub/product_img.jpg" alt=""></a></p>
 									<a href="#;" class="reBtn">REMOVE</a>
 									<input type="hidden">
 								</td>
 								<td>
 									<p><a href="product_view.asp"><img src="images/sub/product_img.jpg" alt=""></a></p>
 									<a href="#;" class="reBtn" rel="2236">REMOVE</a>
 									<input type="hidden">
 								</td>
 							</tr>

 							<tr class="pdName">
 								<th>Product Name</th>
 								<td>
 									<a href="product_view.asp">
 										<em>삼성 노트북 NT931QCG-K582D</em>
 										CPU Intel® Core™ i5-1035G4 1.10G(up 3.70 G.. </a>
 								</td>
 								<td>
 								<a href="product_view.asp">
 										<em>삼성 노트북 NT951XCJ-K582S</em>
 										CPU Intel® Core™ i5-10210U 1.60 G(Up to 4.. </a>
 								</td>
 							</tr>

 							<tr class="subheader">
 								<th class="depth">옵션1</th>
 								<td></td>
 								<td></td>
 							</tr>
 							<tr class="subheader">
 								<th class="depth">옵션2</th>
 								<td></td>
 								<td></td>
 							</tr>
 							<tr class="subheader">
 								<th class="depth">옵션3</th>
 								<td></td>
 								<td></td>
 							</tr>

 						</tbody>
 					</table>
 				</div>
		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/srm/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>