<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<link rel="stylesheet" href="/srm/m/_css/slick-theme.css">
	<script src="/srm/m/js/slick.js"></script>
</head>
<script>
	$(document).ready(function() {
		$('.product_catecory_inner').slick({
			infinite: false,
			slidesToScroll: 1,
//			arrows: false,
//			   autoplay: true,
//      autoplaySpeed: 3000,
			 variableWidth: true
		});
	});

</script>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	= SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	= SQL_Injection(Trim(Request("fieldvalue")))
Dim orderby		: orderby		= SQL_Injection(Trim(Request("orderby")))
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="pd0">
				<div id="sub_contents" class="product">
<%
	cate = Request.QueryString("c")

	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
				<div class="product_catecory">
					<ul class="product_catecory_inner">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
						<li><a href="product_category.asp?c=<%=rs("c_code")%>"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
						<!--li><a href="product_category.asp">디스플레이/TV</a></li>
						<li><a href="product_category.asp">화상회의</a></li>
						<li><a href="product_category.asp">복합기/프린터</a></li>
						<li><a href="product_category.asp">네트워크</a></li>
						<li><a href="product_category.asp">태블릿/모바일</a></li>
						<li><a href="product_category.asp">부품/소프트웨어</a></li>
						<li><a href="product_category.asp">가전</a></li-->
					</ul>
				</div>
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intBlockPage		: intBlockPage 		= 5
Dim intPageSize			: intPageSize 		= 5

If Request.Cookies("SRM_LEVEL") <> 9 Then
	Dim query_filde			: query_filde		= " a.g_idx, a.g_name, a.g_Money, a.g_simg, a.g_spec, ISNULL(b.mem_price, 0) AS mem_price "
	Dim query_Tablename		: query_Tablename	= "GOODS a INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx"
Else
	query_filde		= " a.g_idx, a.g_name, a.g_Money, a.g_simg, a.g_spec "
	query_Tablename	= "GOODS a "
End If
Dim query_where			: query_where		= " a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y'"

If Request.Cookies("SRM_LEVEL") = 2 Then
	query_where = query_where & " AND b.mem_idx = " & intSeq
ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
	query_where = query_where & " AND b.mem_idx = " & masterIDX
End If

If intNowPage = "" Then
	intNowPage = 1
End If

If orderby = "" Then
	query_orderby = " ORDER BY a.g_idx DESC"
Else
	If orderby = "g_Money_asc" Then
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			query_orderby = " ORDER BY b.mem_price ASC"
		Else
			query_orderby = " ORDER BY a.g_Money ASC"
		End If
	ElseIf orderby = "g_Money_desc" Then
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			query_orderby = " ORDER BY b.mem_price DESC"
		Else
			query_orderby = " ORDER BY a.g_Money DESC"
		End If
	ElseIf orderby = "regdate" Then
		query_orderby = " ORDER BY a.g_insertDay DESC"
	End If
End If

If cate <> "" Then
	If Right(cate, 8) = "00000000" Then
		query_where = query_where & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
	ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
		query_where = query_where & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
	ElseIf Mid(cate, 6, 1) <> 0 Then
		query_where = query_where & " AND a.g_category = '" & cate & "'"
	End If
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND a."& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
				<div class="inner">
					<div class="product_search">
						<span>전체 (<%=intTotalCount%>)</span>
						<form name="search_form" action="" method="post">
						<div class="right">
							<select name="orderby" onchange="document.search_form.submit();">
								<option value="g_read" <% If orderby = "g_read" Then %> selected<% End If %>>인기순</option>
								<option value="regdate" <% If orderby = "regdate" Then %> selected<% End If %>>최신순</option>
								<option value="g_Money_asc" <% If orderby = "g_Money_asc" Then %> selected<% End If %>>낮은 가격순</option>
								<option value="g_Money_desc" <% If orderby = "g_Money_desc" Then %> selected<% End If %>>높은 가격순</option>
							</select>
						</div>
						</form>
					</div>
				</div>

				<ul class="pro_list pro_list03">
<%
If rs.bof Or rs.eof Then
%>
					<li>등록된 제품이 없습니다.</li>
<%
Else
	i = 0
	Do While Not rs.eof
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			If rs("mem_price") = 0 Then
				finalPrice = rs("g_Money")
			Else
				finalPrice = rs("mem_price")
			End If
		Else
			finalPrice = rs("g_Money")
		End If
%>
					<li>
						<div class="img_box"><a href="product_view.asp?idx=<%=rs("g_idx")%>">
								<div class="chk_box">
									<input id="pro0<%=i%>" type="checkbox" value="<%=rs("g_idx")%>">
									<label for="pro0<%=i%>"><span></span></label>
								</div><img src="/upload/goods/<%=rs("g_simg")%>" alt="<%=rs("g_name")%>">
							</a></div>
						<div class="cont_box">
							<p class="pro_tit"><a href="product_view.asp?idx=<%=rs("g_idx")%>"><%=rs("g_name")%></a></p>
							<span><a href="product_view.asp?idx=<%=rs("g_idx")%>"># <%=rs("g_idx")%></a></span>
							<p class="hide"><a href="product_view.asp?idx=<%=rs("g_idx")%>"><%=rs("g_spec")%></a></p>
							<strong class="price"><a href="product_view.asp?idx=<%=rs("g_idx")%>"><%=FormatNumber(finalPrice, 0)%> 원</a></strong>
						</div>
						<div class="a_box">

							<label for="prod"><span></span></label>
							<a href="./product_estimate.asp?idx=<%=rs("g_idx")%>">견적요청</a>
							<a href="/srm/m/order/cart_add_ok.asp?idx=<%=rs("g_idx")%>&price=<%=finalPrice%>">장바구니</a>
						</div>
					</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
				</ul>
				<% If intTotalCount <> i Then %>
				<div class="inner">
					<button class="btn_more" onclick="location.href='?c=<%=cate%>&page=<%=intNowPage+1%>'">
						제품 더 불러오기 (+5)
					</button>
				</div>
				<% End If %>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
			</div>
		</div>

	</div>

</body>

</html>