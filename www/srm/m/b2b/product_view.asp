<!-- #include virtual="/srm/usercheck.asp" -->
<%
g_idx = SQL_Injection(Trim(Request.QueryString("idx")))
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<link rel="stylesheet" href="/srm/m/_css/slick-theme.css">
	<script src="/srm/m/js/slick.js"></script>
</head>
<script>
	$(document).ready(function() {

			$(".tab_contents").hide();
			$(".sub_con_tab li:first").addClass("active").show();
			$(".tab_contents:first").show();

			$(".sub_con_tab li").click(function() {
				$(".sub_con_tab li").removeClass("active");
				$(this).addClass("active");
				$(".tab_contents").hide();
				var activeTab = $(this).find("a").attr("href");
				$(activeTab).fadeIn();
				return false;
			});
//		$('.product_catecory_inner').slick({
//			infinite: false,
//			slidesToScroll: 1,
//			arrows: false,
////			   autoplay: true,
////      autoplaySpeed: 2000,
//			 variableWidth: true
//		});

				$('.slider-for').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
//				slidesToShow: 5,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				dots:false,
				centerMode: true,
				focusOnSelect: true,
				variableWidth: true
			});

			$('#increase').click(function(e){
				e.preventDefault();
				var stat = $('#updown').val();
				var num = parseInt(stat,10);
				num++;

				$('#updown').val(num);
				$('#GoodPrice').text(num*$("#GoodPrice").attr("rel"));
				$('#GoodPrice').number( true );
			});

			$('#decrease').click(function(e){
				e.preventDefault();
				var stat = $('#updown').val();
				var num = parseInt(stat,10);
				num--;

				if(num<=0){
					alert('더이상 줄일수 없습니다.');
					num =1;
				}
				$('#updown').val(num);
				$('#GoodPrice').text(num*$("#GoodPrice").attr("rel"));
				$('#GoodPrice').number( true );
			});
	});

		function direct_order() {
			document.directOrderForm.method = "post";
			document.directOrderForm.action = "/srm/m/order/order_direct.asp";
			document.directOrderForm.submit();
		}

</script>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
sql2 = "SELECT b.c_name FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE g_idx = '" & g_idx & "'"
Set rs4 = dbconn.execute(sql2)

If Not rs4.eof Or rs4.bof Then
	cateName = rs4("c_name")
End If

rs4.close
Set rs4 = Nothing
%>
		<div id="A_Container_Wrap">
			<div id="A_Container">
				<div id="sub_contents" class="product product_view">

<!--
				<div class="product_catecory">
				<div class="inner">
					<ul class="product_catecory_inner">
						<li><a href="product_category.asp">데스크탑/서버</a></li>
						<li><a href="product_category.asp">디스플레이/TV</a></li>
						<li><a href="product_category.asp">화상회의</a></li>
						<li><a href="product_category.asp">복합기/프린터</a></li>
						<li><a href="product_category.asp">네트워크</a></li>
						<li><a href="product_category.asp">태블릿/모바일</a></li>
						<li><a href="product_category.asp">부품/소프트웨어</a></li>
						<li><a href="product_category.asp">가전</a></li>
					</ul>
					</div>
				</div>
-->
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2><%=cateName%></h2>
					</div>
<%
ReDim g_bimg(4), g_menual(4)

If Request.Cookies("SRM_LEVEL") = 2 Then
	query_where = query_where & " AND B.mem_idx = " & intSeq
ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
	query_where = query_where & " AND B.mem_idx = " & masterIDX
End If

If Request.Cookies("SRM_LEVEL") = 9 Then
	sql = "SELECT A.* FROM GOODS A WHERE A.g_display <> 'home' AND A.g_act = 'Y' AND A.g_idx = '" & g_idx & "'"
Else
	sql = "SELECT A.*, ISNULL(B.mem_price, 0) AS mem_price FROM GOODS A INNER JOIN goods_srm_join B ON A.g_idx = B.g_idx WHERE A.g_display <> 'home' AND A.g_act = 'Y' AND A.g_idx = '" & g_idx & "'" & query_where
End If
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	g_name = rs("g_name")
	g_spec = rs("g_spec")
	g_intro = rs("g_intro")
	g_money = rs("g_money")
	total_op = rs("total_op")
	g_use_totalChk = rs("g_use_totalChk")
	g_optionList = rs("g_optionList")
	g_simg = rs("g_simg")
	For i = 1 To 5
		g_bimg(i-1) = rs("g_bimg" & i)
	Next
	g_memo = rs("g_memo")
	g_video = rs("g_video")
	For j = 1 To 5
		g_menual(j-1) = rs("g_menual" & j)
	Next

	If Request.Cookies("SRM_LEVEL") <> 9 Then
		If rs("mem_price") = 0 Then
			FinalPrice = rs("g_Money")
		Else
			FinalPrice = rs("mem_price")
		End If
	Else
		FinalPrice = rs("g_Money")
	End If
Else
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

sql = "UPDATE GOODS SET g_read = g_read + 1 WHERE g_type = 1 AND g_display <> 'home' AND g_act = 'Y' AND g_idx = '" & g_idx & "'"
dbconn.execute(sql)

rs.Close
%>
					<!-- 슬라이드영역-->
					<div class="pdtop">
						<div class="inner">
							<div class="prov_slide">
								<div class="slider-for">
								<% If g_simg <> "" Then %>
									<img src="/upload/goods/<%=g_simg%>">
								<% End If %>
<%
							For i = 1 To 5
								If g_bimg(i-1) <> "" Then
%>
									<img src="/upload/goods/<%=g_bimg(i-1)%>">
<%
								End If
							Next
%>
								</div>
								<div class="slider-nav">
								<div class="img_box">
									<img src="/upload/goods/<%=g_simg%>">
								</div>
<%
							For i = 1 To 5
								If g_bimg(i-1) <> "" Then
%>
								<div class="img_box">
									<img src="/upload/goods/<%=g_bimg(i-1)%>">
								</div>
<%
								End If
							Next
%>
								</div>
							</div>

							<!--제품설명영역-->

							<div class="prov_cont">
								<ul>
									<li>
										<p>제품코드 : #<%=g_idx%></p>
										<h3><%=g_name%></h3>
									</li>
									<li class="line">
										<p class="tit">주요특징 <span><%=g_spec%></span></p>
										<p class="tit">제품개요 <span><%=g_intro%></span></p>
									</li>
									<li>
										<p class="tit">가격 </p><strong id="GoodPrice" name="GoodPrice" rel="<%=FinalPrice%>"><%=FormatNumber(FinalPrice, 0)%></strong>
									</li>
									<form name="directOrderForm">
									<input type="hidden" name="idx" value="<%=g_idx%>">
									<li>
										<p class="tit">
											수량 <span class="inven">(재고:<%=total_op%>)</span>
										</p> <strong>
											<div class="vol_btns">
												<a href="#"><img src="/srm/m/image/sub/but_vol_up.png" id="decrease"></a>
												<input type="text" name="g_cnt" id="updown" value="1">
												<a href="#"><img src="/srm/m/image/sub/but_vol_down.png" id="increase"></a>
											</div>
										</strong>
									</li>
									</form>
									<li class="btn_list"><button class="btn_wh" onclick="location.href='/srm/m/b2b/product_estimate.asp?idx=<%=g_idx%>'">견적요청</button>
										<button class="btn_bk" onclick="location.href='/srm/m/order/cart_add_ok.asp?idx=<%=g_idx%>&price=<%=finalPrice%>'">장바구니</button>
										<button class="btn_blue" onclick="direct_order();">바로구매</button>
										</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="inner">
						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li><a href="#pro_view_01">제품사양</a></li>
								<li><a href="#pro_view_02">제품상세</a></li>
								<li><a href="#pro_view_03">제품영상</a></li>
								<li><a href="#pro_view_04">제품 매뉴얼</a></li>
							</ul>
							<div class="tab_contents " id="pro_view_01">
								<table class="table pv_table">
									<tbody>
<%
	arr_g_optionList = Split(g_optionList, ",")

	If Ubound(arr_g_optionList) > 0 Then
		For i = 0 To Ubound(arr_g_optionList) - 1
			arr_g_optionList1 = Split(arr_g_optionList(i), "||")
%>
										<tr>
											<th><%=Trim(arr_g_optionList1(0))%></th>
											<td><%=Trim(arr_g_optionList1(1))%></td>
										</tr>
<%
		Next
	End If
%>
									</tbody>
								</table>
							</div>

							<div class="tab_contents " id="pro_view_02">
								<div class="dateil_area">
									<p><%=g_memo%></p>
								</div>
							</div>
<%
sql = "SELECT b_addtext1 FROM BOARD_v1 WHERE b_part = 'board08' AND b_idx = " & g_video
Set rs = dbconn.execute(sql)
%>
							<div class="tab_contents " id="pro_view_03">
								<div class="video_area">
								<% If Not(rs.eof) Then %>
									<%=rs("b_addtext1")%>
								<% End If %>
								</div>
							</div>
<%
rs.close

For j = 1 To 5
	If g_menual(j-1) <> "" Then
		If j = 1 Then
			arr_var = arr_var & g_menual(j-1)
		Else
			arr_var = arr_var & "," & g_menual(j-1)
		End If
	End If
Next

sql = "SELECT file_1 FROM BOARD_v1 WHERE b_part = 'board07' AND b_idx IN (" & arr_var & ")"
Set rs = dbconn.execute(sql)
%>
							<div class="tab_contents " id="pro_view_04">
								<table class="tab_table">
									<tbody>
<%
	If Not(rs.eof) Then
		Do Until rs.Eof
%>
										<tr>
											<th><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download><%=rs("file_1")%> <!--p>20MB</p--></a></th>
											<td><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download>다운로드</a></td>
										</tr>
<%
			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
									</tbody>
								</table>
							</div>
						</div>
					</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
			</div>
		</div>

	</div>

</body>

</html>