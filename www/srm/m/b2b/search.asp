<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script src="/srm/m/js/product.js"></script>
</head>

<body>
	<div id="A_Wrap">
<%

	all_fieldvalue 	= SQL_Injection(Trim(Request("all_fieldvalue")))
	fieldvalue 		= SQL_Injection(Trim(Request("fieldvalue")))

	Dim orderby		: orderby		= SQL_Injection(Trim(Request("orderby")))
	Dim intTotalCount, intTotalPage

	Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
	Dim intPageSize			: intPageSize 		= 4
	Dim intBlockPage		: intBlockPage 		= 5

	Dim query_filde			: query_filde		= " a.g_idx, a.g_name, a.g_Money, a.g_simg, a.g_Memo, ISNULL(b.mem_price, 0) AS mem_price "
	Dim query_Tablename		: query_Tablename	= "GOODS a INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx"
	Dim query_where			: query_where		= " a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y'"

	If Request.Cookies("SRM_LEVEL") = 2 Then
		query_where = query_where & " AND b.mem_idx = " & intSeq
	ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
		query_where = query_where & " AND b.mem_idx = " & masterIDX
	End If

	If Len(fieldvalue) > 0 Then
		query_where = query_where &" AND a.g_name LIKE '%" & fieldvalue & "%' "
	End If

	If orderby = "" Then
		query_orderby = " ORDER BY a.g_idx DESC"
	Else
		If orderby = "g_Money_asc" Then
			query_orderby = " ORDER BY a.g_Money ASC"
		ElseIf orderby = "g_Money_desc" Then
			query_orderby = " ORDER BY a.g_Money DESC"
		Else
			query_orderby = " ORDER BY a." & orderby & " DESC"
		End If
	End If

	Call intTotal

	Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

	sql = getQuery

	Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div class="pdz">
				<div id="sub_contents" class="search">
				<form name="schForm" method="post">
				<div class="sub_tit">
					<div>
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<input type="text" name="fieldvalue" value="<% If fieldvalue = "" Then %><%=all_fieldvalue%><% Else %><%=fieldvalue%><% End If %>">
					</div>
					<button class="btn_search02" type="button" onclick="document.schForm.submit();">검색</button>
				</div>
				</form>
				<div class="inner">
					<div class="product_search product_search2">
						<p>‘<% If fieldvalue = "" Then %><%=all_fieldvalue%><% Else %><%=fieldvalue%><% End If %>’에 대한 제품 검색결과입니다.</p>
					</div>
					<form name="search_form" action="" method="post">
					<div class="product_search">
						<span><% If fieldvalue = "" Then %><%=all_fieldvalue%><% Else %><%=fieldvalue%><% End If %> (<%=intTotalCount%>)</span>
						<div class="right">
							<select name="orderby" onchange="document.search_form.submit();">
								<option value="g_read" <% If orderby = "g_read" Then %> selected<% End If %>>인기순</option>
								<option value="g_insertDay" <% If orderby = "g_insertDay" Then %> selected<% End If %>>최신순</option>
								<option value="g_Money_asc" <% If orderby = "g_Money_asc" Then %> selected<% End If %>>낮은 가격순</option>
								<option value="g_Money_desc" <% If orderby = "g_Money_desc" Then %> selected<% End If %>>높은 가격순</option>
							</select>
						</div>
					</div>
					</form>
				</div>

				<ul class="pro_list pro_list03">
<%
If rs.bof Or rs.eof Then
%>
						<li>등록된 제품이 없습니다.</li>
<%
Else
	i = 1
	Do While Not rs.eof
		If rs("mem_price") = 0 Then
			finalPrice = rs("g_Money")
		Else
			finalPrice = rs("mem_price")
		End If
%>
					<li>
						<div class="img_box"><a href="product_view.asp?<%=rs("g_idx")%>">
								<div class="chk_box">
									<input id="pro0<%=i%>" type="checkbox">
									<label for="pro0<%=i%>"><span></span></label>
								</div><img src="/upload/goods/<%=rs("g_simg")%>" alt="">
							</a></div>
						<div class="cont_box">
							<p class="pro_tit"><a href="product_view.asp"><%=rs("g_name")%></a></p>
							<span><a href="product_view.asp?<%=rs("g_idx")%>"># <%=rs("g_idx")%></a></span>
							<p class="hide"><a href="product_view.asp?<%=rs("g_idx")%>"><%=rs("g_memo")%></a></p>
							<strong class="price"><a href="product_view.asp?<%=rs("g_idx")%>"><%=FormatNumber(finalPrice, 0)%> 원</a></strong>
						</div>
						<div class="a_box">

							<label for="prod"><span></span></label>
							<a href="./product_estimate.asp?idx=<%=rs("g_idx")%>">견적요청</a>
							<a href="/srm/m/order/cart_add_ok.asp?idx=<%=rs("g_idx")%>&price=<%=finalPrice%>">장바구니</a>
						</div>
					</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>

				</ul>

			</div>

			</div>
		</div>

	</div>

</body>

</html>