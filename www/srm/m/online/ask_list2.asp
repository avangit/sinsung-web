<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="/srm/m/js/jquery.navgoco.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#sdate").datepicker({
				showOn: "both",
				buttonImageOnly: true,
				buttonImage: "/srm/m/image/common/ic_date.png",
				dateFormat: "yy-mm-dd"
			});
			$("#edate").datepicker({
				showOn: "both",
				buttonImageOnly: true,
				buttonImage: "/srm/m/image/common/ic_date.png",
				dateFormat: "yy-mm-dd"
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim dayday 		: dayday 		=  SQL_Injection(Trim(Request("d")))
Dim sdate 		: sdate 		=  SQL_Injection(Trim(Request("sdate")))
Dim edate 		: edate 		=  SQL_Injection(Trim(Request("edate")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "board_online"
Dim query_where			: query_where		= " on_type = 'etc'"
Dim query_orderby		: query_orderby		= " ORDER BY on_idx DESC"

If Request.Cookies("SRM_LEVEL") <> 9 Then
	query_where = query_where & " AND mem_idx = " & intSeq
End If

If dayday = "" Then
	sdate = ""
	edate = ""
ElseIf dayday = "7" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -7, Date)
	edate = Date
ElseIf dayday = "30" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -30, Date)
	edate = Date
ElseIf dayday = "365" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -365, Date)
	edate = Date
End If

If sdate <> "" And edate <> "" Then
	query_where = query_where & " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day, 1, '" & edate & "')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where & " AND regdate >= '" & sdate & "'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where & " AND regdate <= DATEADD(day, 1, '" & edate & "')"
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="pd0">
				<div id="sub_contents" class="serial order_list">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>기타문의</h2>
					</div>

					<div class="search_box2">
							<div class="tab_btn">
								<button onclick="location.href='./ask_list2.asp'" <% If dayday = "" Then %>class="on"<% End If %>>전체</button>
								<button onclick="location.href='./ask_list2.asp?d=7'" <% If dayday = "7" Then %>class="on"<% End If %>>1주일</button>
								<button onclick="location.href='./ask_list2.asp?d=30'" <% If dayday = "30" Then %>class="on"<% End If %>>1개월</button>
								<button onclick="location.href='./ask_list2.asp?d=365'" <% If dayday = "365" Then %>class="on"<% End If %>>1년</button>
							</div>
							<form name="sch_Form" method="post" action="">
							<div>
								<div class="date_box"><input type="text" name="sdate" id="sdate" value="<%=sdate%>" autocomplete="off"></div>
								<div class="date_box"><input type="text" name="edate" id="edate" class="date" value="<%=edate%>" autocomplete="off"></div>
							</div>
					</div>

					<div class="inner">
						<div class="search_box">
							<input type="text" name="fieldvalue" id="fieldvalue" value="<%=fieldvalue%>">
							<button class="btn_search" onclick="document.sch_Form.submit();">검색</button>
						</div>
						</form>
						<div class="middle_box">
							<p class="total_t">전체검색</p>
							<div class="btn_small">
								<button class="btn_blue btn_wd" onclick="location.href = 'ask.asp'">문의하기</button>
							</div>
						</div>

						<div id="slide_tab">
<%
If rs.bof Or rs.eof Then
%>
							<div class="lnb">등록된 문의내역이 없습니다.</div>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
		status = rs("on_status")

		If status = "Y" Then
			status_txt = "답변완료"
		Else
			status_txt = "접수완료"
		End If
%>
							<div class="lnb">
								<ul class="nav">
									<li>
										<a href="ask_detail.asp?idx=<%=rs("on_idx")%>">
											<span class="num"><%=intNowNum%></span>
											<span><%=Left(rs("regdate"), 10)%></span>
											<span class="code"><%=rs("on_cate")%></span>
											<div class="pro_name">
												<div class="l_box">
													<p><%=rs("on_title")%></p>
													<p class="color9"><%=rs("on_name")%></p>
													<span class="<% If status <> "Y" Then %>custom<% Else %> custom2<% End If %>"><%=status_txt%></span>
												</div>
											</div>
										</a>
									</li>

								</ul>
							</div>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
						</div>

						<% Call Paging_user_srm("") %>

					</div>
				</div>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>