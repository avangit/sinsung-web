<!-- #include virtual="/srm/usercheck.asp" -->
<%
on_idx = SQL_Injection(Trim(Request.QueryString("idx")))

Call DbOpen()

If Request.Cookies("SRM_LEVEL") <> 9 Then
	query_where = query_where & " AND mem_idx = " & intSeq
End If

sql = "SELECT * FROM board_online WHERE on_idx = " & on_idx & query_where
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	on_status = rs("on_status")
	on_cate = rs("on_cate")
	on_company = rs("on_company")
	on_name = rs("on_name")
	on_mobile = rs("on_mobile")
	on_email = rs("on_email")
	on_title = rs("on_title")
	on_content = rs("on_content")
	on_file1 = rs("on_file1")
	on_file2 = rs("on_file2")
	re_name = rs("re_name")
	re_content = rs("re_content")
	regdate = rs("regdate")
	replydate = rs("replydate")
	p_idx = rs("p_idx")

	If on_status = "Y" Then
		status_txt = "답변완료"
	Else
		status_txt = "접수완료"
	End If
End If

rs.Close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container">
				<div id="sub_contents" class="ask order_view">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>견적요청</h2>
					</div>
<%
If p_idx <> "" Then
	sql2 = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_simg, ISNULL(b.mem_price, 0) AS mem_price FROM GOODS A INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx WHERE a.g_idx IN (" & p_idx & ") AND B.mem_idx = '" & Request.Cookies("SRM_IDX") & "' ORDER BY a.g_idx DESC"
	Set rs2 = dbconn.execute(sql2)
%>
					<div class="inner">

						<section class="order_section">

							<h5 class="section_tit">상품정보</h5>
							<div class="bd1">
<%
	If Not (rs2.eof) Then
		Do While Not rs2.eof
%>
								<div class="cart_div">
									<div class="img_box">
										<a href="#"><img src="/upload/goods/<%=rs2("g_simg")%>"></a>
									</div>
									<div class="txt_box">
								
										<p class="pro_name"><%=rs2("g_name")%></p>
										<a href="#" class="pro_code">제품코드 : #<%=rs2("g_idx")%></a>
										<strong><% If rs2("mem_price") = 0 Then %><%=FormatNumber(rs("g_Money"),0)%><% Else %><%=FormatNumber(rs2("mem_price"),0)%><% End If %>원</strong>
										<span>1개</span>
									</div>
								</div>
<%
			rs2.MoveNext
		Loop
	End If
%>
								<!--div class="cart_div">
									<div class="img_box">
										<a href="#"><img src="/srm/m/image/sub/cart_img02.jpg"></a>
									</div>
									<div class="txt_box">
								
										<p class="pro_name">제품명이 노출됩니다</p>
										<a href="#" class="pro_code">제품코드 : #123456789</a>
										<strong>100,000원</strong>
										<span>1개</span>
									</div>
								</div-->
							</div>
						</section>
<%
	rs2.close
	Set rs2 = Nothing

	Call DbClose()
End If
%>
						<div class="order_section">
							<h5 class="section_tit">견적신청내역</h5>
							<div class="info_view">
								<div class="tit_box">
									<p><%=on_title%></p>
									<p class="cate"><% If on_cate <> "" Then %>분류 : <%=on_cate%> <% End If %></p>
									<ul>
										<li><%=on_name%></li>
										<li><%=on_company%></li>
										<li><%=on_mobile%> </li>
									</ul>
									<div class="tit_bottom">
										<span class="bar2"><%=on_email%> </span>
										<span class="right"><%=Left(regdate, 10)%></span>
									</div>
									<strong class="ing_box"><%=status_txt%></strong>
								</div>
								<div class="content_box">
									<div class="contents">
										<%=on_content%>
									</div>
									<div class="file_box">
										<p>첨부파일</p>
									<% If on_file1 <> "" Then %>
										<span><a href="/download.asp?fn=<%=escape(on_file1)%>&ph=online" class="pdR20"><%=on_file1%></a></span>
									<% End If %>
									<% If on_file2 <> "" Then %>
										<span><a href="/download.asp?fn=<%=escape(on_file1)%>&ph=online" class="pdR20"><%=on_file2%></a></span>
									<% End If %>
									</div>
								</div>
							</div>
						</div>

						<% If re_content <> "" Then %>
						<div class="order_section">
							<h5 class="section_tit">견적신청내역</h5>
							<div class="info_view answer">
								<div class="tit_box">
									<!--p>제목이 노출됩니다 제목이 노출됩니다</p-->
									<ul>
										<!--li>담당자</li>
										<li>010-1234-5678</li-->
									</ul>
									<div class="tit_bottom">
										<!--span>Sample@email.co.kr</span-->
										<span class="right"><%=Left(replydate,10)%></span>
									</div>
								</div>
								<div class="content_box">
									<div class="contents">
										<%=re_content%>
									</div>
									<!--div class="file_box">
										<p>첨부파일</p>
										<span>Sample1.jpg</span>
										<span>Sample2.jpg</span>
									</div-->
								</div>
							</div>
						</div>
						<% End If %>

						<button class="btn_navy" onclick="location.href = 'estimate_list.asp'">목록</button>


					</div>
				</div>
			</div>
		</div>
		<div id="A_Footer">
			<!-- #include virtual="/srm/m/_inc/footer.asp" -->
		</div>
	</div>


</body>

</html>
