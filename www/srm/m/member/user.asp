<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script src="/srm/m/js/member.js" type="text/javascript"></script>
</head>
<script>
	$(document).on('ready', function() {
		// 첨부파일
		var fileTarget = $('.file_box .upload_hidden');
		fileTarget.on('change', function() {
			if (window.FileReader) {
				var filename = $(this)[0].files[0].name;
			} else {
				var filename = $(this).val().split('/').pop().split('\\').pop();
			}
			$(this).siblings('.upload_name').val(filename);
		});
	});

		function openDaumPostcode() {new daum.Postcode({
				oncomplete: function(data) {
					// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

					// 각 주소의 노출 규칙에 따라 주소를 조합한다.
					// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
					var fullAddr = ''; // 최종 주소 변수
					var extraAddr = ''; // 조합형 주소 변수

					// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
					if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
						fullAddr = data.roadAddress;

					} else { // 사용자가 지번 주소를 선택했을 경우(J)
						fullAddr = data.jibunAddress;
					}

					// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
					if(data.userSelectedType === 'R'){
						//법정동명이 있을 경우 추가한다.
						if(data.bname !== ''){
							extraAddr += data.bname;
						}
						// 건물명이 있을 경우 추가한다.
						if(data.buildingName !== ''){
							extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
						}
						// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
						fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
					}

					// 우편번호와 주소 정보를 해당 필드에 넣는다.
					document.getElementById('cZip').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('cAddr1').value = fullAddr;

					// 커서를 상세주소 필드로 이동한다.
					document.getElementById('cAddr2').focus();
				}
			}).open();
		}
</script>
<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
If strPhone <> "" Then
	ArrPhone = Split(strPhone, "-")
	If Ubound(ArrPhone) = 2 Then
		strPhone1 = ArrPhone(0)
		strPhone2 = ArrPhone(1)
		strPhone3 = ArrPhone(2)
	End If
End If

If strMobile <> "" Then
	ArrMobile = Split(strMobile, "-")
	If Ubound(ArrMobile) = 2 Then
		strMobile1 = ArrMobile(0)
		strMobile2 = ArrMobile(1)
		strMobile3 = ArrMobile(2)
	End If
End If

If strEmail <> "" Then
	ArrstrEmail = Split(strEmail, "@")
	If Ubound(ArrstrEmail) = 1 Then
		strEmail1 = ArrstrEmail(0)
		strEmail2 = ArrstrEmail(1)
	End If
End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user">

					<div class="sub_tit">
						<button class="btn_back" onclick="javascript:history.back();">뒤로가기</button>
						<h2>회원정보관리</h2>
					</div>

					<div class="inner">
						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li class="active"><a href="user.asp">회원정보변경</a></li>
								<li><a href="user_2.asp">세금계산서발행정보</a></li>
								<li><a href="user_3.asp">배송지 관리</a></li>
								<li><a href="user_4.asp">조직관리</a></li>
							</ul>
							<form name="editform" method="post" action="./user_ok.asp" enctype="multipart/form-data">
							<input type="hidden" name="strId" value="<%=strId%>">
							<div class="tab_contents " id="user_01">
								<table class="table table_type02">
								<colgroup>
								<col width="220px">
								<col width="300px">
							</colgroup>

									<tbody>
										<tr>
											<th>회사명 <span>*</span></th>
											<td><%=cName%></td>
										</tr>
										<tr>
											<th>사업자번호 <span>*</span></th>
											<td><%=cNum%></td>
										</tr>
										<tr>
											<th>아이디 <span>*</span></th>
											<td><%=strId%></td>
										</tr>
										<tr>
											<th>기존 비밀번호 <span>*</span></th>
											<td><input type="password" name="strPwd"></td>
										</tr>
										<tr>
											<th>새 비밀번호</th>
											<td><input type="password" name="newPwd" placeholder="8~20자, 영문, 숫자, 특수문자 사용"></td>
										</tr>
										<tr>
											<th>새 비밀번호 확인</th>
											<td><input type="password" name="newPwdRe"></td>
										</tr>
										<tr>
											<th>이름 <span>*</span></th>
											<td><%=strName%></td>
										</tr>
										<tr class="phone">
											<th>전화번호</th>
											<td>
												<input type="text" name="strPhone1" style="width:70px" maxlength="4" value="<%=strPhone1%>">
												<input type="text" name="strPhone2" style="width:70px" maxlength="4" value="<%=strPhone2%>">&shy; - &shy;
												<input type="text" name="strPhone3" style="width:70px" maxlength="4" value="<%=strPhone3%>">
											</td>
										</tr>
										<tr class="phone">
											<th>휴대폰 번호 <span>*</span></th>
											<td>
												<input type="text" name="strMobile1" style="width:70px" maxlength="4" value="<%=strMobile1%>">&shy; - &shy;
												<input type="text" name="strMobile2" style="width:70px" maxlength="4" value="<%=strMobile2%>">&shy; - &shy;
												<input type="text" name="strMobile3" style="width:70px" maxlength="4" value="<%=strMobile3%>">
												<div class="chk_box">
													<input id="smsYN" type="checkbox" name="smsYN" value="Y" <% If smsYN = "Y" Then %> checked<% End If %>>
													<label for="smsYN"><span></span>
														<p>SMS 수신동의</p>
													</label>
												</div>
											</td>
										</tr>
										<tr class="mail">
											<th>이메일 주소 <span>*</span></th>
											<td>
												<input type="text" name="strEmail" value="<%=strEmail%>">

												<div class="chk_box">
													<input id="mailYN" type="checkbox" name="mailYN" value="Y" <% If mailYN = "Y" Then %> checked<% End If %>>
													<label for="mailYN"><span></span>
														<p>메일 수신동의</p>
													</label>
												</div>
											</td>
										</tr>
										<tr class="add">
											<th>회사주소</th>
											<td><input type="text" name="cZip" id="cZip" class="wd360" value="<%=cZip%>" readonly> <button class="zip_code btn_bk">우편번호 검색</button> <input type="text" name="cAddr1" id="cAddr1" value="<%=cAddr1%>" readonly><input type="text" name="cAddr2" id="cAddr2" value="<%=cAddr2%>"></td>
										</tr>
										<tr>
											<th>사업자등록증 사본</th>
											<td>
												<div class="file_box">

													<label for="strFile1">파일첨부</label>
													<input type="file" id="strFile1" name="strFile1" class="upload_hidden">
													<input class="upload_name" disabled="disabled">
													<span><% If rsFile <> "" Then %>기존파일 : <a href="/download.asp?fn=<%=escape(rsFile)%>&ph=member"><%=rsFile%></a><% End If %><br>(10M 이하)</span>
												</div>
											</td>
										</tr>

									</tbody>
								</table>
								<div class="btn_box">

									<button class="btn_blue" onclick="update_chk();return false;">확인</button>
								</div>
							</div>
							</form>
						</div>
<%
Set rs = Nothing

Call dbClose()
%>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>