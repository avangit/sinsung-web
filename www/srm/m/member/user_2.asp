<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "member_taxbill"
Dim query_where			: query_where		= " mem_idx = '" & intSeq & "' "
Dim query_orderby		: query_orderby		= " ORDER BY bill_idx DESC"

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user">

					<div class="sub_tit">
						<button class="btn_back" onclick="javascript:history.back();">뒤로가기</button>
						<h2>회원정보관리</h2>
					</div>

					<div class="inner">
						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li><a href="user.asp">회원정보변경</a></li>
								<li class="active"><a href="user_2.asp">세금계산서발행정보</a></li>
								<li><a href="user_3.asp">배송지 관리</a></li>
								<li><a href="user_4.asp">조직관리</a></li>
							</ul>

								<div class="tab_contents " id="user_02">
								<p class="cont"><span><%=intTotalCount%></span>건</p>
								<div class="btn_right">
									<button class="btn_blue" onclick="location.href = './user_tax_write.asp'">등록하기</button>
								</div>

								<div id="slide_tab">
<%
If rs.bof Or rs.eof Then
%>
									<div class="lnb">
										<ul class="nav"><li>데이터가 없습니다.</li></ul>
									</div>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>
									<div class="lnb">
										<ul class="nav">
											<li>
												<a>
													<span class="num"><%=intNowNum%></span>
													<span><%=Left(rs("regdate"), 10)%></span>
													<span class="code"><%=rs("cNum")%></span>
													<div class="pro_name">
														<div class="l_box">
															<p><% If rs("mainYN") = "Y" Then %><span>[대표]</span><% End If %><%=rs("cName")%></p>
															<p><%=rs("ceo")%></p>
															<p><%=rs("cEmail")%></p>
														</div>
														<div class="edit_box">
															<button class="btn_gray" onclick="location.href='user_tax_write.asp?idx=<%=rs("bill_idx")%>'">수정</button>
															<button class="btn_navy" onclick="if(confirm('삭제하시겠습니까?')){location.href='user_tax_write_ok_get.asp?idx=<%=rs("bill_idx")%>'}else{return false;}">삭제</button>
														</div>
													</div>
												</a>
											</li>
										</ul>
									</div>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
								</div>

								<% Call Paging_user_srm("") %>
							</div>

						</div>

					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>