<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script type="text/javascript" src="/srm/m/js/member.js"></script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
idx = SQL_Injection(Trim(Request.QueryString("idx")))

If idx <> "" Then

	sql = "SELECT * FROM mTb_Member2 WHERE intSeq = " & idx & " AND intGubun = 3 "
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		strName_sub = rs("strName")
		subId = rs("strId")
		subPhone = rs("strPhone")
		subMobile = rs("strMobile")
		subEmail = rs("strEmail")
		department = rs("department")

		If subPhone <> "" Then
			ArrPhone = Split(subPhone, "-")
			If Ubound(ArrPhone) = 2 Then
				subPhone1 = ArrPhone(0)
				subPhone2 = ArrPhone(1)
				subPhone3 = ArrPhone(2)
			End If
		End If

		If subMobile <> "" Then
			ArrMobile = Split(subMobile, "-")
			If Ubound(ArrMobile) = 2 Then
				subMobile1 = ArrMobile(0)
				subMobile2 = ArrMobile(1)
				subMobile3 = ArrMobile(2)
			End If
		End If

		If subEmail <> "" Then
			ArrstrEmail = Split(subEmail, "@")
			If Ubound(ArrstrEmail) = 1 Then
				subEmail1 = ArrstrEmail(0)
				subEmail2 = ArrstrEmail(1)
			End If
		End If
	Else

		Call jsAlertMsgBack("일치하는 정보가 없습니다.")
	End If

	rs.close
	Set rs = Nothing

	Call dbClose()
End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user">

					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>회원정보관리</h2>
					</div>

					<div class="inner">
						<h5 class="section_tit">회원 정보</h5>
						<form name="subjoinform" method="post" action="./add_member_ok.asp">
						<input type="hidden" name="num" value="">
						<input type="hidden" name="idx" value="<%=idx%>">
						<input type="hidden" name="ori_id" value="<%=subId%>">
						<table class="table table_type02">
							<colgroup>
								<col width="145px">
								<col width="*">
							</colgroup>

							<tbody>
								 <tr>
									<th>이름 <span>*</span></th>
									<td><input type="text" name="subname" value="<%=strName_sub%>"></td>
								</tr>
								<tr>
									<th>부서 <span>*</span></th>
									<td><input type="text" name="department" value="<%=department%>"></td>
								</tr>
								<tr>
									<th>아이디 <span>*</span></th>
									<td>
									<% If idx = "" Then %>
										<input type="text" class="id" name="subid" id="subid" style="width:200px" onkeyup="id_check2();return false;"> <!--button class="id_btn btn_bk" onclick="id_check2();return false;">중복체크</button-->
										<span class="add_tx" id="id_ck" style="padding-left:0px; margin-left:0px;"></span>
									<% Else %>
										<%=subId%>
									<% End If %>
									</td>
								</tr>
								<tr>
									<th>비밀번호 <span>*</span></th>
									<td><input type="password" name="subpwd" placeholder="8~20자, 영문, 숫자, 특수문자 사용"></td>
								</tr>
								<tr>
									<th>비밀번호 확인 <span>*</span></th>
									<td><input type="password" name="subpwdRe" placeholder="8~20자, 영문, 숫자, 특수문자 사용"></td>
								</tr>
								<tr class="phone">
									<th>전화번호 </th>
									<td>
										<input type="text" name="subphone1" value="<%=subPhone1%>" style="width:70px" maxlength="4"> &nbsp;-&nbsp;
										<input type="text" name="subphone2" value="<%=subPhone2%>" style="width:70px" maxlength="4">&nbsp; -&nbsp;
										<input type="text" name="subphone3" value="<%=subPhone3%>" style="width:70px" maxlength="4">
									</td>
								</tr>
								<tr class="phone">
									<th>휴대폰 번호 <span>*</span></th>
									<td>
										<input type="text" name="submobile1" value="<%=subMobile1%>" style="width:70px" maxlength="4"> &nbsp;-&nbsp;
										<input type="text" name="submobile2" value="<%=subMobile2%>" style="width:70px" maxlength="4"> &nbsp;-&nbsp;
										<input type="text" name="submobile3" value="<%=subMobile3%>" style="width:70px" maxlength="4">
									</td>
								</tr>
								<tr class="mail">
									<th>이메일 주소 <span>*</span></th>
									<td>
										<input type="text" name="subemail" value="<%=subemail%>">

									</td>
								</tr>
							</tbody>
						</table>
						<div class="btn_box ask_btn_box">
							<button class="btn_blue" onclick="<% If idx = "" Then %>subjoin_chk();<% Else %>subupdate_chk();<% End If %>return false;">확인</button>
						</form>
							<!--button class="btn_wh" type="button" onclick="location.href='user_4.asp'">취소</button-->
						</div>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>