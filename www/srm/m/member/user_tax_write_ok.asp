<!-- #include virtual="/srm/usercheck.asp" -->
<%
RF_idx		= SQL_Injection(Trim(Request.Form("idx")))
RF_intSeq	= SQL_Injection(Trim(Request.Form("intSeq")))
RF_mainYN	= SQL_Injection(Trim(Request.Form("mainYN")))
RF_cNum1	= SQL_Injection(Trim(Request.Form("cNum1")))
RF_cNum2	= SQL_Injection(Trim(Request.Form("cNum2")))
RF_cNum3	= SQL_Injection(Trim(Request.Form("cNum3")))
RF_cName	= SQL_Injection(Trim(Request.Form("cName")))
RF_ceo		= SQL_Injection(Trim(Request.Form("ceo")))
RF_cZip		= SQL_Injection(Trim(Request.Form("cZip")))
RF_cAddr1	= SQL_Injection(Trim(Request.Form("cAddr1")))
RF_cAddr2	= SQL_Injection(Trim(Request.Form("cAddr2")))
RF_cType	= SQL_Injection(Trim(Request.Form("cType")))
RF_cCate	= SQL_Injection(Trim(Request.Form("cCate")))
RF_cPhone1	= SQL_Injection(Trim(Request.Form("cPhone1")))
RF_cPhone2	= SQL_Injection(Trim(Request.Form("cPhone2")))
RF_cPhone3	= SQL_Injection(Trim(Request.Form("cPhone3")))
RF_cMobile1	= SQL_Injection(Trim(Request.Form("cMobile1")))
RF_cMobile2	= SQL_Injection(Trim(Request.Form("cMobile2")))
RF_cMobile3	= SQL_Injection(Trim(Request.Form("cMobile3")))
RF_cEmail1	= SQL_Injection(Trim(Request.Form("cEmail1")))
RF_cEmail2	= SQL_Injection(Trim(Request.Form("cEmail2")))

'// 변수 가공
RF_cNum = RF_cNum1 & "-" & RF_cNum2 & "-" & RF_cNum3
RF_cPhone = RF_cPhone1 & "-" & RF_cPhone2 & "-" & RF_cPhone3
RF_cMobile = RF_cMobile1 & "-" & RF_cMobile2 & "-" & RF_cMobile3
RF_cEmail = RF_cEmail1 & "@" & RF_cEmail2

If RF_mainYN <> "Y" Then
	RF_mainYN = "N"
End If

If RF_idx = "" Then

	Msg = "등록"
	url = "./user_2.asp"

	Sql = "INSERT INTO member_taxbill("
	Sql = Sql & "mainYN, "
	Sql = Sql & "cNum, "
	Sql = Sql & "cName, "
	Sql = Sql & "ceo, "
	Sql = Sql & "cZip, "
	Sql = Sql & "cAddr1, "
	Sql = Sql & "cAddr2, "
	Sql = Sql & "cType, "
	Sql = Sql & "cCate, "
	Sql = Sql & "cPhone, "
	Sql = Sql & "cMobile, "
	Sql = Sql & "cEmail, "
	Sql = Sql & "mem_idx) VALUES("
	Sql = Sql & "'" & RF_mainYN & "',"
	Sql = Sql & "'" & RF_cNum & "',"
	Sql = Sql & "N'" & RF_cName & "',"
	Sql = Sql & "N'" & RF_ceo & "',"
	Sql = Sql & "'" & RF_cZip & "',"
	Sql = Sql & "N'" & RF_cAddr1 & "',"
	Sql = Sql & "N'" & RF_cAddr2 & "',"
	Sql = Sql & "N'" & RF_cType & "',"
	Sql = Sql & "N'" & RF_cCate & "',"
	Sql = Sql & "'" & RF_cPhone & "',"
	Sql = Sql & "'" & RF_cMobile & "',"
	Sql = Sql & "'" & RF_cEmail & "',"
	Sql = Sql & "'" & RF_intSeq & "')"

	dbconn.execute(Sql)
Else

	Msg = "수정"
	url = "./user_tax_write.asp?idx=" & RF_idx

	Sql = "UPDATE member_taxbill SET "
	Sql = Sql & "mainYN='" & RF_mainYN & "', "
	Sql = Sql & "cNum = '" & RF_cNum & "', "
	Sql = Sql & "cName = N'" & RF_cName & "', "
	Sql = Sql & "ceo = N'" & RF_ceo & "', "
	Sql = Sql & "cZip = '" & RF_cZip & "', "
	Sql = Sql & "cAddr1 = N'" & RF_cAddr1 & "', "
	Sql = Sql & "cAddr2 = N'" & RF_cAddr2 & "', "
	Sql = Sql & "cType = N'" & RF_cType & "', "
	Sql = Sql & "cCate = N'" & RF_cCate & "', "
	Sql = Sql & "cPhone = '" & RF_cPhone & "', "
	Sql = Sql & "cMobile = '" & RF_cMobile & "', "
	Sql = Sql & "cEmail = '" & RF_cEmail & "' "
	Sql = Sql & " WHERE bill_idx = " & RF_idx & ""

	dbconn.execute(Sql)
End If

Set rs = Nothing

Call DbClose()

Call jsAlertMsgUrl("" & Msg & "되었습니다.", url)
%>