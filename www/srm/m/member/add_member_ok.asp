<!-- #include virtual="/srm/usercheck.asp" -->
<%
RF_idx			= SQL_Injection(Trim(Request.Form("idx")))
ori_id			= SQL_Injection(Trim(Request.Form("ori_id")))
strName_sub		= SQL_Injection(Trim(Request.Form("subname")))
department		= SQL_Injection(Trim(Request.Form("department")))
subId			= SQL_Injection(Trim(Request.Form("subid")))
subPwd			= SQL_Injection(Trim(Request.Form("subpwd")))
subPhone1		= SQL_Injection(Trim(Request.Form("subphone1")))
subPhone2		= SQL_Injection(Trim(Request.Form("subphone2")))
subPhone3		= SQL_Injection(Trim(Request.Form("subphone3")))
subMobile1		= SQL_Injection(Trim(Request.Form("submobile1")))
subMobile2		= SQL_Injection(Trim(Request.Form("submobile2")))
subMobile3		= SQL_Injection(Trim(Request.Form("submobile3")))
subEmail1		= SQL_Injection(Trim(Request.Form("subemail1")))
subEmail2		= SQL_Injection(Trim(Request.Form("subemail2")))

'// 변수 가공
subPhone = subPhone1 & "-" & subPhone2 & "-" & subPhone3
subMobile = subMobile1 & "-" & subMobile2 & "-" & subMobile3
subEmail = subEmail1 & "@" & subEmail2
intGubun = 3
smsYN = "N"
mailYN = "N"
yeosinYN = "N"

If RF_idx = "" Then

	Sql = "INSERT INTO mTb_Member2("
	Sql = Sql & "intGubun, "
	Sql = Sql & "strId, "
	Sql = Sql & "strPwd, "
	Sql = Sql & "strName, "
	Sql = Sql & "strPhone, "
	Sql = Sql & "strMobile, "
	Sql = Sql & "smsYN, "
	Sql = Sql & "strEmail, "
	Sql = Sql & "mailYN, "
	Sql = Sql & "yeosinYN, "
	Sql = Sql & "cName, "
	Sql = Sql & "cNum, "
	Sql = Sql & "cZip, "
	Sql = Sql & "cAddr1, "
	Sql = Sql & "cAddr2, "
	Sql = Sql & "department, "
	Sql = Sql & "strAgreeId) VALUES("
	Sql = Sql & "'" & intGubun & "',"
	Sql = Sql & "N'" & subId & "',"
	Sql = Sql & "'" & Encrypt_Sha(subPwd) & "',"
	Sql = Sql & "N'" & strName_sub & "',"
	Sql = Sql & "'" & subPhone & "',"
	Sql = Sql & "'" & subMobile & "',"
	Sql = Sql & "'" & smsYN & "',"
	Sql = Sql & "N'" & subEmail & "',"
	Sql = Sql & "'" & mailYN & "',"
	Sql = Sql & "'" & yeosinYN & "',"
	Sql = Sql & "N'" & cName & "',"
	Sql = Sql & "'" & cNum & "',"
	Sql = Sql & "'" & cZip & "',"
	Sql = Sql & "N'" & cAddr1 & "',"
	Sql = Sql & "N'" & cAddr2 & "',"
	Sql = Sql & "N'" & department & "',"
	Sql = Sql & "'" & strId & "')"

	dbconn.execute(Sql)
	dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&Request.Cookies("SRM_ID")&"','"&subId&"','생성','하위회원등록','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

	Call DbClose()

	Call jsAlertMsgUrl("회원 등록이 완료되었습니다.", "./user_4.asp")
Else

	Sql = "UPDATE mTb_Member2 SET "
	Sql = Sql & "strName=N'" & strName_sub & "', "
	Sql = Sql & "strPhone = '" & subPhone & "', "
	Sql = Sql & "strMobile = N'" & subMobile & "', "
	Sql = Sql & "strEmail = N'" & subEmail & "', "
	Sql = Sql & "department = '" & department & "' "
	Sql = Sql & " WHERE intSeq = " & RF_idx & ""

	dbconn.execute(Sql)
	dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&Request.Cookies("SRM_ID")&"','"&ori_id&"','수정','하위회원수정','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

	Call DbClose()

	Call jsAlertMsgUrl("회원 수정이 완료되었습니다.", "./add_member.asp?idx="&RF_idx)
End If
%>