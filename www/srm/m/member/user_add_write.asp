<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script src="/srm/m/js/jquery.navgoco.js"></script>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script src="/srm/m/js/member.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$(".tab_contents").hide();
			$(".sub_con_tab li:first").addClass("active").show();
			$(".tab_contents:first").show();

			$(".sub_con_tab li").click(function() {
				$(".sub_con_tab li").removeClass("active");
				$(this).addClass("active");
				$(".tab_contents").hide();
				var activeTab = $(this).find("a").attr("href");
				$(activeTab).fadeIn();
				return false;
			});

			// 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function() {
				if (window.FileReader) {
					var filename = $(this)[0].files[0].name;
				} else {
					var filename = $(this).val().split('/').pop().split('\\').pop();
				}
				$(this).siblings('.upload_name').val(filename);
			});
		});

		function openDaumPostcode() {new daum.Postcode({
				oncomplete: function(data) {
					// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

					// 각 주소의 노출 규칙에 따라 주소를 조합한다.
					// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
					var fullAddr = ''; // 최종 주소 변수
					var extraAddr = ''; // 조합형 주소 변수

					// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
					if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
						fullAddr = data.roadAddress;

					} else { // 사용자가 지번 주소를 선택했을 경우(J)
						fullAddr = data.jibunAddress;
					}

					// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
					if(data.userSelectedType === 'R'){
						//법정동명이 있을 경우 추가한다.
						if(data.bname !== ''){
							extraAddr += data.bname;
						}
						// 건물명이 있을 경우 추가한다.
						if(data.buildingName !== ''){
							extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
						}
						// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
						fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
					}

					// 우편번호와 주소 정보를 해당 필드에 넣는다.
					document.getElementById('mem_Zip').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('mem_Addr1').value = fullAddr;

					// 커서를 상세주소 필드로 이동한다.
					document.getElementById('mem_Addr2').focus();
				}
			}).open();
		}

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
idx = Request.QueryString("idx")

If idx <> "" Then
	sql = "SELECT * FROM member_addr WHERE addr_idx = " & idx & " AND mem_idx = " & intSeq
	Set rs = dbconn.execute(sql)

	If Not(rs.eof) Then
		mainYN		= rs("mainYN")
		mem_Zip		= rs("mem_Zip")
		mem_Addr1	= rs("mem_Addr1")
		mem_Addr2	= rs("mem_Addr2")
		mem_name	= rs("mem_name")
		mem_phone	= rs("mem_phone")
		mem_mobile	= rs("mem_mobile")
		mem_email	= rs("mem_email")

		If mem_phone <> "" Then
			ArrPhone = Split(mem_phone, "-")
			If Ubound(ArrPhone) = 2 Then
				mem_Phone1 = ArrPhone(0)
				mem_Phone2 = ArrPhone(1)
				mem_Phone3 = ArrPhone(2)
			End If
		End If

		If mem_mobile <> "" Then
			ArrMobile = Split(mem_mobile, "-")
			If Ubound(ArrMobile) = 2 Then
				mem_Mobile1 = ArrMobile(0)
				mem_Mobile2 = ArrMobile(1)
				mem_Mobile3 = ArrMobile(2)
			End If
		End If

		If mem_email <> "" Then
			ArrstrEmail = Split(mem_email, "@")
			If Ubound(ArrstrEmail) = 1 Then
				mem_Email1 = ArrstrEmail(0)
				mem_Email2 = ArrstrEmail(1)
			End If
		End If
	End If

	rs.close
	Set rs = Nothing
End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user">

					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>회원정보관리</h2>
					</div>

					<div class="inner">
						<h5 class="section_tit">배송지 정보</h5>
						<form name="billform" method="post" action="./user_add_write_ok.asp">
						<input type="hidden" name="intSeq" value="<%=intSeq%>">
						<input type="hidden" name="idx" value="<%=idx%>">
						<table class="table table_type02">
							<colgroup>
								<col width="145px">
								<col width="*">
							</colgroup>

							<tbody>
								 <tr>
									<th>대표 배송지</th>
									<td>
										<div class="chk_box radio_box">
											<input id="mainYN" type="checkbox" name="mainYN" value="Y" <% If mainYN = "Y" Then %> checked<% End If %>>
											<label for="mainYN"><span></span>
												<p>대표 배송지로 설정</p>
											</label>
										</div>
									</td>
								</tr>
								<tr class="add">
									<th>주소 (도로명 주소)<span>*</span></th>
									<td><input type="text" class="wd360" name="mem_Zip" id="mem_Zip" readonly value="<%=mem_Zip%>"> <button class="zip_code btn_bk" onclick="openDaumPostcode();return false;">우편번호 검색</button> <input type="text" name="mem_Addr1" id="mem_Addr1" value="<%=mem_Addr1%>" readonly><input type="text" name="mem_Addr2" id="mem_Addr2" value="<%=mem_Addr2%>"></td>
								</tr>
								<tr>
									<th>수령인 <span>*</span></th>
									<td><input type="text" name="mem_name" value="<%=mem_name%>"></td>
								</tr>
								<tr class="phone">
									<th>전화번호 <span>*</span></th>
									<td>
										<input type="text" name="mem_Phone1" style="width:70px" maxlength="4" value="<%=mem_Phone1%>"> &nbsp;-&nbsp;
										<input type="text" name="mem_Phone2" style="width:70px" maxlength="4" value="<%=mem_Phone2%>">&nbsp; -&nbsp;
										<input type="text" name="mem_Phone3" style="width:70px" maxlength="4" value="<%=mem_Phone3%>">
									</td>
								</tr>
								<tr class="phone">
									<th>휴대폰 번호 <span>*</span></th>
									<td>
										<input type="text" name="mem_Mobile1" style="width:70px" maxlength="4" value="<%=mem_Mobile1%>"> &nbsp;-&nbsp;
										<input type="text" name="mem_Mobile2" style="width:70px" maxlength="4" value="<%=mem_Mobile2%>"> &nbsp;-&nbsp;
										<input type="text" name="mem_Mobile3" style="width:70px" maxlength="4" value="<%=mem_Mobile3%>">
									</td>
								</tr>
								<tr class="mail">
									<th>이메일 주소 <span>*</span></th>
									<td>
										<input type="text" name="cEmail" value="<%=cEmail%>">
									</td>
								</tr>
							</tbody>
						</table>
						<div class="btn_box ask_btn_box">
							<button class="btn_blue" onclick="addr_chk();return false;">확인</button>
						</form>
							<!--button class="btn_wh" onclick="location.href='user_3.asp'">취소</button-->
						</div>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>