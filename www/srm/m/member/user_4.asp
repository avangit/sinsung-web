<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script type="text/javascript" src="/srm/m/js/member.js"></script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim useYN				: useYN 			= SQL_Injection(Trim(Request("useYN")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "mTb_Member2"
Dim query_where			: query_where		= " intGubun = 3 AND strAgreeId = '" & strId & "' "
Dim query_orderby		: query_orderby		= " ORDER BY intseq DESC"

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

If useYN <> "" Then
	query_where = query_where & " AND intAction = " & useYN
End If

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user">

					<div class="sub_tit">
						<button class="btn_back" onclick="javascript:history.back();">뒤로가기</button>
						<h2>회원정보관리</h2>
					</div>

					<div class="inner">
						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li><a href="user.asp">회원정보변경</a></li>
								<li><a href="user_2.asp">세금계산서발행정보</a></li>
								<li><a href="user_3.asp">배송지 관리</a></li>
								<li  class="active"><a href="user_4.asp">조직관리</a></li>
							</ul>
							
								
							<div class="tab_contents " id="user_04">
								<div class="table_top">
									<div class="a_option">
										<form name="useform" method="post">
										<select name="useYN" onchange="document.useform.submit();">
											<option value="">전체 사용자</option>
											<option value="0" <% If useYN = "0" Then %> selected<% End If %>>사용</option>
											<option value="1" <% If useYN = "1" Then %> selected<% End If %>>미사용</option>
										</select>
										</form>
									</div>
									<p class="cont"><span><%=intTotalCount%></span>건</p>
									<div class="btn_right">
										<button class="btn_blue" onclick="location.href = 'add_member.asp'">등록하기</button>
									</div>
								</div>

								<form name="list" method="post" action="./user_4_ok.asp">
								<input type="hidden" name="n">
								<input type="hidden" name="m">
								<input type="hidden" name="message">
								<div class="order_list">
									<div id="slide_tab">
<%
If rs.bof Or rs.eof Then
%>
										<div class="lnb">
											<ul class="nav"><li>데이터가 없습니다.</li></ul>
										</div>
<%
Else
	i = 1
	rs.move MoveCount
	Do While Not rs.eof

		If rs("intAction") = 0 Then
			intActionTxt = "사용"
		Else
			intActionTxt = "미사용"
		End If
%>
										<div class="lnb">
											<ul class="nav">
												<li>
													<a href="member_modify.asp">
														<div class="chk_box">
															<input id="team1" type="checkbox">
															<label for="team1"><span></span></label>
														</div>
														<span><%=rs("strName")%></span>
														<span class="code"><%=rs("department")%></span>
														<div class="pro_name">
															<div class="l_box"><p><%=rs("strEmail")%></p>
																<p><%=rs("strPhone")%> <br><%=rs("strMobile")%> </p>
																<span class="custom"><%=intActionTxt%></span>
															</div>
														</div>
													</a>
												</li>

											</ul>
										</div>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
										<!--div class="lnb">
											<ul class="nav">
												<li>
													<a href="member_modify.asp">
														<div class="chk_box">
															<input id="team2" type="checkbox">
															<label for="team2"><span></span></label>
														</div>
														<span>홍길동</span>
														<span class="code">부서B</span>
														<div class="pro_name">
															<div class="l_box"><p>sample@mail.co.kr</p>
																<p>02-1234-1234 <br>010-1234-1234 </p>
																<span class="custom">미사용</span>
															</div>
														</div>
													</a>
												</li>

											</ul>
										</div>
										<div class="lnb">
											<ul class="nav">
												<li>
													<a href="member_modify.asp">
														<div class="chk_box">
															<input id="team3" type="checkbox">
															<label for="team3"><span></span></label>
														</div>
														<span>홍길동</span>
														<span class="code">부서C</span>
														<div class="pro_name">
															<div class="l_box"><p>sample@mail.co.kr</p>
																<p>02-1234-1234 <br>010-1234-1234 </p>
																<span class="custom">사용</span>
															</div>
														</div>
													</a>
												</li>

											</ul>
										</div>
										<div class="lnb">
											<ul class="nav">
												<li>
													<a href="member_modify.asp">
														<div class="chk_box">
															<input id="team4" type="checkbox">
															<label for="team4"><span></span></label>
														</div>
														<span>홍길동</span>
														<span class="code">부서D</span>
														<div class="pro_name">
															<div class="l_box"><p>sample@mail.co.kr</p>
																<p>02-1234-1234 <br>010-1234-1234 </p>
																<span class="custom">사용</span>
															</div>
														</div>
													</a>
												</li>

											</ul>
										</div>
										<div class="lnb">
											<ul class="nav">
												<li>
													<a href="member_modify.asp"> 
														<div class="chk_box">
															<input id="team5" type="checkbox">
															<label for="team5"><span></span></label>
														</div>
														<span>홍길동</span>
														<span class="code">부서E</span>
														<div class="pro_name">
															<div class="l_box"><p>sample@mail.co.kr</p>
																<p>02-1234-1234 <br>010-1234-1234 </p>
																<span class="custom">사용</span>
															</div>
														</div>
													</a>
												</li>

											</ul>
										</div-->
									</div>
								</div>


								<div class="middle_box">

									<div class="btn_small">
										<button class="btn_bk" onclick="javascript:GetCheckbox(document.list, 'etc', '사용');return false;">사용</button>
										<button class="btn_wh" onclick="javascript:GetCheckbox(document.list, 'etc', '미사용');return false;">미사용</button>
										<button class="btn_blue" onclick="javascript:GetCheckbox(document.list, 'del', '삭제');return false;">삭제</button>
									</div>
								</div>
								</form>

								<% Call Paging_user_srm("") %>
								<!--div class="list_paging">
									<a class="paging_prev" href="#">처음</a>
									<ul>
										<li><a href="#">21</a></li>
										<li><a href="#">22</a></li>
										<li class="active"><a href="#">23</a></li>
										<li><a href="#">24</a></li>
										<li><a href="#">25</a></li>
									</ul>
									<a class="paging_next" href="#">맨끝</a>
								</div-->

							</div>

						</div>

					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>