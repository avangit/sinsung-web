	<div class="inner">
		<header>
			<h1 class="logo">
				<a href="/srm/m/"><img src="/srm/m/image/common/logo.png" alt="신성 srm"></a>
			</h1>
			<ul class="user">
			<li><button class="btn_logout" onclick="location.href='/srm/m/logout.asp'">로그아웃</button></li>
				<li><a href="/srm/m/member/user.asp"><i></i></a></li>
				<li><button class="btn_search"><i></i></button></li>
				<li><a href="/srm/m/order/cart.asp"><i><% If cartCNT > 0 Then %><span><%=cartCNT%></span><% End If %></i></a></li>
			</ul>
		</header>

<%
all_fieldvalue 	= SQL_Injection(Trim(Request("all_fieldvalue")))
%>
		<div class="search_wrap">
			<div class="inner">
			<form name="searchForm" method="post" action="/srm/m/b2b/search.asp">
				<button class="btn_close" type="button">닫기</button>
				<input type="text" name="all_fieldvalue" value="<%=all_fieldvalue%>">
				<div class="btn_box">
					<button class="btn_search02" type="button" onclick="document.searchForm.submit();">검색</button>
				</div>
			</form>
            </div>
        </div>

		<ul class="menubar">
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
	<div class="menu_wrap">
		<div>
			<div class="gnb_wrap">
				<ul class="gnb">
					<li><a href="/srm/m/b2b/product.asp" class="gnb_tit"><i></i>B2B 제품</a></li>
				<%' If Request.Cookies("SRM_LEVEL") = 9 Then %>
					<li><a href="/srm/m/order/order_list.asp" class="gnb_tit"><i></i>구매 전자결재</a></li>
				<%' End If %>
					<li><a href="/srm/m/order/order_list2.asp" class="gnb_tit"><i></i>주문배송조회</a></li>
					<li><a href="/srm/m/online/estimate_list.asp" class="gnb_tit"><i></i>견적요청</a></li>
					<li>
						<p href="/srm/m/online/ask_list.asp" class="gnb_tit"><i></i>문의하기</p>
						<ul class="lnb">
							<li><a href="/srm/m/online/ask_list.asp">기술문의</a></li>
							<li><a href="/srm/m/online/ask_list2.asp">기타문의</a></li>
						</ul>
					</li>
				<% If Request.Cookies("SRM_LEVEL") = 2 Or Request.Cookies("SRM_LEVEL") = 9 Then %>
					<li><a href="/srm/m/serial/serial.asp" class="gnb_tit"><i></i>시리얼관리</a></li>
				<% End If %>
					<li>
						<p class="gnb_tit"><i></i>IT자료실</p>
						<ul class="lnb">
							<li><a href="/srm/m/board/it.asp">제품설명서</a></li>
							<li><a href="/srm/m/board/it_manual.asp">기술지원매뉴얼</a></li>
							<li><a href="/srm/m/board/it_video.asp">제품영상</a></li>
						</ul>
					</li>
					<li>
						<a href="/srm/m/board/notice.asp" class="gnb_tit"><i></i>공지사항</a>
					</li>
				</ul>
			</div>
		</div>
	</div>