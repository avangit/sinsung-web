<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>
<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<script src="/srm/m/js/slick.js"></script>
	<link rel="stylesheet" href="/srm/m/_css/slick-theme.css" />
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script src="/srm/m/js/slick.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			//슬라이드
			$('.name_slide').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 4000,
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
If Request.Cookies("SRM_ID") = "" Then
	Call jsAlertUrl("/srm/m/")
Else

	sql = "SELECT * FROM mtb_member2 WHERE strId='" & Request.Cookies("SRM_ID") & "' AND strName = '" & Request.Cookies("SRM_NAME") & "' AND intGubun <> 1 AND intaction = 0 "
	Set rs = dbconn.execute(sql)

	If rs.eof Then
		Call jsAlertMsgBack("일치하는 정보가 없습니다.")
	Else
		intSeq = rs("intSeq")
		manager1 = rs("manager1")
		m1_Email = rs("m1_Email")
		m1_Phone = rs("m1_Phone")
		m1_Mobile = rs("m1_Mobile")
		manager2 = rs("manager2")
		m2_Email = rs("m2_Email")
		m2_Phone = rs("m2_Phone")
		m2_Mobile = rs("m2_Mobile")
		manager3 = rs("manager3")
		m3_Email = rs("m3_Email")
		m3_Phone = rs("m3_Phone")
		m3_Mobile = rs("m3_Mobile")
	End If

	rs.close

	If Request.Cookies("SRM_LEVEL") = 2 Then
		query_where = query_where & " AND b.mem_idx = " & intSeq
	ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
		query_where = query_where & " AND b.mem_idx = " & masterIDX
	End If

	sql = "SELECT TOP 6 a.g_idx, a.g_name, a.g_Money, a.g_simg, b.mem_price FROM GOODS a LEFT JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx WHERE b.mainYN = 'Y' AND a.g_display <> 'home' " & query_where & " ORDER BY b.regdate DESC"
	Set rs = dbconn.execute(sql)
End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main index">
				<div id="sub_contents" class="ask">
				<section class="sec1">
					<div class="name_slide">
						<ul class="name_card name_slide">
						<% If manager1 <> "" Then %>
							<li>
								<div class="tit">
									<h3>영업</h3><img src="/srm/m/image/main/name_card01.png" alt="">
								</div>
								<span>이름</span>
								<p><%=manager1%></p>
								<span>일반전화</span>
								<p><%=m1_Phone%></p>
								<span>휴대폰 번호</span>
								<p><%=m1_Mobile%></p>
								<span>이메일 주소</span>
								<p><%=m1_Email%></p>
							</li>
						<% End If %>
						<% If manager2 <> "" Then %>
							<li>
								<div class="tit">
									<h3>기술 전담</h3><img src="/srm/m/image/main/name_card02.png" alt="">
								</div>
								<span>이름</span>
								<p><%=manager2%></p>
								<span>일반전화</span>
								<p><%=m2_Phone%></p>
								<span>휴대폰 번호</span>
								<p><%=m2_Mobile%></p>
								<span>이메일 주소</span>
								<p><%=m2_Email%></p>
							</li>
						<% End If %>
						<% If manager3 <> "" Then %>
							<li>
								<div class="tit">
									<h3>세금계산서</h3><img src="/srm/m/image/main/name_card03.png" alt="">
								</div>
								<span>이름</span>
								<p><%=manager3%></p>
								<span>일반전화</span>
								<p><%=m3_Phone%></p>
								<span>휴대폰 번호</span>
								<p><%=m3_Mobile%></p>
								<span>이메일 주소</span>
								<p><%=m3_Email%></p>
							</li>
						<% End If %>
						</ul>
					</div>

					</section>
					<div class="inner">
					<section class="sec2">
						<h2>Our Products</h2>
						<ul>
<%
	If rs.bof Or rs.eof Then
%>
							<li>등록된 데이터가 없습니다.</li>
<%
	Else
		Do While Not rs.eof
			g_idx = rs("g_idx")
			g_name = rs("g_name")
			g_Money = rs("g_Money")
			g_simg = rs("g_simg")
			mem_price = rs("mem_price")

			If mem_price = 0 Or mem_price = "" Or IsNULL(mem_price) Then
				price = g_Money
			Else
				price = mem_price
			End If
%>
							<li><a href="/srm/m/b2b/product_view.asp?idx=<%=g_idx%>">
									<div class="img_box">
										<img src="/upload/goods/<%=g_simg%>">
									</div>
									<div class="text_box">
										<p><%=g_name%></p>
										<span class="price"><%=FormatNumber(price, 0)%> 원</span>
									</div>
								</a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
						</ul>
					</section>
				</div>
				<section class="sec3">
					<div class="inner">
						<div class="board">
							<div class="tit_box">
								<h3>문의하기</h3>
								<button type="button" onclick="location.href='/srm/m/online/ask_list.asp'">더보기 <span>+</span></button>
							</div>
							<table class="inq">
								<tbody>
<%
	sql = "SELECT TOP 4 on_idx, on_status, on_title, regdate FROM board_online WHERE on_type = 'tech' AND mem_idx = " & intSeq & " ORDER BY on_idx DESC "
	Set rs = dbconn.execute(sql)

	If rs.bof Or rs.eof Then
%>
									<tr><td colspan="3">등록된 문의내역이 없습니다.</td></tr>
<%
	Else
		Do While Not rs.eof
%>
									<tr onclick="location.href='/srm/m/online/ask_view.asp?idx=<%=rs("on_idx")%>'">
										<td class="tit"><p><%=Cut(rs("on_title"), 30, "...")%></p></td>
										<td class="date"><%=Replace(Left(rs("regdate"), 10), "-", ".")%></td>
										<td class="case"><span><%If rs("on_status") = "Y" Then %>답변완료<% Else %>접수완료<% End If %></span></td>
									</tr>
<%
			intNowNum = intNowNum - 1
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
								</tbody>
							</table>
						</div>
						<div class="board">
							<div class="tit_box">
								<h3>공지사항</h3>
								<button type="button" onclick="location.href='/srm/m/board/notice.asp'">더보기 <span>+</span></button>
							</div>
							<table class="inq">
								<colgroup>
									<col style="width: 90%">
									<col style="width: 100px">
								</colgroup>
								<tbody>
<%
	sql = "SELECT TOP 4 b_idx, b_title, b_writeday FROM BOARD_v1 WHERE b_part = 'board01' AND display_mode <> 'home' ORDER BY option_notice DESC, b_idx DESC "
	Set rs = dbconn.execute(sql)

	If rs.bof Or rs.eof Then
%>
									<tr><td colspan="2">등록된 데이터가 없습니다.</td></tr>
<%
	Else
		Do While Not rs.eof
%>
									<tr onclick="location.href='/srm/board/notice_view.asp?idx=<%=rs("b_idx")%>'">
										<td class="tit"><p><%=Cut(rs("b_title"), 30, "...")%></p></td>
										<td class="date"><%=Replace(rs("b_writeday"), "-", ".")%></td>
									</tr>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
								</tbody>
							</table>
						</div>

						<ul class="banner_area">
<%
	sql = "SELECT TOP 3 * FROM banner WHERE useYN = 'Y' ORDER BY regdate DESC"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
							<li>
								<a href="<% If rs("strLink") <> "" Then %><%=rs("strLink")%><% Else %>javascript:;<% End If %>" target="<%=rs("strTarget")%>">
									<div class="img_box">
										<img src="/upload/banner/<%=rs("strFile1")%>" alt="<%=rs("title")%>">
										<div class="text_box">
											<!--h4>banner</h4>
											<p>abcdefg abcd a abcdef ab abcd <br>
												abcdefghij ab a abcdef</p-->
										</div>
									</div>
								</a>
							</li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
	Set rs = Nothing

	Call DbClose()
%>
						</ul>
					</div>
				</section>

				</div>


			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>