<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<script>
		function goBack() {
		  window.history.back();
		}

		function frmSubmit() {
			$("#sch_Form").submit();
		}
</script>

</head>
<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim fieldvalue2 : fieldvalue2 	=  SQL_Injection(Trim(Request("fieldvalue2")))
Dim b_addtext5 	: b_addtext5 	=  SQL_Injection(Trim(Request("b_addtext5")))
Dim b_addtext6 	: b_addtext6 	=  SQL_Injection(Trim(Request("b_addtext6")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " GOODS.*, BOARD_v1.file_1, BOARD_v1.b_addtext5, BOARD_v1.b_addtext6  "
Dim query_Tablename		: query_Tablename	= "GOODS, BOARD_v1"
Dim query_where			: query_where		= " GOODS.g_type = 1 AND GOODS.g_display <> 'home' AND GOODS.g_act = 'Y' AND BOARD_v1.b_part = 'board07' AND GOODS.g_menual1 = BOARD_v1.b_idx"
Dim query_orderby		: query_orderby		= " ORDER BY GOODS.g_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND GOODS."& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

If b_addtext5 <> "" Then
	query_where = query_where & " AND BOARD_v1.b_addtext5 = '" & b_addtext5 & "'"
End If

If b_addtext6 <> "" Then
	query_where = query_where & " AND BOARD_v1.b_addtext6 = '" & b_addtext6 & "'"
End If

If fieldvalue2 <> "" Then
	query_where = query_where & " AND (BOARD_v1.b_addtext5 LIKE '%" & fieldvalue2 & "%' OR BOARD_v1.b_addtext6 = '" & fieldvalue2 & "' OR GOODS.g_name LIKE '%" & fieldvalue2 & "%')"
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="info video">
		<div id="sub_contents">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>제품설명서</h2>
					</div>

						<div class="search_box">
							<div class="inner">
								<form name="sch_Form" method="POST">
								<div>
									<select name="b_addtext5" id="b_addtext5" class="w140">
										<option value="">&nbsp; ::: 제조사 :::</option>
										<option value="HP" <% If b_addtext5 = "HP" Then %> selected<% End If %>>&nbsp; HP</option>
										<option value="삼성" <% If b_addtext5 = "삼성" Then %> selected<% End If %>>&nbsp; 삼성</option>
										<option value="삼성 단납점 모델" <% If b_addtext5 = "삼성 단납점 모델" Then %> selected<% End If %>>&nbsp; 삼성 단납점 모델</option>
										<option value="LG" <% If b_addtext5 = "LG" Then %> selected<% End If %>>&nbsp; LG</option>
										<option value="HPE" <% If b_addtext5 = "HPE" Then %> selected<% End If %>>&nbsp; HPE</option>
										<option value="NETGEAR" <% If b_addtext5 = "NETGEAR" Then %> selected<% End If %>>&nbsp; NETGEAR</option>
										<option value="ATEN" <% If b_addtext5 = "ATEN" Then %> selected<% End If %>>&nbsp; ATEN</option>
									</select>
									<select name="b_addtext6" id="b_addtext6">
										<option value="">&nbsp; ::: 카테고리 ::: </option>
										<option value="데스크탑/서버" <% If b_addtext6 = "데스크탑/서버" Then %> selected<% End If %>>데스크탑/서버</option>
										<option value="디스플레이/TV" <% If b_addtext6 = "디스플레이/TV" Then %> selected<% End If %>>디스플레이/TV</option>
										<option value="화상회의" <% If b_addtext6 = "화상회의" Then %> selected<% End If %>>화상회의</option>
										<option value="복합기/프린터" <% If b_addtext6 = "복합기/프린터" Then %> selected<% End If %>>복합기/프린터</option>
										<option value="네트워크" <% If b_addtext6 = "네트워크" Then %> selected<% End If %>>네트워크</option>
										<option value="태블릿/모바일" <% If b_addtext6 = "태블릿/모바일" Then %> selected<% End If %>>태블릿/모바일</option>
										<option value="부품/소프트웨어" <% If b_addtext6 = "부품/소프트웨어" Then %> selected<% End If %>>부품/소프트웨어</option>
										<option value="가전" <% If b_addtext6 = "가전" Then %> selected<% End If %>>가전</option>
									</select>
										<input type="text" name="fieldvalue2" value="<%=fieldvalue2%>">
										<button class="btn_search" onclick="frmSubmit();">검색</button>
									</div>
								
								</form>
							</div>
							</div>
					<div class="inner">
						<div class="tab_contents" id="it_01">
						<table class="table info_board down_borad">
							<colgroup>
								<col style="width: 120px">
								<col style="width: 400px">
								<col style="width: 180px">
							</colgroup>
							<thead>
								<tr>
									<th>번호</th>
									<th>파일명</th>
									<th>다운로드</th>
								</tr>
							</thead>
							<tbody>
<%
If rs.bof Or rs.eof Then
%>
								<tr><td colspan="3" align="center">등록된 데이터가 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>

								<tr>
									<td><span><%=intNowNum%></span></td>
									<td class="tit"><p class="over"><%=rs("g_name")%></p></td>
									<td><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download></a></td>
								</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
						</tbody></table>
                        </div>
					<% Call Paging_user_srm("") %>

				</div>
				</div>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>