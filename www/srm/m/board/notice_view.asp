<!-- #include virtual="/srm/usercheck.asp" -->
<%
b_idx = SQL_Injection(Trim(Request.QueryString("idx")))

Call DbOpen()

sql = "UPDATE BOARD_v1 SET b_read = b_read + 1 WHERE b_idx = " & b_idx
dbconn.execute(sql)

sql = "SELECT * FROM BOARD_v1 WHERE b_idx = " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	b_title = rs("b_title")
	b_text = rs("b_text")
	file_1 = rs("file_1")
	file_2 = rs("file_2")
	file_3 = rs("file_3")
	b_read = rs("b_read")
	b_writeday = rs("b_writeday")
End If

rs.Close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container">
				<div id="sub_contents" class="ask order_view">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>공지사항</h2>
					</div>

					<div class="inner">


						<div class="order_section">
							<h5 class="section_tit">공지사항</h5>
							<div class="info_view">
								<div class="tit_box">
									<p><%=b_title%></p>
									<div class="tit_bottom">
										<span>조회수 : <%=b_read%></span>
										<span class="right">작성일 : <%=Left(b_writeday, 10)%></span>
									</div>
								</div>
								<div class="content_box">
									<div class="contents">
										<%=Replace(b_text, Chr(13), "<br>")%>
									</div>
									<div class="file_box">
										<p>첨부파일</p>
										<% If on_file1 <> "" Then %>
										<span><a href="/download.asp?fn=<%=escape(on_file1)%>&ph=online"><%=on_file1%></a></span>
										<% End If %>
										<% If on_file2 <> "" Then %>
										<span><a href="/download.asp?fn=<%=escape(on_file2)%>&ph=online"><%=on_file2%></a></span>
										<% End If %>
									</div>
								</div>
							</div>
						</div>

						<button class="btn_navy" onclick="location.href = 'notice.asp'">목록</button>

					</div>
				</div>
			</div>
		</div>
		<div id="A_Footer">
			<!-- #include virtual="/srm/m/_inc/footer.asp" -->
		</div>
	</div>


</body>

</html>