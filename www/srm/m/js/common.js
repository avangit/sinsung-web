$(document).on('ready', function() {
    // Header
    // $("#A_Header").mouseover(function() {
    //     $(this).addClass('on');
    // });
    // $("#A_Header").mouseleave(function() {
    //     $(this).removeClass('on');
    //     $('.lnb_wrap').stop().slideUp(400);
    // });
    // 스크롤 시 헤더
    window.onscroll = function(){
        if($(document).scrollTop() > 1){
            $("#A_Header").addClass("on");
            // $("#A_Header").mouseleave(function() {
            //     $(this).addClass('on');
            // });
        } else {
            $("#A_Header").removeClass("on");
        }
    }

    var posY;
    function bodyFreezeScroll() {
        posY = $(window).scrollTop();
        $("html").addClass('fix');
        $("html").css("top",-posY);
    }

    function bodyUnfreezeScroll() {
        $("html").removeAttr('class');
        $("html").removeAttr('style');
        posY = $(window).scrollTop(posY);
    }
    // menu open
    menu_bt = 0;
    $('.menubar').click(function(){
        if( menu_bt == 0 ) {
            // bodyFreezeScroll();
            $('.menubar li:eq(0)').animate({'rotate':'45deg', 'top':'10px'},300);
            $('.menubar li:eq(1)').fadeOut();
            $('.menubar li:eq(2)').animate({'rotate':'-45deg', 'top':'10px'},300);
            $("#A_Header").addClass('on');
            $('.menu_wrap').slideDown();
            menu_bt = 1; 
        } else if( menu_bt == 1 ) {
            // bodyUnfreezeScroll();
            $('.menubar li:eq(0)').animate({'rotate':'0', 'top':'0'},300);
            $('.menubar li:eq(1)').fadeIn();
            $('.menubar li:eq(2)').animate({'rotate':'0', 'top':'20px'},300);
            $('.menu_wrap').slideUp();
            setTimeout(function() { 
                $("#A_Header").removeClass('on');
            }, 400);
            menu_bt = 0;
        }       
    });

    $('.gnb_tit').click(function() {
        if($(this).parent().hasClass('open')){
            $(this).parent().removeClass('open');
            $('.lnb').slideUp();
        } else {
            $('.lnb').slideUp();
            $('.gnb_tit').parent().removeClass('open');
            $(this).parent().addClass('open');
            $(this).siblings().stop().slideDown();
        }
    });
	
	     // 검색
    $('#A_Header .user .btn_search').click(function(){
        $('.search_wrap').fadeIn();
    });
    $('.search_wrap .btn_close').click(function() {
        $('.search_wrap').fadeOut();
    });

    $( '.btn_top' ).click( function() {
        $( 'html, body' ).animate( { scrollTop : 0 }, 400 );
        return false;
    } );


});

function goBack() {
  window.history.back();
}
