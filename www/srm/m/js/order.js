$(document).ready(function() {
	$(".modify_btn").click(function () {
		var n = $('.modify_btn').index(this);
		var g_cnt = $(".num:eq("+n+")").val();
		var data = $(this).data('id');

		$.post("/srm/order/cart_ok.asp", {m: 'edt', idx: data, cnt: g_cnt},function(data){
			location.href = '/srm/m/order/cart.asp';
		}); 
	});

	$(".del_btn").click(function () {
		if(confirm('선택하신 제품을 삭제하시겠습니까?')) {
			var data = $(this).data('id');

			$.post("/srm/order/cart_ok.asp", {m: 'del', idx: data},function(data){
				location.href = '/srm/m/order/cart.asp';
			}); 
		} else {
			return false;
		}
	});

	$('.orderPlus').click(function(){ 
		var n = $('.orderPlus').index(this);
		var g_cnt = $(".num:eq("+n+")").val();
		g_cnt = $(".num:eq("+n+")").val(g_cnt*1+1);
	});

	$('.orderMinus').click(function(){ 
		var n = $('.orderMinus').index(this);
		var g_cnt = $(".num:eq("+n+")").val();

		if(g_cnt < 2) {
			alert("최소수량은 1개입니다.");
			return false;
		}

		g_cnt = $(".num:eq("+n+")").val(g_cnt*1-1);
	});

	$("#equal").change(function(){
		if($("#equal").is(":checked")) {
			$("#s_name").val($("#o_name").val());
			$("#s_mobile1").val($("#o_mobile1").val());
			$("#s_mobile2").val($("#o_mobile2").val());
			$("#s_mobile3").val($("#o_mobile3").val());
			$("#s_email1").val($("#o_email1").val());
			$("#s_email2").val($("#o_email2").val());
			$("#s_email3").val($("#o_email3").val());
		} else {
			$("#s_name").val("");
			$("#s_mobile1").val("");
			$("#s_mobile2").val("");
			$("#s_mobile3").val("");
			$("#s_email1").val("");
			$("#s_email2").val("");
			$("#s_email3").val("");
		}
	});

	$("input[name=payment]").click(function() {
		if(document.orderForm.payment[1].checked) {
			$("#bankinfo").css("display", "");
		} else {
			$("#bankinfo").css("display", "none");
			document.orderForm.d_name.value = "";
			document.orderForm.d_bank.value = "";
			document.orderForm.o_cNum.value = "";
			document.orderForm.o_cName.value = "";
			document.orderForm.o_ceo.value = "";
			document.orderForm.o_cemail1.value = "";
			document.orderForm.o_cemail2.value = "";
		}
	});

});

// 체크박스 전체선택
var check = 1;
function checkAll(frm) {
	if (check) {
		for (i = 0; i < frm.length; i++) {
			if (frm[i].type == "checkbox") {
				if (frm[i].checked)
					continue;
				else
					frm[i].checked = true;
			}
		}
		check = 0;
	} else {
		for (i = 0; i < frm.length; i++) {
			if (frm[i].type == "checkbox") {
				if(frm[i].checked) frm[i].checked = false;
				else continue;
			}
		}
		check = 1;
	}
	return false;
}

// 체크박스 값 넘기기
function GetCheckbox(frm, mod, message) {
	var tmp = "";
	var cnt = 0;
	var i;
	var idk = false;

	for (i = 0; i < frm.length; i++) {
//		if (frm[i].type != "checkbox")
//			continue;
//		if (frm[i].checked) {

		if ($("input:checked[id='order_chk" + i + "']").is(":checked")) {
			tmp += $("input:checked[id='order_chk" + i + "']").val() + ",";
			cnt = cnt + 1;
			idk = true;
		}
	}

	if (idk == true) {
		if (mod.indexOf("del") != -1) {
			var cfm = confirm("선택하신 내용을 삭제하시겠습니까?");
		} else {
			if (message != null) {
				var cfm = confirm("선택하신 내용을 '" + message + "' 하시겠습니까?");
			} else {
				var cfm = confirm("선택하신 내용의 상태를 변경하시겠습니까?");
			}
		}

		if (cfm) {
			frm.n.value = tmp.substr(0, tmp.length - 1);
			frm.m.value = mod;
			frm.message.value = message;
			frm.submit();
		} else {
			return false;
		}
	} else {
		window.alert('실행할 데이터를 선택하세요');
		return false;
	}
	return false;
}

function cart_Chk(mod, msg){

	var len = $("input:checkbox[name=gList]:checked").length;

	$("input[name=goods_list]").val("");

	 if(!len || len==0) {
		alert('제품을 선택하여 주십시오.');
		return false;
	 } else {
		var goods_cnt	= new Array();
		var goods_list	= new Array();
		var goods_price	= new Array();
		var goods_price_len = 0;
		var replaceChar = /[ \{\}\[\]\/?.,;:|\)*~`!^\-_+┼<>@\#$%&\'\"\\(\=]/gi;
		var j = 0;

		$("input[name=gList]:checked").each(function(i){
			goods_list[i]	= $(this).val();
			goods_cnt[i]	= $("#g_cnt"+$(this).val()).val();
			goods_price[i]	= $("#price"+$(this).val()).text();
			goods_price[i]	= goods_price[i].replace(replaceChar,"");

			j++;
		});

		$("input[name=goods_list]").val(goods_list.join(','));
		$("input[name=goods_price]").val(goods_price.join(','));
		$("input[name=goods_cnt]").val(goods_cnt.join(','));

		if(!j || j==0) {
			$("input[name=goods_list]").val("");
			$("input[name=goods_price]").val("");
			$("input[name=goods_cnt]").val("");
		}

		if(confirm(msg+' 하시겠습니까?')) {
			if(mod == 'req'){ // 견적요청
				document.goods_Form.action = "/srm/m/b2b/product_estimate.asp";
			} else if(mod == 'print'){ // 견적서출력
				document.goods_Form.action = "/srm/m/order/estimate_print.asp";
			} else if(mod == 'cgood' || mod == 'agood'){ // 선택주문,전체주문
				document.goods_Form.action = "/srm/m/order/order.asp";
			}

			document.goods_Form.submit();
		} 
	 }

}

function ChangeEmail() {
	if (orderForm.o_email3.value == '0') {
		orderForm.o_email2.readOnly = false;
		orderForm.o_email2.value = '';
		orderForm.o_email2.focus();
	} else {
		orderForm.o_email2.readOnly = true;
		orderForm.o_email2.value = orderForm.o_email3.value;
	}
}

function ChangeEmail2() {
	if (orderForm.s_email3.value == '0') {
		orderForm.s_email2.readOnly = false;
		orderForm.s_email2.value = '';
		orderForm.s_email2.focus();
	} else {
		orderForm.s_email2.readOnly = true;
		orderForm.s_email2.value = orderForm.s_email3.value;
	}
}

function ChangeEmail3() {
	if (orderForm.o_cemail3.value == '0') {
		orderForm.o_cemail2.readOnly = false;
		orderForm.o_cemail2.value = '';
		orderForm.o_cemail2.focus();
	} else {
		orderForm.o_cemail2.readOnly = true;
		orderForm.o_cemail2.value = orderForm.o_cemail3.value;
	}
}


function openDaumPostcode(mod) {
	new daum.Postcode({
		oncomplete: function(data) {
			// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

			// 각 주소의 노출 규칙에 따라 주소를 조합한다.
			// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
			var fullAddr = ''; // 최종 주소 변수
			var extraAddr = ''; // 조합형 주소 변수

			// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
			if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
				fullAddr = data.roadAddress;

			} else { // 사용자가 지번 주소를 선택했을 경우(J)
				fullAddr = data.jibunAddress;
			}

			// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
			if(data.userSelectedType === 'R'){
				//법정동명이 있을 경우 추가한다.
				if(data.bname !== ''){
					extraAddr += data.bname;
				}
				// 건물명이 있을 경우 추가한다.
				if(data.buildingName !== ''){
					extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}
				// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
				fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
			}

			if(mod == "order"){
				// 우편번호와 주소 정보를 해당 필드에 넣는다.
				document.getElementById('s_zip').value = data.zonecode; //5자리 새우편번호 사용
				document.getElementById('s_addr1').value = fullAddr;

				// 커서를 상세주소 필드로 이동한다.
				document.getElementById('s_addr2').focus();
			} else if(mod == "member") {
			}
		}
	}).open();
}

function order_chk() {
	var frm = document.orderForm;

	if(frm.o_company.value == "") {
		alert("회사명을 입력해 주세요.");
		frm.o_company.focus();
		return false;
	} else if(frm.o_name.value == "") {
		alert("주문자명을 입력해 주세요.");
		frm.o_name.focus();
		return false;
	} else if(frm.o_department.value == "") {
		alert("주문부서를 입력해 주세요.");
		frm.o_department.focus();
		return false;
	} else if(frm.o_mobile1.value == "" || frm.o_mobile2.value == "" || frm.o_mobile3.value == "") {
		alert("휴대폰 번호를 입력해 주세요.");
		frm.o_mobile1.focus();
		return false;
	} else if(frm.o_email1.value == "" || frm.o_email2.value == "") {
		alert("이메일 주소를 입력해 주세요.");
		frm.o_email1.focus();
		return false;
	} else if(frm.s_name.value == "") {
		alert("수령인을 입력해 주세요.");
		frm.s_name.focus();
		return false;
	} else if(frm.s_phone1.value == "" || frm.s_phone2.value == "" || frm.s_phone3.value == "") {
		alert("연락처를 입력해 주세요.");
		frm.s_phone1.focus();
		return false;
	} else if(frm.s_mobile1.value == "" || frm.s_mobile2.value == "" || frm.s_mobile3.value == "") {
		alert("휴대폰 번호를 입력해 주세요.");
		frm.s_mobile1.focus();
		return false;
	} else if(frm.s_email1.value == "" || frm.s_email2.value == "") {
		alert("이메일 주소를 입력해 주세요.");
		frm.s_email1.focus();
		return false;
	} else if(frm.s_zip.value == "" || frm.s_addr1.value == "") {
		alert("주소를 입력해 주세요.");
		openDaumPostcode('order');
		return false;
	} else if(frm.payment[1].checked == true && frm.d_name.value == "") {
		alert("입금자명을 입력해 주세요.");
		frm.d_name.focus();
		return false;
	} else if(frm.payment[1].checked == true && frm.d_bank.value == "") {
		alert("결제계좌정보를 선택해 주세요.");
		frm.d_bank.focus();
		return false;
	} else if(frm.o_cNum.value != "" && frm.taxConfirm.value != "Y") {
		alert("세금계산서 정보 확인을 선택해 주세요.");
		frm.taxConfirm.focus();
		return false;
	} else {
		if(frm.payment[0].checked == true) {
			frm.action = "./order_trade.asp"
		} else {
			frm.action = "./order_ok.asp"
		}
		frm.submit();
	}
}

function exchange_write() {
	if(document.exchangeForm.reason.value == "") {
		alert("교환사유를 입력해 주세요.");
		document.exchangeForm.reason.focus();
		return false;
	} else {
		document.exchangeForm.submit();
	}
}

function return_write() {
	if(document.returnForm.reason.value == "") {
		alert("반품 사유를 입력해 주세요.");
		document.returnForm.reason.focus();
		return false;
	} else {
		document.returnForm.submit();
	}
}