function ChangeEmail() {
	if (onlineform.on_email3.value == '0') {
		onlineform.on_email2.readOnly = false;
		onlineform.on_email2.value = '';
		onlineform.on_email2.focus();
	} else {
		onlineform.on_email2.readOnly = true;
		onlineform.on_email2.value = onlineform.on_email3.value;
	}
}

function ask_chk() {
	var frm=document.onlineform;

	if(frm.on_company.value == "") {
		alert("회사명을 입력해 주세요.");
		frm.on_company.focus();
		return false;
	} else if(frm.on_name.value == "") {
		alert("담당자를 입력해 주세요.");
		frm.on_name.focus();
		return false;
	} else if(frm.on_mobile1.value == "" || frm.on_mobile2.value == "" || frm.on_mobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(frm.on_email1.value == "" || frm.on_email2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.on_email1.focus();
		return false;
	} else if(frm.on_title.value == "") {
		alert("제목을 입력해 주세요.");
		frm.on_title.focus();
		return false;
	} else if(frm.on_content.value == "") {
		alert("내용을 입력해 주세요.");
		frm.on_content.focus();
		return false;
	} else {
		frm.submit();
	}
}

function goBack() {
  window.history.back();
}

// 체크박스 값 넘기기
function GetCheckbox(frm, mod, message) {
	var tmp = "";
	var cnt = 0;
	var i;
	var idk = false;

	for (i = 0; i < frm.length; i++) {
//		if (frm[i].type != "checkbox")
//			continue;
//		if (frm[i].checked) {

		if ($("input:checked[id='pro0" + i + "']").is(":checked")) {
			tmp += $("input:checked[id='pro0" + i + "']").val() + ",";
			cnt = cnt + 1;
			idk = true;
		}
	}

	if (idk == true) {
		if (mod.indexOf("del") != -1) {
			var cfm = confirm("선택하신 내용을 삭제하시겠습니까?");
		} else if (mod == "compare") {
			if (cnt < 2) {
				var cfm = confirm("비교할 대상은 최소 2개를 선택해 주세요");
				return false;
			} else {
				var cfm = confirm("선택하신 제품을 비교하시겠습니까?");
			}
		} else {
			if (message != null) {
				var cfm = confirm("선택하신 내용을 '" + message + "' 하시겠습니까?");
			} else {
				var cfm = confirm("선택하신 내용의 상태를 변경하시겠습니까?");
			}
		}

		if (cfm) {
			frm.n.value = tmp.substr(0, tmp.length - 1);
			frm.m.value = mod;
			frm.message.value = message;
			frm.submit();
		} else {
			return false;
		}
	} else {
		window.alert('실행할 제품을 선택하세요');
		return false;
	}
	return;
}