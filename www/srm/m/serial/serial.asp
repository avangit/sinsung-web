<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/m/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/m/_css/style.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="/srm/m/js/jquery.navgoco.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(5)").addClass("active");

			$("#sdate").datepicker({
				showOn: "both",
				buttonImageOnly: true,
				buttonImage: "/srm/m/image/common/ic_date.png",
				dateFormat: "yy-mm-dd"
			});
			$("#edate").datepicker({
				showOn: "both",
				buttonImageOnly: true,
				buttonImage: "/srm/m/image/common/ic_date.png",
				dateFormat: "yy-mm-dd"
			});

			$("#serial_chg").click(function(){
				var frm = document.list;

				 var len = parseInt($("input[name^=serial]").length);
				 $("input[name=arr_val_list]").val("");

				 if(!len || len==0) {
					alert('데이터가 없습니다.');
				 } else {
					var arr_val_list = new Array();
					var j = 0;

					$("input[name^=serial]").each(function(i){
						arr_val_list[i] = $(this).attr("rel");
						j++;
					});

					$("input[name=arr_val_list]").val(arr_val_list.join('@'));

					if(!j || j==0) {
						$("input[name=arr_val_list]").val("");
					} else {
						document.list.submit();
					}
				 }
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/m/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim dayday 		: dayday 		=  SQL_Injection(Trim(Request("d")))
Dim sdate 		: sdate 		=  SQL_Injection(Trim(Request("sdate")))
Dim edate 		: edate 		=  SQL_Injection(Trim(Request("edate")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 10
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "orders"
Dim query_where			: query_where		= " o_status = 4"
Dim query_orderby		: query_orderby		= " ORDER BY o_idx DESC"

If dayday = "" Then
	sdate = ""
	edate = ""
ElseIf dayday = "7" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -7, Date)
	edate = Date
ElseIf dayday = "30" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -30, Date)
	edate = Date
ElseIf dayday = "365" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -365, Date)
	edate = Date
End If

If sdate <> "" And edate <> "" Then
	query_where = query_where & " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day, 1, '" & edate & "')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where & " AND regdate >= '" & sdate & "'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where & " AND regdate <= DATEADD(day, 1, '" & edate & "')"
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="pd0">
				<div id="sub_contents" class="serial table_type01 order_list">
					<div class="sub_tit">
						<button class="btn_back" onclick="goBack()">뒤로가기</button>
						<h2>시리얼 관리</h2>
					</div>

					<div class="search_box2">
						<div class="tab_btn">
							<button onclick="location.href='./serial.asp'" <% If dayday = "" Then %> class="on"<% End If %>>전체</button>
							<button onclick="location.href='./serial.asp?d=7'" <% If dayday = "7" Then %> class="on"<% End If %>>1주일</button>
							<button onclick="location.href='./serial.asp?d=30'" <% If dayday = "30" Then %> class="on"<% End If %>>1개월</button>
							<button onclick="location.href='./serial.asp?d=365'" <% If dayday = "365" Then %> class="on"<% End If %>>1년</button>
						</div>
						<div>
							<div class="date_box"><input type="text" id="sdate" name="sdate" class="date" value="<%=sdate%>" autocomplete="off"></div>
							<div class="date_box"><input type="text" id="edate" name="edate" class="date" value="<%=edate%>" autocomplete="off"></div>
						</div>

					</div>

					<div class="inner">
						<div class="search_box">
							<input type="text" name="fieldvalue" id="fieldvalue" value="<%=fieldvalue%>">
							<button class="btn_search" onclick="document.sch_Form.submit();">검색</button>
						</div>

						<form name="list" method="post" action="./serial_exec.asp" style="margin:0;">
						<input type="hidden" name="n">
						<input type="hidden" name="arr_val_list">
						<input type="hidden" name="page" value="<%=intNowPage%>">
						<div id="slide_tab">
<%
If rs.bof Or rs.eof Then
%>
							<div class="lnb">판매완료된 내역이 없습니다.</div>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
		o_idx			= rs("o_idx")
		o_num			= rs("o_num")
		o_company 		= rs("o_company")
		o_name 			= rs("o_name")
		g_idx 			= rs("g_idx")
		o_serial		= rs("o_serial")
		o_department 	= rs("o_department")
		regdate 		= rs("regdate")

		strSQL = "SELECT g_name FROM GOODS WHERE g_idx IN ( " & g_idx & ")"
		Set goodsRS = dbconn.execute(strSQL)

		If Not goodsRS.eof Then
			g_name = goodsRS("g_name")
		End If

		goodsRS.close
		Set goodsRS = Nothing

		If InStr(g_idx, ",") > 0 Then
			totalCnt = Ubound(Split(g_idx, ",")) + 1
		Else
			totalCnt = 1
		End If

		If totalCnt > 1 Then
			valCnt = "외 " & totalCnt - 1 & "건"
		End If
%>
							<div class="lnb">
								<ul class="nav">
									<li>
										<!--div class="chk_box">
											<input id="serial01" type="checkbox">
											<label for="serial01"><span></span></label>
										</div-->
										<a href="#">
											<span><%=Left(regdate, 10)%></span>
											<span class="code"><input type="text" name="serial<%=rs("o_idx")%>" value="<%=o_serial%>" rel="<%=o_idx%>"></span>
											<div class="pro_name"><%=g_name%><%=valCnt%></div>
										</a>
										<ul class="ss_menu">
											<li><a href="#.asp">
													<dl class="tit">
														<dt>회사명</dt>
														<dd><%=o_company%></dd>
													</dl>
													<dl>
														<dt>사용자</dt>
														<dd><%=o_name%></dd>
													</dl>
													<dl>
														<dt>부서</dt>
														<dd><%=o_department%></dd>
													</dl>
													<button class="btn_blue" onclick="">저장</button>
												</a></li>
										</ul>
									</li>

								</ul>
							</div>
<%
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
							</form>
							<script type="text/javascript">
								$(document).ready(function() {
									$(".nav").navgoco({
										caretHtml: '',
										accordion: false,
										openClass: 'open',
										save: true,
										cookie: {
											name: 'navgoco',
											expires: false,
											path: '/'
										},
										slide: {
											duration: 400,
											easing: 'swing'
										},
									});

									$("#collapseAll").click(function(e) {
										e.preventDefault();
										$(".nav").navgoco('toggle', false);
									});

									$("#expandAll").click(function(e) {
										e.preventDefault();
										$(".nav").navgoco('toggle', true);
									});
								});

							</script>
						</div>


						<% Call Paging_user_srm("") %>

					</div>
				</div>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/m/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>