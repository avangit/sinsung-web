<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script type="text/javascript" src="/srm/js/member.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child()").addClass("active");
			$(".tab_contents").hide();
			$(".sub_con_tab li:first").addClass("active").show();
			$(".tab_contents:first").show();
		});

		function chg_val() {
			document.subjoinform.num = "";
		}
	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
idx = SQL_Injection(Trim(Request.QueryString("idx")))

If idx <> "" Then

	sql = "SELECT * FROM mTb_Member2 WHERE intSeq = " & idx & " AND intGubun = 3 "
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		strName_sub = rs("strName")
		subId = rs("strId")
		subPhone = rs("strPhone")
		subMobile = rs("strMobile")
		subEmail = rs("strEmail")
		department = rs("department")

		If subPhone <> "" Then
			ArrPhone = Split(subPhone, "-")
			If Ubound(ArrPhone) = 2 Then
				subPhone1 = ArrPhone(0)
				subPhone2 = ArrPhone(1)
				subPhone3 = ArrPhone(2)
			End If
		End If

		If subMobile <> "" Then
			ArrMobile = Split(subMobile, "-")
			If Ubound(ArrMobile) = 2 Then
				subMobile1 = ArrMobile(0)
				subMobile2 = ArrMobile(1)
				subMobile3 = ArrMobile(2)
			End If
		End If

		If subEmail <> "" Then
			ArrstrEmail = Split(subEmail, "@")
			If Ubound(ArrstrEmail) = 1 Then
				subEmail1 = ArrstrEmail(0)
				subEmail2 = ArrstrEmail(1)
			End If
		End If
	Else

		Call jsAlertMsgBack("일치하는 정보가 없습니다.")
	End If

	rs.close
	Set rs = Nothing

	Call dbClose()
End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user_add user">
					<div class="inner">
						<div class="sub_tit">
							<h2>회원정보관리</h2>
						</div>
						<div class="section_tit">
							<h4>회원 정보</h4>
						</div>
						<form name="subjoinform" method="post" action="./add_member_ok.asp">
						<input type="hidden" name="num" value="">
						<input type="hidden" name="idx" value="<%=idx%>">
						<input type="hidden" name="ori_id" value="<%=subId%>">
						<table class="table_type02 table">
							<tbody>
								<tr>
									<th>이름 <span>*</span></th>
									<td><input type="text" name="subname" value="<%=strName_sub%>"></td>
								</tr>
								<tr>
									<th>부서 <span>*</span></th>
									<td><input type="text" name="department" value="<%=department%>"></td>
								</tr>
								<tr class="id">
									<th>아이디 <% If idx = "" Then %><span>*</span><% End If %></th>
									<td>
									<% If idx = "" Then %>
										<input type="text" class="id" name="subid" id="subid" onkeyup="id_check2();return false;"> <!--button class="id_btn btn_bk" onclick="id_check2();return false;">중복체크</button-->
										<span class="add_tx" id="id_ck" style="padding-left:0px; margin-left:0px;"></span>
									<% Else %>
										<%=subId%>
									<% End If %>
									</td>
								</tr>
								<tr>
									<th>비밀번호 <% If idx = "" Then %><span>*</span><% End If %></th>
									<td><input type="password" name="subpwd" placeholder="8~20자, 영문, 숫자, 특수문자 사용"></td>
								</tr>
								<tr>
									<th>비밀번호 확인 <% If idx = "" Then %><span>*</span><% End If %></th>
									<td><input type="password" name="subpwdRe"></td>
								</tr>

								<tr class="phone">
									<th>전화번호 </th>
									<td>
										<input type="text" name="subphone1" value="<%=subPhone1%>" maxlength="4"> &nbsp;-&nbsp;
										<input type="text" name="subphone2" value="<%=subPhone2%>" maxlength="4">&nbsp; -&nbsp;
										<input type="text" name="subphone3" value="<%=subPhone3%>" maxlength="4">

									</td>
								</tr>
								<tr class="phone">
									<th>휴대폰 번호 <span>*</span></th>
									<td>
										<input type="text" name="submobile1" value="<%=subMobile1%>" maxlength="4"> &nbsp;-&nbsp;
										<input type="text" name="submobile2" value="<%=subMobile2%>" maxlength="4"> &nbsp;-&nbsp;
										<input type="text" name="submobile3" value="<%=subMobile3%>" maxlength="4">
									</td>
								</tr>
								<tr class="mail">
									<th>이메일 주소 <span>*</span></th>
									<td>
										<input type="text" name="subemail1" value="<%=subEmail1%>">&shy; @ &shy;
										<input type="text" name="subemail2" value="<%=subEmail2%>">
										<label>
											<select name=="subemail3" id="subemail3" onchange="ChangeEmail2();">
												<option value="0">직접입력</option>
												<option value="dreamwiz.co.kr" <% If subEmail2 = "dreamwiz.co.kr" Then %> selected<% End If %>>dreamwiz.co.kr</option>
												<option value="empal.com" <% If subEmail2 = "empal.com" Then %> selected<% End If %>>empal.com</option>
												<option value="gmail.com" <% If subEmail2 = "gmail.com" Then %> selected<% End If %>>gmail.com</option>
												<option value="lycos.co.kr" <% If subEmail2 = "lycos.co.kr" Then %> selected<% End If %>>lycos.co.kr</option>
												<option value="naver.com" <% If subEmail2 = "naver.com" Then %> selected<% End If %>>naver.com</option>
												<option value="nate.com" <% If subEmail2 = "nate.com" Then %> selected<% End If %>>nate.com</option>
												<option value="netsgo.com" <% If subEmail2 = "netsgo.com" Then %> selected<% End If %>>netsgo.com</option>
												<option value="hanmail.net" <% If subEmail2 = "hanmail.net" Then %> selected<% End If %>>hanmail.net</option>
												<option value="hotmail.com" <% If subEmail2 = "hotmail.com" Then %> selected<% End If %>>hotmail.com</option>
												<option value="paran.com" <% If subEmail2 = "paran.com" Then %> selected<% End If %>>paran.com</option>
											</select>
										</label>
									</td>
								</tr>
							</tbody>
						</table>

						<div class="btn_box ask_btn_box">
							<button class="btn_wh" type="button" onclick="location.href='user_4.asp'">취소</button>
							<button class="btn_blue" onclick="<% If idx = "" Then %>subjoin_chk();<% Else %>subupdate_chk();<% End If %>return false;">확인</button>
						</div>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>