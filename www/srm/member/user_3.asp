<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script type="text/javascript">
		$(document).on('ready', function() {
//			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");

		    // 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function(){
				if(window.FileReader){
					var filename = $(this)[0].files[0].name;
				}
				else {
					var filename = $(this).val().split('/').pop().split('\\').pop();
				}
				$(this).siblings('.upload_name').val(filename);
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "member_addr"
Dim query_where			: query_where		= " mem_idx = '" & intSeq & "' "
Dim query_orderby		: query_orderby		= " ORDER BY addr_idx DESC"

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user">
					<div class="inner">
						<div class="sub_tit">
							<h2>회원정보관리</h2>
						</div>

						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li><a href="user.asp">회원정보변경</a></li>
								<li><a href="user_2.asp">세금계산서발행정보</a></li>
								<li  class="active"><a href="user_3.asp">배송지 관리</a></li>
							<% If Request.Cookies("SRM_LEVEL") = 2 Or Request.Cookies("SRM_LEVEL") = 9 Then %>
								<li><a href="./user_4.asp">조직관리</a></li>
							<% End If %>
							</ul>

							<div class="tab_contents" id="user_03">
								<p class="cont"><span><%=intTotalCount%></span>건</p>
								<div class="btn_right">
									<button class="btn_blue" onclick="location.href='user_add_write.asp'">등록하기</button>
								</div>
								<table class="table tax_table">
									<colgroup>
										<col width="80px">
										<col width="120px">
										<col width="250px">
										<col width="180px">
										<col width="100px">
										<col width="220px;">
									</colgroup>
									<thead>
										<tr>
											<th>번호</th>
											<th>수취인</th>
											<th>주소</th>
											<th>휴대폰번호<br>전화번호</th>
											<th>등록일</th>
											<th>관리</th>
										</tr>
									</thead>
									<tbody>
<%
If rs.bof Or rs.eof Then
%>
										<tr>
											<td colspan="6" align="center">데이터가 없습니다.</td>
										</tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>
										<tr>
											<td><%=intNowNum%></td>
											<td><% If rs("mainYN") = "Y" Then %><span class="repre">[대표]</span><% End If %><%=rs("mem_name")%></td>
											<td><%=rs("mem_Addr1")%>&nbsp;<%=rs("mem_Addr2")%></td>
											<td><%=rs("mem_mobile")%><% If rs("mem_phone") <> "" Then %><br><%=rs("mem_phone")%><% End If %></td>
											<td><%=Left(rs("regdate"), 10)%></td>
											<td class="btn_area">
												<button class="btn_blue btn_modify" onclick="location.href='user_add_write.asp?idx=<%=rs("addr_idx")%>'">수정</button>
												<button class="btn_wh btn_del" onclick="if(confirm('삭제하시겠습니까?')){location.href='user_add_write_ok_get.asp?idx=<%=rs("addr_idx")%>'}else{return false;}">삭제</button>
											</td>
										</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
									</tbody>
								</table>

								<% Call Paging_user_srm("") %>
								<!--div class="list_paging">
									<a class="paging_prev" href="#">처음</a>
									<ul>
										<li><a href="#">21</a></li>
										<li><a href="#">22</a></li>
										<li class="active"><a href="#">23</a></li>
										<li><a href="#">24</a></li>
										<li><a href="#">25</a></li>
									</ul>
									<a class="paging_next" href="#">맨끝</a>
								</div-->
							</div>

						</div>


					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>