<!-- #include virtual="/srm/usercheck.asp" -->
<%
RF_idx		= SQL_Injection(Trim(Request.Form("idx")))
RF_intSeq	= SQL_Injection(Trim(Request.Form("intSeq")))
RF_mainYN	= SQL_Injection(Trim(Request.Form("mainYN")))
RF_Zip		= SQL_Injection(Trim(Request.Form("mem_Zip")))
RF_Addr1	= SQL_Injection(Trim(Request.Form("mem_Addr1")))
RF_Addr2	= SQL_Injection(Trim(Request.Form("mem_Addr2")))
RF_Name		= SQL_Injection(Trim(Request.Form("mem_name")))
RF_Phone1	= SQL_Injection(Trim(Request.Form("mem_Phone1")))
RF_Phone2	= SQL_Injection(Trim(Request.Form("mem_Phone2")))
RF_Phone3	= SQL_Injection(Trim(Request.Form("mem_Phone3")))
RF_Mobile1	= SQL_Injection(Trim(Request.Form("mem_Mobile1")))
RF_Mobile2	= SQL_Injection(Trim(Request.Form("mem_Mobile2")))
RF_Mobile3	= SQL_Injection(Trim(Request.Form("mem_Mobile3")))
RF_Email1	= SQL_Injection(Trim(Request.Form("cEmail1")))
RF_Email2	= SQL_Injection(Trim(Request.Form("cEmail2")))

'// 변수 가공
RF_Phone = RF_Phone1 & "-" & RF_Phone2 & "-" & RF_Phone3
RF_Mobile = RF_Mobile1 & "-" & RF_Mobile2 & "-" & RF_Mobile3
RF_Email = RF_Email1 & "@" & RF_Email2

If RF_mainYN <> "Y" Then
	RF_mainYN = "N"
End If

If RF_idx = "" Then

	Msg = "등록"

	Sql = "INSERT INTO member_addr("
	Sql = Sql & "mainYN, "
	Sql = Sql & "mem_Zip, "
	Sql = Sql & "mem_Addr1, "
	Sql = Sql & "mem_Addr2, "
	Sql = Sql & "mem_name, "
	Sql = Sql & "mem_phone, "
	Sql = Sql & "mem_mobile, "
	Sql = Sql & "mem_email, "
	Sql = Sql & "mem_idx) VALUES("
	Sql = Sql & "'" & RF_mainYN & "',"
	Sql = Sql & "'" & RF_Zip & "',"
	Sql = Sql & "N'" & RF_Addr1 & "',"
	Sql = Sql & "N'" & RF_Addr2 & "',"
	Sql = Sql & "N'" & RF_Name & "',"
	Sql = Sql & "'" & RF_Phone & "',"
	Sql = Sql & "'" & RF_Mobile & "',"
	Sql = Sql & "'" & RF_Email & "',"
	Sql = Sql & "'" & RF_intSeq & "')"

	dbconn.execute(Sql)
Else

	Msg = "수정"

	Sql = "UPDATE member_addr SET "
	Sql = Sql & "mainYN='" & RF_mainYN & "', "
	Sql = Sql & "mem_Zip = '" & RF_Zip & "', "
	Sql = Sql & "mem_Addr1 = N'" & RF_Addr1 & "', "
	Sql = Sql & "mem_Addr2 = N'" & RF_Addr2 & "', "
	Sql = Sql & "mem_name = N'" & RF_Name & "', "
	Sql = Sql & "mem_phone = '" & RF_Phone & "', "
	Sql = Sql & "mem_mobile = '" & RF_Mobile & "', "
	Sql = Sql & "mem_email = '" & RF_Email & "' "
	Sql = Sql & " WHERE addr_idx = " & RF_idx & ""

	dbconn.execute(Sql)
End If

Set rs = Nothing

Call DbClose()

Call jsAlertMsgUrl("" & Msg & "되었습니다.", "./user_3.asp")
%>