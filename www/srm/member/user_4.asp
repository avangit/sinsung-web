<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script type="text/javascript" src="/srm/js/member.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
//			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");

		    // 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function(){
				if(window.FileReader){
					var filename = $(this)[0].files[0].name;
				}
				else {
					var filename = $(this).val().split('/').pop().split('\\').pop();
				}
				$(this).siblings('.upload_name').val(filename);
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim useYN				: useYN 			= SQL_Injection(Trim(Request("useYN")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "mTb_Member2"
Dim query_where			: query_where		= " intGubun = 3 AND strAgreeId = '" & strId & "' "
Dim query_orderby		: query_orderby		= " ORDER BY intseq DESC"

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

If useYN <> "" Then
	query_where = query_where & " AND intAction = " & useYN
End If

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user">
					<div class="inner">
						<div class="sub_tit">
							<h2>회원정보관리</h2>
						</div>


						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li><a href="user.asp">회원정보변경</a></li>
								<li><a href="user_2.asp">세금계산서발행정보</a></li>
								<li><a href="user_3.asp">배송지 관리</a></li>
								<li  class="active"><a href="user_4.asp">조직관리</a></li>
							</ul>

							<div class="tab_contents " id="user_04">
								<div class="order_list table_type01">
									<div class="table_top">
										<div class="a_option">
											<form name="useform" method="post">
											<select name="useYN" onchange="document.useform.submit();">
												<option value="">전체 사용자</option>
												<option value="0" <% If useYN = "0" Then %> selected<% End If %>>사용</option>
												<option value="1" <% If useYN = "1" Then %> selected<% End If %>>미사용</option>
											</select>
											</form>
										</div>
										<div class="btn_right">
											<button class="btn_blue" onclick="location.href='add_member.asp'">등록하기</button>
										</div>
									</div>
									<p class="cont"><span><%=intTotalCount%></span>건</p>
									<form name="list" method="post" action="./user_4_ok.asp">
									<input type="hidden" name="n">
									<input type="hidden" name="m">
									<input type="hidden" name="message">
									<table class="table">
										<colgroup>
											<col style="width:60px">
											<col style="width:100px">
											<col style="width:100px">
											<col style="width:180px">
											<col style="width:120px">
											<col style="width:120px">
											<col style="width:80px">
											<col style="width:80px">
										</colgroup>
										<thead>
											<tr>
												<th>
													<div class="chk_box">
														<input id="add_m_chk01" type="checkbox" value="" onclick="checkAll(document.list);">
														<label for="add_m_chk01"><span></span></label>
													</div>
												</th>
												<th>이름</th>
												<th>아이디</th>
												<th>이메일</th>
												<th>전화번호</th>
												<th>휴대폰번호</th>
												<th>부서명</th>
												<th>상태</th>
											</tr>
										</thead>
										<tbody>
<%
If rs.bof Or rs.eof Then
%>
										<tr>
											<td colspan="8" align="center">데이터가 없습니다.</td>
										</tr>
<%
Else
	i = 1
	rs.move MoveCount
	Do While Not rs.eof

		If rs("intAction") = 0 Then
			intActionTxt = "사용"
		Else
			intActionTxt = "미사용"
		End If
%>
											<tr>
												<td>
													<div class="chk_box">
														<input id="add_m_chk02<%=i%>" type="checkbox" value="<%=rs("intSeq")%>">
														<label for="add_m_chk02<%=i%>"><span></span></label>
													</div>
												</td>
												<td><%=rs("strName")%></td>
												<td><a href="./add_member.asp?idx=<%=rs("intSeq")%>"><%=rs("strId")%></a></td>
												<td><%=rs("strEmail")%></td>
												<td><%=rs("strPhone")%></td>
												<td><%=rs("strMobile")%></td>
												<td><%=rs("department")%></td>
												<td><%=intActionTxt%></td>
											</tr>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
										</tbody>
									</table>
									<div class="btn_box save_btn">
										<div class="left">
											<button class="btn_bk" onclick="javascript:GetCheckbox(document.list, 'etc', '사용');return false;">사용</button>
											<button class="btn_wh" onclick="javascript:GetCheckbox(document.list, 'etc', '미사용');return false;">미사용</button>
											<button class="btn_blue" onclick="javascript:GetCheckbox(document.list, 'del', '삭제');return false;">삭제</button>
										</div>
									</div>
									</form>

									<% Call Paging_user_srm("") %>
								</div>
							</div>
						</div>


					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>