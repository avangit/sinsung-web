<!-- #include virtual="/srm/usercheck.asp" -->
<%
intseq		= SQL_Injection(Trim(Request("n")))
mode		= SQL_Injection(Trim(Request("m")))
message		= SQL_Injection(Trim(Request("message")))

If intseq = "" Or ISNULL(intseq) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","history.back();")
Else
	intseq = Replace(intseq, " ", ",")
End If

'// 삭제 시
If mode = "del" Then

	sql = "SELECT strId FROM mTb_Member2 WHERE intseq IN (" & intseq & ")"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
			strId = rs("strId")
			dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&Request.Cookies("SRM_ID")&"','"&strId&"','"&message&"','하위회원삭제','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

			rs.movenext
		Loop
	End If

	rs.close
	Set rs = Nothing

	dbconn.execute("DELETE FROM mTb_Member2 WHERE intseq IN (" & intseq & ")")

'// 하위회원 상태변경 시
ElseIf mode = "etc" Then
	If message = "사용" Then
		mem_status = 0
	ElseIf message = "미사용" Then
		mem_status = 1
	End If

	dbconn.execute("UPDATE mTb_Member2 SET intAction = '" & mem_status & "' WHERE intseq IN (" & intseq & ")")

	sql = "SELECT strId FROM mTb_Member2 WHERE intseq IN (" & intseq & ")"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
			strId = rs("strId")
			dbconn.execute("INSERT INTO master_log (m_id,target_id,act,act_detail,ip) VALUES('"&Request.Cookies("SRM_ID")&"','"&strId&"','"&message&"','하위회원상태변경','"&Request.ServerVariables("REMOTE_ADDR")&"') ")

			rs.movenext
		Loop
	End If

	rs.close
	Set rs = Nothing

Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("" & message & "되었습니다.", "./user_4.asp")
%>