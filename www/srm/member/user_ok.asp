<!-- #include virtual="/srm/usercheck.asp" -->
<%
'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/member/")
UploadForm.MaxFileLen = int(1024 * 1024 * 20)

UF_strId		= SQL_Injection(Trim(UploadForm("strId")))
UF_strPwd		= SQL_Injection(Trim(UploadForm("strPwd")))
UF_newPwd		= SQL_Injection(Trim(UploadForm("newPwd")))
UF_strPhone1	= SQL_Injection(Trim(UploadForm("strPhone1")))
UF_strPhone2	= SQL_Injection(Trim(UploadForm("strPhone2")))
UF_strPhone3	= SQL_Injection(Trim(UploadForm("strPhone3")))
UF_strMobile1	= SQL_Injection(Trim(UploadForm("strMobile1")))
UF_strMobile2	= SQL_Injection(Trim(UploadForm("strMobile2")))
UF_strMobile3	= SQL_Injection(Trim(UploadForm("strMobile3")))
UF_smsYN		= SQL_Injection(Trim(UploadForm("smsYN")))
UF_strEmail1	= SQL_Injection(Trim(UploadForm("strEmail1")))
UF_strEmail2	= SQL_Injection(Trim(UploadForm("strEmail2")))
UF_mailYN		= SQL_Injection(Trim(UploadForm("mailYN")))
UF_cZip			= SQL_Injection(Trim(UploadForm("cZip")))
UF_cAddr1		= SQL_Injection(Trim(UploadForm("cAddr1")))
UF_cAddr2		= SQL_Injection(Trim(UploadForm("cAddr2")))

sql = "SELECT strId FROM mTb_Member2 WHERE strId = '" & UF_strId & "' AND strPwd = '" & Encrypt_Sha(UF_strPwd) & "' AND intGubun <> 1 AND intaction = 0 "
Set rs = dbconn.execute(sql)

If Not(rs.eof) Then

	'// 변수 가공
	UF_strPhone = UF_strPhone1 & "-" & UF_strPhone2 & "-" & UF_strPhone3
	UF_strMobile = UF_strMobile1 & "-" & UF_strMobile2 & "-" & UF_strMobile3
	UF_strEmail = UF_strEmail1 & "@" & UF_strEmail2

	If UF_smsYN <> "Y" Then
		UF_smsYN = "N"
	End If

	If UF_mailYN <> "Y" Then
		UF_mailYN = "N"
	End If

	If UploadForm("strFile1").FileName <> "" Then
		'// 업로드 가능 여부 체크
		If UploadFileChk(UploadForm("strFile1").FileName)=False Then
			Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 용량 체크
		If UploadForm("strFile1").FileLen > UploadForm.MaxFileLen Then
			Response.Write "<script language='javascript'>alert('20MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'기존파일 삭제
		If rsFile <> "" Then
			UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile
		End If

		'// 파일 저장
		mFile = UploadForm("strFile1").Save(, False)
		mFile = UploadForm("strFile1").LastSavedFileName

		Sql = "UPDATE mTb_Member2 SET strFile1 = N'" & SQL_Injection(mFile) & "' WHERE intseq='" & intseq & "'"
		dbconn.execute(Sql)

	'// 새로운 파일 업로드 없이 기존 파일 삭제만 선택한 경우
	ElseIf UploadForm("strFile1").FileName = "" And UploadForm("ori_file1_del") = "Y" Then
		'기존파일 삭제
		If rsFile <> "" Then
			UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile
		End If

		Sql = "UPDATE mTb_Member2 SET strFile1 = '' WHERE intseq='" & intseq & "'"
		dbconn.execute(Sql)
	End If

	Set UploadForm = Nothing

	Sql = "UPDATE mTb_Member2 SET "
	If UF_newPwd <> "" Then
		Sql = Sql & "strPwd='" & Encrypt_Sha(UF_newPwd) & "', "
	End If
	Sql = Sql & "strPhone = '" & UF_strPhone & "', "
	Sql = Sql & "strMobile = '" & UF_strMobile & "', "
	Sql = Sql & "smsYN = '" & UF_smsYN & "', "
	Sql = Sql & "strEmail = N'" & UF_strEmail & "', "
	Sql = Sql & "mailYN = '" & UF_mailYN & "', "
	Sql = Sql & "cZip = '" & UF_cZip & "', "
	Sql = Sql & "cAddr1 = N'" & UF_cAddr1 & "', "
	Sql = Sql & "cAddr2 = N'" & UF_cAddr2 & "', "
	Sql = Sql & "update_date = '" & Date() & "' "
	Sql = Sql & " WHERE intseq = '" & intseq & "'"

	dbconn.execute(Sql)

	Call DbClose()

	Call jsAlertMsgUrl("수정되었습니다.", "user.asp")
Else

	Set UploadForm = Nothing
	Call DbClose()

	Call jsAlertMsgBack("비밀번호를 확인해 주세요.")
End If
%>