<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script src="/srm/js/member.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child()").addClass("active");
			$(".tab_contents").hide();
			$(".sub_con_tab li:first").addClass("active").show();
			$(".tab_contents:first").show();
		});

		function openDaumPostcode() {new daum.Postcode({
				oncomplete: function(data) {
					// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

					// 각 주소의 노출 규칙에 따라 주소를 조합한다.
					// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
					var fullAddr = ''; // 최종 주소 변수
					var extraAddr = ''; // 조합형 주소 변수

					// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
					if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
						fullAddr = data.roadAddress;

					} else { // 사용자가 지번 주소를 선택했을 경우(J)
						fullAddr = data.jibunAddress;
					}

					// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
					if(data.userSelectedType === 'R'){
						//법정동명이 있을 경우 추가한다.
						if(data.bname !== ''){
							extraAddr += data.bname;
						}
						// 건물명이 있을 경우 추가한다.
						if(data.buildingName !== ''){
							extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
						}
						// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
						fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
					}

					// 우편번호와 주소 정보를 해당 필드에 넣는다.
					document.getElementById('cZip').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('cAddr1').value = fullAddr;

					// 커서를 상세주소 필드로 이동한다.
					document.getElementById('cAddr2').focus();
				}
			}).open();
		}
	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
idx = Request.QueryString("idx")

If idx <> "" Then
	sql = "SELECT * FROM member_taxbill WHERE bill_idx = " & idx & " AND mem_idx = " & intSeq
	Set rs = dbconn.execute(sql)

	If Not(rs.eof) Then
		mainYN	= rs("mainYN")
		cNum	= rs("cNum")
		cName	= rs("cName")
		ceo		= rs("ceo")
		cZip	= rs("cZip")
		cAddr1	= rs("cAddr1")
		cAddr2	= rs("cAddr2")
		cTypes	= rs("cType")
		cCate	= rs("cCate")
		cPhone	= rs("cPhone")
		cMobile = rs("cMobile")
		cEmail	= rs("cEmail")

		If cNum <> "" Then
			ArrNum = Split(cNum, "-")
			If Ubound(ArrNum) = 2 Then
				cNum1 = ArrNum(0)
				cNum2 = ArrNum(1)
				cNum3 = ArrNum(2)
			End If
		End If

		If cPhone <> "" Then
			ArrPhone = Split(cPhone, "-")
			If Ubound(ArrPhone) = 2 Then
				cPhone1 = ArrPhone(0)
				cPhone2 = ArrPhone(1)
				cPhone3 = ArrPhone(2)
			End If
		End If

		If cMobile <> "" Then
			ArrMobile = Split(cMobile, "-")
			If Ubound(ArrMobile) = 2 Then
				cMobile1 = ArrMobile(0)
				cMobile2 = ArrMobile(1)
				cMobile3 = ArrMobile(2)
			End If
		End If

		If cEmail <> "" Then
			ArrstrEmail = Split(cEmail, "@")
			If Ubound(ArrstrEmail) = 1 Then
				cEmail1 = ArrstrEmail(0)
				cEmail2 = ArrstrEmail(1)
			End If
		End If
	End If

	rs.close
	Set rs = Nothing
End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="user">
					<div class="inner">
						<div class="sub_tit">
							<h2>회원정보관리</h2>
						</div>
						<div class="section_tit">
							<h4>세금계산서발행정보</h4>
						</div>
						<form name="billform" method="post" action="./user_tax_write_ok.asp">
						<input type="hidden" name="intSeq" value="<%=intSeq%>">
						<input type="hidden" name="idx" value="<%=idx%>">
						<table class="table_type02 table">

							<tbody>
								<tr>
									<th>대표 세금계산서발행정보</th>
									<td>
										<div class="chk_box radio_box">
											<input id="mainYN" type="checkbox" name="mainYN" value="Y" <% If mainYN = "Y" Then %> checked<% End If %>>
											<label for="mainYN"><span></span>
												<p>대표 세금계산서발행정보로 설정
												</p>
											</label>
										</div>
									</td>
								</tr>
								<tr class="phone">
									<th>사업자등록번호 <span>*</span></th>
									<td>
										<input type="text" name="cNum1" maxlength="3" value="<%=cNum1%>"> &nbsp;-&nbsp;
										<input type="text" name="cNum2" maxlength="2" value="<%=cNum2%>"> &nbsp;-&nbsp;
										<input type="text" name="cNum3" maxlength="5" value="<%=cNum3%>">
									</td>
								</tr>
								<tr>
									<th>상호 (법인명) <span>*</span></th>
									<td><input type="text" name="cName" value="<%=cName%>"></td>
								</tr>
								<tr>
									<th>대표자 <span>*</span></th>
									<td><input type="text" name="ceo" value="<%=ceo%>"></td>
								</tr>
								<tr class="add">
									<th>사업장 주소 (도로명 주소) <span>*</span> </th>
									<td><input type="text" class="wd360" name="cZip" id="cZip" readonly value="<%=cZip%>"> <button class="zip_code btn_bk" onclick="openDaumPostcode();return false;">우편번호 검색</button> <input type="text" name="cAddr1" id="cAddr1" value="<%=cAddr1%>" readonly><input type="text" name="cAddr2" id="cAddr2" value="<%=cAddr2%>"></td>
								</tr>
								<tr>
									<th>업태 <span>*</span></th>
									<td><input type="text" name="cType" value="<%=cTypes%>"></td>
								</tr>
								<tr>
									<th>종목 <span>*</span></th>
									<td><input type="text" name="cCate" value="<%=cCate%>"></td>
								</tr>


								<tr class="phone">
									<th>전화번호 <span>*</span></th>
									<td>
										<input type="text" name="cPhone1" style="ime-mode:disabled" maxlength="4" value="<%=cPhone1%>"> &nbsp;-&nbsp;
										<input type="text" name="cPhone2" style="ime-mode:disabled" maxlength="4" value="<%=cPhone2%>">&nbsp; -&nbsp;
										<input type="text" name="cPhone3" style="ime-mode:disabled" maxlength="4" value="<%=cPhone3%>">

									</td>
								</tr>
								<tr class="phone">
									<th>휴대폰 번호 <span>*</span></th>
									<td>
										<input type="text" name="cMobile1" style="ime-mode:disabled" maxlength="4" value="<%=cMobile1%>"> &nbsp;-&nbsp;
										<input type="text" name="cMobile2" style="ime-mode:disabled" maxlength="4" value="<%=cMobile2%>"> &nbsp;-&nbsp;
										<input type="text" name="cMobile3" style="ime-mode:disabled" maxlength="4" value="<%=cMobile3%>">
									</td>
								</tr>
								<tr class="mail">
									<th>이메일 주소<span>*</span></th>
									<td> <input type="text" name="cEmail1" value="<%=cEmail1%>">&nbsp; @&nbsp;
										<input type="text" name="cEmail2" value="<%=cEmail2%>" readonly>
										<label>
											<select name="cEmail3" id="cEmail3" onchange="ChangeEmail1();">
												<option value="0">직접입력</option>
												<option value="dreamwiz.co.kr" <% If cEmail2 = "dreamwiz.co.kr" Then %> selected<% End If %>>dreamwiz.co.kr</option>
												<option value="empal.com" <% If cEmail2 = "empal.com" Then %> selected<% End If %>>empal.com</option>
												<option value="gmail.com" <% If cEmail2 = "gmail.com" Then %> selected<% End If %>>gmail.com</option>
												<option value="lycos.co.kr" <% If cEmail2 = "lycos.co.kr" Then %> selected<% End If %>>lycos.co.kr</option>
												<option value="naver.com" <% If cEmail2 = "naver.com" Then %> selected<% End If %>>naver.com</option>
												<option value="nate.com" <% If cEmail2 = "nate.com" Then %> selected<% End If %>>nate.com</option>
												<option value="netsgo.com" <% If cEmail2 = "netsgo.com" Then %> selected<% End If %>>netsgo.com</option>
												<option value="hanmail.net" <% If cEmail2 = "hanmail.net" Then %> selected<% End If %>>hanmail.net</option>
												<option value="hotmail.com" <% If cEmail2 = "hotmail.com" Then %> selected<% End If %>>hotmail.com</option>
												<option value="paran.com" <% If cEmail2 = "paran.com" Then %> selected<% End If %>>paran.com</option>
											</select>
										</label></td>
								</tr>
							</tbody>
						</table>

						<div class="btn_box ask_btn_box">
						<button class="btn_wh" type="button" onclick="location.href='user_2.asp'">취소</button>
							<button class="btn_blue" onclick="tax_chk();return false;"><% If idx <> "" Then %>수정<% Else %>확인<% End If %></button>
						</form>
							<!--button class="btn_wh" type="button" onclick="location.href='user_2.asp'">취소</button-->
						</div>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>
