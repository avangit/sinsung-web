<!-- #include virtual="/srm/usercheck.asp" -->
<%
arr_o_idx	= SQL_Injection(Trim(Request.Form("n")))
mode		= SQL_Injection(Trim(Request.Form("m")))
message		= SQL_Injection(Trim(Request.Form("message")))

If Request.Cookies("SRM_LEVEL") < 9 Then
'	Call jsAlertMsgBack("접근 권한이 없습니다.")
End If

If arr_o_idx = "" Or ISNULL(arr_o_idx) Then
	Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","history.back();")
End If

'// 전자결재 상태변경
If Left(mode, 3) = "chg" Then
	If Right(mode, 2) = 15 Then
		message = "반려"
	ElseIf Right(mode, 2) = 20 Then
		message = "승인"
	End If

	dbconn.execute("UPDATE orders SET o_status = '" & Right(mode, 2) & "'  WHERE o_idx IN (" & arr_o_idx & ")")
Else
	jsAlertMsg("유효하지 않은 실행종류입니다.")
End If

Call dbClose()

Call jsAlertMsgUrl("전자결재가 " & message & "되었습니다.", "./order_list.asp")
%>