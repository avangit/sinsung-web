<!-- #include virtual="/srm/usercheck.asp" -->
<%
	If Request.Cookies("SRM_LEVEL") < 9 Then
	'	Call jsAlertMsgBack("접근 권한이 없습니다.")
	End If

	sql = "SELECT * FROM Record_views3"
	Set rs = dbconn.execute(sql)

	If Not rs.eof Then
		cost = rs("cost")
	End If

	rs.close
%>
<!doctype html>
<html>

<head>
    <!-- #include virtual="/srm/_inc/head.asp" -->
    <!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
    <link rel="stylesheet" href="/srm/_css/sub.css" />

    <script type="text/javascript">
        $(document).on('ready', function() {
            $("#A_Header .gnb ul.nav li:nth-child(2)").addClass("active");
        });

    </script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="order_view">
					<div class="inner">
						<div class="sub_tit">
							<h2>주문내역</h2>
						</div>
<%
	o_idx = SQL_Injection(Trim(Request.QueryString("idx")))

	If Request.Cookies("SRM_LEVEL") <> 9 Then
'		query_where = " AND strId = '" & Request.Cookies("SRM_ID") & "'"
	End If

	sql = "SELECT * FROM orders WHERE o_payment = 'yeosin' AND o_idx = " & o_idx & query_where
	Set rs = dbconn.execute(sql)

	If rs.eof Or rs.bof Then
		Call jsAlertMsgUrl("접근경로가 올바르지 않습니다.","/srm/")
	Else

		If InStr(rs("g_idx"), ",") > 0 Then
			arr_g_cnt = Split(rs("g_cnt"), ",")
			arr_g_price = Split(rs("g_price"), ",")
		Else
			arr_g_cnt = rs("g_cnt")
			arr_g_price = rs("g_price")
		End If
%>
						<table class="table table_type03">
							<colgroup>
								<col style="width:600px">
								<col style="width:200px">
								<col style="width:200px">
							</colgroup>
							<thead>
								<tr>
									<th>상품정보</th>
									<th>상품금액</th>
									<th>수량</th>
								</tr>
							</thead>
							<tbody>
<%
		strSQL = "SELECT g_idx,g_name,g_money,g_simg FROM GOODS WHERE g_idx IN (" & rs("g_idx") & ") ORDER BY g_idx DESC"
		Set rs2 = dbconn.execute(strSQL)

			i = 0
			Do While Not rs2.eof

				If rs("o_status") = "0" Then
					status_txt = "구매요청"
				ElseIf rs("o_status") = "15" Then
					status_txt = "구매반려"
				ElseIf rs("o_status") = "20" Then
					status_txt = "구매승인"
				End If
%>
								<tr>
									<td>
										<div class="cart_div">
											<div class="img_box">
												<a href="#"><img src="/upload/goods/<%=rs2("g_simg")%>"></a>
											</div>
											<div class="txt_box">
												<a href="#" class="pro_code">제품코드 : #<%=rs2("g_idx")%></a>
												<p class="pro_name"><%=rs2("g_name")%></p><br>
											</div>
										</div>
									</td>
								<% If InStr(rs("g_idx"), ",") > 0 Then %>
									<td class="price"><%=FormatNumber(rs("o_total_price")-cost, 0)%>원</td>
									<td><%=arr_g_cnt(i)%>개</td>
								<% Else %>
									<td class="price"><%=FormatNumber(rs("o_total_price")-cost, 0)%>원</td>
									<td><%=arr_g_cnt%>개</td>
								<% End If %>
								</tr>
<%
				i = i + 1
				rs2.MoveNext
			Loop

		rs2.close
		Set rs2 = Nothing
%>
							</tbody>
						</table>

						<div class="cart_price_box2">
							<div class="price">
								<div>
									<p>
										<span>구매금액</span>
										<%=FormatNumber(rs("o_total_price")-cost, 0)%>원
									</p>
									<img src="/srm/images/sub/icon_plus.png" alt="">
									<p>
										<span>배송비</span>
										<%=FormatNumber(cost, 0)%>원
									</p>
									<img src="/srm/images/sub/icon_total.png" alt="">
									<p>
										<span>총 결제금액</span>
										<%=FormatNumber(rs("o_total_price"), 0)%>원
									</p>
								</div>
							</div>
						</div>

						<div class="order_middle">
							<div class="section_tit">
								<h4>주문/결제정보</h4>
							</div>
							<table class="table table_type02">
								<colgroup>
									<col width="200px">
									<col width="500px">
									<col width="200px">
									<col width="500px">
								</colgroup>
								<tbody>
									<tr>
										<th>주문상태</th>
										<td><%=status_txt%></td>
										<th>주문번호</th>
										<td><%=rs("o_num")%></td>
									</tr>
									<tr>
										<th>결제금액</th>
										<td><%=FormatNumber(rs("o_total_price"), 0)%>원</td>
										<th>주문일시</th>
										<td><%=rs("regdate")%></td>
									</tr>
								</tbody>
							</table>

						</div>

						<div class="order_middle">
							<div class="section_tit">
								<h4>주문하시는 분</h4>
							</div>
							<table class="table table_type02">
								<colgroup>
									<col width="200px">
									<col width="500px">
									<col width="200px">
									<col width="500px">
								</colgroup>
								<tbody>
									<tr>
										<th>회사명</th>
										<td colspan="3"><%=rs("o_company")%></td>
									</tr>
									<tr>
										<th>주문자</th>
										<td><%=rs("o_name")%></td>
										<th>주문부서</th>
										<td><%=rs("o_department")%></td>
									</tr>
									<tr>
										<th>휴대폰 번호</th>
										<td><%=rs("o_mobile")%></td>
										<th>이메일주소</th>
										<td><%=rs("o_email")%></td>
									</tr>
									<tr>
										<th>주소</th>
										<td colspan="3">[<%=rs("s_zip")%>] <%=rs("s_addr1")%> <br><%=rs("s_addr2")%></td>
									</tr>
								</tbody>
							</table>

						</div>
						<div class="order_middle">
							<div class="section_tit">
								<h4>받는 분</h4>
							</div>
							<table class="table table_type02">
								<colgroup>
									<col width="200px">
									<col width="500px">
									<col width="200px">
									<col width="500px">
								</colgroup>
								<tbody>
									<tr>
										<th>수령인</th>
										<td><%=rs("s_name")%></td>
										<th>연락처</th>
										<td><%=rs("s_phone")%></td>
									</tr>
									<tr>
										<th>이메일주소</th>
										<td><%=rs("s_email")%></td>
										<th>휴대폰 번호</th>
										<td><%=rs("s_mobile")%></td>
									</tr>
									<tr>
										<th>주소</th>
										<td colspan="3">[<%=rs("s_zip")%>] <%=rs("s_addr1")%> <br>
											<%=rs("s_addr2")%></td>
									</tr>
									<tr>
										<th>배송 시 요청사항</th>
										<td colspan="3"><%=rs("s_request")%></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="btn_sbox">
							<button class="btn_bk">명세표 출력</button>
							<button class="btn_bk">주문서 출력</button>
						</div>

						<div class="btn_box btn_box_p">
						<% If rs("o_status") < 3 Then %>
							<button class="btn_wh" onclick="location.href='order_list.asp'">주문취소</button>
						<% End If %>
							<button class="btn_blue" onclick="location.href='order_list.asp'">목록</button>
						</div>

					</div>
				</div>

			</div>
<%
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->