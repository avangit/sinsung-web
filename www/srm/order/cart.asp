<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script src="/srm/js/order.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
//			$("#A_Header .gnb ul.nav li:nth-child(2)").addClass("active");
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="cart">
					<div class="inner">
						<div class="sub_tit">
							<h2>장바구니</h2>
						</div>
						<div class="progress_box">
							<ul>
								<li class="on">1</li>
								<li>2</li>
								<li>3</li>
							</ul>
						</div>
<%
sql = "SELECT a.c_idx, a.g_idx, a.g_cnt, a.g_price, b.g_act, b.g_name, b.g_simg FROM cart a LEFT JOIN GOODS b ON a.g_idx = b.g_idx WHERE a.mem_id = '" & Request.Cookies("SRM_ID") & "' AND b.g_display <> 'home' AND b.g_type = 1 ORDER BY c_idx DESC"
Set rs = dbconn.execute(sql)
%>
						<form name="goods_Form" method="post" style="margin:0;">
						<input type="hidden" name="goods_list">
						<input type="hidden" name="goods_price">
						<input type="hidden" name="goods_cnt">
						<table class="table table_type03">
							<colgroup>
								<col style="width:80px">
								<col style="width:500px">
								<col style="width:150px">
								<col style="width:300px">
								<col style="width:150px">
							</colgroup>
							<thead>
								<tr>
									<th>
										<div class="chk_box">
											<input id="cart_chk" type="checkbox" value="" onclick="checkAll(document.goods_Form);">
											<label for="cart_chk"><span></span></label>
										</div>
									</th>
									<th>상품정보</th>
									<th>상품금액</th>
									<th>수량</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
<%
If rs.bof Or rs.eof Then
%>
								<tr><td colspan="5" align="center">등록된 제품이 없습니다.</td></tr>
<%
Else
	i = 0
	Do While Not rs.eof
%>
								<tr>
									<td>
										<div class="chk_box">
											<input id="cart_chk<%=i%>" type="checkbox" name="gList" value="<%=rs("g_idx")%>">
											<label for="cart_chk<%=i%>"><span></span></label>
										</div>
									</td>
									<td>
										<div class="cart_div">
											<div class="img_box">
												<a href="#"><img src="/upload/goods/<%=rs("g_simg")%>"></a>
											</div>
											<div class="txt_box">
												<a href="#" class="pro_code">제품코드 : #<%=rs("g_idx")%></a>
												<p class="pro_name"><%=rs("g_name")%></p><br>
											</div>
										</div>
									</td>
									<td class="price" id="price<%=rs("g_idx")%>"><%=FormatNumber(rs("g_price")*rs("g_cnt"), 0)%>원</td>
									<td>

										<div class="vol_btns">
											<a href="javascript:;"><img src="/srm/images/sub/but_vol_up.png" class="orderMinus"></a>
											<input type="text" id="g_cnt<%=rs("g_idx")%>" name="g_cnt<%=i%>" value="<%=rs("g_cnt")%>" class="num">
											<a href="javascript:;"><img src="/srm/images/sub/but_vol_down.png" class="orderPlus"></a>
										</div>
									</td>
									<td>
										<div class="e_btns">
											<button class="modify_btn" data-id="<%=rs("c_idx")%>">수정</button>
											<button class="del_btn" data-id="<%=rs("c_idx")%>">삭제</button>
										</div>
									</td>
								</tr>
<%
		TotalPrice = TotalPrice + (rs("g_price") * rs("g_cnt"))
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close

sql = "SELECT * FROM Record_views3"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	cost = rs("cost")
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
							</tbody>
						</table>

						<div class="btn_box estimate_btn">
							<button class="btn_blue" onclick="javascript:cart_Chk('print', '견적서출력');return false;">견적서 출력</button>
							<button class="btn_bk" onclick="javascript:cart_Chk('req', '견적요청');return false;">견적 요청</button>
						</div>

						<div class="cart_price_box2">
							<div class="price">
								<div>
									<p>
										<span>구매금액</span>
										<label class="buyPrice" id="buyPrice"><%=FormatNumber(TotalPrice, 0)%>원</label>
									</p>
									<img src="/srm/images/sub/icon_plus.png" alt="">
									<p>
										<span>배송비</span>
										<%=FormatNumber(cost, 0)%>원
									</p>
									<img src="/srm/images/sub/icon_total.png" alt="">
									<p>
										<span>총 결제금액</span>
										<label id="totalPrice"><%=FormatNumber(TotalPrice+cost, 0)%>원</label>
									</p>
								</div>
							</div>
						</div>
						<div class="btn_box">
							<button class="btn_wh" onclick="javascript:cart_Chk('cgood', '선택주문');return false;">선택상품 주문</button>
							<button class="btn_blue" onclick="javascript:checkAll2(document.goods_Form);cart_Chk('agood', '전체주문');">전체상품 주문</button>
						</div>
						</form>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->