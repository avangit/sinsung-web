<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(3)").addClass("active");

			$("#sdate").datepicker({
				showOn: "both",
				buttonImageOnly: false,
				buttonText: "날짜 선택",
				dateFormat: "yy-mm-dd"
			});
			$("#edate").datepicker({
				showOn: "both",
				buttonImageOnly: false,
				buttonText: "날짜 선택",
				dateFormat: "yy-mm-dd"
			});
		});

		$(document).ready(function() {
			var posY;

			function bodyFreezeScroll() {
				posY = $(window).scrollTop();
				$("html").addClass('fix');
				$("html").css("top", -posY);
			}

			function bodyUnfreezeScroll() {
				$("html").removeAttr('class');
				$("html").removeAttr('style');
				posY = $(window).scrollTop(posY);
			}
			$('.thumb_img a, .video .video_area ul li a').click(function() {
				$('.popup').fadeIn();
				bodyFreezeScroll();

			});
			$('.popup .btn_close').click(function() {
				$('.popup').fadeOut();
				bodyUnfreezeScroll()

			});
		});

		function frmReset() {
				history.replaceState({}, null, location.pathname);
				$("#fieldname").val('');
				$("#fieldvalue").val('');
				$("#sdate").val('');
				$("#edate").val('');

		}

		function frmSubmit() {
			if($("#fieldvalue").val() == "" && $("#sdate").val() == "" && $("#edate").val() == "") {
				alert("검색어를 입력하세요");
				return false;
			} else {
				document.sch_Form.submit();
			}
		}

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="order_list table_type01">
					<div class="inner">
						<div class="sub_tit">
							<h2>주문배송조회</h2>
						</div>
						<!--
						<div class="order_tab_area">
							<ul class="sub_con_tab">
								<li><a href="order_list.html">구매 전자결재</a></li>
								<li  class="active"><a href="order_list2.html">주문/배송조회</a></li>
							</ul>
						</div>
-->
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim dayday 		: dayday 		=  SQL_Injection(Trim(Request("d")))
Dim sdate 		: sdate 		=  SQL_Injection(Trim(Request("sdate")))
Dim edate 		: edate 		=  SQL_Injection(Trim(Request("edate")))
Dim orderstep	: orderstep		=  SQL_Injection(Trim(Request("orderstep")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "orders"
Dim query_where			: query_where		= " o_payment <> 'yeosin'"
Dim query_orderby		: query_orderby		= " ORDER BY o_idx DESC"

If Request.Cookies("SRM_LEVEL") <> 9 Then
	query_where = query_where & " AND intSeq = " & intSeq
End If

If dayday = "" Then
'	sdate = ""
'	edate = ""
ElseIf dayday = "7" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -7, Date)
	edate = Date
ElseIf dayday = "30" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -30, Date)
	edate = Date
ElseIf dayday = "365" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -365, Date)
	edate = Date
End If

If sdate <> "" And edate <> "" Then
	query_where = query_where & " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day, 1, '" & edate & "')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where & " AND regdate >= '" & sdate & "'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where & " AND regdate <= DATEADD(day, 1, '" & edate & "')"
End If

If orderstep <> "" Then
	query_where = query_where &" AND o_status = '" & orderstep & "' "
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
						<div class="tab_contents" id="order_02">
							<table class="search_table">
								<colgroup>
									<col style="width:120px">
									<col style="width:540px;">
									<col style="width:120px">
									<col style="width:400px">
								</colgroup>
								<tbody>
									<tr>
										<th>기간검색</th>
										<td>
											<div class="tab_btn">
												<button onclick="location.href='./order_list2.asp'" <% If dayday = "" Then %>class="on"<% End If %>>전체</button>
												<button onclick="location.href='./order_list2.asp?d=7'" <% If dayday = "7" Then %>class="on"<% End If %>>1주일</button>
												<button onclick="location.href='./order_list2.asp?d=30'" <% If dayday = "30" Then %>class="on"<% End If %>>1개월</button>
												<button onclick="location.href='./order_list2.asp?d=365'" <% If dayday = "365" Then %>class="on"<% End If %>>1년</button>
											</div>
							<form name="sch_Form" id="sch_Form" method="post">
											<div>
												<input type="text" id="sdate" name="sdate" class="date" value="<%=sdate%>" autocomplete="off"> -
												<input type="text" id="edate" name="edate" class="date" value="<%=edate%>" autocomplete="off">
											</div>
										</td>
										<th>조건검색</th>
										<td>
											<select name="fieldname" id="fieldname">
												<option value="o_num" <% If fieldname = "o_num" Then %> selected<% End If %>>주문번호</option>
												<!--option>제조사</option>
												<option>카테고리</option>
												<option>제품명</option-->
											<% If Request.Cookies("SRM_LEVEL") = 9 Then %>
												<option value="o_serial" <% If fieldname = "o_serial" Then %> selected<% End If %>>시리얼번호</option>
												<option value="o_department" <% If fieldname = "o_department" Then %> selected<% End If %>>부서</option>
												<!--option value="on_email" <% If fieldname = "on_email" Then %> selected<% End If %>>사용자</option-->
											<% End If %>
												<input type="text" name="fieldvalue" id="fieldvalue" value="<%=fieldvalue%>" onkeypress="if( event.keyCode == 13 ){frmSubmit();return false;}">
											</select>
										</td>
									</tr>
								</tbody>
							</table>

							<div class="btn_box">
								<button class="btn_wh" onclick="frmReset();">검색 초기화</button>
								<button class="btn_blue" onclick="frmSubmit();return false;">검색</button>
							</div>
							</form>

							<form name="s_Form" method="post">
							<p class="cont"><span><%=intTotalCount%></span>건</p>
							<div class="a_option">
								<select name="orderstep" onchange="document.s_Form.submit();">
									<option value="">주문상태</option>
									<option value="1" <% If orderstep = "1" Then %> selected<% End If %>>결제대기</option>
									<option value="2" <% If orderstep = "2" Then %> selected<% End If %>>결제확인</option>
									<option value="3" <% If orderstep = "3" Then %> selected<% End If %>>상품배송</option>
									<option value="4" <% If orderstep = "4" Then %> selected<% End If %>>판매완료</option>
									<option value="91" <% If orderstep = "91" Then %> selected<% End If %>>취소</option>
									<option value="92" <% If orderstep = "92" Then %> selected<% End If %>>교환</option>
									<option value="93" <% If orderstep = "93" Then %> selected<% End If %>>반품</option>
								</select>
							</div>
							</form>
							<table class="table type01">

								<colgroup>
									<col style="width:200px">
									<col style="width:200px">
									<col style="width:200px">
									<col style="width:200px">
									<col style="width:200px">
									<col style="width:120px">
								</colgroup>
								<thead>
									<tr>
										<th>주문일자</th>
										<th>주문번호</th>
										<th>주문정보</th>
										<th>주문금액</th>
										<th>주문상태</th>
										<th>명세표 출력</th>
									</tr>
								</thead>
								<tbody>
<%
If rs.bof Or rs.eof Then
%>
									<tr><td colspan="8" align="center">등록된 주문 내역이 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
		status = rs("o_status")

		If rs("o_status") = "1" Then
			status_txt = "결제대기"
		ElseIf rs("o_status") = "2" Then
			status_txt = "결제확인"
		ElseIf rs("o_status") = "3" Then
			status_txt = "상품배송"
		ElseIf rs("o_status") = "4" Then
			status_txt = "판매완료"
		ElseIf rs("o_status") = "91" Then
			status_txt = "취소"
		ElseIf rs("o_status") = "92" Then
			status_txt = "교환"
		ElseIf rs("o_status") = "93" Then
			status_txt = "반품"
		End If

		If InStr(rs("g_idx"), ",") > 0 Then
			totalCnt = Ubound(Split(rs("g_idx"), ",")) + 1
		Else
			totalCnt = 1
		End If
%>
										<tr>
											<td><%=Left(rs("regdate"), 10)%></td>
											<td><a href="./order_view2.asp?idx=<%=rs("o_idx")%>"><%=rs("o_num")%></a></td>
											<td><%=totalCnt%>건</td>
											<td><%=FormatNumber(rs("o_total_price"), 0)%>원</td>
											<td class="case<%=status%>"><%=status_txt%></td>
											<td><button class="btn_print" onclick="location.href='javascript:;'">출력</button></td>
										</tr>
<%
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
									</tbody>
								</table>
							</form>
							<% If Request.Cookies("SRM_LEVEL") = 9 Then %>
							<div class="btn_box save_btn">
								<button class="btn_bk" onclick="location.href='./exceldownload2.asp?sdate=<%=sdate%>&edate=<%=edate%>&fieldname=<%=fieldname%>&fieldvalue=<%=fieldvalue%>&page=<%=intNowPage%>&orderstep=<%=orderstep%>'">엑셀파일 저장</button>
							</div>
							<% End If %>
						</div>

						<% Call Paging_user_srm("") %>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->