<!-- #include virtual="/srm/usercheck.asp" -->
<!DOCTYPE html>
<html lang="ko">

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<link rel="stylesheet" href="/srm/_css/sub_print.css" />
	<script>
		window.onload = function() {
//			window.print();
		};

	</script>

	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

</head>
<%
Call DbOpen()

sql = "SELECT manager1, m1_Email, m1_Mobile FROM mtb_member2 WHERE strId='" & Request.Cookies("SRM_ID") & "' AND strName = '" & Request.Cookies("SRM_NAME") & "' AND intGubun <> 1 AND intaction = 0 "
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	manager1 = rs("manager1")
	m1_Email = rs("m1_Email")
	m1_Mobile = rs("m1_Mobile")
End If

rs.close
%>
<body>
	<div id="popupE">
		<img src="/srm/images/common/logo_print.gif" alt="로고" class="logo">
		<img src="/srm/images/common/stamp_print.png" alt="직인" class="stamp">
		<h1>견적서</h1>
<%
goods_list = Request("goods_list")
'goods_price = Request.Form("goods_price")
goods_cnt = Request("goods_cnt")

If InStr(goods_cnt, ",") > 0 Then
	arr_goods_cnt = Split(goods_cnt, ",")
Else
	arr_goods_cnt = Split(goods_cnt, "")
End If

If Request.Cookies("SRM_LEVEL") = 2 Then
	query_where = " AND c.mem_idx = " & intSeq
ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
	query_where = " AND c.mem_idx = " & masterIDX
End If

sql = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_optionList, ISNULL(C.mem_price, 0) AS mem_price FROM GOODS A LEFT JOIN goods_srm_join C ON a.g_idx = C.g_idx WHERE a.g_idx IN (" & goods_list & ") " & query_where & " ORDER BY a.g_idx DESC"
Set rs = dbconn.execute(sql)
%>
		<div class="stampCon">
			<div class="box sectE1">
				<div class="left">
					<h2><%=cName%> <!--span class="han">貴中</span--></h2>
					<p><strong class="tit">견적일자 :</strong> <%=year(Date)%>년 <%=month(Date)%>월 <%=day(Date)%>일</p>
					<p><strong class="tit">견적담당 :</strong> <%=manager1%><span class="mar"><%=m1_Mobile%></span> <span class="mar"><%=m1_Email%></span></p>
					<!--p><strong class="tit">참&nbsp;&nbsp;조 :</strong> 김 영구 님</p-->
					<p class="bold">견적요청에 감사드리며 아래와 같이 견적합니다.</p>
				</div>
				<div class="right">
					<h2>공<br>급<br>자</h2>
					<ul>
						<li>
							<p>등록번호</p>
							<span class="fosi14 wd_cal">119-86-18434</span>
						</li>
						<li>
							<p>상&nbsp;&nbsp;&nbsp;&nbsp;호</p>
							<span class="wid2 fosi11">주식회사 신성씨앤에스</span>
							<p class="tit2">대표자: 전 성 우</p>
							<!--						<span> 전성우</span>-->
						</li>
						<li>
							<p>주&nbsp;&nbsp;&nbsp;&nbsp;소</p>
							<span class="wd_cal">서울특별시 구로구 디지털로 272 한신IT타워 5층</span>
						</li>
						<li>
							<p>업&nbsp;&nbsp;&nbsp;&nbsp;태</p>
							<span class="wid1">도소매, 서비스</span>
							<p>전화번호</p>
							<span class="wid1">02-867-2626</span>
						</li>
						<li>
							<p>종&nbsp;&nbsp;&nbsp;&nbsp;목</p>
							<span class="wid1 letter2">컴퓨터, 소프트웨어개발, 시스템통합및관리자문</span>
							<p>팩스번호</p>
							<span class="wid1">02-867-2621</span>
						</li>
					</ul>
				</div>
<%
If Not (rs.eof) Then
	Totalprice = 0
	i = 0
	Do While Not rs.eof
		If rs("mem_price") = 0 Then
			finalPrice = rs("g_Money")
		Else
			finalPrice = rs("mem_price")
		End If

		Totalprice = Totalprice + (finalPrice * arr_goods_cnt(i))
		i = i + 1
		rs.MoveNext
	Loop

	rs.MoveFirst
End If
%>
				<div class="total">
					<p>합계금액</p>
					<div class="center"><!--strong>일금 이천이백 원정 </strong--><span class="total_price_txt c_red">₩ <%=FormatNumber(Totalprice+(Totalprice/10),0)%></span> <span class="c_red"> &lt;V.A.T포함&gt;</span></div>
				</div>

			</div>
		</div>

		<div class="box sectE2">
			<table class="tableE">
				<colgroup>
					<col style="width:60px">
					<col style="width:70px">
					<col style="width:273px">
					<col style="width:33px">
					<col style="width:60px">
					<col style="width:75px">
					<col style="width:99px">
					<col style="width:75px">
					<col style="width:38px">
				</colgroup>
				<thead>
					<tr>
						<th>순&nbsp;번</th>
						<th colspan="2">품목명 / 규격</th>
						<th>단&nbsp;위</th>
						<th>수&nbsp;량</th>
						<th>단&nbsp;가</th>
						<th>공급가액</th>
						<th>세&nbsp;액</th>
						<th>비&nbsp;고</th>
					</tr>
				</thead>
				<tbody>
<%
If Not (rs.eof) Then
	i = 1
	Totalprice = 0
	Do While Not rs.eof
		If rs("mem_price") = 0 Then
			finalPrice = rs("g_Money")
		Else
			finalPrice = rs("mem_price")
		End If
%>
					<tr class="pd_info">
						<td class="first" rowspan="2"><%=i%></td>
						<td colspan="2" class="left"><%=rs("g_name")%></td>
						<td>EA</td>
						<td><%=arr_goods_cnt(i-1)%></td>
						<td class="c_red"><%=FormatNumber(rs("g_Money"),0)%></td>
						<td><%=FormatNumber(finalPrice,0)%></td>
						<td><%=FormatNumber(finalPrice/10,0)%></td>
						<td rowspan="2"></td>
					</tr>
					<tr>
						<td colspan="7">
							<ul class="esti_con">
<%
		arr_g_optionList = Split(rs("g_optionList"), ",")

		If Ubound(arr_g_optionList) > 0 Then
			For j = 0 To Ubound(arr_g_optionList) - 1
				arr_g_optionList1 = Split(arr_g_optionList(j), "||")
%>
								<li><span class="tit"><%=Trim(arr_g_optionList1(0))%>:</span> <span class="con"><%=Trim(arr_g_optionList1(1))%></span></li>
<%
			Next
		End If
%>
							</ul>
						</td>
					</tr>
<%
			Totalprice = Totalprice + (finalPrice * arr_goods_cnt(i-1))
			i = i + 1
			rs.MoveNext
		Loop
	End If

	rs.close
%>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="4">공 급 가 합 계</th>
						<td colspan="5"><%=FormatNumber(Totalprice,0)%></td>
					</tr>
					<tr>
						<th colspan="4">V.A.T</th>
						<td colspan="5"><%=FormatNumber(Totalprice/10,0)%></td>
					</tr>
					<tr>
						<th colspan="4">총 합 계</th>
						<td colspan="5"><%=FormatNumber(Totalprice+(Totalprice/10),0)%></td>
					</tr>
				</tfoot>
			</table>


			<table class="tableE ver2">
				<colgroup>
					<col style="width:145px">
					<col style="width:198px">
					<col style="width:145px">
					<col style="width:281px">
				</colgroup>
				<tbody>
					<tr class="first">
						<th>납기일자</th>
						<td>별도 협의</td>
						<th>납품장소</th>
						<td><strong>귀사 지정장소</strong><br>(기본 택배배송 / 기타배송 요청 시 별도 협의요망)</td>
					</tr>
					<tr>
						<th>유효일자</th>
						<td>2020년 08월 24일</td>
						<th>결제조건</th>
						<td>별도 협의</td>
					</tr>
					<tr>
						<th>비&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;고</th>
						<td colspan="3"></td>
					</tr>
					<tr>
						<th>발주확인</th>
						<td colspan="3">
							<div class="c_box">
								<p class="c_red">※ 본 견적서를 당사와 사전동의 없이 외부로 유출하여 당사에 손해가 발생할 시 법적인 손해 배상 및 보상을 요구할 수 있습니다.
								</p>
								<p class="c_blue">※ 발주 시 귀사의 명판 및 직인(또는 담당자 서명) 날인하여 당사 팩스(02-867-2621) 혹은 담당자 이메일을 통해 발신해주시기 바랍니다.</p>
							</div>
							<p>당사는 이 견적서 상의 가격 및 조건들을 수용하고 이 견적서를 귀사에 대한 공식 발주서로 대신하고자 합니다.</p>

							<div class="sign">발주자명 : <input type="text" class="sign_area"> (서명)</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>

</html>