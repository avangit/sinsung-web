<!-- #include virtual="/srm/usercheck.asp" -->
<%
'// 변수 설정
goods_list		= SQL_Injection(Trim(Request.Form("goods_list")))
goods_price		= SQL_Injection(Trim(Request.Form("goods_price")))
goods_cnt		= SQL_Injection(Trim(Request.Form("goods_cnt")))
arr_g_idx		= SQL_Injection(Trim(Request.Form("arr_g_idx")))
total_price		= SQL_Injection(Trim(Request.Form("price")))
total_cnt		= SQL_Injection(Trim(Request.Form("cnt")))

o_company		= SQL_Injection(Trim(Request.Form("o_company")))
o_name			= SQL_Injection(Trim(Request.Form("o_name")))
o_department	= SQL_Injection(Trim(Request.Form("o_department")))
o_mobile1		= SQL_Injection(Trim(Request.Form("o_mobile1")))
o_mobile2		= SQL_Injection(Trim(Request.Form("o_mobile2")))
o_mobile3		= SQL_Injection(Trim(Request.Form("o_mobile3")))
o_email1		= SQL_Injection(Trim(Request.Form("o_email1")))
o_email2		= SQL_Injection(Trim(Request.Form("o_email2")))

s_name			= SQL_Injection(Trim(Request.Form("s_name")))
s_phone1		= SQL_Injection(Trim(Request.Form("s_phone1")))
s_phone2		= SQL_Injection(Trim(Request.Form("s_phone2")))
s_phone3		= SQL_Injection(Trim(Request.Form("s_phone3")))
s_mobile1		= SQL_Injection(Trim(Request.Form("s_mobile1")))
s_mobile2		= SQL_Injection(Trim(Request.Form("s_mobile2")))
s_mobile3		= SQL_Injection(Trim(Request.Form("s_mobile3")))
s_email1		= SQL_Injection(Trim(Request.Form("s_email1")))
s_email2		= SQL_Injection(Trim(Request.Form("s_email2")))
s_zip			= SQL_Injection(Trim(Request.Form("s_zip")))
s_addr1			= SQL_Injection(Trim(Request.Form("s_addr1")))
s_addr2			= SQL_Injection(Trim(Request.Form("s_addr2")))
s_request		= SQL_Injection(Trim(Request.Form("s_request")))

payment			= SQL_Injection(Trim(Request.Form("payment")))
d_name			= SQL_Injection(Trim(Request.Form("d_name")))
d_bank			= SQL_Injection(Trim(Request.Form("d_bank")))
o_cNum			= SQL_Injection(Trim(Request.Form("o_cNum")))
o_cName			= SQL_Injection(Trim(Request.Form("o_cName")))
o_ceo			= SQL_Injection(Trim(Request.Form("o_ceo")))
o_cemail1		= SQL_Injection(Trim(Request.Form("o_cemail1")))
o_cemail2		= SQL_Injection(Trim(Request.Form("o_cemail2")))

'// 변수 가공
goods_price = Replace(goods_price,"원","")
o_mobile	=  o_mobile1 & "-" & o_mobile2 & "-" & o_mobile3
o_email		=  o_email1 & "@" & o_email2
s_phone		=  s_phone1 & "-" & s_phone2 & "-" & s_phone3
s_mobile	=  s_mobile1 & "-" & s_mobile2 & "-" & s_mobile3
s_email		=  s_email1 & "@" & s_email2

If o_cemail1 <> "" Then
	o_cemail = o_cemail1 & "@" & o_cemail2
End If

If payment = "yeosin" Then
	o_status = 0
ElseIf payment = "account" Then
	o_status = 1
ElseIf payment = "card" Then
	o_status = 2
End If

o_num = DateDiff("s", "1970-01-01", now()) - (9*60*60)

If payment <> "card" Then
	sql = "SET NOCOUNT ON;"
	sql = sql & "INSERT INTO orders("
	sql = sql & "o_num, "
	sql = sql & "g_idx, "
	sql = sql & "g_price, "
	sql = sql & "g_cnt, "
	sql = sql & "o_total_mount, "
	sql = sql & "o_total_price, "
	sql = sql & "o_company, "
	sql = sql & "o_name, "
	sql = sql & "o_department, "
	sql = sql & "o_mobile, "
	sql = sql & "o_email, "
	sql = sql & "s_name, "
	sql = sql & "s_phone, "
	sql = sql & "s_mobile, "
	sql = sql & "s_email, "
	sql = sql & "s_zip, "
	sql = sql & "s_addr1, "
	sql = sql & "s_addr2, "
	sql = sql & "s_request, "
	sql = sql & "o_payment, "
	sql = sql & "o_card_name, "
	sql = sql & "o_bank_name, "
	sql = sql & "o_cNum, "
	sql = sql & "o_cName, "
	sql = sql & "o_ceo, "
	sql = sql & "o_cEmail, "
	sql = sql & "o_status, "
	sql = sql & "strName, "
	sql = sql & "strId, "
	sql = sql & "masterId, "
	sql = sql & "intSeq) VALUES("
	sql = sql & "'" & o_num & "',"
	sql = sql & "'" & arr_g_idx & "',"
	sql = sql & "'" & goods_price & "',"
	sql = sql & "'" & goods_cnt & "',"
	sql = sql & "'" & total_cnt & "',"
	sql = sql & "'" & total_price & "',"
	sql = sql & "N'" & o_company & "',"
	sql = sql & "N'" & o_name & "',"
	sql = sql & "N'" & o_department & "',"
	sql = sql & "'" & o_mobile & "',"
	sql = sql & "'" & o_email & "',"
	sql = sql & "N'" & s_name & "',"
	sql = sql & "'" & s_phone & "',"
	sql = sql & "'" & s_mobile & "',"
	sql = sql & "'" & s_email & "',"
	sql = sql & "'" & s_zip & "',"
	sql = sql & "N'" & s_addr1 & "',"
	sql = sql & "N'" & s_addr2 & "',"
	sql = sql & "N'" & s_request & "',"
	sql = sql & "'" & payment & "',"
	sql = sql & "N'" & card_name & "',"
	sql = sql & "N'" & d_bank & "',"
	sql = sql & "'" & o_cNum & "',"
	sql = sql & "N'" & o_cName & "',"
	sql = sql & "'" & o_ceo & "',"
	sql = sql & "'" & o_cEmail & "',"
	sql = sql & "'" & o_status & "',"
	sql = sql & "N'" & strName & "',"
	sql = sql & "'" & strId & "',"
	sql = sql & "'" & masterID & "',"
	sql = sql & "'" & intSeq & "'); SELECT SCOPE_IDENTITY();"
	sql = sql & "SET NOCOUNT OFF;"

	Set rs = dbconn.execute(sql)

	idx = rs(0)

	rs.close
	Set rs = Nothing

	If payment <> "yeosin" Then
		dbconn.execute("DELETE FROM cart WHERE c_idx IN (" & goods_list & ")")
	End If
'	dbconn.execute("INSERT INTO order_Record (menu,manage_id,manage_name,ip) VALUES('회원등록','"&session("aduserid")&"','"&session("adusername")&"','"&Request.ServerVariables("REMOTE_ADDR")&"') ")
End If

Call DbClose()

Call jsAlertMsgUrl("주문이 완료되었습니다.", "./order_finish.asp?idx="&idx)
%>