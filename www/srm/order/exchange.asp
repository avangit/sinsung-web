<!-- #include virtual="/srm/usercheck.asp" -->
<%
	sql = "SELECT * FROM Record_views3"
	Set rs = dbconn.execute(sql)

	If Not rs.eof Then
		cost = rs("cost")
	End If

	rs.close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script src="/srm/js/order.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(3)").addClass("active");
		});
	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
	o_idx = SQL_Injection(Trim(Request.QueryString("idx")))

	sql = "SELECT * FROM orders WHERE o_idx = " & o_idx & " AND strId = '" & strId & "'"
	Set rs = dbconn.execute(sql)

	If rs.eof Or rs.bof Then
		Call jsAlertMsgBack("일치하는 정보가 없습니다.")
	Else
		If rs("o_status") = "1" Then
			status_txt = "결제대기"
		ElseIf rs("o_status") = "2" Then
			status_txt = "결제확인"
		End If

		If rs("o_payment") = "card" Then
			payment_txt = "카드결제"
		ElseIf rs("o_payment") = "account" Then
			payment_txt = "무통장입금"
		ElseIf rs("o_payment") = "yeosin" Then
			payment_txt = "여신결제"
		End If
	End If
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="order_view return">
					<div class="inner">
						<div class="sub_tit">
							<h2>교환요청</h2>
						</div>
						<table class="table table_type03">
							<colgroup>
								<col style="width:600px">
								<col style="width:200px">
								<col style="width:200px">
							</colgroup>
							<thead>
								<tr>
									<th>상품정보</th>
									<th>상품금액</th>
									<th>수량</th>
								</tr>
							</thead>
							<tbody>
<%
	If InStr(rs("g_idx"), ",") > 0 Then
		arr_g_idx = Split(rs("g_idx"), ",")
		arr_g_price = Split(rs("g_price"), ",")
		arr_g_cnt = Split(rs("g_cnt"), ",")

		NextArr = Ubound(arr_g_idx)
	Else
		arr_g_idx = rs("g_idx")
		arr_g_price = rs("g_price")
		arr_g_cnt = rs("g_cnt")

		NextArr = 0
	End If
		For i = 0 To NextArr
			If NextArr > 0 Then
				arr_idx = arr_g_idx(i)
				arr_price = arr_g_price(i)
				arr_cnt = arr_g_cnt(i)
			Else
				arr_idx = arr_g_idx
				arr_price = arr_g_price
				arr_cnt = arr_g_cnt
			End If
			sql2 = "SELECT g_name, g_simg FROM GOODS WHERE g_idx = '" & arr_idx & "'"
			Set rs_g_name = dbconn.execute(sql2)
%>
								<tr>
									<td>
										<div class="cart_div">
											<div class="img_box">
												<a href="#"><img src="/upload/goods/<%=rs_g_name("g_simg")%>"></a>
											</div>
											<div class="txt_box">
												<a href="#" class="pro_code">제품코드 : #<%=arr_idx%></a>
												<p class="pro_name"><%=rs_g_name("g_name")%></p><br>
											</div>
										</div>
									</td>
									<td class="price"><%=FormatNumber(arr_price, 0)%>원</td>
									<td><%=arr_cnt%>개</td>
								</tr>
<%
			rs_g_name.close
			Set rs_g_name = Nothing
		Next
%>
							</tbody>
						</table>

						<div class="cart_price_box2">
							<div class="price">
								<p>
									<span>구매금액</span>
									<%=FormatNumber(rs("o_total_price")-cost, 0)%>원
								</p>
								<img src="/srm/images/sub/icon_plus.png" alt="">
								<p>
									<span>배송비</span>
									<%=FormatNumber(cost, 0)%>원
								</p>
								<img src="/srm/images/sub/icon_total.png" alt="">
								<p>
									<span>총 결제금액</span>
									<%=FormatNumber(rs("o_total_price"), 0)%>원
								</p>
							</div>
						</div>

						<div class="order_middle">
							<div class="section_tit">
								<h4>주문/결제정보</h4>
							</div>
							<table class="table table_type02">
								<colgroup>
									<col width="200px">
									<col width="500px">
									<col width="200px">
									<col width="500px">
								</colgroup>
								<tbody>
								   <tr><th>주문상태</th>
								   <td colspan="3"><%=status_txt%></td></tr>
									<tr>
										<th>주문번호</th>
										<td><%=rs("o_num")%></td>
										  <th>주문일시</th>
										<td><%=rs("regdate")%></td>
									</tr>
									<tr>
										<th>결제금액</th>
										<td><%=FormatNumber(rs("o_total_price"), 0)%>원</td>
										<th>결제수단</th>
										<td><%=payment_txt%></td>
									</tr>
								</tbody>
							</table>
<%
rs.close
Set rs = Nothing
%>
						</div>
						<div class="order_middle">
							<div class="section_tit">
								<h4>교환 요청</h4>
							</div>
							<form name="exchangeForm" method="post" action="exchange_ok.asp">
							<input type="hidden" name="idx" value="<%=o_idx%>">
							<table class="table table_type02 textarea_table">
								<colgroup>
									<col width="200px">
									<col width="500px">
									<col width="200px">
									<col width="500px">
								</colgroup>
								<tbody>
								   <tr class="text_area"><th>교환요청 사유 <span>*</span></th>
								   <td colspan="3"><textarea name="reason" placeholder="교환요청 내용을 입력해주세요"></textarea></td></tr>
								</tbody>
							</table>

						</div>


						<div class="btn_box">
							<button class="btn_wh" onclick="location.href='order_list2.asp'">취소</button>
							<button class="btn_blue" onclick="exchange_write();return false;">확인</button>
						</div>
						</form>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->