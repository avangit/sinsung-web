<!-- #include virtual="/srm/usercheck.asp" -->

<%
sdate		= SQL_Injection(Trim(Request.QueryString("sdate")))
edate		= SQL_Injection(Trim(Request.QueryString("edate")))
fieldname	= SQL_Injection(Trim(Request.QueryString("fieldname")))
fieldvalue	= SQL_Injection(Trim(Request.QueryString("fieldvalue")))
page		= SQL_Injection(Trim(Request.QueryString("page")))
orderstep	= SQL_Injection(Trim(Request.QueryString("orderstep")))

whereQuery = " "
If sdate <> "" And edate <> "" Then
	query_where = query_where &  " AND dtmInsertDate BETWEEN '" & sdate & "' AND DATEADD(day,1,'"&edate&"')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where &  " AND dtmInsertDate >= '" & sdate  &"'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where &  " AND dtmInsertDate <= DATEADD(day,1,'"&edate&"')"
End If

If fieldvalue <> "" Then
	fieldvalue = whereQuery & " And " & fieldname & " LIKE '%" & fieldvalue & "%'"
End If

If orderstep <> "" Then
	whereQuery = whereQuery & " And o_status = '" & orderstep & "'"
End If

sql = "SELECT * FROM orders WHERE o_payment = 'yeosin' " & whereQuery & " ORDER BY o_idx DESC"
Set rs = dbconn.execute(sql)

fn = "전자결재 리스트"

Response.Buffer=True
Response.ContentType = "application/vnd.ms-excel;charset=utf-8"
Response.CacheControl = "public"
Response.AddHeader "Content-Disposition", "attachment;filename="& Server.URLPathEncode(fn) &".xls"
%>

	<table width="100%" cellpadding="2" cellspacing="1" border="1" bgcolor="#ffffff">
		<tr>
			<th>주문일자</th>
			<th>주문번호</th>
			<th>주문정보</th>
			<th>주문금액</th>
			<th>주문자</th>
			<th>주문부서</th>
			<th>주문상태</th>
		</tr>
<%
If Not rs.eof Then

	If rs("o_status") = "0" Then
		status_txt = "구매요청"
	ElseIf rs("o_status") = "15" Then
		status_txt = "구매반려"
	ElseIf rs("o_status") = "20" Then
		status_txt = "구매승인"
	End If

	Do While Not rs.eof
		strSQL = "SELECT strName FROM mTb_Member2 WHERE intSeq = " & rs("intSeq")
		Set rs2 = dbconn.execute(strSQL)

		If Not(rs2.bof Or rs2.eof) Then
			order_name = rs2("strName")
		End If

		rs2.close
		Set rs2 = Nothing
%>
		<tr>
			<td align="center"><%=Left(rs("regdate"), 10)%></td>
			<td style="mso-number-format:'\@'" align="center"><%=rs("o_num")%></td>
			<td align="center"><%=rs("o_total_mount")%>건</td>
			<td align="center"><%=FormatNumber(rs("o_total_price"), 0)%></td>
			<td align="center"><%=order_name%></td>
			<td align="center"><%=rs("o_department")%></td>
			<td align="center"><%=status_txt%></td>
		</tr>
<%
		rs.MoveNext
	Loop

	rs.close()
	Set rs = Nothing
End If

Call DbClose()
%>
	<table>