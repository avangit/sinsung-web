<!-- #include virtual="/srm/usercheck.asp" -->

<%
sdate		= SQL_Injection(Trim(Request.QueryString("sdate")))
edate		= SQL_Injection(Trim(Request.QueryString("edate")))
fieldname	= SQL_Injection(Trim(Request.QueryString("fieldname")))
fieldvalue	= SQL_Injection(Trim(Request.QueryString("fieldvalue")))
page		= SQL_Injection(Trim(Request.QueryString("page")))
orderstep	= SQL_Injection(Trim(Request.QueryString("orderstep")))

whereQuery = " "
If sdate <> "" And edate <> "" Then
	query_where = query_where &  " AND dtmInsertDate BETWEEN '" & sdate & "' AND DATEADD(day,1,'"&edate&"')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where &  " AND dtmInsertDate >= '" & sdate  &"'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where &  " AND dtmInsertDate <= DATEADD(day,1,'"&edate&"')"
End If

If fieldvalue <> "" Then
	fieldvalue = whereQuery & " And " & fieldname & " LIKE '%" & fieldvalue & "%'"
End If

If orderstep <> "" Then
	whereQuery = whereQuery & " And o_status = '" & orderstep & "'"
End If

sql = "SELECT * FROM orders WHERE o_payment <> 'yeosin' " & whereQuery & " ORDER BY o_idx DESC"
Set rs = dbconn.execute(sql)

fn = "주문배송 리스트"

Response.Buffer=True
Response.ContentType = "application/vnd.ms-excel;charset=utf-8"
Response.CacheControl = "public"
Response.AddHeader "Content-Disposition", "attachment;filename="& Server.URLPathEncode(fn) &".xls"
%>

	<table width="100%" cellpadding="2" cellspacing="1" border="1" bgcolor="#ffffff">
		<tr>
			<th>주문일자</th>
			<th>주문번호</th>
			<th>주문정보</th>
			<th>주문금액</th>
			<th>주문상태</th>
		</tr>
<%
If Not rs.eof Then

	If rs("o_status") = "1" Then
		status_txt = "결제대기"
	ElseIf rs("o_status") = "2" Then
		status_txt = "결제확인"
	ElseIf rs("o_status") = "3" Then
		status_txt = "상품배송"
	ElseIf rs("o_status") = "4" Then
		status_txt = "판매완료"
	ElseIf rs("o_status") = "91" Then
		status_txt = "취소"
	ElseIf rs("o_status") = "92" Then
		status_txt = "교환"
	ElseIf rs("o_status") = "93" Then
		status_txt = "반품"
	End If

	Do While Not rs.eof
%>
		<tr>
			<td align="center"><%=Left(rs("regdate"), 10)%></td>
			<td style="mso-number-format:'\@'" align="center"><%=rs("o_num")%></td>
			<td align="center"><%=rs("o_total_mount")%>건</td>
			<td align="center"><%=FormatNumber(rs("o_total_price"), 0)%></td>
			<td align="center"><%=status_txt%></td>
		</tr>
<%
		rs.MoveNext
	Loop
End If

rs.close()
Set rs = Nothing

Call DbClose()
%>
	<table>