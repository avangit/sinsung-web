<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="/srm/js/order.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(2)").addClass("active");

			$("#sdate").datepicker({
				showOn: "both",
				buttonImageOnly: false,
				buttonText: "날짜 선택",
				dateFormat: "yy-mm-dd"
			});
			$("#edate").datepicker({
				showOn: "both",
				buttonImageOnly: false,
				buttonText: "날짜 선택",
				dateFormat: "yy-mm-dd"
			});
		});

		$(document).ready(function() {
			var posY;

			function bodyFreezeScroll() {
				posY = $(window).scrollTop();
				$("html").addClass('fix');
				$("html").css("top", -posY);
			}

			function bodyUnfreezeScroll() {
				$("html").removeAttr('class');
				$("html").removeAttr('style');
				posY = $(window).scrollTop(posY);
			}
			$('.thumb_img a, .video .video_area ul li a').click(function() {
				$('.popup').fadeIn();
				bodyFreezeScroll();
			});
			$('.popup .btn_close').click(function() {
				$('.popup').fadeOut();
				bodyUnfreezeScroll()
			});

		});

		function frmReset() {
				history.replaceState({}, null, location.pathname);
				$("#fieldname").val('');
				$("#fieldvalue").val('');
				$("#sdate").val('');
				$("#edate").val('');

		}

		function frmSubmit() {
			if($("#fieldvalue").val() == "" && $("#sdate").val() == "" && $("#edate").val() == "") {
				alert("검색어를 입력하세요");
				return false;
			} else {
				document.sch_Form.submit();
			}
		}

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="order_list table_type01">
					<div class="inner">
						<div class="sub_tit">
							<h2>구매 전자결재</h2>
						</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim dayday 		: dayday 		=  SQL_Injection(Trim(Request("d")))
Dim sdate 		: sdate 		=  SQL_Injection(Trim(Request("sdate")))
Dim edate 		: edate 		=  SQL_Injection(Trim(Request("edate")))
Dim orderstep	: orderstep		=  SQL_Injection(Trim(Request("orderstep")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "orders"
Dim query_where			: query_where		= " o_payment = 'yeosin' AND strId = '" & strId & "'"
Dim query_orderby		: query_orderby		= " ORDER BY o_idx DESC"

If dayday = "" Then
'	sdate = ""
'	edate = ""
ElseIf dayday = "7" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -7, Date)
	edate = Date
ElseIf dayday = "30" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -30, Date)
	edate = Date
ElseIf dayday = "365" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -365, Date)
	edate = Date
End If

If sdate <> "" And edate <> "" Then
	query_where = query_where & " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day, 1, '" & edate & "')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where & " AND regdate >= '" & sdate & "'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where & " AND regdate <= DATEADD(day, 1, '" & edate & "')"
End If

If orderstep <> "" Then
	query_where = query_where &" AND o_status = '" & orderstep & "' "
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
						<div class="tab_contents" id="order_01">
							<table class="search_table">
								<colgroup>
									<col style="width:120px">
									<col style="width:540px;">
									<col style="width:120px">
									<col style="width:400px">
								</colgroup>
								<tbody>
									<tr>
										<th>기간검색</th>
										<td>
											<div class="tab_btn">
												<button onclick="location.href='./order_list.asp'" <% If dayday = "" Then %> class="on"<% End If %>>전체</button>
												<button onclick="location.href='./order_list.asp?d=7'" <% If dayday = "7" Then %> class="on"<% End If %>>1주일</button>
												<button onclick="location.href='./order_list.asp?d=30'" <% If dayday = "30" Then %> class="on"<% End If %>>1개월</button>
												<button onclick="location.href='./order_list.asp?d=365'" <% If dayday = "365" Then %> class="on"<% End If %>>1년</button>
											</div>
							<form name="sch_Form" id="sch_Form" method="post">
											<div>
												<input type="text" id="sdate" name="sdate" class="date" value="<%=sdate%>" autocomplete="off"> -
												<input type="text" id="edate" name="edate" class="date" value="<%=edate%>" autocomplete="off">
											</div>
										</td>
										<th>조건검색</th>
										<td>
											<select name="fieldname" id="fieldname">
												<option value="o_num" <% If fieldname = "o_num" Then %> selected<% End If %>>주문번호</option>
												<option value="o_serial" <% If fieldname = "o_serial" Then %> selected<% End If %>>시리얼번호</option>
												<option value="o_department" <% If fieldname = "o_department" Then %> selected<% End If %>>부서</option>
												<input type="text" name="fieldvalue" id="fieldvalue" value="<%=fieldvalue%>" onkeypress="if( event.keyCode == 13 ){frmSubmit();return false;}">
											</select>
										</td>
									</tr>
								</tbody>
							</table>

							<div class="btn_box">
								<button class="btn_wh" onclick="frmReset();">검색 초기화</button>
								<button class="btn_blue" onclick="frmSubmit();return false;">검색</button>
							</div>
							</form>

							<form name="s_Form" method="post">
							<p class="cont"><span><%=intTotalCount%></span>건</p>
							<div class="a_option">
								<select name="orderstep" onchange="document.s_Form.submit();">
									<option value="">승인상태</option>
									<option value="0" <% If orderstep = "0" Then %> selected<% End If %>>구매요청</option>
									<option value="20" <% If orderstep = "20" Then %> selected<% End If %>>구매승인</option>
									<option value="15" <% If orderstep = "15" Then %> selected<% End If %>>구매반려</option>
								</select>
							</div>
							</form>

							<form name="orderForm0" method="post" action="./order0_status_ok.asp">
							<input type="hidden" name="n">
							<input type="hidden" name="m">
							<input type="hidden" name="message">
							<table class="table type01">

								<colgroup>
									<col style="width:60px">
									<col style="width:100px">
									<col style="width:150px">
									<col style="width:150px">
									<col style="width:150px">
									<col style="width:100px">
									<col style="width:100px">
									<col style="width:100px">
								</colgroup>
								<thead>
									<tr>
										<th><div class="chk_box">
											<input id="order_chk" type="checkbox" value="" onclick="checkAll(document.orderForm0);">
											<label for="order_chk"><span></span></label>
										</div></th>
										<th>주문일자</th>
										<th>주문번호</th>
										<th>주문정보</th>
										<th>주문금액</th>
										<th>주문자</th>
										<th>주문부서</th>
										<th>주문상태</th>
									</tr>
								</thead>
								<tbody>
<%
If rs.bof Or rs.eof Then
%>
								<tr><td colspan="8" align="center">등록된 전재결재 내역이 없습니다.</td></tr>
<%
Else
	i = 1
	rs.move MoveCount
	Do While Not rs.eof

		If InStr(rs("g_cnt"), ",") > 0 Then
			arr_ea = Ubound(Split(rs("g_cnt"), ",")) + 1
		Else
			arr_ea = rs("g_cnt")
		End If

		If rs("o_status") = "0" Then
			status_txt = "구매요청"
		ElseIf rs("o_status") = "15" Then
			status_txt = "구매반려"
		ElseIf rs("o_status") = "20" Then
			status_txt = "구매승인"
		End If

		strSQL = "SELECT strName FROM mTb_Member2 WHERE intSeq = " & rs("intSeq")
		Set rs2 = dbconn.execute(strSQL)

		If Not(rs2.bof Or rs2.eof) Then
			order_name = rs2("strName")
		End If

		rs2.close
		Set rs2 = Nothing
%>
									<tr>
										<td>
											<div class="chk_box">
											<input id="order_chk<%=i%>" type="checkbox" value="<%=rs("o_idx")%>">
											<label for="order_chk<%=i%>"><span></span></label>
										</div></td>
										<td><%=Left(rs("regdate"), 10)%></td>
										<td><a href="./order_view.asp?idx=<%=rs("o_idx")%>"><%=rs("o_num")%></a></td>
										<td ><%=arr_ea%>건</td>
										<td class="price">
											<%=FormatNumber(rs("o_total_price"), 0)%>원
										</td>
										<td><%=order_name%></td>
										<td><%=rs("o_department")%></td>
										<td><%=status_txt%></td>
									</tr>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
									</tbody>
								</table>
							<div class="btn_box save_btn">
								<div class="left">
									<button class="btn_blue" onclick="javascript:GetCheckbox(document.orderForm0, 'chg15', '반려');return false;">반려</button>
									<button class="btn_bk" onclick="javascript:GetCheckbox(document.orderForm0, 'chg20', '승인');return false;">승인</button>
								</div>
								</form>
							</div>
							<div class="btn_right">
							<button class="btn_bk" onclick="location.href='./exceldownload.asp?sdate=<%=sdate%>&edate=<%=edate%>&fieldname=<%=fieldname%>&fieldvalue=<%=fieldvalue%>&page=<%=intNowPage%>&orderstep=<%=orderstep%>'">엑셀파일 저장</button>
							</div>
						</div>

						<% Call Paging_user_srm("") %>
						<!--div class="list_paging">
							<a class="paging_prev" href="#">처음</a>
							<ul>
								<li><a href="#">21</a></li>
								<li><a href="#">22</a></li>
								<li class="active"><a href="#">23</a></li>
								<li><a href="#">24</a></li>
								<li><a href="#">25</a></li>
							</ul>
							<a class="paging_next" href="#">맨끝</a>
						</div-->
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->