<!-- #include virtual="/srm/usercheck.asp" -->
<%
    ' CA : 카드승인, CAO : 카드승인옵션
    ' CC : 카드승인취소, CCO : 카드승인취소옵션, CPC : 카드매입취소

    res_cd               = request("res_cd")           ' 응답코드          (CA, CAO, CC, CCO, CPC)
    res_msg              = unescape(request("res_msg"))          ' 응답메시지        (CA, CAO, CC, CCO, CPC)
    cno                  = request("cno")              ' PG거래번호        (CA, CAO, CC, CCO, CPC)
    amount               = request("amount")           ' 총 결제금액       (CA,                  )
    order_no             = request("order_no")         ' 주문번호          (CA,                  )
    auth_no              = request("auth_no")          ' 승인번호          (CA,                  )
    tran_date            = request("tran_date")        ' 승인일시          (CA,      CC,      CPC)
    escrow_yn            = request("escrow_yn")        ' 에스크로 사용유무 (CA,                  )
    complex_yn           = request("complex_yn")       ' 복합결제 유무     (CA,                  )
    stat_cd              = request("stat_cd")          ' 상태코드          (CA,      CC,      CPC)
    stat_msg             = request("stat_msg")         ' 상태메시지        (CA,      CC,      CPC)
    pay_type             = request("pay_type")         ' 결제수단          (CA,                  )
    mall_id              = request("mall_id")          ' 가맹점 Mall ID    (CA                   )
    card_no              = request("card_no")          ' 카드번호          (CA,          CCO     )
    issuer_cd            = request("issuer_cd")        ' 발급사코드        (CA,          CCO     )
    issuer_nm            = request("issuer_nm")        ' 발급사명          (CA,          CCO     )
    acquirer_cd          = request("acquirer_cd")      ' 매입사코드        (CA,          CCO     )
    acquirer_nm          = request("acquirer_nm")      ' 매입사명          (CA,          CCO     )
    install_period       = request("install_period")   ' 할부개월          (CA,          CCO     )
    noint                = request("noint")            ' 무이자여부        (CA                   )
    part_cancel_yn       = request("part_cancel_yn")   ' 부분취소 가능여부 (CA                   )
    card_gubun           = request("card_gubun")       ' 신용카드 종류     (CA                   )
    card_biz_gubun       = request("card_biz_gubun")   ' 신용카드 구분     (CA                   )
    cpon_flag            = request("cpon_flag")        ' 쿠폰 사용유무     (    CAO,     CCO     )
    used_cpon            = request("used_cpon")        ' 쿠폰 사용금액     (    CAO              )
    canc_acq_date        = request("canc_acq_date")    ' 매입취소일시      (                  CPC)
    canc_date            = request("canc_date")        ' 취소일시          (CC,               CPC)
    account_no           = request("account_no")       ' 계좌번호          (CC,                  )

	arr_g_idx			= request("arr_g_idx")
	g_price				= Replace(request("g_price"), "원", "")
	arr_g_name			= unescape(request("arr_g_name"))
	g_cnt				= request("g_cnt")
	o_total_mount		= request("o_total_mount")
	o_total_price		= request("o_total_price")
	o_company			= unescape(request("o_company"))
	o_name				= unescape(request("o_name"))
	o_department		= unescape(request("o_department"))
	o_mobile			= request("o_mobile")
	o_email				= request("o_email")
	s_name				= unescape(request("s_name"))
	s_phone				= request("s_phone")
	s_mobile			= request("s_mobile")
	s_email				= request("s_email")
	s_zip				= request("s_zip")
	s_addr1				= unescape(request("s_addr1"))
	s_addr2				= unescape(request("s_addr2"))
	s_request			= unescape(request("s_request"))

	If InStr(arr_g_idx, ",") > 0 Then
		total_mount = Ubound(Split(arr_g_idx, ",")) + 1
	Else
		total_mount = 1
	End If

	final_order_no = DateDiff("s", "1970-01-01", now()) - (9*60*60)

	sql = "SELECT c_idx FROM cart WHERE g_idx IN(" & arr_g_idx & ") AND mem_id = '" & strId & "'"
	Set rs = Server.CreateObject("ADODB.RecordSet")

	rs.CursorType = 3
	rs.CursorLocation = 3
	rs.LockType = 3
	rs.Open sql, dbconn

	If Not rs.eof Then
		i = 1
		Do While Not rs.eof
			If i = rs.RecordCount Then
				comma = ""
			Else
				comma = ","
			End If

			c_idx = c_idx & rs("c_idx") & comma

			i = i + 1
			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing

	If res_cd = "0000" Then
		strSQL = ""
		strSQL = strSQL & "INSERT INTO orders("
		strSQL = strSQL & "o_num, "
		strSQL = strSQL & "g_idx, "
		strSQL = strSQL & "g_price, "
		strSQL = strSQL & "g_cnt, "
		strSQL = strSQL & "o_total_mount, "
		strSQL = strSQL & "o_total_price, "
		strSQL = strSQL & "o_company, "
		strSQL = strSQL & "o_name, "
		strSQL = strSQL & "o_department, "
		strSQL = strSQL & "o_mobile, "
		strSQL = strSQL & "o_email, "
		strSQL = strSQL & "s_name, "
		strSQL = strSQL & "s_phone, "
		strSQL = strSQL & "s_mobile, "
		strSQL = strSQL & "s_email, "
		strSQL = strSQL & "s_zip, "
		strSQL = strSQL & "s_addr1, "
		strSQL = strSQL & "s_addr2, "
		strSQL = strSQL & "s_request, "
		strSQL = strSQL & "o_payment, "
		strSQL = strSQL & "o_card_name, "
		strSQL = strSQL & "o_bank_name, "
		strSQL = strSQL & "o_cNum, "
		strSQL = strSQL & "o_cName, "
		strSQL = strSQL & "o_ceo, "
		strSQL = strSQL & "o_cEmail, "
		strSQL = strSQL & "o_status, "
		strSQL = strSQL & "strName, "
		strSQL = strSQL & "strId, "
		strSQL = strSQL & "masterId, "
		strSQL = strSQL & "intSeq) VALUES("
		strSQL = strSQL & "'" & final_order_no & "',"
		strSQL = strSQL & "'" & arr_g_idx & "',"
		strSQL = strSQL & "'" & g_price & "',"
		strSQL = strSQL & "'" & g_cnt & "',"
		strSQL = strSQL & "'" & total_mount & "',"
		strSQL = strSQL & "'" & amount & "',"
		strSQL = strSQL & "N'" & o_company & "',"
		strSQL = strSQL & "N'" & o_name & "',"
		strSQL = strSQL & "N'" & o_department & "',"
		strSQL = strSQL & "'" & o_mobile & "',"
		strSQL = strSQL & "'" & o_email & "',"
		strSQL = strSQL & "N'" & s_name & "',"
		strSQL = strSQL & "'" & s_phone & "',"
		strSQL = strSQL & "'" & s_mobile & "',"
		strSQL = strSQL & "'" & s_email & "',"
		strSQL = strSQL & "'" & s_zip & "',"
		strSQL = strSQL & "N'" & s_addr1 & "',"
		strSQL = strSQL & "N'" & s_addr2 & "',"
		strSQL = strSQL & "N'" & s_request & "',"
		strSQL = strSQL & "'card',"
		strSQL = strSQL & "N'" & card_name & "',"
		strSQL = strSQL & "N'" & d_bank & "',"
		strSQL = strSQL & "'" & o_cNum & "',"
		strSQL = strSQL & "N'" & o_cName & "',"
		strSQL = strSQL & "'" & o_ceo & "',"
		strSQL = strSQL & "'" & o_cEmail & "',"
		strSQL = strSQL & "'2',"
		strSQL = strSQL & "N'" & strName & "',"
		strSQL = strSQL & "'" & strId & "',"
		strSQL = strSQL & "'" & masterID & "',"
		strSQL = strSQL & "'" & intSeq & "'); DELETE FROM cart WHERE mem_id = '" & strId & "' AND c_idx IN (" & c_idx & ");"

		dbconn.execute(strSQL)
	End If
%>
<!doctype html>
<html>

<head>
    <!-- #include virtual="/srm/_inc/head.asp" -->
    <!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
    <link rel="stylesheet" href="/srm/_css/sub.css" />
	<script language="javascript" src="/easypay/web/js/default.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {
//            $("#A_Header .gnb ul.nav li:nth-child(2)").addClass("active");
        });


    </script>
</head>

<body>
    <div id="A_Wrap">
        <div id="A_Header">
            <!-- #include virtual="/srm/_inc/header.asp" -->
        </div>
        <div id="A_Container_Wrap">
            <div id="A_Container" class="main">
                <div id="sub_contents" class="order_finish">
                    <div class="inner">
                        <div class="sub_tit">
                            <h2>주문완료</h2>
                        </div>
                        <div class="progress_box">
                            <ul>
                                <li>1</li>
                                <li>2</li>
                                <li class="on">3</li>
                            </ul>
                        </div>

                        <div class="order_top">
                            <img src="/srm/images/sub/icon_order_finish.png">
                            <p><strong><%=arr_g_name%></strong> 의 주문이 정상적으로 접수되었습니다.</p>
                        </div>
                        <div class="order_middle">
                            <div class="section_tit">
                                <h4>주문/결제정보</h4>
                            </div>
                            <table class="table table_type02">
                                <colgroup>
                                    <col width="200px">
                                    <col width="500px">
                                    <col width="200px">
                                    <col width="500px">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th>주문번호</th>
                                        <td><%=final_order_no%></td>
                                        <th>주문일시</th>
                                        <td><%=Left(Date(), 10)%></td>
                                    </tr>
                                    <tr>
                                        <th>결제금액</th>
                                        <td><%=FormatNumber(amount, 0)%>원</td>
                                        <th>결제수단</th>
                                        <td>카드결제</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                        <div class="order_middle">
                            <div class="section_tit">
                                <h4>주문자 정보</h4>
                            </div>
                            <table class="table table_type02">
                                <colgroup>
                                    <col width="200px">
                                    <col width="500px">
                                    <col width="200px">
                                    <col width="500px">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th>회사명</th>
                                        <td><%=o_company%></td>
                                        <th>주문자</th>
                                        <td><%=o_name%></td>
                                    </tr>
                                    <tr>
                                        <th>주문부서</th>
                                        <td colspan="3"><%=o_department%></td>
                                    </tr>
                                    <tr>
                                        <th>휴대폰번호</th>
                                        <td><%=o_mobile%></td>
                                        <th>이메일 주소</th>
                                        <td><%=o_email%></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="order_middle">
                            <div class="section_tit">
                                <h4>배송지 정보</h4>
                            </div>
                            <table class="table table_type02">
                                <colgroup>
                                    <col width="200px">
                                    <col width="500px">
                                    <col width="200px">
                                    <col width="500px">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th>수령인</th>
                                        <td><%=s_name%></td>
                                        <th>연락처</th>
                                        <td><%=s_phone%></td>
                                    </tr>
                                    <tr>
                                        <th>이메일 주소</th>
                                        <td><%=s_email%></td>
                                        <th>휴대폰번호</th>
                                        <td><%=s_mobile%></td>
                                    </tr>
                                    <tr>
                                        <th>주소</th>
                                        <td colspan="3">[<%=s_zip%>] <%=s_addr1%> <br> <%=s_addr2%></td>
                                    </tr>
                                    <tr>
                                        <th>배송 시 요청사항</th>
                                        <td colspan="3"><%=s_request%></td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                        <div class="order_btn">
                            <button class="btn_wh" onclick="location.href='/srm/'">메인으로</button>
                            <button class="btn_blue" onclick="location.href='./order_list2.asp'">주문배송조회</button>
                        </div>


                    </div>


                </div>


            </div>
            <div id="A_Footer">
                <!-- #include virtual="/srm/_inc/footer.asp" -->
            </div>
        </div>

    </div>

</body>

</html>
<%
	Call DbClose()
%>
<!--body>
<table border="0" width="910" cellpadding="10" cellspacing="0">
    <tr>
        <td>
            <table border="0" width="900" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="30" bgcolor="#FFFFFF" align="left">&nbsp;<img src="/easypay/web/img/arow3.gif" border="0" align="absmiddle">&nbsp;<b>결과</b></td>
                </tr>
                <tr>
                    <td height="2" bgcolor="#2D4677"></td>
                </tr>
            </table>
            <table border="0" width="900" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="5"></td>
                </tr>
            </table>
            <table border="0" width="1000" cellpadding="0" cellspacing="1" bgcolor="#DCDCDC">
                <tr>
                    <td height="25" bgcolor="#EDEDED" width="150">&nbsp;응답코드</td>
                    <td bgcolor="#FFFFFF" width="180">&nbsp;<%=res_cd%></td>
                    <td bgcolor="#EDEDED" width="150">&nbsp;응답메시지</td>
                    <td bgcolor="#FFFFFF" width="180">&nbsp;<%=res_msg%></td>
                    <td height="25" bgcolor="#EDEDED" width="150">&nbsp;PG거래번호</td>
                    <td bgcolor="#FFFFFF" width="180">&nbsp;<%=cno%></td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;총 결제금액</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=amount%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;주문번호</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=order_no%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;승인번호</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=auth_no%></td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;승인일시</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=tran_date%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;에스크로 사용유무</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=escrow_yn%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;복합결제 유무</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=complex_yn%></td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;상태코드</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=stat_cd%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;계좌번호</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=account_no%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;결제수단</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=pay_type%></td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;가맹점 Mall ID</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=mall_id%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;카드번호</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=card_no%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;발급사코드</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=issuer_cd%></td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;발급사명</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=issuer_nm%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;매입사코드</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=acquirer_cd%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;매입사명</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=acquirer_nm%></td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;할부개월</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=install_period%></td>
                    <td bgcolor="#EDEDED">&nbsp;무이자여부</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=noint%></td>
                    <td bgcolor="#EDEDED">&nbsp;</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;부분취소 가능여부</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=part_cancel_yn%></td>
                    <td bgcolor="#EDEDED">&nbsp;신용카드 종류</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=card_gubun%></td>
                    <td height="25" bgcolor="#EDEDED">&nbsp;신용카드 구분</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=card_biz_gubun%></td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;쿠폰 사용유무</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=cpon_flag%></td>
                    <td bgcolor="#EDEDED">&nbsp;쿠폰 사용금액</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=used_cpon%></td>
                    <td bgcolor="#EDEDED">&nbsp;매입취소일시</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=canc_acq_date%></td>
                </tr>
                <tr>
                    <td height="25" bgcolor="#EDEDED">&nbsp;취소일시</td>
                    <td bgcolor="#FFFFFF">&nbsp;<%=canc_date%></td>
                    <td bgcolor="#EDEDED">&nbsp;</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#EDEDED">&nbsp;</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>
</body>
</html-->