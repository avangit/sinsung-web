<!-- #include virtual="/srm/usercheck.asp" -->
<%
g_idx = Request("idx")
g_cnt = Request("g_cnt")
GoodPrice = Request("GoodPrice")

sql = "SELECT * FROM Record_views3"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	cost = rs("cost")
End If

rs.close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script src="/srm/js/jquery.scrollfollow.js"></script>
	<script src="/srm/js/order.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
//			$("#A_Header .gnb ul.nav li:nth-child(2)").addClass("active");
		});

		$(window).scroll(function() {
			var scrollTop = $(document).scrollTop();
	        // console.log(scrolltop);
	        var height = $(document).height();
	        // console.log(height);
	        var windowHeight = $(window).height();

	        var height02 = $(document).height() - 218;
	        console.log(height02);

			if($(window).scrollTop() == 0) {
				$('#order_right').animate({top: $(window).scrollTop()}, {queue: false, duration: 900});
			} else if ($(window).scrollTop() == $(document).height() - $(window).height()) {
				$('#order_right').animate({top: $(window).scrollTop() - 460}, {queue: false, duration: 900});
			} else if ($(window).scrollTop() > height02 - $(window).height()) {
				$('#order_right').animate({top: $(window).scrollTop() - 460}, {queue: false, duration: 900});
			} else {
				$('#order_right').animate({top: $(window).scrollTop() - 180}, {queue: false, duration: 900});
			}


		});

		$(document).ready(function() {
			var posY;

			function bodyFreezeScroll() {
				posY = $(window).scrollTop();
				$("html").addClass('fix');
				$("html").css("top", -posY);
			}

			function bodyUnfreezeScroll() {
				$("html").removeAttr('class');
				$("html").removeAttr('style');
				posY = $(window).scrollTop(posY);
			}
			$('.btn_delivery').click(function() {
				$('.pop_add').fadeIn();
				bodyFreezeScroll();
				return false;
			});
			$('.popup .radio_table .btn_modify').click(function() {
				$('.pop_add').fadeOut();
				$('.pop_modify').fadeIn();

			});
			$('.btn_tax').click(function() {
				$('.pop_tax').fadeIn();
				bodyFreezeScroll();
				return false;
			});
			$('.pop_tax .radio_table .btn_modify').click(function() {
				$('.pop_tax').fadeOut();
				$('.pop_modify2').fadeIn();

			});

			$('.popup .btn_close').click(function() {
				$('.popup').fadeOut();
				bodyUnfreezeScroll();
				return false;

			});

			$('#memAddrChg').click(function() {
				var strArray = document.memAddr.mainYN.value.split("||");

				if (strArray[4] != "") {
					strPhoneArray = strArray[4].split("-")
				}
				if (strArray[5] != "") {
					strMobileArray = strArray[5].split("-")
				}

				document.orderForm.s_zip.value = strArray[0];
				document.orderForm.s_addr1.value = strArray[1];
				document.orderForm.s_addr2.value = strArray[2];
				document.orderForm.s_name.value = strArray[3];
				document.orderForm.s_phone1.value = strPhoneArray[0];
				document.orderForm.s_phone2.value = strPhoneArray[1];
				document.orderForm.s_phone3.value = strPhoneArray[2];
				document.orderForm.s_mobile1.value = strMobileArray[0];
				document.orderForm.s_mobile2.value = strMobileArray[1];
				document.orderForm.s_mobile3.value = strMobileArray[2];

				$('.popup').fadeOut();
				bodyUnfreezeScroll()
			});

			$('#memBillChg').click(function() {
				var strArray = document.memBill.bill_mainYN.value.split("||");

				if (strArray[3] != "") {
					strEmailArray = strArray[3].split("@")
				}

				document.orderForm.o_cNum.value = strArray[0];
				document.orderForm.o_cName.value = strArray[1];
				document.orderForm.o_ceo.value = strArray[2];
				document.orderForm.o_cemail1.value = strEmailArray[0];
				document.orderForm.o_cemail2.value = strEmailArray[1];

				$('.popup').fadeOut();
				bodyUnfreezeScroll()
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="order">
					<div class="inner">
						<div class="sub_tit">
							<h2>주문결제</h2>
						</div>
						<div class="progress_box">
							<ul>
								<li>1</li>
								<li class="on">2</li>
								<li>3</li>
							</ul>
						</div>
						<div class="section_tit">
							<h4>주문내역</h4>
						</div>
<%
	If Request.Cookies("SRM_LEVEL") = 2 Then
		query_where = query_where & " AND b.mem_idx = " & intSeq
	ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
		query_where = query_where & " AND b.mem_idx = " & masterIDX
	End If

	If Request.Cookies("SRM_LEVEL") = 9 Then
		sql = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_simg FROM GOODS A WHERE A.g_idx = '" & g_idx & "'"
	Else
		sql = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_simg, ISNULL(B.mem_price, 0) AS mem_price FROM GOODS A INNER JOIN goods_srm_join B ON A.g_idx = B.g_idx WHERE A.g_idx = '" & g_idx & "'" & query_where
	End If

	Set rs = Server.CreateObject("ADODB.RecordSet")

	rs.CursorType = 3
	rs.CursorLocation = 3
	rs.LockType = 3
	rs.Open sql, dbconn
%>
						<form name="orderForm" method="post">
						<input type="hidden" name="goods_list" value="<%=g_idx%>">
						<input type="hidden" name="cost" value="<%=cost%>">
						<div id="order_cons">
							<div class="table order_price" id="order_right">

								<table>
									<thead>
										<tr>
											<th colspan="2">최종 결제 금액</th>
										</tr>
									</thead>
									<tbody>
<%

	If Not (rs.eof) Then
		i = 1
		Do While Not rs.eof
			If Request.Cookies("SRM_LEVEL") <> 9 Then
				If rs("mem_price") = 0 Then
					finalPrice = rs("g_Money")
				Else
					finalPrice = rs("mem_price")
				End If
			Else
				finalPrice = rs("g_Money")
			End If

			TotalPrice = TotalPrice + (FinalPrice * g_cnt)

			g_idx = rs("g_idx")

			If i = rs.RecordCount Then
				comma = ""
			Else
				comma = ","
			End If

			If rs("g_idx") <> "" Then
				arr_g_idx = arr_g_idx & rs("g_idx") & comma
				arr_g_name = arr_g_name & rs("g_name") & comma
			End If

			i = i + 1
			rs.MoveNext
		Loop

		rs.MoveFirst
	End If
%>
										<input type="hidden" name="arr_g_idx" value="<%=arr_g_idx%>">
										<input type="hidden" name="arr_g_name" value="<%=arr_g_name%>">
										<input type="hidden" name="goods_cnt" value="<%=g_cnt%>">
										<input type="hidden" name="cnt" value="<%=g_cnt%>">
										<input type="hidden" name="goods_price" value="<%=TotalPrice%>">
										<tr>
											<th>구매금액</th>
											<td><%=FormatNumber(TotalPrice, 0)%>원</td>
										</tr>
										<tr>
											<th>배송비</th>
											<td>+ <%=FormatNumber(cost, 0)%>원</td>
										</tr>
										<tr class="total">
											<th>총 결제 금액</th>
											<td><%=FormatNumber(TotalPrice+cost, 0)%><span>원</span></td>
										</tr>
										<tr>
											<th colspan="2"><button class="btn_wh" type="button" onclick="history.back()">취소하기</button></th>
										</tr>
										<tr>
											<th colspan="2"><button class="btn_blue" onclick="order_chk('direct');return false;">결제하기</button></th>
										</tr>
									</tbody>
								</table>

							</div>

							<div id="order_left">
								<table class="table table_type03 order">
									<colgroup>
										<col style="width:600px">
										<col style="width:300px">
										<col style="width:300px">
									</colgroup>
									<thead>
										<tr>
											<th>상품정보</th>
											<th>상품금액</th>
											<th>수량</th>
										</tr>
									</thead>
									<tbody>
<%
	If Not (rs.eof) Then
		Do While Not rs.eof
%>
										<tr>
											<td>
												<div class="cart_div">
													<div class="img_box">
														<a href="#"><img src="/upload/goods/<%=rs("g_simg")%>"></a>
													</div>
													<div class="txt_box">
														<a href="#" class="pro_code">제품코드 : #<%=rs("g_idx")%></a>
														<p class="pro_name"><%=rs("g_name")%></p><br>
													</div>
												</div>
											</td>
											<td class="price"><%' If rs("mem_price") = 0 Then %><%=FormatNumber(GoodPrice,0)%><%' Else %><%'=FormatNumber(rs("mem_price"),0)%><%' End If %>원</td>
											<td><%=g_cnt%></td>
										</tr>
<%
			rs.MoveNext
		Loop
	End If

	rs.close
%>
									</tbody>
								</table>
								<input type="hidden" name="price" value=<%=TotalPrice+cost%>>
								<div class="order_write">
									<div class="section_tit">
										<h4>주문하시는 분</h4>
									</div>
									<table class="table table_type02">
										<colgroup>
											<col width="200px">
											<col width="*">
										</colgroup>
										<thead>
											<tr>
												<th colspan="2">주문자 정보</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>회사명 <span>*</span></th>
												<td><input type="text" name="o_company" value="<%=cName%>"></td>
											</tr>
											<tr>
												<th>주문자 <span>*</span></th>
												<td><input type="text" name="o_name" id="o_name" value=<%=strName%>></td>
											</tr>
											<tr>
												<th>주문부서 <span>*</span></th>
												<td><input type="text" name="o_department"></td>
											</tr>
											<tr class="phone">
												<th>휴대폰 번호 <span>*</span></th>
												<td>
													<input type="text" name="o_mobile1" id="o_mobile1" maxlength="4" value="<%=strMobile1%>" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);"> -
													<input type="text" name="o_mobile2" id="o_mobile2" maxlength="4" value="<%=strMobile2%>" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);"> -
													<input type="text" name="o_mobile3" id="o_mobile3" maxlength="4" value="<%=strMobile3%>" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);">
												</td>
											</tr>
											<tr class="mail">
												<th>이메일 주소 <span>*</span></th>
												<td>
													<input type="text" name="o_email1" id="o_email1" value="<%=strEmail1%>"> @
													<input type="text" name="o_email2" id="o_email2" value="<%=strEmail2%>">
													<label>
														<select name="o_email3" id="o_email3" onchange="ChangeEmail()">
															<option value="0">직접입력</option>
															<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
															<option value="empal.com">empal.com</option>
															<option value="gmail.com">gmail.com</option>
															<option value="lycos.co.kr">lycos.co.kr</option>
															<option value="naver.com">naver.com</option>
															<option value="nate.com">nate.com</option>
															<option value="netsgo.com">netsgo.com</option>
															<option value="hanmail.net">hanmail.net</option>
															<option value="hotmail.com">hotmail.com</option>
															<option value="paran.com">paran.com</option>
														</select>
													</label>
												</td>
											</tr>
										</tbody>
									</table>
									<div class="section_tit fl_left">
										<h4 class="">받는 분</h4>
										<div class="chk_box">
											<input type="checkbox" id="equal" >
											<label for="equal"><span></span>
												<p>주문자와 동일합니다</p>
											</label>
											<button class="btn_blue btn_delivery">배송지 선택</button>
										</div>
									</div>
									<table class="table table_type02">
										<colgroup>
											<col width="200px">
											<col width="*">
										</colgroup>
										<thead>
											<tr>
												<th colspan="2">배송지 정보</th>
											</tr>
										</thead>
										<tbody>

											<tr>
												<th>수령인 <span>*</span></th>
												<td><input type="text" name="s_name" id="s_name"></td>
											</tr>
											<tr class="phone">
												<th>연락처 <span>*</span></th>
												<td>
													<input type="text" name="s_phone1" maxlength="4" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);"> &nbsp;-&nbsp;
													<input type="text" name="s_phone2" maxlength="4" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);">&nbsp; -&nbsp;
													<input type="text" name="s_phone3" maxlength="4" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);">

												</td>
											</tr>
											<tr class="phone">
												<th>휴대폰 번호 <span>*</span></th>
												<td>
													<input type="text" name="s_mobile1" id="s_mobile1" maxlength="4" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);"> &nbsp;-&nbsp;
													<input type="text" name="s_mobile2" id="s_mobile2" maxlength="4" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);"> &nbsp;-&nbsp;
													<input type="text" name="s_mobile3" id="s_mobile3" maxlength="4" onkeypress="filterNumber(event);" onkeydown="fn_press_han(this);">

												</td>
											</tr>
											<tr class="mail">
												<th>이메일 주소 <span>*</span></th>
												<td>
													<input type="text" name="s_email1" id="s_email1"> @
													<input type="text" name="s_email2" id="s_email2">

													<label>
														<select name="s_email3" id="s_email3" onchange="ChangeEmail2()">
															<option value="0">직접입력</option>
															<option value="Dreamwiz.co.kr">Dreamwiz.co.kr</option>
															<option value="empal.com">empal.com</option>
															<option value="gmail.com">gmail.com</option>
															<option value="lycos.co.kr">lycos.co.kr</option>
															<option value="naver.com">naver.com</option>
															<option value="nate.com">nate.com</option>
															<option value="netsgo.com">netsgo.com</option>
															<option value="hanmail.net">hanmail.net</option>
															<option value="hotmail.com">hotmail.com</option>
															<option value="paran.com">paran.com</option>
														</select>
													</label>
												</td>
											</tr>
											<tr class="add">
												<th>주소<span>*</span></th>
												<td><input type="text" class="wd360" name="s_zip" id="s_zip" value="<%=cZip%>" readonly> <button class="zip_code btn_bk" onclick="openDaumPostcode('order');return false;">우편번호 검색</button> <input type="text" name="s_addr1" id="s_addr1" value="<%=cAddr1%>" readonly><input type="text" name="s_addr2" value="<%=cAddr2%>" id="s_addr2"></td>
											</tr>
											<tr>
												<th>배송요청사항</th>
												<td><input type="text" name="s_request"></td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- 카드결제시-->
								<div class="pay_box">
									<div class="section_tit">
										<h4>결제정보</h4>
									</div>
									<table class="table table_type02 table_pay">
										<colgroup>
											<col width="200px">
											<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th>결제방법 선택 <span>*</span></th>
												<td>
													<div class="chk_box radio_box">
														<div class="radio">
															<input id="radio_order01" type="radio" name="payment" value="card" checked>
															<label for="radio_order01"><span></span>
																<p>카드결제</p>
															</label>
														</div>
														<div class="radio">
															<input id="radio_order02" type="radio" name="payment" value="account">
															<label for="radio_order02"><span></span>
																<p>무통장 입금</p>
															</label>
														</div>
														<% If yeosinYN = "Y" Then %>
														<div class="radio">
															<input id="radio_order03" type="radio" name="payment" value="yeosin">
															<label for="radio_order03"><span></span>
																<p>여신결제</p>
															</label>
														</div>
														<% End If %>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
									<table class="table table_type02 table_pay table_pay2" id="bankinfo" style="display:none;">
										<colgroup>
											<col width="200px">
											<col width="*">
										</colgroup>

										<tbody>
										<tr>
											<th>입금자명 <span>*</span></th>
											<td><input type="text" name="d_name"></td>
										</tr>

										<tr class="pay_bank">
											<th>결제계좌정보 <span>*</span></th>
											<td>
												<select name="d_bank">
													<option value="">은행선택</option>
													<option value="기업은행 137-069560-01-010 (주식회사 신성씨앤에스)">기업은행 137-069560-01-010 (주식회사 신성씨앤에스)</option>
												</select>
											</td>
										</tr>
										<tr class="document">
											<th>증빙서류</th>
											<td>
												<button class="btn_blue btn_tax">세금계산서발행정보 선택</button>
												<table class="inner_table">
													<tr>
														<th>사업자등록번호</th>
														<td>
															<input type="text" name="o_cNum">
														</td>
													</tr>
													<tr>
														<th> 상호명 </th>
														<td>
															<input type="text" name="o_cName">
														</td>
													</tr>
													<tr>
														<th> 대표자 </th>
														<td>
															<input type="text" name="o_ceo">
														</td>
													</tr>

													<tr class="mail">
														<th> 이메일</th>
														<td>
															<input type="text" name="o_cemail1">&nbsp; @&nbsp;
															<input type="text" name="o_cemail2">

															<label>
																<select name="o_cemail3" id="o_cemail3" onchange="ChangeEmail3()">
																	<option value="0">직접입력</option>
																	<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
																	<option value="empal.com">empal.com</option>
																	<option value="gmail.com">gmail.com</option>
																	<option value="lycos.co.kr">lycos.co.kr</option>
																	<option value="naver.com">naver.com</option>
																	<option value="nate.com">nate.com</option>
																	<option value="netsgo.com">netsgo.com</option>
																	<option value="hanmail.net">hanmail.net</option>
																	<option value="hotmail.com">hotmail.com</option>
																	<option value="paran.com">paran.com</option>
																</select>
															</label>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr class="chk">
											<th colspan="2">
												<div class="chk_box radio_box">
													<input type="checkbox" name="taxConfirm" id="fliter01" value="Y">
													<label for="fliter01"><span></span>
														<p>세금계산서 정보를 확인했습니다.</p>
													</label>
												</div>
											</th>
										</tr>
										</tbody>
									</table>
								</div>
								</form>

								<!--무통장입금/여신결제선택시-->
							<!--div class="pay_box">
								<div class="section_tit">
									<h4>결제정보</h4>
								</div>
								<table class="table table_type02">
									<colgroup>
										<col width="200px">
										<col width="*">
									</colgroup>

									<tbody>
										<tr>
											<th>입금자명</th>
											<td><input type="text"></td>
										</tr>

										<tr class="pay_bank">
											<th>결제계좌정보 <span>*</span></th>
											<td>
												<select required>
													<option>은행선택</option>
												</select>
											</td>
										</tr>
										<tr class="document">
											<th>증빙서류</th>
											<td>
												<button class="btn_blue btn_tax">세금계산서발행정보 선택</button>
												<table class="inner_table">
													<tr>
														<th>사업자등록번호</th>
														<td>
															<input type="text">
														</td>
													</tr>
													<tr>
														<th> 상호명 </th>
														<td>
															<input type="text">
														</td>
													</tr>
													<tr>
														<th> 대표자 </th>
														<td>
															<input type="text">
														</td>
													</tr>

													<tr class="mail">
														<th> 이메일</th>
														<td>
															<input type="text">&nbsp; @&nbsp;
															<input type="text">

															<label>
																<select>
																	<option>직접입력</option>
																	<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
																	<option value="empal.com">empal.com</option>
																	<option value="gmail.com">gmail.com</option>
																	<option value="lycos.co.kr">lycos.co.kr</option>
																	<option value="naver.com">naver.com</option>
																	<option value="nate.com">nate.com</option>
																	<option value="netsgo.com">netsgo.com</option>
																	<option value="hanmail.net">hanmail.net</option>
																	<option value="hotmail.com">hotmail.com</option>
																	<option value="paran.com">paran.com</option>
																</select>
															</label>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr class="chk">
											<th colspan="2">
												<div class="chk_box radio_box">
													<input id="fliter01" type="checkbox">
													<label for="fliter01"><span></span>
														<p>세금계산서 정보를 확인했습니다.</p>
													</label>
												</div>
											</th>
										</tr>

									</tbody>
								</table>

							</div-->
	</div>

	</div>
					</div>
				</div>
<%
sql = "SELECT * FROM member_addr WHERE mem_idx = '" & intSeq & "'"
Set rs = dbconn.execute(sql)
%>
				<div class="order_poparea">
					<div class="popup pop_add">
						<div class="pop_wrap">
							<div class="tit">
								<h3>배송지 선택</h3>
								<button class="btn_close">닫기</button>
							</div>
							<div class="pop_contents">
								<form name="memAddr">
								<table class="radio_table">
									<colgroup>
										<col width="8%">
										<col width="20%">
										<col width="25%">
										<col width="20%">
										<col width="20%">
										<!--col width="8%"-->
									</colgroup>
									<thead>
										<tr>
											<th></th>
											<th>배송지 정보</th>
											<th>주소</th>
											<th>수령인</th>
											<th>전화번호</th>
											<!--th>수정</th-->
										</tr>
									</thead>
									<tbody>
<%
If Not (rs.bof Or rs.eof) Then
	Do While Not rs.eof
%>
										<tr>
											<td>
												<div class="chk_box radio_box">
													<div class="radio">
														<input type="radio" id="mainYN<%=rs("addr_idx")%>" name="mainYN" <% If rs("mainYN") = "Y" Then %> checked<% End If %> value="<%=rs("mem_zip")%>||<%=rs("mem_addr1")%>||<%=rs("mem_addr2")%>||<%=rs("mem_name")%>||<%=rs("mem_phone")%>||<%=rs("mem_mobile")%>">
														<label for="mainYN<%=rs("addr_idx")%>"><span></span>
														</label>
													</div>
												</div>
											</td>
											<td><% If rs("mainYN") = "Y" Then %>[대표]<% End If %></td><!-- class="add_name" -->
											<td><%=rs("mem_addr1")%>&nbsp;<%=rs("mem_addr2")%></td>
											<td><%=rs("mem_name")%></td>
											<td><% If rs("mem_phone") <> "" Then %><%=rs("mem_phone")%> <br><% End If %>
												<%=rs("mem_mobile")%></td>
											<!--td><button class="btn_wh btn_modify">수정</button></td-->
											<input type="hidden" name="o_zip" value="<%=rs("mem_zip")%>">
											<input type="hidden" name="o_addr1" value="<%=rs("mem_addr1")%>">
											<input type="hidden" name="o_addr2" value="<%=rs("mem_addr2")%>">
										</tr>
<%
		rs.MoveNext
	Loop
End If

rs.close
%>
									</tbody>

								</table>
								</form>
								<div class="btn_box">
									<button class="btn_wh btn_close">취소하기</button>
									<button class="btn_blue" id="memAddrChg">선택하기</button>
								</div>
							</div>
						</div>
					</div>


					<!--div class="popup pop_modify">
						<div class="pop_wrap">
							<div class="tit">
								<h3>배송지 선택</h3>
								<button class="btn_close">닫기</button>
							</div>
							<div class="pop_contents">
								<table class="modify_table">

									<tbody>
										<tr>
											<th>대표 배송지</th>
											<td>
												<div class="chk_box radio_box">
													<input id="pop_chk01" type="checkbox">
													<label for="pop_chk01"><span></span>
														<p>대표 배송지로 설정</p>
													</label>
												</div>
											</td>
										</tr>
										<tr class="add">
											<th>주소 (도로명 주소)<span>*</span></th>
											<td><input type="text" class="wd360" required> <button class="zip_code btn_bk">우편번호 검색</button> <input type="text" required><input type="text" required></td>
										</tr>

										<tr>
											<th>수령인 <span>*</span></th>
											<td><input type="text" required></td>
										</tr>
										<tr class="phone">
											<th>전화번호 <span>*</span></th>
											<td>
												<input type="text" maxlength="4" required> &nbsp;-&nbsp;
												<input type="text" maxlength="4" required>&nbsp; -&nbsp;
												<input type="text" maxlength="4" required>

											</td>
										</tr>
										<tr class="phone">
											<th>휴대폰 번호 <span>*</span></th>
											<td>
												<input type="text" maxlength="4" required> &nbsp;-&nbsp;
												<input type="text" maxlength="4" required> &nbsp;-&nbsp;
												<input type="text" maxlength="4" required>
											</td>
										</tr>
										<tr class="mail">
											<th>이메일 주소 <span>*</span></th>
											<td>
												<input type="text" required=""> @
												<input type="text" required="">

												<label>
													<select required>
														<option>직접입력</option>
														<option value="Dreamwiz.co.kr">Dreamwiz.co.kr</option>
														<option value="empal.com">empal.com</option>
														<option value="gmail.com">gmail.com</option>
														<option value="lycos.co.kr">lycos.co.kr</option>
														<option value="naver.com">naver.com</option>
														<option value="nate.com">nate.com</option>
														<option value="netsgo.com">netsgo.com</option>
														<option value="hanmail.net">hanmail.net</option>
														<option value="hotmail.com">hotmail.com</option>
														<option value="paran.com">paran.com</option>
													</select>
												</label>
											</td>
										</tr>
									</tbody>
								</table>

								<div class="btn_box">
									<button class="btn_wh btn_close">취소하기</button>
									<button class="btn_blue">수정</button>
								</div>
							</div>
						</div>
					</div-->

<%
sql = "SELECT * FROM member_taxbill WHERE mem_idx = '" & intSeq & "'"
Set rs = dbconn.execute(sql)
%>
					<div class="popup pop_tax">
						<div class="pop_wrap">
							<div class="tit">
								<h3>세금계산서발행정보 선택</h3>
								<button class="btn_close">닫기</button>
							</div>
							<div class="pop_contents">
								<form name="memBill">
								<table class="radio_table">
									<colgroup>
										<col width="8%">
										<col width="20%">
										<col width="15%">
										<col width="15%">
										<col width="20%">
										<!--col width="10%"-->
									</colgroup>
									<thead>
										<tr>
											<th></th>
											<th>사업자등록번호</th>
											<th>상호</th>
											<th>대표자명</th>
											<th>이메일</th>
											<!--th>수정</th-->
										</tr>
									</thead>
									<tbody>
<%
If Not (rs.bof Or rs.eof) Then
	Do While Not rs.eof
%>
										<tr>
											<td>
												<div class="chk_box radio_box">
													<div class="radio">
														<input id="bill<%=rs("bill_idx")%>" type="radio" name="bill_mainYN" value="<%=rs("cNum")%>||<%=rs("cName")%>||<%=rs("ceo")%>||<%=rs("cEmail")%>" <% If rs("mainYN") = "Y" Then %> checked<% End If %>>
														<label for="bill<%=rs("bill_idx")%>"><span></span>
														</label>
													</div>
												</div>
											</td>
											<td><%=rs("cNum")%><% If rs("mainYN") = "Y" Then %>[대표]<% End If %></td>
											<td><%=rs("cName")%></td>
											<td><%=rs("ceo")%></td>
											<td><%=rs("cEmail")%></td>
											<!--td><button class="btn_wh btn_modify">수정</button></td-->
										</tr>
<%
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing
%>
									</tbody>

								</table>
								</form>
								<div class="btn_box">
									<button class="btn_wh btn_close">취소하기</button>
									<button class="btn_blue" id="memBillChg">선택하기</button>
								</div>
							</div>
						</div>
					</div>



					<!--div class="popup pop_modify2">
						<div class="pop_wrap">
							<div class="tit">
								<h3>세금계산서발행정보 선택</h3>
								<button class="btn_close">닫기</button>
							</div>
							<div class="pop_contents">
								<table class="modify_table">

									<tbody>
										<tr>
											<th>대표 세금계산서발행정보</th>
											<td>
												<div class="chk_box radio_box">
													<input id="pop_chk02" type="checkbox">
													<label for="pop_chk02"><span></span>
														<p>대표 세금계산서발행정보로 설정
														</p>
													</label>
												</div>
											</td>
										</tr>
										<tr class="phone">
											<th>사업자등록번호 <span>*</span></th>
											<td>
												<input type="text" maxlength="4" required> &nbsp;-&nbsp;
												<input type="text" maxlength="4" required> &nbsp;-&nbsp;
												<input type="text" maxlength="4" required>
											</td>
										</tr>
										<tr>
											<th>상호 (법인명) <span>*</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th>대표자 <span>*</span></th>
											<td><input type="text"></td>
										</tr>
										<tr class="add">
											<th>사업장 주소<br>(도로명 주소)<span>*</span></th>
											<td><input type="text" class="wd360" required> <button class="zip_code btn_bk">우편번호 검색</button> <input type="text" required><input type="text" required></td>
										</tr>
										<tr>
											<th>업태 <span>*</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th>종목 <span>*</span></th>
											<td><input type="text" required></td>
										</tr>


										<tr class="phone">
											<th>전화번호 <span>*</span></th>
											<td>
												<input type="text" maxlength="4" required> &nbsp;-&nbsp;
												<input type="text" maxlength="4" required>&nbsp; -&nbsp;
												<input type="text" maxlength="4" required>

											</td>
										</tr>
										<tr class="phone">
											<th>휴대폰 번호 <span>*</span></th>
											<td>
												<input type="text" maxlength="4" required> &nbsp;-&nbsp;
												<input type="text" maxlength="4" required> &nbsp;-&nbsp;
												<input type="text" maxlength="4" required>
											</td>
										</tr>
										<tr class="mail">
											<th>이메일 주소<span>*</span></th>
											<td> <input type="text">&nbsp; @&nbsp;
												<input type="text">
												<label>
													<select required>
														<option>직접입력</option>
														<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
														<option value="empal.com">empal.com</option>
														<option value="gmail.com">gmail.com</option>
														<option value="lycos.co.kr">lycos.co.kr</option>
														<option value="naver.com">naver.com</option>
														<option value="nate.com">nate.com</option>
														<option value="netsgo.com">netsgo.com</option>
														<option value="hanmail.net">hanmail.net</option>
														<option value="hotmail.com">hotmail.com</option>
														<option value="paran.com">paran.com</option>
													</select>
												</label></td>
										</tr>
									</tbody>
								</table>

								<div class="btn_box">
									<button class="btn_wh btn_close">취소하기</button>
									<button class="btn_blue">수정</button>
								</div>
							</div>
						</div>
					</div-->
				</div>

			</div>

			<script>

			</script>

			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->