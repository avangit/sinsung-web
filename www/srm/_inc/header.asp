<header>

	<div class="inner">
		<h1 class="logo">
			<a href="/srm/"><img src="/srm/images/common/logo.png" alt="신성 srm"></a>
		</h1>
		<ul class="user">
			<li>
				<span><%=Request.Cookies("SRM_COMPANY")&" "&Request.Cookies("SRM_NAME")%>
				<% if Request.Cookies("SRM_LEVEL") = 2 then %>
					마스터회원님
				<% elseif Request.Cookies("SRM_LEVEL") = 3 then %>
					고객님
				<% end if %>
				</span>

			</li>
			<li><a href="/srm/member/user.asp"><i></i></a></li>
			<li><a href="/srm/order/cart.asp"><i><% If cartCNT > 0 Then %><span><%=cartCNT%></span><% End If %></i></a></li>
			<li><button class="login_box" onclick="location.href='/srm/logout.asp'">로그아웃</button></li>
		</ul>
	</div>
</header>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
<div class="gnb">
	<div class="inner">
		<div class="total_menu"> <a href="#"></a></div>
		<div class="b2b_prd_menu">
			<div class="b2b_inner">
				<ul class="depth01">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
					<li>
						<p class="depth01_tit"><%=rs("c_name")%></p>
						<ul class="depth02">
<%
			sql2 = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 6) = '000000' AND c_code LIKE '" & Left(rs("c_code"), 2) & "%' AND SUBSTRING(c_code, 4, 1) <> '0' ORDER BY c_sunse"
			Set rs2 = dbconn.execute(sql2)

			If Not(rs2.bof Or rs2.eof) Then
				Do While Not rs2.eof

%>
							<li>
								<a href="javascript:;"><%=rs2("c_name")%></a>
								<ul class="depth03">
<%
					sql3 = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 4) = '0000' AND c_code LIKE '" & Left(rs2("c_code"), 4) & "%' AND SUBSTRING(c_code, 6, 1) <> '0' ORDER BY c_sunse"
					Set rs3 = dbconn.execute(sql3)

					If Not(rs3.bof Or rs3.eof) Then
						Do While Not rs3.eof
%>
									<li><a href="/srm/b2b/product.asp?c=<%=rs3("c_code")%>"><%=rs3("c_name")%></a></li>
<%
							rs3.MoveNext
						Loop
					End If

					rs3.Close
					Set rs3 = Nothing
%>
								</ul>
							</li>
<%
					rs2.MoveNext
				Loop
			End If

			rs2.Close
			Set rs2 = Nothing
%>
						</ul>
					</li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
				</ul>
				<div class="brand">
					<ul>
						<li><img src="/srm/images/common/brand_logo01.png" alt="NAVER" /></li>
						<li><img src="/srm/images/common/brand_logo02.png" alt="Daum" /></li>
						<li><img src="/srm/images/common/brand_logo02.png" alt="kakao" /></li>
						<li><img src="/srm/images/common/brand_logo04.png" alt="Melon" /></li>
						<li><img src="/srm/images/common/brand_logo05.png" alt="genie" /></li>
						<li><img src="/srm/images/common/brand_logo01.png" alt="NAVER" /></li>
						<li><img src="/srm/images/common/brand_logo02.png" alt="Daum" /></li>
						<li><img src="/srm/images/common/brand_logo02.png" alt="kakao" /></li>
						<li><img src="/srm/images/common/brand_logo04.png" alt="Melon" /></li>
						<li><img src="/srm/images/common/brand_logo05.png" alt="genie" /></li>
						<li><img src="/srm/images/common/brand_logo01.png" alt="NAVER" /></li>
						<li><img src="/srm/images/common/brand_logo02.png" alt="Daum" /></li>
						<li><img src="/srm/images/common/brand_logo02.png" alt="kakao" /></li>
						<li><img src="/srm/images/common/brand_logo04.png" alt="Melon" /></li>
						<li><img src="/srm/images/common/brand_logo05.png" alt="genie" /></li>
					</ul>
				</div>
			</div>
		</div>

		<ul class="nav">
			<li><a href="/srm/b2b/product.asp"><i></i>B2B 제품</a></li>
		<%' If Request.Cookies("SRM_LEVEL") = 9 Then %>
			<li><a href="/srm/order/order_list.asp"><i><!--span>N</span--></i>구매전자결재</a></li>
		<%' End If %>
			<li><a href="/srm/order/order_list2.asp"><i></i>주문배송조회</a></li>
			<li><a href="/srm/online/estimate_list.asp"><i></i>견적요청</a></li>
			<li><a href="/srm/online/ask_list.asp"><i></i>문의하기</a></li>
		<%' If Request.Cookies("SRM_LEVEL") = 2 Or Request.Cookies("SRM_LEVEL") = 9 Then %>
			<li><a href="/srm/serial/serial.asp"><i></i>시리얼 관리</a></li>
		<%' End If %>
			<li><a href="/srm/board/it.asp"><i></i>IT 자료실</a></li>
			<li><a href="/srm/board/notice.asp"><i></i>공지사항</a></li>
		</ul>
	</div>
</div>
