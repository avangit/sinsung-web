<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
If Request.Cookies("SRM_ID") <> "" Then
	Call jsAlertUrl("/srm/srm_main.asp")
End If
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script type="text/javascript" src="/srm/js/member.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$('#loginForm input').on("keypress", function (event) {
				if (event.keyCode == 13) {
					LoginChk();return false;
					$("#loginForm").submit();
				}
			});
		});
	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Container" class="main login_wrap">
			<div class="inner">
				<div class="logo_box">
					<h1><a href="./login.asp"><img src="/srm/images/common/login_logo.png" alt="신성 srm"></a></h1>
				</div>

				<div class="login_box">
					<div class="tit_box">
						<h3>로그인</h3>
						<p>신성씨앤에스 계정으로 로그인하기</p>
					</div>
					<form name="loginForm" id="loginForm" method="post" action="./login_ok.asp">
						<input type="text" name="srm_id" placeholder="아이디"/>
						<input type="password" name="srm_pw" placeholder="비밀번호"/>
						<p id="id_ck"></p>
						<button type="button" class="btn_blue" onclick="LoginChk();">로그인</button>
					</form>
					<!--ul class="find_links">
						<li><a href="javascript:;">아이디 찾기</a></li>
						<li><a href="javascript:;">비밀번호 찾기</a></li>
					</ul-->
				</div>

			</div>
		</div>

	</div>

</body>

</html>
