function ChangeEmail() {
	if (onlineform.on_email3.value == '0') {
		onlineform.on_email2.readOnly = false;
		onlineform.on_email2.value = '';
		onlineform.on_email2.focus();
	} else {
		onlineform.on_email2.readOnly = true;
		onlineform.on_email2.value = onlineform.on_email3.value;
	}
}

function ask_chk() {
	var frm=document.onlineform;

	if(frm.on_company.value == "") {
		alert("회사명을 입력해 주세요.");
		frm.on_company.focus();
		return false;
	} else if(frm.on_name.value == "") {
		alert("담당자를 입력해 주세요.");
		frm.on_name.focus();
		return false;
	} else if(frm.on_mobile1.value == "" || frm.on_mobile2.value == "" || frm.on_mobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(isNaN(frm.on_mobile1.value) || isNaN(frm.on_mobile2.value) || isNaN(frm.on_mobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(frm.on_email1.value == "" || frm.on_email2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.on_email1.focus();
		return false;
	} else if(frm.on_title.value == "") {
		alert("제목을 입력해 주세요.");
		frm.on_title.focus();
		return false;
	} else if(frm.on_content.value == "") {
		alert("내용을 입력해 주세요.");
		frm.on_content.focus();
		return false;
	} else {
		frm.submit();
	}
}

function ask_chk_m() {
	var frm=document.onlineform;

	if(frm.on_company.value == "") {
		alert("회사명을 입력해 주세요.");
		frm.on_company.focus();
		return false;
	} else if(frm.on_name.value == "") {
		alert("담당자를 입력해 주세요.");
		frm.on_name.focus();
		return false;
	} else if(frm.on_mobile1.value == "" || frm.on_mobile2.value == "" || frm.on_mobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(isNaN(frm.on_mobile1.value) || isNaN(frm.on_mobile2.value) || isNaN(frm.on_mobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(frm.on_email.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.on_email.focus();
		return false;
	} else if(frm.on_title.value == "") {
		alert("제목을 입력해 주세요.");
		frm.on_title.focus();
		return false;
	} else if(frm.on_content.value == "") {
		alert("내용을 입력해 주세요.");
		frm.on_content.focus();
		return false;
	} else {
		frm.submit();
	}
}

function goBack() {
  window.history.back();
}