

$(document).ready(function () {
	$('.total_menu').click(function () {
		$(this).siblings(".b2b_prd_menu").slideToggle('fast');
	});
});



$(document).ready(function () {
	$(".b2b_prd_menu .depth01 > li").bind("mouseenter focusin", function () {
		$(".b2b_prd_menu .depth01 > li").removeClass("on");
		$(".b2b_prd_menu .depth02").fadeOut("fast");
		$(this).find(".depth02").stop().fadeIn("fast");
		$(this).addClass("on");
	}).bind("mouseleave focusout", function () {
		$(this).find(".depth02").stop().fadeOut("fast");
		$(this).removeClass("on");
	});

	$(".b2b_prd_menu .depth02 > li").bind("mouseenter focusin", function () {
		$(".b2b_prd_menu .depth02 > li").removeClass("on");
		$(".b2b_prd_menu .depth03").fadeOut("fast");
		$(this).find(".depth03").stop().fadeIn("fast");
		$(this).addClass("on");
	}).bind("mouseleave focusout", function () {
		$(this).removeClass("on");
		$(this).find(".depth03").stop().fadeOut("fast");
	});




	// 스크롤 시 헤더
	window.onscroll = function () {
		if ($(document).scrollTop() > 1) {
			$("#A_Header").addClass("on");
			$("#A_Header").mouseleave(function () {
				$(this).addClass('on');
			});
		} else {
			$("#A_Header").removeClass("on");
			$("#A_Header").mouseleave(function () {
				$(this).removeClass('on');
			});
		}
	}

});
