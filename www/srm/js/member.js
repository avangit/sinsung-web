function LoginChk(){
	var frm = document.loginForm;
	var id=frm.srm_id.value;
	var pwd=frm.srm_pw.value;

	if(!frm.srm_id.value){
		alert('아이디를 입력해주세요');
		frm.srm_id.focus();
		return false;
	}
	if(!frm.srm_pw.value){
		alert('패스워드를 입력해주세요');
		frm.srm_pw.focus();
		return false;
	} else {
		frm.submit();
	}
}

function ChangeEmail() {
	if (editform.strEmail3.value == '0') {
		editform.strEmail2.readOnly = false;
		editform.strEmail2.value = '';
		editform.strEmail2.focus();
	} else {
		editform.strEmail2.readOnly = true;
		editform.strEmail2.value = editform.strEmail3.value;
	}
}

function ChangeEmail1() {
	if (billform.cEmail3.value == '0') {
		billform.cEmail2.readOnly = false;
		billform.cEmail2.value = '';
		billform.cEmail2.focus();
	} else {
		billform.cEmail2.readOnly = true;
		billform.cEmail2.value = billform.cEmail3.value;
	}
}

function ChangeEmail2() {
	if (subjoinform.subemail3.value == '0') {
		subjoinform.subemail2.readOnly = false;
		subjoinform.subemail2.value = '';
		subjoinform.subemail2.focus();
	} else {
		subjoinform.subemail2.readOnly = true;
		subjoinform.subemail2.value = subjoinform.subemail3.value;
	}
}

function ChangeEmail3() {
	if (onlineform.on_email3.value == '0') {
		onlineform.on_email2.readOnly = false;
		onlineform.on_email2.value = '';
		onlineform.on_email2.focus();
	} else {
		onlineform.on_email2.readOnly = true;
		onlineform.on_email2.value = onlineform.on_email3.value;
	}
}

function update_chk() {
	var frm=document.editform;

	if(frm.newPwd.value != "") {
		if(frm.newPwd.value.length < 8 || frm.newPwd.value.length > 20) {
			alert("비밀번호는 8~20자 이내입니다.");
			frm.newPwd.focus();
			return false;
		} else if(frm.newPwdRe.value == "") {
			alert("비밀번호 확인을 입력해 주세요.");
			frm.newPwdRe.focus();
			return false;
		} else if(frm.newPwdRe.value.length < 8 || frm.newPwdRe.value.length > 20) {
			alert("비밀번호는 8~20자 이내입니다.");
			frm.newPwdRe.focus();
			return false;
		} else if(frm.newPwd.value != frm.newPwdRe.value) {
			alert("비밀번호가 일치하지 않습니다.");
			frm.newPwdRe.focus();
			return false;
		}
	}

	if(frm.strPwd.value == "") {
		alert("비밀번호를 입력해 주세요.");
		frm.strPwd.focus();
		return false;
	} else if(frm.strMobile1.value == "" || frm.strMobile2.value == "" || frm.strMobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.strMobile1.focus();
		return false;
	} else if(isNaN(frm.strMobile1.value) || isNaN(frm.strMobile2.value) || isNaN(frm.strMobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.strMobile1.focus();
		return false;
	} else if(frm.strEmail1.value == "" || frm.strEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.strEmail1.focus();
		return false;
	} else {
		frm.submit();
	}
}

function tax_chk() {
	var frm=document.billform;

	if(frm.cNum1.value == "" || frm.cNum2.value == "" || frm.cNum3.value == "") {
		alert("사업자등록번호를 입력해 주세요.");
		frm.cNum1.focus();
		return false;
	} else if(isNaN(frm.cNum1.value) || isNaN(frm.cNum2.value) || isNaN(frm.cNum3.value)) {
		alert("사업자등록번호를 올바르게 입력해 주세요.");
		frm.cNum1.focus();
		return false;
	} else if(frm.cName.value == "") {
		alert("법인명을 입력해 주세요.");
		frm.cName.focus();
		return false;
	} else if(frm.ceo.value == "") {
		alert("대표자를 입력해 주세요.");
		frm.ceo.focus();
		return false;
	} else if(frm.cZip.value == "" || frm.cAddr1.value == "" || frm.cAddr2.value == "") {
		alert("사업장 주소를 입력해 주세요.");
		return false;
	} else if(frm.cType.value == "") {
		alert("업태를 입력해 주세요.");
		frm.cType.focus();
		return false;
	} else if(frm.cCate.value == "") {
		alert("종목을 입력해 주세요.");
		frm.cCate.focus();
		return false;
	} else if(frm.cPhone1.value == "" || frm.cPhone2.value == "" || frm.cPhone3.value == "") {
		alert("전화번호를 입력해 주세요.");
		frm.cPhone1.focus();
		return false;
	} else if(isNaN(frm.cPhone1.value) || isNaN(frm.cPhone2.value) || isNaN(frm.cPhone3.value)) {
		alert("전화번호를 올바르게 입력해 주세요.");
		frm.cPhone1.focus();
		return false;
	} else if(frm.cMobile1.value == "" || frm.cMobile2.value == "" || frm.cMobile3.value == "") {
		alert("휴대폰 번호를 입력해 주세요.");
		frm.cMobile1.focus();
		return false;
	} else if(isNaN(frm.cMobile1.value) || isNaN(frm.cMobile2.value) || isNaN(frm.cMobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.cMobile1.focus();
		return false;
	} else if(frm.cEmail1.value == "" || frm.cEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.cEmail1.focus();
		return false;
	} else {
		frm.submit();
	}
}

function addr_chk() {
	var frm=document.billform;

	if(frm.mem_Zip.value == "" || frm.mem_Addr1.value == "") {
		alert("주소를 입력해 주세요.");
		return false;
	} else if(frm.mem_name.value == "") {
		alert("수령인을 입력해 주세요.");
		frm.mem_name.focus();
		return false;
	} else if(frm.mem_Mobile1.value == "" || frm.mem_Mobile2.value == "" || frm.mem_Mobile3.value == "") {
		alert("휴대폰 번호를 입력해 주세요.");
		frm.mem_Mobile1.focus();
		return false;
	} else if(isNaN(frm.mem_Mobile1.value) || isNaN(frm.mem_Mobile2.value) || isNaN(frm.mem_Mobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.mem_Mobile1.focus();
		return false;
	} else if(frm.cEmail1.value == "" || frm.cEmail2.value == "") {
		alert("이메일 주소를 입력해 주세요.");
		frm.cEmail1.focus();
		return false;
	} else {
		frm.submit();
	}
}

// 체크박스 전체선택
var check = 1;
function checkAll(frm) {
	if (check) {
		for (i = 0; i < frm.length; i++) {
			if (frm[i].type == "checkbox") {
				if (frm[i].checked)
					continue;
				else
					frm[i].checked = true;
			}
		}
		check = 0;
	} else {
		for (i = 0; i < frm.length; i++) {
			if (frm[i].type == "checkbox") {
				if(frm[i].checked) frm[i].checked = false;
				else continue;
			}
		}
		check = 1;
	}
	return false;
}

// 체크박스 값 넘기기
function GetCheckbox(frm, mod, message) {
	var tmp = "";
	var cnt = 0;
	var i;
	var idk = false;

	for (i = 0; i < frm.length; i++) {
		if (frm[i].type != "checkbox")
			continue;
		if (frm[i].checked) {
			tmp += frm[i].value + " ";
			cnt = cnt + 1;
			idk = true;
		}
	}

	if (idk == true) {
		if (mod.indexOf("del") != -1) {
			var cfm = confirm("선택하신 내용을 삭제하시겠습니까?");
		} else if (mod == "sec") {
			var cfm = confirm("선택하신 회원을 탈퇴시키겠습니까?");
		} else if (mod == "copy") {
			if(cnt > 1) {
				alert('복사할 대상은 하나만 선택해 주세요.');
			} else {
				var cfm = confirm("선택하신 내용을 복사하시겠습니까?");
			}
		} else if (mod.substr(0,4) == "dsp-") {
			var cfm = confirm("선택하신 내용의 상태를 변경하시겠습니까?");
		} else {
			if (message != null) {
				var cfm = confirm("선택하신 항목을 '" + message + "' 하시겠습니까?");
			} else {
				var cfm = confirm("선택하신 항목의 상태를 변경하시겠습니까?");
			}
		}

		if (cfm) {
			frm.n.value = tmp.substr(0, tmp.length - 1);
			frm.m.value = mod;
			frm.message.value = message;
			frm.submit();
		} else {
			return;
		}
	} else {
		window.alert('실행할 데이터를 선택하세요');
		return;
	}
	return false;
}

function getXMLHttpRequest() {
	if (window.ActiveXObject) {
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e1) { return null; }
		}
	} else if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else {
		return null;
	}
}
var httpRequest = null;

function sendRequest(url, params, callback, method) {
	httpRequest = getXMLHttpRequest();
	var httpMethod = method ? method : 'GET';
	if (httpMethod != 'GET' && httpMethod != 'POST') {
		httpMethod = 'GET';
	}
	var httpParams = (params == null || params == '') ? null : params;
	var httpUrl = url;
	if (httpMethod == 'GET' && httpParams != null) {
		httpUrl = httpUrl + "?" + httpParams;
	}
	httpRequest.open(httpMethod, httpUrl, true);
	httpRequest.setRequestHeader(
		'Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.onreadystatechange = callback;
	httpRequest.send(httpMethod == 'POST' ? httpParams : null);
}

function id_check2(){

	var frm = document.subjoinform;
	var id = frm.subid.value;

	
//	if(id.length < 4 || id.length> 20) {
//		alert('4~20자 이내의 아이디를 입력하여 주십시오.');
//		frm.subid.focus();
//		return;
//	}

	var chk_num = id.search(/[0-9]/g); 
	var chk_eng = id.search(/[a-z]/ig);
	//var chk_esp = id.search(/[^0-9a-zA-Z]/g);  
	/*
	if(chk_eng> 0 || chk_num < 0) {
		alert('영문 또는 영문+숫자의 조합으로 아이디를 설정해 주세요 첫번째 글자는 영문입니다.'); 
		frm.strId.focus();
		return;
	}
	*/
	if(id.length > 3 && id.length < 21) {
		var params = "strId="+encodeURIComponent(id);
		sendRequest("/srm/member/ajax_idCheck.asp", params, idcheckResult2, 'POST');
	}

}

function idcheckResult2() {
	if (httpRequest.readyState == 4) {

		if (httpRequest.status == 200) {
			var resultText = httpRequest.responseText;

			rel=resultText.split("||");
			var jangList=document.getElementById("id_ck");

			var resultText = httpRequest.responseText;
			jangList.innerHTML=rel[1];

			if(rel[0]=="y"){ document.subjoinform.num.value="y";	}else{	document.subjoinform.num.value="";	}

		} else {
			alert("Error: "+httpRequest.status);
		}
	}
}

function subjoin_chk() {
	var frm=document.subjoinform;

	if(frm.subname.value == "") {
		alert("이름을 입력해 주세요.");
		frm.subname.focus();
		return false;
	} else if(frm.department.value == "") {
		alert("부서를 입력해 주세요.");
		frm.department.focus();
		return false;
	} else if(frm.subid.value == "") {
		alert("아이디를 입력해 주세요.");
		frm.subid.focus();
		return false;
	} else if(frm.subid.value.length < 4 || frm.subid.value.length > 20) {
		alert("아이디는 4~20자 이내입니다.");
		frm.subid.focus();
		return false;
	} else if(frm.num.value == "") {
		alert("아이디 중복체크를 하시거나 중복된 아이디 사용을 피해주세오.");
		id_check2();
		return false;
	} else if(frm.subpwd.value == "") {
		alert("비밀번호를 입력해 주세요.");
		frm.subpwd.focus();
		return false;
	} else if(frm.subpwd.value.length < 8 || frm.subpwd.value.length > 20) {
		alert("비밀번호는 8~20자 이내입니다.");
		frm.subpwd.focus();
		return false;
	} else if(frm.subpwdRe.value == "") {
		alert("비밀번호 확인을 입력해 주세요.");
		frm.subpwdRe.focus();
		return false;
	} else if(frm.subpwd.value != frm.subpwdRe.value) {
		alert("비밀번호가 일치하지 않습니다.");
		frm.subpwdRe.focus();
		return false;
	} else if(frm.submobile1.value == "" || frm.submobile2.value == "" || frm.submobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.submobile1.focus();
		return false;
	} else if(isNaN(frm.submobile1.value) || isNaN(frm.submobile2.value) || isNaN(frm.submobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.submobile1.focus();
		return false;
	} else if(frm.subemail1.value == "" || frm.subemail2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.subemail1.focus();
		return false;
	} else {
		frm.submit();
	}
}

function subupdate_chk() {
	var frm=document.subjoinform;

	if(frm.subpwd.value != "") {
		if(frm.subpwd.value.length < 8 || frm.subpwd.value.length > 20) {
			alert("비밀번호는 8~20자 이내입니다.");
			frm.subpwd.focus();
			return false;
		} else if(frm.subpwdRe.value == "") {
			alert("비밀번호 확인을 입력해 주세요.");
			frm.subpwdRe.focus();
			return false;
		} else if(frm.subpwdRe.value.length < 8 || frm.subpwdRe.value.length > 20) {
			alert("비밀번호는 8~20자 이내입니다.");
			frm.subpwdRe.focus();
			return false;
		} else if(frm.subpwd.value != frm.subpwdRe.value) {
			alert("비밀번호가 일치하지 않습니다.");
			frm.subpwdRe.focus();
			return false;
		}
	}

	if(frm.subname.value == "") {
		alert("이름을 입력해 주세요.");
		frm.subname.focus();
		return false;
	} else if(frm.department.value == "") {
		alert("부서를 입력해 주세요.");
		frm.department.focus();
		return false;
	} else if(frm.submobile1.value == "" || frm.submobile2.value == "" || frm.submobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.submobile1.focus();
		return false;
	} else if(isNaN(frm.submobile1.value) || isNaN(frm.submobile2.value) || isNaN(frm.submobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.submobile1.focus();
		return false;
	} else if(frm.subemail1.value == "" || frm.subemail2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.subemail1.focus();
		return false;
	} else {
		frm.submit();
	}
}

function ask_chk() {
	var frm=document.onlineform;

	if(frm.on_cate.value == "") {
		alert("분류를 선택해 주세요.");
		frm.on_cate.focus();
		return false;
	} else if(frm.on_company.value == "") {
		alert("회사명을 입력해 주세요.");
		frm.on_company.focus();
		return false;
	} else if(frm.on_name.value == "") {
		alert("담당자를 입력해 주세요.");
		frm.on_name.focus();
		return false;
	} else if(frm.on_mobile1.value == "" || frm.on_mobile2.value == "" || frm.on_mobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(isNaN(frm.on_mobile1.value) || isNaN(frm.on_mobile2.value) || isNaN(frm.on_mobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(frm.on_email1.value == "" || frm.on_email2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.on_email1.focus();
		return false;
	} else if(frm.on_title.value == "") {
		alert("제목을 입력해 주세요.");
		frm.on_title.focus();
		return false;
	} else if(frm.on_content.value == "") {
		alert("내용을 입력해 주세요.");
		frm.on_content.focus();
		return false;
	} else {
		frm.submit();
	}
}

function estimate_ask_chk() {
	var frm=document.onlineform;

	if(frm.on_type.value == "estimate2") {
		if(frm.on_cate.value == "") {
			alert("분류를 선택해 주세요.");
			frm.on_cate.focus();
			return false;
		}
	}

	if(frm.on_company.value == "") {
		alert("회사명을 입력해 주세요.");
		frm.on_company.focus();
		return false;
	} else if(frm.on_name.value == "") {
		alert("담당자를 입력해 주세요.");
		frm.on_name.focus();
		return false;
	} else if(frm.on_mobile1.value == "" || frm.on_mobile2.value == "" || frm.on_mobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(isNaN(frm.on_mobile1.value) || isNaN(frm.on_mobile2.value) || isNaN(frm.on_mobile3.value)) {
		alert("휴대폰번호를 올바르게 입력해 주세요.");
		frm.on_mobile1.focus();
		return false;
	} else if(frm.on_email1.value == "" || frm.on_email2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.on_email1.focus();
		return false;
	} else if(frm.on_title.value == "") {
		alert("제목을 입력해 주세요.");
		frm.on_title.focus();
		return false;
	} else if(frm.on_content.value == "") {
		alert("내용을 입력해 주세요.");
		frm.on_content.focus();
		return false;
	} else {
		frm.submit();
	}
}

function filterNumber(event) {
	if(event.keyCode > 47 && event.keyCode < 58) {
		return;
	}
	event.preventDefault();
}

function fn_press_han(obj) {
	if(event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 ) return;
	obj.value = obj.value.replace(/[\ㄱ-ㅎㅏ-ㅣ가-힣]/g, '');
}