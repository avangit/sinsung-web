<!-- #include virtual="/srm/usercheck.asp" -->
<%
goods_list = Request("goods_list")
idx = Request("idx")
'goods_price = Request.Form("goods_price")
'goods_cnt = Request.Form("goods_cnt")

If idx <> "" Then
	goods_list = idx
End If
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script type="text/javascript" src="/srm/js/product.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");


		    // 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function(){
				if(window.FileReader){
					var filename = $(this)[0].files[0].name;
				}
				else {
					var filename = $(this).val().split('/').pop().split('\\').pop();
				}
				$(this).siblings('.upload_name').val(filename);
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="ask">
					<div class="inner">
						<div class="sub_tit">
							<h2>견적요청</h2>
						</div>

						<table class="table table_type03">
							<colgroup>
								<col style="width:600px">
								<col style="width:200px">
								<col style="width:200px">

							</colgroup>
							<thead>
								<tr>
									<th>상품정보</th>
									<th>상품금액</th>
									<th>수량</th>

								</tr>
							</thead>
							<tbody>
<%
If Request.Cookies("SRM_LEVEL") = 2 Then
	query_where = query_where & " AND b.mem_idx = " & intSeq
ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
	query_where = query_where & " AND b.mem_idx = " & masterIDX
End If

sql = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_simg, ISNULL(b.mem_price, 0) AS mem_price FROM GOODS A INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx WHERE a.g_idx IN (" & goods_list & ")" & query_where
Set rs = dbconn.execute(sql)

If Not (rs.eof) Then
	Do While Not rs.eof
%>
								<tr>
									<td>
										<div class="cart_div">
											<div class="img_box">
												<a href="#"><img src="/upload/goods/<%=rs("g_simg")%>"></a>
											</div>
											<div class="txt_box">
												<a href="#" class="pro_code">제품코드 : #<%=rs("g_idx")%></a>
												<p class="pro_name"><%=rs("g_name")%></p><br>
											</div>
										</div>
									</td>
									<td class="price"><% If rs("mem_price") = 0 Then %><%=FormatNumber(rs("g_Money"),0)%><% Else %><%=FormatNumber(rs("mem_price"),0)%><% End If %>원</td>
									<td>1개</td>
								</tr>
<%
		rs.MoveNext
	Loop
End If

rs.close
Set rs = Nothing

Call DbClose()
%>
							</tbody>
						</table>
						<div class="order_middle">
							<div class="section_tit">
								<h4>문의 신청 내역</h4>
							</div>
						</div>
						<form name="onlineform" method="post" action="./estimate_ok.asp" enctype="multipart/form-data">
						<input type="hidden" name="goods_list" value="<%=idx%>">
						<table class="table table_type02">
							<colgroup>
								<col width="200px">
								<col width="*">
							</colgroup>

							<tbody>
								<tr>
									<th>회사명 <span>*</span></th>
									<td><input type="text" name="on_company" value="<%=cName%>"></td>
								</tr>
								<tr>
									<th>담당자 <span>*</span></th>
									<td><input type="text" name="on_name" value="<%=strName%>"></td>
								</tr>
								<tr class="phone">
									<th>휴대폰 번호 <span>*</span></th>
									<td>
										<input type="text" name="on_mobile1" maxlength="4" value="<%=strMobile1%>"> -
										<input type="text" name="on_mobile2" maxlength="4" value="<%=strMobile2%>"> -
										<input type="text" name="on_mobile3" maxlength="4" value="<%=strMobile3%>">
										<p><span>*</span> 답변 시 SMS 문자가 발송될 수 있습니다.</p>
									</td>
								</tr>
								<tr class="mail">
									<th>이메일 주소 <span>*</span></th>
									<td>
										<input type="text" name="on_email1" value="<%=strEmail1%>"> @
										<input type="text" name="on_email2" value="<%=strEmail2%>">
										<label>
											<select name="on_email3" id="on_email3" onchange="ChangeEmail()">
												<option value="0">직접입력</option>
												<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												<option value="empal.com">empal.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="lycos.co.kr">lycos.co.kr</option>
												<option value="naver.com">naver.com</option>
												<option value="nate.com">nate.com</option>
												<option value="netsgo.com">netsgo.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="paran.com">paran.com</option>
											</select>
										</label>
										<p><span>*</span> 답변 시 메일이 발송될 수 있습니다.</p>
									</td>
								</tr>
								<tr>
									<th>제목 <span>*</span></th>
									<td><input type="text" name="on_title"></td>
								</tr>
								<tr>
									<th>내용 <span>*</span></th>
									<td><textarea name="on_content"></textarea></td>
								</tr>
								<tr>
									<th>첨부파일 <span class="sp_file">(10M 이하)</span></th>
									<td>
										<div class="file_box">
											<label for="on_file1">첨부파일</label>
											<input type="file" id="on_file1" name="on_file1" class="upload_hidden" style="display: none;">
											<input class="upload_name" disabled="disabled">
										</div>
										<div class="file_box">
											<label for="on_file2">첨부파일</label>
											<input type="file" id="on_file2" name="on_file2" class="upload_hidden" style="display: none;">
											<input class="upload_name" disabled="disabled">
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="btn_box ask_btn_box">
							<button class="btn_wh" onclick="goBack();return false;">취소</button>
							<button class="btn_blue" onclick="ask_chk();return false;">확인</button>
						</div>
						</form>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->