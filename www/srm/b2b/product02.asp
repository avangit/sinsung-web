<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>
<%
Dim fieldname 	: fieldname 	= SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	= SQL_Injection(Trim(Request("fieldvalue")))
Dim orderby		: orderby		= SQL_Injection(Trim(Request("orderby")))
Dim intNowPage	: intNowPage 	= SQL_Injection(Trim(Request("page")))
%>
<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script src="/srm/js/jquery.navgoco.js"></script>
	<script src="/js/product.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");

			// 상품 배열
			$(".align_box .two").click(function() {
				$(".pro_list").addClass('pro_list02');
			});
			$(".align_box .one").click(function() {
				$(".pro_list").removeClass('pro_list02');
			});

			// 두번째 탭에 메뉴가 있으면 탭보여주고 아니면 숨기기
			if($('.pro_tab_area_hide .sub_con_tab').find('li').length == 0) {
				$('.pro_tab_area_hide').hide();
			} else {
				$('.pro_tab_area_hide').show();
			}

			var currentUrl = $(location).attr('href'); // 현재 URL 반환
			var urlAdd = currentUrl.substring(currentUrl.lastIndexOf('=') + 1); // 현재 URL에서 =부터시작하는 주소를 담음
			var urlNum =  urlAdd.substring(0,4); // 현재 URL에서 =부터시작하는 주소중 앞 4자리
			var firstDepthLinks = $(".pro_tab_depth1 a").get(); // 첫번째 탭메뉴의 모든 a를 배열로 담음

			// 두번째 탭 메뉴 클릭시 첫번째 탭 active 유지
			if($('.pro_tab_area_hide .sub_con_tab').find('li').length != 0) {
				for(var i = 0; i < firstDepthLinks.length; i++) { // 첫번째 탭메뉴 a의 수만큼 반복
					if(firstDepthLinks[i].getAttribute('href').indexOf(urlNum) != -1) { // 첫번째 탭 메뉴들의 주소 중에서 현재 URL에서 =부터시작하는 주소중 앞 4자리가 포함되어 있다면
						firstDepthLinks[i].parentNode.classList.add('active'); // 해당 a메뉴의 부모노드에 active 클래스 추가
					}
				}
			}

			var currentUrlMenu = currentUrl.substring(currentUrl.lastIndexOf('?')); // 현재 URL에서 ?부터시작하는 주소를 담음

			for(var i = 0; i < firstDepthLinks.length; i++) { //a의 수만큼 반복
				if(currentUrlMenu == firstDepthLinks[i].getAttribute('href')) { // 현재 ?부터 시작하는 주소와 a의 href 주소가 일치하면
					firstDepthLinks[i].parentNode.classList.add('active'); // 해당 a의 부모노드에 active 클래스 추가
				}
			}


			// 탭메뉴 더보기
			$(".p_plus").click(function() {
				if($(this).hasClass('on')) {
					$(this).removeClass('on');
					$(this).parent().children('.sub_con_tab').css('height','70px');
				} else {
					$(this).addClass('on');
					$(this).parent().children('.sub_con_tab').css('height','100%');
				}
			});

			$(".li_close").click(function() {
				$(this).remove();
			});

			// 세부내용
			$('.f_hide').click(function() {
				if ($(this).hasClass('open')) {
					$(this).removeClass('open');
					$(this).text('세부내용 숨기기');
					$('.hide').show();
				} else {
					$(this).addClass('open');
					$(this).text('세부내용 보기');
					$('.hide').hide();
				}
			});

		});
	</script>
</head>
<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="product">
					<div class="inner">
						<div class="sub_tit">
							<h2>B2B제품</h2>
						</div>
						<div id="sub_nav">
							<div class="inner">
								<ul>
									<li class="home"><a href="/srm/">HOME</a></li>
									<li><a href="#">B2B 제품</a></li>
								</ul>
							</div>
						</div>
					</div>
<%
	cate = Request.QueryString("c")

	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
					<div class="pro_tab">
						<div class="inner">
							<ul>
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
								<li <% If Left(cate, 2) = Left(rs("c_code"), 2) Then %> class="on"<% End If %>><a href="product02.asp?c=<%=rs("c_code")%>"><i></i><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
							</ul>
						</div>
					</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 6) = '000000' AND c_code LIKE '" & Left(cate, 2) & "%' AND SUBSTRING(c_code, 4, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
					<div class="inner">
						<div class="pro_tab_area">
							<ul class="sub_con_tab">
<%
	If Not(rs.bof Or rs.eof) Then
		i = 1
		Do While Not rs.eof
%>
								<li><a href="?c=<%=rs("c_code")%>#product_0<%=i%>"><%=rs("c_name")%></a></li>
<%
			i = i + 1
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
							</ul>
							<button class="p_plus"><img src="/srm/images/sub/p_icon_plus.png"></button>
						</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 4) = '0000' AND c_code LIKE '" & Left(cate, 4) & "%' AND SUBSTRING(c_code, 6, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
						<div class="pro_tab_area pro_tab_area_hide">
							<ul class="sub_con_tab">
<%
	If Not(rs.bof Or rs.eof) Then
		j = i
		Do While Not rs.eof
%>
									<li <% If cate = rs("c_code") Then %> class="active"<% End If %>><a href="product02.asp?c=<%=rs("c_code")%>#product_<%=j%>"><%=rs("c_name")%></a></li>
<%
			j = j + 1
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
								<!--li><a href="#product_11">토너/잉크/드럼</a></li>
								<li><a href="#product_12">2차 카테고리</a></li>
								<li><a href="#product_13">2차 카테고리</a></li>
								<li><a href="#product_14">2차 카테고리</a></li>
								<li><a href="#product_15">2차 카테고리</a></li>
								<li><a href="#product_16">2차 카테고리</a></li-->
							</ul>
							<button class="p_plus"></button>
						</div>
						<form name="search_form" action="" method="post">
						<div class="search_box">
							<div>
								<select name="fieldname">
									<option value="g_name" <% If fieldname = "" Or fieldname = "g_name" Then %> selected<% End If %>>제품명</option>
									<option value="g_idx" <% If fieldname = "g_idx" Then %> selected<% End If %>>제품코드</option>
								</select>
								<input type="text" name="fieldvalue" value="<%=fieldvalue%>">
								<button class="btn_search" onclick="document.search_form.submit();">검색</button>
							</div>
						</div>
						<div class="align_box">
							<button class="one">1단보기</button>
							<button class="two">2단보기</button>
							<select name="orderby" onchange="document.search_form.submit();">
								<option value="g_read" <% If orderby = "g_read" Then %> selected<% End If %>>인기순</option>
								<option value="regdate" <% If orderby = "regdate" Then %> selected<% End If %>>최신순</option>
								<option value="g_Money_asc" <% If orderby = "g_Money_asc" Then %> selected<% End If %>>낮은 가격순</option>
								<option value="g_Money_desc" <% If orderby = "g_Money_desc" Then %> selected<% End If %>>높은 가격순</option>
							</select>
						</div>
						</form>
<%
	If Request.Cookies("SRM_LEVEL") = 2 Then
		query_where3 = query_where3 & " AND c.mem_idx = " & intSeq
	ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
		query_where3 = query_where3 & " AND b.mem_idx = " & masterIDX
	End If

	If cate <> "" Then
		If Right(cate, 8) = "00000000" Then
			query_where2 = query_where2 & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
		ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
			query_where2 = query_where2 & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
		ElseIf Mid(cate, 6, 1) <> 0 Then
			query_where2 = query_where2 & " AND a.g_category = '" & cate & "'"
		End If

		sql = "SELECT c_name FROM CATEGORY WHERE c_code = '" & cate & "'"
		Set rs = dbconn.execute(sql)

		If Not(rs.bof Or rs.eof) Then
			cateName = rs("c_name")
		End If

		rs.close

		If Request.Cookies("SRM_LEVEL") <> 9 Then
			sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a LEFT JOIN CATEGORY b ON a.g_category = b.c_code LEFT JOIN GOODS_SRM_JOIN c ON a.g_idx = c.g_idx WHERE a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y' " & query_where2 & " " & query_where3 & ""
		Else
			sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a LEFT JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y' " & query_where2 & ""
		End If
	Else
		cateName = "전체제품"
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a LEFT JOIN CATEGORY b ON a.g_category = b.c_code LEFT JOIN GOODS_SRM_JOIN c ON a.g_idx = c.g_idx WHERE a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y' " & query_where3 & ""
		Else
			sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a LEFT JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y' "
		End If
	End If

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		TotalCnt = rs("CNT")
	End If

	rs.close
%>
						<div class="filter_set">
							<h4><%=cateName%> <span>(<%=TotalCnt%>)</span></h4>
							<!--ul>
								<li class="f_reset"><a href="javascript:;">필터재설정</a></li>
								<li class="li_close"><a href="javascript:;">필터재설정</a></li>
								<li class="li_close"><a href="javascript:;">필터재설정</a></li>
								<li class="li_close"><a href="javascript:;">필터재설정</a></li>
							</ul-->
							<button type="button" class="f_hide">세부내용 숨기기</button>
						</div>
					</div>
					<div class="tab_contents" id="product_02">
						<div class="inner">
							<div id="A_Container_L">
								<div class="lnb">
									<ul class="nav">
<%
	sql2 = "SELECT b.ogroup_content FROM CATEGORY a INNER JOIN cate_option_group b ON a.c_option_group = b.ogroup_idx WHERE a.c_code = '" & cate & "'"
	Set cateRS = dbconn.execute(sql2)

	If Not cateRS.eof Then
		ogroup_content = cateRS("ogroup_content")
	End If

	cateRS.close
	Set cateRS = Nothing

	For og = 0 To UBound(Split(ogroup_content, ", "))
		sql = "SELECT opt_name, opt_content FROM cate_option WHERE opt_name = '" & Split(ogroup_content, ", ")(og) & "'"
		Set rs = dbconn.execute(sql)

		If Not rs.eof Then
			i = 1
			Do While Not rs.eof
				opt_name = Trim(rs("opt_name"))
				opt_content = Trim(rs("opt_content"))

				arr_opt_content = Split(opt_content, ", ")
%>
										<li class="open">
											<a href="#"><span><%=opt_name%></span></a>
											<ul class="ss_menu">
												<li><a href="#.html">
													<div class="chk_box">
													<% For j = 0 To Ubound(arr_opt_content) %>
														<input id="fliter<%=j%>" type="checkbox" name="fliter<%=i%>" value="<%=opt_name%>||<%=arr_opt_content(j)%>">
														<label for="fliter<%=j%>"><span></span><%=arr_opt_content(j)%></label>
													<% Next %>
													</div>
												</a></li>
											</ul>
										</li>
<%
				i = i + 1
				rs.MoveNext
			Loop
		End If
	Next

'	rs.close
%>
										<!--li>
											<a href="#"><span>필터명</span></a>
											<ul class="ss_menu">
												<li><a href="#.html">
														<div class="chk_box">
															<input id="fliter02" type="checkbox">
															<label for="fliter02"><span></span></label>
														</div>필터상세명
													</a></li>
												<li><a href="#.html">
														<div class="chk_box">
															<input id="fliter03" type="checkbox">
															<label for="fliter03"><span></span></label>
														</div>필터상세명
													</a></li>
												<li><a href="#.html">
														<div class="chk_box">
															<input id="fliter04" type="checkbox">
															<label for="fliter04"><span></span></label>
														</div>필터상세명
													</a></li>
											</ul>
										</li-->

									</ul>
								</div>
								<script type="text/javascript">
									$(document).ready(function() {
										$(".nav").navgoco({
											caretHtml: '',
											accordion: false,
											openClass: 'open',
											save: true,
											cookie: {
												name: 'navgoco',
												expires: false,
												path: '/'
											},
											slide: {
												duration: 400,
												easing: 'swing'
											},
										});
										$("#collapseAll").click(function(e) {
											e.preventDefault();
											$(".nav").navgoco('toggle', false);
										});

										$("#expandAll").click(function(e) {
											e.preventDefault();
											$(".nav").navgoco('toggle', true);
										});
									});

								</script>
							</div>
<%
Dim intTotalCount, intTotalPage
Dim intPageSize			: intPageSize 		= 4
Dim intBlockPage		: intBlockPage 		= 5

If Request.Cookies("SRM_LEVEL") <> 9 Then
	Dim query_filde			: query_filde		= " a.g_idx, a.g_name, a.g_Money, a.g_simg, a.g_spec, ISNULL(b.mem_price, 0) AS mem_price "
	Dim query_Tablename		: query_Tablename	= "GOODS a INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx"
Else
	query_filde		= " a.g_idx, a.g_name, a.g_Money, a.g_simg, a.g_spec "
	query_Tablename	= "GOODS a "
End If
Dim query_where			: query_where		= " a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y'"

If Request.Cookies("SRM_LEVEL") = 2 Then
	query_where = query_where & " AND b.mem_idx = " & intSeq
ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
	query_where = query_where & " AND b.mem_idx = " & masterIDX
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

If cate <> "" Then
	If Right(cate, 8) = "00000000" Then
		query_where = query_where & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
	ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
		query_where = query_where & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
	ElseIf Mid(cate, 6, 1) <> 0 Then
		query_where = query_where & " AND a.g_category = '" & cate & "'"
	End If
End If

If orderby = "" Then
	query_orderby = " ORDER BY a.g_read DESC"
Else
	If orderby = "g_Money_asc" Then
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			query_orderby = " ORDER BY b.mem_price ASC"
		Else
			query_orderby = " ORDER BY a.g_Money ASC"
		End If
	ElseIf orderby = "g_Money_desc" Then
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			query_orderby = " ORDER BY b.mem_price DESC"
		Else
			query_orderby = " ORDER BY a.g_Money DESC"
		End If
	ElseIf orderby = "regdate" Then
		query_orderby = " ORDER BY a.g_insertDay DESC"
	End If
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
							<div id="A_Container_R" class="right_list">
							<form name="list" method="post" action="./product_compare.asp" style="margin:0;">
							<input type="hidden" name="n">
							<input type="hidden" name="m">
							<input type="hidden" name="message">
								<ul class="pro_list pro_list02">
<%
If rs.bof Or rs.eof Then
%>
									<li>등록된 제품이 없습니다.</li>
<%
Else
	i = 0
	rs.move MoveCount
	Do While Not rs.eof
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			If rs("mem_price") = 0 Then
				finalPrice = rs("g_Money")
			Else
				finalPrice = rs("mem_price")
			End If
		Else
			finalPrice = rs("g_Money")
		End If
%>
									<li>
										<div class="a_box">
											<div class="chk_box">
												<input id="pro0<%=i%>" type="checkbox" value="<%=rs("g_idx")%>">
												<label for="pro0<%=i%>"><span></span></label>
											</div>
											<label for="prod"><span></span></label>
											<a href="/srm/b2b/product_estimate.asp?idx=<%=rs("g_idx")%>">견적요청</a>
											<a href="/srm/order/cart_add_ok.asp?idx=<%=rs("g_idx")%>">장바구니</a>
										</div>
										<div class="p_contents">

											<a href="./product_view.asp?idx=<%=rs("g_idx")%>"> <div class="img_box"><img src="/upload/goods/<%=rs("g_simg")%>" alt="<%=rs("g_name")%>"></div>
											<div class="cont_box">
												<p class="pro_tit"><%=rs("g_name")%></p>
												<span># <%=rs("g_idx")%></span>
												<span class="hide"><%=rs("g_spec")%></span>
												<strong class="price"><%=FormatNumber(finalPrice, 0)%> 원</strong>
											</div>
											</a>
										</div>
									</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
								</ul>
								</form>

								<% Call Paging_user_srm("") %>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->