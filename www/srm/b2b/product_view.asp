<!-- #include virtual="/srm/usercheck.asp" -->
<%
g_idx = SQL_Injection(Trim(Request.QueryString("idx")))
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<link rel="stylesheet" href="/srm/_css/slick-theme.css">
	<script src="/srm/js/slick.js"></script>
	<script type="text/javascript" src="/srm/js/jquery.number.min.js"></script>

	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");
			$(".tab_contents").hide();
			$(".sub_con_tab li:first").addClass("active").show();
			$(".tab_contents:first").show();

			$(".sub_con_tab li").click(function() {
				$(".sub_con_tab li").removeClass("active");
				$(this).addClass("active");
				$(".tab_contents").hide();
				var activeTab = $(this).find("a").attr("href");
				$(activeTab).fadeIn();
				return false;
			});
			$('.slider-for').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
//				slidesToShow: 5,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				dots:false,
				centerMode: true,
				focusOnSelect: true,
				variableWidth: true
			});

			$('#increase').click(function(e){
				e.preventDefault();
				var stat = $('#updown').val();
				var num = parseInt(stat,10);
				num++;

				$('#updown').val(num);
				$('#GoodPrice').text(num*$("#GoodPrice").attr("rel"));
				$('#GoodPrice').number( true );
			});

			$('#decrease').click(function(e){
				e.preventDefault();
				var stat = $('#updown').val();
				var num = parseInt(stat,10);
				num--;

				if(num<=0){
					alert('더이상 줄일수 없습니다.');
					num =1;
				}
				$('#updown').val(num);
				$('#GoodPrice').text(num*$("#GoodPrice").attr("rel"));
				$('#GoodPrice').number( true );
			});
		});

		function direct_order() {
			document.directOrderForm.GoodPrice.value = Number($("#GoodPrice").attr("rel")) * Number($('#updown').val());
			document.directOrderForm.method = "post";
			document.directOrderForm.action = "/srm/order/order_direct.asp";
			document.directOrderForm.submit();
		}

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
ReDim g_bimg(4), g_menual(4)

If Request.Cookies("SRM_LEVEL") = 2 Then
	query_where = query_where & " AND b.mem_idx = " & intSeq
ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
	query_where = query_where & " AND b.mem_idx = " & masterIDX
End If

If Request.Cookies("SRM_LEVEL") = 9 Then
	sql = "SELECT A.* FROM GOODS A WHERE A.g_display <> 'home' AND A.g_act = 'Y' AND A.g_idx = '" & g_idx & "'"
Else
	sql = "SELECT A.*, ISNULL(B.mem_price, 0) AS mem_price FROM GOODS A INNER JOIN goods_srm_join B ON A.g_idx = B.g_idx WHERE A.g_display <> 'home' AND A.g_act = 'Y' AND A.g_idx = '" & g_idx & "'" & query_where
End If
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	g_name = rs("g_name")
	g_spec = rs("g_spec")
	g_intro = rs("g_intro")
	g_money = rs("g_money")
	total_op = rs("total_op")
	g_use_totalChk = rs("g_use_totalChk")
	g_optionList = rs("g_optionList")
	g_simg = rs("g_simg")
	For i = 1 To 5
		g_bimg(i-1) = rs("g_bimg" & i)
	Next
	g_memo = rs("g_memo")
	g_video = rs("g_video")
	For j = 1 To 5
		g_menual(j-1) = rs("g_menual" & j)
	Next

	If Request.Cookies("SRM_LEVEL") <> 9 Then
		If rs("mem_price") = 0 Then
			FinalPrice = rs("g_Money")
		Else
			FinalPrice = rs("mem_price")
		End If
	Else
		FinalPrice = rs("g_Money")
	End If
Else
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

sql = "UPDATE GOODS SET g_read = g_read + 1 WHERE g_type = 1 AND g_display <> 'home' AND g_act = 'Y' AND g_idx = '" & g_idx & "'"
dbconn.execute(sql)

rs.Close
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="product_view">
					<div class="inner">
						<div class="sub_tit">
							<h2>B2B제품</h2>
						</div>
<%
	sql2 = "SELECT * FROM GOODS_CATE_JOIN WHERE g_idx = '" & g_idx & "' ORDER BY gcj_idx DESC"
	Set rs2 = dbconn.execute(sql2)
%>
						<div id="sub_nav">
							<div class="inner">
								<ul>
<%
	If Not rs2.eof Then
		Do While Not rs2.eof
%>
									<li class="home"><a href="/srm/">HOME <%=getCateColPrint(rs2("c_code"), "CATEGORY")%></a></li>
<%
			rs2.movenext
		Loop
	End If

	rs2.close
	Set rs2 = Nothing
%>
								</ul>
							</div>
						</div>
					</div>
					<!-- 슬라이드영역-->
					<div class="pdtop">
						<div class="inner">
							<div class="prov_slide">
								<div class="slider-for">
								<% If g_simg <> "" Then %>
									<img src="/upload/goods/<%=g_simg%>">
								<% End If %>
<%
							For i = 1 To 5
								If g_bimg(i-1) <> "" Then
%>
									<img src="/upload/goods/<%=g_bimg(i-1)%>">
<%
								End If
							Next
%>
								</div>
								<div class="slider-nav">
									<div class="img_box">
										<img src="/upload/goods/<%=g_simg%>">
									</div>
<%
							For i = 1 To 5
								If g_bimg(i-1) <> "" Then
%>
									<div class="img_box">
										<img src="/upload/goods/<%=g_bimg(i-1)%>">
									</div>
<%
								End If
							Next
%>
								</div>
							</div>

							<!--제품설명영역-->
							<div class="prov_cont">
								<ul>
									<li>
										<p>제품코드 : #<%=g_idx%></p>
										<h3><%=g_name%></h3>
									</li>
									<li class="line">
										<p class="tit">주요특징 <span><%=g_spec%></span></p>
										<p class="tit">제품개요 <span><%=g_intro%></span></p>
									</li>
									<li class="li_mg">
										<p class="tit tit2">가격 </p><strong id="GoodPrice" name="GoodPrice" rel="<%=FinalPrice%>"><%=FormatNumber(FinalPrice, 0)%></strong>
									</li>
								<form name="directOrderForm">
								<input type="hidden" name="idx" value="<%=g_idx%>">
								<input type="hidden" name="GoodPrice">
									<li class="li_mg">
										<p class="tit tit2" style="clear: both;">
											수량 <span class="inven">(재고:<%=total_op%>)</span>
										</p> <strong>
											<div class="vol_btns">
												<a href="#"><img src="/srm/images/sub/but_vol_up.png" id="decrease"></a>
												<input type="text" name="g_cnt" id="updown" value="1">
												<a href="#"><img src="/srm/images/sub/but_vol_down.png" id="increase"></a>
											</div>
										</strong>
									</li>
								</form>
									<li class="btn_list">
										<button class="btn_wh" onclick="location.href='/srm/b2b/product_estimate.asp?idx=<%=g_idx%>'">견적요청</button>
										<button class="btn_bk" onclick="location.href='/srm/order/cart_add_ok.asp?idx=<%=g_idx%>&price=<%=finalPrice%>'">장바구니</button>
										<button class="btn_blue" onclick="direct_order();">바로구매</button>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="inner">
						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li><a href="#pro_view_01">제품사양</a></li>
								<li><a href="#pro_view_02">제품상세</a></li>
								<li><a href="#pro_view_03">제품영상</a></li>
								<li><a href="#pro_view_04">제품 매뉴얼</a></li>
							</ul>
							<div class="tab_contents " id="pro_view_01">
								<table class="table pv_table">
									<tbody>
<%
	If g_optionList <> "" Then
		arr_g_optionList = Split(g_optionList, ",")

		If Ubound(arr_g_optionList) > 0 Then
			For i = 0 To Ubound(arr_g_optionList) - 1
				arr_g_optionList1 = Split(arr_g_optionList(i), "||")
%>
										<tr>
											<th><%=Trim(arr_g_optionList1(0))%></th>
											<td><%=Trim(arr_g_optionList1(1))%></td>
										</tr>
<%
			Next
		End If
	End If
%>
									</tbody>
								</table>
							</div>

							<div class="tab_contents " id="pro_view_02">
								<div class="dateil_area">
									<%=g_memo%>
								</div>
							</div>
<%
sql = "SELECT b_addtext1 FROM BOARD_v1 WHERE b_part = 'board08' AND b_idx = " & g_video
Set rs = dbconn.execute(sql)
%>
							<div class="tab_contents " id="pro_view_03">
								<div class="video_area">
								<% If Not(rs.eof) Then %>
									<%=rs("b_addtext1")%>
								<% End If %>
								</div>
<% rs.close %>
							</div>
<%
For j = 1 To 5
	If g_menual(j-1) <> "" Then
		If j = 1 Then
			arr_var = arr_var & g_menual(j-1)
		Else
			arr_var = arr_var & "," & g_menual(j-1)
		End If
	End If
Next

sql = "SELECT file_1 FROM BOARD_v1 WHERE b_part = 'board07' AND b_idx IN (" & arr_var & ")"
Set rs = dbconn.execute(sql)
%>
							<div class="tab_contents " id="pro_view_04">
								<table class="tab_table">
									<tbody>
<%
	If Not(rs.eof) Then
		Do Until rs.Eof
%>
										<tr>
											<th><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download><%=rs("file_1")%> <!--p>20MB</p--></a></th>
											<td><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download>다운로드</a></td>
										</tr>
<%
			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->