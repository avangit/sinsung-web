<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script src="/srm/js/jquery.navgoco.js"></script>
		<script type="text/javascript">
			$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");

		});
		$(document).ready(function() {
		$(".p_plus").click(function() {
				if($(this).hasClass('on')) {
					$(this).removeClass('on');
					$(this).parent().children('.sub_con_tab').css('height','70px');
				} else {
					$(this).addClass('on');
					$(this).parent().children('.sub_con_tab').css('height','100%');
				}
			});
			$(".sub_con_tab li").click(function() {
				$(".pro_tab_area_hide").slideDown();
			});
			$(".li_close").click(function() {
				$(this).remove();
			});
		});

		$(document).ready(function() {
			$('.f_hide').click(function() {
				if ($(this).hasClass('open')) {
					$(this).removeClass('open');
					$(this).text('세부내용 숨기기');
					$('.hide').show();
				} else {
					$(this).addClass('open');
					$(this).text('세부내용 보기');
					$('.hide').hide();
				}
			});
		});
	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	= SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	= SQL_Injection(Trim(Request("fieldvalue")))
Dim orderby		: orderby		= SQL_Injection(Trim(Request("orderby")))
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="product">
					<div class="inner">
						<div class="sub_tit">
							<h2>B2B제품</h2>
						</div>
<%
	sql2 = "SELECT * FROM GOODS_CATE_JOIN WHERE g_idx = '" & g_idx & "' ORDER BY gcj_idx DESC"
	Set rs2 = dbconn.execute(sql2)
%>
						<div id="sub_nav">
							<div class="inner">
								<ul>
<%
	If Not rs2.eof Then
		Do While Not rs2.eof
%>
									<li class="home"><a href="/srm/">HOME <%=getCateColPrint(rs2("c_code"), "CATEGORY")%></a></li>
<%
			rs2.movenext
		Loop
	End If

	rs2.close
	Set rs2 = Nothing
%>
								</ul>
							</div>
						</div>
					</div>
<%
	cate = Request.QueryString("c")

	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
					<div class="pro_tab">
						<div class="inner">
							<ul>
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
								<li <% If Left(cate, 2) = Left(rs("c_code"), 2) Then %> class="on"<% End If %>><a href="product01.asp?c=<%=rs("c_code")%>"><i></i><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
							</ul>
						</div>
					</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 6) = '000000' AND c_code LIKE '" & Left(cate, 2) & "%' AND SUBSTRING(c_code, 4, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
					<div class="inner">
						<div class="pro_tab_area pro_tab_area1">
							<ul class="sub_con_tab">
								<li><a href="#product_01">전체</a></li>
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
								<li><a href="?c=<%=rs("c_code")%>#product_01"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
							</ul>
							<button class="p_plus"></button>
						</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 4) = '0000' AND c_code LIKE '" & Left(cate, 4) & "%' AND SUBSTRING(c_code, 6, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
						<div class="pro_tab_area pro_tab_area_hide">
							<ul class="sub_con_tab">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
								<li><a href="#product_11"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
							</ul>
							<button class="p_plus2 p_plus"></button>
						</div>
						<!--
						<div class="pro_margin">
							<form name="search_form" action="" method="post">
							<div class="search_box">
								<div>
									<select name="fieldname">
										<option value="g_name" <% If fieldname = "" Or fieldname = "g_name" Then %> selected<% End If %>>제품명</option>
										<option value="g_idx" <% If fieldname = "g_idx" Then %> selected<% End If %>>제품코드</option>
									</select>
									<input type="text" name="fieldvalue" value="<%=fieldvalue%>">
									<button class="btn_search" onclick="document.search_form.submit();">검색</button>
								</div>
							</div>
							<div class="align_box">
								<a href="product01.asp"><img src="/srm/images/sub/icon_one.png"></a>
								<a href="product02.asp"><img src="/srm/images/sub/icon_two.png"></a>
								<select name="orderby" onchange="document.search_form.submit();">
									<option value="g_read" <% If orderby = "g_read" Then %> selected<% End If %>>인기순</option>
									<option value="regdate" <% If orderby = "regdate" Then %> selected<% End If %>>최신순</option>
									<option value="g_Money_asc" <% If orderby = "g_Money_asc" Then %> selected<% End If %>>낮은 가격순</option>
									<option value="g_Money_desc" <% If orderby = "g_Money_desc" Then %> selected<% End If %>>높은 가격순</option>
								</select>
							</div>
							<div class="filter_set filter_set2">
								<button type="button" class="f_hide">세부내용 숨기기</button>
							</div>
						-->
						
						
						</div>
						</form>
					</div>
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 3
Dim intBlockPage		: intBlockPage 		= 5

If Request.Cookies("SRM_LEVEL") <> 9 Then
	Dim query_filde			: query_filde		= " a.g_idx, a.g_name, a.g_Money, a.g_simg, a.g_spec, ISNULL(b.mem_price, 0) AS mem_price "
	Dim query_Tablename		: query_Tablename	= "GOODS a INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx"
Else
	query_filde		= " a.g_idx, a.g_name, a.g_Money, a.g_simg, a.g_spec "
	query_Tablename	= "GOODS a "
End If
Dim query_where			: query_where		= " a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y'"

If Request.Cookies("SRM_LEVEL") = 2 Then
	query_where = query_where & " AND b.mem_idx = " & intSeq
ElseIf Request.Cookies("SRM_LEVEL") = 3 Then
	query_where = query_where & " AND b.mem_idx = " & masterIDX
End If

If orderby = "" Then
	query_orderby = " ORDER BY a.g_read DESC"
Else
	If orderby = "g_Money_asc" Then
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			query_orderby = " ORDER BY b.mem_price ASC"
		Else
			query_orderby = " ORDER BY a.g_Money ASC"
		End If
	ElseIf orderby = "g_Money_desc" Then
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			query_orderby = " ORDER BY b.mem_price DESC"
		Else
			query_orderby = " ORDER BY a.g_Money DESC"
		End If
	ElseIf orderby = "regdate" Then
		query_orderby = " ORDER BY a.g_insertDay DESC"
	End If
End If

If cate <> "" Then
	If Right(cate, 8) = "00000000" Then
		query_where = query_where & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
	ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
		query_where = query_where & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
	ElseIf Mid(cate, 6, 1) <> 0 Then
		query_where = query_where & " AND a.g_category = '" & cate & "'"
	End If
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND a."& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
					<div class="w_100">
						<div class="tab_contents" id="product_02">
							<div class="inner">

								<div id="A_Container_R" class="right_list03">
								<div id="A_Container_R" class="right_list03">
								<form name="list" method="post" action="./product_compare.asp" style="margin:0;">
								<input type="hidden" name="n">
								<input type="hidden" name="m">
								<input type="hidden" name="message">
									<ul class="pro_list pro_list03">
<%
If rs.bof Or rs.eof Then
%>
									<li>등록된 제품이 없습니다.</li>
<%
Else
	i = 0
	rs.move MoveCount
	Do While Not rs.eof
		If Request.Cookies("SRM_LEVEL") <> 9 Then
			If rs("mem_price") = 0 Then
				finalPrice = rs("g_Money")
			Else
				finalPrice = rs("mem_price")
			End If
		Else
			finalPrice = rs("g_Money")
		End If
%>
										<li>
											<div class="a_box">
												<div class="chk_box">
													<input type="checkbox" id="pro0<%=i%>" value="<%=rs("g_idx")%>">
													<label for="pro0<%=i%>"><span></span></label>
												</div>
												<label for="prod"><span></span></label>
												<a href="./product_estimate.asp?idx=<%=rs("g_idx")%>">견적요청</a>
												<a href="/srm/order/cart_add_ok.asp?idx=<%=rs("g_idx")%>&price=<%=finalPrice%>">장바구니</a>
											</div>
											<div class="p_contents">
												<div class="img_box"><a href="product_view.asp?idx=<%=rs("g_idx")%>"> <img src="/upload/goods/<%=rs("g_simg")%>" alt="<%=rs("g_name")%>"></a></div>
												<div class="cont_box">
													<p class="pro_tit"><a href="product_view.asp?idx=<%=rs("g_idx")%>"><%=rs("g_name")%></a></p>
													<span><a href="product_view.asp?idx=<%=rs("g_idx")%>"># <%=rs("g_idx")%></a></span>
													<span class="hide"><a href="product_view.asp?idx=<%=rs("g_idx")%>"><%=rs("g_spec")%></a></span>
													<strong class="price"><a href="product_view.asp?idx=<%=rs("g_idx")%>"><%=FormatNumber(finalPrice, 0)%> 원</a></strong>
												</div>
											</div>
										</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
									</ul>
								</div>
							</div>
							<% Call Paging_user_srm("") %>
						</div>
					</div>
				</div>
				<div class="popup">
					<div class="pop_wrap">
						<div class="tit">
							<h3>제목이 노출됩니다</h3>
							<button class="btn_close">닫기</button>
						</div>
						<div class="v_box">
							<img src="image/sub/video_popup_img.jpg">
						</div>
					</div>
				</div>
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>