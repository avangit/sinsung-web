<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>
<%
Dim fieldname 	: fieldname 	= SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	= SQL_Injection(Trim(Request("fieldvalue")))
Dim orderby		: orderby		= SQL_Injection(Trim(Request("orderby")))
Dim intNowPage	: intNowPage 	= SQL_Injection(Trim(Request("page")))
%>
<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script src="/srm/js/jquery.navgoco.js"></script>
	<script src="/js/product.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");

			// 상품 배열
			$(".align_box .one").click(function() {
				$(".pro_list").removeClass('pro_list02');
			});
			$(".align_box .two").click(function() {
				$(".pro_list").addClass('pro_list02');
			});

			// 두번째 탭에 메뉴가 있으면 탭보여주고 아니면 숨기기
			if($('.pro_tab_area_hide .sub_con_tab').find('li').length == 0) {
				$('.pro_tab_area_hide').hide();
			} else {
				$('.pro_tab_area_hide').show();
			}

			var currentUrl = $(location).attr('href'); // 현재 URL 반환
			var urlAdd = currentUrl.substring(currentUrl.lastIndexOf('=') + 1); // 현재 URL에서 =부터시작하는 주소를 담음
			var urlNum =  urlAdd.substring(0,4); // 현재 URL에서 =부터시작하는 주소중 앞 4자리
			var firstDepthLinks = $(".pro_tab_depth1 a").get(); // 첫번째 탭메뉴의 모든 a를 배열로 담음

			// 두번째 탭 메뉴 클릭시 첫번째 탭 active 유지
			if($('.pro_tab_area_hide .sub_con_tab').find('li').length != 0) {
				for(var i = 0; i < firstDepthLinks.length; i++) { // 첫번째 탭메뉴 a의 수만큼 반복
					if(firstDepthLinks[i].getAttribute('href').indexOf(urlNum) != -1) { // 첫번째 탭 메뉴들의 주소 중에서 현재 URL에서 =부터시작하는 주소중 앞 4자리가 포함되어 있다면
						firstDepthLinks[i].parentNode.classList.add('active'); // 해당 a메뉴의 부모노드에 active 클래스 추가
					}
				}
			}

			var currentUrlMenu = currentUrl.substring(currentUrl.lastIndexOf('?')); // 현재 URL에서 ?부터시작하는 주소를 담음

			for(var i = 0; i < firstDepthLinks.length; i++) { //a의 수만큼 반복
				if(currentUrlMenu == firstDepthLinks[i].getAttribute('href')) { // 현재 ?부터 시작하는 주소와 a의 href 주소가 일치하면
					firstDepthLinks[i].parentNode.classList.add('active'); // 해당 a의 부모노드에 active 클래스 추가
				}
			}


			// 탭메뉴 더보기
			$(".p_plus").click(function() {
				if($(this).hasClass('on')) {
					$(this).removeClass('on');
					$(this).parent().children('.sub_con_tab').css('height','70px');
				} else {
					$(this).addClass('on');
					$(this).parent().children('.sub_con_tab').css('height','100%');
				}
			});

			$(".li_close").click(function() {
				$(this).remove();
			});

			// 세부내용
			$('.f_hide').click(function() {
				if ($(this).hasClass('open')) {
					$(this).removeClass('open');
					$(this).text('세부내용 숨기기');
					$('.hide').show();
				} else {
					$(this).addClass('open');
					$(this).text('세부내용 보기');
					$('.hide').hide();
				}
			});

		});
	</script>
</head>
<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="product">
					<div class="inner">
						<div class="sub_tit">
							<h2>B2B제품</h2>
						</div>
						<div id="sub_nav">
							<div class="inner">
								<ul>
									<li class="home"><a href="/srm/">HOME</a></li>
									<li><a href="#">B2B 제품</a></li>
								</ul>
							</div>
						</div>
					</div>
<%
	cate = Request.QueryString("c")

	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
					<div class="pro_tab">
						<div class="inner">
							<ul>
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
								<li <% If Left(cate, 2) = Left(rs("c_code"), 2) Then %> class="on"<% End If %>><a href="product02.asp?c=<%=rs("c_code")%>"><i></i><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
								<!--li><a href="#"><i></i>디스플레이/TV</a></li>
								<li><a href="#"><i></i>화상회의</a></li>
								<li><a href="#"><i></i>복합기/프린터</a></li>
								<li><a href="#"><i></i>네트워크</a></li>
								<li><a href="#"><i></i>태블릿/모바일</a></li>
								<li><a href="#"><i></i>부품/소프트웨어</a></li>
								<li><a href="#"><i></i>가전</a></li-->
							</ul>
						</div>
					</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 6) = '000000' AND c_code LIKE '" & Left(cate, 2) & "%' AND SUBSTRING(c_code, 4, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
					<div class="inner">
						<div class="pro_tab_area">
							<ul class="sub_con_tab">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
								<li><a href="?c=<%=rs("c_code")%>#product_01"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
								<!--li><a href="#product_02">잉크젯</a></li>
								<li><a href="#product_03">레이저젯</a></li>
								<li><a href="#product_04">토너/잉크/드럼</a></li>
								<li><a href="#product_05">2차 카테고리</a></li>
								<li><a href="#product_06">2차 카테고리</a></li>
								<li><a href="#product_07">2차 카테고리</a></li>
								<li><a href="#product_08">2차 카테고리</a></li>
								<li><a href="#product_09">2차 카테고리</a></li>
								<li><a href="#product_10">2차 카테고리</a></li-->
							</ul>
							<button class="p_plus"><img src="/srm/images/sub/p_icon_plus.png"></button>
						</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 4) = '0000' AND c_code LIKE '" & Left(cate, 4) & "%' AND SUBSTRING(c_code, 6, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
						<div class="pro_tab_area pro_tab_area_hide">
							<ul class="sub_con_tab">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
									<li <% If cate = rs("c_code") Then %> class="active"<% End If %>><a href="product02.asp?c=<%=rs("c_code")%>"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
								<!--li><a href="#product_11">토너/잉크/드럼</a></li>
								<li><a href="#product_12">2차 카테고리</a></li>
								<li><a href="#product_13">2차 카테고리</a></li>
								<li><a href="#product_14">2차 카테고리</a></li>
								<li><a href="#product_15">2차 카테고리</a></li>
								<li><a href="#product_16">2차 카테고리</a></li-->
							</ul>
							<button class="p_plus"></button>
						</div>
						<div class="search_box">
							<form name="search_form" action="" method="post">
							<div>
								<select name="fieldname">
									<option value="g_name" <% If fieldname = "" Or fieldname = "g_name" Then %> selected<% End If %>>제품명</option>
									<option value="g_idx" <% If fieldname = "g_idx" Then %> selected<% End If %>>제품코드</option>
								</select>
								<input type="text" name="fieldvalue" value="<%=fieldvalue%>">
								<button class="btn_search" onclick="document.search_form.submit();">검색</button>
							</div>
							</form>
						</div>
						<div class="align_box">
							<button class="one" onclick="./location.href='product01.asp'">1단보기</button>
							<button class="two" onclick="./location.href='product02.asp'">2단보기</button>
							<select name="orderby">
								<option value="g_read">인기순</option>
								<option value="g_insertDay">최신순</option>
								<option value="g_Money_asc">낮은 가격순</option>
								<option value="g_Money_desc">높은 가격순</option>
							</select>
						</div>
<%
	If Len(fieldvalue) > 0 Then
		query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
	End If

	If cate <> "" Then
		If Right(cate, 8) = "00000000" Then
			query_where = query_where & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
		ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
			query_where = query_where & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
		ElseIf Mid(cate, 6, 1) <> 0 Then
			query_where = query_where & " AND a.g_category = '" & cate & "'"
		End If

		sql = "SELECT c_name FROM CATEGORY WHERE c_code = '" & cate & "'"
		Set rs = dbconn.execute(sql)

		If Not(rs.bof Or rs.eof) Then
			cateName = rs("c_name")
		End If

		rs.close

		sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y' " & query_where & ""
	Else
		cateName = "전체제품"
		sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y'"
	End If

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		TotalCnt = rs("CNT")
	End If

	rs.close
%>
						<div class="filter_set">
							<h4><%=cateName%> <span>(<%=TotalCnt%>)</span></h4>
							<!--ul>
								<li class="f_reset"><a href="javascript:;">필터재설정</a></li>
								<li class="li_close"><a href="javascript:;">필터재설정</a></li>
								<li class="li_close"><a href="javascript:;">필터재설정</a></li>
								<li class="li_close"><a href="javascript:;">필터재설정</a></li>
							</ul-->
							<button type="button" class="f_hide">세부내용 숨기기</button>
						</div>
					</div>
					<div class="tab_contents" id="product_02">
						<div class="inner">
							<div id="A_Container_L">
								<div class="lnb">
									<ul class="nav">
<%
	sql = "SELECT opt_name, opt_content FROM cate_option"
	Set rs = dbconn.execute(sql)

	If Not rs.eof Then
		i = 1
		Do While Not rs.eof
			opt_name = Trim(rs("opt_name"))
			opt_content = Trim(rs("opt_content"))

			arr_opt_content = Split(opt_content, ", ")
%>
										<li class="open">
											<a href="#"><span><%=opt_name%></span></a>
											<ul class="ss_menu">
												<li><a href="#.html">
													<div class="chk_box">
													<% For j = 0 To Ubound(arr_opt_content) %>
														<input id="fliter<%=j%>" type="checkbox" name="fliter<%=i%>" value="<%=opt_name%>||<%=arr_opt_content(j)%>">
														<label for="fliter<%=j%>"><span></span></label><%=arr_opt_content(j)%>
													<% Next %>
													</div>
												</a></li>
											</ul>
										</li>
<%
			i = i + 1
			rs.MoveNext
		Loop
	End If

	rs.close
%>
										<!--li>
											<a href="#"><span>필터명</span></a>
											<ul class="ss_menu">
												<li><a href="#.html">
														<div class="chk_box">
															<input id="fliter02" type="checkbox">
															<label for="fliter02"><span></span></label>
														</div>필터상세명
													</a></li>
												<li><a href="#.html">
														<div class="chk_box">
															<input id="fliter03" type="checkbox">
															<label for="fliter03"><span></span></label>
														</div>필터상세명
													</a></li>
												<li><a href="#.html">
														<div class="chk_box">
															<input id="fliter04" type="checkbox">
															<label for="fliter04"><span></span></label>
														</div>필터상세명
													</a></li>
											</ul>
										</li-->

									</ul>
								</div>
								<script type="text/javascript">
									$(document).ready(function() {
										$(".nav").navgoco({
											caretHtml: '',
											accordion: false,
											openClass: 'open',
											save: true,
											cookie: {
												name: 'navgoco',
												expires: false,
												path: '/'
											},
											slide: {
												duration: 400,
												easing: 'swing'
											},
										});
										$("#collapseAll").click(function(e) {
											e.preventDefault();
											$(".nav").navgoco('toggle', false);
										});

										$("#expandAll").click(function(e) {
											e.preventDefault();
											$(".nav").navgoco('toggle', true);
										});
									});

								</script>
							</div>
<%
Dim intTotalCount, intTotalPage
Dim intPageSize			: intPageSize 		= 4
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " a.g_idx, a.g_name, a.g_Money, a.g_simg, a.g_Memo "
Dim query_Tablename		: query_Tablename	= "GOODS a INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx"
Dim query_where			: query_where		= " a.g_type = 1 AND a.g_display <> 'home' AND a.g_act = 'Y'"

If Request.Cookies("SRM_LEVEL") <> 9 Then
	query_where = query_where & " AND b.mem_idx = " & intSeq
End If

If cate <> "" Then
	If Right(cate, 8) = "00000000" Then
		query_where = query_where & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
	ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
		query_where = query_where & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
	ElseIf Mid(cate, 6, 1) <> 0 Then
		query_where = query_where & " AND a.g_category = '" & cate & "'"
	End If
End If

If orderby = "" Then
	query_orderby = " ORDER BY a.g_idx DESC"
Else
	If orderby = "g_Money_asc" Then
		query_orderby = " ORDER BY a.g_Money ASC"
	ElseIf orderby = "g_Money_desc" Then
		query_orderby = " ORDER BY a.g_Money DESC"
	Else
		query_orderby = " ORDER BY a." & orderby & " DESC"
	End If
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
							<div id="A_Container_R" class="right_list">
							<form name="list" method="post" action="./product_compare.asp" style="margin:0;">
							<input type="hidden" name="n">
							<input type="hidden" name="m">
							<input type="hidden" name="message">
								<ul class="pro_list pro_list02">
<%
If rs.bof Or rs.eof Then
%>
									<li>등록된 제품이 없습니다.</li>
<%
Else
	i = 0
	Do While Not rs.eof
%>
									<li>
										<div class="a_box">
											<div class="chk_box">
												<input id="pro0<%=i%>" type="checkbox" value="<%=rs("g_idx")%>">
												<label for="pro0<%=i%>"><span></span></label>
											</div>
											<label for="prod"><span></span></label>
											<a href="/srm/online/product_estimate.asp?idx=<%=rs("g_idx")%>">견적요청</a>
											<a href="/srm/order/cart_add_ok.asp?idx=<%=rs("g_idx")%>">장바구니</a>
										</div>
										<div class="p_contents">
											<div class="img_box"><a href="./product_view.asp?idx=<%=rs("g_idx")%>"> <img src="/upload/goods/<%=rs("g_simg")%>" alt="<%=rs("g_name")%>"></a></div>
											<div class="cont_box">
												<p class="pro_tit"><%=rs("g_name")%></p>
												<span># <%=rs("g_idx")%></span>
												<span class="hide"><%=rs("g_Memo")%></span>
												<strong class="price"><%=FormatNumber(rs("g_Money"), 0)%> 원</strong>
											</div>
										</div>
									</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
									<!--li>
										<div class="a_box">
											<div class="chk_box">
												<input id="pro02" type="checkbox">
												<label for="pro02"><span></span></label>
											</div>
											<label for="prod"><span></span></label>
											<a href="product_estimate.html">견적요청</a>
											<a href="cart.html">장바구니</a>
										</div>
										<div class="p_contents">
											<div class="img_box"> <a href="product_view.html"> <img src="/srm/image/sub/product_img.jpg" alt=""></a></div>
											<div class="cont_box">
												<p class="pro_tit">제품명이 노출됩니다</p>
												<span># 1234567890</span>
												<p class="hide">내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다<br>
													내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다<br>
													내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다</p>
												<strong class="price">100,000 원</strong>
											</div>
										</div>
									</li>
									<li>
										<div class="a_box">
											<div class="chk_box">
												<input id="pro03" type="checkbox">
												<label for="pro03"><span></span></label>
											</div>
											<label for="prod"><span></span></label>
											<a href="product_estimate.html">견적요청</a>
											<a href="cart.html">장바구니</a>
										</div>
										<div class="p_contents">
											<div class="img_box"><a href="product_view.html"> <img src="/srm/image/sub/product_img.jpg" alt=""></a></div>
											<div class="cont_box">
												<p class="pro_tit">제품명이 노출됩니다</p>
												<span># 1234567890</span>
												<p class="hide">내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다<br>
													내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다<br>
													내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다 내용이 노출됩니다</p>
												<strong class="price">100,000 원</strong>
											</div>
										</div>
									</li-->
								</ul>
								</form>

								<% Call Paging_user_srm("") %>
								<!--div class="list_paging">
									<a class="paging_prev" href="#">처음</a>
									<ul>
										<li><a href="#">21</a></li>
										<li><a href="#">22</a></li>
										<li class="active"><a href="#">23</a></li>
										<li><a href="#">24</a></li>
										<li><a href="#">25</a></li>
									</ul>
									<a class="paging_next" href="#">맨끝</a>
								</div-->
							</div>
						</div>
					</div>
				</div>
				<!--div class="popup">
					<div class="pop_wrap">
						<div class="tit">
							<h3>제목이 노출됩니다</h3>
							<button class="btn_close">닫기</button>
						</div>
						<div class="v_box">
							<img src="/srm/image/sub/video_popup_img.jpg">
						</div>
					</div>
				</div-->
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->