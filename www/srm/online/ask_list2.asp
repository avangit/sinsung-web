<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(5)").addClass("active");
			$("#sdate").datepicker({
				showOn: "both",
				buttonImageOnly: false,
				buttonText: "날짜 선택",
				dateFormat: "yy-mm-dd"
			});
			$("#edate").datepicker({
				showOn: "both",
				buttonImageOnly: false,
				buttonText: "날짜 선택",
				dateFormat: "yy-mm-dd"
			});
		});

		function frmReset() {
				history.replaceState({}, null, location.pathname);
				$("#fieldname").val('');
				$("#fieldvalue").val('');
				$("#sdate").val('');
				$("#edate").val('');

		}

		function frmSubmit() {
			if($("#fieldvalue").val() == "" && $("#sdate").val() == "" && $("#edate").val() == "") {
				alert("검색어를 입력하세요");
				return false;
			} else {
				document.sch_Form.submit();
			}
		}
	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim dayday 		: dayday 		=  SQL_Injection(Trim(Request("d")))
Dim sdate 		: sdate 		=  SQL_Injection(Trim(Request("sdate")))
Dim edate 		: edate 		=  SQL_Injection(Trim(Request("edate")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "board_online"
Dim query_where			: query_where		= " on_type = 'etc'"
Dim query_orderby		: query_orderby		= " ORDER BY on_idx DESC"

If Request.Cookies("SRM_LEVEL") <> 9 Then
	query_where = query_where & " AND mem_idx = " & intSeq
End If

If dayday = "" Then
'	sdate = ""
'	edate = ""
ElseIf dayday = "7" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -7, Date)
	edate = Date
ElseIf dayday = "30" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -30, Date)
	edate = Date
ElseIf dayday = "365" And sdate = "" And edate = "" Then
	sdate = DateAdd("d", -365, Date)
	edate = Date
End If

If sdate <> "" And edate <> "" Then
	query_where = query_where & " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day, 1, '" & edate & "')"
ElseIf sdate <> "" And edate = "" Then
	query_where = query_where & " AND regdate >= '" & sdate & "'"
ElseIf sdate = "" And edate <> "" Then
	query_where = query_where & " AND regdate <= DATEADD(day, 1, '" & edate & "')"
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="order_list table_type01">
					<div class="inner">
						<div class="sub_tit">
							<h2>문의하기</h2>
						</div>


						<div class="order_tab_area">
							<ul class="sub_con_tab">
								<li><a href="ask_list.asp">기술문의</a></li>
								<li class="active"><a href="ask_list2.asp">기타문의</a></li>
							</ul>
						</div>

						<table class="search_table">
							<colgroup>
								<col style="width:120px">
								<col style="width:540px;">
								<col style="width:120px">
								<col style="width:400px">
							</colgroup>
							<tbody>
								<tr>
									<th>기간검색</th>
									<td>
										<div class="tab_btn">
											<button onclick="location.href='./ask_list2.asp'" <% If dayday = "" Then %>class="on"<% End If %>>전체</button>
											<button onclick="location.href='./ask_list2.asp?d=7'" <% If dayday = "7" Then %>class="on"<% End If %>>1주일</button>
											<button onclick="location.href='./ask_list2.asp?d=30'" <% If dayday = "30" Then %>class="on"<% End If %>>1개월</button>
											<button onclick="location.href='./ask_list2.asp?d=365'" <% If dayday = "365" Then %>class="on"<% End If %>>1년</button>
										</div>
							<form name="sch_Form" id="sch_Form" method="post">
										<div>
											<input type="text" name="sdate" id="sdate" class="date" value="<%=sdate%>" autocomplete="off"> -
											<input type="text" name="edate" id="edate" class="date" value="<%=edate%>" autocomplete="off">
										</div>
									</td>
									<th>조건검색</th>
									<td>
										<select name="fieldname" id="fieldname">
											<option value="on_title" <% If fieldname = "on_title" Then %> selected<% End If %>>제목</option>
											<option value="on_content" <% If fieldname = "on_content" Then %> selected<% End If %>>내용</option>
										<% If Request.Cookies("SRM_LEVEL") = 9 Then %>
											<option value="on_name" <% If fieldname = "on_name" Then %> selected<% End If %>>작성자</option>
											<option value="on_mobile" <% If fieldname = "on_mobile" Then %> selected<% End If %>>휴대폰번호</option>
											<option value="on_email" <% If fieldname = "on_email" Then %> selected<% End If %>>이메일</option>
										<% End If %>
											<input type="text" name="fieldvalue" id="fieldvalue" value="<%=fieldvalue%>" onkeypress="if( event.keyCode == 13 ){frmSubmit();return false;}">
										</select>
									</td>
								</tr>
							</tbody>
						</table>

						<div class="tab_contents" id="ask_02">

							<div class="btn_box">
								<button class="btn_wh" onclick="frmReset();">검색 초기화</button>
								<button class="btn_blue" onclick="frmSubmit();return false;">검색</button>
							</div>
							</form>

							<p class="cont"><span><%=intTotalCount%></span>건</p>
							<div class="btn_right">
								<button class="btn_blue" onclick="location.href='ask.asp'">문의하기</button>
							</div>
							<table class="table ask_list">

								<colgroup>
									<col style="width:80px">
									<col style="width:180px">
									<col style="width:500px">
									<col style="width:100px">
									<col style="width:150px">
									<col style="width:*">
								</colgroup>
								<thead>
									<tr>
										<th>번호</th>
										<th>분류</th>
										<th>제목</th>
										<th>담당자</th>
										<th>등록일</th>
										<th>답변여부</th>
									</tr>
								</thead>
								<tbody>
<%
If rs.bof Or rs.eof Then
%>
									<tr><td colspan="6" align="center">등록된 문의내역이 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
		status = rs("on_status")

		If status = "Y" Then
			status_txt = "답변완료"
		Else
			status_txt = "접수완료"
		End If
%>
									<tr>
										<td><%=intNowNum%></td>
										<td><%=rs("on_cate")%></td>
										<td class="ask_tit"  >
											<p class="over"><a href="ask_detail.asp?idx=<%=rs("on_idx")%>"><%=rs("on_title")%></a></p>
										</td>
										<td><%=rs("on_name")%></td>
										<td><%=Left(rs("regdate"), 10)%></td>
										<td><button class="<% If status = "Y" Then %>btn_wh reply_y<% Else %>btn_blue reply_n<% End If %>"><%=status_txt%></button></td>
									</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
								</tbody>
							</table>
						</div>

						<% Call Paging_user_srm("") %>

					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->