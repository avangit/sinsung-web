<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script type="text/javascript" src="/srm/js/member.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(5)").addClass("active");

			// 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function() {
				if (window.FileReader) {
					var filename = $(this)[0].files[0].name;
				} else {
					var filename = $(this).val().split('/').pop().split('\\').pop();
				}
				$(this).siblings('.upload_name').val(filename);
			});
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="ask">
					<div class="inner">
						<div class="sub_tit">
							<h2>문의하기</h2>
						</div>
						<div class="section_tit">
							<h4>기타 문의하기</h4>
						</div>
						<form name="onlineform" method="post" action="./ask_ok.asp" enctype="multipart/form-data">
						<table class="table table_type02">
							<colgroup>
								<col width="200px">
								<col width="*">
							</colgroup>

							<tbody>
								<tr>
									<th>분류 <span>*</span></th>
									<td>
										<select name="on_cate">
											<option value="">선택</option>
											<option value="주문관련">주문관련</option>
											<option value="배송문의">배송문의</option>
											<option value="거래명세표">거래명세표</option>
											<option value="세금계산서">세금계산서</option>
											<option value="기타">기타</option>
										</select>
									</td>
								</tr>
								<tr>
									<th>회사명 <span>*</span></th>
									<td><input type="text" name="on_company"></td>
								</tr>
								<tr>
									<th>담당자 <span>*</span></th>
									<td><input type="text" name="on_name"></td>
								</tr>
								<tr class="phone">
									<th>휴대폰 번호 <span>*</span></th>
									<td>
										<input type="text" name="on_mobile1" maxlength="4"> -
										<input type="text" name="on_mobile2" maxlength="4"> -
										<input type="text" name="on_mobile3" maxlength="4">
										<p><span>*</span> 답변 시 SMS 문자가 발송될 수 있습니다.</p>
									</td>
								</tr>
								<tr class="mail">
									<th>이메일 주소 <span>*</span></th>
									<td>
										<input type="text" name="on_email1"> @
										<input type="text" name="on_email2">
										<label>
											<select name="on_email3" id="on_email3" onchange="ChangeEmail3()">
												<option value="0">직접입력</option>
												<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												<option value="empal.com">empal.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="lycos.co.kr">lycos.co.kr</option>
												<option value="naver.com">naver.com</option>
												<option value="nate.com">nate.com</option>
												<option value="netsgo.com">netsgo.com</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="paran.com">paran.com</option>
											</select>
										</label>
										<p><span>*</span> 답변 시 메일이 발송될 수 있습니다.</p>
									</td>
								</tr>
								<tr>
									<th>제목 <span>*</span></th>
									<td><input type="text" name="on_title"></td>
								</tr>
								<tr>
									<th>내용 <span>*</span></th>
									<td><textarea name="on_content"></textarea></td>
								</tr>
								<tr>
									<th>첨부파일 <span class="sp_file">(20M 이하)</span></th>
									<td>
										<div class="file_box">
											<label for="on_file1">첨부파일</label>
											<input type="file" id="on_file1" name="on_file1" class="upload_hidden" style="display: none;">
											<input class="upload_name" disabled="disabled">
										</div>
										<div class="file_box">
											<label for="on_file2">첨부파일</label>
											<input type="file" id="on_file2" name="on_file2" class="upload_hidden" style="display: none;">
											<input class="upload_name" disabled="disabled">
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="btn_box ask_btn_box">
							<button class="btn_wh" type="button" onclick="location.href='ask_list2.asp'">취소</button>
							<button class="btn_blue" onclick="ask_chk();return false;">확인</button>
						</div>
						</form>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>