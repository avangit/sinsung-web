<!-- #include virtual="/srm/usercheck.asp" -->
<%
on_idx = SQL_Injection(Trim(Request.QueryString("idx")))

sql = "SELECT * FROM board_online WHERE on_idx = " & on_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	on_status = rs("on_status")
	on_cate = rs("on_cate")
	on_company = rs("on_company")
	on_name = rs("on_name")
	on_mobile = rs("on_mobile")
	on_email = rs("on_email")
	on_title = rs("on_title")
	on_content = rs("on_content")
	on_file1 = rs("on_file1")
	on_file2 = rs("on_file2")
	re_name = rs("re_name")
	re_content = rs("re_content")
	regdate = rs("regdate")
	replydate = rs("replydate")

	If on_status = "Y" Then
		status_txt = "답변완료"
	Else
		status_txt = "접수완료"
	End If
End If

rs.Close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(5)").addClass("active");
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="ask">
					<div class="inner">
						<div class="sub_tit">
							<h2>문의하기</h2>
						</div>
						<div class="section_tit">
						    <h4>기술 문의내역</h4>
						</div>

						<div class="info_view">
							<div class="tit_box">
								<p><%=on_title%></p>
								<p class="cate">분류: <%=on_cate%></p>
								<ul>
									<li>작성자: <%=on_name%></li>
									<li><%=on_company%></li>
									<li><%=on_mobile%> </li>
									<li><%=on_email%> </li>
									<li class="right">작성일 : <%=Left(regdate, 10)%></li>
								</ul>
								<span class="ing_box"><%=status_txt%></span>
							</div>
							<div class="content_box">
								<div class="contents">
									<%=Replace(on_content, Chr(13), "<br>")%>
								</div>
								<div class="file_box">
									<p>첨부파일</p>
									<% If on_file1 <> "" Then %>
									<span><a href="/download.asp?fn=<%=escape(on_file1)%>&ph=online"><%=on_file1%></a></span>
									<% End If %>
									<% If on_file2 <> "" Then %>
									<span><a href="/download.asp?fn=<%=escape(on_file2)%>&ph=online"><%=on_file2%></a></span>
									<% End If %>
								</div>
							</div>
						</div>

						<% If re_content <> "" Then %>
						<div class="section_tit">
							<h4>답변 내역</h4>
						</div>
						<div class="info_view answer">
							<div class="tit_box">
								<!--p>제목이 노출됩니다제목이 노출됩니다제목이 노출됩니다제목이 노출됩니다제목이 노출됩니다</p-->
								<ul>
									<li>작성자: <%=re_name%></li>
									<!--li class="bar">010-1234-5678</li>
									<li class="bar">Sample@email.co.kr</li-->
									<li class="right">작성일 : <%=Left(replydate, 10)%></li>
								</ul>
							</div>
							<div class="content_box">
								<div class="contents">
									<%=re_content%>
								</div>
								<!--div class="file_box">
									<p>첨부파일</p>
									<span>Sample1.jpg</span>
									<span>Sample2.jpg</span>
								</div-->
							</div>
						</div>
						<% End If %>

						<div class="btn_box">
							<button class="btn_blue" onClick="location.href='ask_list.asp'">목록</button>
						</div>

					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>