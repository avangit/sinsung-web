<!-- #include virtual="/srm/usercheck.asp" -->
<%
on_idx = SQL_Injection(Trim(Request.QueryString("idx")))

Call DbOpen()

If Request.Cookies("SRM_LEVEL") <> 9 Then
	query_where = query_where & " AND mem_idx = " & intSeq
End If

sql = "SELECT * FROM board_online WHERE on_idx = " & on_idx & query_where
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	on_status = rs("on_status")
	on_cate = rs("on_cate")
	on_company = rs("on_company")
	on_name = rs("on_name")
	on_mobile = rs("on_mobile")
	on_email = rs("on_email")
	on_title = rs("on_title")
	on_content = rs("on_content")
	on_file1 = rs("on_file1")
	on_file2 = rs("on_file2")
	re_name = rs("re_name")
	re_content = rs("re_content")
	regdate = rs("regdate")
	replydate = rs("replydate")
	p_idx = rs("p_idx")

	If on_status = "Y" Then
		status_txt = "답변완료"
	Else
		status_txt = "접수완료"
	End If
End If

rs.Close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />

	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(4)").addClass("active");
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="ask">
					<div class="inner">
						<div class="sub_tit">
							<h2>견적요청</h2>
						</div>
<%
If p_idx <> "" Then
	sql2 = "SELECT A.g_idx, A.g_name, A.g_Money, A.g_simg, ISNULL(b.mem_price, 0) AS mem_price FROM GOODS A INNER JOIN GOODS_SRM_JOIN b ON a.g_idx = b.g_idx WHERE a.g_idx IN (" & p_idx & ") AND B.mem_idx = '" & Request.Cookies("SRM_IDX") & "' ORDER BY a.g_idx DESC"
	Set rs2 = dbconn.execute(sql2)
%>
						<table class="table table_type03">
							<colgroup>
								<col style="width:600px">
								<col style="width:200px">
								<col style="width:200px">
							</colgroup>
							<thead>
								<tr>
									<th>상품정보</th>
									<th>상품금액</th>
									<th>수량</th>
								</tr>
							</thead>
							<tbody>
<%
	If Not (rs2.eof) Then
		Do While Not rs2.eof
%>
								<tr>
									<td>
										<div class="cart_div">
											<div class="img_box">
												<a href="#"><img src="/upload/goods/<%=rs2("g_simg")%>"></a>
											</div>
											<div class="txt_box">
												<a href="#" class="pro_code">제품코드 : #<%=rs2("g_idx")%></a>
												<p class="pro_name"><%=rs2("g_name")%></p><br>
											</div>
										</div>
									</td>
									<td class="price"><% If rs2("mem_price") = 0 Then %><%=FormatNumber(rs("g_Money"),0)%><% Else %><%=FormatNumber(rs2("mem_price"),0)%><% End If %>원</td>
									<td>1개</td>
								</tr>
<%
			rs2.MoveNext
		Loop
	End If
%>
							</tbody>
						</table>
<%
	rs2.close
	Set rs2 = Nothing

	Call DbClose()
End If
%>
						<div class="order_middle">
							<div class="section_tit">
							    <h4>견적 신청 내역</h4>
							</div>
						</div>
						<form>
							<div class="info_view">
								<div class="tit_box">
									<p><%=on_title%></p>
									<p class="cate"><% If on_cate <> "" Then %>분류: <%=on_cate%> <% End If %></p>
									<ul>
										<li>작성자: <%=on_name%></li>
										<li><%=on_company%></li>
										<li><%=on_mobile%> </li>
										<li><%=on_email%> </li>
										<li class="right">작성일 : <%=Left(regdate, 10)%></li>
									</ul>
									<span class="ing_box"><%=status_txt%></span>
								</div>
								<div class="content_box">
									<div class="contents">
										<%=on_content%>
									</div>
									<div class="file_box">
										<p>첨부파일</p>
									<% If on_file1 <> "" Then %>
										<span><a href="/download.asp?fn=<%=escape(on_file1)%>&ph=online" class="pdR20"><%=on_file1%></a></span>
									<% End If %>
									<% If on_file2 <> "" Then %>
										<span><a href="/download.asp?fn=<%=escape(on_file1)%>&ph=online" class="pdR20"><%=on_file2%></a></span>
									<% End If %>
									</div>
								</div>
							</div>
							</form>
						<% If re_content <> "" Then %>
							<div class="section_tit">
							    <h4>답변 내역</h4>
							</div>
							<div class="info_view answer">
								<div class="tit_box">
									<!--p>제목이 노출됩니다제목이 노출됩니다제목이 노출됩니다제목이 노출됩니다제목이 노출됩니다</p-->
									<ul>
										<!--li>작성자: 담당자</li>
										<li>010-1234-5678</li>
										<li>Sample@email.co.kr</li-->
										<li class="right">작성일 : <%=Left(replydate,10)%></li>
									</ul>
								</div>
								<div class="content_box">
									<div class="contents">
										<%=re_content%>
									</div>
									<!--div class="file_box">
										<p>첨부파일</p>
										<span>Sample1.jpg</span>
										<span>Sample2.jpg</span>
									</div-->
								</div>

							</div>
						<% End If %>
							<div class="btn_box pd0">
								<button class="btn_blue"  type="button" onClick="location.href='estimate_list.asp'">목록</button>
							</div>



					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->