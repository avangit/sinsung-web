<!-- #include virtual="/srm/usercheck.asp" -->
<%
If Request.Cookies("SRM_LEVEL") < 9 Then
'	Call jsAlertMsgBack("접근 권한이 없습니다.")
End If
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(6)").addClass("active");

			$(".date").datepicker({
				showOn: "both",
				buttonImageOnly: false,
				buttonText: "날짜 선택",
				dateFormat: "yy-mm-dd"
			});
//			$("#edate").datepidcker({
//				showOn: "both",
//				buttonImageOnly: false,
//				buttonText: "날짜 선택",
//				dateFormat: "yy-mm-dd"
//			});

			$("#serial_chg").click(function(){
				var frm = document.list;

				 var len = parseInt($("input[name^=s_user]").length);
				 $("input[name=arr_val_list]").val("");

				 if(!len || len==0) {
					alert('데이터가 없습니다.');
				 } else {
					var arr_val_list = new Array();
					var j = 0;

					$("input[name^=s_user]").each(function(i){
						arr_val_list[i] = $(this).attr("rel");
						j++;
					});

					$("input[name=arr_val_list]").val(arr_val_list.join('@'));

					if(!j || j==0) {
						$("input[name=arr_val_list]").val("");
					} else {
						document.list.submit();
					}
				 }
			});
		});

		function frmReset() {
			history.replaceState({}, null, location.pathname);
			$("#fieldname").val('');
			$("#fieldvalue").val('');
			$("#sdate").val('');
			$("#edate").val('');
		}

	</script>

</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim dayday 		: dayday 		=  SQL_Injection(Trim(Request("d")))
Dim sdate 		: sdate 		=  SQL_Injection(Trim(Request("sdate")))
Dim edate 		: edate 		=  SQL_Injection(Trim(Request("edate")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 10
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "SN"
If Request.Cookies("SRM_LEVEL") < 9 Then
Dim query_where			: query_where		= " masterId = '" & strId & "' "
End If
Dim query_orderby		: query_orderby		= " ORDER BY idx DESC"

'If dayday = "" Then
'	sdate = ""
'	edate = ""
''ElseIf dayday = "7" And sdate = "" And edate = "" Then
'	sdate = DateAdd("d", -7, Date)
'	edate = Date
'ElseIf dayday = "30" And sdate = "" And edate = "" Then
'	sdate = DateAdd("d", -30, Date)
'	edate = Date
'ElseIf dayday = "365" And sdate = "" And edate = "" Then
'	sdate = DateAdd("d", -365, Date)
'	edate = Date
'End If

'If sdate <> "" And edate <> "" Then
'	query_where = query_where & " AND regdate BETWEEN '" & sdate & "' AND DATEADD(day, 1, '" & edate & "')"
'ElseIf sdate <> "" And edate = "" Then'
'	query_where = query_where & " AND regdate >= '" & sdate & "'"
'ElseIf sdate = "" And edate <> "" Then
'	query_where = query_where & " AND regdate <= DATEADD(day, 1, '" & edate & "')"
'End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="serial table_type01 order_list">
					<div class="inner">
						<div class="sub_tit">
							<h2>시리얼 관리</h2>
						</div>

						<table class="search_table">
							<colgroup>
								<col style="width:120px">
								<col style="width:540px;">
								<col style="width:120px">
								<col style="width:400px">
							</colgroup>
							<tbody>
								<tr>
									<!--<th>기간검색</th>
									<td>
										<div class="tab_btn">
											<button onclick="location.href='./serial.asp'" <% If dayday = "" Then %> class="on"<% End If %>>전체</button>
											<button onclick="location.href='./serial.asp?d=7'" <% If dayday = "7" Then %> class="on"<% End If %>>1주일</button>
											<button onclick="location.href='./serial.asp?d=30'" <% If dayday = "30" Then %> class="on"<% End If %>>1개월</button>
											<button onclick="location.href='./serial.asp?d=365'" <% If dayday = "365" Then %> class="on"<% End If %>>1년</button>
										</div>
										<form name="sch_Form" id="sch_Form" method="post" action="">
										<div>
											<input type="text" id="sdate" name="sdate" class="date" value="<%=sdate%>" autocomplete="off"> -
											<input type="text" id="edate" name="edate" class="date" value="<%=edate%>" autocomplete="off">
										</div>
									</td>-->
									<th>조건검색</th>
									<td colspan="3">
										<select name="fieldname" id="fieldname" class="table_select_wd">
											<option value="key_order" <% If fieldname = "key_order" Then %> selected<% End If %>>주문번호</option>
											<option value="serial" <% If fieldname = "serial" Then %> selected<% End If %>>시리얼번호</option>
											<option value="username" <% If fieldname = "username" Then %> selected<% End If %>>사용자</option>
											<option value="partname" <% If fieldname = "partname" Then %> selected<% End If %>>사용부서</option>
											<input type="text" name="fieldvalue" id="fieldvalue" value="<%=fieldvalue%>" class="table_ip_wd">
										</select>
<!--										<button class="btn_blue table_search_btn" onclick="document.sch_Form.submit();">검색</button>-->
									</td>
								</tr>
							</tbody>
						</table>
						<div class="btn_box">
							<button class="btn_wh" onclick="frmReset();">검색 초기화</button>
							<button class="btn_blue" onclick="document.sch_Form.submit();">검색</button>
						</div>
						</form>

							<p class="cont"><span><%=intTotalCount%></span>건</p>

							<form name="list" method="post" action="./serial_exec.asp" style="margin:0;">
							<input type="hidden" name="n">
							<input type="hidden" name="arr_val_list">
							<input type="hidden" name="page" value="<%=intNowPage%>">
							<table class="table">

								<colgroup>
									<!--col style="width:60px"-->
									<col style="width:210px">
									<col style="width:210px">
									<col style="width:210px">
									<col style="width:210px">
									<col style="width:*">
									<col style="width:*">
								</colgroup>
								<thead>
									<tr>
										<!--th><div class="chk_box">
												<input id="serial_chk01" type="checkbox">
												<label for="serial_chk01"><span></span></label>
											</div></th-->
										<th>주문일자</th>
										<th>주문번호</th>
										<th>상품명</th>
										<th>시리얼번호</th>
										<th>사용자</th>
										<th>사용부서</th>
									</tr>
								</thead>
								<tbody>
<%
If rs.bof Or rs.eof Then
%>
									<tr><td colspan="6" align="center">판매완료된 내역이 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof


		If InStr(g_idx, ",") > 0 Then
			totalCnt = Ubound(Split(g_idx, ",")) + 1
		Else
			totalCnt = 1
		End If

		If totalCnt > 1 Then
			valCnt = "외 " & totalCnt - 1 & "건"
		End If

		strSQL = "select o_idx, o_payment from orders where o_num = '"&rs("key_order")&"'"
		Set orderRS = dbconn.execute(strSQL)

		If Not (orderRS.eof) Then
			orderNum = orderRS("o_idx")
			o_payment = orderRS("o_payment")
		End If
%>
									<tr>
										<!--td>
											<div class="chk_box">
												<input id="serial_chk02" type="checkbox">
												<label for="serial_chk02"><span></span></label>
											</div>
										</td-->
										<td><%=getAdoRsScalar("select regdate from orders where o_num = '"&rs("key_order")&"'")%></td>
									<% If o_payment = "yeosin" And Request.Cookies("SRM_LEVEL") <> 3 Then %>
										<td><p><a href="/srm/order/order_view.asp?idx=<%=orderNum%>"><%=rs("key_order")%></a></p></td>
									<% ElseIf o_payment <> "yeosin" Then %>
										<td><p><a href="/srm/order/order_view2.asp?idx=<%=orderNum%>"><%=rs("key_order")%></a></p></td>
									<% End If %>
										<td><p><%=rs("productName")%></p></td>

										<td><%=rs("serial")%></td>
										<td><input type="text" name="s_user<%=rs("idx")%>" value="<%=rs("username")%>" rel="<%=rs("idx")%>"></td>
										<td><input type="text" name="s_part<%=rs("idx")%>" value="<%=rs("partname")%>" rel="<%=rs("idx")%>"></td>

									</tr>
<%
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
								</tbody>
							</table>
						<div class="btn_box save_btn">
							<button class="btn_blue" id="serial_chg">저장</button>
							<button class="btn_bk" download>엑셀 다운로드</button>
						</div>
						</form>

						<% Call Paging_user_srm("") %>

					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>