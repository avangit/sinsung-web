<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />

	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:last-child").addClass("active");
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "BOARD_v1"
Dim query_where			: query_where		= " b_part = 'board01' AND display_mode <> 'home' "
Dim query_orderby		: query_orderby		= " ORDER BY option_notice DESC, b_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main info">
				<div class="inner">

					<div class="sub_tit">
						<h2>공지사항</h2>
					</div>
					<div class="search_box">
						<div>
							<!--select>
								<option>제목</option>
								<option>내용</option>
							</select>
							<input type="text">
							<button class="btn_search">검색</button-->
							<form name="search_form" action="" method="post">
								<select name="fieldname" class="pd_020">
									<option value="b_title" <% If fieldname = "" Or fieldname = "b_title" Then %> selected<% End If %>>제목</option>
									<option value="b_text" <% If fieldname = "b_text" Then %> selected<% End If %>>내용</option>
								</select>
								<input type="text" name="fieldvalue" value="<%=fieldvalue%>">
								<button class="btn_search" onclick="document.search_form.submit();">검색</button>
							</form>
						</div>

					</div>
					<form>
						<p class="cont"><span><%=intTotalCount%></span>건</p>
						<table class="table info_board">
							<colgroup>
								<col style="width: 80px">
								<col style="width: 600px">
								<col style="width: 80px">
								<col style="width: 120px">

							</colgroup>
							<thead>
								<tr>
									<th>번호</th>
									<th>제목</th>
									<th>조회수</th>
									<th>작성일</th>
								</tr>
							</thead>
							<tboby>
<%
If rs.bof Or rs.eof Then
%>
							<tr><td colspan="4" align="center">등록된 데이터가 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>
								<tr>
									<td>
									<% If rs("option_notice") = True Then %>
										<span>공지</span>
									<% Else %>
										<%=intNowNum%>
									<% End If %>
									</td>
									<td class="tit"><p class="over"><a href="./notice_view.asp?idx=<%=rs("b_idx")%>"><%=Cut(rs("b_title"),44,"...")%></p></td>
									<td class="num"><%=rs("b_read")%></td>
									<td class="num"><%=rs("b_writeday")%></td>
								</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
							</tboby>
						</table>

					<% Call Paging_user_srm("") %>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>
