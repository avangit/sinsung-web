<!-- #include virtual="/srm/usercheck.asp" -->
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(7)").addClass("active");

		});

		$(document).ready(function() {
			var posY;

			function bodyFreezeScroll() {
				posY = $(window).scrollTop();
				$("html").addClass('fix');
				$("html").css("top", -posY);
			}

			function bodyUnfreezeScroll() {
				$("html").removeAttr('class');
				$("html").removeAttr('style');
				posY = $(window).scrollTop(posY);
			}
			$('.thumb_img a, .video .video_area ul li a').click(function() {
				$('.popup').fadeIn();
				bodyFreezeScroll();

			});
			$('.popup .btn_close').click(function() {
				$('.popup').fadeOut();
				bodyUnfreezeScroll()

			});
		});

		function frmSubmit() {
			$("#sch_Form").submit();
		}

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim fieldvalue2 : fieldvalue2 	=  SQL_Injection(Trim(Request("fieldvalue2")))
Dim b_addtext5 	: b_addtext5 	=  SQL_Injection(Trim(Request("b_addtext5")))
Dim b_addtext6 	: b_addtext6 	=  SQL_Injection(Trim(Request("b_addtext6")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " GOODS.*, BOARD_v1.file_1, BOARD_v1.b_addtext5, BOARD_v1.b_addtext6  "
Dim query_Tablename		: query_Tablename	= "GOODS, BOARD_v1"
Dim query_where			: query_where		= " GOODS.g_type = 1 AND GOODS.g_display <> 'home' AND GOODS.g_act = 'Y' AND BOARD_v1.b_part = 'board07' AND GOODS.g_menual1 = BOARD_v1.b_idx"
Dim query_orderby		: query_orderby		= " ORDER BY GOODS.g_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND GOODS."& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

If b_addtext5 <> "" Then
	query_where = query_where & " AND BOARD_v1.b_addtext5 = '" & b_addtext5 & "'"
End If

If b_addtext6 <> "" Then
	query_where = query_where & " AND BOARD_v1.b_addtext6 = '" & b_addtext6 & "'"
End If

If fieldvalue2 <> "" Then
	query_where = query_where & " AND (BOARD_v1.b_addtext5 LIKE '%" & fieldvalue2 & "%' OR BOARD_v1.b_addtext6 = '" & fieldvalue2 & "' OR GOODS.g_name LIKE '%" & fieldvalue2 & "%')"
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div id="sub_contents" class="video">
					<div class="inner">
						<div class="sub_tit">
							<h2>IT 자료실</h2>
						</div>

						<div class="it_tab_area">
							<ul class="sub_con_tab">
								<li class="active"><a href="it.asp">제품설명서</a></li>
								<li><a href="it_manual.asp">기술지원매뉴얼</a></li>
								<li><a href="it_video.asp">제품영상</a></li>
							</ul>
						</div>

						<form>
						<div class="search_box">
							<div>
							<form name="sch_Form" id="sch_Form" method="POST" action="./it.asp">
								<select name="b_addtext5" id="b_addtext5">
									<option value="">&nbsp; ::: 제조사 :::</option>
									<option value="HP" <% If b_addtext5 = "HP" Then %> selected<% End If %>>&nbsp; HP</option>
									<option value="삼성" <% If b_addtext5 = "삼성" Then %> selected<% End If %>>&nbsp; 삼성</option>
									<option value="삼성 단납점 모델" <% If b_addtext5 = "삼성 단납점 모델" Then %> selected<% End If %>>&nbsp; 삼성 단납점 모델</option>
									<option value="LG" <% If b_addtext5 = "LG" Then %> selected<% End If %>>&nbsp; LG</option>
									<option value="HPE" <% If b_addtext5 = "HPE" Then %> selected<% End If %>>&nbsp; HPE</option>
									<option value="NETGEAR" <% If b_addtext5 = "NETGEAR" Then %> selected<% End If %>>&nbsp; NETGEAR</option>
									<option value="ATEN" <% If b_addtext5 = "ATEN" Then %> selected<% End If %>>&nbsp; ATEN</option>
								</select>
								<select name="b_addtext6" id="b_addtext6">
									<option value="">&nbsp; ::: 카테고리 ::: </option>
									<option value="데스크탑/서버" <% If b_addtext6 = "데스크탑/서버" Then %> selected<% End If %>>데스크탑/서버</option>
									<option value="디스플레이/TV" <% If b_addtext6 = "디스플레이/TV" Then %> selected<% End If %>>디스플레이/TV</option>
									<option value="화상회의" <% If b_addtext6 = "화상회의" Then %> selected<% End If %>>화상회의</option>
									<option value="복합기/프린터" <% If b_addtext6 = "복합기/프린터" Then %> selected<% End If %>>복합기/프린터</option>
									<option value="네트워크" <% If b_addtext6 = "네트워크" Then %> selected<% End If %>>네트워크</option>
									<option value="태블릿/모바일" <% If b_addtext6 = "태블릿/모바일" Then %> selected<% End If %>>태블릿/모바일</option>
									<option value="부품/소프트웨어" <% If b_addtext6 = "부품/소프트웨어" Then %> selected<% End If %>>부품/소프트웨어</option>
									<option value="가전" <% If b_addtext6 = "가전" Then %> selected<% End If %>>가전</option>
								</select>
								<input type="text" name="fieldvalue2" value="<%=fieldvalue2%>">
								<button class="btn_search" onclick="frmSubmit();">검색</button>
							</div>
						</div>
						</form>
						<div class="tab_contents" id="it_01">
						<table class="table info_board down_borad">
							<colgroup>
								<col style="width: 80px">
								<col style="width: 800px">
								<col style="width: 120px">
							</colgroup>
							<thead>
								<tr>
									<th>번호</th>
									<th>파일명</th>
									<th>다운로드</th>
								</tr>
							</thead>
							<tbody>
<%
If rs.bof Or rs.eof Then
%>
								<tr><td colspan="3" align="center">등록된 데이터가 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>

								<tr>
									<td><span><%=intNowNum%></span></td>
									<td class="tit"><p class="over"><a href="/srm/b2b/product_view.asp?idx=<%=rs("g_idx")%>" class="no_ic"><%=rs("g_name")%></a></p></td>
									<td><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download>다운로드</a></td>
								</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
								<!--tr>
									<td><span>4</span></td>
									<td class="tit"><p class="over">파일명이 노출됩니다</p></td>
									<td><a href="#" download>다운로드</a></td>
								</tr>
								<tr>
									<td><span>3</span></td>
									<td class="tit"><p class="over">파일명이 노출됩니다</p></td>
									<td><a href="#" download>다운로드</a></td>
								</tr>
								<tr>
									<td><span>2</span></td>
									<td class="tit"><p class="over">파일명이 노출됩니다</p></td>
									<td><a href="#" download>다운로드</a></td>
								</tr>
								<tr>
									<td><span>1</span></td>
									<td class="tit"><p class="over">파일명이 노출됩니다</p></td>
									<td><a href="#" download>다운로드</a></td>
								</tr-->
						</tbody></table>
                        </div>

						<% Call Paging_user_srm("") %>
					</div>
				</div>
				<!--div class="popup">
					<div class="pop_wrap">
						<div class="tit">
							<h3>제목이 노출됩니다</h3>
							<button class="btn_close">닫기</button>
						</div>
						<div class="v_box">
							<img src="/srm/image/sub/video_popup_img.jpg">
						</div>
					</div>
				</div-->
			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>

</html>