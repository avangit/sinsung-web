<!-- #include virtual="/srm/usercheck.asp" -->
<%
b_idx = SQL_Injection(Trim(Request.QueryString("idx")))

Call DbOpen()

sql = "UPDATE BOARD_v1 SET b_read = b_read + 1 WHERE b_idx = " & b_idx
dbconn.execute(sql)

sql = "SELECT * FROM BOARD_v1 WHERE b_idx = " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	b_title = rs("b_title")
	b_text = rs("b_text")
	file_1 = rs("file_1")
	file_2 = rs("file_2")
	file_3 = rs("file_3")
	b_read = rs("b_read")
	b_writeday = rs("b_writeday")
End If

rs.Close
%>
<!doctype html>
<html>

<head>
	<!-- #include virtual="/srm/_inc/head.asp" -->
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/srm/_css/sub.css" />

	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:last-child").addClass("active");
		});

	</script>
</head>

<body>
	<div id="A_Wrap">
		<div id="A_Header">
			<!-- #include virtual="/srm/_inc/header.asp" -->
		</div>
		<div id="A_Container_Wrap">
			<div id="A_Container" class="main">
				<div class="inner">

					<div class="sub_tit">
						<h2>공지사항</h2>
					</div>

					<div class="info_view">
						<div class="tit_box">
							<p><%=b_title%></p>
							<span>작성일 : <%=Replace(b_writeday, "-", ".")%></span>
							<span class="bar">조회수 : <%=b_read%></span>
						</div>
						<div class="content_box">
							<div class="contents">
								<p><%=b_text%></p>
							</div>
							<div class="file_box">
								<p>첨부파일</p>
								<span>
								<% If file_1 <> "" Then %>
									<a href="/download.asp?fn=<%=escape(file_1)%>&ph=avanboard_v3"><%=file_1%></a>
								<% End If %>
								<% If file_2 <> "" Then %>
									&nbsp;&nbsp;<a href="/download.asp?fn=<%=escape(file_2)%>&ph=avanboard_v3"><%=file_2%></a>
								<% End If %>
								<% If file_3 <> "" Then %>
									&nbsp;&nbsp;<a href="/download.asp?fn=<%=escape(file_3)%>&ph=avanboard_v3"><%=file_3%></a>
								<% End If %>
								</span>
							</div>
						</div>

						<div class="page_box">
<%
sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board01' AND display_mode <> 'home' AND b_idx < " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>

							<ul>
								<li>이전글</li>
								<li><a href="./notice_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a></li>
							</ul>
<%
End If

rs.Close

sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board01' AND display_mode <> 'home' AND b_idx > " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
							<ul>
								<li>다음글</li>
								<li><a href="./notice_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a></li>
							</ul>
						</div>
<%
End If

rs.Close
Set rs = Nothing
%>
					</div>
					<div class="btn_box">
						<button type="button" class="btn_list" onclick="location.href='./notice.asp'">목록</button>
					</div>
				</div>

			</div>
			<div id="A_Footer">
				<!-- #include virtual="/srm/_inc/footer.asp" -->
			</div>
		</div>

	</div>

</body>
</html>