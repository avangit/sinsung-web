
<script language="javascript">
function GetCookie(name){
	var nameOfCookie = name + "=";
	var x = 0;
	while(x<=document.cookie.length){
		var y = (x+nameOfCookie.length);
		if(document.cookie.substring(x, y) == nameOfCookie){
		if((endOfCookie=document.cookie.indexOf(";", y))==-1)
			endOfCookie = document.cookie.length;
			return unescape( document.cookie.substring( y, endOfCookie ) );
		}
		x = document.cookie.indexOf( " ", x ) + 1;
		if ( x == 0 ) break;
	}
	return "";
}

function SetCookie(name, value, expiredays){
	var todayDate = new Date();
	todayDate.setDate(todayDate.getDate() + expiredays);
	document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
}

function PopClose(idx, mode){
	if (mode=="1"){
		SetCookie("NOTICE"+idx, "done", 1);
	} else if (mode=="2"){
		SetCookie("NOTICE"+idx, "done", 365);
	}
	document.getElementById("pop"+idx).style.display="none";
}
</script>

<%
Dim rs, sql, pop_idx, pop_type, hide_type, scrollYN, pop_width, pop_height, pop_top, pop_left, pop_contents, htmlFile

Call DbOpen()

sql = "SELECT * FROM popup WHERE useYN = 'Y' AND sdate <= '"&Date()&"' and edate >= '"& Date()&"' AND display <> 'home' ORDER BY pop_idx DESC"
set rs = DBconn.execute(sql)

'Response.write sql

Do Until rs.EOF
	pop_idx  = rs("pop_idx")
	pop_type  = rs("pop_type")
	hide_type  = rs("hide_type")
	scrollYN  = rs("scrollYN")
	pop_width  = rs("pop_width")
	pop_height  = rs("pop_height")
	pop_top  = rs("pop_top")
	pop_left = rs("pop_left")
	pop_contents = rs("contents")
	htmlFile = rs("htmlFile")

	If pop_type = "layer" Then
%>

	<div class="pop-wrap" id="pop<%=pop_idx%>" style="left:<%=pop_left%>px; top:<%=pop_top%>px; z-index:<%=1000+pop_idx%>;">
		<div class="pop-content" style="width:<%=pop_width%>px; height:<%=pop_height%>px; overflow:hidden;"><%=pop_contents%></div>
		<% If hide_type <> "none" Then %>
		<ul class="pop-bottom">
		<% If hide_type="day" Then %>
			<li class="pb-left" onclick="PopClose(<%=pop_idx%>, 1)" style="cursor:pointer">
				오늘 하루 표시하지 않습니다.
		<% ElseIf hide_type="none" Then %>
			<li class="pb-left" onclick="PopClose(<%=pop_idx%>, 2)" style="cursor:pointer">
				이 창을 다시는 띄우지 않습니다.
		<%End If%></li>
			<li class="pb-right" onclick="PopClose(<%=pop_idx%>, 0)" style="cursor:pointer"><img src="/images/main/x.gif" class="vmiddle" alt="close"></li>
		</ul>
		<% End If %>
	</div>

	<script type="text/javascript">
	$(function() {
		$( "#pop<%=pop_idx%>" ).draggable();
	});
	if(GetCookie("NOTICE<%=pop_idx%>")!="done" ) {
		document.getElementById("pop<%=pop_idx%>").style.display = "block";
	}
	</script>
<%
	ElseIf pop_type = "win" Then
%>
	<script type="text/javascript">
	if(GetCookie("NOTICE<%=pop_idx%>")!="done" ) {
		window.open("/upload/popup/<%=htmlFile%>", "_blank", "toolbar=no,scrollbars=<%=scrollYN%>,resizable=no,top=<%=pop_top%>,left=<%=pop_left%>,width=<%=pop_width%>,height=<%=pop_height%>");
	}
	</script>
<%
	End If
	rs.Movenext
Loop

Set rs = Nothing

Call DbClose()
%>

<script type="text/javascript">
// 팝업 이미지 영역 자동맞춤
window.onload = function(){
	pop_size();
};
$(window).resize(function(){
	pop_size();
});
function pop_size(){
	$(".pop-content img").each(function(){
		var img_h = $(this).height();
		$(this).closest(".pop-content").css({
			height:img_h
		});
	});
};

</script>