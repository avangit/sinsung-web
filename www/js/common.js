$(document).on('ready', function() {
    // Header
    $("#A_Header").mouseover(function() {
        // $(this).addClass('on');
    });
    $("#A_Header").mouseleave(function() {
        // $(this).removeClass('on');
        // $('.lnb_wrap').stop().slideUp();
    });

    // PC 서브 메뉴
    // $(".gnb li").mouseover(function() {
    //     $(this).children('.lnb').slideDown();
    // });
    // $('.lnb').mouseleave(function() {
    //     $(this).stop().slideUp();
    //     $('.gnb > li').removeClass('active');
    // });
    // $(".gnb li").mouseleave(function() {
    //     $(this).children('.lnb').stop().slideUp();
    // });

    // $('.lnb > li').mouseover(function() {
    //     var activeLnb = $(this).attr('class');
    //     var lnbNum = activeLnb.substring(3,5);
    //     $('.gnb > li').removeClass('active');
    //     $('.gnb' + lnbNum).addClass('active');
    // });
    $(".gnb_area .gnb").bind("mouseenter focusin", function(){ 
        $("#underLayer0").addClass("on");
    }).bind("mouseleave focusout", function(){
        $("#underLayer0").removeClass("on");
    });

    $(".gnb_area .gnb > li").bind("mouseenter focusin", function(){ 
        $(".gnb_area .gnb > li").removeClass("on");
        $(this).find(".bg_blue").stop().slideDown("fast");
        // $("#underLayer0").addClass("on");
        $(this).addClass("on");
    }).bind("mouseleave focusout", function(){
        $(this).find(".bg_blue").stop().slideUp("fast");
        // $("#underLayer0").removeClass("on");
        $(this).removeClass("on");
    });

    $(".gnb01 .lnb_b2b").bind("mouseenter focusin", function(){ 
        $(".gnb01 > li").removeClass("on");
        $(this).find(".b2b_prd_menu").stop().slideDown("fast");
        $(this).addClass("on");
    }).bind("mouseleave focusout", function(){
        $(this).find(".b2b_prd_menu").stop().slideUp("fast");
        $(this).removeClass("on");
    });

    $(".b2b_prd_menu .depth01 > li").bind("mouseenter focusin", function(){ 
        $(".b2b_prd_menu .depth01 > li").removeClass("on");
        $(".b2b_prd_menu .depth02").fadeOut("fast");
        $(this).find(".depth02").stop().fadeIn("fast");
        $(this).addClass("on");
    }).bind("mouseleave focusout", function(){
        $(this).find(".depth02").stop().fadeOut("fast");
        $(this).removeClass("on");
    });

    $(".b2b_prd_menu .depth02 > li").bind("mouseenter focusin", function(){ 
        $(".b2b_prd_menu .depth02 > li").removeClass("on");
        $(".b2b_prd_menu .depth03").fadeOut("fast");
        $(this).find(".depth03").stop().fadeIn("fast");
        $(this).addClass("on");
    }).bind("mouseleave focusout", function(){
        $(this).removeClass("on");    
        $(this).find(".depth03").stop().fadeOut("fast");
    });


    // $(".b2b_prd_menu .depth01 > li").bind("click", function(){ 
    //     if($(this).hasClass('on')) {
    //         $(this).removeClass("on");    
    //         $(this).find(".depth02").stop().fadeOut("fast");
    //     } else {
    //         $(".b2b_prd_menu .depth01 > li").removeClass("on");
    //         $(".b2b_prd_menu .depth02").fadeOut("fast");
    //         $(this).find(".depth02").stop().fadeIn("fast");
    //         $(this).addClass("on");
    //     }
    // });


    // $(".b2b_prd_menu .depth02 > li").bind("click", function(){ 
    //     if($(this).hasClass('on')) {
    //         $(this).removeClass("on");    
    //         $(this).find(".depth03").stop().fadeOut("fast");
    //     } else {
    //         $(".b2b_prd_menu .depth02 > li").removeClass("on");
    //         $(".b2b_prd_menu .depth03").fadeOut("fast");
    //         $(this).find(".depth03").stop().fadeIn("fast");
    //         $(this).addClass("on");
    //     }
    // });


    // 스크롤 시 헤더
    window.onscroll = function(){
        if($(document).scrollTop() > 1){
            $("#A_Header").addClass("on");
            $("#A_Header").mouseleave(function() {
                $(this).addClass('on');
            });
        } else {
            $("#A_Header").removeClass("on");
            $("#A_Header").mouseleave(function() {
                $(this).removeClass('on');
                // $('.lnb_wrap').stop().slideUp(400);
            });
        }
    }

    // 검색
    $('#A_Header .util .btn_search').click(function(){
        $('.search_wrap').stop().slideDown();
    });
    $('.search_wrap .btn_close').click(function() {
        $('.search_wrap').stop().slideUp();
    });

});

function SetCookie(name, value, expireDays) {
	var todayDate = new Date();
	todayDate.setDate(todayDate.getDate() + expireDays);
	document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";";
}