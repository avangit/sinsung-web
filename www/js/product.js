$(document).ready(function(){
	//REMOVE
	$(".reBtn").click(function(){
		var idx = $(this).attr("rel");
		var cnt = $(this).attr("data");
		var n = new Array();
		var j = 0;

		if(cnt <= 1) {
			alert("제품비교는 최소 2개부터입니다.");
			return false;
		}

		$("input[name^=del_idx]").each(function(i){
			if($("input[name^=del_idx]").eq(i).val()!=idx && $("input[name^=del_idx]").eq(i).val()!=""){
				n[i] = $("input[name^=del_idx]").eq(i).val();
			}
			j++;
		});

		var arr_val = n.join(',');

		$("input[name=n]").val(arr_val);

		if(!j || j==0){ $("input[name=n]").val(""); }

		document.remove_Form.submit();
	});

	// 옵션필터
	$("input[name^=option]").click(function(){
		var option = new Array();

		$("input[name^=option]").each(function(i){
			option[i] = $(this).val();
		});

		var arr_option = option.join(',');
		document.filter_form.submit();
	});
});

// 체크박스 값 넘기기
function GetCheckbox(frm, mod, message) {
	var tmp = "";
	var cnt = 0;
	var i;
	var idk = false;

	for (i = 0; i < frm.length; i++) {
//		if (frm[i].type != "checkbox")
//			continue;
//		if (frm[i].checked) {

		if ($("input:checked[id='pro0" + i + "']").is(":checked")) {
			tmp += $("input:checked[id='pro0" + i + "']").val() + ",";
			cnt = cnt + 1;
			idk = true;
		}
	}

	if (idk == true) {
		if (mod.indexOf("del") != -1) {
			var cfm = confirm("선택하신 내용을 삭제하시겠습니까?");
		} else if (mod == "compare") {
			if (cnt < 2) {
				var cfm = confirm("비교할 대상은 최소 2개를 선택해 주세요");
				return false;
			} else {
				var cfm = confirm("선택하신 제품을 비교하시겠습니까?");
			}
		} else {
			if (message != null) {
				var cfm = confirm("선택하신 내용을 '" + message + "' 하시겠습니까?");
			} else {
				var cfm = confirm("선택하신 내용의 상태를 변경하시겠습니까?");
			}
		}

		if (cfm) {
			frm.n.value = tmp.substr(0, tmp.length - 1);
			frm.m.value = mod;
			frm.message.value = message;

			if(mod == "compare"){
				frm.action = "/sub/b2b/product_compare.asp";
			} else if(mod == "request"){
				frm.action = "/sub/b2b/onlineReceive_estimate.asp";
			}

			frm.submit();
		} else {
			return false;
		}
	} else {
		window.alert('실행할 제품을 선택하세요');
		return false;
	}
	return;
}