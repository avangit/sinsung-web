
<!DOCTYPE html>
<html lang="kr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

		<script type="text/javascript" src="https://www.eventservice.kr/js/jquery.min.js"></script>
		<link rel="stylesheet" href="/workstation/css/base_style.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="/workstation/css/style.css" type="text/css" media="screen" charset="utf-8" />

		<!-- 링크 공유 -->
		<meta property="og:type" content="website">
		<meta property="og:title" content="SINSUNG CNS">
		<meta property="og:url" content="https://www.eventservice.kr/2019/hpi/0718_sinsung/index.html">
		<meta property="og:description" content="여기를 눌러 자세한 내용을 확인하세요.">
		<meta property="og:image" content="/workstation/images/share-img.jpg">
		<title>SINSUNG CNS</title>
	</head>

	<body>


		<div id="wrap">
			<div class="header-area">

				<section id="header">

					<div class="inner">
						<div class="logo-area">
							<a href="/"><img src="/workstation/images/logo-sinsung.png" alt="SINSUNG CNS" /></a><a href=""><img src="/workstation/images/logo-hpi.png" alt="HPI" /></a>
						</div>

						<ul class="gnb">
							<li><a href="/sub/b2b/product01.asp?c=1001000000" target="_blank">워크스테이션</a></li>
							<li><a href="/sub/b2b/product01.asp?c=1001000000" target="_blank">노트북</a></li>
							<li><a href="/sub/b2b/product_list.asp?c=1002010000" target="_blank">데스크탑</a></li>
							<li><a href="/sub/experience/construction_case.php" target="_blank">구축사례</a></li>
							<li><a href="/sub/company/management.php" target="_blank">회사소개</a></li>
							<li><a href="/sub/support/onlineReceive.php" target="_blank">Contact US</a></li>
						</ul>

						<div id="mo-menu-trigger-close" class="mo-menu-trigger">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>

					<div class="m-gnb-bg"></div>
				</section>

				<section id="snb-area">
					<div class="inner">
						<ul class="snb">
							<li><a href="#Z240">Z240</a></li>
							<li><a href="#Z2">Z2</a></li>
							<li><a href="#Z4">Z4</a></li>
							<li><a href="#Z6">Z6</a></li>
							<li><a href="#Z8">Z8</a></li>
							<li><a href="#MINI">Z2 MINI</a></li>
							<li><a href="#ZBOOK">ZBOOK</a></li>
							<li><a href="#Display">HP Z Display</a></li>
						</ul>

						<p class="contact-us"><strong>문의</strong><img src="/workstation/images/icon-call.png" alt="전화번호" />02-867-3007 [영업전문가가 도와드리겠습니다]</p>

					</div>
				</section>
			</div>

			<section id="visual">
				<div class="inner">
					<img src="/workstation/images/logo-intel.png" alt="인텔® 제온® E프로세서" />
				</div>
			</section>

			<a name="Z240"></a>
			<section id="quarter">
				<div class="inner">
					<div class="quarter-area">
						<div><img src="/workstation/images/quarter-img.png" alt="quarter mark" /></div>
						<dl>
							<dt><h1><strong>34분기 연속, </strong>HP 최고등급 파트너 인증기업 (주)신성씨앤에스</h1></dt>
							<dd>신성CNS 홈페이지에서 지금 바로 구매하세요!</dd>
						</dl>
					</div>
				</div>
			</section>

			<section id="contents">
				<div class="inner">
					<div class="product">
						<h2><span>HP Z240 Workstation</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-z240.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP Z240 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z240 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® 프로세서 E3-1225 v5</span></li>
									<li><strong>OS</strong><span>Win 10 Pro 64 Downgrade Win 7 64</span></li>
									<li><strong>HDD</strong><span>1TB 7200 RPM SATA 1st Hard Drive</span></li>
									<li><strong>ODD</strong><span>9.5mm Slim SuperMulti DVDRW 1st ODD</span></li>
									<li><strong>메모리</strong><span>8GB DDR4-2133 ECC (1x8GB) Unbuffered</span></li>
									<li><strong>그래픽</strong><span>Intel® HD Graphics P530</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/onlineReceive_estimate.asp?idx=81&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP Z240 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z240 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® 프로세서 E3-1280 v5</span></li>
									<li><strong>OS</strong><span>Win 10 Pro 64 Downgrade Win 7 64</span></li>
									<li><strong>HDD</strong><span>1TB 7200 RPM SATA 1st Hard Drive</span></li>
									<li><strong>ODD</strong><span>9.5mm Slim SuperMulti DVDRW 1st ODD</span></li>
									<li><strong>메모리</strong><span>8GB DDR4-2133 ECC (1x8GB) Unbuffered</span></li>
									<li><strong>그래픽</strong><span>Intel® HD Graphics P530</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/onlineReceive_estimate.asp?idx=81&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
						<a name="Z2"></a>
					</div>
				</div>
			</section>

			<section id="contents" class="color-gray100">
				<div class="inner">
					<div class="product">
						<h2><span>HP Z2 G4 Workstation</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-z2-g4.png" alt="" /></div>
							<div class="pro-list">
								<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
								<h3>HP Z2 G4 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z2 G4 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® E-2104G 프로세서</span></li>
									<li><strong>OS</strong><span>Windows 10 Pro</span></li>
									<li><strong>HDD</strong><span>Z Turbo 256GB SSD</span></li>
									<li><strong>ODD</strong><span>Slim DVDWR</span></li>
									<li><strong>메모리</strong><span>8GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>Intel® UHD Graphics P630</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/onlineReceive_estimate.asp?idx=1476&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
								<h3>HP Z2 G4 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z2 G4 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® E-2144G 프로세서</span></li>
									<li><strong>OS</strong><span>Windows 10 Pro</span></li>
									<li><strong>HDD</strong><span>Z Turbo 256GB SSD/1TB HDD + 2TB HDD</span></li>
									<li><strong>ODD</strong><span>Slim DVDWR</span></li>
									<li><strong>메모리</strong><span>16GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P1000 4GB</span></li>
								</ul>
								<a name="Z4"></a><div class="btn-area"><a href="/sub/b2b/onlineReceive_estimate.asp?idx=1476&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
					</div>

					<div class="product">
						<h2><span>HP Z4 G4 Workstation</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-z4-g4.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP Z4 G4 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z4 G4 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® 프로세서 W-2125</span></li>
									<li><strong>OS</strong><span>Win10 Pro 64 Downgrade Win7 Pro 64</span></li>
									<li><strong>HDD</strong><span>1TB 7200 SATA HDD</span></li>
									<li><strong>SSD</strong><span>Z Turbo Drive M.2 512GB TLC SSD</span></li>
									<li><strong>ODD</strong><span>9.5mm 슬림 광 디스크 드라이브 베이</span></li>
									<li><strong>RAM</strong><span>64GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P4000</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/onlineReceive_estimate.asp?idx=1087&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP Z4 G4 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z4 G4 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® 프로세서 W-2133 </span></li>
									<li><strong>OS</strong><span>Win10 Pro 64 Downgrade Win7 Pro 64</span></li>
									<li><strong>HDD</strong><span>1TB 7200 SATA HDD</span></li>
									<li><strong>SSD</strong><span>Z Turbo Drive M.2 256GB TLC SSD</span></li>
									<li><strong>ODD</strong><span>9.5mm 슬림 광 디스크 드라이브 베이</span></li>
									<li><strong>RAM</strong><span>64GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P4000</span></li>
								</ul>
								<a name="Z6"></a><div class="btn-area"><a href="/sub/b2b/onlineReceive_estimate.asp?idx=1087&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
					</div>

					<div class="product">
						<h2><span>HP Z6 G4 Workstation</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-z6-g4.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP Z6 G4 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z6 G4 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® 프로세서 W-4114</span></li>
									<li><strong>OS</strong><span>Win10 Pro 64 Downgrade Win7 Pro 64</span></li>
									<li><strong>HDD</strong><span>1TB 7200 SATA HDD</span></li>
									<li><strong>SSD</strong><span>Z Turbo Drive M.2 256/512GB 2nd</span></li>
									<li><strong>ODD</strong><span>9.5mm 슬림 광 디스크 드라이브 베이</span></li>
									<li><strong>RAM</strong><span>32GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P2000/P2000 2nd</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1089&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP Z6 G4 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z6 G4 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® 프로세서 W-4116</span></li>
									<li><strong>OS</strong><span>Win10 Pro 64 Downgrade Win7 Pro 64</span></li>
									<li><strong>HDD</strong><span>1TB 7200 SATA HDD</span></li>
									<li><strong>SSD</strong><span>Z Turbo Drive M.2 256/512GB 2nd</span></li>
									<li><strong>ODD</strong><span>9.5mm 슬림 광 디스크 드라이브 베이</span></li>
									<li><strong>RAM</strong><span>32GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P2000/P2000 2nd</span></li>
								</ul>
								<a name="Z8"></a><div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1089&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
					</div>

					<div class="product">
						<h2><span>HP Z8 G4 Workstation</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-z8-g4.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP Z8 G4 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z8 G4 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® 프로세서 W-5118</span></li>
									<li><strong>OS</strong><span>Win10 Pro 64 Downgrade Win7 Pro 64</span></li>
									<li><strong>SSD</strong><span>Z Turbo Drive M.2 512GB TLC SSD</span></li>
									<li><strong>ODD</strong><span>9.5mm 슬림 광 디스크 드라이브 베이</span></li>
									<li><strong>RAM</strong><span>64GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P4000/P4000 2nd </span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1088&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP Z8 G4 Workstation</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z8 G4 Workstation</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® 프로세서 W-6130</span></li>
									<li><strong>OS</strong><span>Win10 Pro 64 Downgrade Win7 Pro 64</span></li>
									<li><strong>SSD</strong><span>2TB 7200 SATA HDD</span></li>
									<li><strong>ODD</strong><span>9.5mm 슬림 광 디스크 드라이브 베이</span></li>
									<li><strong>RAM</strong><span>32GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P2000/P2000 2nd/P2000 3rd</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1088&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
						<a name="MINI"></a>
					</div>
				</div>
			</section>

			<section id="contents">
				<div class="inner">
					<div class="product">
						<h2><span>HP Z2 Mini G4 Workstation</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-z2-mini-g4.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP Z2 Mini G4</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z2 Mini G4</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® E-2104G 프로세서</span></li>
									<li><strong>OS</strong><span>Linux-ready</span></li>
									<li><strong>HDD</strong><span>256G Z Turbo+1TB 2.5ʺ HDD</span></li>
									<li><strong>ODD</strong><span>N/A</span></li>
									<li><strong>메모리</strong><span>8GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P1000 4GB</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1708&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP Z2 Mini G4</h3>
								<ul>
									<li><strong>제품명</strong><span>HP Z2 Mini G4</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® E-2104G 프로세서</span></li>
									<li><strong>OS</strong><span>Windows 10 Pro</span></li>
									<li><strong>HDD</strong><span>512G Z Turbo Drive</span></li>
									<li><strong>ODD</strong><span>N/A</span></li>
									<li><strong>메모리</strong><span>16GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P1000 4GB</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1708&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
						<a name="ZBOOK"></a>
					</div>
				</div>
			</section>

			<section id="contents" class="color-gray100">
				<div class="inner">
					<div class="product">
						<h2><span>HP Zbook Mobile Workstation</span></h2>
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-zbook-moile1.png" alt="" /></div>
							<div class="pro-list">
								<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
								<h3>HP ZBOOK 15v G5</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ZBOOK 15v G5</span></li>
									<li><strong>CPU</strong><span>인텔® 코어™ i7-8750H 프로세서</span></li>
									<li><strong>OS</strong><span>Free Dos 2.0</span></li>
									<li><strong>HDD</strong><span>256GB SSD NVMe+1TB HDD</span></li>
									<li><strong>ODD</strong><span>N/A</span></li>
									<li><strong>메모리</strong><span>16GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P600 4G</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1709&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
								<h3>HP ZBOOK 15v G5</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ZBOOK 15v G5</span></li>
									<li><strong>CPU</strong><span>인텔® 코어™ i7-8750H 프로세서</span></li>
									<li><strong>OS</strong><span>Windows 10 Pro </span></li>
									<li><strong>HDD</strong><span>256GB SSD NVMe+1TB HDD</span></li>
									<li><strong>ODD</strong><span>N/A</span></li>
									<li><strong>메모리</strong><span>16GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P600 4G</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1709&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
					</div>

					<div class="product">
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-zbook-moile2.png" alt="" /></div>
							<div class="pro-list">
								<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
								<h3>HP ZBOOK 15 G5</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ZBOOK 15 G5</span></li>
									<li><strong>CPU</strong><span>인텔® 코어™ i7-8750H 프로세서</span></li>
									<li><strong>OS</strong><span>Windows 10 Pro</span></li>
									<li><strong>HDD</strong><span>256GB SSD NVMe+1TB HDD</span></li>
									<li><strong>ODD</strong><span>N/A</span></li>
									<li><strong>메모리</strong><span>8GB DDR4-26</span></li>
									<li><strong>그래픽</strong><span>P1000 4G</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1710&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
								<h3>HP ZBOOK 15 G5</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ZBOOK 15 G5</span></li>
									<li><strong>CPU</strong><span>인텔® 코어™ i7-8850H vPro™ 프로세서</span></li>
									<li><strong>OS</strong><span>Windows 10 Pro</span></li>
									<li><strong>HDD</strong><span>256GB SSD NVMe+1TB HDD</span></li>
									<li><strong>ODD</strong><span>N/A</span></li>
									<li><strong>메모리</strong><span>16GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P2000 4G</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1710&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
					</div>

					<div class="product">
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-zbook-moile3.png" alt="" /></div>
							<div class="pro-list">
								<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
								<h3>HP ZBOOK 17 G5</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ZBOOK 17 G5</span></li>
									<li><strong>CPU</strong><span>인텔® 제온® E-2176M 프로세서</span></li>
									<li><strong>OS</strong><span>Windows 10 Pro</span></li>
									<li><strong>HDD</strong><span>256GB SSD NVMe+1TB HDD</span></li>
									<li><strong>ODD</strong><span>N/A</span></li>
									<li><strong>메모리</strong><span>16GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P4200 8GB</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1710&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
								<h3>HP ZBOOK 17 G5</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ZBOOK 17 G5</span></li>
									<li><strong>CPU</strong><span>인텔® 코어™ i7-8850H 프로세서</span></li>
									<li><strong>OS</strong><span>Windows 10 Pro</span></li>
									<li><strong>HDD</strong><span>256GB SSD NVMe+1TB HDD</span></li>
									<li><strong>ODD</strong><span>N/A</span></li>
									<li><strong>메모리</strong><span>16GB DDR4-2666</span></li>
									<li><strong>그래픽</strong><span>P4200 8GB</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1711&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
					</div>

					<div class="product">
						<div class="pro-area">
							<div class="pro-img"><img src="/workstation/images/product-zbook-moile4.png" alt="" /></div>
							<div class="pro-list">
								<h3>HP ZBook x2 G4</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ZBook x2 G4</span></li>
									<li><strong>CPU</strong><span>인텔® 코어™ i7-8550 프로세서</span></li>
									<li><strong>OS</strong><span>Win10 Pro 64 Downgrade Win7 Pro 64</span></li>
									<li><strong>HDD</strong><span>256GB PCIe® NVMe™</span></li>
									<li><strong>ODD</strong><span>9.5mm 슬림 광 디스크 드라이브 베이</span></li>
									<li><strong>메모리</strong><span>8GB DDR4-2133</span></li>
									<li><strong>그래픽</strong><span>M620</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1096&part_idx=19&recom=1" target="_blank">맞춤 견적</a></div>
							</div>
							<div class="pro-list">
								<h3>HP ZBook x2 G4</h3>
								<ul>
									<li><strong>제품명</strong><span>HP ZBook x2 G4</span></li>
									<li><strong>CPU</strong><span>인텔® 코어™ i7-8250 프로세서</span></li>
									<li><strong>OS</strong><span>Win10 Pro 64 Downgrade Win7 Pro 64</span></li>
									<li><strong>HDD</strong><span>512GB PCIe® NVMe™</span></li>
									<li><strong>ODD</strong><span>9.5mm 슬림 광 디스크 드라이브 베이</span></li>
									<li><strong>메모리</strong><span>16GB DDR4-2133</span></li>
									<li><strong>그래픽</strong><span>M620</span></li>
								</ul>
								<div class="btn-area"><a href="/sub/b2b/estimate_form.php?idx=1096&part_idx=19&recom=2" target="_blank">맞춤 견적</a></div>
							</div>
						</div>
						<a name="Display"></a>
					</div>

				</div>
			</section>

			<section id="contents">
				<div class="inner">
					<h2><span>HP Z2 Mini G4 Workstation</span></h2>
					<ul class="pro-moniter">
						<li>
							<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
							<div class="pro-img"><img src="/workstation/images/product-hpz27.png" alt="" /></div>
							<dl>
								<dt>HP Z27</dt>
								<dd><strong>화면 사이즈</strong><span>27형 IPS</span></dd>
								<dd><strong>해상도</strong><span>4K UHD(3840x2160)</span></dd>
								<dd><strong>밝기</strong><span>350cd/㎡</span></dd>
								<dd><strong>포트</strong><span>1 DisplayPort™ 1.2<br />1 Mini DisplayPort™ 1.2<br />1 HDMI 2.0; 1 USB Type-C™</span></dd>
							</dl>
							<div class="btn-area"><a href="/sub/b2b/product_view.php?idx=1706&part_idx=7&s_o=Z27&o=1" target="_blank">맞춤 견적</a></div>
						</li>
						<li>
							<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
							<div class="pro-img"><img src="/workstation/images/product-hpz27n.png" alt="" /></div>
							<dl>
								<dt>HP Z27N</dt>
								<dd><strong>화면 사이즈</strong><span>27형 IPS</span></dd>
								<dd><strong>해상도</strong><span>QHD(2560x1440)</span></dd>
								<dd><strong>밝기</strong><span>350cd/㎡</span></dd>
								<dd><strong>포트</strong><span>1 DVI-D<br />1 HDMI<br />1 DisplayPort™ 1.2</span></dd>
							</dl>
							<div class="btn-area"><a href="/sub/b2b/product_view.php?idx=1705&part_idx=7&s_o=Z27&o=1" target="_blank">맞춤 견적</a></div>
						</li>
						<li>
							<div class="new-product"><img src="/workstation/images/new-product.png" alt="신제품" /></div>
							<div class="pro-img"><img src="/workstation/images/product-hp24i.png" alt="" /></div>
							<dl>
								<dt>HP24i</dt>
								<dd><strong>화면 사이즈</strong><span>24형 IPS/LED</span></dd>
								<dd><strong>해상도</strong><span>1920x1200</span></dd>
								<dd><strong>밝기</strong><span>300cd/㎡</span></dd>
								<dd><strong>포트</strong><span>1 DVI-D<br />1 VGA<br />1 DisplayPort™ 1.2</span></dd>
							</dl>
							<div class="btn-area"><a href="/sub/b2b/product_view.php?idx=1510&part_idx=7&s_o=1548910348&o=1&title=Z24i G2" target="_blank">맞춤 견적</a></div>
						</li>
					</ul>

					<div class="img-cut"><img src="/workstation/images/product-cut-img.jpg" alt="모니터 전경 사진" ></div>

					<h2 class="color-blue"><span>OVERVIEW</span></h2>
					<div class="overview-tit">
						<h3>IT 전문가로서 <br />기업의 올바른 업무환경 구현을 위해 <br /><strong>최고의 솔루션을 지속적으로 제공</strong>한다.</h3>
						<ul>
							<li>신뢰</li>
							<li>열정</li>
							<li>성장</li>
							<li>고객중심 서비스</li>
							<li>감사</li>
						</ul>
					</div>

					<ul class="overview-list">
						<li>
							<a href="/sub/support/service.asp" target="_blank">
								<img src="/workstation/images/overview-img01.jpg" />
								<dl>
									<dt>HP 공인서비스 지정점</dt>
									<dd>서비스 및 유지보수 지원 마스터 지원, 설치지원 빠른 기술상담 가능</dd>
								</dl>
							</a>
						</li>
						<li>
							<a href="/sub/support/service.asp" target="_blank">
								<img src="/workstation/images/overview-img02.jpg" />
								<dl>
									<dt>HP총판기업</dt>
									<dd>고객사의 구매비용 절감 제공</dd>
								</dl>
							</a>
						</li>
						<li>
							<a href="/sub/support/service.asp" target="_blank">
								<img src="/workstation/images/overview-img03.jpg" />
								<dl>
									<dt>맞춤형 세팅 서비스</dt>
									<dd>최첨단 대량 세팅 시스템, SW설치 지원, 시리얼 이력관리, 자산관리 QR코드</dd>
								</dl>
							</a>
						</li>
						<li>
							<a href="/sub/support/service.asp" target="_blank">
								<img src="/workstation/images/overview-img04.jpg" />
								<dl>
									<dt>재고운영</dt>
									<dd>PC재고운영을 통한 납기일 단축 및 적시 납품 가능</dd>
								</dl>
							</a>
						</li>
					</ul>

					<h2 class="color-blue"><span>인증현황</span></h2>
					<ul class="certi-list">
						<li>
							<a href="/sub/company/prize.asp" target="_blank">
								<img src="/workstation/images/certification-img01.jpg" />
								<dl>
									<dt>HP Gold Partner</dt>
									<dd>(2017.11.1 ~ 2018.4.30)</dd>
								</dl>
							</a>
						</li>
						<li>
							<a href="/sub/company/prize.asp" target="_blank">
								<img src="/workstation/images/certification-img02.jpg" />
								<dl>
									<dt>HP Gold Partner</dt>
									<dd>(2018.11.1 ~ 2019.4.30)</dd>
								</dl>
							</a>
						</li>
						<li>
							<a href="/sub/company/prize.asp" target="_blank">
								<img src="/workstation/images/certification-img03.jpg" />
								<dl>
									<dt>기업부설연구 인증서</dt>
									<dd>(2018)</dd>
								</dl>
							</a>
						</li>
						<li>
							<a href="/sub/company/prize.asp" target="_blank">
								<img src="/workstation/images/certification-img04.jpg" />
								<dl>
									<dt>ISO 9001</dt>
									<dd>(2018.12.24 ~ 2021.12.23)</dd>
								</dl>
							</a>
						</li>
						<li>
							<a href="/sub/company/prize.asp" target="_blank">
								<img src="/workstation/images/certification-img05.jpg" />
								<dl>
									<dt>MAIN BIZ</dt>
									<dd>(2019.3.20 ~ 2022.3.19)</dd>
								</dl>
							</a>
						</li>
					</ul>
				</div>
			</section>

			<section id="footer-logo">
				<ul class="inner">
					<li><img src="/workstation/images/logo-foo-intel.png" alt="인텔® 제온® E프로세서" /></li>
					<li><img src="/workstation/images/logo-foo-sinsung.png" alt="HP | SINSUNG" /></li>
				</ul>
			</section>

			<section id="footer">
				<div class="inner">
					<div class="foo-btn">
						<a href="/srm/login.asp" target="_blank"><img src="/workstation/images/footer-btn1.jpg" alt="전자구매시스템" /></a>
						<a href="https://939.co.kr/sinsungcns/" target="_blank"><img src="/workstation/images/footer-btn2.jpg" alt="원격 기술지원" /></a>
					</div>
					<div class="foo-txt">
						<p>
							<span>08389 서울시 구로구 디지털로 272 한신 IT타워 5F</span>
							<span>Tel: 02-867-3007</span>
							<span>Fax: 02-867-2328</span>
							<span>Email: <a href="mailto:sklee@sinsungcns.com" target="_blank">sklee@sinsungcns.com</a></span>
						</p>
						<p>
							<span>사업자 등록번호: 119-86-18434</span>
							<span>통신판매업 신고번호 제 2016-서울구로-0842호</span>
							<span>개인정보책임자: 박세화 <a href="/sub/member/safeguard.php?PHPSESSID=33cf009cb97b8fc97c7581dff61fbac1" target="_blank" class="foo-link">개인정보처리방침</a></span><br />
							홈페이지에 게제된 모든 글과 이미지 등의 정보는 저작권법에 따라 보호를 받는 ㈜신성씨앤에스의 저작물이므로 무단 전재나 복제를 금합니다. <br />COPYRIGHT (C)SINSUNGCNS CORP. ALL RIGHTS RESERVED.
						</p>
					</div>
				</div>
			</section>

		</div>

		<script>

		$(document).ready(function(){
			/* 모바일 메뉴 */
			$('.mo-menu-trigger').click(function(){
				$('.m-gnb-bg').toggleClass('active');
				$(this).toggleClass('active');
				$('.gnb').toggleClass('active');
				$('.tnb ul').toggleClass('active');
			})

			$('.m-gnb-bg').click(function(){
				$('.m-gnb-bg').removeClass('active');
				$('.mo-menu-trigger').removeClass('active');
				$('.gnb').removeClass('active');
				$('.tnb ul').removeClass('active');
			})

			$(window).resize(function(){

				winW = $(window).width()
				winH = $(window).height()

				/* 모바일 메뉴 */
				if(winW > 940){
					$('.gnb').css({
						height: 'auto'
					})

					$('.m-gnb-bg').css({
						height: 'auto'
					})

				}else if(winW < 960){

					$('.gnb').css({
						height: winH
					})

					$('.m-gnb-bg').css({
						height: winH
					})

				}else if(winW < 940){
					$('.gnb').click(function(){
						$('.m-gnb-bg').removeClass('active');
						$('.mo-menu-trigger').removeClass('active');
						$('.gnb').removeClass('active');
					})
				}

			}).resize()

			var naviOffset = $( '#header' ).offset();
			$( window ).scroll( function() {
				if ( $( document ).scrollTop() > naviOffset.top ) {
					$( '.header-area' ).addClass( 'header-fixed' );
				}else {
					$( '.header-area' ).removeClass( 'header-fixed' );
				}
			});

		})
		</script>

	</body>
</html>