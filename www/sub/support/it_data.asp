 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(3)").addClass('on');
 		});

 	</script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">고객서비스</a></li>
 					<li><a href="#">IT 자료실</a></li>
 				</ul>
 			</div>
 		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim b_addtext5 	: b_addtext5 	=  SQL_Injection(Trim(Request("b_addtext5")))
Dim b_addtext6 	: b_addtext6 	=  SQL_Injection(Trim(Request("b_addtext6")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " GOODS.*, BOARD_v1.file_1, BOARD_v1.b_addtext5, BOARD_v1.b_addtext6  "
Dim query_Tablename		: query_Tablename	= "GOODS, BOARD_v1"
Dim query_where			: query_where		= " GOODS.g_type = 1 AND GOODS.g_display <> 'srm' AND GOODS.g_act = 'Y' AND BOARD_v1.b_part = 'board07' AND GOODS.g_menual1 = BOARD_v1.b_idx "
Dim query_orderby		: query_orderby		= " ORDER BY GOODS.g_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND GOODS.g_name LIKE '%" & fieldvalue & "%' "
End If

If b_addtext5 <> "" Then
	query_where = query_where & " AND BOARD_v1.b_addtext5 = '" & b_addtext5 & "'"
End If

If b_addtext6 <> "" Then
	query_where = query_where & " AND BOARD_v1.b_addtext6 = '" & b_addtext6 & "'"
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
 				<div class="title">
 					<h3>IT 자료실</h3>
 				</div>
 				<div id="content" class="videoCon case">
 					<ul id="tabMenu_s2" class="mb30">
 						<li rel="m2" onclick="location.href='./it_data2.asp'">기술지원메뉴얼</li>
 						<li rel="m1" class="active">제품설명서</li>
 						<li rel="m3" onclick="location.href='./it_data3.asp'">제품영상</li>
 					</ul>
 					<div class="sch it_sch clfix">
 						<form name="sch_Form" method="POST" action="./it_data.asp">
 							<select name="b_addtext5" id="b_addtext5">
 								<option value="">&nbsp; ::: 제조사 :::</option>
 								<option value="HP" <% If b_addtext5 = "HP" Then %> selected<% End If %>>&nbsp; HP</option>
 								<option value="삼성" <% If b_addtext5 = "삼성" Then %> selected<% End If %>>&nbsp; 삼성</option>
 								<option value="삼성 단납점 모델" <% If b_addtext5 = "삼성 단납점 모델" Then %> selected<% End If %>>&nbsp; 삼성 단납점 모델</option>
 								<option value="LG" <% If b_addtext5 = "LG" Then %> selected<% End If %>>&nbsp; LG</option>
 								<option value="HPE" <% If b_addtext5 = "HPE" Then %> selected<% End If %>>&nbsp; HPE</option>
 								<option value="NETGEAR" <% If b_addtext5 = "NETGEAR" Then %> selected<% End If %>>&nbsp; NETGEAR</option>
 								<option value="ATEN" <% If b_addtext5 = "ATEN" Then %> selected<% End If %>>&nbsp; ATEN</option>
 							</select>

 							<select name="b_addtext6" id="b_addtext6">
 								<option value="">&nbsp; ::: 카테고리 ::: </option>
								<option value="데스크탑/서버" <% If b_addtext6 = "데스크탑/서버" Then %> selected<% End If %>>데스크탑/서버</option>
								<option value="디스플레이/TV" <% If b_addtext6 = "디스플레이/TV" Then %> selected<% End If %>>디스플레이/TV</option>
								<option value="화상회의" <% If b_addtext6 = "화상회의" Then %> selected<% End If %>>화상회의</option>
								<option value="복합기/프린터" <% If b_addtext6 = "복합기/프린터" Then %> selected<% End If %>>복합기/프린터</option>
								<option value="네트워크" <% If b_addtext6 = "네트워크" Then %> selected<% End If %>>네트워크</option>
								<option value="태블릿/모바일" <% If b_addtext6 = "태블릿/모바일" Then %> selected<% End If %>>태블릿/모바일</option>
								<option value="부품/소프트웨어" <% If b_addtext6 = "부품/소프트웨어" Then %> selected<% End If %>>부품/소프트웨어</option>
								<option value="가전" <% If b_addtext6 = "가전" Then %> selected<% End If %>>가전</option>
 							</select>

 							<input type="text" name="fieldvalue" value="<%=fieldvalue%>" placeholder="검색어를 입력하세요.">
 							<a href="javascript:;" onclick="document.sch_Form.submit();">검색</a>
 						</form>
 					</div>

 					<table id="empTable">
 						<caption class="blind"> 게시판</caption>
 						<colgroup>
 							<col width="10%">
 							<col width="*">
 							<col width="45%">
 						</colgroup>
 						<tbody>
 							<tr>
 								<th>번호</th>
 								<th>제품명</th>
 								<th>다운로드</th>
 							</tr>
<%
If rs.bof Or rs.eof Then
%>
							<tr><td colspan="3" align="center">등록된 데이터가 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>
 							<tr>
 								<td><%=intNowNum%></td>
 								<td class="left">
 									<a href="/sub/b2b/product_view.asp?idx=<%=rs("g_idx")%>"><%=rs("g_name")%></a>
 								</td>

 								<td class="taL">
 									<p><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3"><i class="fa fa-download" aria-hidden="true"></i> <%=rs("file_1")%> </a></p>
 								</td>

 							</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 						</tbody>
 					</table>

					<%call Paging_user("")%>
 					<!--div class="page">
 						<ul class="clfix">
 							<li><a href="#">&lt;&lt;</a></li>
 							<li class="on"><a href="#">1</a></li>
 							<li><a href="#">2</a></li>
 							<li><a href="#">3</a></li>
 							<li><a href="#">4</a></li>
 							<li><a href="#">5</a></li>
 							<li><a href="#">6</a></li>
 							<li><a href="#">7</a></li>
 							<li><a href="#">8</a></li>
 							<li><a href="#">9</a></li>
 							<li><a href="#">10</a></li>
 							<li><a href="#">&gt;</a></li>
 							<li><a href="#">&gt;&gt;</a></li>
 						</ul>
 					</div-->
 				</div>
 			</div>
			<div id="A_Container_R">
				<h3 class="sub_tit">고객서비스</h3>
				<!-- #include virtual="/_inc/right_service.asp" -->
				<!-- #include virtual="/_inc/right_bnn.asp" -->
			</div>

<!-- #include virtual="/_inc/_footer.asp" -->