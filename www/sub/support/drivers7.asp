 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<script type="text/javascript">
	$(document).on('ready', function() {
    	$(".sub_lnb li:nth-child(1)").addClass('on');

    	$(".faqUl li label").click(function(){
			if ($(this).closest("li").hasClass("on"))
			{
				$(this).next("div").stop().slideUp("fast");
				$(this).closest("li").removeClass("on");
			} else{
				$(".faqUl li").removeClass("on");
				$(".faqUl li div").stop().slideUp("fast");
				$(this).next("div").stop().slideDown("fast");
				$(this).closest("li").addClass("on");
			}
		});
		$(".content-tab ul li a").click(function(){
			var tabIdx = $(this).parent().index();
			$(".content-tab ul li").removeClass("active");
			$(this).parent().addClass("active");
			$(".drivers7 .content-wrap .content").hide();
			$(".drivers7 .content-wrap .content").eq(tabIdx).hide().fadeIn();
		});
    });
	</script>
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
	<!-- 서브 네비게이션 -->
	<div id="sub_nav">
		<div class="inner">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="#">고객서비스</a></li>
				<li><a href="#">신성케어 7drivers</a></li>
			</ul>
		</div>
	</div>
	<div id="A_Container">
		<!-- 서브컨텐츠 -->
		<div id="A_Container_C">
			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>신성케어 7drivers</h3>
				<a href="https://pf.kakao.com/_kxipdT" target="_blank"><img src="/images/sub/driver-icon01.png" alt="멤버십 인증서 신청">멤버십 인증서 신청</a>
			</div>
			<div id="content" class="drivers7">
				<img src="/images/sub/driver7-img01.jpg" alt="이미지">
				<dl>
					<dt class="cont_tit">Why?</dt>
					<dd>
						<p>고객의 OS 설치와 조립 없는 전산 업무환경! IT 구매와 관리로 낭비되는 업무시간을 최소화하기 위한 고객사 맞춤 구매시스템 SRM!<br>
						어떻게 하면 전산총무 업무 시간을 줄이고 고객 업무의 질을 향상시킬 수 있을까? 고민하여 출시한 서비스가 "신성 케어 7drivers"입니다.</p>
					</dd>
				</dl>
				<dl>
					<dt class="cont_tit">전산 하드웨어 구매 시, 아래 서비스를 제대로 받고 있습니까?</dt>
					<dd>
						<ul>
							<li class="check">사전 부품 추가, OS 탑재 서비스</li>
							<li>자산 이력 관리 및 추적 가능한 시스템</li>
							<li>긴급 기술 문의, 장애 지원 서비스</li>
							<li class="check">무료 택배 발송/회수 서비스</li>
							<li>업무에 필요한 설치 매뉴얼 제공받기 </li>
							<li>구매 전, 데모 장비 신청 서비스</li>
							<li>원클릭으로 발주하는 맞춤형 앱 플랫폼</li>
						</ul>
					</dd>
				</dl>
			<div class="inner">
				<div class="content-tab">
					<ul>
						<li class="active">
							<a href="javascript:;">맞춤형 제품 제공</a>
						</li>
						<li>
							<a href="javascript:;">기술 및 장애 처리</a>
						</li>
						<li>
							<a href="javascript:;">기술자료</a>
						</li>
						<li>
							<a href="javascript:;">맞춤형 구매시스템</a>
						</li>
						<li>
							<a href="javascript:;">자산이력관리</a>
						</li>
						<li>
							<a href="javascript:;">무료배송</a>
						</li>
						<li>
							<a href="javascript:;">데모장비지원</a>
						</li>
					</ul>
				</div>
				<div class="content-wrap">
					<div class="content">
						<div class="line">
							<p class="tit">Driver 1. 맞춤형 제품 제공</p>
							<p class="sub-txt">기업의 요구 사항에 맞는 하드웨어를 구성하고, 필요한 소프트웨어를 설치하는 <b>맞춤형 세팅 서비스</b>를 제공합니다.  <br>고객은 별다른 설정 없이 구입한 장비를 즉시 배포할 수 있어 <b>시간과 비용을 줄일 수 있습니다.</b></p>
							<ul class="type01">
								<li>
									<img src="/images/sub/driver7-img02.jpg" alt="이미지">
									<p>추가 부품 장착(고객 요청 시)</p>
								</li>
								<li>
									<img src="/images/sub/driver7-img03.jpg" alt="이미지">
									<p>최고 수준의 대량 세팅 시스템</p>
								</li>
								<li>
									<img src="/images/sub/driver7-img04.jpg" alt="이미지">
									<p>BIOS 설정 및 변경</p>
								</li>
								<li>
									<img src="/images/sub/driver7-img05.jpg" alt="이미지">
									<p>마스터 이미지 로딩(최첨단 장비)</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="line">
							<p class="tit">Driver 2. 기술 및 장애 처리</p>
							<p class="sub-txt">제조사의 보증 서비스와는 별개로 신속한 기술 지원 및 긴급 장애 대응으로 업무 손실을 최소화하기 위해<br><b>전담 엔지니어 콜센터를 운영</b>합니다. 게시판 플랫폼을 통해서 <b>실시간으로 처리 상태</b>를 확인하실 수 있습니다. </p>
							<ul class="type02">
								<li>
									<img src="/images/sub/driver7-img13.jpg" alt="이미지">
									<p>원격지원</p>
								</li>
								<li>
									<img src="/images/sub/driver7-img14.jpg" alt="이미지">
									<p>카카오톡</p>
								</li>
								<li>
									<img src="/images/sub/driver7-img15.jpg" alt="이미지">
									<p>엔지니어 콜센터</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="line">
							<p class="tit">Driver 3. 기술자료</p>
							<p class="sub-txt"><b>필요한 설치 매뉴얼을 제공</b>함으로써 기업 전산 운영의 효율성을 높입니다. 다양한 기업 환경에 따른 <b>구축 사례를 제공</b>하고,<br>소프트웨어에 맞는 하드웨어 <b>성능 테스트(BMT),</b> 신제품 등 꼭 필요한 정보를 제공합니다.</p>
							<img src="/images/sub/driver7-img16.jpg" alt="이미지">
						</div>
					</div>
					<div class="content">
						<div class="line">
							<p class="tit">Driver 4. 맞춤형 구매시스템</p>
							<p class="sub-txt"><b>고객 맞춤 솔루션과 제품 표준화</b>로 구매 프로세스를 간소화하여 시간과 비용을 절감 시킵니다. 거래명세서, 견적서, 구매 이력 조회,<br>전자 구매 승인 시스템, 여신 결제를 갖춘 <b>원 클릭 구매 시스템</b>입니다.</p>
							<img src="/images/sub/driver7-img17.jpg" alt="이미지">
						</div>
					</div>
					<div class="content">
						<div class="line">
							<p class="tit">Driver 5. 자산이력관리</p>
							<p class="sub-txt">효과적인 PC 자산 관리를 위한 <b>QR 라벨</b>을 장비에 부착하여 사양 및 유저 정보를 확인 가능합니다. SRM에서는 고객이<br><b>별다른 전산 입력 작업없이</b> 주문 제품의 입고일자, 시리얼번호, 사용자 등의 <b>자산관리가 가능</b>합니다.</p>
							<ul class="type01">
								<li>
									<img src="/images/sub/driver7-img18.jpg" alt="이미지">
									<p>제품준비</p>
								</li>
								<li>
									<img src="/images/sub/driver7-img19.jpg" alt="이미지">
									<p>출고 정보 바코드 생성</p>
								</li>
								<li>
									<img src="/images/sub/driver7-img20.jpg" alt="이미지">
									<p>바코드 출력</p>
								</li>
								<li>
									<img src="/images/sub/driver7-img21.jpg" alt="이미지">
									<p>바코드 부착 및 정보 대조</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="content">
						<div class="line">
							<p class="tit">Driver 6. 무료배송</p>
							<p class="sub-txt">MOT 교육을 이수한 엔지니어가 직접 납품 및 설치를 합니다. 고객 요구에 따라 택배, 퀵, 화물 등의 다양한 배송 방법으로<br><b>무료 발송 및 회수 픽업 서비스</b>를 운영 중입니다.</p>
							<img src="/images/sub/driver7-img22.jpg" alt="이미지">
						</div>
					</div>
					<div class="content">
						<div class="line">
							<p class="tit">Driver 7. 데모장비지원</p>
							<p class="sub-txt">장비 도입을 위한 테스트 또는 시연이 필요하신 경우에는 <b>데모 장비를 무상으로 지원합니다. 또한 테스트를 의뢰하실 경우<br>지원 가능합니다. 신청 절차는 홈페이지 게시판, 카카오톡 플러스에서 하실 수 있습니다.</b></p>
							<img src="/images/sub/driver7-img23.jpg" alt="이미지">
						</div>
					</div>
				</div>
				<!-- 공통 -->
				<div class="common">
					<dl>
						<dt>신성 케어 7drivers! 고객 구축사례</dt>
						<dd>
							<ul>
								<li>
									<img src="/images/sub/driver7-img06.jpg" alt="이미지">
								</li>
								<li>
									<img src="/images/sub/driver7-img07.jpg" alt="이미지">
								</li>
								<li>
									<img src="/images/sub/driver7-img08.jpg" alt="이미지">
								</li>
								<li>
									<img src="/images/sub/driver7-img09.jpg" alt="이미지">
								</li>
								<li>
									<img src="/images/sub/driver7-img10.jpg" alt="이미지">
								</li>
								<li>
									<img src="/images/sub/driver7-img11.jpg" alt="이미지">
								</li>
							</ul>
							<a href="/sub/experience/construction_case.asp">더보기</a>
						</dd>
					</dl>
					<div id="faqCon">
						<h3 id="goB">
							<img src="/images/sub/driver7-img12.jpg" alt="이미지">
						</h3>
						<ul class="faqUl">
							<li>
								<label class="jq_toggle tit">
									<span class="tit">Q.</span>
									<span class="txt">신성 케어 7drivers는 무엇입니까?</span>
								</label>
								<div class="jq_hide">
									<span class="tit">A.</span>
									<span class="txt">신성씨앤에스의 멤버십 고객에게 무료로 지원되는 서비스입니다.</span>
								</div>
							</li>
							<li>
								<label class="jq_toggle tit">
									<span class="tit">Q.</span>
									<span class="txt">인증서 발송 신청 절차는 어떻게 됩니까?</span>
								</label>
								<div class="jq_hide">
									<span class="tit">A.</span>
									<span class="txt">카카오톡에서 ‘신성씨앤에스’ 친구 추가하시어 ‘신청하셨나요? 멤버십 인증서!’를 클릭해 주시면 됩니다.</span>
								</div>
							</li>
							<li>
								<label class="jq_toggle tit">
									<span class="tit">Q.</span>
									<span class="txt">멤버십 고객의 기준은 무엇입니까?</span>
								</label>
								<div class="jq_hide">
									<span class="tit">A.</span>
									<span class="txt">신성씨앤에스와 1회 이상 거래가 있는 기업 고객입니다.</span>
								</div>
							</li>
							<li>
								<label class="jq_toggle tit">
									<span class="tit">Q.</span>
									<span class="txt">최소 수량의 제한이 있습니까?</span>
								</label>
								<div class="jq_hide">
									<span class="tit">A.</span>
									<span class="txt">아닙니다. 수량의 제한은 없습니다.
								</span>
								</div>
							</li>
							<li>
								<label class="jq_toggle tit">
									<span class="tit">Q.</span>
									<span class="txt">긴급 기술 지원 및 장애 처리는 차별화 되는 점이 있습니까?</span>
								</label>
								<div class="jq_hide">
									<span class="tit">A.</span>
									<span class="txt">전담 엔지니어 콜센터를 운영하여 SRM 또는 카카오톡에 장애 서비스를 신청하시면 실시간으로 처리 상태를 확인하실 수 있습니다.</span>
								</div>
							</li>
							<li>
								<label class="jq_toggle tit">
									<span class="tit">Q.</span>
									<span class="txt">기술 자료는 어떤 자료입니까?</span>
								</label>
								<div class="jq_hide">
									<span class="tit">A.</span>
									<span class="txt">고객사에서 요청하신 매뉴얼 또는 고객사에 필요한 매뉴얼을 제작하였습니다.  필요하실 때 다운로드 받으실 수 있습니다.  </span>
								</div>
							</li>
							<li class="on">
								<label class="jq_toggle tit">
									<span class="tit">Q.</span>
									<span class="txt">맞춤형 전자 구매 시스템은 무엇입니까?</span>
								</label>
								<div class="jq_hide">
									<span class="tit">A.</span>
									<span class="txt">고객사의 구매 품목을 표준화 시켜 시스템에서 구매팀 또는 상사에게 구매 승인 요청을 하실 수 있습니다. <br>결제기능, 거래명세서 출력, 견적서 출력, 자산 이력관리, 주문 이력조회, 배송조회 등의 서비스를 이용하실 수 있습니다.</span>
								</div>
							</li>
							<li>
								<label class="jq_toggle tit">
									<span class="tit">Q.</span>
									<span class="txt">맞춤형 전자 구매 시스템의 가장 큰 이점은 무엇입니까?</span>
								</label>
								<div class="jq_hide">
									<span class="tit">A.</span>
									<span class="txt">통상적으로 기업에서 거치는 구매 프로세스는 19단계입니다. 신성 맞춤형 전자 구매 시스템(SRM)은 11단계로 단축시켜<br>업무 시간 로스를 최소화하였습니다. </span>
								</div>
							</li>
						</ul>
					</div>
				</div>
</div>
		</div>
		</div>
		<div id="A_Container_R">
			<h3 class="sub_tit">고객서비스</h3>
			<!-- #include virtual="/_inc/right_service.asp" -->
			<!-- #include virtual="/_inc/right_bnn.asp" -->
		</div>

<!-- #include virtual="/_inc/_footer.asp" -->