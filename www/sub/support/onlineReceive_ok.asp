<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/online/")
UploadForm.MaxFileLen = int(1024 * 1024 * 20)

on_type		= SQL_Injection(Trim(UploadForm("on_type")))
on_cate		= SQL_Injection(Trim(UploadForm("on_cate")))
on_company	= SQL_Injection(Trim(UploadForm("on_company")))
on_name		= SQL_Injection(Trim(UploadForm("on_name")))
on_mobile1	= SQL_Injection(Trim(UploadForm("on_mobile1")))
on_mobile2	= SQL_Injection(Trim(UploadForm("on_mobile2")))
on_mobile3	= SQL_Injection(Trim(UploadForm("on_mobile3")))
on_email1	= SQL_Injection(Trim(UploadForm("on_email1")))
on_email2	= SQL_Injection(Trim(UploadForm("on_email2")))
on_title	= SQL_Injection(Trim(UploadForm("on_title")))
on_content	= SQL_Injection(Trim(UploadForm("on_content")))

'// 변수 가공
on_mobile	= on_mobile1 & "-" & on_mobile2 & "-" & on_mobile3
on_email	= on_email1 & "@" & on_email2

If Request.Cookies("U_ID") <> "" Then
	isMember = "Y"
Else
	isMember = "N"
End If

If on_type = "estimate1" Then
	url = "./onlineReceive2.asp"
ElseIf on_type = "estimate2" Then
	url = "./onlineReceive_solution.asp"
ElseIf on_type = "tech" Then
	url = "./onlineReceive.asp"
ElseIf on_type = "demo" Then
	url = "./demo.asp"
End If

ReDim mFile(1)
For i = 1 To 2
	If UploadForm("on_file" & i).FileName <> "" Then
		'// 업로드 가능 여부 체크
		If UploadFileChk(UploadForm("on_file" & i).FileName)=False Then
			Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 용량 체크
		If UploadForm("on_file" & i).FileLen > UploadForm.MaxFileLen Then
			Response.Write "<script language='javascript'>alert('20MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
			Set UploadForm = Nothing
			Response.End
		End If

		'// 파일 저장
		mFile(i-1) = UploadForm("on_file" & i).Save(, False)
		mFile(i-1) = UploadForm("on_file" & i).LastSavedFileName
	End If
Next

Call DbOpen()

Sql = "INSERT INTO board_online("
Sql = Sql & "on_type, "
Sql = Sql & "on_cate, "
Sql = Sql & "on_company, "
Sql = Sql & "on_name, "
Sql = Sql & "on_mobile, "
Sql = Sql & "on_email, "
Sql = Sql & "on_title, "
Sql = Sql & "on_content, "
Sql = Sql & "on_file1, "
Sql = Sql & "on_file2, "
Sql = Sql & "isMember) VALUES("
Sql = Sql & "'" & on_type & "',"
Sql = Sql & "N'" & on_cate & "',"
Sql = Sql & "N'" & on_company & "',"
Sql = Sql & "N'" & on_name & "',"
Sql = Sql & "'" & on_mobile & "',"
Sql = Sql & "'" & on_email & "',"
Sql = Sql & "N'" & on_title & "',"
Sql = Sql & "N'" & on_content & "',"
Sql = Sql & "N'" & SQL_Injection(Trim(mFile(0))) & "',"
Sql = Sql & "N'" & SQL_Injection(Trim(mFile(1))) & "',"
Sql = Sql & "'" & isMember & "')"

dbconn.execute(Sql)

Set UploadForm = Nothing

Call DbClose()

Call jsAlertMsgUrl("문의가 등록되었습니다.", url)
%>