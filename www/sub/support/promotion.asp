 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(4)").addClass('on');
 		});

 	</script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">견적문의</a></li>
 					<li><a href="#">프로모션</a></li>
 				</ul>
 			</div>
 		</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 5
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "BOARD_v1"
Dim query_where			: query_where		= " b_part = 'board05' "
Dim query_orderby		: query_orderby		= " ORDER BY option_notice DESC, b_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
 				<div class="title">
 					<h3>프로모션<%=intNowPage%></h3>
 				</div>

 				<div id="content">
 					<div class="article">
 						<ul>
<%
If rs.bof Or rs.eof Then
%>
							<li><table class="article_tbl"><tr><td colspan="3" align="center">등록된 데이터가 없습니다.</td></tr></table></li>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>
 							<li>
 								<table class="article_tbl">
 									<caption class="blind">이벤트</caption>
 									<colgroup>
 										<col width="60px">
 										<col width="202px">
 										<col width="*">
 									</colgroup>
 									<tbody>
 										<tr>
 											<td class="taC"><% If rs("option_notice") = True Then %>[공지]<% Else %><%=intNowNum%><% End If %></td>
 											<td class="article_img">
											<% If rs("file_1") <> "" Then %>
 												<a href="./promotion_view.asp?idx=<%=rs("b_idx")%>"><img src="/upload/avanboard_v3/<%=rs("file_1")%>" border="0"></a>
											<% End If %>
 											</td>
 											<td class="article_con">
 												<p class="tit"><a href="./promotion_view.asp?idx=<%=rs("b_idx")%>"><%=Cut(rs("b_title"),48,"...")%></a> </p>
 												<p class="date">등록일 : <%=rs("b_writeday")%> <span class="pdL30">조회수 : <%=rs("b_read")%></span></p>
 												<!--p class="con"><a href="./promotion.asp?mode=v&amp;idx=281&amp;startPage=0&amp;code=event"></a></p-->
 											</td>
 										</tr>
 									</tbody>
 								</table>
 							</li>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 						</ul>
 					</div>
 					<!--버튼-->
 					<div class="ntb-listbtn-area mgT10">
 					</div>
 					<!--검색폼-->
 					<div class="ntb-search-area">
 						<form name="search_form" action="" method="post">
 							<select name="fieldname" class="AXSelect vmiddle">
 								<option value="b_title" <% If fieldname = "" Or fieldname = "b_title" Then %> selected<% End If %>>제목</option>
 								<option value="b_text" <% If fieldname = "b_text" Then %> selected<% End If %>>내용</option>
 							</select>
 							<input type="text" name="fieldvalue" value="<%=fieldvalue%>" class="AXInput vmiddle">
 							<input type="submit" value="검색" class="AXButton">
 						</form>
 					</div>

					<% Call Paging_user("") %>
 				</div>

 			</div>
			<div id="A_Container_R">
				<h3 class="sub_tit">견적문의</h3>
				<!-- #include virtual="/_inc/right_estimate.asp" -->
				<!-- #include virtual="/_inc/right_bnn.asp" -->
			</div>

<!-- #include virtual="/_inc/_footer.asp" -->