<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(1)").addClass('on');
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">견적문의</a></li>
 					<li><a href="#">맞춤사양견적</a></li>
 				</ul>
 			</div>
 		</div>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<div class="h3Title">
 					<h3>맞춤사양견적</h3>
 					<p></p>
 					<dl class="phonNum">
 						<dt><img src="/images/sub/img_phonNum1.gif" alt="">전문가 상담</dt>
 						<dd>02-867-3007</dd>
 					</dl>
 				</div>
 				<div id="tabsholder1" class="estCon">
 					<ul class="tabs">
 						<li id="tab11" class="current"><a href="/sub/b2b/estimate_list.asp">데스크탑</a></li>
 						<li id="tab12"><a href="#">노트북</a></li>
 						<li id="tab13"><a href="#">워크스테이션</a></li>
 						<li id="tab14"><a href="#">서버</a></li>
 						<li id="tab15"><a href="#">서버 백업 솔루션</a></li>
 						<li id="tab16"><a href="#">스토리지</a></li>
 					</ul>

 					<div class="contents">

 						<div id="content1" class="tabscontent tabCon_est">
 							<ul>
 								<li>
 									<div><a href="/sub/b2b/product_view.asp"><img src="/images/sub/15820767141.png" alt=""></a></div>
 									<dl>
 										<dt>HP ProDesk 600 G4 SFF</dt>
 										<dd></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP ProDesk 600 G4 SFF</dt>
 										<dd>HP ProDesk 600 G4 SFF<br>
 											Intel® Core™ i5-8500 3.0GHz (Up to 4.1GHz, 9MB, 6C, 6th)<br>
 											8GB DDR4-2666<br>
 											Windows 10 Professional 64bit<br>
 											256GB SSD NVMe<br>
 											1TB HDD (7,200rpm)<br>
 											Intel® UHD Graphics 630</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP ProDesk 600 G4 SFF</dt>
 										<dd>HP ProDesk 600 G4 SFF<br>
 											Intel® Core™ i7-8700 3.2GHz (Up to 4.6GHz, 12MB, 6C, 12th)<br>
 											16G DDR4-2666<br>
 											Windows 10 Professional 64bit<br>
 											256GB SSD NVMe<br>
 											1TB HDD (7,200rpm)<br>
 											Intel® UHD Graphics 630<br>
 										</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 							</ul>
 							<ul>
 								<li>
 									<div><a href="/sub/b2b/product_view.asp"><img src="/images/sub/15820767131.png" alt=""></a></div>
 									<dl>
 										<dt>HP EliteDesk 800 G4 SFF</dt>
 										<dd></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP EliteDesk 800 G4 SFF</dt>
 										<dd>HP EliteDesk 800 G4 SFF<br>
 											Intel® Core™ i7-8700U 3.2GHz<br>
 											16GB DDR4-2666<br>
 											Windows 10 Professional 64bit<br>
 											512G SSD NVMe<br>
 											1TB HDD (7,200rpm, SATA)<br>
 											Intel® UHD Graphics 630</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP EliteDesk 800 G4 SFF</dt>
 										<dd>HP EliteDesk 800 G4 SFF<br>
 											Intel® Core™ i5-8500 3.0GHz (Up to 4.1GHz, 9MB, 6C, 6th)<br>
 											16GB DDR4-2666<br>
 											Windows 10 Professional 64bit<br>
 											512G SSD NVMe<br>
 											1TB HDD (7,200rpm, SATA)<br>
 											<br>
 											Intel® UHD Graphics 630</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 							</ul>
 							<ul>
 								<li>
 									<div><a href="/sub/b2b/product_view.asp"><img src="/images/sub/15820767121.png" alt=""></a></div>
 									<dl>
 										<dt>HP EliteDesk 800 G4 TWR</dt>
 										<dd></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP EliteDesk 800 G4 TWR</dt>
 										<dd>HP EliteDesk 800 G4 TWR<br>
 											Intel® Pentium® Gold G5400 3.7GHz<br>
 											4GB DDR4-2666<br>
 											Free Dos 2.0<br>
 											1TB HDD (7,200rpm, SATA)<br>
 											Slim Super Multi DVD Writer<br>
 											Intel® UHD Graphics 610<br>
 										</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP EliteDesk 800 G4 TWR</dt>
 										<dd>HP EliteDesk 800 G4 TWR<br>
 											Intel® Pentium® Gold G5400 3.7GHz<br>
 											4GB DDR4-2666<br>
 											Free Dos 2.0<br>
 											1TB HDD (7,200rpm, SATA)<br>
 											Slim Super Multi DVD Writer<br>
 											Intel® UHD Graphics 610<br>
 										</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 							</ul>
 							<ul>
 								<li>
 									<div><a href="/sub/b2b/product_view.asp"><img src="/images/sub/15820765811.png" alt=""></a></div>
 									<dl>
 										<dt>HP ProDesk 280 G3 SFF</dt>
 										<dd></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP ProDesk 280 G3 SFF</dt>
 										<dd>HP ProDesk 280 G3 SFF<br>
 											Intel® Core™ i3-9100U 3.6GHz<br>
 											Windows 10 Professional 64bit<br>
 											256G SSD NVMe<br>
 											8GB DDR4-2666<br>
 											Intel® UHD Graphics 630<br>
 										</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP ProDesk 280 G3 SFF</dt>
 										<dd>HP ProDesk 280 G3 SFF<br>
 											Intel® Core™ i5-9400 2.9GHz (Up to 4.1GHz, 9MB, 6C, 6th)<br>
 											Windows 10 Professional 64bit<br>
 											512GB M.2 2280 PCIe NVMe SSD<br>
 											16G DDR4-2666<br>
 											Intel® UHD Graphics 630</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 							</ul>
 							<ul>
 								<li>
 									<div><a href="/sub/b2b/product_view.asp"><img src="/images/sub/15820764431.png" alt=""></a></div>
 									<dl>
 										<dt>HP EliteDesk 800 G3 DM</dt>
 										<dd></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP EliteDesk 800 G3 DM</dt>
 										<dd>HP EliteDesk 800 G3 DM<br>
 											Intel® Core™ i3-6100U 2.3GHz<br>
 											4GB DDR4-2400<br>
 											Windows 10 Professional 64bit<br>
 											128GB 2.5" SSD<br>
 											Intel® HD Graphics 530<br>
 										</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP EliteDesk 800 G3 DM</dt>
 										<dd>HP EliteDesk 800 G3 DM<br>
 											Intel® Core™ i3-6100U 2.3GHz<br>
 											4GB DDR4-2400<br>
 											Windows 10 Professional 64bit<br>
 											128GB 2.5" SSD<br>
 											Intel® HD Graphics 530<br>
 											<br>
 										</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 							</ul>
 							<ul>
 								<li>
 									<div><a href="/sub/b2b/product_view.asp"><img src="/images/sub/15820763051.png" alt=""></a></div>
 									<dl>
 										<dt>HP ProOne 400 G4 AIO</dt>
 										<dd></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>ProOne 400 G4 AIO</dt>
 										<dd>ProOne 400 G4 AIO<br>
 											Intel® i3-8100T 3.1GHz (6MB, 4Core, 4Th)<br>
 											4GB (4GBx1) DDR4-2666 (최대 32GB / 2 Slots)<br>
 											Windows10 Professional 64-bit<br>
 											128G NVMe<br>
 											Intel® UHD Graphics 630<br>
 										</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>ProOne 400 G4 AIO</dt>
 										<dd>ProOne 400 G4 AIO<br>
 											Intel® i3-8100T 3.1GHz (6MB, 4Core, 4Th)<br>
 											4GB (4GBx1) DDR4-2666 (최대 32GB / 2 Slots)<br>
 											Windows10 Professional 64-bit<br>
 											128G NVMe<br>
 											Intel® UHD Graphics 630</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 							</ul>
 							<ul>
 								<li>
 									<div><a href="/sub/b2b/product_view.asp"><img src="/images/sub/15820759371.png" alt=""></a></div>
 									<dl>
 										<dt>HP EliteOne 800 G5 AIO</dt>
 										<dd></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP EliteOne 800 G5 AIO</dt>
 										<dd>HP EliteOne 800 G5 AIO<br>
 											23.8"(60.45cm) IPS (1920x1080) / WLED Backlit LCD<br>
 											Intel® i3-9100 3.6GHz (6MB, 4Core, 4Th)<br>
 											4GB (4GBx1) DDR4-2666 (최대 32GB / 2 Slots)<br>
 											Windows10 Pro (64bit)<br>
 											256G NVMe<br>
 											Intel® UHD Graphics 630</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 								<li>
 									<p class="new"><span>추천제품</span></p>
 									<dl class="estBox">
 										<dt>HP EliteOne 800 G5 AIO</dt>
 										<dd>HP EliteOne 800 G5 AIO<br>
 											23.8"(60.45cm) IPS (1920x1080) / WLED Backlit LCD<br>
 											Intel® Core™ i5-9400 2.9GHz (Up to 4.1GHz, 9MB, 6C, 6th)<br>
 											8G DDR4-2666<br>
 											Windows10 Pro (64bit)<br>
 											256G NVMe<br>
 											AMD® Radeon™ RX 560X 4GB</dd>
 										<dd class="price"><span class="d-iB h70"></span></dd>
 										<dd class="btn"><a href="/sub/b2b/product_view.asp">사양변경</a></dd>
 									</dl>
 								</li>
 							</ul>

 						</div>


 					</div>
 				</div>
 			</div>

					<div id="A_Container_R">
 				<h3 class="sub_tit">견적문의</h3>
 				<!-- #include virtual="/_inc/right_estimate.asp" -->
 				<!-- #include virtual="/_inc/right_bnn.asp" -->
 			</div>

<!-- #include virtual="/_inc/_footer.asp" -->