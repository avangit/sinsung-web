 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<script type="text/javascript">
	$(document).on('ready', function() {
    	$(".sub_lnb li:nth-child(4)").addClass('on');
    });
</script>
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
	<!-- 서브 네비게이션 -->
	<div id="sub_nav">
		<div class="inner">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="#">고객서비스</a></li>
				<li><a href="#">카톡 1:1 서비스</a></li>
			</ul>
		</div>
	</div>
	<div id="A_Container">
		<!-- 서브컨텐츠 -->
		<div id="A_Container_C">
		<!-- 서브 타이틀 -->
			<div class="title">
				<h3>카톡 1:1 서비스</h3>
			</div>
			<div id="content" class="kakao">
			<a href="https://pf.kakao.com/_kxipdT" target="_blank">
				<img src="/images/sub/kakao.jpg" alt="카카오톡 연결하기">
			</a>
		</div>
		</div>
		<div id="A_Container_R">
			<h3 class="sub_tit">고객서비스</h3>
			<!-- #include virtual="/_inc/right_service.asp" -->
			<!-- #include virtual="/_inc/right_bnn.asp" -->
		</div>

<!-- #include virtual="/_inc/_footer.asp" -->