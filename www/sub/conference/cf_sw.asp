 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript" src="/js/tytabs.jquery.min.js"></script>
 	<script type="text/javascript">
 		$(function() {
 			$("#tabsholderW").tytabs({
 				tabinit: "1",
 				fadespeed: "fast"
 			});
 		});

 		$(function() {

 			$(window).on("scroll", function() {
 				var winTop = $(window).scrollTop();
 				if (winTop >= 580) {
 					$(".confCon .pageTab").addClass("scroll");
 				} else {
 					$(".confCon .pageTab").removeClass("scroll");
 				}
 			});
 		});

 	</script>

 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<div class="bg_conf"></div>
 		<div id="A_Container" class="confer">
 			<!-- 서브컨텐츠 -->

 			<div id="container" class="confCon" style="padding-top:0;">
 				<div class="pageTab">
 					<ul>
 						<li><a href="./conference.asp">화상 회의 H/W 솔루션</a></li>
 						<li class="on"><a href="./cf_sw.asp">화상 회의 S/W<img src="/images/sub/conf_tab_arr.jpg" alt=""></a></li>
 					</ul>
 				</div>


 				<div class="titArea">
 					화상회의 S/W 소개
 				</div>
 				<table class="swtb">
 					<colgroup>
 						<col width="16%">
 						<col width="248px">
 						<col width="248px">
 						<col width="248px">
 						<col width="248px">
 					</colgroup>
 					<tbody>
 						<tr>
 							<th>구분</th>
 							<td>
 								<img src="/images/sub/logo_skype.gif" alt="skype">
 								<p class="btn-st ma0"><a href="http://skype.daesung.com/download/downloadMain.asp" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>S/W 다운로드</a></p>
 							</td>
 							<td>
 								<img src="/images/sub/logo_curix.gif" alt="curix">
 								<p class="btn-st ma0"><a href="/download/[sinsungcns]_Proposal_of_integrated%20wired_wireless_video_conferencing_system_Kyeryx.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>제안서 다운로드</a></p>
 							</td>
 							<td>
 								<img src="/images/sub/logo_videocall.gif" alt="videocall">
 								<p class="btn-st ma0"><a href="/download/[sinsungcns]_VideoCall_Standard%20product_introduction.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>제안서 다운로드</a></p>
 							</td>
 							<td>
 								<img src="/images/sub/logo_zoom.gif" alt="zoom">
 								<p class="btn-st ma0"><a href="http://skype.daesung.com/download/downloadMain.asp" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>가이드 다운로드</a></p>
 							</td>
 						</tr>
 						<tr>
 							<th>지원 단말</th>
 							<td class="vtalign">
 								PC<br>
 								스마트폰<br>
 								스마트패드<br>
 								인터넷전화</td>
 							<td class="vtalign">
 								PC<br>
 								스마트폰<br>
 								스마트패드<br>
 								인터넷전화<br>
 								MCU<br>
 								TP<br>
 								CCTV<br>
 							</td>
 							<td class="vtalign">
 								PC<br>
 								스마트폰<br>
 								스마트패드<br>
 								인터넷전화<br>
 								MCU<br>
 								TP<br>
 							</td>
 							<td class="vtalign">
 								PC<br>
 								스마트폰<br>
 								스마트패드<br>
 								인터넷전화<br>
 								MCU<br>
 								TP<br>
 							</td>
 						</tr>
 						<tr>
 							<th>화상 통화 비용</th>
 							<td>FREE<br>
 								(Office 365 Business Essentials 구매 시<br>
 								Office 365 Business Premium 구매 시)</td>
 							<td>월 20,000원</td>
 							<td>월 9,900원</td>
 							<td>월 $19.99원</td>
 						</tr>
 						<tr>
 							<th>화상 통화 참가자수</th>
 							<td>250명</td>
 							<td>200명 이상</td>
 							<td>200명</td>
 							<td>최대 500명</td>
 						</tr>
 						<tr>
 							<th>실시간 화면공유</th>
 							<td>○</td>
 							<td>○</td>
 							<td>○</td>
 							<td>○</td>
 						</tr>
 						<tr>
 							<th>CODEC</th>
 							<td>-</td>
 							<td>H.239/BFCP</td>
 							<td>H.264</td>
 							<td>H.323</td>
 						</tr>
 						<tr>
 							<th>해상도</th>
 							<td>FHD<br>1920 x 1080</td>
 							<td>FHD<br>1920 x 1080</td>
 							<td>FHD<br>1920 x 1080</td>
 							<td>FHD<br>1920 x 1080</td>
 						</tr>
 					</tbody>
 				</table>
 				<div id="tabsholderW">
 					<ul class="tabs" id="goA">
 						<li id="tab1" class="current"><img src="/images/sub/logo_skype.gif" alt="skype"></li>
 						<li id="tab2"><img src="/images/sub/logo_curix.gif" alt="curix"></li>
 						<li id="tab3"><img src="/images/sub/logo_videocall.gif" alt="videocall"></li>
 						<li id="tab4"><img src="/images/sub/logo_zoom.gif" alt="zoom"></li>
 					</ul>

 					<div class="confTabConW">
 						<div id="content1" class="tabscontent" style="display: block;">
 							<div class="swBx">
 								<div class="sw-titBx">
 									<dl>
 										<dt><img src="/images/sub/logo_skype.gif" alt="skype"></dt>
 										<dd>그룹 영상 통화에는 매월 100시간, 매일 10시간, 개별 영상 통화 당 4시간의 공정 사용 제한이 적용됩니다. <br>
 											이러한 제한에 도달하게 되면, 영상이 꺼지게 되며 통화가 음성 통화로 전환됩니다.
 										</dd>
 									</dl>
 									<ul>
 										<li>
 											<p class="btn-st"><a href="http://skype.daesung.com/download/downloadMain.asp" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>다운로드</a></p>
 										</li>
 									</ul>
 								</div>
 								<section>
 									<ul class="wid100">
 										<li>
 											<i><img src="/images/sub/ico_skype1.gif" alt=""></i>
 											<dl>
 												<dt>일대일 영상통화</dt>
 												<dd>얼굴을 보며 통화하면 상대와 더욱 가까워질 수 있습니다.</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_skype2.gif" alt=""></i>
 											<dl>
 												<dt>그룹 영상통화</dt>
 												<dd>영상 통화로 여러 친구와 함께 만나세요. (최대 25명 동시 접속 가능)</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_skype3.gif" alt=""></i>
 											<dl>
 												<dt>화면 공유</dt>
 												<dd>대화 상대에게 나의 컴퓨터 화면을 공유하여 보여주세요.</dd>
 											</dl>
 										</li>
 									</ul>
 								</section>
 							</div>

 						</div>
 						<div id="content2" class="tabscontent" style="display: none;">
 							<div class="swBx">
 								<div class="sw-titBx">
 									<dl>
 										<dt><img src="/images/sub/logo_curix.gif" alt="curix"></dt>
 										<dd>코덱, MCU, IP 영상폰, 모바일, CCTV..<br>
 											이 모든 것이 CURIX 클라우드에서 Collaboration이 가능합니다.
 										</dd>
 									</dl>
 									<ul>
 										<li>
 											<p class="btn-st"><a href="/download/[sinsungcns]_Proposal_of_integrated%20wired_wireless_video_conferencing_system_Kyeryx.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>제안서 다운로드</a></p>
 										</li>
 										<li>
 											<p class="btn-st"><a href="javascript:alert('준비중입니다.');"><i class="fa fa-video-camera" aria-hidden="true"></i>도입사례</a></p>
 										</li>
 									</ul>
 								</div>
 								<section>
 									<ul>
 										<li>
 											<i><img src="/images/sub/ico_curix1.gif" alt=""></i>
 											<dl>
 												<dt>H/W 화상장비 연동</dt>
 												<dd>CISCO(구, Tandberg), Polycom, SONY, Radvision, Aver, Lifesize, LG전자</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_curix2.gif" alt=""></i>
 											<dl>
 												<dt>H.239, BFCP</dt>
 												<dd>표준 듀얼 스트리밍</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_curix3.gif" alt=""></i>
 											<dl>
 												<dt>유무선 통합 서비스</dt>
 												<dd>PC, Mobile, 코덱, IP영상폰, CCTV 통합화상회의 제공</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_curix4.gif" alt=""></i>
 											<dl>
 												<dt>CCTV 화자 참석</dt>
 												<dd>화상회의 참석자로써 CCTV 접속, 회의자료로 사용</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_curix5.gif" alt=""></i>
 											<dl>
 												<dt>판서</dt>
 												<dd>모든 공유대상에서 판서 가능</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_curix6.gif" alt=""></i>
 											<dl>
 												<dt>서버 불필요</dt>
 												<dd>별도의 서버구축 없이 ID발급만으로 화상회의 서비스 이용</dd>
 											</dl>
 										</li>
 									</ul>
 								</section>
 							</div>
 						</div>
 						<div id="content3" class="tabscontent" style="display: none;">
 							<div class="swBx">
 								<div class="sw-titBx">
 									<dl>
 										<dt><img src="/images/sub/logo_videocall.gif" alt="videocall"></dt>
 										<dd>VideoCall은 Full HD의 영상, 다양한 Collaboration 기술 (문서공유, 미디어공유)를 제공하는 <br>
 											혁신적인 협업 솔루션 입니다.
 										</dd>
 									</dl>
 									<ul>
 										<li>
 											<p class="btn-st"><a href="/download/[sinsungcns]_VideoCall_Standard%20product_introduction.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>제안서 다운로드</a></p>
 										</li>
 										<li>
 											<p class="btn-st"><a href="javascript:alert('준비중입니다.');"><i class="fa fa-video-camera" aria-hidden="true"></i>도입사례</a></p>
 										</li>
 									</ul>
 								</div>
 								<section>
 									<ul>
 										<li>
 											<i><img src="/images/sub/ico_videocall1.gif" alt=""></i>
 											<dl>
 												<dt>다양한 언어지원</dt>
 												<dd>영어, 일본어, 중국어</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_videocall2.gif" alt=""></i>
 											<dl>
 												<dt>간편한 설치, 쉬운 사용</dt>
 												<dd>원클릭 인스톨, 직관적 UI</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_videocall3.gif" alt=""></i>
 											<dl>
 												<dt>인터넷 환경을 통한 자유로운 회의</dt>
 												<dd>VPN, 사내방</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_videocall4.gif" alt=""></i>
 											<dl>
 												<dt>안전한 화상회의</dt>
 												<dd>국제 표준 보안, 자체 암호화 기술</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_videocall5.gif" alt=""></i>
 											<dl>
 												<dt>회의를 위한 다양한 기능</dt>
 												<dd>영상회의, 음성회의, 문서회의</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_videocall6.gif" alt=""></i>
 											<dl>
 												<dt>고해상도 HD 영상</dt>
 												<dd>최신 영상 Codec 기술 적용</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_videocall7.gif" alt=""></i>
 											<dl>
 												<dt>참여자에 따른 비디오 모드</dt>
 												<dd>1:1, 1:2, 1:3 ...</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_videocall8.gif" alt=""></i>
 											<dl>
 												<dt>각종 커스터 마이징 기능</dt>
 												<dd>직접 설계 구축 가능</dd>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_videocall9.gif" alt=""></i>
 											<dl>
 												<dt>잡음 없는 선명한 음질</dt>
 												<dd>국제 표준 보안, 자체 암호화 기술</dd>
 											</dl>
 										</li>
 									</ul>
 								</section>
 							</div>
 						</div>
 						<div id="content4" class="tabscontent" style="display: none;">
 							<div class="swBx">
 								<div class="sw-titBx">
 									<dl>
 										<dt><img src="/images/sub/logo_zoom.gif" alt="zoom"></dt>
 										<dd>ZoomVideoCommunications(비디오 커뮤니케이션)는 2018 Gartner의 다른 파트너로 선정되어 솔루션을 <br>
 											충족하기 위한 고객의 선택에 큰 영향을 미치고 있습니다.
 										</dd>
 									</dl>
 									<ul>
 										<li>
 											<p class="btn-st"><a href="javascript:alert('준비중입니다.');"><i class="fa fa-download" aria-hidden="true"></i>가이드 다운로드</a></p>
 										</li>
 										<li>
 											<p class="btn-st"><a href="javascript:alert('준비중입니다.');"><i class="fa fa-video-camera" aria-hidden="true"></i>도입사례</a></p>
 										</li>
 									</ul>
 								</div>
 								<section>
 									<ul>
 										<li>
 											<i><img src="/images/sub/ico_zoom1.gif" alt=""></i>
 											<dl>
 												<dt class="boldn">Meetings
 												</dt>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_zoom2.gif" alt=""></i>
 											<dl>
 												<dt class="boldn">Business IM</dt>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_zoom3.gif" alt=""></i>
 											<dl>
 												<dt class="boldn">Video Webinar</dt>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_zoom4.gif" alt=""></i>
 											<dl>
 												<dt class="boldn">H.323/SIP Connector</dt>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_zoom5.gif" alt=""></i>
 											<dl>
 												<dt class="boldn">Zoom Rooms</dt>
 											</dl>
 										</li>
 										<li>
 											<i><img src="/images/sub/ico_zoom6.gif" alt=""></i>
 											<dl>
 												<dt class="boldn">Developer Platform</dt>
 											</dl>
 										</li>
 									</ul>
 								</section>
 							</div>
 						</div>
 					</div>




 				</div>




 			</div>

<!-- #include virtual="/_inc/_footer.asp" -->