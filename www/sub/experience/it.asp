 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<script type="text/javascript">
			$(document).on('ready', function() {
				$(".sub_lnb li:nth-child(2)").addClass('on');
			});
		</script>
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
	<!-- 서브 네비게이션 -->
	<div id="sub_nav">
		<div class="inner">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="#">우리의 경험</a></li>
				<li><a href="#">IT 구매제안</a></li>
			</ul>
		</div>
	</div>
	<div id="A_Container">
		<!-- 서브컨텐츠 -->
		<div id="A_Container_C">
			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>IT 구매제안</h3>
			</div>
			<div id="content" class="epItCon">
			<h4>고객니즈(Customer Needs) 노트북, 데스크탑 구매시</h4>
			<div class="itBox">
				<ul>
					<li>
						<dl>
							<dt>저렴한 가격</dt>
								<dd>인터넷 최저가</dd>
								<dd>회사에서 잘아는 누군가?</dd>
						</dl>
					</li>
					<li class="it2">
						<p>용도에 맞는 구매</p>
						<span>IT전문컨설팅 필요</span>
					</li>
					<li class="it3">
						<p>관리용이, 획일화된 품목</p>
						<span>자산관리</span>
					</li>
					<li class="it4">
						<p>신속하고 안정적인 공급</p>
						<span>정확한 수요 대비</span>
					</li>
					<li class="it5">
						<p>정품 소프트웨어 사용</p>
						<span>기업용 운영체제 사용 필수<br>(WIN7PRO, WIN10PRO)</span>
					</li>
					<li class="it6">
						<p>3~5년 이내 수리비 부담 최소</p>
						<span>주요 메이커 회사 구매</span>
					</li>
				</ul>
				<dl>
					<dt>문제점</dt>
						<dd>
							<span class="cnt">1.</span>
							<span class="txt">자산조사의 어려움<br>(조립pc, 품목 시리얼 규칙이 없어짐)</span>
						</dd>
						<dd>
							<span class="cnt">2.</span>
							<span class="txt">회사 업무에 맞는 구매보다는<br>개인이 쓰고 싶은 제품을 구매해서<br>구매용도와 맞지 않음</span>
						</dd>
						<dd>
							<span class="cnt">3.</span>
							<span class="txt">가성비나 유행 상품을 고르는데<br>자칫 쇼핑이 되어야 함</span>
						</dd>
						<dd>
							<span class="cnt">4.</span>
							<span class="txt">A/S 문제 발생시 즉각 조치가 어려움<br>(중소기업 모니터, 노트북)</span>
						</dd>
						<dd>
							<span class="cnt">5.</span>
							<span class="txt">관리포인트 증가 – 각 사용자마다<br>각기 다른 제품 구매</span>
						</dd>
				</dl>
			</div>
			<h4 class="mt80">컨설팅 기대효과</h4>
			<ul class="itEffect">
				<li>
					<dl>
						<dt>IT전문 컨설팅</dt>
							<dd>- 고객의 환경 파악 및 현황 분석을 통한 최적의 제품 컨설팅 및 솔루션을 제공</dd>
					</dl>
				</li>
				<li class="ef2">
					<dl>
						<dt>TCO절감</dt>
							<dd>- 구매비용 절감 : 업계 유일의 공급단가 산정방식 혁신을 통한 획기적 구매비용 절감</dd>
							<dd>- 관리비용 절감 : 비효율적인 구매소요시간 절감, 전산환경의 각종 시장정보 확보</dd>
					</dl>
				</li>
				<li class="ef3">
					<dl>
						<dt>공급의 안정성 확보</dt>
							<dd>- 서버, 스토리지,Business H/W, S/W, Printing 등 다양한 제품군 보유</dd>
					</dl>
				</li>
				<li class="ef4">
					<dl>
						<dt>생산성 강화</dt>
							<dd>- DB표준화 및 관리 수준 향상, 구매 행정 절차 간소화</dd>
					</dl>
				</li>
				<li class="ef5">
					<dl>
						<dt>업무 효율성 증대</dt>
							<dd>- 모든 사용자가 기본적인 장애에 빨리 대처가 가능하여 업무 손실을 최소화</dd>
					</dl>
				</li>
				<li class="ef6">
					<dl>
						<dt>적기 수급 가능</dt>
							<dd>- 수요 예측 시스템으로 회사에서 필요한 시점에 수급가능, 교체 수요 예측가능</dd>
					</dl>
				</li>
				<li class="ef7">
					<dl>
						<dt>자산관리 솔루션</dt>
							<dd>- 환경과 예산에 맞는 자산관리 솔루션 도입가능</dd>
					</dl>
				</li>
			</ul>
		</div>

		</div>
		<div id="A_Container_R">
			<h3 class="sub_tit">우리의 경험</h3>
			<!-- #include virtual="/_inc/right_experience.asp" -->
			<!-- #include virtual="/_inc/right_bnn.asp" -->
		</div>

<!-- #include virtual="/_inc/_footer.asp" -->