<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
		<script type="text/javascript">
	$(document).on('ready', function() {
    	$(".sub_lnb li:nth-child(7)").addClass('on');
    });
</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">회사소개</a></li>
 					<li><a href="#">공지사항</a></li>
 				</ul>
 			</div>
 		</div>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
			<div class="title">
				<h3>공지사항</h3>
			</div>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "BOARD_v1"
Dim query_where			: query_where		= " b_part = 'board01' AND display_mode <> 'srm' "
Dim query_orderby		: query_orderby		= " ORDER BY option_notice DESC, b_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
 				<div id="content">
 					<table id="empTable">
 						<caption class="blind">공지사항 게시판</caption>
 						<colgroup>
 							<col width="10%">
 							<col width="">
 							<col width="10%">
 							<col width="12%">
 						</colgroup>
 						<tbody>
 							<tr>
 								<th>번호</th>
 								<th>제목</th>
 								<th>조회수</th>
 								<th>작성일</th>
 							</tr>
<%
If rs.bof Or rs.eof Then
%>
							<tr><td colspan="4" align="center">등록된 데이터가 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>
 							<tr>
 								<td>
								<% If rs("option_notice") = True Then %>
									<img src="/images/common/icon_notice.gif" alt="공지">
								<% Else %>
									<%=intNowNum%>
								<% End If %>
								</td>
 								<td class="left"><a href="./notice_view.asp?idx=<%=rs("b_idx")%>"><%=Cut(rs("b_title"),44,"...")%></a></td>
 								<td><%=rs("b_read")%></td>
 								<td><%=rs("b_writeday")%></td>
 							</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 						</tbody>
 					</table>


 					<!--버튼-->
 					<div class="ntb-listbtn-area mgT10">
 					</div>


 					<!--검색폼-->
 					<div class="ntb-search-area">
 						<form name="search_form" action="" method="post">
 							<select name="fieldname" class="AXSelect vmiddle">
 								<option value="b_title" <% If fieldname = "" Or fieldname = "b_title" Then %> selected<% End If %>>제목</option>
 								<option value="b_text" <% If fieldname = "b_text" Then %> selected<% End If %>>내용</option>
 							</select>
 							<input type="text" name="fieldvalue" value="<%=fieldvalue%>" class="AXInput vmiddle">
 							<input type="submit" value="검색" class="AXButton">
 						</form>
 					</div>

					<% Call Paging_user("") %>
 					<!--div class="page">
 						<ul class="clfix">
 							<li><a href="/sub/company/notice.php?startPage=0&amp;code=notice">&lt;&lt;</a></li>
 							<li class="on"><a href="#">1</a></li>
 							<li><a href="/sub/company/notice.php?startPage=0&amp;code=notice">&gt;&gt;</a></li>
 						</ul>
 					</div-->

 				</div>
 			</div>
 			<div id="A_Container_R">
 				<h3 class="sub_tit">회사소개</h3>
 				<!-- #include virtual="/_inc/right_about_cpy.asp" -->
				<!-- #include virtual="/_inc/right_bnn.asp" -->
 			</div>

<!-- #include virtual="/_inc/_footer.asp" -->