 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 			<script type="text/javascript">
	$(document).on('ready', function() {
    	$(".sub_lnb li:nth-child(2)").addClass('on');
    });
		</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">인재경영</a></li>
 					<li><a href="#">우리문화</a></li>
 				</ul>
 			</div>
 		</div>
<%
b_idx = SQL_Injection(Trim(Request.QueryString("idx")))

sql = "UPDATE BOARD_v1 SET b_read = b_read + 1 WHERE b_idx = " & b_idx
dbconn.execute(sql)

sql = "SELECT * FROM BOARD_v1 WHERE b_idx = " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	b_title = rs("b_title")
	b_text = rs("b_text")
	file_1 = rs("file_1")
	file_2 = rs("file_2")
	file_3 = rs("file_3")
	b_read = rs("b_read")
	b_writeday = rs("b_writeday")
	sdate = rs("b_addtext3")
	edate = rs("b_addtext4")
End If

rs.Close
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
 				<div class="title">
 					<h3>우리문화</h3>
 				</div>
 				<div id="content">
 					<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
 						<caption>게시판 내용</caption>
 						<colgroup>
 							<col width="12%">
 							<col width="">
 							<col width="12%">
 							<col width="15%">
 						</colgroup>
 						<thead>
 							<tr>
 								<th colspan="4" class="r_none"> <%=b_title%></th>
 							</tr>
 						</thead>
 						<tbody>
 							<tr>
 								<th>등록일</th>
 								<td class="left"><%=b_writeday%></td>
 								<th>조회수</th>
 								<td><%=b_read%></td>
 							</tr>
 							<tr>
 								<td colspan="4" class="init">
 									<div class="content-area">
									<% If file_1 <> "" Then %>
										<img title="<%=file_1%>" src="/upload/avanboard_v3/<%=file_1%>"><br><br>
									<% End If %>
									<% If file_2 <> "" Then %>
										<img title="<%=file_2%>" src="/upload/avanboard_v3/<%=file_2%>"><br><br>
									<% End If %>
									<% If file_3 <> "" Then %>
										<img title="<%=file_3%>" src="/upload/avanboard_v3/<%=file_3%>"><br><br>
									<% End If %>
 										<%=b_text%>
 									</div>
 								</td>
 							</tr>

 							<!-- 파일첨부 -->

 							<!-- //파일첨부 -->

 						</tbody>
 					</table>

 					<!--버튼-->
 					<div class="ntb-tb-view-btn" style="width:100%">
 						<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='./culture.asp'">
 						<div class="btnr">

 						</div>
 					</div>


 					<div class="ntb-tb-view-reply">

 						<!--이전글 시작-->
 						<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
 							<caption>게시판 이전/다음글</caption>
 							<colgroup>
 								<col width="12%">
 								<col width="">
 							</colgroup>
 							<tbody>
<%
sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board02' AND b_idx < " & b_idx & " ORDER BY b_idx DESC"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 								<tr>
 									<th width="12%">이전글</th>
 									<td width="88%" class="left font_gray">
 										<a href="./culture_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 									</td>
 								</tr>
<%
End If

rs.Close

sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board02' AND b_idx > " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 								<!--다음글 시작-->
 								<tr>
 									<th width="12%">다음글</th>
 									<td width="88%" class="left font_gray">
 										<a href="./culture_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 									</td>
 								</tr>
<%
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 							</tbody>
 						</table>
 					</div>
 					<!-- 코멘트 시작 -->

 					<!-- 코멘트 종료-->
 				</div>
 			</div>
			<div id="A_Container_R">
				<h3 class="sub_tit">인재경영</h3>
				<!-- #include virtual="/_inc/right_recruit.asp" -->
				<!-- #include virtual="/_inc/right_bnn.asp" -->
			</div>

<!-- #include virtual="/_inc/_footer.asp" -->