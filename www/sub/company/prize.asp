 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
			<script type="text/javascript">
			$(document).on('ready', function() {
				$(".sub_lnb li:nth-child(6)").addClass('on');
			});
		</script>
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
	<!-- 서브 네비게이션 -->
	<div id="sub_nav">
		<div class="inner">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">대표 인사말</a></li>
			</ul>
		</div>
	</div>
	<div id="A_Container">
		<!-- 서브컨텐츠 -->
		<div id="A_Container_C">
			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>수상실적</h3>
			</div>
			<div id="content" class="prize">
			<h4>인증 및 라이선스 현황</h4>
			<!-- part -->
			<div class="part mt30">
				<p class="pr_tit">2020</p>
				<ul>
					<li>
						<img src="/images/sub/cert2020-01.jpg" alt="">
						<p class="pr_txt">
							Inno Biz<br>
							<span>(2020.1.20~2023.1.19)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cert2020-02.jpg" alt="">
						<p class="pr_txt">
							벤처기업확인서<br>
							<span>(2020.1.17~2022.1.16)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cert2020-03.jpg" alt="">
						<p class="pr_txt">
							삼성 스타점<br>
							<span>(2020)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cert2020-04.jpg" alt="">
						<p class="pr_txt">
							성과공유기업 확인서<br>
							<span>(2020.1.10~2021.1.9)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cert2020-05.jpg" alt="">
						<p class="pr_txt">
							청년 친화 강소기업 선정서<br>
							<span>(2020.1.1~2020.12.31)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cert2020-06.jpg" alt="">
						<p class="pr_txt">
							Hi-Seoul 브랜드기업 지정서<br>
							<span>(2020)</span>
						</p>
					</li>
				</ul>
			</div>
			<div class="part mt30">
				<p class="pr_tit">2019</p>
				<ul>
					<li>
						<img src="/images/sub/cert2019-10.jpg" alt="">
						<p class="pr_txt">
							서울형 강소기업 선정<br>
							<span>(2018.8.10~2020.12.31)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2019-2.jpg" alt="">
						<p class="pr_txt">
							ATEN Partner Program<br>
							<span>(2019.1.1~2019.12.31)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2019_hp.jpg" alt="">
						<p class="pr_txt">
							HP Gold Partner<br>
							<span>(2018.11.1~2019.4.30)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2019.jpg" alt="">
						<p class="pr_txt">
							HPE Gold Partner<br>
							<span>(Fiscal Year 2019)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/iso9001.jpg" alt="">
						<p class="pr_txt">
							ISO 9001<br>
							<span>(2018.12.24~2021.12.23)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/samsung2019.jpg" alt="">
						<p class="pr_txt">
							삼성 스타점<br>
							<span>(2019)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/mainbiz2019.jpg" alt="">
						<p class="pr_txt">
							MAIN BIZ<br>
							<span>(2019.3.20~2022.3.19)</span>
						</p>
					</li>
					<li>
				<img src="/images/sub/sq2019.jpg" alt="">
				<p class="pr_txt">
					한국서비스품질우수기업<br>
					<span>(2019.7.31~2022.7.30)</span>
				</p>
			</li>
			<li>
				<img src="/images/sub/cer2019_3.jpg" alt="">
				<p class="pr_txt">
					경영혁신 우수사례 공모전<br>
					<span>(2019.11.20)</span>
				</p>
			</li>
			<li>
				<img src="/images/sub/cer2019_4.jpg" alt="">
				<p class="pr_txt">
					일·생활 균형 캠페인 확인서<br>
					<span>(2019.11.18~2021.11.17)</span>
				</p>
			</li>
				</ul>
			</div>
			<div class="part">
				<p class="pr_tit">2018</p>
				<ul>
				<li>
						<img src="/images/sub/cer2018-6.jpg" alt="">
						<p class="pr_txt">
							기업부설연구 인증서<br>
							<span>(2018)</span>
						</p>
					</li>
				<li>
						<img src="/images/sub/cer2018-5.jpg" alt="">
						<p class="pr_txt">
							HPE Gold Partner<br>
							<span>(Fiscal Year 2018)</span>
						</p>
					</li>

					<li>
						<img src="/images/sub/cer2018-1.gif" alt="">
						<p class="pr_txt">
							HP Gold Partner<br>
							<span>(2017.11.1~2018.4.30)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2018-2.gif" alt="">
						<p class="pr_txt">
							ATEN Partner Program<br>
							<span>(2018.1.1~2018.12.31)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2018-3.gif" alt="">
						<p class="pr_txt">
							Samsung Star Partner<br>
							<span>(2018)</span>
						</p>
					</li>
					<li class="last">
						<img src="/images/sub/cer2018-4.gif" alt="">
						<p class="pr_txt">
							Logitech Gold Partner<br>
							<span>(2018)</span>
						</p>
					</li>
				</ul>
			</div>
			<!-- //part -->

			<!-- part -->
			<div class="part mt30">
				<p class="pr_tit">2017</p>
				<ul>
					<li>
						<img src="/images/sub/cer2017.gif" alt="">
						<p class="pr_txt">
							HP Commercial Gold Partner<br>
							<span>(2017.5.1~2017.10.31)</span>
						</p>
					</li>
				</ul>
			</div>
			<!-- //part -->

			<!-- part -->
			<div class="part mt30">
				<p class="pr_tit">2016</p>
				<ul>
					<li>
						<img src="/images/sub/cer2016-biz.jpg" alt="">
						<p class="pr_txt">
							MAIN-BIZ 인증서<br>
							<span>(2016.2.25~2019.2.24)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2016.gif" alt="">
						<p class="pr_txt">
							Partner First Gold<br>
							<span>(2016.5.1~2016.10.31)</span>
						</p>
					</li>
				</ul>
			</div>
			<!-- //part -->
			<!-- part -->
			<div class="part">
				<p class="pr_tit">2015</p>
				<ul>
					<li>
						<img src="/images/sub/cer2015-1.gif" alt="">
						<p class="pr_txt">
							PPS GOLD Partner<br>
							<span>(2014.11.1~2015.4.30)</span>
						</p>
					</li>
					<li class="last">
						<img src="/images/sub/cer2015-2.gif" alt="">
						<p class="pr_txt">
							PPS Workstation<br>
							<span>(2014.11.1~2015.4.30)</span>
						</p>
					</li>
				</ul>
			</div>
			<!-- //part -->
			<!-- part -->
			<div class="part">
				<p class="pr_tit">2014</p>
				<ul>
					<li>
						<img src="/images/sub/cer2014-1.gif" alt="">
						<p class="pr_txt">
							PPS GOLD Partner<br>
							<span>(2013.11.1~2014.4.30)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2014-2.gif" alt="">
						<p class="pr_txt">
							PPS GOLD Partner<br>
							<span>(2014.5.1~2014.10.31)</span>
						</p>
					</li>
					<li class="last">
						<img src="/images/sub/cer2014-3.gif" alt="">
						<p class="pr_txt">
							HP PPS WINNERS’ CLUB<br>
							<span>(2014)</span>
						</p>
					</li>
				</ul>
			</div>
			<!-- //part -->
			<!-- part -->
			<div class="part">
				<p class="pr_tit">2013</p>
				<ul>
					<li>
						<img src="/images/sub/cer2013-1.gif" alt="">
						<p class="pr_txt">
							PPS Preferred Partner<br>
							<span>(2013.5.1~2013.10.31)</span>
						</p>
					</li>
					<li class="last">
						<img src="/images/sub/cer2013-2.gif" alt="">
						<p class="pr_txt">
							PPS Preferred Partner<br>
							<span>(2012.11.1~2013.4.30)</span>
						</p>
					</li>
				</ul>
			</div>
			<!-- //part -->
			<!-- part -->
			<div class="part">
				<p class="pr_tit">2012</p>
				<ul>
					<li>
						<img src="/images/sub/cer2012-1.gif" alt="">
						<p class="pr_txt">
							Achievers’ Club 2012<br>
							<span>(2012)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2012-2.gif" alt="">
						<p class="pr_txt">
							PSG Preferred Business<br>
							<span>(2012.5.1~2012.10.31)</span>
						</p>
					</li>
					<li class="last">
						<img src="/images/sub/cer2012-3.gif" alt="">
						<p class="pr_txt">
							PSG Premier Business<br>
							<span>(2011.11.1~2012.4.30)</span>
						</p>
					</li>
				</ul>
			</div>
			<!-- //part -->
			<!-- part -->
			<div class="part">
				<p class="pr_tit">2011</p>
				<ul>
					<li>
						<img src="/images/sub/cer2011-1.gif" alt="">
						<p class="pr_txt">
							PSG  Premier  Business<br>
							<span>(2010.11.1~2011.4.30)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2011-2.gif" alt="">
						<p class="pr_txt">
							ESSN Silver Partner<br>
							<span>(2010.11.1~2011.4.30)</span>
						</p>
					</li>
					<li>
						<img src="/images/sub/cer2011-3.gif" alt="">
						<p class="pr_txt">
							PSG  Premier  Business<br>
							<span>(2011.5.1~2011.10.31)</span>
						</p>
					</li>
					<li class="last">
						<img src="/images/sub/cer2011-4.gif" alt="">
						<p class="pr_txt">
							ESSN Silver Partner<br>
							<span>(2011.5.1~2011.10.31)</span>
						</p>
					</li>
				</ul>
			</div>
			<!-- //part -->
		</div>
		</div>
		<div id="A_Container_R">
			<h3 class="sub_tit">회사소개</h3>
			<!-- #include virtual="_inc/right_about_cpy.asp" -->
			<!-- #include virtual="_inc/right_bnn.asp" -->
		</div>

<!-- #include virtual="/_inc/_footer.asp" -->