 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
		<script type="text/javascript">
	$(document).on('ready', function() {
    	$(".sub_lnb li:nth-child(8)").addClass('on');
    });
</script>
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
	<!-- 서브 네비게이션 -->
	<div id="sub_nav">
		<div class="inner">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">대표 인사말</a></li>
			</ul>
		</div>
	</div>
	<div id="A_Container">
		<!-- 서브컨텐츠 -->
		<div id="A_Container_C">
			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>찾아오시는 길</h3>
			</div>
			<div id="content" class="map">

			<!-- * Daum 지도 - 지도퍼가기 -->
			<!-- 1. 지도 노드 -->
			<div id="daumRoughmapContainer1464588300537" class="root_daum_roughmap root_daum_roughmap_landing"></div>

			<!--
				2. 설치 스크립트
				* 지도 퍼가기 서비스를 2개 이상 넣을 경우, 설치 스크립트는 하나만 삽입합니다.
			-->
			<script charset="UTF-8" class="daum_roughmap_loader_script" src="http://dmaps.daum.net/map_js_init/roughmapLoader.js"></script><script charset="UTF-8" src="http://t1.daumcdn.net/kakaomapweb/place/jscss/roughmap/bdd89fff/roughmapLander.js"></script>

			<!-- 3. 실행 스크립트 -->
			<script charset="UTF-8">
				new daum.roughmap.Lander({
					"timestamp" : "1464588300537",
					"key" : "bzjg",
					"mapWidth" : "950",
					"mapHeight" : "419"
				}).render();
			</script>
			<p class="cont_tit mt30">(주)신성씨엔에스</p>
			<dl style="border-top:2px solid #2570c3;">
				<dt>주소</dt>
				<dd>서울 구로구 디지털로 272, 한신IT타워 5층 </dd>
			</dl>
			<dl>
				<dt>Call Us </dt>
				<dd>02-867-2626</dd>
			</dl>
			<dl>
				<dt>Fax </dt>
				<dd>02-867-2621</dd>
			</dl>
			<dl>
				<dt>E-Mail </dt>
				<dd>psh@sinsungcns.com</dd>
			</dl>
			<p class="btn_print fl_right"><a onclick="javascript:print(document.getElementById('content').innerHTML)" style="cursor:pointer"><img src="/images/sub/icon_print.png" alt="">&nbsp;&nbsp;출력하기</a></p>
		</div>
		</div>
		<div id="A_Container_R">
			<h3 class="sub_tit">회사소개</h3>
			<!-- #include virtual="/_inc/right_about_cpy.asp" -->
			<!-- #include virtual="/_inc/right_bnn.asp" -->
		</div>

<!-- #include virtual="/_inc/_footer.asp" -->