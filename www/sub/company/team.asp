 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
			<script type="text/javascript">
			$(document).on('ready', function() {
				$(".sub_lnb li:nth-child(4)").addClass('on');
			});
		</script>
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
	<!-- 서브 네비게이션 -->
	<div id="sub_nav">
		<div class="inner">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">팀을 만나다</a></li>
			</ul>
		</div>
	</div>
	<div id="A_Container">
		<!-- 서브컨텐츠 -->
		<div id="A_Container_C">
			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>팀을 만나다</h3>
			</div>
			<div class="team" id="content">
					<div class="titBox">
				<dl>
					<dt>Meet<br>
					The Team</dt>
					<dd class="blue_tit blue">Commitment without boundaries</dd>
					<dd>우리는 고객과 동료의 성장을 위해 최선을 다합니다. <br>
					심지어 회계팀도 IT 지식이 있어야 내∙외부적으로 소통이 가능합니다.<br>
					그만큼 우리는 IT 전문지식을 학습하고 이식하는 것에 전력을 다합니다. <br>
					지식은 곧 우리가 고객에게 드릴 수 있는 가장 기본이면서 강력한 본질이기 때문입니다. <br>
					고객과 고객사의 성장은 우리의 심장을 뛰게 하고, 전 직원이 한 마음으로 매우 자랑스럽게 생각합니다.</dd>
				</dl>
			</div>

			<div class="section">
				<p class="team_tit">IT Consultant Team</p>
				<ul>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_1.jpg" alt=""></p>
						<p class="team_txt"><em>차장</em>&nbsp;송강기</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_3.jpg" alt=""></p>
						<p class="team_txt"><em>과장</em>&nbsp;윤경호 </p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_3new.jpg" alt=""></p>
						<p class="team_txt"><em>과장</em>&nbsp;김신철 </p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_7.jpg" alt=""></p>
						<p class="team_txt"><em>대리</em>&nbsp;김영진</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_5.jpg" alt=""></p>
						<p class="team_txt"><em>과장</em>&nbsp;이혜빈</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_6_old.jpg" alt=""></p>
						<p class="team_txt"><em>대리</em>&nbsp;이상규</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_9.jpg" alt=""></p>
						<p class="team_txt"><em>주임</em>&nbsp;이훈석</p>
					</li>
				</ul>
			</div>

			<div class="section">
				<p class="team_tit">IT Technical Service team</p>
				<ul>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_11.jpg" alt=""></p>
						<p class="team_txt"><em>과장</em>&nbsp;동근도</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_12.jpg" alt=""></p>
						<p class="team_txt"><em>과장</em>&nbsp;이주홍</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_13.jpg" alt=""></p>
						<p class="team_txt"><em>대리</em>&nbsp;박정현</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_17.jpg" alt=""></p>
						<p class="team_txt"><em>스타</em>&nbsp;서명훈</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_15new.jpg" alt=""></p>
						<p class="team_txt"><em>주임</em>&nbsp;남덕우</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_16new.jpg" alt=""></p>
						<p class="team_txt"><em>주임</em>&nbsp;김범석</p>
					</li>
				<ul>
				</ul>
			</ul>
			</div>
			<div class="section">
				<p class="team_tit">Management innovation team</p>
				<ul>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_19.jpg" alt=""></p>
						<p class="team_txt"><em>COO</em>&nbsp;박세화</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_22.jpg" alt=""></p>
						<p class="team_txt"><em>과장</em>&nbsp;이보람</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_23.jpg" alt=""></p>
						<p class="team_txt"><em>주임</em>&nbsp;황수현</p>
					</li>
					<li class="last">
						<p class="team_img"><img src="/images/sub/team_i_24.jpg" alt=""></p>
						<p class="team_txt"><em>BH컨설턴트</em>&nbsp;박진호</p>
					</li>
					<li class="last">
						<p class="team_img"><img src="/images/sub/team_i_new4.jpg" alt=""></p>
						<p class="team_txt"><em>팀장</em>&nbsp;정우현</p>
					</li>
					<li class="last">
						<p class="team_img"><img src="/images/sub/team_i_new5.jpg" alt=""></p>
						<p class="team_txt"><em>스타</em>&nbsp;김진수</p>
					</li>
				</ul>
			</div>
			<div class="section">
				<p class="team_tit">Logistics team</p>
				<ul>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_26.jpg" alt=""></p>
						<p class="team_txt"><em>대리</em>&nbsp;이홍선</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_28.jpg" alt=""></p>
						<p class="team_txt"><em>주임</em>&nbsp;정기섭</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_28new.jpg" alt=""></p>
						<p class="team_txt"><em>주임</em>&nbsp;이상현</p>
					</li>
					<li>
						<p class="team_img"><img src="/images/sub/team_i_29new.jpg" alt=""></p>
						<p class="team_txt"><em>주임</em>&nbsp;이규황</p>
					</li>
				</ul>
			</div>
			<div id="content" class="psCon">
				<ul class="img">
					<li><img src="/images/sub/img_ps1.jpg"></li>
					<li><img src="/images/sub/img_ps2.jpg"></li>
					<li><img src="/images/sub/img_ps3.jpg"></li>
					<li><img src="/images/sub/img_ps4.jpg"></li>
					<li><img src="/images/sub/img_ps5.jpg"></li>
				</ul>
			</div>

			</div>
		</div>


		<div id="A_Container_R">
			<h3 class="sub_tit">회사소개</h3>
			<!-- #include virtual="/_inc/right_about_cpy.asp" -->
			<!-- #include virtual="/_inc/right_bnn.asp" -->
		</div>

<!-- #include virtual="/_inc/_footer.asp" -->