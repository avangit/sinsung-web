 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(5)").addClass('on');
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">인재경영</a></li>
 					<li><a href="#">복지제도</a></li>
 				</ul>
 			</div>
 		</div>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
 				<div class="title">
 					<h3>복지제도</h3>
 				</div>
 				<div id="content" class="wfCon">
 					<div class="tit" style="background:none">
 						<h4>행복한 트러스타가 되기 위한 노력</h4>
 					</div>
 					<dl>
 						<dt class="skyblue">Meaning</dt>
 						<dd>IT전문가로서 기업의 올바른 업무환경 구현을 위하는 <em class="skyblue">일에 자부심</em>을 가지고</dd>
 						<dt class="skyblue">Autonomy</dt>
 						<dd>자율 속에서 <em class="skyblue">주도적으로 참여</em>하고 스스로 결정하며</dd>
 						<dt class="skyblue">Growth</dt>
 						<dd><em class="skyblue">일과 학습</em>을 통해서 배우고 성장하고 <em class="skyblue">지식 나눔</em>하며</dd>
 						<dt class="skyblue">Systom</dt>
 						<dd><em class="skyblue">탁월한 성과를 창출</em>하고 이에 대한 <em class="skyblue">인정과 보상</em>을 받으며</dd>
 						<dt class="skyblue">Connection</dt>
 						<dd><em class="skyblue">뛰어난 동료</em>와 더불어 하나되어</dd>
 						<dt class="skyblue">Fun</dt>
 						<dd><em class="skyblue">감사를 통해 긍정적</em>으로, 보다 많은 경험을 통해~ 재미있게~ 신나게 </dd>
 						<dt class="skyblue">Safety</dt>
 						<dd>내가 <em class="skyblue">나를 책임지는 주인으로서의 삶</em>을 사는 것</dd>
 					</dl>
 					<!--
			<div>
				<ul>
					<li>
						<p><img src="images/sub/img_wf1.gif" alt=""></p>
						<dl>
							<dt>생활안정</dt>
							<dd>경조사 및 휴가</dd>
							<dd>출산선물, 초등학교 입학축하금 지원</dd>
							<dd>미취학자녀, 초등학교, 중학교 보육비 지원</dd>
							<dd>설날/추석 선물 및 지원금</dd>
							<dd>예체능 동호회 운영</dd>
						</dl>
					</li>
					<li>
						<p><img src="images/sub/img_wf2.gif" alt=""></p>
						<dl>
							<dt>52주 문화 플랜</dt>
							<dd>오피스 올림픽</dd>
							<dd>깃발나눔정복 워크샵</dd>
							<dd>이색체험 워크샵</dd>
							<dd>어버이날 이벤트</dd>
							<dd>크리스마스 이벤트 및 타임캡슐</dd>
							<dd>생일파티, 결혼기념일 </dd>
						</dl>
					</li>
					<li>
						<p><img src="images/sub/img_wf3.gif" alt=""></p>
						<dl>
							<dt>포상 제도</dt>
							<dd>지식페스티벌</dd>
							<dd>올해의 리더상, 올해의 트러스타상</dd>
							<dd>개근상</dd>
							<dd>장기근속포상</dd>
						</dl>
					</li>
					<li>
						<p><img src="images/sub/img_wf4.gif" alt=""></p>
						<dl>
							<dt>건강증진</dt>
							<dd>종합 건강검진</dd>
							<dd>중식 무료 제공</dd>
						</dl>
					</li>
					<li>
						<p><img src="images/sub/img_wf5.gif" alt=""></p>
						<dl>
							<dt>법정 복리후생</dt>
							<dd>주 5일 근무제 </dd>
							<dd>국민연금</dd>
							<dd>고용보험</dd>
							<dd>건강보험, 산재재해보상보험</dd>
							<dd>퇴직연금 (1년이상자)</dd>
						</dl>
					</li>
				</ul>
			</div>
-->
 					<div class="wel_img">
 						<ul>
 							<li>
 								<div class="bg"></div><strong class="tit">생활안정</strong>
 								<p>· 경조사 및 휴가, 출산선물, 초등학교 입학축하금 지원 <br>
 									· 미취학자녀, 초등학교, 중학교 보육비 지원 <br>
 									· 설날/추석 선물 <br>
 									· 단체 상해 보험</p>
 							</li>
 							<li>
 								<div class="bg"></div><strong class="tit">52주 문화 플랜</strong>
 								<p class="left">깃발나눔정복 워크샵 <br>
 									이색체험 워크샵 <br>
 									어버이날 이벤트 <br>
 									크리스마스 이벤트 <br>
 									생일파티, 결혼기념일 외식상품권 지급</p>
 							</li>
 							<li>
 								<div class="bg"></div><strong class="tit">포상제도</strong>
 								<p>· 지식페스티벌 <br>
 									· 올해의 리더상, 올해의 트러스타상 개근상 <br>
 									· 장기근속포상(3년, 5년)</p>
 							</li>
 							<li>
 								<div class="bg"></div><strong class="tit">성과공유</strong>
 								<p>· PS (Profit Share) <br>
 									· GA (Goal Achievement)</p>
 							</li>
 							<li>
 								<div class="bg"></div><strong class="tit">법적복리후생</strong>
 								<p class="left">주 5일 근무제 <br>
 									국민연금 <br>
 									고용보험 <br>
 									건강보험, 산재재해보상보험 <br>
 									퇴직연금 DB형</p>
 							</li>
 							<li>
 								<div class="bg"></div><strong class="tit">건강증진</strong>
 								<p class="left">종합 건강검진 지원(본인, 배우자) <br>
 									중식 무료 제공</p>
 							</li>

 						</ul>

 					</div>



 				</div>
 			</div>
			<div id="A_Container_R">
				<h3 class="sub_tit">인재경영</h3>
				<!-- #include virtual="/_inc/right_recruit.asp" -->
				<!-- #include virtual="/_inc/right_bnn.asp" -->
			</div>

<!-- #include virtual="/_inc/_footer.asp" -->