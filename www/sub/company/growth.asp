 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<script type="text/javascript">
			$(document).on('ready', function() {
				$(".sub_lnb li:nth-child(2)").addClass('on');
			});
		</script>
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
	<!-- 서브 네비게이션 -->
	<div id="sub_nav">
		<div class="inner">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">회사성장</a></li>
			</ul>
		</div>
	</div>
	<div id="A_Container">
		<!-- 서브컨텐츠 -->
		<div id="A_Container_C">
			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>회사성장</h3>
			</div>
			<div id="content" class="growth">
					<h4>신용등급 <span>BB+</span> / 현금흐름등급 <span>B</span> / Watch등급 <span>정상</span> <em> (평가기관 : ㈜이크레더블 2018년 재무결산일)</em></h4>
				<div class="img_box">
					<img src="/images/sub/growth.jpg" alt="회사성장 그래프"/>
				</div>
			</div>
		</div>
		<div id="A_Container_R">
			<h3 class="sub_tit">회사소개</h3>
			<!-- #include virtual="/_inc/right_about_cpy.asp" -->
			<!-- #include virtual="/_inc/right_bnn.asp" -->
		</div>

<!-- #include virtual="/_inc/_footer.asp" -->