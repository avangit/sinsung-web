 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(4)").addClass('on');

			$(".tab-slider--body").hide();
 			$(".tab-slider--body:first").show();
			$(".tab-slider--nav li").click(function() {
				$(".tab-slider--body").hide();
				var activeTab = $(this).attr("rel");
				$("#" + activeTab).fadeIn();

				if ($(this).attr("rel") == "tab1") {
					$(".tab-slider--nav .slide").animate({
						left: "0"
					}, 1);
				} else if ($(this).attr("rel") == "tab2") {
					$(".tab-slider--nav .slide").animate({
						left: "20%"
					}, 1);
				} else if ($(this).attr("rel") == "tab3") {
					$(".tab-slider--nav .slide").animate({
						left: "40%"
					}, 1);
				} else if ($(this).attr("rel") == "tab4") {
					$(".tab-slider--nav .slide").animate({
						left: "60%"
					}, 1);
				} else if ($(this).attr("rel") == "tab5") {
					$(".tab-slider--nav .slide").animate({
						left: "80%"
					}, 1);
				}
				$(".tab-slider--nav li").removeClass("active");
				$(this).addClass("active");
			});

 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">인재경영</a></li>
 					<li><a href="#">인재상</a></li>
 				</ul>
 			</div>
 		</div>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
 				<div class="title">
 					<h3>인재상</h3>
 				</div>
 				<div id="content" class="psCon">

 					<!-- 탭영역 -->
 					<div class="tab-slider--nav">
 						<ul class="tab-slider--tabs">
 							<li class="tab-slider--trigger active" rel="tab1">신뢰</li>
 							<li class="tab-slider--trigger" rel="tab2">열정</li>
 							<li class="tab-slider--trigger" rel="tab3">성장</li>
 							<li class="tab-slider--trigger" rel="tab4">고객중심</li>
 							<li class="tab-slider--trigger" rel="tab5">감사</li>
 						</ul>
 						<div class="slide" style="left: 0%;"></div>
 					</div>
 					<!-- //탭영역 -->

 					<!-- 탭컨텐츠영역 -->
 					<div class="tab-slider--container">

 						<!-- tab1 -->
 						<div id="tab1" class="tab-slider--body">
 							<ul class="psTxt">
 								<li class="ps1">
 									<p>신뢰</p>
 									<dl>
 										<dt><span>What</span>신성은 보이지 않는 끈의 가치를 믿는다.</dt>
 										<dd class="first">
 											<p>Why</p>
 											<ul>
 												<li>1. 신뢰는 비즈니스의 시작이자 마지막이다.</li>
 												<li>2. 신뢰를 잃는 것은 모든 것을 잃는 것을 의미한다.</li>
 											</ul>
 										</dd>
 										<dd>
 											<p>How</p>
 											<ul>
 												<li>1. 고객과의 약속은 반드시 지킨다.</li>
 												<li>2. 당장의 위기를 모면하기 위한 편법이나 요행을 추구하지 않는다.</li>
 												<li>3. 정직하게 일하며, 올바르게 수익을 낸다.</li>
 											</ul>
 										</dd>
 									</dl>
 								</li>
 							</ul>
 							<p class="cont_txt mb40">우리는 고객과의 신뢰, 직장 동료들간의 신뢰를 중요하게 여깁니다. 하지만 처음 만나는 사람과 신뢰를 쌓기란 쉽지 않은 일입니다.<br>
 								결국 업무적, 개인적으로 신뢰를 구축하는 유일한 방법은 신뢰할 수 있는 사람이 되는 것이라 생각합니다. </p>
 							<dl class="numper mb40">
 								<dt class="sol_tit skyblue">신성이 생각하는 신뢰란?</dt>
 								<dd>첫 째. 고객의 니즈를 정확히 파악하는 것</dd>
 								<dd>둘 째. 전문성을 바탕으로 고객에게 더 나은 정보를 제공해 주는 것</dd>
 								<dd>셋 째. 합리적인 가격을 제안하는 것</dd>
 								<dd>넷 째. 요청한 납기일에 최대한 맞추는 것</dd>
 							</dl>
 							<p class="cont_txt">고객과의 신뢰가 형성되면 그 관계는 오랫동안 지속될 수 있다고 믿습니다. <br>
 								이것은 바로 신성의 중요한 자산이자 핵심가치입니다. </p>
 							<p class="cont_txt">신뢰는 당당한 책임 앞에 성립합니다. <br>
 								책임 앞에 당당하지 않는 사람은 신뢰할 수 없는 사람입니다. <br>
 								인생은 무한한 신뢰와 책임으로 묶어야 든든할 수 있습니다. </p>
 							<div class="fmBx">
 								<p class="fmcom">
 									<i class="fa fa-quote-left" aria-hidden="true"></i>
 									<em>신뢰는 유리거울 같은 것이다. <br>
 										한번 금이 가면 원래대로 하나가 될 수 없다.</em>
 									<i class="fa fa-quote-right" aria-hidden="true"></i>
 								</p>
 								<p class="fmName">- 존 F. 케네디</p>
 							</div>
 						</div>
 						<!-- //tab1 -->

 						<!-- tab2 -->
 						<div id="tab2" class="tab-slider--body">
 							<ul class="psTxt">
 								<li class="ps2">
 									<p>열정</p>
 									<dl>
 										<dt><span>What</span>신성인의 혈액형은 Passion이다.</dt>
 										<dd class="first">
 											<p>Why</p>
 											<ul>
 												<li>1. 열정은 우리의 성장 엔진이다.</li>
 												<li>2. 열정은 역경과 고난이란 벽을 뛰어넘게 하는 디딤돌이다.</li>
 											</ul>
 										</dd>
 										<dd>
 											<p>How</p>
 											<ul>
 												<li>1. 내가 하는 일의 주인이 되어 일을 진행한다.</li>
 												<li>2. 무슨 일을 하든지 일하는 과정 속에서 최선을 다한다.</li>
 												<li>3. 동료와 서로 즐기면서 일하고, 긍정적 관계를 유지한다.</li>
 											</ul>
 										</dd>
 									</dl>
 								</li>
 							</ul>
 							<p class="sol_tit skyblue">열정은 곧 I can do it!</p>
 							<p class="cont_txt">해결해야 할 문제가 아무리 어렵고 험난하다고 해서 포기를 하면 앞으로 나아갈 수 없지만 열정을 갖고 극복하려고 한다면<br>최소한 멈춰있진 않는다고 생각합니다.
 								멈춰있지 않는 사고는 굉장히 중요합니다.</p>
 							<p class="cont_txt">어렸을 적 정주영 회장의 성공 스토리를 읽으면서 누구나 열정을 가지고 목표를 향해 달려가면 성공할 수 있다라는 메시지를 강하게 전달받게 되었고<br>매사에 열정적으로 살아가려고 노력하였습니다.<br>하지만 사회생활을 거듭할수록 실패를 두려워하고 부정적인 생각들을 많이 하게 되면서 열정에 대한 가치를 잊고 살아가고 있는<br>나 자신을 발견하게 되었습니다.<br>
 								하지만 다시금 열정의 가치가 얼마나 중요한지 일깨워준 계기가 있었습니다.</p>
 							<p class="cont_txt">
 								신성CNS을 설립하고 5년이 지난 어느 날, 새로 입사한지 얼마 안 된 직원이 밤새도록 업무를 보고 퇴근하기를 반복하는 모습을 보고 이상하게 여겨<br>
 								‘당신은 일이 많아서 매일마다 야근을 하는 것인가요?’ 라고 묻자 그 직원은 ‘일이 많아서 야근을 하는 것이 아닙니다. 일을 더 만들기 위해 야근을 하는 것<br>입니다.’ 라며
 								웃으며 대답하는 것을 보고 회사를 생각하는 것이 나뿐만이 아니구나 라는 것을 느끼며 우리 회사를 열정적인 회사로 만들려고 노력하였습니다.
 							</p>
 							<p class="cont_txt">우리는 모두 주인의식을 가지고 회사를 성장시키려고 노력하고 또 노력하고 있습니다. <br>이것이야말로 신성이 가지고 있는 열정입니다.</p>
 							<div class="fmBx">
 								<p class="fmcom">
 									<i class="fa fa-quote-left" aria-hidden="true"></i>
 									<em>너는 왜 평범하게 노력하는가, 시시하게 살기를 원치 않으면서!</em>
 									<i class="fa fa-quote-right" aria-hidden="true"></i>
 								</p>
 								<p class="fmName">- 존 F. 케네디</p>
 							</div>
 						</div>
 						<!-- //tab2 -->

 						<!-- tab3 -->
 						<div id="tab3" class="tab-slider--body" style="display: block;">
 							<ul class="psTxt">
 								<li class="ps3">
 									<p>성장</p>
 									<dl>
 										<dt><span>What</span>신성인은 언제나 최고를 지향한다.</dt>
 										<dd class="first">
 											<p>Why</p>
 											<ul>
 												<li>1. 전문가는 성장을 멈추지 않는다.</li>
 												<li>2. 우리가 성장하는 만큼 고객가치에 기여할 수 있다.</li>
 											</ul>
 										</dd>
 										<dd>
 											<p>How</p>
 											<ul>
 												<li>1. 항상 겸손한 태도를 유지하고 학습한다.</li>
 												<li>2. 새로운 변화에 민첩하게 반응하고 지식은 서로 공유한다.</li>
 												<li>3. 우리 성장 도구(독서/피드백)의 질을 높힌다.</li>
 											</ul>
 										</dd>
 									</dl>
 								</li>
 							</ul>
 							<p class="cont_txt">HP의 성장 노하우를 신성에 적용 시키려고 노력하였습니다. <br>
 								HP의 성장 노하우는 목표와 동기부여를 함께 준다는 것에 큰 강점을 가지고 있습니다. 매출 목표를 지정해 주고 달성했을 시 적절한 보상을 제공해 주고<br>더 높은 목표를 다시 지정해 주는 것을 반복함으로써 자연스럽게 성장을 이끌어 줍니다. HP의 기업문화 ‘HP Way’중 하나인 이윤분배제도에 해당한다고<br>볼 수 있습니다.</p>
 							<p class="cont_txt">이윤분배제도(Profit Sharing System)는 휴렛팩커드의 주요한 특징입니다. <br>
 								회사의 이익은 이에 기여한 구성원과 공동으로 분배되어야 한다는 이념 하에 휴렛팩커드는 세전 이익의 12%를 구성원들에게 분배하는 이윤분배제도를<br>전 세계적으로 적용하고 있습니다.</p>
 							<p class="cont_txt">구체적이고 실현 가능한 목표를 정하면 적어도 방향을 잃지 않고 한걸음 한걸음 성장해 나아갈 수 있습니다.<br>
 								우리 신성인은 자신의 목표를 정확히 알고 꾸준히 성장해 나가는 인재들입니다.</p>
 							<div class="fmBx">
 								<p class="fmcom">
 									<i class="fa fa-quote-left" aria-hidden="true"></i>
 									<em>자신의 몸, 정신, 영혼에 대한 자신감이야 말로 새로운 모험, 새로운 성장 방향, <br>새로운 교훈을 계속 찾아 나서게 하는 원동력이며, 바로 이것이 인생이다.</em>
 									<i class="fa fa-quote-right" aria-hidden="true"></i>
 								</p>
 								<p class="fmName">- 오프라 윈프리</p>
 							</div>
 						</div>
 						<!-- //tab3 -->

 						<!-- tab4 -->
 						<div id="tab4" class="tab-slider--body">
 							<ul class="psTxt">
 								<li class="ps4">
 									<p>고객중심서비스</p>
 									<dl>
 										<dt><span>What</span>신성의 참기쁨은 고객이 감동하는 순간이다.</dt>
 										<dd class="first">
 											<p>Why</p>
 											<ul>
 												<li>1. 신성의 고객은 신성의 가족이다.</li>
 												<li>2. 우리는 언제나 우리 가족이 행복하기를 바란다.</li>
 											</ul>
 										</dd>
 										<dd>
 											<p>How</p>
 											<ul>
 												<li>1. 고객의 입장에서 생각하고 응대한다.</li>
 												<li>2. 고객의 잠재적 니즈를 발견하고 채운다.</li>
 												<li>3. KSP(Kind/Speed/PERFECT) A/S 기본원칙을 지킨다.</li>
 											</ul>
 										</dd>
 									</dl>
 								</li>
 							</ul>
 							<p class="cont_txt">우리 회사는 고객이 손해를 보지 않게 하는 것은 물론, 더 많은 이득과 행복을 드리기 위해 고객중심 서비스 경영을 중요시 여깁니다.</p>
 							<p class="cont_txt mb30">영업을 하는 것은 마치 좋아하는 이성에게 대시를 하는 것이라고 종종 비유하곤 합니다.<br>
 								이성의 마음을 얻고 움직이기 위해서는 적극적인 자세로 자신을 어필하고 상대방이 무엇을 좋아하는지 어떤 성격인지 빨리 파악하는 것이 중요 합니다. <br>
 								영업도 마찬가지로 고객에게 우리 회사가 어떤 회사인지 소개하고 어떠한 서비스를 찾고 있는지를 신속하게 파악하여 합리적인 가격을 제시하고<br>
 								적극적인 자세로 사후관리를 해주는 것이 고객의 만족도를 높이고 신뢰를 줄 수 있는 중요한 요소들이라고 생각합니다.</p>
 							<dl class="numper mb40">
 								<dt class="sol_tit skyblue">사람의 마음을 사로잡는 6가지 불변의 법칙</dt>
 								<dd>상호성의 법칙 : 샘플을 받아 본 상품은 사게 될 가능성이 높다.</dd>
 								<dd>- 일관성의 법칙 : 내가 선택한 상품과 서비스가 최고라고 믿고 싶어 한다.</dd>
 								<dd>- 사회적 증거의 법칙 : ‘가장 많이 팔린 상품’은 ‘더 많이’ 팔릴 것이다.</dd>
 								<dd>- 호감의 법칙 : 잘생긴 피의자가 무죄판결 받을 가능성이 높다.</dd>
 								<dd>- 권위의 법칙 : 상 받은 상품, 큰 체구, 높은 직책, 우아한 옷차림에 약하다.</dd>
 								<dd>- 희귀성은 법칙 : 한정 판매, 백화점 세일 마지막날에 사람이 몰린다.</dd>

 							</dl>
 							<div class="fmBx">
 								<p class="fmcom">
 									<i class="fa fa-quote-left" aria-hidden="true"></i>
 									<em>당신이 필요한 사람에게 자비를 베풀지 않는다면 <br>
 										어떻게 당신 고객이 당신을 달리 대접할 것을 기대할 수 있겠는가?</em>
 									<i class="fa fa-quote-right" aria-hidden="true"></i>
 								</p>
 								<p class="fmName">- 케몬스 윌슨</p>
 							</div>
 						</div>
 						<!-- //tab4 -->

 						<!-- tab5 -->
 						<div id="tab5" class="tab-slider--body">
 							<ul class="psTxt">
 								<li class="ps5">
 									<p>감사</p>
 									<dl>
 										<dt><span>What</span>신성인은 감사라는 거울을 통해 겸손을 유지한다.</dt>
 										<dd class="first">
 											<p>Why</p>
 											<ul>
 												<li>1. 감사는 우리가 하는 일의 소중함을 일깨워 준다.</li>
 												<li>2. 감사는 고객을 사랑하는 마음이다.</li>
 											</ul>
 										</dd>
 										<dd>
 											<p>How</p>
 											<ul>
 												<li>1. 우리가 하는 일 속에서 감사함을 찾는다.</li>
 												<li>2. 어떠한 상황에도 절대 감사를 실천한다.</li>
 												<li>3. 동료와 고객에게 항상 감사한 마음을 갖는다.</li>
 											</ul>
 										</dd>
 									</dl>
 								</li>
 							</ul>
 							<p class="cont_txt">감사하는 마음은 우리 스스로에게 성장의 발판을 마련해 주고 성숙과 미성숙을 구분 짓는 중요한 기준이 됩니다. <br>
 								또한 정서적, 인격적, 긍정적 사고를 위해 필요하며 타인의 평가에도 유리해집니다.</p>
 							<p class="cont_txt">상대방에게 증오나 불신 등의 감정은 잘 전달이 되지 않습니다.<br>
 								타인에 대한 부정적인 생각과 불만은 칼 방향이 상대방을 향해 찌르는 것이 아닌, 나를 향해 찌르는 것과 같다고 생각합니다.<br>
 								반대로 상대방에게 항상 감사하는 마음을 갖고 있으면 증오, 배신, 불신과 같은 감정이 자라날 자리가 없으며 오히려 상대방을 존중하게 됩니다.</p>
 							<p class="cont_txt">신성은 매주 월요일 아침 회의 시간을 통해 전 직원이 서로를 향해 감사했던 일, 고마웠던 일에 대해 표현하는 자리를 갖습니다.</p>
 							<p class="cont_txt">이처럼, 우리 신성인은 가족, 친구, 회사 동료, 고개에게 항상 감사하는 마음을 갖고 살아갑니다.<br>
 								감사하다는 말을 아끼지 않고 사소한 것에도 감사함을 느낍니다.</p>
 							<div class="fmBx">
 								<p class="fmcom">
 									<i class="fa fa-quote-left" aria-hidden="true"></i>
 									<em>어떤 사람들은 장미가 가시를 가지고 있기 때문에 항상 불평을 합니다.<br>
 										저는 가시라는 것이 장미를 가지고 있기 때문에 감사함을 느낍니다.</em>
 									<i class="fa fa-quote-right" aria-hidden="true"></i>
 								</p>
 								<p class="fmName">- 알퐁스 카</p>
 							</div>
 						</div>
 						<!-- tab5 -->


 					</div>
 					<!-- //tab5 -->

 				</div>

 			</div>

			<div id="A_Container_R">
				<h3 class="sub_tit">인재경영</h3>
				<!-- #include virtual="/_inc/right_recruit.asp" -->
				<!-- #include virtual="/_inc/right_bnn.asp" -->
			</div>

<!-- #include virtual="/_inc/_footer.asp" -->