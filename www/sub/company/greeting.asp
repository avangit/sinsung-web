 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<script type="text/javascript">
			$(document).on('ready', function() {
				$(".sub_lnb li:nth-child(3)").addClass('on');
			});
		</script>
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
	<!-- 서브 네비게이션 -->
	<div id="sub_nav">
		<div class="inner">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">대표 인사말</a></li>
			</ul>
		</div>
	</div>
	<div id="A_Container">
		<!-- 서브컨텐츠 -->
		<div id="A_Container_C">
			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>대표 인사말</h3>
			</div>
			<div id="content" class="greeting">
				<div class="txt_box">
					<strong>안녕하십니까?<br/><em>(주)신성씨앤에스</em> 대표 전성우입니다.</strong>
					<p>2009년 7월과 2015년 11월 14일은 우리에게 아주 귀한 날입니다.</p>
					<p>2009년 7월은 우리가 고객사의 올바른 IT 업무환경 구현을 위해 기업이 각 社의 구매 기준과 예산 안에서 더 나은 구매 결정을 내릴 수 있도록 제품 전문지식, 솔루션 컨설팅, 기술 지원 서비스를 제공함으로써 고객 섬기기를 시작한 감사한 날입니다.</p>
					<p>2015년 11월 14일은 성장 초기로 신성씨앤에스의 모든 조직원이 하나되어 우리의 비전과 사명을 정의하고 나아가야 할 1년/3년/10년의 Horizon map을 설계한 소중한 날입니다.</p>
					<p>'고객에게 필요한 가치는 무엇이며, 그 가치는 과연 옳은 것인가?'<br/>또한 '더 나은 가치를 어떻게 기여할 것인가'를 우리의 핵심문제로 정의하고 이에 피드백 시스템을 통하여 끊임없이 학습하고 있습니다.</p>
					<p>그 가치의 첫 번째는 신성 리커버리 솔루션입니다.<br/>2015년부터는 신성 리커버리 솔루션을 통해 고객사의 시스템과 데이터 백업으로 업무의 연속성을 보장하여 생산성을 향상시키고, 10초의 빠른 복구로 비용과 시간을 최대 절감시킬 수 있습니다.<br/>두 번째는 자산관리 솔루션을 통한 IT 장비의 이력 관리를 편하고 쉽게 할 수 있도록 개발하고 있습니다.</p>
					<p>신성의 핵심가치인 신뢰, 열정, 감사정신, 성장으로 고객중심 서비스를 실천하겠습니다.</p>
					<p>감사합니다.</p>
				</div>
				<div class="img_box">
					<img src="/images/sub/img_greeting.jpg" alt="대표이사 이미지"/>
					<img src="/images/sub/sign.jpg" alt="(주)신성씨엔에스 대표이사 전성우" />
				</div>
			</div>
		</div>
		<div id="A_Container_R">
			<h3 class="sub_tit">회사소개</h3>
 			<!-- #include virtual="/_inc/right_about_cpy.asp" -->
			<!-- #include virtual="/_inc/right_bnn.asp" -->
		</div>

<!-- #include virtual="/_inc/_footer.asp" -->