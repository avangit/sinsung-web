<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/member/")
UploadForm.MaxFileLen = int(1024 * 1024 * 10)

strPwd		= SQL_Injection(Trim(UploadForm("strPwd")))
strEmail1	= SQL_Injection(Trim(UploadForm("strEmail1")))
strEmail2	= SQL_Injection(Trim(UploadForm("strEmail2")))
strPhone1	= SQL_Injection(Trim(UploadForm("strPhone1")))
strPhone2	= SQL_Injection(Trim(UploadForm("strPhone2")))
strPhone3	= SQL_Injection(Trim(UploadForm("strPhone3")))
strMobile1	= SQL_Injection(Trim(UploadForm("strMobile1")))
strMobile2	= SQL_Injection(Trim(UploadForm("strMobile2")))
strMobile3	= SQL_Injection(Trim(UploadForm("strMobile3")))
cZip		= SQL_Injection(Trim(UploadForm("cZip")))
cAddr1		= SQL_Injection(Trim(UploadForm("cAddr1")))
cAddr2		= SQL_Injection(Trim(UploadForm("cAddr2")))
mailYN		= SQL_Injection(Trim(UploadForm("mailYN")))
smsYN		= SQL_Injection(Trim(UploadForm("smsYN")))

'// 변수 가공
strEmail = strEmail1 & "@" & strEmail2
strPhone = strPhone1 & "-" & strPhone2 & "-" & strPhone3
strMobile = strMobile1 & "-" & strMobile2 & "-" & strMobile3

If smsYN <> "Y" Then
	smsYN = "N"
End If

If mailYN <> "Y" Then
	mailYN = "N"
End If

Call DbOpen()

strSQL = "SELECT strFile1, strFile2 FROM mTb_Member2 WHERE strId = '" & Request.Cookies("U_ID") & "'"
Set rs = dbconn.execute(strSQL)

If Not(rs.eof) Then
	rsFile = rs("strFile1")
End If

rs.close
Set rs = Nothing

If UploadForm("strFile1").FileName <> "" Then
	'// 업로드 가능 여부 체크
	If UploadFileChk(UploadForm("strFile1").FileName)=False Then
		Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
		Set UploadForm = Nothing
		Response.End
	End If

	'// 파일 용량 체크
	If UploadForm("strFile1").FileLen > UploadForm.MaxFileLen Then
		Response.Write "<script language='javascript'>alert('10MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
		Set UploadForm = Nothing
		Response.End
	End If

	'기존파일 삭제
	If rsFile <> "" Then
		UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile
	End If

	'// 파일 저장
	mFile = UploadForm("strFile1").Save(, False)
	mFile = UploadForm("strFile1").LastSavedFileName

	Sql = "UPDATE mTb_Member2 SET strFile1 =N'" & SQL_Injection(mFile) & "' WHERE strId = '" & Request.Cookies("U_ID") & "'"
	dbconn.execute(Sql)
'// 새로운 파일 업로드 없이 기존 파일 삭제만 선택한 경우
ElseIf UploadForm("strFile1").FileName = "" And UploadForm("ori_file1_del1") = "Y" Then

	'기존파일 삭제
	If rsFile <> "" Then
		UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile
	End If

	Sql = "UPDATE mTb_Member2 SET strFile1 ='' WHERE strId = '" & Request.Cookies("U_ID") & "'"
	dbconn.execute(Sql)
End If

Set UploadForm = Nothing

Sql = "UPDATE mTb_Member2 SET "
Sql = Sql & "strPhone = '" & strPhone & "', "
If strPwd <> "" Then
	Sql = Sql & "strPwd='" & Encrypt_Sha(strPwd) & "', "
End If
Sql = Sql & "strMobile = '" & strMobile & "', "
Sql = Sql & "smsYN = '" & smsYN & "', "
Sql = Sql & "strEmail = N'" & strEmail & "', "
Sql = Sql & "mailYN = '" & mailYN & "', "
Sql = Sql & "cZip = '" & cZip & "', "
Sql = Sql & "cAddr1 = N'" & cAddr1 & "', "
Sql = Sql & "cAddr2 = N'" & cAddr2 & "', "
Sql = Sql & "update_date = '" & Date() & "' "
Sql = Sql & " WHERE strId = '" & Request.Cookies("U_ID") & "'"

dbconn.execute(Sql)

Call DbClose()

Call jsAlertMsgUrl("수정되었습니다.", "mypage.asp")
%>