 <% @CODEPAGE="65001" language="vbscript" %>
 <!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/_inc/header.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="/_css/sub.css" />
	<script src="/js/member.js"></script>
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header" class="active">
		<!-- #include virtual="/_inc/_header.asp" -->
	</div>
<%
returnUrl	= SQL_Injection(Trim(Request.Form("returnUrl")))
%>
	<div id="A_Container" class="login_wrap">
		<!-- 서브컨텐츠 -->
		<div class="login_box">
			<div class="tit_box">
				<h3>로그인</h3>
				<p>신성씨앤에스 계정으로 로그인하기</p>
			</div>
			<form name="login" method="post" action="./login_ok.asp">
			<input type="hidden" name="returnUrl" value="<%=returnUrl%>">
				<input type="text" name="uid" placeholder="아이디" />
				<input type="password" name="upass" placeholder="비밀번호" />
				<button type="button" class="btn_blue" onclick="login_chk();">로그인</button>
			</form>
			<ul class="find_links">
				<li><a href="./find_idpw.asp">아이디 찾기</a></li>
				<li><a href="./find_idpw.asp">비밀번호 찾기</a></li>
			</ul>
		</div>
		<div class="join_box">
			<div class="tit_box">
				<h3>회원가입</h3>
				<p>신성씨앤에스 회원이 되시면 누리는 <em>혜택 7가지</em></p>
			</div>
			<ul>
				<li>사전 부품 추가, OS 탑재 서비스</li>
				<li>자산 이력 관리 및 추적 가능한 시스템</li>
				<li>긴급 기술 문의, 장애 지원 서비스</li>
				<li>무료 택배 발송 / 회수 서비스</li>
				<li>업무에 필요한 설치 매뉴얼 제공받기</li>
				<li>구매 전, 데모 장비 신청 서비스</li>
				<li>원클릭으로 발주하는 맞춤형 앱 플랫폼</li>
			</ul>
			<button type="button" class="btn_bk" onclick="location.href='./join.asp' ">회원가입</button>
		</div>
	</div>

<!-- #include virtual="/_inc/_footer.asp" -->