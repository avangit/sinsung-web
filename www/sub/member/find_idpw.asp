 <% @CODEPAGE="65001" language="vbscript" %>
 <!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
	<script src="/js/member.js"></script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(5)").addClass('on');
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">아이디/패스워드 찾기</a></li>
 				</ul>
 			</div>
 		</div>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
 				<h3>아이디/패스워드 찾기</h3>
 			</div>
 			<div id="content">
 				<!-- idpw Start -->
 				<div class="idpw_Box">
 						<form name="findIdform" method="post" action="./find_ok.asp">
 						<input type="hidden" name="code" value="id">
 						<fieldset>
 							<legend>아이디 찾기</legend>
 							<div class="cnt">
 								<div class="txtA">
 									<p class="ttl"><span class="blue">아이디</span> 찾기</p>
 									<p class="sub_tit">
 										아이디를 잊어버리셨나요?<br>
 										아래 입력사항에 가입하실때 작성하신 정보를 입력해주세요!
 									</p>
 									<div class="loginp">
 										<div class="ipt1 ipt"><span>이 름 </span><input type="text" name="strName"></div>
 										<div class="ipt2 ipt"><span>이메일 </span><input type="text" name="strEmail1" class="w140"> @ <input type="text" name="strEmail2" class="w160"></div>
 										<div class="ipt3 ipt"><span>휴대전화 </span>
 											<input type="text" name="strMobile1" class="w91 mt5 taC" maxlength="4"> -
 											<input type="text" name="strMobile2" class="w91 mt5 taC" maxlength="4"> -
 											<input type="text" name="strMobile3" class="w91 mt5 taC" maxlength="4" >
 										</div>
 										<a href="javascript:;" class="btn_log" onclick="id_search();">확인</a>
 									</div>
 								</div>
 							</div>
 						</fieldset>
 						</form>
 				</div>
 				<div class="idpw_Box mgT30">
 						<form name="findPwform" method="post" action="./find_ok.asp">
 						<input type="hidden" name="code" value="pw">
 						<fieldset>
 							<legend>비밀번호 찾기</legend>
 							<div class="cnt">
 								<div class="txtA">
 									<p class="ttl"><span class="blue">비밀번호</span> 찾기</p>
 									<p class="sub_tit">
 										비밀번호를 잊어버리셨나요?<br>
 										아래 입력사항에 가입하실때 작성하신 정보를 입력해주세요! 가입시 이메일로 전송됩니다.
 									</p>
 									<div class="loginp">
 										<div class="ipt1 ipt"><span>아이디 </span><input type="text" name="strId"></div>
 										<div class="ipt2 ipt"><span>이 름 </span><input type="text" name="strName"></div>
 										<div class="ipt3 ipt"><span>이메일 </span><input type="text" name="strEmail1" class="w140"> @ <input type="text" name="strEmail2" class="w160"></div>
 										<a href="javascript:;" class="btn_log" onclick="pw_search();">확인</a>
 									</div>
 								</div>
 							</div>
 						</fieldset>
 						</form>
 				</div> <!-- //idpw Start -->
 			</div>
 		</div>

<!-- #include virtual="/_inc/_footer.asp" -->