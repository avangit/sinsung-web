<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
code		= SQL_Injection(Trim(Request.Form("code")))
strName		= SQL_Injection(Trim(Request.Form("strName")))
strEmail1	= SQL_Injection(Trim(Request.Form("strEmail1")))
strEmail2	= SQL_Injection(Trim(Request.Form("strEmail2")))
strMobile1	= SQL_Injection(Trim(Request.Form("strMobile1")))
strMobile2	= SQL_Injection(Trim(Request.Form("strMobile2")))
strMobile3	= SQL_Injection(Trim(Request.Form("strMobile3")))
strId		= SQL_Injection(Trim(Request.Form("strId")))

strMobile = strMobile1 & "-" & strMobile2 & "-" & strMobile3
strEmail = strEmail1 & "@" & strEmail2

Call DbOpen()

If code = "id" Then

	sql = "SELECT strId FROM mTb_Member2 WHERE strName = '"&strName&"' AND strEmail = '"&strEmail&"' AND strMobile = '"&strMobile&"' AND intAction = 0"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Msg = "아이디를 기재하신 메일주소로 발송했습니다."

		userId = rs("strId")

		m_title = "[신성CNS] 아이디 안내 메일"
		m_content = strName & "님의 아이디는 " & rs("strId") & " 입니다"

		call sendMail("skk@sinsungcns.com", strEmail,  m_title, m_content, "html", "", "")
	Else
		Msg = "일치하는 정보가 없습니다."
	End If
ElseIf code = "pw" Then

	sql = "SELECT intSeq FROM mTb_Member2 WHERE strId = '"&strId&"' AND strName = '"&strName&"'  AND strEmail = '"&strEmail&"' AND intAction = 0"
	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		Msg = "임시비밀번호를 기재하신 메일주소로 발송했습니다."

		temp_pwd = RndNumber(10)

		sql = "UPDATE mTb_Member2 SET strPwd = '" & Encrypt_Sha(temp_pwd) & "' WHERE intSeq = " & rs("intSeq") & ""
		dbconn.execute(sql)

		m_title = "[신성CNS] 임시비밀번호 안내 메일"
		m_content = strName & "님의 임시 비밀번호는 " & temp_pwd & " 입니다." & Chr(10) & "이용해 주셔서 감사합니다."

		call sendMail("skk@sinsungcns.com", strEmail,  m_title, m_content, "html", "", "")
	Else
		Msg = "일치하는 정보가 없습니다."
	End If
End If

rs.Close
Set rs = Nothing

Call DbClose()

Call jsAlertMsgUrl(Msg, "./find_idpw.asp")
%>