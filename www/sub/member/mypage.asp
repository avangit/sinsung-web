  <% @CODEPAGE="65001" language="vbscript" %>
 <!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script src="/js/member.js"></script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(4)").addClass('on');
 		});

		function openDaumPostcode() {new daum.Postcode({
				oncomplete: function(data) {
					// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

					// 각 주소의 노출 규칙에 따라 주소를 조합한다.
					// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
					var fullAddr = ''; // 최종 주소 변수
					var extraAddr = ''; // 조합형 주소 변수

					// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
					if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
						fullAddr = data.roadAddress;

					} else { // 사용자가 지번 주소를 선택했을 경우(J)
						fullAddr = data.jibunAddress;
					}

					// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
					if(data.userSelectedType === 'R'){
						//법정동명이 있을 경우 추가한다.
						if(data.bname !== ''){
							extraAddr += data.bname;
						}
						// 건물명이 있을 경우 추가한다.
						if(data.buildingName !== ''){
							extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
						}
						// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
						fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
					}

					// 우편번호와 주소 정보를 해당 필드에 넣는다.
					document.getElementById('cZip').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('cAddr1').value = fullAddr;

					// 커서를 상세주소 필드로 이동한다.
					document.getElementById('cAddr2').focus();
				}
			}).open();
		}
 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">MEMBER</a></li>
 					<li><a href="#">회원가입</a></li>
 				</ul>
 			</div>
 		</div>
<%
sql = "SELECT * FROM mtb_member2 WHERE strId = '" & Request.Cookies("U_ID") & "'"
Set rs  = dbconn.execute(sql)

If rs.bof Or rs.eof Then
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

If rs("strEmail") <> "" Then
	ArrEmail = Split(rs("strEmail"), "@")
	If Ubound(ArrEmail) > 0 Then
		strEmail1 = ArrEmail(0)
		strEmail2 = ArrEmail(1)
	End If
End If

If rs("strPhone") <> "" Then
	ArrPhone = Split(rs("strPhone"), "-")
	If Ubound(ArrPhone) > 0 Then
		strPhone1 = ArrPhone(0)
		strPhone2 = ArrPhone(1)
		strPhone3 = ArrPhone(2)
	End If
End If

If rs("strMobile") <> "" Then
	ArrMobile = Split(rs("strMobile"), "-")
	If Ubound(ArrMobile) > 0 Then
		strMobile1 = ArrMobile(0)
		strMobile2 = ArrMobile(1)
		strMobile3 = ArrMobile(2)
	End If
End If
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
 				<h3>정보수정</h3>
 			</div>
 			<div id="content">
 				<!-- join Start -->
 				<!--회원 테이블-->
 				<form name="joinform" method="post" action="/mypage_ok.asp" enctype="multipart/form-data">
 					<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
 						<caption>정보수정</caption>
 						<colgroup>
 							<col width="18%">
 							<col width="82%">
 						</colgroup>
 						<thead>
 							<tr>
 								<th>회사명 *</th>
 								<td class="left"><strong><%=rs("cName")%></strong></td>
 							</tr>
 							<tr>
 								<th>사업자번호 *</th>
 								<td class="left"><strong><%=rs("cNum")%></strong></td>
 							</tr>
 							<tr>
 								<th align="center">아이디 *</th>
 								<td class="left"><strong><%=Request.Cookies("U_ID")%></strong></td>
 							</tr>
 						</thead>
 						<tbody>
 							<tr>
 								<th>패스워드 *</th>
 								<td class="left"><input type="password" name="strPwd" class="AXInput W150" maxlength="30">
 								</td>
 							</tr>
 							<tr>
 								<th>패스워드 재확인 *</th>
 								<td class="left"><input type="password" name="strPwdRe" class="AXInput W150" maxlength="30"></td>
 							</tr>
 							<tr>
 								<th>성함 *</th>
 								<td class="left"><strong><%=Request.Cookies("U_NAME")%></strong></td>
 							</tr>
 							<tr>
 								<th>이메일 *</th>
 								<td class="left">
 									<input type="text" name="strEmail1" value="<%=strEmail1%>" class="AXInput w20p"> @
 									<input type="text" name="strEmail2" value="<%=strEmail2%>" class="AXInput w20p">

 									<label>
 										<select name="strEmail3" id="strEmail3" class="AXSelect" onchange="ChangeEmail();">
 											<option value="0">직접입력</option>
 											<option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
 											<option value="empal.com">empal.com</option>
 											<option value="gmail.com">gmail.com</option>
 											<option value="lycos.co.kr">lycos.co.kr</option>
 											<option value="naver.com">naver.com</option>
 											<option value="nate.com">nate.com</option>
 											<option value="netsgo.com">netsgo.com</option>
 											<option value="hanmail.net">hanmail.net</option>
 											<option value="hotmail.com">hotmail.com</option>
 											<option value="paran.com">paran.com</option>
 											<option value="yahoo.co.kr">yahoo.co.kr</option>
 										</select>
 									</label>


 								</td>
 							</tr>
 							<tr>
 								<th>연락처 * </th>
 								<td class="left">
 									<input type="text" name="strPhone1" maxlength="4" value="<%=strPhone1%>" class="AXInput W50 taC"> -
 									<input type="text" name="strPhone2" maxlength="4" value="<%=strPhone1%>" class="AXInput W50 taC"> -
 									<input type="text" name="strPhone3" maxlength="4" value="<%=strPhone1%>" class="AXInput W50 taC">
 								</td>
 							</tr>
 							<tr>
 								<th>휴대전화 * </th>
 								<td class="left">
 									<select name="strMobile1" id="strMobile1" title="앞3자리" class="AXSelect">
 										<option value="010" <% If strMobile1 = "010" Then %> selected<% End If %>>010</option>
 										<option value="011" <% If strMobile1 = "011" Then %> selected<% End If %>>011</option>
 										<option value="016" <% If strMobile1 = "016" Then %> selected<% End If %>>016</option>
 										<option value="017" <% If strMobile1 = "017" Then %> selected<% End If %>>017</option>
 										<option value="018" <% If strMobile1 = "018" Then %> selected<% End If %>>018</option>
 										<option value="019" <% If strMobile1 = "019" Then %> selected<% End If %>>019</option>
 									</select> -
 									<input type="text" name="strMobile2" maxlength="4" value="<%=strMobile2%>" class="AXInput W50 taC"> -
 									<input type="text" name="strMobile3" maxlength="4" value="<%=strMobile3%>" class="AXInput W50 taC">
 								</td>
 							</tr>
 							<tr>
 								<th>회사주소 * </th>
 								<td class="left" style="height:86px">
 									<p class="pd3-0">
 										<input type="text" name="cZip" id="cZip" value="<%=rs("cZip")%>" class="AXInput W50" readonly="readonly">
 										<input type="button" value="우편번호검색" class="AXButtonSmall Classic"  onclick="openDaumPostcode();">
 									</p>
 									<p class="pd3-0"><input type="text" name="cAddr1" id="cAddr1" value="<%=rs("cAddr1")%>" class="AXInput" readonly="readonly" style="width:50%"></p>
 									<p class="pd3-0"><input type="text" name="cAddr2" id="cAddr2" value="<%=rs("cAddr2")%>" class="AXInput" style="width:50%"></p>
 								</td>
 							</tr>
 							<tr>
 								<th>사업자등록증 사본</th>
 								<td class="left"><input type="file" name="strFile1" class="AXInput w50p">
 									<span class="desCription">첨부파일 : 최대 용량 10M 이하</span>
								<% If rs("strFile1") <> "" Then %>
									<br><a href="/download.asp?fn=<%=escape(rs("strFile1"))%>&ph=member"><%=rs("strFile1")%></a>&nbsp;&nbsp;<input type="checkbox" name="ori_file1_del1" value="Y"> 삭제<br>
								<% End If %>
 								</td>
 							</tr>
 							<tr>
 								<th>메일 정보</th>
 								<td class="left"><input type="checkbox" name="mailYN" id="mailYN" value="y" <% If rs("mailYN") = "Y" Then %> checked<% End If %>> <label for="mailYN">관련 정보 메일을 수신합니다.</label></td>
 							</tr>
 							<tr>
 								<th>SMS 정보</th>
 								<td class="left"><input type="checkbox" name="smsYN" id="smsYN" value="y" <% If rs("smsYN") = "Y" Then %> checked<% End If %>> <label for="smsYN">관련 정보 SMS를 수신합니다.</label></td>
 							</tr>
 						</tbody>
 					</table>

 					<div class="ntb-listbtn-area" style="width:100%">
 						<input type="button" value="   확인   " class="AXButton Classic" onclick="update_chk();">
 						<input type="button" value="   취소   " class="AXButton" onclick="history.back();">

 					</div>

 				</form> <!-- //join Start -->
 			</div>


 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/_inc/_footer.asp" -->