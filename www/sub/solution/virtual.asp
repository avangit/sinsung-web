 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script>
 			$(document).on('ready', function() {
    	$(".sub_lnb li:nth-child(4)").addClass('on');
    });

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">솔루션</a></li>
 					<li><a href="#">가상화</a></li>
 				</ul>
 			</div>
 		</div>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
					<div id="leftS">
 				<div class="title">
 					<h3>가상화</h3>
 					<a href="/sub/support/onlineReceive_solution.asp?c=4"><img src="/images/sub/driver-icon01.png"> 솔루션 상담요청</a>
 				</div>
 				<div id="content" class="solution">
 					<p class="sol_tit">가상화란?</p>
 					<p class="cont_txt mb30">가상화는 여러 운영 체제 및 애플리케이션을 동일한 서버에서 동시에 실행할 수 있도록 하는 검증된 소프트웨어 기술입니다. 가상화는 IT 환경을<br>
 						혁신하고 기술 활용 방식을 근본적으로 바꾸고 있습니다.</p>
 					<dl>
 						<dt class="skyblue sol_stit">가상화 101</dt>
 						<dd class="cont_txt">가상화란 무엇일까요? 간단히 말하자면 가상화는 어떤 사물을 물리적 버전이 아닌 가상 버전으로 생성하는 과정입니다. <br>
 							가상화는 컴퓨터, 운영 체제, 스토리지 디바이스, 애플리케이션 또는 네트워크에 적용할 수 있습니다. 그러나 가상화의 핵심은 서버 가상화입니다. <br>
 							IT 조직은 한 번에 하나의 운영 체제와 애플리케이션만 실행할 수 있도록 설계된 현재의 x86 서버가 지닌 제약 때문에 어려움을 겪고 있습니다. <br>
 							따라서 아무리 작은 데이터 센터도 서버를 여러 대 구축해야 하며, 이 경우 각 서버는 용량의 5%~15%만 사용하기 때문에 어느 모로 보나 <br>
 							매우 비효율적입니다. <br>
 							가상화는 소프트웨어로 하드웨어의 존재를 시뮬레이션하여 가상 컴퓨터 시스템을 생성합니다. 이러한 가상화를 통해 기업은 단일 서버에서 하나 <br>
 							이상의 가상 시스템과 다수의 운영 체제 및 애플리케이션을 실행할 수 있게 됩니다. 이로써 규모 있는 경제성과 뛰어난 효율성을 얻을 수 있습니다.</dd>
 					</dl>
 					<dl>
 						<dt class="skyblue sol_stit">가상 머신</dt>
 						<dd class="cont_txt">'가상 머신(VM)'이란 가상 컴퓨터 시스템을 의미하며, 내부에 운영 체제와 애플리케이션을 갖춘 완전히 분리된 소프트웨어 컨테이너입니다. <br>
 							자체적으로 완전한 각각의 가상 머신은 완벽하게 독립되어 있습니다. 컴퓨터 한 대에 여러 가상 머신을 연결하면 단 하나의 물리적 서버 또는 <br>
 							'호스트'에서 다수의 운영 체제와 애플리케이션을 실행할 수 있습니다.하이퍼바이저라는 씬 계층 소프트웨어가 가상 머신을 호스트에서 분리하고 <br>
 							필요에 따라 동적으로 각 가상 머신에 컴퓨팅 리소스를 할당합니다.</dd>
 					</dl>
 					<dl>
 						<dt class="skyblue sol_stit">가상 머신의 주요 속성</dt>
 						<dd class="cont_txt">
 							<ol>
 								<li>
 									<p class="sl_bulltt">파티셔닝</p>
 									<p class="sl_bull_txt">
 										· 하나의 물리적 시스템에서 여러 운영 체제 실행<br>
 										· 가상 머신 간에 시스템 리소스 분배</p>
 								</li>
 								<li>
 									<p class="sl_bulltt">캡슐화</p>
 									<p class="sl_bull_txt">
 										· 가상 머신의 전체 상태를 파일에 저장<br>
 										· 파일을 이동하고 복사하는 것처럼 손쉽게 가상 머신을 이동 및 복사</p>
 								</li>
 								<li>
 									<p class="sl_bulltt">분리성</p>
 									<p class="sl_bull_txt">
 										· 하드웨어 수준에서 장애 및 보안 분리성 제공<br>
 										· 고급 리소스 제어로 성능 유지</p>
 								</li>
 								<li>
 									<p class="sl_bulltt">하드웨어 독립성</p>
 									<p class="sl_bull_txt">
 										· 원하는 물리적 서버로 원하는 가상 머신을 프로비저닝 또는 마이그레이션</p>
 								</li>
 							</ol>
 						</dd>
 					</dl>
 					<dl>
 						<dt class="skyblue sol_stit">가상화의 이점</dt>
 						<dd class="cont_txt">가상화를 통해 IT 대응력, 유연성, 확장성을 높이면서 비용을 대폭 절감할 수 있습니다. 워크로드가 더욱 빠르게 배포되고 성능과 가용성이 <br>
 							높아지고 운영이 자동화되므로 IT 관리가 간편해지고 소유 및 관리 비용을 절감할 수 있습니다.</dd>
 						<div class="grBox">
 							<ul>
 								<li class="sl_bulltt">자본 및 운영 비용 절감</li>
 								<li class="sl_bulltt">애플리케이션 고가용성 제공</li>
 								<li class="sl_bulltt">다운타임 최소화 또는 제거</li>
 								<li class="sl_bulltt">IT 생산성, 효율성, 대응력 및 응답성 향상</li>
 								<li class="sl_bulltt">중앙 집중식 관리 가능</li>
 								<li class="sl_bulltt">비즈니스 연속성 및 재해 복구 지원</li>
 								<li class="sl_bulltt">진정한 소프트웨어 정의 데이터 센터 구축</li>
 								<li class="sl_bulltt">애플리케이션 및 리소스 프로비저닝 <br>
 									시간 단축 및 간소화</li>
 							</ul>
 						</div>
 					</dl>
 					<ul class="solu_list">
 						<li>
 							<img src="/images/sub/virtual01.gif" alt="">
 							<p class="ta_center">운영 비용 절감</p>
 						</li>
 						<li>
 							<img src="/images/sub/virtual02.gif" alt="">
 							<p class="ta_center">IT 생산성 및 효율성 증대</p>
 						</li>
 						<li>
 							<img src="/images/sub/virtual03.gif" alt="">
 							<p class="ta_center">다운타임의 최소화</p>
 						</li>
 						<li>
 							<img src="/images/sub/virtual04.gif" alt="">
 							<p class="ta_center">애플리케이션 고가용성</p>
 						</li>
 					</ul>
 				</div>

				</div>

 			</div>
 			<div id="A_Container_R">
 				<h3 class="sub_tit">솔루션</h3>
 				<!-- #include virtual="/_inc/right_solution.asp" -->
				<!-- #include virtual="/_inc/right_bnn.asp" -->
 			</div>

<!-- #include virtual="/_inc/_footer.asp" -->