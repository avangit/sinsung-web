<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
Dim fieldname 	: fieldname 	= SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	= SQL_Injection(Trim(Request("fieldvalue")))
Dim orderby		: orderby		= SQL_Injection(Trim(Request("orderby")))
Dim intNowPage	: intNowPage 	= SQL_Injection(Trim(Request("page")))
%>
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<script src="/js/jquery.navgoco.js"></script>
	<script src="/js/product.js"></script>

 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
			<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");
			$(".sub_con_tab li").click(function() {
				$(this).siblings().removeClass("active");
				$(this).addClass("active");
				$(".tab_contents").hide();
			});
			$(".align_box .two").click(function() {
				$(".pro_list").addClass('pro_list02');
			});
			$(".align_box .one").click(function() {
				$(".pro_list").removeClass('pro_list02');
			});
		});
		$(document).ready(function() {
			$(".p_plus").click(function() {
				if($(this).hasClass('on')) {
					$(this).removeClass('on');
					$(this).parent().children('.sub_con_tab').css('height','70px');
				} else {
					$(this).addClass('on');
					$(this).parent().children('.sub_con_tab').css('height','100%');
				}
			});
			$(".sub_con_tab li").click(function() {
				$(".pro_tab_area_hide").slideDown();
			});
			$(".li_close").click(function() {
				$(this).remove();
			});
		});

		$(document).ready(function() {
			$('.f_hide').click(function() {
				if ($(this).hasClass('open')) {
					$(this).removeClass('open');
					$(this).text('세부내용 숨기기');
					$('.hide').show();
				} else {
					$(this).addClass('open');
					$(this).text('세부내용 보기');
					$('.hide').hide();
				}
			});
		});
	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">B2B 제품</a></li>
 				</ul>
 			</div>
 		</div>

 			<!-- 서브컨텐츠 -->

				<div id="sub_contents" class="product">
					<div class="inner">
						<div class="sub_tit">
							<h2>B2B제품</h2>
						</div>
					</div>
<%
	cate = Request.QueryString("c")

	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
					<div class="pro_tab">
						<div class="inner">
							<ul>
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
								<li <% If Left(cate, 2) = Left(rs("c_code"), 2) Then %> class="on"<% End If %>><a href="product02.asp?c=<%=rs("c_code")%>"><i></i><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
							</ul>
						</div>
					</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 6) = '000000' AND c_code LIKE '" & Left(cate, 2) & "%' AND SUBSTRING(c_code, 4, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
					<div id="A_Container">
						<div class="inner">
							<div class="pro_tab_area">
								<ul class="sub_con_tab">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
									<li><a href="?c=<%=rs("c_code")%>#product_01"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
								</ul>
								<button class="p_plus"></button>
							</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 4) = '0000' AND c_code LIKE '" & Left(cate, 4) & "%' AND SUBSTRING(c_code, 6, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
							<div class="pro_tab_area pro_tab_area_hide">
								<ul class="sub_con_tab">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
									<li <% If cate = rs("c_code") Then %> class="active"<% End If %>><a href="product02.asp?c=<%=rs("c_code")%>"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
								</ul>
								<button class="p_plus"></button>
							</div>
							<div class="search_box">
								<form name="search_form" action="" method="post">
								<div>
									<select name="fieldname">
										<option value="g_name" <% If fieldname = "" Or fieldname = "g_name" Then %> selected<% End If %>>제품명</option>
										<option value="g_idx" <% If fieldname = "g_idx" Then %> selected<% End If %>>제품코드</option>
									</select>
									<input type="text" name="fieldvalue" value="<%=fieldvalue%>">
									<button class="btn_search" onclick="document.search_form.submit();">검색</button>
								</div>
								</form>
							</div>
							<div class="align_box">
								<button class="one" onclick="./location.href='product01.asp'">1단보기</button>
								<button class="two" onclick="./location.href='product02.asp'">2단보기</button>
								<select name="orderby">
									<option value="g_read">인기순</option>
									<option value="g_insertDay">최신순</option>
									<option value="g_Money_asc">낮은 가격순</option>
									<option value="g_Money_desc">높은 가격순</option>
								</select>
							</div>
<%
	If Len(fieldvalue) > 0 Then
		query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
	End If

	If cate <> "" Then
		If Right(cate, 8) = "00000000" Then
			query_where = query_where & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
		ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
			query_where = query_where & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
		ElseIf Mid(cate, 6, 1) <> 0 Then
			query_where = query_where & " AND a.g_category = '" & cate & "'"
		End If

		sql = "SELECT c_name FROM CATEGORY WHERE c_code = '" & cate & "'"
		Set rs = dbconn.execute(sql)

		If Not(rs.bof Or rs.eof) Then
			cateName = rs("c_name")
		End If

		rs.close

		sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y' " & query_where & ""

	Else
		cateName = "전체제품"
		sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y'"
	End If

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		TotalCnt = rs("CNT")
	End If

	rs.close
%>
							<div class="filter_set">
								<h4><%=cateName%> <span>(<%=TotalCnt%>)</span></h4>
								<!--ul>
									<li class="f_reset"><a href="javascript:;">필터재설정</a></li>
									<li class="li_close"><a href="javascript:;">필터재설정</a></li>
									<li class="li_close"><a href="javascript:;">필터재설정</a></li>
									<li class="li_close"><a href="javascript:;">필터재설정</a></li>
								</ul-->
								<button type="button" class="f_hide">세부내용 숨기기</button>
							</div>
						</div>
					<div class="tab_contents" id="product_02">
						<div class="inner">
							<div id="A_Container_L">
								<div class="lnb">
									<ul class="nav">
<%
	sql2 = "SELECT b.ogroup_content FROM CATEGORY a INNER JOIN cate_option_group b ON a.c_option_group = b.ogroup_idx WHERE a.c_code = '" & cate & "'"
	Set cateRS = dbconn.execute(sql2)

	If Not cateRS.eof Then
		ogroup_content = cateRS("ogroup_content")
	End If

	cateRS.close
	Set cateRS = Nothing

	For og = 0 To UBound(Split(ogroup_content, ", "))
		sql = "SELECT opt_name, opt_content FROM cate_option WHERE opt_name = '" & Split(ogroup_content, ", ")(og) & "'"
		Set rs = dbconn.execute(sql)

		If Not rs.eof Then
			seq = 1
			Do While Not rs.eof
				opt_name = Trim(rs("opt_name"))
				opt_content = Trim(rs("opt_content"))

				arr_opt_content = Split(opt_content, ", ")
%>
										<li class="open">
											<a href="#"><span><%=opt_name%></span></a>
											<ul class="ss_menu">
												<li><a href="#.html">
														<div class="chk_box">
														<% For j = 0 To Ubound(arr_opt_content) %>
															<input id="fliter_<%=opt_name&j%>" type="checkbox" name="option<%=seq%>" value="<%=opt_name%>||<%=arr_opt_content(j)%>" <% If arr_option(seq) = opt_name&"||"&arr_opt_content(j) Then %> checked<% End If %>>
															<label for="fliter_<%=opt_name&j%>">
															<span></span><%=arr_opt_content(j)%></label>
														<% Next %>
														</div>

													</a></li>
											</ul>
										</li>
<%
				seq = seq + 1
				rs.MoveNext
			Loop
		End If
	Next

'	rs.close
%>
									</ul>
								</div>
								<script type="text/javascript">
									$(document).ready(function() {
										$(".nav").navgoco({
											caretHtml: '',
											accordion: false,
											openClass: 'open',
											save: true,
											cookie: {
												name: 'navgoco',
												expires: false,
												path: '/'
											},
											slide: {
												duration: 400,
												easing: 'swing'
											},
										});

										$("#collapseAll").click(function(e) {
											e.preventDefault();
											$(".nav").navgoco('toggle', false);
										});

										$("#expandAll").click(function(e) {
											e.preventDefault();
											$(".nav").navgoco('toggle', true);
										});
									});

								</script>
							</div>
<%
Dim intTotalCount, intTotalPage
Dim intPageSize			: intPageSize 		= 4
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " g_idx, g_name, g_simg, g_spec "
Dim query_Tablename		: query_Tablename	= "GOODS"
Dim query_where			: query_where		= " g_type = 1 AND g_display <> 'srm' AND g_act = 'Y'"

If cate <> "" Then
	If Right(cate, 8) = "00000000" Then
		query_where = query_where & " AND g_category LIKE '" & Left(cate,2) & "%'"
	ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
		query_where = query_where & " AND g_category LIKE '" & Left(cate,4) & "%'"
	ElseIf Mid(cate, 6, 1) <> 0 Then
		query_where = query_where & " AND g_category = '" & cate & "'"
	End If
End If

If orderby = "" Then
	query_orderby = " ORDER BY g_idx DESC"
Else
	If orderby = "g_Money_asc" Then
		query_orderby = " ORDER BY g_Money ASC"
	ElseIf orderby = "g_Money_desc" Then
		query_orderby = " ORDER BY g_Money DESC"
	Else
		query_orderby = " ORDER BY " & orderby & " DESC"
	End If
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
							<div id="A_Container_R" class="right_list">
							<form name="list" method="post" action="./product_compare.asp" style="margin:0;">
							<input type="hidden" name="n">
							<input type="hidden" name="m">
							<input type="hidden" name="message">
								<ul class="pro_list pro_list02">
<%
If rs.bof Or rs.eof Then
%>
									<li>등록된 제품이 없습니다.</li>
<%
Else
	i = 0
	rs.move MoveCount
	Do While Not rs.eof
%>
									<li>
										<div class="a_box">
											<div class="chk_box">
												<input id="pro0<%=i%>" type="checkbox" value="<%=rs("g_idx")%>">
												<label for="pro0<%=i%>"><span></span></label>
											</div>
											<label for="prod"><span></span></label>
											<a href="javascript:GetCheckbox(document.list, 'compare', '옵션비교');">옵션비교</a>
											<a href="onlineReceive_estimate.asp?idx=<%=rs("g_idx")%>">견적요청</a>
										</div>
										<div class="p_contents">
											<a href="product_view.asp?idx=<%=rs("g_idx")%>"><div class="img_box"> <img src="/upload/goods/<%=rs("g_simg")%>" alt="<%=rs("g_name")%>"></div>
												<div class="cont_box">
													<p class="pro_tit"><%=rs("g_name")%></p>
													<span># <%=rs("g_idx")%></span>
													<span class="hide"><%=rs("g_spec")%></span>
												</div></a>
										</div>
										</form>
									</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
								</ul>

								<% Call Paging_user_srm("") %>
								<!--div class="list_paging">
									<a class="paging_prev" href="#">처음</a>
									<ul>
										<li><a href="#">21</a></li>
										<li><a href="#">22</a></li>
										<li class="active"><a href="#">23</a></li>
										<li><a href="#">24</a></li>
										<li><a href="#">25</a></li>
									</ul>
									<a class="paging_next" href="#">맨끝</a>
								</div-->
							</div>
						</div>
					</div>
				</div>

 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/_inc/_footer.asp" -->