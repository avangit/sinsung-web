<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
g_idx =  SQL_Injection(Trim(Request("n")))
mode =  SQL_Injection(Trim(Request("m")))
message =  SQL_Injection(Trim(Request("message")))

If g_idx = "" Then
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

If Left(g_idx, 1) = "," Then
	list_g_idx = Split(g_idx, ",")

	For k = 0 To Ubound(list_g_idx)
		If list_g_idx(k) <> "" Then
			If CInt(k) < CInt(Ubound(list_g_idx)) Then
				test = ","
			Else
				test = ""
			End If
				var_g_idx = var_g_idx & list_g_idx(k) & test
		End If
	Next

	g_idx = var_g_idx
End If

arr_g_idx = Split(g_idx, ",")
%>
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
	<script src="/js/product.js"></script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(3)").addClass('on');
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">견적문의</a></li>
 					<li><a href="#">제품비교</a></li>
 				</ul>
 			</div>
 		</div>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="container">
 				<div class="h3Title">
 					<h3>제품비교</h3>
 					<p></p>
 				</div>
<%
	ReDim sql(3), rs(3)

	For i = 0 To Ubound(arr_g_idx)
		If arr_g_idx(i) <> "" And Not (ISNULL(arr_g_idx(i))) Then
			sql(i) = "SELECT * FROM GOODS WHERE g_idx = '" & arr_g_idx(i) & "' ORDER BY g_idx DESC"
			Set rs(i) = dbconn.execute(sql(i))
		End If
	Next
%>
 				<div id="cpCon">
 					<!--div class="blueBarTit">
 						<p>Highlight : </p>
 						<ul class="chk">
 							<li><input type="radio" name="highlightrad" id="None_radio" value="None" checked=""> <label for="None_radio">Off</label></li>
 							<li><input type="radio" name="highlightrad" id="Similar_radio" value="Similar"> <label for="Similar_radio">Similarities</label></li>
 							<li><input type="radio" name="highlightrad" id="Differences_radio" value="Differences"> <label for="Differences_radio">Differences</label></li>
 						</ul>
 					</div-->
 					<table class="cpTable">
 						<colgroup>
<%
						For i = 0 To Ubound(arr_g_idx)
%>
 							<col style="width:<%=Round(80 / (Ubound(arr_g_idx) + 1), 1)%>%">
<%
						Next
%>
 						</colgroup>
 						<tbody>
 						<form name="remove_Form" method="post" action="./product_compare.asp">
						<input type="hidden" name="n" value="<%=g_idx%>">
 							<tr class="pdThum">
 								<th>Product Thumbnail</th>
<%
	For i = 0 To Ubound(arr_g_idx)
		If arr_g_idx(i) <> "" And Not (ISNULL(arr_g_idx(i))) Then
			If Not(rs(i).eof) Then
				Do While Not rs(i).eof
%>
 								<td>
									<input type="hidden" name="del_idx<%=i%>" value="<%=rs(i)("g_idx")%>">
 									<p><a href="product_view.asp?idx=<%=rs(i)("g_idx")%>" alt="<%=rs(i)("g_name")%>"><img src="/upload/goods/<%=rs(i)("g_simg")%>"></a></p>
 									<a href="#;" class="reBtn" rel="<%=rs(i)("g_idx")%>" data="<%=Ubound(arr_g_idx)%>">REMOVE</a>
 								</td>
<%
					rs(i).MoveNext
				Loop
			End If

			rs(i).MoveFirst
		End If
	Next
%>
 							</tr>
							</form>

 							<tr class="pdName">
 								<th>Product Name</th>
<%
	For i = 0 To Ubound(arr_g_idx)
		If arr_g_idx(i) <> "" And Not (ISNULL(arr_g_idx(i))) Then
			If Not(rs(i).eof) Then
				Do While Not rs(i).eof
%>
 								<td>
 									<a href="product_view.asp?idx=<%=rs(i)("g_idx")%>"><em><%=rs(i)("g_name")%></em></a>
 								</td>
<%
					rs(i).MoveNext
				Loop
			End If

			rs(i).MoveFirst
		End If
	Next
%>
 							</tr>
<%
	strSQL = "SELECT opt_name FROM cate_option"
	Set optRS = dbconn.execute(strSQL)

	If Not(optRS.eof) Then
		Do While Not optRS.eof
%>
 							<tr class="subheader">
 								<th class="depth"><%=optRS("opt_name")%></th>
<%
			For i = 0 To Ubound(arr_g_idx)
				If arr_g_idx(i) <> "" And Not (ISNULL(arr_g_idx(i))) Then
					If Not(rs(i).eof) Then
						Do While Not rs(i).eof
							g_optionList = rs(i)("g_optionList")
							arr_g_optionList = Split(g_optionList, ",")

							If Ubound(arr_g_optionList) > 0 Then
								For j = 1 To Ubound(arr_g_optionList)
									arr_g_optionList1 = Split(arr_g_optionList(j-1), "||")

									If optRS("opt_name") = arr_g_optionList1(0) Then
%>
 									<td><%=arr_g_optionList1(1)%></td>
<%
									Else
'										If i = 0 Then
%>
									<!--td>-</td-->
<%
'										End If
									End If
								Next
							End If

							rs(i).MoveNext
						Loop

						rs(i).MoveFirst
					End If
				End If
			Next
%>
 							</tr>
<%
			optRS.MoveNext
		Loop
	End If

	optRS.close
	Set optRS = Nothing
%>
 						</tbody>
 					</table>
 				</div>
 			</div>
 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/_inc/_footer.asp" -->