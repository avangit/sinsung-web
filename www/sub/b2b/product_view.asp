 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/slick.css">
	<link rel="stylesheet" href="/_css/sub.css" />
 	<script src="/js/slick.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");
			$(".tab_contents").hide();
			$(".sub_con_tab li:first").addClass("active").show();
			$(".tab_contents:first").show();

			$(".sub_con_tab li").click(function() {
				$(".sub_con_tab li").removeClass("active");
				$(this).addClass("active");
				$(".tab_contents").hide();
				var activeTab = $(this).find("a").attr("href");
				$(activeTab).fadeIn();
				return false;
			});
			$('.slider-for').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
//				slidesToShow: 5,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				dots:false,
				centerMode: true,
				focusOnSelect: true,
				variableWidth: true,
				arrows: false,
//				centerPadding: '10px'
			});
		});

	</script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>

 		<!-- 서브 네비게이션 -->
<%
	g_idx = SQL_Injection(Trim(Request.QueryString("idx")))

	sql2 = "SELECT * FROM GOODS_CATE_JOIN WHERE g_idx = '" & g_idx & "' ORDER BY gcj_idx DESC"
	Set rs4 = dbconn.execute(sql2)
%>
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
<%
	If Not rs4.eof Then
		Do While Not rs4.eof
%>
 					<li class="home"><a href="/">Home<%=getCateColPrint(rs4("c_code"), "CATEGORY")%></a></li>
<%
			rs4.movenext
		Loop
	End If

	rs4.close
	Set rs4 = Nothing
%>
 				</ul>
 			</div>
 		</div>
 			<!-- 서브컨텐츠 -->
<%
ReDim g_bimg(4), g_menual(4)

sql = "SELECT * FROM GOODS WHERE g_type = 1 AND g_display <> 'srm' AND g_act = 'Y' AND g_idx = '" & g_idx & "'"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	g_name = rs("g_name")
	g_spec = rs("g_spec")
	g_intro = rs("g_intro")
	g_optionList = rs("g_optionList")
	g_simg = rs("g_simg")
	For i = 1 To 5
		g_bimg(i-1) = rs("g_bimg" & i)
	Next
	g_memo = rs("g_memo")
	g_video = rs("g_video")
	For j = 1 To 5
		g_menual(j-1) = rs("g_menual" & j)
	Next
Else
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

sql = "UPDATE GOODS SET g_read = g_read + 1 WHERE g_type = 1 AND g_display <> 'srm' AND g_act = 'Y' AND g_idx = '" & g_idx & "'"
dbconn.execute(sql)

rs.Close
%>
 				<div id="sub_contents" class="product_view">
					<div class="inner">
						<div class="sub_tit">
							<h2>B2B제품</h2>
						</div>
					</div>

					<!-- 슬라이드영역-->
					<div class="pdtop">
						<div class="inner">
							<div class="prov_slide">
								<div class="slider-for">
								<% If g_simg <> "" Then %>
									<img src="/upload/goods/<%=g_simg%>">
								<% End If %>
<%
							For i = 1 To 5
								If g_bimg(i-1) <> "" Then
%>
									<img src="/upload/goods/<%=g_bimg(i-1)%>">
<%
								End If
							Next
%>
								</div>
								<div class="slider-nav">
									<div class="img_box">
										<img src="/upload/goods/<%=g_simg%>">
									</div>
<%
							For i = 1 To 5
								If g_bimg(i-1) <> "" Then
%>
									<div class="img_box">
										<img src="/upload/goods/<%=g_bimg(i-1)%>">
									</div>
<%
								End If
							Next
%>
								</div>
							</div>
							<!--제품설명영역-->

							<div class="prov_cont">
								<ul>
									<li>
										<p>제품코드 : #<%=g_idx%></p>
										<h3><%=g_name%></h3>
									</li>
									<li class="line">
										<p class="tit">주요특징 <span><%=g_spec%></span></p>
										<p class="tit">제품개요 <span><%=g_intro%></span></p>
									</li>
<!--
									<li>
										<p class="tit">가격 </p><strong>100,000원</strong>
									</li>
									<li>
										<p class="tit">
											수량 <span class="inven">(재고:100)</span>
										</p> <strong>
											<div class="vol_btns">
												<a href="#"><img src="images/sub/but_vol_up.png"></a>
												<input type="text" placeholder="1">
												<a href="#"><img src="images/sub/but_vol_down.png"></a>
											</div>
										</strong>
									</li>
-->
									<li class="btn_list"><button class="btn_blue" onclick="location.href='onlineReceive_estimate.asp?idx=<%=g_idx%>'">견적요청</button>
<!--
										<button class="btn_bk" onclick="location.href =  'cart.html'">장바구니</button>
										<button class="btn_blue" onclick="location.href = 'order.html'">바로구매</button></li>
-->
								</ul>
							</div>
						</div>
					</div>
					<div class="inner">
						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li><a href="#pro_view_01">제품사양</a></li>
								<li><a href="#pro_view_02">제품상세</a></li>
								<li><a href="#pro_view_03">제품영상</a></li>
								<li><a href="#pro_view_04">제품 매뉴얼</a></li>
							</ul>
							<div class="tab_contents " id="pro_view_01">
								<table class="table pv_table">
									<tbody>
<%
	arr_g_optionList = Split(g_optionList, ",")

	If Ubound(arr_g_optionList) > 0 Then
		For i = 0 To Ubound(arr_g_optionList) - 1
			arr_g_optionList1 = Split(arr_g_optionList(i), "||")
%>
										<tr>
											<th><%=Trim(arr_g_optionList1(0))%></th>
											<td><%=Trim(arr_g_optionList1(1))%></td>
										</tr>
<%
		Next
	End If
%>
									</tbody>
								</table>
							</div>

							<div class="tab_contents" id="pro_view_02">
								<div class="dateil_area">
									<%=g_memo%>
								</div>

							</div>
<%
sql = "SELECT b_addtext1 FROM BOARD_v1 WHERE b_part = 'board08' AND b_idx = " & g_video
Set rs = dbconn.execute(sql)
%>
							<div class="tab_contents " id="pro_view_03">
								<div class="video_area">
								<% If Not(rs.eof) Then %>
									<%=rs("b_addtext1")%>
								<% End If %>
								</div>
<% rs.close %>
							</div>
<%
For j = 1 To 5
	If g_menual(j-1) <> "" Then
		If j = 1 Then
			arr_var = arr_var & g_menual(j-1)
		Else
			arr_var = arr_var & "," & g_menual(j-1)
		End If
	End If
Next

sql = "SELECT file_1 FROM BOARD_v1 WHERE b_part = 'board07' AND b_idx IN (" & arr_var & ")"
Set rs = dbconn.execute(sql)
%>
							<div class="tab_contents " id="pro_view_04">
								<table class="tab_table">
									<tbody>
<%
	If Not(rs.eof) Then
		Do Until rs.Eof
%>
										<tr>
											<th><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download><%=rs("file_1")%> <!--p>20MB</p--></a></th>
											<td><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download>다운로드</a></td>
										</tr>
<%
			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
									</tbody>
								</table>
							</div>
						</div>
					</div>

 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/_inc/_footer.asp" -->