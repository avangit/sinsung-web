<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
g_idx =  SQL_Injection(Trim(Request("n")))
mode =  SQL_Injection(Trim(Request("m")))
message =  SQL_Injection(Trim(Request("message")))

If g_idx = "" Then
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

arr_g_idx = Split(g_idx, ",")

'Response.write Ubound(arr_g_idx)
%>
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(3)").addClass('on');
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">견적문의</a></li>
 					<li><a href="#">제품비교</a></li>
 				</ul>
 			</div>
 		</div>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="container">
 				<div class="h3Title">
 					<h3>제품비교</h3>
 					<p></p>
 				</div>
<%
sql = "SELECT * FROM GOODS WHERE g_idx IN (" & g_idx & ") ORDER BY g_idx DESC"
Set rs = dbconn.execute(sql)
%>
 				<div id="cpCon">
 					<div class="blueBarTit">
 						<p>Highlight : </p>
 						<ul class="chk">
 							<li><input type="radio" name="highlightrad" id="None_radio" value="None" checked=""> <label for="None_radio">Off</label></li>
 							<li><input type="radio" name="highlightrad" id="Similar_radio" value="Similar"> <label for="Similar_radio">Similarities</label></li>
 							<li><input type="radio" name="highlightrad" id="Differences_radio" value="Differences"> <label for="Differences_radio">Differences</label></li>
 						</ul>
 					</div>
 					<table class="cpTable" id="compareTbl">
 						<colgroup>
						<% For i = 0 To Ubound(arr_g_idx) %>
 							<col style="width:14%">
						<% Next %>
 							<!--col style="width:14%">
 							<col style="width:14%">
 							<col style="width:14%">
 							<col style="width:14%"-->
 						</colgroup>
 						<form name="remove_Form" method="POST" action="./product_compare.asp"></form>
 						<input type="hidden" name="arr_goods_list" value="@2238@2236@2235@2234@">
 						<tbody>
 							<tr class="pdThum">
 								<th>Product Thumbnail</th>
<%
	If Not(rs.eof) Then
		Do while Not rs.eof
%>
 								<td>
 									<p><a href="product_view.asp?idx=<%=rs("g_idx")%>" alt="<%=rs("g_name")%>"><img src="/upload/goods/<%=rs("g_simg")%>"></a></p>
 									<a href="#;" class="reBtn">REMOVE</a>
 								</td>
<%
			rs.MoveNext
		Loop
	End If

	rs.MoveFirst
%>
 								<!--td>
 									<p><a href="product_view.asp"><img src="/images/sub/product_img.jpg" alt=""></a></p>
 									<a href="#;" class="reBtn" rel="2236">REMOVE</a>
 									<input type="hidden">
 								</td>
 								<td>
 									<p><a href="product_view.asp"><img src="/images/sub/product_img.jpg" alt=""></a></p>
 									<a href="#;" class="reBtn" rel="2235">REMOVE</a>
 									<input type="hidden">
 								</td>
 								<td>
 									<p><a href="product_view.asp"><img src="/images/sub/product_img.jpg" alt=""></a></p>
 									<a href="#;" class="reBtn" rel="2234">REMOVE</a>
 									<input type="hidden" >
 								</td-->
 							</tr>

 							<tr class="pdName">
 								<th>Product Name</th>
<%
	If Not(rs.eof) Then
		Do while Not rs.eof
%>
 								<td>
 									<a href="product_view.asp?idx=<%=rs("g_idx")%>"><em><%=rs("g_name")%></em></a>
 								</td>
<%
			rs.MoveNext
		Loop
	End If

	rs.MoveFirst
%>
 								<!--td>
 								<a href="product_view.html">
 										<em>삼성 노트북 NT951XCJ-K582S</em>
 										CPU Intel® Core™ i5-10210U 1.60 G(Up to 4.. </a>
 								</td>
 								<td>
 									<a href="product_view.html">
 										<em>삼성 노트북 NT931XCJ-K582S</em>
 										CPU Intel® Core™ i5-10210U 1.60 G(Up to 4... </a>
 								</td>
 								<td>
 									<a href="product_view.html">
 										<em>삼성 노트북 NT931XCJ-K582D</em>
 										CPU Intel® Core™ i5-10210U 1.60 G(Up to 4.. </a>
 								</td-->
 							</tr>
<%
	If Not(rs.eof) Then
		Do while Not rs.eof
			g_optionList = rs("g_optionList")
			arr_g_optionList = Split(g_optionList, ",")

			If Ubound(arr_g_optionList) > 0 Then
				For i = 0 To Ubound(arr_g_optionList) - 1
					arr_g_optionList1 = Split(arr_g_optionList(i), "||")

%>
 							<tr class="subheader">
 								<th class="depth"><%=Trim(arr_g_optionList1(0))%></th>
 								<td><%=Trim(arr_g_optionList1(1))%></td>
 								<!--td></td>
 								<td></td>
 								<td></td-->
 							</tr>
<%
				Next
			End If

			rs.MoveNext
		Loop
	End If

	rs.close
%>
 							<!--tr class="subheader">
 								<th class="depth">옵션2</th>
 								<td></td>
 								<td></td>
 								<td></td>
 								<td></td>
 							</tr>
 							<tr class="subheader">
 								<th class="depth">옵션3</th>
 								<td></td>
 								<td></td>
 								<td></td>
 								<td></td>
 							</tr-->

 						</tbody>
 					</table>
 				</div>
 			</div>
 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/_inc/_footer.asp" -->