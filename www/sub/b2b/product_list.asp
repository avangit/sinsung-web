<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/_inc/header.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/_css/sub.css" />
 	<script src="/js/jquery.navgoco.js"></script>
 	<script src="/js/product.js"></script>

 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$("#A_Header .gnb ul.nav li:nth-child(1)").addClass("active");
// 			$(".tab_contents").hide();
// 			$(".sub_con_tab li:first").addClass("active").show();
// 			$(".sub_con_tab li:first").show();
//
// 			$(".sub_con_tab li").click(function() {
// 				$(".sub_con_tab li").removeClass("active");
// 				$(this).addClass("active");
// 				$(".tab_contents").hide();
// 				var activeTab = $(this).find("a").attr("href");
// 				$(activeTab).fadeIn();
// 				return false;
// 			});

 			$(".pro_tab li").click(function() {
 				$(this).toggleClass('on');
 				$(".pro_tab_area1").show();
 			});
 			$(".pro_tab li").click(function() {
 				$(this).hasClass('on');
 				$(this).siblings().removeClass('open')
 			});


 		});
 		$(document).ready(function() {
 			$(".p_plus").click(function() {
 				if ($(this).hasClass('on')) {
 					$(this).removeClass('on');
 					$(this).parent().children('.sub_con_tab').css('height', '70px');
 				} else {
 					$(this).addClass('on');
 					$(this).parent().children('.sub_con_tab').css('height', '100%');
 				}
 			});
 			$(".sub_con_tab li").click(function() {
 				$(".pro_tab_area_hide").slideDown();
 			});
 			$(".li_close").click(function() {
 				$(this).remove();
 			});
 		});

 		$(document).ready(function() {
 			$('.f_hide').click(function() {
 				if ($(this).hasClass('open')) {
 					$(this).removeClass('open');
 					$(this).text('세부내용 숨기기');
 					$('.hide').show();
 				} else {
 					$(this).addClass('open');
 					$(this).text('세부내용 보기');
 					$('.hide').hide();
 				}
 			});
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/_inc/_header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="sub_nav">
 			<div class="inner">
 				<ul>
 					<li class="home"><a href="/">Home</a></li>
 					<li><a href="#">B2B 제품</a></li>
 				</ul>
 			</div>
 		</div>
 		<!-- 서브컨텐츠 -->
<%
	cate = Request.QueryString("c")
	Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
	Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
	Dim orderbys	: orderbys		=  SQL_Injection(Trim(Request("orderbys")))

	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
 		<div id="sub_contents" class="product product_default">
 			<div class="inner">
 				<div class="sub_tit">
 					<h2>B2B제품</h2>
 				</div>
 			</div>
 			<div class="pro_tab">
 				<div class="inner">
 					<ul>
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
 						<li <% If Left(cate, 2) = Left(rs("c_code"), 2) Then %> class="on"<% End If %>><a href="product01.asp?c=<%=rs("c_code")%>"><i></i><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
 					</ul>
 				</div>
 			</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 6) = '000000' AND c_code LIKE '" & Left(cate, 2) & "%' AND SUBSTRING(c_code, 4, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
 			<div id="A_Container">
 				<div class="inner">
 					<div class="pro_tab_area pro_tab_area1">
 						<ul class="sub_con_tab">
 							<li><a href="#product_01">전체</a></li>
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
 							<li><a href="?c=<%=rs("c_code")%>#product_01"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
 						</ul>
 						<button class="p_plus"></button>
 					</div>
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 4) = '0000' AND c_code LIKE '" & Left(cate, 4) & "%' AND SUBSTRING(c_code, 6, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
 					<div class="pro_tab_area pro_tab_area_hide">
 						<ul class="sub_con_tab">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
 							<li <% If cate = rs("c_code") Then %> class="active"<% End If %>><a href="product01.asp?c=<%=rs("c_code")%>"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
 						</ul>
 						<button class="p_plus2 p_plus"></button>
 					</div>
					<form name="s_form" action="" method="post">
 					<div class="search_box">
 						<div>
 							<select name="fieldname">
 								<option value="g_name" <% If fieldname = "" Or fieldname = "g_name" Then %> selected<% End If %>>제품명</option>
 								<option value="g_idx" <% If fieldname = "g_idx" Then %> selected<% End If %>>제품코드</option>
 							</select>
 							<input type="text" name="fieldvalue" value="<%=fieldvalue%>">
 							<button class="btn_search" onclick="document.s_form.submit();">검색</button>
 						</div>
 					</div>
 					<div class="align_box">
 						<a href="product01.asp"><img src="/images/sub/icon_one.png"></a>
 						<a href="product02.asp"><img src="/images/sub/icon_two.png"></a>
						<select name="orderbys" onchange="document.s_form.submit();">
							<option value="g_read" <% If orderbys = "g_read" Then %> selected<% End If %>>인기순</option>
							<option value="regdate" <% If orderbys = "regdate" Then %> selected<% End If %>>최신순</option>
							<option value="g_Money_asc" <% If orderbys = "g_Money_asc" Then %> selected<% End If %>>낮은 가격순</option>
							<option value="g_Money_desc" <% If orderbys = "g_Money_desc" Then %> selected<% End If %>>높은 가격순</option>
						</select>
 					</div>
					</form>
<%
	If cate <> "" Then
		If Right(cate, 8) = "00000000" Then
			query_where2 = query_where2 & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
		ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
			query_where2 = query_where2 & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
		ElseIf Mid(cate, 6, 1) <> 0 Then
			query_where2 = query_where2 & " AND a.g_category = '" & cate & "'"
		End If
		sql = "SELECT b.c_name, ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y' " & query_where2 & " GROUP BY b.c_name"
	Else
		If Len(fieldvalue1) > 0 Then
			query_where2 = query_where2 &" AND g_name LIKE '%" & fieldvalue1 & "%' "
		End If
		sql = "SELECT '전체제품' AS c_name, ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y' " & query_where2 & ""
	End If

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		cateName = rs("c_name")
		TotalCnt = rs("CNT")
	End If

	rs.close
%>
 					<div class="filter_set">
						<h4><%=cateName%> <span>(<%=TotalCnt%>)</span></h4>
						<button type="button" class="f_hide">세부내용 숨기기</button>
					</div>
 				</div>
<%
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 3
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " g_idx, g_name, g_Money, g_simg, g_spec, g_insertDay "
Dim query_Tablename		: query_Tablename	= " GOODS "
Dim query_where			: query_where		= " g_type = 1 AND g_display <> 'srm' AND g_act = 'Y'"

If cate <> "" Then
	If Right(cate, 8) = "00000000" Then
		query_where = query_where & " AND g_category LIKE '" & Left(cate,2) & "%'"
	ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
		query_where = query_where & " AND g_category LIKE '" & Left(cate,4) & "%'"
	ElseIf Mid(cate, 6, 1) <> 0 Then
		query_where = query_where & " AND g_category = '" & cate & "'"
	End If
End If

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

If Len(fieldvalue1) > 0 Then
	query_where = query_where &" AND g_name LIKE '%" & fieldvalue1 & "%' "
End If

If orderbys = "" Then
	query_orderby = " ORDER BY g_read DESC"
Else
	If orderbys = "g_Money_asc" Then
		query_orderby = " ORDER BY g_Money ASC"
	ElseIf orderbys = "g_Money_desc" Then
		query_orderby = " ORDER BY g_Money DESC"
	ElseIf orderbys = "regdate" Then
		query_orderby = " ORDER BY g_insertDay DESC"
	End If
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
 				<div class="w_100">
 					<div class="tab_contents" id="product_02">
 						<div class="inner">

							<form name="list" method="post" style="margin:0;">
							<input type="hidden" name="n">
							<input type="hidden" name="m">
							<input type="hidden" name="message">
 							<div id="A_Container_R" class="right_list03">
 								<ul class="pro_list pro_list03">
<%
If rs.bof Or rs.eof Then
%>
									<li>등록된 제품이 없습니다.</li>
<%
Else
	i = 0
	rs.move MoveCount
	Do While Not rs.eof
%>
 									<li>
 										<div class="a_box">
 											<div class="chk_box">
 												<input type="checkbox" id="pro0<%=i%>" value="<%=rs("g_idx")%>">
 												<label for="pro0<%=i%>"><span></span></label>
 											</div>
 											<label for="prod"><span></span></label>
 											<!--a href="javascript:GetCheckbox(document.list, 'compare', '옵션비교');">옵션비교</a-->
 											<a href="javascript:GetCheckbox(document.list, 'request', '견적요청');">견적요청</a>
 										</div>
 										<div class="p_contents">
 											<div class="img_box"><a href="product_view.asp?idx=<%=rs("g_idx")%>"> <img src="/upload/goods/<%=rs("g_simg")%>" alt="<%=rs("g_name")%>"></a></div>
 											<div class="cont_box">
 												<p class="pro_tit"><a href="product_view.asp?idx=<%=rs("g_idx")%>"><%=rs("g_name")%></a></p>
 												<span><a href="product_view.asp?idx=<%=rs("g_idx")%>"># <%=rs("g_idx")%></a></span>
 												<span class="hide"><a href="product_view.asp?idx=<%=rs("g_idx")%>"><%=rs("g_spec")%></a></span>
 											</div>
 										</div>
 									</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 								</ul>
 							</div>
							</form>
							<% Call Paging_user("") %>
 						</div>


 					</div>
 				</div>
 			</div>

 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/_inc/_footer.asp" -->