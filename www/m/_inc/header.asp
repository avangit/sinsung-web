<%
fieldvalue1 	=  SQL_Injection(Trim(Request("fieldvalue1")))
%>
<div class="inner">
	<h1 class="logo">
		<a href="/m/">SINSUNG CNS</a>
	</h1>
	<div class="user">
    <button class="btn_user" type="button" onclick="location.href=<% If Request.Cookies("U_ID") = "" Then %>'/m/sub/member/login.asp'<% Else %>'/m/sub/member/modify.asp'<% End If %>"><i></i></button>
    <button class="btn_search" type="button">검색</button>

	</div>
	  <div class="search_wrap">
	<div class="inner">
		<form name="search_form" method="post" action="/m/sub/b2b/product_list.asp">
			<input type="text" name="fieldvalue1" placeholder="검색어를 입력해주세요." value=<%=fieldvalue1%>>
			<div class="btn_box">
				<button class="btn_search02" type="button" onclick="if(document.search_form.fieldvalue1.value == '') {alert('제품명을 입력하세요');return false;} else {document.search_form.submit(); }">검색</button>
				<button class="btn_close" type="button">닫기</button>
			</div>
		</form>
            </div>
        </div>

	<ul class="menubar">
		<li></li>
		<li></li>
		<li></li>
	</ul>
	</div>

	<div class="menu_wrap">
	<div>
		<div class="gnb_wrap">
			<ul class="gnb">
			<li class="gnb_user">
			    <div>
				<% If Request.Cookies("U_IDX") = "" Then %>
					<a href="/m/sub/member/login.asp">로그인/회원가입</a>
				<% Else %>
					<a href="/m/sub/member/modify.asp">회원정보수정</a>
				<% End If %>
				</div>
				<% If Request.Cookies("U_IDX") <> "" Then %>
			    <div><a href="/m/sub/member/logout.asp">로그아웃</a></div>
				<% End If %>
			</li>
				<li>
					<p class="gnb_tit">제품구매</p>
					<ul class="lnb">
						<li><a href="/m/sub/b2b/product_list.asp">B2B 제품</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">솔루션</p>
					<ul class="lnb">
						<li><a href="/m/sub/solution/recovery.asp">신성 리커버리</a></li>
						<li><a href="/m/sub/solution/backup.asp">백업 및 복구</a></li>
						<li><a href="/m/sub/solution/server.asp">서버이중화</a></li>
						<li><a href="/m/sub/solution/virtual.asp">가상화</a></li>
						<li><a href="/m/sub/solution/document.asp">문서중앙화</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">화상회의</p>
					<ul class="lnb">
						<li><a href="/m/sub/conference/conference.asp">화상 회의 H/W 솔루션</a></li>
						<li><a href="/m/sub/conference/cf_sw.asp">화상 회의 S/W</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">고객서비스</p>
					<ul class="lnb">
						<li><a href="/m/sub/support/drivers7.asp">신성케어 7drivers</a></li>
						<li><a href="/m/sub/support/service.asp">맞춤형 세팅 서비스</a></li>
						<li><a href="/m/sub/support/it_data2.asp">기술자료설치매뉴얼</a></li>
						<li><a href="/m/sub/support/talk.asp">카톡1:1서비스</a></li>
						<li><a href="/m/sub/support/onlineReceive.asp">기술지원 문의</a></li>
						<li><a href="/m/sub/support/demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">우리의 경험</p>
					<ul class="lnb">
					<li><a href="/m/sub/experience/business.asp">사업소개</a></li>
						<li><a href="/m/sub/experience/it.asp">IT구매제안</a></li>
						<li><a href="/m/sub/experience/srm.asp">신성SRM</a></li>
						<li><a href="/m/sub/experience/construction_case.asp">구축사례</a></li>
						<li><a href="/m/sub/experience/cooperative.asp">협력사</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">인재경영</p>
					<ul class="lnb">
						<li><a href="/m/sub/company/plan.asp">우리성장</a></li>
						<li><a href="/m/sub/company/culture.asp">우리문화</a></li>
						<li><a href="/m/sub/company/video.asp">동영상</a></li>
						<li><a href="/m/sub/company/person.asp">인재상</a></li>
						<li><a href="/m/sub/company/welfare.asp">복지제도</a></li>
						<li><a href="/m/sub/company/recruit.asp">인재채용</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">회사소개</p>
					<ul class="lnb">
						<li><a href="/m/sub/company/management.asp">경영철학</a></li>
						<li><a href="/m/sub/company/growth.asp">회사성장</a></li>
						<li><a href="/m/sub/company/greeting.asp">대표 인사말</a></li>
						<li><a href="/m/sub/company/team.asp">팀을 만나다</a></li>
						<li><a href="/m/sub/company/history.asp">연혁</a></li>
						<li><a href="/m/sub/company/prize.asp">수상실적</a></li>
						<li><a href="/m/sub/company/notice.asp">공지사항</a></li>
						<li><a href="/m/sub/company/map.asp">찾아오시는 길</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">견적문의</p>
					<ul class="lnb">
<!--						<li><a href="/m/sub/online/estimate_list.asp">맞춤사양견적</a></li>-->
						<li><a href="/m/sub/online/onlineReceive2.asp">일반견적요청</a></li>
						<li><a href="/m/sub/online/onlineReceive_solution.asp">솔루션 상담요청</a></li>
						<li><a href="/m/sub/online/promotion.asp">프로모션</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="bt_gnb">
		<ul>
			<li><a href="/m/sub/experience/construction_case.asp">구축사례</a></li>
			<li><a href="/m/sub/support/drivers7.asp">신성케어 멤버십</a></li>
			<li><p>구매상담 02-267-3007</p></li>
		</ul>
	</div>
</div>


