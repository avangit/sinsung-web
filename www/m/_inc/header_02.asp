<div class="inner">
	<h1 class="logo">
		<a href="/m/">SINSUNG CNS</a>
	</h1>
	<div class="gnb_nav">
		<ul class="gnb">
			<li class="gnb01">
				<a href="history.asp">제품구매</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li class="lnb_b2b">
							<a href="ceo.asp">B2B 제품</a>
							<div class="b2b_prd_menu">
								<div class="b2b_inner">
									<ul class="depth01">
										<li>
											<p class="depth01_tit">하드웨어</p>
											<ul class="depth02">
												<li>
													<a href="javascript:;">노트북</a>
													<ul class="depth03">
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
													</ul>
												</li>
												<li><a href="#">데스크탑</a></li>
												<li><a href="#">모니터</a></li>
												<li><a href="#">서버</a></li>
												<li><a href="#">워크스테이션</a></li>
												<li><a href="#">TV/디스플레이</a></li>
												<li><a href="#">프린터/복합기</a></li>
												<li><a href="#">네트워크</a></li>
											</ul>
										</li>
										<li>
											<p class="depth01_tit">소프트웨어</p>
											<ul class="depth02">
												<li>
													<a href="#">백업솔루션</a>
													<ul class="depth03">
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
													</ul>
												</li>
												<li><a href="#">NAS 스토리지</a></li>
												<li><a href="#">화상회의 장비</a></li>
												<li><a href="#">케어팩 서비스</a></li>
											</ul>
										</li>
										<li>
											<p class="depth01_tit">전산소모품</p>
											<ul class="depth02">
												<li>
													<a href="#">SSD</a>
													<ul class="depth03">
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
														<li><a href="#">3차 카테고리</a></li>
													</ul>
												</li>
												<li><a href="#">토너</a></li>
												<li><a href="#">전산소모품</a></li>
											</ul>
										</li>
									</ul>
									<div class="brand">
										<ul>
											<li><img src="/m/images/common/brand_logo01.png" alt="NAVER" /></li>
											<li><img src="/m/images/common/brand_logo02.png" alt="Daum" /></li>
											<li><img src="/m/images/common/brand_logo02.png" alt="kakao" /></li>
											<li><img src="/m/images/common/brand_logo04.png" alt="Melon" /></li>
											<li><img src="/m/images/common/brand_logo05.png" alt="genie" /></li>
											<li><img src="/m/images/common/brand_logo01.png" alt="NAVER" /></li>
											<li><img src="/m/images/common/brand_logo02.png" alt="Daum" /></li>
											<li><img src="/m/images/common/brand_logo02.png" alt="kakao" /></li>
											<li><img src="/m/images/common/brand_logo04.png" alt="Melon" /></li>
											<li><img src="/m/images/common/brand_logo05.png" alt="genie" /></li>
											<li><img src="/m/images/common/brand_logo01.png" alt="NAVER" /></li>
											<li><img src="/m/images/common/brand_logo02.png" alt="Daum" /></li>
											<li><img src="/m/images/common/brand_logo02.png" alt="kakao" /></li>
											<li><img src="/m/images/common/brand_logo04.png" alt="Melon" /></li>
											<li><img src="/m/images/common/brand_logo05.png" alt="genie" /></li>
										</ul>
									</div>
								</div>
							</div>
						</li>
						<li><a href="history.asp">IT 자료실</a></li>
						<li><a href="vision.asp">공지사항</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb02">
				<a href="busi_housing.asp">솔루션</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="busi_housing.asp">신성 리커버리</a></li>
						<li><a href="busi_building.asp">백업 및 복구</a></li>
						<li><a href="busi_landscaping.asp">서버이중화</a></li>
						<li><a href="busi_civil_engineering.asp">가상화</a></li>
						<li><a href="busi_plant.asp">문서중앙화</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb03">
				<a href="quality.asp">화상회의</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="quality.asp">화상 회의 H/W 솔루션</a></li>
						<li><a href="environmental.asp">화상 회의 S/W</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb04">
				<a href="invest_financial.asp">고객서비스</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="drivers7.asp">신성케어 7drivers</a></li>
						<li><a href="service.asp">맞춤형 세팅 서비스</a></li>
						<li><a href="it_data2.asp">기술자료설치매뉴얼</a></li>
						<li><a href="talk.asp">카톡1:1서비스</a></li>
						<li><a href="onlineReceive.asp">기술지원 문의</a></li>
						<li><a href="demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb05">
				<a href="brand.asp">우리의 경험</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="brand.asp">사업소개</a></li>
						<li><a href="notice.asp">IT구매제안</a></li>
						<li><a href="pr_material.asp">신성SRM</a></li>
						<li><a href="people.asp">구축사례</a></li>
						<li><a href="ci.asp">협력사</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb06">
				<a href="recruit.asp">인재경영</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="plan.asp">우리성장</a></li>
						<li><a href="culture.asp">우리문화</a></li>
						<li><a href="video.asp">동영상</a></li>
						<li><a href="person.asp">인재상</a></li>
						<li><a href="welfare.asp">복지제도</a></li>
						<li><a href="empNotice.asp">인재채용</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb07">
				<a href="faq.asp">회사소개</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="management.asp">경영철학</a></li>
						<li><a href="growth.asp">회사성장</a></li>
						<li><a href="greeting.asp">대표 인사말</a></li>
						<li><a href="team.asp">팀을 만나다</a></li>
						<li><a href="history.asp">연혁</a></li>
						<li><a href="prize.asp">수상실적</a></li>
						<li><a href="notice.asp">공지사항</a></li>
						<li><a href="map.asp">찾아오시는 길</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb08">
				<a href="faq.asp">견적문의</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="faq.asp">맞춤사양견적</a></li>
						<li><a href="inquiry.asp">일반견적요청</a></li>
						<li><a href="inquiry_result.asp">솔루션 상담요청</a></li>
						<li><a href="location.asp">프로모션</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
	<div class="util">
		<ul>
			<li><a href="#">로그인</a></li>
			<li><a href="#">구축사례</a></li>
			<li><a href="#">신성케어 멤버십</a></li>
			<li><p>구매상담 02-267-3007</p></li>
		</ul>
		<button class="btn_search" type="button">검색</button>
	</div>
</div>
<div class="search_wrap">
	<div class="inner">
		<form>
			<input type="text" name="" placeholder="검색어를 입력해주세요.">
			<div class="btn_box">
				<button class="btn_search02" type="button">검색</button>
				<button class="btn_close" type="button">닫기</button>
			</div>
		</form>
		<div class="hot_keyword">
			<p>인기검색어</p>
			<ul>
				<li><a href="#">노트북</a></li>
				<li><a href="#">데스크탑</a></li>
				<li><a href="#">모니터</a></li>
				<li><a href="#">서버</a></li>
				<li><a href="#">워크스테이션</a></li>
			</ul>
		</div>
	</div>
</div>

