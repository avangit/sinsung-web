<div class="inner">
	<h1 class="logo">
		<a href="/m/">SINSUNG CNS</a>
	</h1>
	<div class="user">
    <button  class="btn_user" type="button" onclick="location.href='login.asp' "><i></i></button>
    <button class="btn_search" type="button">검색</button>
	  
	</div>
	  <div class="search_wrap">
	<div class="inner">
		<form>
			<input type="text" name="" placeholder="검색어를 입력해주세요.">
			<div class="btn_box">
				<button class="btn_search02" type="button">검색</button>
				<button class="btn_close" type="button">닫기</button>
			</div>
		</form>
            </div>
        </div>
	
	<ul class="menubar">
		<li></li>
		<li></li>
		<li></li>
	</ul>
	</div>
	
	<div class="menu_wrap">
	<div>
		<div class="gnb_wrap">
			<ul class="gnb">
			<li class="gnb_user">
			    <div><a href="#">로그아웃</a></div>
			    <div><a href="modify.asp">정보수정</a></div>
			</li>
				<li>
					<p class="gnb_tit">제품구매</p>
					<ul class="lnb">
						<li><a href="product_list.asp">B2B 제품</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">솔루션</p>
					<ul class="lnb">
						<li><a href="recovery.asp">신성 리커버리</a></li>
						<li><a href="backup.asp">백업 및 복구</a></li>
						<li><a href="server.asp">서버이중화</a></li>
						<li><a href="virtual.asp">가상화</a></li>
						<li><a href="document.asp">문서중앙화</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">화상회의</p>
					<ul class="lnb">
						<li><a href="conference.asp">화상 회의 H/W 솔루션</a></li>
						<li><a href="cf_sw.asp">화상 회의 S/W</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">고객서비스</p>
					<ul class="lnb">
						<li><a href="drivers7.asp">신성케어 7drivers</a></li>
						<li><a href="service.asp">맞춤형 세팅 서비스</a></li>
						<li><a href="it_data2.asp">기술자료설치매뉴얼</a></li>
						<li><a href="talk.asp">카톡1:1서비스</a></li>
						<li><a href="onlineReceive.asp">기술지원 문의</a></li>
						<li><a href="demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">우리의 경험</p>
					<ul class="lnb">
					<li><a href="business.asp">사업소개</a></li>
						<li><a href="it.asp">IT구매제안</a></li>
						<li><a href="srm.asp">신성SRM</a></li>
						<li><a href="construction_case.asp">구축사례</a></li>
						<li><a href="cooperative.asp">협력사</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">인재경영</p>
					<ul class="lnb">
						<li><a href="plan.asp">우리성장</a></li>
						<li><a href="culture.asp">우리문화</a></li>
						<li><a href="video.asp">동영상</a></li>
						<li><a href="person.asp">인재상</a></li>
						<li><a href="welfare.asp">복지제도</a></li>
						<li><a href="recruit.asp">인재채용</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">회사소개</p>
					<ul class="lnb">
						<li><a href="management.asp">경영철학</a></li>
						<li><a href="growth.asp">회사성장</a></li>
						<li><a href="greeting.asp">대표 인사말</a></li>
						<li><a href="team.asp">팀을 만나다</a></li>
						<li><a href="history.asp">연혁</a></li>
						<li><a href="prize.asp">수상실적</a></li>
						<li><a href="notice.asp">공지사항</a></li>
						<li><a href="map.asp">찾아오시는 길</a></li>
					</ul>
				</li>
				<li>
					<p class="gnb_tit">견적문의</p>
					<ul class="lnb">
						<li><a href="estimate_list.asp">맞춤사양견적</a></li>
						<li><a href="onlineReceive2.asp">일반견적요청</a></li>
						<li><a href="onlineReceive_solution.asp">솔루션 상담요청</a></li>
						<li><a href="promotion.asp">프로모션</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="bt_gnb">
		<ul>
			<li><a href="construction_case.asp">구축사례</a></li>
			<li><a href="drivers7.asp">신성케어 멤버십</a></li>
			<li><p>구매상담 02-267-3007</p></li>
		</ul>
	</div>
</div>


