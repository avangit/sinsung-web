function agree_chk() {
	if($("#agree1").is(":checked") == false) {
		$("#agree1").attr("checked", true);
	}

	if($("#agree2").is(":checked") == false) {
		$("#agree2").attr("checked", true);
	}

	if($("#agree1").is(":checked") == true && $("#agree2").is(":checked") == true) {
		location.href = './join2.asp';
	}
}

function getXMLHttpRequest() {
	if (window.ActiveXObject) {
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e1) { return null; }
		}
	} else if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else {
		return null;
	}
}
var httpRequest = null;

function sendRequest(url, params, callback, method) {
	httpRequest = getXMLHttpRequest();
	var httpMethod = method ? method : 'GET';
	if (httpMethod != 'GET' && httpMethod != 'POST') {
		httpMethod = 'GET';
	}
	var httpParams = (params == null || params == '') ? null : params;
	var httpUrl = url;
	if (httpMethod == 'GET' && httpParams != null) {
		httpUrl = httpUrl + "?" + httpParams;
	}
	httpRequest.open(httpMethod, httpUrl, true);
	httpRequest.setRequestHeader(
		'Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.onreadystatechange = callback;
	httpRequest.send(httpMethod == 'POST' ? httpParams : null);
}

function id_check(){
	var frm	=	document.joinform;
	var id=frm.strId.value;

	
	if(id.length < 4 || id.length> 20) {
		alert('4~20자 이내의 아이디를 입력하여 주십시오.');
		frm.strId.focus();
		return;
	}

	var chk_num = id.search(/[0-9]/g); 
	var chk_eng = id.search(/[a-z]/ig); 
	//var chk_esp = id.search(/[^0-9a-zA-Z]/g);  
	/*
	if(chk_eng> 0 || chk_num < 0) {
		alert('영문 또는 영문+숫자의 조합으로 아이디를 설정해 주세요 첫번째 글자는 영문입니다.'); 
		frm.strId.focus();
		return;
	}
	*/
	var params = "strId="+encodeURIComponent(id);
	sendRequest("/m/sub/member/idCheck_ajax.asp", params, idcheckResult, 'POST');

}

function idcheckResult() {
	if (httpRequest.readyState == 4) {

		if (httpRequest.status == 200) {
			var resultText = httpRequest.responseText;

			rel=resultText.split("||");
			var jangList=document.getElementById("id_ck");

			var resultText = httpRequest.responseText;
			jangList.innerHTML=rel[1];

			if(rel[0]=="y"){ document.joinform.num.value="y";	}else{	document.joinform.num.value="";	}

		} else {
			alert("Error: "+httpRequest.status);
		}
	}
}

function ChangeEmail() {
	if (joinform.strEmail3.value == '0') {
		joinform.strEmail2.readOnly = false;
		joinform.strEmail2.value = '';
		joinform.strEmail2.focus();
	} else {
		joinform.strEmail2.readOnly = true;
		joinform.strEmail2.value = joinform.strEmail3.value;
	}
}

function login_chk() {
	var frm=document.login;

	if(frm.uid.value == "") {
		alert("아이디를 입력해 주세요.");
		frm.uid.focus();
		return false;
	} else if(frm.uid.value.length < 4 || frm.uid.value.length > 20) {
		alert("아이디는 4~20자 이내입니다.");
		frm.uid.focus();
		return false;
	} else if(frm.upass.value == "") {
		alert("비밀번호를 입력해 주세요.");
		frm.upass.focus();
		return false;
	} else if(frm.upass.value.length < 4 || frm.upass.value.length > 16) {
		alert("비밀번호는 4~16자 이내입니다.");
		frm.upass.focus();
		return false;
	} else {
		frm.submit();
	}
}

function join_chk() {
	var frm = document.joinform;

	if(frm.cName.value == "") {
		alert("회사명을 입력해 주세요.");
		frm.cName.focus();
		return false;
	} else if(frm.cNum.value == "") {
		alert("사업자번호를 입력해 주세요.");
		frm.cNum1.focus();
		return false;
	} else if(frm.strId.value == "") {
		alert("아이디를 입력해 주세요.");
		frm.strId.focus();
		return false;
	} else if(frm.strId.value.length < 4 || frm.strId.value.length > 20) {
		alert("아이디는 4~20자 이내입니다.");
		frm.strId.focus();
		return false;
	} else if(frm.num.value == "") {
		alert("아이디 중복체크를 하시거나 중복된 아이디 사용을 피해주세오.");
		id_check();
		return false;
	} else if(frm.strPwd.value == "") {
		alert("비밀번호를 입력해 주세요.");
		frm.strPwd.focus();
		return false;
	} else if(frm.strPwd.value.length < 4 || frm.strPwd.value.length > 16) {
		alert("비밀번호는 4~16자 이내입니다.");
		frm.strPwd.focus();
		return false;
	} else if(frm.strPwdRe.value == "") {
		alert("비밀번호 재확인을 입력해 주세요.");
		frm.strPwdRe.focus();
		return false;
	} else if(frm.strPwd.value != frm.strPwdRe.value) {
		alert("비밀번호가 일치하지 않습니다.");
		frm.strPwdRe.focus();
		return false;
	} else if(frm.strName.value == "") {
		alert("이름을 입력해 주세요.");
		frm.strName.focus();
		return false;
	} else if(frm.strMobile1.value == "" || frm.strMobile2.value == "" || frm.strMobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.strMobile1.focus();
		return false;
	} else if(frm.strEmail1.value == "" || frm.strEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.strEmail1.focus();
		return false;
	} else if(frm.cZip.value == "" || frm.cAddr1.value == "") {
		alert("사업장 주소를 입력해 주세요.");
		return false;
	} else {
		frm.submit();
	}
}

function id_search() {
	var frm = document.findIdform;

	if(frm.strName.value == "") {
		alert("이름을 입력해 주세요.");
		frm.strName.focus();
		return false;
	} else if(frm.strEmail1.value == "" || frm.strEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.strEmail1.focus();
		return false;
	} else if(frm.strMobile1.value == "" || frm.strMobile2.value == "" || frm.strMobile3.value == "") {
		alert("휴대폰번호를 입력해 주세요.");
		frm.strMobile1.focus();
		return false;
	} else {
		frm.submit();
	}
}

function pw_search() {
	var frm = document.findPwform;

	if(frm.strId.value == "") {
		alert("아이디를 입력해 주세요.");
		frm.strId.focus();
		return false;
	} else if(frm.strName.value == "") {
		alert("이름을 입력해 주세요.");
		frm.strName.focus();
		return false;
	} else if(frm.strEmail1.value == "" || frm.strEmail2.value == "") {
		alert("이메일을 입력해 주세요.");
		frm.strEmail1.focus();
		return false;
	} else {
		frm.submit();
	}
}