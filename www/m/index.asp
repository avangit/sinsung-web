 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
     <!-- #include virtual="/m/_inc/head.asp" -->
     <!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
     <link rel="stylesheet" href="/m/_css/slick.css" />
     <link rel="stylesheet" href="/m/_css/slick-theme.css" />
     <link rel="stylesheet" href="/m/_css/style.css" />
     <script src="/m/js/slick.js"></script>
     <!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
     <script>
         $(document).on('ready', function() {
            $('#visual').slick({
                 accessibility: false,
                 autoplay: true,
                 autoplaySpeed: 3000,
                 speed: 1000,
                 slidesToShow: 1,
                 slidesToScroll: 1,
                 dots: true,
                 arrows: false,
             });
             $('.prd_slide').slick({
                 accessibility: false,
                 autoplay: true,
                 autoplaySpeed: 5000,
                 speed: 1000,
                 slidesToShow: 2,
                 slidesToScroll: 1,
                 dots: true,
                 arrows: false,
             });
             $('.review_slide').slick({
                 accessibility: false,
                 autoplay: true,
                 autoplaySpeed: 5000,
                 speed: 1000,
                 slidesToShow: 1,
                 slidesToScroll: 1,
                 dots: true,
                 arrows: true,
             });
             $('.brand_slide').slick({
                 accessibility: false,
                 autoplay: true,
                 autoplaySpeed: 5000,
                 speed: 1000,
                 slidesToShow: 3,
                 slidesToScroll: 1,
                 dots: true,
                 arrows: false,
             });

             // 가이드 팝업
             var posY;

             function bodyFreezeScroll() {
                 posY = $(window).scrollTop();
                 $("html").addClass('fix');
                 $("html").css("top", -posY);
             }

             function bodyUnfreezeScroll() {
                 $("html").removeAttr('class');
                 $("html").removeAttr('style');
                 posY = $(window).scrollTop(posY);
             }


             $('.quick_area .btn_blue').click(function() {
                 $('.form_popup').fadeIn();
                 bodyFreezeScroll()
             });
             $('.quick_area .btn_pop_close').click(function() {
                 $('.form_popup').fadeOut();
                 bodyUnfreezeScroll()
             });

			$('.subscription_area .btn_subscribe').click(function(){
				if($("#email").val() == "") {
					alert("이메일 주소를 입력해 주세요");
					$("#email").focus();
					return false;
				} else {
					$('.pop_subscribe').fadeIn();
					bodyFreezeScroll();
					$("#s_email").val($("#email").val());
				}
			});

			$('.pop_subscribe .btn_pop_close').click(function() {
				$('.pop_subscribe').fadeOut();
				bodyUnfreezeScroll();
				$("#s_email").val('');
			});
         });


		function online_chk() {
			var frm = document.online_form;

			if(frm.on_company.value == "") {
				alert("회사명을 입력해주세요.");
				frm.on_company.focus();
				return false;
			} else if(frm.on_name.value == "") {
				alert("이름을 입력해주세요.");
				frm.on_name.focus();
				return false;
			} else if(frm.on_mobile.value == "") {
				alert("연락처를 입력해주세요.");
				frm.on_mobile.focus();
				return false;
			} else if(frm.on_email.value == "") {
				alert("이메일을 입력해주세요.");
				frm.on_email.focus();
				return false;
			} else if(frm.on_title.value == "") {
				alert("제목을 입력해주세요.");
				frm.on_title.focus();
				return false;
			} else if(frm.on_content.value == "") {
				alert("문의내용을 입력해주세요.");
				frm.on_content.focus();
				return false;
			} else {
				frm.submit();
			}
		}

		function subscribe_chk() {
			var frm = document.regform2;

			if(frm.s_company.value == "") {
				alert("회사명을 입력해주세요.");
				frm.s_company.focus();
				return false;
			} else if(frm.s_email.value == "") {
				alert("이메일을 입력해주세요.");
				frm.s_email.focus();
				return false;
			} else if(frm.s_name.value == "") {
				alert("이름을 입력해주세요.");
				frm.s_name.focus();
				return false;
			} else if(frm.s_addr.value == "") {
				alert("주소를 입력해주세요.");
				frm.s_addr.focus();
				return false;
			} else {
				frm.submit();
			}
		}
     </script>
 </head>

 <body>
     <div id="A_Wrap">
         <div id="A_Header">
             <!-- #include virtual="/m/_inc/header.asp" -->
         </div>
         <div id="A_Container_Wrap">
             <!-- 메인 비주얼 -->
             <div id="visual">
                <div class="slide slide01">
                     <div class="visual_inner">
                         <div class="txt_box">
                             <strong>신성 무료 케어 서비스 출시</strong>
                             <p>IT 전문가로서 기업의 올바른 업무환경 구현을 위해<br />최고의 솔루션을 지속적으로 제공합니다.</p>
                         </div>
                     </div>
                 </div>
                 <div class="slide slide02">
                     <div class="visual_inner">
                         <div class="txt_box">
                             <strong>신성 무료 케어 서비스 출시</strong>
                             <p>IT 전문가로서 기업의 올바른 업무환경 구현을 위해<br />최고의 솔루션을 지속적으로 제공합니다.</p>
                         </div>
                     </div>
                 </div>
                 <div class="slide slide03">
                     <div class="visual_inner">
                         <div class="txt_box">
                             <strong>신성 무료 케어 서비스 출시</strong>
                             <p>IT 전문가로서 기업의 올바른 업무환경 구현을 위해<br />최고의 솔루션을 지속적으로 제공합니다.</p>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="drivers_area">
                 <div class="inner">
                     <h3 class="sub_tit">2,320개 기업이 <br><em>신성씨앤에스를 선택한 이유</em></h3>
                     <p class="sub_txt">고객의 OS 설치와 조립 없는 전산 업무환경! <br>IT 구매와 관리로 시간 낭비 없는 업무 효율화!<br />우리는 기업의 올바른 업무환경 구현을 위해 <br>최고의 제품과 only 신성 케어 서비스를 제공합니다.</p>
                     <strong class="drivers_tit">Sinsung Care Service 7 drivers</strong>
                     <ul class="drivers_list">
                         <li>
                             <i></i>
                             <p>신속한<br />맞춤세팅</p>
                         </li>
                         <li>
                             <i></i>
                             <p>전담 엔지니어<br />콜센터 운영</p>
                         </li>
                         <li>
                             <i></i>
                             <p>설치&amp;장애<br />매뉴얼 제공</p>
                         </li>
                         <li>
                             <i></i>
                             <p>고객사 맞춤<br />구매시스템</p>
                         </li>
                         <li>
                             <i></i>
                             <p>자산, 시리얼<br />이력관리</p>
                         </li>
                         <li>
                             <i></i>
                             <p>발송&amp;회수<br />무상 서비스</p>
                         </li>
                         <li>
                             <i></i>
                             <p>데모장비<br />지원</p>
                         </li>
                     </ul>
                 </div>
             </div>
             <div class="product_area">
                 <div class="inner">
                     <h3 class="sub_tit">Our <em>Products</em></h3>
                     <ul class="product_list">
                         <li>
                             <a href="/m/sub/b2b/product_category.asp?c=1001000000">
                                 <img src="/m/images/main/prd01.jpg" />
                                 <div class="txt_box">
                                     <strong>노트북</strong>
                                     <p>NoteBook</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="/m/sub/b2b/product_category.asp?c=1005000000">
                                 <img src="/m/images/main/prd02.jpg" />
                                 <div class="txt_box">
                                     <strong>모니터</strong>
                                     <p>Monitor</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="/m/sub/b2b/product_category.asp?c=1004000000">
                                 <img src="/m/images/main/prd03.jpg" />
                                 <div class="txt_box">
                                     <strong>서버</strong>
                                     <p>Server</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="/m/sub/b2b/product_category.asp?c=1003000000">
                                 <img src="/m/images/main/prd04.jpg" />
                                 <div class="txt_box">
                                     <strong>워크스테이션</strong>
                                     <p>Workstation</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="/m/sub/b2b/product_category.asp?c=1002000000">
                                 <img src="/m/images/main/prd05.jpg" />
                                 <div class="txt_box">
                                     <strong>데스크탑</strong>
                                     <p>Desktop</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="/m/sub/b2b/product_category.asp?c=1300000000">
                                 <img src="/m/images/main/prd06.jpg" />
                                 <div class="txt_box">
                                     <strong>프린터&amp;복합기</strong>
                                     <p>Printer</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="#">
                                 <img src="/m/images/main/prd07.jpg" />
                                 <div class="txt_box">
                                     <strong>플로터</strong>
                                     <p>Plotter</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="/m/sub/b2b/product_category.asp?c=1602000000">
                                 <img src="/m/images/main/prd08.jpg" />
                                 <div class="txt_box">
                                     <strong>전산소모품<span>(토너, 잉크, 주변기기)</span></strong>
                                     <p>Toner, Ink, Peripheral</p>
                                 </div>
                             </a>
                         </li>
                     </ul>
                 </div>
             </div>
             <div class="custom_area">
                 <div class="inner">
                     <h3 class="sub_tit"><em>브랜드 PC 맞춤 견적</em></h3>
                     <p class="sub_txt">기업 환경에 필요한 사양만을 선택하여, <br>맞춤 견적요청 및 주문생산을 하실 수 있습니다.</p>
                     <p class="center_info">전문 IT 컨설턴트 상담센터 02-867-3007<br />
                         상담시간 : 10:00 ~ 18:00</p>
                     <ul class="step_list">
                         <li>
                             <i></i>
                             <span>STEP 01</span>
                             <p>하드웨어 선택</p>
                         </li>
                         <li>
                             <i></i>
                             <span>STEP 02</span>
                             <p>사양선택</p>
                         </li>
                         <li>
                             <i></i>
                             <span>STEP 03</span>
                             <p>견적확인</p>
                         </li>
                         <li>
                             <i></i>
                             <span>STEP 04</span>
                             <p>Nego 요청</p>
                         </li>
                         <li>
                             <i></i>
                             <span>STEP 05</span>
                             <p>이메일 견적회신</p>
                         </li>
                     </ul>
                     <button type="button" class="btn_blue">맞춤 주문제작</button>
                 </div>
             </div>
             <div class="quick_area">
                 <div class="inner">
                     <h3 class="sub_tit">빠른 견적 요청</h3>
                     <p class="sub_txt">* 30분 이내 견적을 받아보실 수 있습니다.</p>
                     <button type="button" class="btn_blue">견적요청</button>
                     <div class="form_popup popup">
                         <div class="popup_wrap">
                             <div class="pop_tit">
                                 <strong>빠른 견적 요청</strong>
                             </div>
								<form name="online_form" method="post" action="./index_ok.asp" class="estimate_form">
								<input type="hidden" name="mode" value="online">
								<input type="hidden" name="on_type" value="estimate1">
								<input type="hidden" name="isfastYN" value="Y">
								<input type="hidden" name="mem_idx" value="<%=Request.Cookies("U_IDX")%>">
                                 <div>
                                     <table class="table_type01">
                                         <tbody>
                                             <tr>
                                                 <th>회사명</th>
                                                 <td><input type="text" name="on_company" maxlength="50"></td>
                                             </tr>
                                             <tr>
                                                 <th>이름</th>
                                                 <td><input type="text" name="on_name" maxlength="30"></td>
                                             </tr>
                                             <tr>
                                                 <th>연락처</th>
                                                 <td><input type="text" name="on_mobile" maxlength="14"></td>
                                             </tr>
                                             <tr>
                                                 <th>이메일</th>
                                                 <td><input type="text" name="on_email" maxlength="100"></td>
                                             </tr>
                                         </tbody>
                                     </table>
                                     <table class="table_type01">
                                         <tbody>
                                             <tr>
                                                 <th class="j_tit">제목</th>
                                                 <td><input type="text" name="on_title" maxlength="100"></td>
                                             </tr>
                                             <tr class="textarea_box">
                                                 <td colspan="2">
                                                     <textarea name="on_content" placeholder="문의내용"></textarea>
                                                 </td>
                                             </tr>

                                         </tbody>
                                     </table>
                                     <button type="button" class="btn_blue" onclick="online_chk();return false;">견적요청</button>
                                 </div>
                                 <button type="button" class="btn_pop_close">닫기</button>
								</form>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="best_area">
                 <div class="inner">
                     <h3 class="sub_tit">BEST <em>Seller</em></h3>
                     <div class="prd_slide">
<%
	Call DbOpen()

	sql = "SELECT g_idx, g_name, g_simg FROM GOODS WHERE g_type = 1 AND g_display <> 'srm' AND g_act = 'Y' AND g_bestYN = 'Y' ORDER BY g_idx DESC"
	Set rs = dbconn.execute(sql)

	If Not(rs.eof) Then
		Do While Not rs.eof
%>
                         <div class="slide">
                             <a href="/m/sub/b2b/product_view.asp?idx=<%=rs("g_idx")%>">
                                 <div class="img_box">
                                     <img src="/upload/goods/<%=rs("g_simg")%>" />
                                 </div>
                                 <p><%=rs("g_name")%></p>
                             </a>
                         </div>
<%
			rs.MoveNext
		Loop
	End If

	rs.close
%>
                     </div>
                 </div>
             </div>
             <div class="solution_area">
                 <div class="inner">
                     <h3 class="sub_tit"><em>SOLUTION</em></h3>
                     <ul class="solution_list">
                         <li>
                             <a href="/m/sub/conference/conference.asp">
                                 <div class="txt_box">
                                     <strong>화상회의 솔루션</strong>
                                     <p>도시, 국가, 대륙의 사람들을 생생한 화상회의로 사람들을 이동시켜, <br>비즈니스가 더 스마트하게 작동하도록 합니다. <br>개인 업무공간부터 대형 회의실까지 어디에서나 가능합니다.</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="/m/sub/solution/server.asp">
                                 <div class="txt_box">
                                     <strong>서버 이중화</strong>
                                     <p>서버, 애플리케이션 서비스, 네트워크, 스토리지뿐만 아니라 <br>시스템 리소스와 애플리케이션 리소스 문제로 인한 장애에 대해서 <br>서비스 연속성을 보장하고 가용성을 극대화합니다.</p>
                                 </div>
                             </a>
                         </li>
                         <li>
                             <a href="/m/sub/solution/backup.asp">
                                 <div class="txt_box">
                                     <strong>백업솔루션</strong>
                                     <p>수많은 백업 및 복구 업체 중에서 누가 최고인지 어떻게 알 수 있을까요? <br />아주 간단합니다. Partner Magic Quadrant를 확인하십시오.</p>
                                 </div>
                             </a>
                         </li>
                     </ul>
                 </div>
             </div>
             <div class="brand_area">
                 <div class="inner">
                     <h3 class="sub_tit">2,320개 이상의 기업에게 <br><em>신성 케어 서비스를 제공하고 있습니다.</em></h3>
                     <ul class="brand_list brand_slide">
                         <li><img  style="position: absolute; transform: translate(-50%,-50%); top: 50%; left: 50%;"src="/m/images/main/brand01.png" /></li>
                         <li><img  style="position: absolute; transform: translate(-50%,-50%); top: 50%; left: 50%;" src="/m/images/main/brand02.png" /></li>
                         <li><img  style="position: absolute; transform: translate(-50%,-50%); top: 50%; left: 50%;" src="/m/images/main/brand03.png" /></li>
                         <li><img  style="position: absolute; transform: translate(-50%,-50%); top: 50%; left: 50%;"  src="/m/images/main/brand04.png" /></li>
                         <li><img   style="position: absolute; transform: translate(-50%,-50%); top: 50%; left: 50%;" src="/m/images/main/brand05.png" /></li>
                     </ul>
                     <button type="button" class="btn_blue02" onclick="location.href='/m/sub/experience/cooperative.asp'">Read More</button>
                 </div>
             </div>
             <div class="review_area">
                 <div class="inner">
                     <h3 class="sub_tit">신성씨앤에스 <em>고객리뷰</em></h3>
                     <div class="review_slide">
<%
	sql = "SELECT b_name, b_text, b_writeday FROM BOARD_v1 WHERE b_part = 'board11'"
	Set rs = dbconn.execute(sql)

	If Not(rs.eof) Then
		Do While Not rs.eof
%>
                         <div class="slide">
                             <a href="#">
                                 <div class="txt_box">
                                     <%=rs("b_text")%>
                                     <strong class="writer"><%=rs("b_name")%> <span><%=Left(rs("b_writeday"), 10)%></span></strong>
                                 </div>
                             </a>
                         </div>
<%
			rs.MoveNext
		Loop
	End If

	rs.close
%>
                     </div>
                 </div>
             </div>
             <div class="our_area">
                 <div class="inner">
                     <ul>
                         <li>
                             <i></i>
                             <strong>경영철학</strong>
                             <p>IT 전문가로서 기업의 올바른 <br />업무환경 구현을 위해 최고의 솔루션을 지속적으로 제공합니다.</p>
                         </li>
                         <li>
                             <i></i>
                             <strong>우리의 성장과 문화</strong>
                             <p>돈보다 일 중심, 일보다 사람 중심</p>
                         </li>
                         <li>
                             <i></i>
                             <strong>수상실적</strong>
                             <p>유비쿼터스 환경에서 최적화된 <br />Infra/Solution/Service를 제공하는 <br />Business Enable</p>
                         </li>
                     </ul>
                 </div>
             </div>
             <div class="subscription_area">
                 <div class="inner">
                     <div class="txt_box">
                         <h3 class="sub_tit">신제품, 매뉴얼 구독</h3>
                         <form>
                             <input type="text"  name="email" id="email" placeholder="당신의 이메일 주소를 입력해 주세요.">
                             <button type="button" class="btn_subscribe">구독하기</button>
                         </form>
                     </div>
                     <div class="icon_box">
                         <ul>
                             <li>
                                 <div>
                                     <i></i>
                                     <p>견적문의</p>
                                     <span>30분 이내 회신</span>
                                 </div>
                             </li>
                             <li>
                                 <div>
                                     <i></i>
                                     <p>SRM 전자<br />구매 시스템</p>
                                 </div>
                             </li>
                             <li>
                                 <div>
                                     <i></i>
                                     <p>기술담당자와<br />채팅하기</p>
                                 </div>
                             </li>
                             <li>
                                 <div>
                                     <i></i>
                                     <p>원격<br />기술지원</p>
                                 </div>
                             </li>
                         </ul>
                     </div>
                 </div>
             </div>
             <div class="popup pop_subscribe">
                 <div class="popup_wrap">
                     <div class="pop_tit">
                         <i></i>
                         <strong>Subscribe</strong>
                         <p>구독을 신청하고 신성씨앤에스에서 보내드리는<br />최신제품 소식, 매뉴얼을 메일로 받아보세요!</p>
                     </div>
                     <form name="regform2" method="post" class="subscribe_form" action="./index_ok.asp">
					 <input type="hidden" name="mode" value="subscribe">
                         <input type="text" name="s_company" placeholder="회사명" />
                         <input type="text" name="s_email" id="s_email" placeholder="이메일 주소"/><!-- 입력한 이메일 값 가져오기 -->
                         <input type="text" name="s_name" placeholder="이름" />
                         <input type="text" name="s_addr" placeholder="주소" />
                         <button type="button" class="btn_blue" onclick="subscribe_chk();return false;">구독하기</button>
                     </form>
                     <button type="button" class="btn_pop_close">닫기</button>
                 </div>
             </div>
         </div>
         <div id="A_Footer">
             <!-- #include virtual="/m/_inc/footer.asp" -->
         </div>
     </div>
     </div>
 </body>

 </html>