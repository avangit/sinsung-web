<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
mode = SQL_Injection(Trim(Request.Form("mode")))

Call DbOpen()

If mode = "online" Then

	on_type		= SQL_Injection(Trim(Request.Form("on_type")))
	isfastYN	= SQL_Injection(Trim(Request.Form("isfastYN")))
	mem_idx		= SQL_Injection(Trim(Request.Form("mem_idx")))
	on_company	= SQL_Injection(Trim(Request.Form("on_company")))
	on_name		= SQL_Injection(Trim(Request.Form("on_name")))
	on_mobile	= SQL_Injection(Trim(Request.Form("on_mobile")))
	on_email	= SQL_Injection(Trim(Request.Form("on_email")))
	on_title	= SQL_Injection(Trim(Request.Form("on_title")))
	on_content	= SQL_Injection(Trim(Request.Form("on_content")))

	msg = "문의가 등록되었습니다."

	If mem_idx <> "" Then
		isMember = "Y"
	Else
		isMember = "N"
	End If

	Sql = "INSERT INTO board_online("
	Sql = Sql & "on_type, "
	Sql = Sql & "on_cate, "
	Sql = Sql & "on_company, "
	Sql = Sql & "on_name, "
	Sql = Sql & "on_mobile, "
	Sql = Sql & "on_email, "
	Sql = Sql & "on_title, "
	Sql = Sql & "on_content, "
	Sql = Sql & "isfastYN, "
	Sql = Sql & "isMember, "
	Sql = Sql & "mem_idx) VALUES("
	Sql = Sql & "'" & on_type & "',"
	Sql = Sql & "N'빠른견적요청',"
	Sql = Sql & "N'" & on_company & "',"
	Sql = Sql & "N'" & on_name & "',"
	Sql = Sql & "'" & on_mobile & "',"
	Sql = Sql & "'" & on_email & "',"
	Sql = Sql & "N'" & on_title & "',"
	Sql = Sql & "N'" & on_content & "',"
	Sql = Sql & "'" & isfastYN & "',"
	Sql = Sql & "'" & isMember & "',"
	Sql = Sql & "'" & mem_idx & "')"

	dbconn.execute(Sql)
ElseIf mode = "subscribe" Then

	s_company	= SQL_Injection(Trim(Request.Form("s_company")))
	s_email		= SQL_Injection(Trim(Request.Form("s_email")))
	s_name		= SQL_Injection(Trim(Request.Form("s_name")))
	s_addr		= SQL_Injection(Trim(Request.Form("s_addr")))

	msg = "구독신청이 완료되었습니다."

	Sql = "INSERT INTO subscribe("
	Sql = Sql & "s_name, "
	Sql = Sql & "s_company, "
	Sql = Sql & "s_email, "
	Sql = Sql & "s_addr) VALUES("
	Sql = Sql & "'" & s_name & "',"
	Sql = Sql & "'" & s_company & "',"
	Sql = Sql & "'" & s_email & "',"
	Sql = Sql & "'" & s_addr & "')"

	dbconn.execute(Sql)
Else
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

Call DbClose()

Call jsAlertMsgUrl(msg, "/m/")
%>