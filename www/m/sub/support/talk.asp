<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">카톡 1:1 서비스</dt>
 			<dd>
 				<ul>
 					<li><a href="drivers7.asp">신성케어 7drivers</a></li>
 					<li><a href="service.asp">맞춤형 세팅 서비스</a></li>
 					<li><a href="it_data2.asp">기술자료 설치매뉴얼</a></li>
 					<li><a href="talk.asp">카톡 1:1 서비스</a></li>
 					<li><a href="onlineReceive.asp">기술지원 문의</a></li>
 					<li><a href="demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
				<h3>카톡 1:1 서비스</h3>
			</div>
			<div id="content" class="kakao">
			<a href="https://pf.kakao.com/_kxipdT" target="_blank">
				<img src="/m/images/sub/kakao.jpg" alt="카카오톡 연결하기">
			</a>
		</div>
		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>