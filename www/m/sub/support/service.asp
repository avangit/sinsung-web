 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">맞춤형 세팅 서비스</dt>
 			<dd>
 				<ul>
 					<li><a href="drivers7.asp">신성케어 7drivers</a></li>
 					<li><a href="service.asp">맞춤형 세팅 서비스</a></li>
 					<li><a href="it_data2.asp">기술자료 설치매뉴얼</a></li>
 					<li><a href="talk.asp">카톡 1:1 서비스</a></li>
 					<li><a href="onlineReceive.asp">기술지원 문의</a></li>
 					<li><a href="demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
				<h3>맞춤형 세팅 서비스</h3>
			</div>
			<div id="content" class="serviceCon">
			<dl>
				<dt>고객을 위한 맞춤형 세팅 서비스</dt>
				<dd>
					업무용  PC구입 시 하드웨어 구성과 소프트웨어 설치로 많은 시간을 소비하십니까?<br>
					이에 고객의 시간과 비용을 단축시키고자 <br> (주)신성씨앤에스는 출하 단계에서기업이 구입한
					PC를 직원들에게 배포한  즉시 별다른 설정 과정 없이 이용할 수 있는  신성 맞춤형 세팅
					서비스를 제공하고 있습니다.
				</dd>
			</dl>

			<ul>
				<li>
					<img src="/m/images/sub/img_service1.jpg" alt="">
					<p>추가 부품 장착(고객 요청시)</p>
				</li>
				<li class="right">
					<img src="/m/images/sub/img_service2.jpg" alt="">
					<p>최고수준의 대량 세팅 시스템</p>
				</li>
				<li>
					<img src="/m/images/sub/img_service3.jpg" alt="">
					<p>BIOS 설정 및 변경</p>
				</li>
				<li class="right">
					<img src="/m/images/sub/img_service4.jpg" alt="">
					<p>마스터 이미지 로딩(최첨단 장비)</p>
				</li>
				<li>
					<img src="/m/images/sub/img_service5.jpg" alt="">
					<p>고객사별 시리얼 이력 관리</p>
				</li>
				<li class="right">
					<img src="/m/images/sub/img_service6.jpg" alt="">
					<p>자산관리  QR코드</p>
				</li>
			</ul>
		</div>
		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>