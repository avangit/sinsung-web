<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$('.video_wrap ul > li').click(function() {
 				n = $('.video_wrap ul > li').index(this);
 				$('.bg').show();
 				$('.video_pop').eq(n).show();
 			});

 			$('.bg').click(function() {
 				$('.bg').hide();
 				$('.video_pop').hide();
 			});
 		});

 	</script>
 	<script type="text/javascript">
 		jQuery(function($) {
 			var layerWindow = $('.mw_layer');
 			var layer = $('#layer');


 			/* ESC Event*/
 			$(document).keydown(function(event) {
 				if (event.keyCode != 27) return true;
 				if (layerWindow.hasClass('open')) {
 					layerWindow.removeClass('open');
 				}
 				return false;
 			});

 			// Hide Window
 			layerWindow.find('>.bg').mousedown(function(event) {
 				layerWindow.removeClass('open');
 				return false;
 			});
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">기술자료 설치매뉴얼</dt>
 			<dd>
 				<ul>
 					<li><a href="drivers7.asp">신성케어 7drivers</a></li>
 					<li><a href="service.asp">맞춤형 세팅 서비스</a></li>
 					<li><a href="it_data2.asp">기술자료 설치매뉴얼</a></li>
 					<li><a href="talk.asp">카톡 1:1 서비스</a></li>
 					<li><a href="onlineReceive.asp">기술지원 문의</a></li>
 					<li><a href="demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
 				</ul>
 			</dd>
 		</dl>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim b_addtext5 	: b_addtext5 	=  SQL_Injection(Trim(Request("b_addtext5")))
Dim b_addtext6 	: b_addtext6 	=  SQL_Injection(Trim(Request("b_addtext6")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "BOARD_v1"
Dim query_where			: query_where		= " b_part = 'board08'"
Dim query_orderby		: query_orderby		= " ORDER BY option_notice DESC, b_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND b_title LIKE '%" & fieldvalue & "%' "
End If

If b_addtext5 <> "" Then
	query_where = query_where & " AND b_addtext5 = '" & b_addtext5 & "'"
End If

If b_addtext6 <> "" Then
	query_where = query_where & " AND b_addtext6 = '" & b_addtext6 & "'"
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
 				<h3>IT 자료실</h3>
 			</div>
 			<div id="content" class="videoCon case">
 				<div class="bg" style="display: none;"></div>
 				<ul id="tabMenu_s2" class="mb30">
 					<li rel="m2" onclick="location.href='it_data2.asp'">기술지원메뉴얼</li>
 					<li rel="m1" onclick="location.href='it_data.asp'">제품설명서</li>
 					<li rel="m3" class="active" onclick="location.href='it_data3.asp'">제품 <img src="/m/images/sub/youtube.png" alt="" style="position:relative; top:-1px;"> </li>
 				</ul>

 				<div class="sch it_sch clfix">
 					<form name="sch_Form" method="POST" action="/sub/support/it_data3.asp">
					<select name="b_addtext5" id="b_addtext5">
						<option value="">&nbsp; ::: 제조사 :::</option>
						<option value="HP" <% If b_addtext5 = "HP" Then %> selected<% End If %>>&nbsp; HP</option>
						<option value="삼성" <% If b_addtext5 = "삼성" Then %> selected<% End If %>>&nbsp; 삼성</option>
						<option value="삼성 단납점 모델" <% If b_addtext5 = "삼성 단납점 모델" Then %> selected<% End If %>>&nbsp; 삼성 단납점 모델</option>
						<option value="LG" <% If b_addtext5 = "LG" Then %> selected<% End If %>>&nbsp; LG</option>
						<option value="HPE" <% If b_addtext5 = "HPE" Then %> selected<% End If %>>&nbsp; HPE</option>
						<option value="NETGEAR" <% If b_addtext5 = "NETGEAR" Then %> selected<% End If %>>&nbsp; NETGEAR</option>
						<option value="ATEN" <% If b_addtext5 = "ATEN" Then %> selected<% End If %>>&nbsp; ATEN</option>
					</select>

					<select name="b_addtext6" id="b_addtext6">
						<option value="">&nbsp; ::: 카테고리 ::: </option>
						<option value="데스크탑/서버" <% If b_addtext6 = "데스크탑/서버" Then %> selected<% End If %>>데스크탑/서버</option>
						<option value="디스플레이/TV" <% If b_addtext6 = "디스플레이/TV" Then %> selected<% End If %>>디스플레이/TV</option>
						<option value="화상회의" <% If b_addtext6 = "화상회의" Then %> selected<% End If %>>화상회의</option>
						<option value="복합기/프린터" <% If b_addtext6 = "복합기/프린터" Then %> selected<% End If %>>복합기/프린터</option>
						<option value="네트워크" <% If b_addtext6 = "네트워크" Then %> selected<% End If %>>네트워크</option>
						<option value="태블릿/모바일" <% If b_addtext6 = "태블릿/모바일" Then %> selected<% End If %>>태블릿/모바일</option>
						<option value="부품/소프트웨어" <% If b_addtext6 = "부품/소프트웨어" Then %> selected<% End If %>>부품/소프트웨어</option>
						<option value="가전" <% If b_addtext6 = "가전" Then %> selected<% End If %>>가전</option>
					</select>

					<input type="text" name="fieldvalue" value="<%=fieldvalue%>" placeholder="검색어를 입력하세요.">
					<a href="javascript:;" onclick="document.sch_Form.submit();">검색</a>
 					</form>
 				</div>

 				<div class="video_wrap">
 					<ul class=" clfix" style="clear:both">
<%
If rs.bof Or rs.eof Then
%>
						<li>등록된 데이터가 없습니다.</li>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
		b_addtext1 = rs("b_addtext1")
		arr_b_addtext1 = Split(b_addtext1, " ")

		For i = 0 To Ubound(arr_b_addtext1) - 1
			If InStr(arr_b_addtext1(i), "src=") > 0 Then

				b_addtext1_content = arr_b_addtext1(i)
				Exit For
			End If
		Next

		b_addtext1_final = Replace(Right(Trim(b_addtext1_content), 12), Chr(34), "")
%>
 						<li>
 							<a href="#;?2104">
 								<h6><%=Cut(rs("b_title"),25,"...")%></h6>
 								<div class="img">
 									<div class="hover"><i>+</i></div>
 									<img src="http://i1.ytimg.com/vi/<%=b_addtext1_final%>/0.jpg" border="0">
 								</div>
 								<div class="vidoe-con"></div>
 								<div class="video_pop">
 									<div class="video_box">
 										<%=b_addtext1%>
 										<p></p>
 									</div>
 								</div>
 							</a>
 						</li>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 					</ul>

 				</div>

 				<!--페이징-->
				<%call Paging_user("")%>
 				<!--div class="page">
 					<ul>
 						<li class="btn prev"><a href="#">&#60;&#60;</a></li>
 						<li class="on"><a href="#;">1</a></li>
 						<li><a href="#">2</a></li>
 						<li class="btn next"><a href="#">&#62;&#62;</a></li>
 					</ul>
 				</div-->
 			</div>
 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>