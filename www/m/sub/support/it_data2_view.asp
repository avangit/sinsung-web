 <% @CODEPAGE="65001" language="vbscript" %>
 <!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">기술자료 설치매뉴얼</dt>
 			<dd>
 				<ul>
 					<li><a href="drivers7.asp">신성케어 7drivers</a></li>
 					<li><a href="service.asp">맞춤형 세팅 서비스</a></li>
 					<li><a href="it_data2.asp">기술자료 설치매뉴얼</a></li>
 					<li><a href="talk.asp">카톡 1:1 서비스</a></li>
 					<li><a href="onlineReceive.asp">기술지원 문의</a></li>
 					<li><a href="demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
 				</ul>
 			</dd>
 		</dl>
<%
b_idx = SQL_Injection(Trim(Request.QueryString("idx")))

Call DbOpen()

sql = "UPDATE BOARD_v1 SET b_read = b_read + 1 WHERE b_idx = " & b_idx
dbconn.execute(sql)

sql = "SELECT * FROM BOARD_v1 WHERE b_idx = " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	b_title = rs("b_title")
	b_text = rs("b_text")
	file_1 = rs("file_1")
	file_2 = rs("file_2")
	file_3 = rs("file_3")
	b_read = rs("b_read")
	b_writeday = rs("b_writeday")
End If

rs.Close
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
 				<h3>IT 자료실</h3>
 			</div>
 			<div id="content" class="videoCon case">
 				<ul id="tabMenu_s2" class="mb30">
 					<li rel="m2" class="active">기술지원메뉴얼</li>
 					<li rel="m1" onclick="location.href='it_data.asp'">제품설명서</li>
 					<li rel="m3" onclick="location.href='it_data3.asp'">제품 <img src="/m/images/sub/youtube.png" alt="" style="position:relative; top:-1px;"></li>
 				</ul>
 				<!--div class="sch it_sch clfix">
 					<form>
 						<input type="hidden" name="s_i" value="all">
 						<select name=" p2" id="part2_code">
 							<option value="">&nbsp; ::: 제조사 :::</option>
 							<option value="21">&nbsp; HP</option>
 							<option value="62">&nbsp; 삼성</option>
 							<option value="103">&nbsp; 삼성 단납점 모델</option>
 							<option value="23">&nbsp; LG</option>
 							<option value="98">&nbsp; HPE</option>
 							<option value="111">&nbsp; NETGEAR</option>
 							<option value="110">&nbsp; ATEN</option>
 						</select>
 						<select name="p1" id="part1_code">
 							<option value="">&nbsp; ::: 카테고리 ::: </option>
 							<option value="1462178498">&nbsp; 노트북</option>
 							<option value="1462178511">&nbsp; 데스크탑</option>
 							<option value="1462178524">&nbsp; 모니터</option>
 							<option value="1462178530">&nbsp; 서버</option>
 							<option value="1462178536">&nbsp; 워크스테이션</option>
 							<option value="1462178544">&nbsp; 프린터/복합기</option>
 							<option value="1462178556">&nbsp; TV/디스플레이</option>
 							<option value="1462178578">&nbsp; 케어팩 서비스</option>
 							<option value="1462178585">&nbsp; 토너</option>
 							<option value="1462178591">&nbsp; 잉크</option>
 							<option value="1475627941">&nbsp; SSD</option>
 							<option value="1476145200">&nbsp; 전산소모품</option>
 							<option value="1512361966">&nbsp; 화상회의 장비</option>
 							<option value="1532681941">&nbsp; NAS 스토리지</option>
 							<option value="1532681957">&nbsp; 백업솔루션</option>
 							<option value="1532682082">&nbsp; 화상회의 장비</option>
 							<option value="1574754886">&nbsp; 네트워크</option>
 						</select>
 						<input type="text" name="s_o" placeholder="검색어를 입력하세요.">
 						<a href="javascript:search_chk2()">검색</a>
 					</form>
 				</div-->
 				<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
 					<caption>게시판 내용</caption>
 					<colgroup>
 						<col width="12%">
 						<col width="">
 						<col width="12%">
 						<col width="15%">
 					</colgroup>
 					<thead>
 						<tr>
 							<th colspan="4" class="r_none"> <%=b_title%></th>
 						</tr>
 					</thead>
 					<tbody>
 						<tr>
 							<th>등록일</th>
 							<td class="left"><%=b_writeday%></td>
 							<th>조회수</th>
 							<td><%=b_read%></td>
 						</tr>
 						<tr>
 							<th>파일</th>
 							<td class="left" colspan="3">
								<% If file_1 <> "" Then %>
 									<img src="/m/images/common/file.gif" alt="파일"> <a href="/download.asp?fn=<%=escape(file_1)%>&ph=avanboard_v3" class="pdR20"><%=file_1%></a>
								<% End If %>
								<% If file_2 <> "" Then %>
 									<br><img src="/m/images/common/file.gif" alt="파일"> <a href="/download.asp?fn=<%=escape(file_2)%>&ph=avanboard_v3" class="pdR20"><%=file_2%></a>
								<% End If %>
								<% If file_3 <> "" Then %>
 									<br><img src="/m/images/common/file.gif" alt="파일"> <a href="/download.asp?fn=<%=escape(file_3)%>&ph=avanboard_v3" class="pdR20"><%=file_3%></a>
								<% End If %>
							</td>
 						</tr>
 						<tr>
 							<td colspan="4" class="init">
 								<div class="content-area"><%=b_text%></div>
 							</td>
 						</tr>
 					</tbody>
 				</table>

 				<!--버튼-->
 				<div class="ntb-tb-view-btn" style="width:100%">
 					<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='it_data2.asp'">
 					<div class="btnr">

 					</div>
 				</div>
 				<div class="ntb-tb-view-reply">

 					<!--이전글 시작-->
 					<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
 						<caption>게시판 이전/다음글</caption>
 						<colgroup>
 							<col width="12%">
 							<col width="">
 						</colgroup>
 						<tbody>
<%
sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board06' AND b_idx < " & b_idx & " ORDER BY b_idx DESC"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 							<tr>
 								<th width="12%">이전글</th>
 								<td width="88%" class="left font_gray">
 									<a href="./it_data2_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 								</td>
 							</tr>
<%
End If

rs.Close

sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board06' AND b_idx > " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 							<!--다음글 시작-->
 							<tr>
 								<th width="12%">다음글</th>
 								<td width="88%" class="left font_gray">
 									<a href="./it_data2_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 								</td>
 							</tr>
<%
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 						</tbody>
 					</table>
 				</div>
 			</div>
 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>