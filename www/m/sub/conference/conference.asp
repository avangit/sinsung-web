  <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" type="text/css">
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript" src="/m/js/tytabs.jquery.min.js"></script>
 	<script type="text/javascript">
 		$(document).on('ready', function() {

 			$("#tabsholderR").tytabs({
 				tabinit: "1",
 				fadespeed: "fast"
 			});

 			//리스트(ul) 자동 onoff
 			$('ul.faqUl').delegate('.jq_toggle', 'click', function() {
 				$(this).parent().siblings('.on').toggleClass('on');
 				$(this).parent().siblings().children('.jq_hide').slideUp('fast');
 				$(this).parent().toggleClass('on');
 				$(this).siblings('.jq_hide').stop('true', 'true').slideToggle('fast');
 			});


 			$(window).on("scroll", function() {
 				var winTop = $(window).scrollTop();
 				if (winTop >= 580) {
 					$(".confCon .pageTab").addClass("scroll");
 				} else {
 					$(".confCon .pageTab").removeClass("scroll");
 				}

 			});
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">화상 회의 H/W 솔루션</dt>
 			<dd>
 				<ul>
 					<li><a href="conference.asp">화상 회의 H/W 솔루션</a></li>
 					<li><a href="cf_sw.asp">화상 회의 S/W</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div class="bg_conf"></div>

 		<!-- 서브컨텐츠 -->

 		<div id="container" class="confCon" style="padding-top:0;">
 			<div class="pageTab">
 				<ul>
 					<li class="on"><a href="conference.asp">화상 회의 H/W 솔루션<img src="/m/images/sub/conf_tab_arr.jpg" alt=""></a></li>
 					<li><a href="cf_sw.asp">화상 회의 S/W</a></li>
 				</ul>
 			</div>
 			<div id="A_Container" class="confer">
 				<div class="titCof">
 					<h5>빠르고 정확한 의사결정, 업무효율과 생산성</h5>
 					<h4>비즈니스를 연결해 주는 <br>화상회의 솔루션</h4>
 					<span><img src="/m/images/sub/icon_check.jpg" alt="">쉽고 단순한 구성</span>
 					<span><img src="/m/images/sub/icon_check.jpg" alt="">경제적인 비용</span>
 					<span><img src="/m/images/sub/icon_check.jpg" alt="">모두의 화상회의</span>
 					<img src="/m/images/sub/partner_logitech.gif" alt="로지텍 골드파트너점" class="logo-img">
 					<img src="/m/images/sub/hw-img.jpg" alt="화상회의 솔루션" class="hw-img">
 					<ul>
 						<li>
 							<p><a href="../support/onlineReceive_solution.asp?c=6" style="padding-left:42px;" class="btnS">솔루션 상담요청</a></p>
 						</li>
 						<li>
 							<p class="btn-st"><a href="/m/sub/b2b/product_category.asp?c=1200000000" class=""><i class="fa fa-search"></i>화상회의 제품보기</a></p>
 						</li>
 						<li>
 							<p class="btn-st"><a href="#" target="_blank" download><i class="fa fa-download"></i>제안서 다운로드</a></p>
 						</li>
 					</ul>


 				</div>

 				<h3>회의실 규모별 화상회의 장비 구성 방안</h3>
 				<div id="tabsholderR">
 					<ul class="tabs" id="goA">
 						<li id="tab1" class="current">임원실/개인<br>(1인)</li>
 						<li id="tab2">소형 회의실<br>(2~7인)</li>
 						<li id="tab3">중형 회의실<br>(7~15인)</li>
 						<li id="tab4">대형 회의실<br>(15~21인)</li>
 						<li id="tab5">도입<br>효과</li>
 					</ul>

 					<div class="confTabCon">
 						<div id="content1" class="tabscontent" style="display: block;">
 							<section>
 								<h5>임원실/개인 특성</h5>
 								<ul>
 									<li>임원실 또는 개인 자리에서 혼자 사용하는 경우가 많으므로 장비구성이 최대한 심플해야 합니다.</li>
 									<li>때론 응접실, 소형회의테이블에서도 접속해야 되는 상황이 발생할 수 있으므로 쉽게 이동이 가능해야합니다. </li>
 								</ul>
 							</section>
 							<section>
 								<h5>구성 방안</h5>
 								<ul>
 									<li>구성이 간단하고 디자인이 고급스런 스피커폰 일체형 카메라를 이용합니다.</li>
 								</ul>
 								<div class="confBox">
 									<p><img src="/m/images/sub/img_hwroom1.jpg" alt="임원실/개인"></p>
 									<dl>
 										<dt>호환 가능한 화상회의 S/W</dt>
 										<dd><img src="/m/images/sub/img_hwroom-logo.jpg" alt="로고"></dd>
 									</dl>
 								</div>
 							</section>
 							<section>
 								<div class="cfproWrap">
 									<div class="cfimgArea">
 										<img src="/m/images/sub/pro_bcc950.jpg" alt="bcc950">

 									</div>
 									<p class="cfslo">올인원 타입의 컨퍼런스 제품</p>
 									<p class="cfprotit">BCC950</p>
 									<div class="cfproBx">
 										<ul class="cfproBx-ul">
 											<li>카메라 &amp; 스피커폰 일체형 디자인</li>
 											<li>USB 연결 방식 / CarlZeiss 렌즈탑재</li>
 											<li>Full HD 1080P / 30fps / H.264 / UVC 1.5지원</li>
 											<li>78º 시야각 / 디지털 팬 &amp; 틸트 / 1.2X 디지털 줌</li>
 											<li>잡음 및 에코 제거 기능</li>
 											<li>대부분 UC 플랫폼과 호환성 인증 획득</li>
 											<li>스피커폰 성능 범위 : 2.5m</li>
 											<li>권장 회의실 크기 : 임원실 / 1~3인 규모 회의실</li>
 										</ul>
 										<ul class="cfbtnArea">
 											<li>
 												<p class="btn-st"><a href="../b2b/product_view.asp?g_idx="><i class="fa fa-search"></i>제품 상세보기</a></p>
 											</li>
 											<li><a href="../support/onlineReceive_solution.asp?c=6" class="btnS">솔루션 상담요청</a></li>

 										</ul>

 									</div>

 								</div>
 							</section>
 						</div>


 						<div id="content2" class="tabscontent" style="display: none;">
 							<section>
 								<h5>소형 회의실 특성</h5>
 								<ul>
 									<li>2~7인 정도가 참석할 수 있는 작은 회의실입니다.</li>
 									<li>누구나 업무용 노트북을 갖고 들어가 쉽게 연결하여 사용할 수 있는 장비로 구성합니다.</li>
 									<li>카메라는 렌즈 화각이 넓고, 디스플레이 거치 또는 삼각대 연결이 가능해야 합니다.</li>

 								</ul>
 							</section>
 							<section>
 								<h5>구성 방안 ①</h5>
 								<ul>
 									<li>디스플레이 상단 또는 하단에 마운트를 이용하여 거치합니다. </li>
 									<li>카메라 촬상 각도가 가장 넓기 때문에 디스플레이와 테이블 간격이 좁은 회의실에서도 대부분의 참석자를 보여줍니다.</li>
 									<li>확장마이크를 연결하면 마이크 입력 범위를 늘릴 수 있어 최대 7인까지 회의에 참석 가능합니다.</li>

 								</ul>
 								<div class="confBox">
 									<p><img src="/m/images/sub/img_hwroom2-1.jpg" alt="소형 회의실"></p>
 									<dl>
 										<dt>호환 가능한 화상회의 S/W</dt>
 										<dd><img src="/m/images/sub/img_hwroom-logo.jpg" alt="로고"></dd>
 									</dl>
 								</div>
 							</section>
 							<section>
 								<div class="cfproWrap">
 								<div class="cfimgArea pt20">
 											<img src="/m/images/sub/pro_mic.jpg" alt="mic">
										</div>
 									<p class="cfslo">120º 시야각 및 4K 화질을 지원하는 허들룸 컨퍼런스 제품 </p>
 									<p class="cfprotit">MEETUP + Expansion Mic</p>
 									<div class="cfproBx">
 										<ul class="cfproBx-ul">
 											<li>USB 연결 방식 / H.264 / UVC 1.5 지원 / 일체형 디자인</li>
 											<li>4K UHD / 30fps (3840x2160) / 3개 사전 위치 설정 </li>
 											<li>120º 광시야각 / 50º 좌우 팬 / 30º 상하 틸트 / 5X HD 줌 </li>
 											<li>잡음 및 에코 제거 기능 / 3개의 광역 빔형 알고리즘 마이크</li>
 											<li>어쿠스틱 서스펜디드 스피커 </li>
 											<li>대부분 UC 플랫폼과 호환성 인증 획득 </li>
 											<li>스피커폰 성능 범위 <br>
 												기본형 : 2.4m / 확장형 : 4.2m </li>
 											<li>권장 회의실 크기 <br>
 												기본형 : 1~5인 규모 회의실 <br>
 												확장형 : 1~7인 규모 회의실 (Expansion Mic 추가)</li>

 										</ul>

 											<ul class="cfbtnArea">
 												<li>
 													<p class="btn-st"><a href="../b2b/product_view.asp?g_idx="><i class="fa fa-search" aria-hidden="true"></i>제품 상세보기</a></p>
 												</li>
 												<li class="ml10"><a href="../support/onlineReceive_solution.asp?c=6" class="btnS">솔루션 상담요청</a></li>
 												<li>
 													<a href="/download/Video_conference_simple_proposal.pdf" target="_blank">
 													</a></li>
 											</ul>

									</div>

 								<section class="bddash">
 									<h5>구성 방안 ②</h5>
 									<ul>
 										<li>디스플레이 상단 또는 하단에 마운트를 이용하여 거치합니다. </li>
 										<li>카메라 촬상 각도가 가장 넓기 때문에 디스플레이와 테이블 간격이 좁은 회의실에서도 대부분의 참석자를 보여줍니다.</li>
 										<li>확장마이크를 연결하면 마이크 입력 범위를 늘릴 수 있어 최대 7인까지 회의에 참석 가능합니다.</li>

 									</ul>
 									<div class="confBox">
 										<p><img src="/m/images/sub/img_hwroom2-2.jpg" alt="소형 회의실"></p>
 										<dl>
 											<dt>호환 가능한 화상회의 S/W</dt>
 											<dd><img src="/m/images/sub/img_hwroom-logo.jpg" alt="로고"></dd>
 										</dl>
 									</div>
 								</section>
								</div>

 							<section><a href="/download/Video_conference_simple_proposal.pdf" target="_blank">
 								</a>
<!--
 								<div class="cfproWrap"><a href="/download/Video_conference_simple_proposal.pdf" target="_blank">
 										<p class="cfslo">넓은 시야, Full HD 1080P를 지원하는 고급형 웹캠 </p>
 										<p class="cfprotit">C930e</p>
 									</a>
 									<div class="cfproBx"><a href="/download/Video_conference_simple_proposal.pdf" target="_blank">
 											<ul class="cfproBx-ul">
 												<li>USB 연결 방식 / CarlZeiss 렌즈 탑재 </li>
 												<li>H.264 SVC / UVC 1.5 지원 </li>
 												<li>Full HD 화상 통화 (1920x1080@30fps) </li>
 												<li>90º 시야각 </li>
 												<li>Full HD 에서 4X 디지털 줌 / 자동 초점 조절 </li>
 												<li>RightLight 2 지원 </li>
 												<li>노이즈 제거 기능의 전방향 듀얼 마이크 탑재 </li>
 												<li>대부분 UC 플랫폼과 호환성 인증 획득 </li>
 												<li>CarlZeiss® 렌즈 탑재 / 표준 삼각대 연결 가능 </li>
 												<li>영상 보안 솔루션 : 외부 프라이버시 셔터 제공</li>
 											</ul>
 										</a>
 										<div class="cfimgArea pt20"><a href="/download/Video_conference_simple_proposal.pdf" target="_blank">
 												<img src="/m/images/sub/pro_c930.jpg" alt="c930e">
 											</a>
 											<ul class="cfbtnArea"><a href="/download/Video_conference_simple_proposal.pdf" target="_blank">
 												</a>
 												<li><a href="/download/Video_conference_simple_proposal.pdf" target="_blank"></a>
 													<p class="btn-st"><a href="/download/Video_conference_simple_proposal.pdf" target="_blank"></a><a href="/sub/b2b/product_view.asp?idx=1177&amp;part_idx=40&amp;o=1&amp;title=C930E"><i class="fa fa-search" aria-hidden="true"></i>제품 상세보기</a></p>
 												</li>
 												<li class="ml10"><a href="#" class="btnS">솔루션 상담요청</a></li>
 											</ul>
 										</div>
 									</div>

 								</div>
-->

 								<div class="cfproWrap">
 								<div class="cfimgArea">
 											<img src="/m/images/sub/pro_yvc300.jpg" alt="yvc300">

 										</div>
 									<p class="cfslo">동급의 USB 버스 전원 스피커폰 중 최고의 음량 달성</p>
 									<p class="cfprotit">YVC-300</p>
 									<div class="cfproBx">
 										<ul class="cfproBx-ul">
 											<li>적응형 에코캔슬러</li>
 											<li>소음 저감</li>
 											<li>Auto gain control 기능</li>
 											<li>잔향 억제</li>
 											<li>Auto tracking 기능</li>

 										</ul>
 										<ul class="cfbtnArea">
 												<li>
 													<p class="btn-st"><a href="../b2b/product_view.asp?g_idx="><i class="fa fa-search" aria-hidden="true"></i>제품 상세보기</a></p>
 												</li>
 												<li class="ml10"><a href="../support/onlineReceive_solution.asp?c=6" class="btnS">솔루션 상담요청</a></li>
 											</ul>

 									</div>

 								</div>
 							</section>
 						</div>


 						<div id="content3" class="tabscontent" style="display: none;">
 							<section>
 								<h5>중형 회의실 특성</h5>
 								<ul>
 									<li>임직원이 언제든지 본인의 노트북을 연결하여 화상회의가 가능하도록 구성합니다.</li>
 									<li>중형회의실부터는 줌과 렌즈 회전의 속도가 빠른 광학 줌 카메라를 사용합니다.</li>
 								</ul>
 							</section>
 							<section>
 								<h5>구성 방안 ①</h5>
 								<ul>
 									<li>카메라 : 광학 20배줌과 렌즈의 상화좌우 고속회전이 가능한 HD급 카메라를 사용합니다.</li>
 									<li>마이크 : 입출력 반경 약 3m를 커버할 수 있는 스피커폰과 확장 마이크를 사용합니다.</li>
 								</ul>
 								<div class="confBox">
 									<p><img src="/m/images/sub/img_hwroom3-1.jpg" alt="중형 회의실"></p>
 									<dl>
 										<dt>호환 가능한 화상회의 S/W</dt>
 										<dd><img src="/m/images/sub/img_hwroom-logo.jpg" alt="로고"></dd>
 									</dl>
 								</div>
 							</section>
 							<section>
 								<div class="cfproWrap">
									<div class="cfimgArea pt20">
 											<img src="/m/images/sub/pro_group.jpg" alt="group">

 										</div>
 									<p class="cfslo">고품질 HD 영상, 깨끗한 음질 </p>
 									<p class="cfprotit">GROUP</p>
 									<div class="cfproBx">
 										<ul class="cfproBx-ul">
 											<li>USB 연결 방식 / CarlZeiss 렌즈 탑재 </li>
 											<li>Full HD 1080P / 30fps / H.264 / UVC 1.5 지원</li>
 											<li>90º 넓은 시야각 / 260º 좌우 팬 / 130º 상하 틸트</li>
 											<li>잡음 및 에코 제거기능 / 광대역 오디오 / 블루투스&amp;NFC연결</li>
 											<li>대부분 UC 플랫폼과 호환성 인증 획득 </li>
 											<li>스피커폰 성능 범위 <br>
 												기본형 : 6m / 확장형 : 8.5m </li>
 											<li>권장 회의실 크기 <br>
 												기본형 : 6~8인 규모 회의실 <br>
 												확장형 : 9~15인 규모 회의실 (별도 옵션 마이크 구매)</li>

 										</ul>
 										<ul class="cfbtnArea">
 												<li>
 													<p class="btn-st"><a href="../b2b/product_view.asp?g_idx="><i class="fa fa-search" aria-hidden="true"></i>제품 상세보기</a></p>
 												</li>
 												<li class="ml10"><a href="../support/onlineReceive_solution.asp?c=6" class="btnS">솔루션 상담요청</a></li>
 											</ul>

 									</div>

 								</div>
 							</section>

 							<section class="bddash">
 								<h5>구성 방안 ②</h5>
 								<ul>
 									<li>카메라 : 광학 20배줌과 렌즈의 상화좌우 고속회전이 가능한 HD급 카메라를 사용합니다.</li>
 									<li>마이크 : 스피커는 디스플레이 바로 앞에 두고, 마이크는 테이블에 일정한 간격으로 2~4개를 배치합니다</li>

 								</ul>
 								<div class="confBox">
 									<p><img src="/m/images/sub/img_hwroom3-2.jpg" alt="중형 회의실"></p>
 									<dl>
 										<dt>호환 가능한 화상회의 S/W</dt>
 										<dd><img src="/m/images/sub/img_hwroom-logo.jpg" alt="로고"></dd>
 									</dl>
 								</div>
 							</section>

 							<section>
 								<div class="cfproWrap">
 								<div class="cfimgArea pt20">
 											<img src="/m/images/sub/pro_ptz.jpg" alt="ptz">

 										</div>
 									<p class="cfslo">화상 회의 전용, 간편한 USB 연결 방식</p>
 									<p class="cfprotit">PTZ Pro 2</p>
 									<div class="cfproBx">
 										<ul class="cfproBx-ul">
 											<li>USB 연결 방식 / 프리미엄 렌즈 탑재 </li>
 											<li>Full HD 1080P / 30fps / H.264 / UVC 1.5 지원 </li>
 											<li>3개 사전 위치 설정 </li>
 											<li>90º 넓은 시야각 / 260º 좌우 팬 / 130º 상하 틸트</li>
 											<li>리모컨 컨트롤 </li>
 											<li>대부분 UC 플랫폼과 호환성 인증 획득 </li>
 											<li>오디오 장비가 이미 구축 되어 있어 카메라만 필요한 회의실</li>
 										</ul>
 										<ul class="cfbtnArea">
 												<li>
 													<p class="btn-st"><a href="../b2b/product_view.asp?g_idx="><i class="fa fa-search" aria-hidden="true"></i>제품 상세보기</a></p>
 												</li>
 												<li class="ml10"><a href="../support/onlineReceive_solution.asp?c=6" class="btnS">솔루션 상담요청</a></li>
 											</ul>

 									</div>

 								</div>

 								<div class="cfproWrap mt50">
 								<div class="cfimgArea pt20">
 											<img src="/m/images/sub/pro_yvc1000.jpg" alt="yvc1000">

 										</div>
 									<p class="cfslo">PC, 스마트폰, 하드웨어 화상회의 시스템에 자유롭게 연결 </p>
 									<p class="cfprotit">YVC-1000</p>
 									<div class="cfproBx">
 										<ul class="cfproBx-ul">
 											<li>NFC 지원 / 간단한 블루투스 연결</li>
 											<li>적응형 에코캔슬러 적용</li>
 											<li>회의실 공간 특성에 최적화 시키는 자동 튜닝 기능</li>
 											<li>스피커 : 최대 볼륨 95dB</li>
 											<li>마이크 : 반경 약 2m 커버(권장 3m)</li>

 										</ul>
 										<ul class="cfbtnArea">
 												<li>
 													<p class="btn-st"><a href="../b2b/product_view.asp?g_idx="><i class="fa fa-search" aria-hidden="true"></i>제품 상세보기</a></p>
 												</li>
 												<li class="ml10"><a href="../support/onlineReceive_solution.asp?c=10" class="btnS">솔루션 상담요청</a></li>
 											</ul>

 									</div>

 								</div>
 							</section>
 						</div>


 						<div id="content4" class="tabscontent" style="display: none;">
 							<section>
 								<h5>대형 회의실 특성</h5>
 								<ul>
 									<li>대형 회의실은 중앙이 비어있거나 넓기 때문에 마이크 4개로 구성합니다. </li>
 									<li>테이블 형태에 따라서 ‘ㄷ’자 또는 1자 형태로 배치해 줍니다.</li>

 								</ul>
 							</section>

 							<section>
 								<h5>구성 방안</h5>
 								<ul>
 									<li>카메라 : 광학 20배줌과 렌즈의 상화좌우 고속회전이 가능한 USB 타입의 HD급 카메라를 사용합니다.</li>
 									<li>마이크 : 입출력 반경 약 3m를 커버할 수 있는 스피커폰과 확장 마이크를 사용합니다. 보통 4개로 구성을 합니다.</li>

 								</ul>
 								<div class="confBox">
 									<p><img src="/m/images/sub/img_hwroom4.jpg" alt="대형 회의실"></p>
 									<dl>
 										<dt>호환 가능한 화상회의 S/W</dt>
 										<dd><img src="/m/images/sub/img_hwroom-logo.jpg" alt="로고"></dd>
 									</dl>
 								</div>
 							</section>

 							<section>
 								<div class="cfproWrap">
 								<div class="cfimgArea pt20">
 											<img src="/m/images/sub/pro_ptz.jpg" alt="ptz">

 										</div>
 									<p class="cfslo">화상 회의 전용, 간편한 USB 연결 방식</p>
 									<p class="cfprotit">PTZ Pro 2</p>
 									<div class="cfproBx">
 										<ul class="cfproBx-ul">
 											<li>USB 연결 방식 / 프리미엄 렌즈 탑재 </li>
 											<li>Full HD 1080P / 30fps / H.264 / UVC 1.5 지원 </li>
 											<li>3개 사전 위치 설정 </li>
 											<li>90º 넓은 시야각 / 260º 좌우 팬 / 130º 상하 틸트</li>
 											<li>리모컨 컨트롤 </li>
 											<li>대부분 UC 플랫폼과 호환성 인증 획득 </li>
 											<li>오디오 장비가 이미 구축 되어 있어 카메라만 필요한 회의실</li>
 										</ul>
 										<ul class="cfbtnArea">
 												<li>
 													<p class="btn-st"><a href="../b2b/product_view.asp?g_idx="><i class="fa fa-search" aria-hidden="true"></i>제품 상세보기</a></p>
 												</li>
 												<li class="ml10"><a href="../support/onlineReceive_solution.asp?c=6" class="btnS">솔루션 상담요청</a></li>
 											</ul>

 									</div>

 								</div>

 								<div class="cfproWrap mt50">
 								<div class="cfimgArea pt20">
 											<img src="/m/images/sub/pro_yvc1000.jpg" alt="yvc1000">

 										</div>
 									<p class="cfslo">PC, 스마트폰, 하드웨어 화상회의 시스템에 자유롭게 연결 </p>
 									<p class="cfprotit">YVC-1000</p>
 									<div class="cfproBx">
 										<ul class="cfproBx-ul">
 											<li>NFC 지원 / 간단한 블루투스 연결</li>
 											<li>적응형 에코캔슬러 적용</li>
 											<li>회의실 공간 특성에 최적화 시키는 자동 튜닝 기능</li>
 											<li>스피커 : 최대 볼륨 95dB</li>
 											<li>마이크 : 반경 약 2m 커버(권장 3m)</li>

 										</ul>
 										<ul class="cfbtnArea">
 												<li>
 													<p class="btn-st"><a href="../b2b/product_view.asp?g_idx="><i class="fa fa-search" aria-hidden="true"></i>제품 상세보기</a></p>
 												</li>
 												<li class="ml10"><a href="../support/onlineReceive_solution.asp?c=6" class="btnS">솔루션 상담요청</a></li>
 											</ul>

 									</div>

 								</div>
 							</section>
 						</div>


 						<div id="content5" class="tabscontent" style="display: none;">
 							<ul class="hw_effBx">
 								<li>
 									<p><img src="/m/images/sub/hw_eff1.jpg" alt=""></p>
 									<dl>
 										<dt>출장 경비를 줄일 수 있습니다.</dt>
 										<dd class="cont_btxt">항공편과 호텔 및 다른 경비 등 회의에 관련된 여행경비에 돈을 쓰는 대신에,
 											간단하게 화상회의를 하면 됩니다. <br>추가비용까지 들여가면서 회사 직원들을
 											멀리까지 보낼 필요는 없습니다.</dd>
 									</dl>
 								</li>
 								<li>
 									<p><img src="/m/images/sub/hw_eff2.jpg" alt=""></p>
 									<dl>
 										<dt>시간을 절약해 줍니다.</dt>
 										<dd class="cont_btxt">매일같이 바쁜 비즈니스맨들에게 불필요한 이동시간을 줄여줍니다.  <br>
 											더 이상 회의를 진행 후 다시 회사로 돌아가는데 소모되는 시간에 대해 걱정할
 											필요가 없습니다. <br>시간을 획기적으로 줄여서 업무의 효율성을 증가시킬 수 있습니다.</dd>
 									</dl>
 								</li>
 								<li>
 									<p><img src="/m/images/sub/hw_eff3.jpg" alt=""></p>
 									<dl>
 										<dt>합리적인 가격</dt>
 										<dd class="cont_btxt">회의실 구조와 참석자 수에 맞춰 자유롭게 장비구성을 할 수 있어 꼭 고가의
 											장비를 구매하지 않더라도 최적의 환경에서 화상회의를 진행할 수 있습니다.</dd>
 									</dl>
 								</li>
 								<li>
 									<p><img src="/m/images/sub/hw_eff4.jpg" alt=""></p>
 									<dl>
 										<dt>페이퍼리스</dt>
 										<dd class="cont_btxt">매 회의 때 마다 자료를 출력할 필요가 없습니다. 화상회의 솔루션 내에서
 											문서공유가 가능하기 때문에 회의 자료 출력 시간을 단축하고 더 이상 종이를
 											낭비하지 않아도 됩니다.</dd>
 									</dl>
 								</li>
 							</ul>
 						</div>

 					</div>
 				</div>


 				<div id="faqCon">
 					<h3 id="goB">자주 묻는 질문<br>
 					<p>화상회의 솔루션에 관련해 궁금하신 점이 있으신가요? <br> FAQ에서 먼저 문제해결에 대한 도움을 받을 수 있습니다.</p></h3>
 					<ul class="btn">
<!-- 						<li><a href="#" target="_blank">FAQ 다운로드</a></li>-->
 					</ul>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " b_title, b_text "
Dim query_Tablename		: query_Tablename	= "BOARD_v1"
Dim query_where			: query_where		= " b_part = 'board10' "
Dim query_orderby		: query_orderby		= " ORDER BY b_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
 					<!-- 목록 -->
 					<ul class="faqUl">
<%
If Not rs.eof Then
	Do while Not rs.eof
%>
 						<li>
 							<label class="jq_toggle tit">
 								<span class="tit">Q.</span>
 								<span class="txt"><%=rs("b_title")%></span>
 							</label>
 							<div class="jq_hide">
 								<span class="tit">A.</span>
 								<span class="txt"><%=rs("b_text")%></span></div>
 						</li>
<%
		rs.movenext
	Loop
End If
%>
 					</ul>
 					<!-- 목록 -->

 					<!--버튼-->
 					<div class="ntb-listbtn-area mgT10">
 					</div>
 					<!--검색폼-->
 					<div class="ntb-search-area">
 						<form name="search_form" action="./conference.asp" method="post">
 							<select name="fieldname" id="fieldname" class="AXSelect vmiddle">
 								<option value="b_title" <% If fieldname = "" Or fieldname = "b_title" Then %> selected<% End If %>>제목</option>
 								<option value="b_text" <% If fieldname = "b_text" Then %> selected<% End If %>>내용</option>
 							</select>
 							<input type="text" name="fieldvalue" value="<%=fieldvalue%>" class="AXInput vmiddle">
 							<input type="submit" value="검색" class="AXButton">
 						</form>
 					</div>

					<% Call Paging_user("") %>
 					<!--div class="page">
 						<ul class="clfix">
 							<li><a href="/sub/conference/conference.asp?startPage=0&amp;code=faq">&lt;&lt;</a></li>
 							<li class="on"><a href="#">1</a></li>
 							<li><a href="/sub/conference/conference.asp?startPage=0&amp;code=faq">&gt;&gt;</a></li>
 						</ul>
 					</div-->


 				</div>
 			</div>
				</div>
 			<div id="A_Footer">
 				<!-- #include virtual="/m/_inc/footer.asp" -->
 			</div>

 		</div>
 	</div>
 </body>

 </html>