   <% @CODEPAGE="65001" language="vbscript" %>
 <!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
     <!-- #include virtual="/m/_inc/head.asp" -->
     <!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
     <link rel="stylesheet" href="/m/_css/style.css" type="text/css">
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script src="/m/js/member.js" type="text/javascript"></script>
     <!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

	<script>
		$(document).on('ready', function() {
			// 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function() {
				if (window.FileReader) {
					var filename = $(this)[0].files[0].name;
				} else {
					var filename = $(this).val().split('/').pop().split('\\').pop();
				}
				$(this).siblings('.upload_name').val(filename);
			});

		});

		function openDaumPostcode() {new daum.Postcode({
				oncomplete: function(data) {
					// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

					// 각 주소의 노출 규칙에 따라 주소를 조합한다.
					// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
					var fullAddr = ''; // 최종 주소 변수
					var extraAddr = ''; // 조합형 주소 변수

					// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
					if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
						fullAddr = data.roadAddress;

					} else { // 사용자가 지번 주소를 선택했을 경우(J)
						fullAddr = data.jibunAddress;
					}

					// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
					if(data.userSelectedType === 'R'){
						//법정동명이 있을 경우 추가한다.
						if(data.bname !== ''){
							extraAddr += data.bname;
						}
						// 건물명이 있을 경우 추가한다.
						if(data.buildingName !== ''){
							extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
						}
						// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
						fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
					}

					// 우편번호와 주소 정보를 해당 필드에 넣는다.
					document.getElementById('cZip').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('cAddr1').value = fullAddr;

					// 커서를 상세주소 필드로 이동한다.
					document.getElementById('cAddr2').focus();
				}
			}).open();
		}
 	</script>
 </head>

 <body>
	 <div id="A_Wrap">
		 <div id="A_Header" class="active">
			 <!-- #include virtual="/m/_inc/header.asp" -->
		 </div>
		 <!-- 서브 네비게이션 -->

		 <ul class="join_sub_menu">
			 <li><a href="login.asp">로그인</a></li>
			 <li class="on"><a href="join.asp">회원가입</a></li>
		 </ul>


		 <div id="A_Container">
			 <!-- 서브컨텐츠 -->
			 <div id="A_Container_C">
				 <!-- 서브 타이틀 -->
				 <div class="title">
					 <h3>회원가입</h3>
				 </div>
				 <div id="content" class="join2">
					 <!-- join Start -->
					 <!--회원 테이블-->
					 <form name="joinform" method="post" action="./join_ok.asp" enctype="multipart/form-data">
						 <input type="hidden" name="num" value="">
						 <table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
							 <caption>회원 가입/정보</caption>
							 <colgroup>
								 <col width="25%">
								 <col width="75%">
							 </colgroup>
							 <thead>
								 <tr>
									 <th>회사명 *</th>
									 <td class="left"><input type="text" name="cName" class="AXInput W300" maxlength="80"></td>
								 </tr>
								 <tr>
									 <th>사업자 번호 *</th>
									 <td class="left"><input type="text" name="cNum" class="AXInput W300" maxlength="12"></td>
								 </tr>
								 <tr>
									 <th align="center">아이디 *</th>
									 <td class="left">
										 <input type="text" name="strId" class="AXInput W190" style="ime-mode:ime-mode" maxlength="30">
										 <input type="button" onclick="id_check()" value="중복체크" class="AXButtonSmall Classic">
										 <br><span class="add_tx" id="id_ck" style="padding-left:0px; margin-left:0px;"></span>
									 </td>
								 </tr>
							 </thead>
							 <tbody>

								 <tr>
									 <th>비밀번호 *</th>
									 <td class="left"><input type="password" name="strPwd" class="AXInput W300" style="ime-mode:disabled" maxlength="30" placeholder="8~20자,  영문, 숫자, 특수문자 사용">
									 </td>
								 </tr>
								 <tr>
									 <th>비밀번호 확인 *</th>
									 <td class="left"><input type="password" name="strPwdRe" class="AXInput W300" style="ime-mode:disabled" maxlength="30"></td>
								 </tr>
								 <tr>
									 <th>이름 *</th>
									 <td class="left"><input type="text" name="strName" class="AXInput W300" maxlength="30"></td>
								 </tr>
								 <tr>
									 <th>전화번호 </th>
									 <td class="left">
										 <input type="text" name="strPhone1" maxlength="4" class="AXInput W110 taC" style="ime-mode:disabled"> -
										 <input type="text" name="strPhone2" maxlength="4" class="AXInput W110 taC" style="ime-mode:disabled"> -
										 <input type="text" name="strPhone3" maxlength="4" class="AXInput W110 taC" style="ime-mode:disabled">

									 </td>
								 </tr>
								 <tr>
									 <th>휴대폰 번호 * </th>
									 <td class="left">
										 <select name="strMobile1" id="strMobile1" title="앞3자리" class="AXSelect W110">
											 <option value="010">010</option>
											 <option value="011">011</option>
											 <option value="016">016</option>
											 <option value="017">017</option>
											 <option value="018">018</option>
											 <option value="019">019</option>
										 </select> -
										 <input type="text" name="strMobile2" maxlength="4" class="AXInput W110 taC" style="ime-mode:disabled"> -
										 <input type="text" name="strMobile3" maxlength="4" class="AXInput W110 taC" style="ime-mode:disabled"> <br>
										  <input type="checkbox" name="smsYN" id="smsYN" value="Y" checked> <label for="smsYN">SMS 수신동의</label>
									 </td>
								 </tr>
								 <tr>
									 <th>이메일 주소 *</th>
									 <td class="left">

										 <input type="text" name="strEmail1" class="AXInput w20p"> @
										 <input type="text" name="strEmail2" class="AXInput w20p">

										 <label>
											 <select name="strEmail3" id="strEmail3" class="AXSelect" onchange="ChangeEmail();">
												 <option value="0">직접입력</option>
												 <option value="dreamwiz.co.kr">dreamwiz.co.kr</option>
												 <option value="empal.com">empal.com</option>
												 <option value="gmail.com">gmail.com</option>
												 <option value="lycos.co.kr">lycos.co.kr</option>
												 <option value="naver.com">naver.com</option>
												 <option value="nate.com">nate.com</option>
												 <option value="netsgo.com">netsgo.com</option>
												 <option value="hanmail.net">hanmail.net</option>
												 <option value="hotmail.com">hotmail.com</option>
												 <option value="paran.com">paran.com</option>
												 <option value="yahoo.co.kr">yahoo.co.kr</option>
											 </select>
										 </label><br>
										 <input type="checkbox" name="mailYN" id="mailYN" value="Y" checked> <label for="mailYN">메일 수신동의</label>


									 </td>
								 </tr>

								 <tr>
									 <th>사업장 주소 <br>(도로명 주소) *</th>
									 <td class="left" style="height:86px">
										 <p class="pd3-0">
											 <input type="text" name="cZip" id="cZip" class="AXInput W150" readonly>
											 <input type="button" value="우편번호검색" class="AXButtonSmall Classic" onclick="openDaumPostcode(2);">
										 </p>
										 <p class="pd3-0"><input type="text" name="cAddr1" id="cAddr1" class="AXInput" readonly style="width:100%"></p>
										 <p class="pd3-0"><input type="text" name="cAddr2" id="cAddr2" class="AXInput" style="width:100%"></p>
									 </td>
								 </tr>
								 <tr>
									 <th>사업자등록증 사본</th>
									 <td class="left"><div class="file_box">
											<label for="strFile1">파일첨부</label>
											<input type="file" id="strFile1" name="strFile1" class="upload_hidden">
											<input class="upload_name" disabled="disabled">
										</div></td>
								 </tr>
							 </tbody>
						 </table>

						 <div class="ntb-listbtn-area" style="width:100%">

							 <input type="button" value="   취소   " class="AXButton" onclick="history.back();">
							   <input type="button" value="   회원가입   " class="AXButton Classic" onclick="join_chk();">

						 </div>

					 </form> <!-- //join Start -->

				 </div>
			 </div>

			 </div>
			 <div id="A_Footer">
				 <!-- #include virtual="/m/_inc/footer.asp" -->
			 </div>

		 </div>
	 </div>
 </body>

 </html>