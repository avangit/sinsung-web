<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
'// 업로드 객체 생성 및 설정
Set UploadForm = Server.Createobject("DEXT.FileUpload")
UploadForm.CodePage = 65001
UploadForm.AutoMakeFolder = True
UploadForm.DefaultPath = Server.MapPath("/upload/member/")
UploadForm.MaxFileLen = int(1024 * 1024 * 10)

intSeq = SQL_Injection(Trim(UploadForm("idx")))
strPwd = SQL_Injection(Trim(UploadForm("strPwd")))
strEmail1 = SQL_Injection(Trim(UploadForm("strEmail1")))
strEmail2 = SQL_Injection(Trim(UploadForm("strEmail2")))
strPhone1 = SQL_Injection(Trim(UploadForm("strPhone1")))
strPhone2 = SQL_Injection(Trim(UploadForm("strPhone2")))
strPhone3 = SQL_Injection(Trim(UploadForm("strPhone3")))
strMobile1 = SQL_Injection(Trim(UploadForm("strMobile1")))
strMobile2 = SQL_Injection(Trim(UploadForm("strMobile2")))
strMobile3 = SQL_Injection(Trim(UploadForm("strMobile3")))
cZip = SQL_Injection(Trim(UploadForm("cZip")))
cAddr1 = SQL_Injection(Trim(UploadForm("cAddr1")))
cAddr2 = SQL_Injection(Trim(UploadForm("cAddr2")))
mailYN = SQL_Injection(Trim(UploadForm("mailYN")))
smsYN = SQL_Injection(Trim(UploadForm("smsYN")))

'// 변수 가공
strPhone = strPhone1 & "-" & strPhone2 & "-" & strPhone3
strMobile = strMobile1 & "-" & strMobile2 & "-" & strMobile3
strEmail = strEmail1 & "@" & strEmail2

Call DbOpen()

strSQL = "SELECT strFile1 FROM mtb_member2 WHERE intseq = '" & intseq & "'"

Set rs = dbconn.execute(strSQL)

ReDim mFile(0), rsFile(0)

If Not(rs.eof) Then
	rsFile(0) = rs("strFile1")
End If

rs.close
Set rs = Nothing

If UploadForm("strFile").FileName <> "" Then
	'// 업로드 가능 여부 체크
	If UploadFileChk(UploadForm("strFile").FileName)=False Then
		Response.Write "<script language='javascript'>alert('등록할 수 없는 파일형식입니다.');history.back();</script>"
		Set UploadForm = Nothing
		Response.End
	End If

	'// 파일 용량 체크
	If UploadForm("strFile").FileLen > UploadForm.MaxFileLen Then
		Response.Write "<script language='javascript'>alert('10MB 이상의 파일은 업로드하실 수 없습니다.');history.back();</script>"
		Set UploadForm = Nothing
		Response.End
	End If

	'기존파일 삭제
	If rsFile(0) <> "" Then
		UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile(0)
	End If

	'// 파일 저장
	mFile(0) = UploadForm("strFile").Save(, False)
	mFile(0) = UploadForm("strFile").LastSavedFileName

	Sql = "UPDATE mtb_member2 SET strFile1 = N'" & SQL_Injection(mFile(0)) & "' WHERE intseq='" & intseq & "'"
	dbconn.execute(Sql)

'// 새로운 파일 업로드 없이 기존 파일 삭제만 선택한 경우
ElseIf UploadForm("strFile1").FileName = "" And UploadForm("ori_file1_del1") = "Y" Then
	'기존파일 삭제
	If rsFile(0) <> "" Then
		UploadForm.Deletefile UploadForm.DefaultPath & "\" & rsFile(0)
	End If

	Sql = "UPDATE mtb_member2 SET strFile1 ='' WHERE intseq='" & intseq & "'"
	dbconn.execute(Sql)
End If

Sql = "UPDATE mtb_member2 SET "
If strPwd <> "" Then
	Sql = Sql & "strPwd =  '" & Encrypt_Sha(strPwd) & "',"
End If
Sql = Sql & "strPhone = '" & strPhone & "', "
Sql = Sql & "strMobile = '" & strMobile & "', "
Sql = Sql & "smsYN = '" & smsYN & "', "
Sql = Sql & "strEmail = N'" & strEmail & "', "
Sql = Sql & "mailYN = '" & mailYN & "', "
Sql = Sql & "cZip = '" & cZip & "', "
Sql = Sql & "cAddr1 = N'" & cAddr1 & "', "
Sql = Sql & "cAddr2 = N'" & cAddr2 & "', "
Sql = Sql & " WHERE intseq = '" & intseq & "'"

dbconn.execute(Sql)

Call DbClose()

Call jsAlertMsgUrl("회원정보가 수정되었습니다.", "modify.asp")
%>