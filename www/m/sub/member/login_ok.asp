<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->

<%
uid			= SQL_Injection(Trim(Request.Form("uid")))
upass		= SQL_Injection(Trim(Request.Form("upass")))
returnUrl	= SQL_Injection(Trim(Request.Form("returnUrl")))

Call DbOpen()

sql = "SELECT intSeq, intGubun, strId, strName, strEmail, cName FROM mtb_member2 WHERE strId = '"&uid&"' AND strPwd = '"&Encrypt_Sha(upass)&"' AND intaction = 0"
Set rs = dbconn.execute(sql)

If Not(rs.bof Or rs.eof) Then
	Response.Cookies("U_IDX")		= rs("intSeq")
	Response.Cookies("U_ID")		= rs("strId")
	Response.Cookies("U_NAME")		= rs("strName")
	Response.Cookies("U_EMAIL")		= rs("strEmail")
	Response.Cookies("U_COMPANY")	= rs("cName")
	Response.Cookies("U_LEVEL")		= rs("intGubun")

	'로그인 정보 업데이트
	Sql = "UPDATE mtb_member2 SET login_date = '" & FormatDateTime(Now(), 2) & " " & FormatDateTime(Now(), 4) & "', login_cnt = ISNULL(login_cnt, 0) + 1, login_ip = '" & Request.ServerVariables("REMOTE_ADDR") & "' WHERE strId='" & uid & "'"
	dbconn.Execute Sql


	rturl = "http://" & Request.ServerVariables("SERVER_NAME") & "/m/" & returnUrl

	Response.Redirect rturl
Else
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>