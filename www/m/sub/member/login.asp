  <% @CODEPAGE="65001" language="vbscript" %>
 <!--#include virtual = "/Avanplus/_Function.asp"-->
 <%
 If Request.Cookies("U_IDX") <> "" Then
	Call jsAlertMsgBack("이미 로그인 중입니다.")
 End If
 %>
 <!DOCTYPE html>
 <html lang="ko">

 <head>
     <!-- #include virtual="/m/_inc/head.asp" -->
     <!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
     <link rel="stylesheet" href="/m/_css/style.css" type="text/css">
	 <script src="/m/js/member.js"></script>
     <!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
     <div id="A_Wrap">
         <div id="A_Header" class="active">
             <!-- #include virtual="/m/_inc/header.asp" -->
         </div>
         <!-- 서브 네비게이션 -->

         <ul class="join_sub_menu">
             <li class="on"><a href="./login.asp">로그인</a></li>
             <li><a href="./join.asp">회원가입</a></li>
         </ul>

<%
returnUrl	= SQL_Injection(Trim(Request.Form("returnUrl")))
%>
      <div id="A_Container" class="login_wrap">
		<!-- 서브컨텐츠 -->
		<div class="login_box">
			<div class="tit_box">
				<h3>로그인</h3>
				<p>신성씨앤에스 계정으로 로그인하기</p>
			</div>
			<form name="login" method="post" action="./login_ok.asp">
			<input type="hidden" name="returnUrl" value="<%=returnUrl%>">
				<input type="text" name="uid" placeholder="아이디" />
				<input type="password" name="upass" placeholder="비밀번호" />
				<button type="button" class="btn_blue" onclick="login_chk();">로그인</button>
			</form>
			<ul class="find_links">
				<li><a href="./find_idpw.asp">아이디 찾기</a></li>
				<li><a href="./find_idpw.asp">비밀번호 찾기</a></li>
			</ul>
		</div>
	</div>
             <div id="A_Footer">
                 <!-- #include virtual="/m/_inc/footer.asp" -->
             </div>

         </div>
     </div>
 </body>

 </html>