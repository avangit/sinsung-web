 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">프로모션</dt>
 			<dd>
 				<ul>
 					<li><a href="estimate_list.asp">맞춤사양견적</a></li>
 					<li><a href="onlineReceive2.asp">일반견적요청</a></li>
 					<li><a href="onlineReceive_solution.asp">솔루션상담요청</a></li>
 					<li><a href="promotion.asp">프로모션</a></li>
 				</ul>
 			</dd>
 		</dl>
<%
b_idx = SQL_Injection(Trim(Request.QueryString("idx")))

Call DbOpen()

sql = "UPDATE BOARD_v1 SET b_read = b_read + 1 WHERE b_idx = " & b_idx
dbconn.execute(sql)

sql = "SELECT * FROM BOARD_v1 WHERE b_idx = " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	b_title = rs("b_title")
	b_text = rs("b_text")
	file_1 = rs("file_1")
	file_2 = rs("file_2")
	file_3 = rs("file_3")
	b_read = rs("b_read")
	b_writeday = rs("b_writeday")
	sdate = rs("b_addtext3")
	edate = rs("b_addtext4")
End If

rs.Close
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
			<div class="title">
 					<h3>프로모션</h3>
 				</div>

 				<div id="content">
 					<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
 						<caption>게시판 내용</caption>
 						<colgroup>
 							<col width="114px">
 							<col width="">
 							<col width="114px">
 							<col width="114px">
 						</colgroup>
 						<thead>
 							<tr>
 								<th colspan="4" class="r_none"> <%=b_title%></th>
 							</tr>
 						</thead>
 						<tbody>
 							<tr>
 								<th>등록일</th>
 								<td class="left"><%=b_writeday%></td>
 								<th>조회수</th>
 								<td><%=b_writeday%></td>
 							</tr>
 							<tr>
 								<th>이벤트 기간</th>
 								<td class="left" colspan="3"><%=sdate%> ~ <%=edate%></td>
 							</tr>

 							<tr>
 								<td colspan="4" class="init">
 									<div class="content-area">
 										<p align="center" style="text-align: center;">
										<% If file_1 <> "" Then %>
											<img title="<%=file_1%>" src="/upload/avanboard_v3/<%=file_1%>">&nbsp;
										<% End If %>
										<% If file_2 <> "" Then %>
											<img title="<%=file_2%>" src="/upload/avanboard_v3/<%=file_2%>">&nbsp;
										<% End If %>
										<% If file_3 <> "" Then %>
											<img title="<%=file_3%>" src="/upload/avanboard_v3/<%=file_3%>">&nbsp;
										<% End If %>
											<%=b_text%>
										</p>
 									</div>

 								</td>
 							</tr>
 						</tbody>
 					</table>
 					<div class="ntb-tb-view-btn" style="width:100%">
 						<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='./promotion.asp'">
 						<div class="btnr">
 						</div>
 					</div>
 					<div class="ntb-tb-view-reply">

 						<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
 							<caption>게시판 이전/다음글</caption>
 							<colgroup>
 								<col width="12%">
 								<col width="">
 							</colgroup>
 							<tbody>
<%
sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board05' AND b_idx < " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 								<tr>
 									<th width="12%">이전글</th>
 									<td width="88%" class="left font_gray">
 										<a href="./promotion_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 									</td>
 								</tr>
<%
End If

rs.Close

sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board05' AND b_idx > " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 								<tr>
 									<th width="12%">다음글</th>
 									<td width="88%" class="left font_gray">
 										<a href="./promotion_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 									</td>
 								</tr>
<%
End If

rs.Close
Set rs = Nothing
%>
 							</tbody>
 						</table>
 					</div>
 				</div>

 			</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>