<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script type="text/javascript" src="/m/js/tytabs.jquery.min.js"></script>
 	<script type="text/javascript">
 		$(document).on('ready', function() {
 			$(".sub_lnb li:nth-child(5)").addClass('on');
 		});
 		$(function() {
 			$("#tabsholderR02").tytabs({
 				tabinit: "1",
 				fadespeed: "fast"
 			});
 		});

 		$(function() {
 			//리스트(ul) 자동 onoff
 			$('ul.faqUl').delegate('.jq_toggle', 'click', function() {
 				$(this).parent().siblings('.on').toggleClass('on');
 				$(this).parent().siblings().children('.jq_hide').slideUp('fast');
 				$(this).parent().toggleClass('on');
 				$(this).siblings('.jq_hide').stop('true', 'true').slideToggle('fast');
 			});
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">협력사</dt>
 			<dd>
 				<ul>
 					<li><a href="business.asp">사업소개</a></li>
 					<li><a href="it.asp">IT 구매제안</a></li>
 					<li><a href="srm.asp">신성SRM</a></li>
 					<li><a href="construction_case.asp">구축사례</a></li>
 					<li><a href="cooperative.asp">협력사</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
				<div class="title">
 					<h3>협력사</h3>
 				</div>
 				<div id="content">
 					<div id="tabsholderR02">
 						<ul class="tabs">
 							<li id="tab1" class="current">HP</li>
 							<li id="tab2">SAMSUNG</li>
 							<li id="tab3">LG</li>
 							<li id="tab4">협력사</li>
 						</ul>
 						<div>
 							<div id="content1" class="tabscontent cooperCon" style="display: block;">
 								<div class="coop1">
 									<h4>HP Inc.<br>Printing and Personal Systems</h4>
 								</div>
 								<dl>
 									<dt>Designed to inspire you</dt>
 									<dd>Sophisticated design meets exceptional performance in our new lineup of premium desktops and notebooks.</dd>
 									<dt>Unstoppable innovation</dt>
 									<dd>Every day, HPI engineers work to create technology that makes life better for everyone, everywhere. Innovation isn’t just about how you use<br>technology, it’s also about the business model, the way you source and create your products, and how you brand and market them.<br>We’re innovating around all three.</dd>
 								</dl>
 								<ul>
 									<li>
 										<img src="/m/images/sub/img_cp11.jpg">
 										<h5>IPG</h5>
 										<p>귀사의 이미징 및 인쇄환경에서 개선된 장비 관리부터 더 나은 워크플로우 설계에 이르는 비용을 절감할 수 있는 기회가 있습니다. <br>HP는 조직에서 전체 인쇄 비용의 최대 30%까지 절감할 수 있도록 지원하고 있습니다.</p>
 									</li>
 									<li>
 										<img src="/m/images/sub/img_cp12.jpg">
 										<h5>PSG</h5>
 										<p>HPI 상품, 서비스, 솔루션을 혁신하여 편리함과 발전을 선도해갑니다.</p>
 										<p class="dot">노트북, 컴퓨터, 모니터, 태블릿, 워크스테이션, 씬 클라이언트</p>
 									</li>
 									<li>
 										<img src="/m/images/sub/img_cp13.jpg">
 										<h5>Warranty Services</h5>
 										<p>HP 워런티 기간에는 익일 영업일 현장 방문서비스를 받으실 수 있습니다. 케어팩 서비스로 보증을 업그레이드하거나 기간을 연장하실 수 있습니다.</p>
 									</li>
 								</ul>
 							</div>
 							<div id="content2" class="tabscontent cooperCon" style="display: none;">
 								<div class="coop2">
 									<h4>SAMSUNG 비즈니스 제안</h4>
 								</div>
 								<dl>
 									<dt>SAMSUNG</dt>
 									<dd>삼성전자 비즈니스는 혁신적이고 다양한 제품, 업종별 특화 솔루션, 비즈니스 고객을 위한 전문 서비스를 바탕으로 한차원 높은 수준의 고객가치를 제공합니다.</dd>
 								</dl>
 								<ul>
 									<li>
 										<img src="/m/images/sub/img_cp21.jpg">
 										<h5><span>Smart Product</span> 혁신적이고 다양한 제품</h5>
 										<p class="dot">삼성 녹스 탑재 스마트폰</p>
 										<p class="dot">MDFPP 보완인증<br>(미국 국가정보보증협회 NIAP, 2014)</p>
 										<p class="dot">비즈니스 경쟁 우위 확보</p>
 									</li>
 									<li>
 										<img src="/m/images/sub/img_cp22.jpg">
 										<h5><span>Solution</span> 업종별 특화 솔루션</h5>
 										<p class="dot">6년 연속 상업용 디스플레이</p>
 										<p class="dot">시장 1위 삼성 스마트사이니지<br>(HIS Display Search, 2014 3Q 기준)</p>
 										<p class="dot">비즈니스 토탈 솔루션</p>
 									</li>
 									<li>
 										<img src="/m/images/sub/img_cp23.jpg">
 										<h5><span>Service</span> (A/S 전문 대응 능력)</h5>
 										<p class="dot">삼성 프린터, 복합기 2년연속 올해의 제품상 수상<br>(美바이어스랩, 2014)</p>
 										<p class="dot">비즈니스 장기 안정성 확보</p>
 									</li>
 								</ul>
 							</div>
 							<div id="content3" class="tabscontent cooperCon" style="display: none;">
 								<div class="coop3">
 									<h4>LG 비즈니스 제안</h4>
 								</div>
 								<dl>
 									<dt>LG</dt>
 									<dd>공장/산업, 사무실, 유통 등 LG전자가 각 기업에 최적화된 비즈니스 환경을 제안합니다.</dd>
 								</dl>
 								<ul>
 									<li>
 										<img src="/m/images/sub/img_cp31.jpg">
 										<h5>IT</h5>
 										<p>실용적이며 뛰어난 성능의 LG전자 IT기기로 편리한 비즈니스 환경 구축이 가능합니다.</p>
 										<p class="dot">컴퓨터, 노트북, 모니터, 프린터</p>
 									</li>
 									<li>
 										<img src="/m/images/sub/img_cp32.jpg">
 										<h5>커머셜 디스플레이</h5>
 										<p class="dot">LG전자 사이니지 솔루션</p>
 										<p>세계가 인정한 IPS방식의 PD패널 사용으로 선명한 화질 재현과 내구성을 강화하고 LG 스마트 에너지 세이빙 기술로 탁월한 소비전력 절감을 이루는 미디어 플레이어 통합 솔루션을 제공합니다. </p>
 									</li>
 									<li>
 										<img src="/m/images/sub/img_cp33.jpg">
 										<h5>주방 ∙ 생활가전</h5>
 										<p>인간적인 디자인, 진보적 기술의 LG전자 주방가전과 생활가전입니다.</p>
 									</li>
 								</ul>
 							</div>
 							<div id="content4" class="tabscontent cooperCon" style="display: none;">
 								<h5>협력사</h5>
 								<div class="logo">
 									<ul>
 										<li><img src="/m/images/sub/logo_coop1.gif" alt="Microsoft"></li>
 										<li><img src="/m/images/sub/logo_coop2.gif" alt="vmware"></li>
 										<li><img src="/m/images/sub/logo_coop3.gif" alt="lenovo"></li>
 										<li><img src="/m/images/sub/logo_coop4.gif" alt="dell"></li>
 										<li><img src="/m/images/sub/logo_coop5.gif" alt="cisco"></li>
 										<li><img src="/m/images/sub/logo_coop6.gif" alt="aten"></li>
 										<li><img src="/m/images/sub/logo_coop7.gif" alt="acronis"></li>
 										<li><img src="/m/images/sub/logo_coop8.gif" alt="hpe"></li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>

 		</div>
 	</div>
 	<div id="A_Footer">
 		<!-- #include virtual="/m/_inc/footer.asp" -->
 	</div>

 	</div>
 	</div>
 </body>

 </.asp>