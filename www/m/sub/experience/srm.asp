<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/m/_inc/head.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
			<dt class="tit">신성SRM</dt>
				<dd>
					<ul>
						<li><a href="business.asp">사업소개</a></li>
						<li><a href="it.asp">IT 구매제안</a></li>
						<li><a href="srm.asp">신성SRM</a></li>
						<li><a href="construction_case.asp">구축사례</a></li>
						<li><a href="cooperative.asp">협력사</a></li>
					</ul>
				</dd>
			</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 				<!-- 서브 타이틀 -->
			<div class="title">
			<h3>신성SRM</h3>
			<a href="../support/onlineReceive_solution.asp?c=7"><img src="/m/images/sub/driver-icon01.png" alt="솔루션 상담요청"> 솔루션 상담요청</a>
		</div>
		<div id="content" class="srmCon">
			<section>
				<h4 class="skyblue">SRM 개요 및 제안 배경</h4>
				<p class="cont_txt">국내 유일의 B2B 네트워크 기반의 고객 맞춤형 <br>IT 전문 구매 시스템입니다.<br>
				신성 SRM 시스템을 이용하시면 구매 현황 파악, 구매 비용 절감, <br>구매 시간 단축, 빠른 기술지원 요청 등으로 인한 <br>
				고객의 업무 생산성이 높아집니다. </p>
				<div>
					<ul>
						<li>
							<p class="titBx titBxg">구매 및 관리의 난점</p>
							<ul>
								<li>
									<dl>
										<dt>01</dt>
										<dd>여러 부소의 구매요청으로 인한
										구매관리 어려움</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>02</dt>
										<dd>다양한 취급 상품 및 공급업체 선정의 어려움</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>03</dt>
										<dd>품목 표준화되지 않고, 자산관리의 낮은 중요도</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>04</dt>
										<dd>소량 다빈도 구매로 인해
										네고의 한계</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>05</dt>
										<dd>복잡한 구매 절차 및 그로 인한 Paper Work 낭비</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>06</dt>
										<dd>납기, 품질에 대한 보증 어려움</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>07</dt>
										<dd>하드웨어 장애 발생 시 업무 생산성
										저하에 따른 대책마련 및 시간자원 낭비</dd>
									</dl>
								</li>
							</ul>
						</li>



						<li class="srmB">
							<p class="titBx">신성 SRM 솔루션</p>
							<ul>
								<li>
									<dl>
										<dt>01</dt>
										<dd>고객 맞춤 전용 구매 시스템으로
										<b>구매프로세스 최적화</b></dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>02</dt>
										<dd><b>1,225종 품목</b> 재고보유
										<b>11,579종 품목</b> 소싱 계약</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>03</dt>
										<dd><b>고객사 업무 환경에 적합한</b>
										하드웨어 및 소모품 <b>맞춤제안.</b><br>
										<b>자산 관리를 위한 QR코드 부착서비스로
										고객 업무</b> 생산성 향상</dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>04</dt>
										<dd>고객사에서 주로 사용하는 품목 DB 분석
										으로 <br><b>1년 출고 품목 재고 운용.</b><br>
										제조사의 공인파트너로 <b>구매비용절감</b></dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>05</dt>
										<dd>시스템에서 제공되는 견적서, 거래명세서,
										<b>배송조회로 <br>구매업무 처리시간 단축</b></dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>06</dt>
										<dd>재고 보유로 <b>고객사의 원하는 적시에
										 납품 가능</b></dd>
									</dl>
								</li>
								<li>
									<dl>
										<dt>07</dt>
										<dd>HP 공인인증점 운영 및 고급 엔지니어가
										<b>긴급하게 기술 지원함</b></dd>
									</dl>
								</li>
							</ul>
						</li>

					</ul>
				</div>
			</section>

			<section class="srm_pro">
				<h4 class="skyblue">SRM 구매 시스템 도입 효과</h4>
				<dl>
					<dt>SRM 구매프로세스 최적화 [19단계 -&gt; 11단계]</dt>
					<dd>외부 요인으로 인한 업무 시간 로스 제거</dd>
					<dd>견적서, 거래명세표, 배송조회 등의 관리적인 업무 시간으로 인한 낭비요소 0%</dd>
					<dd>구매 내역 DB  시스템에 보관됨으로써 별도의 기록이 필요 없고, 내부적인 커뮤니케이션 마찰 해결됨</dd>
				</dl>
				<ul class="mt30">
					<li><img src="/m/images/sub/srm_step19.gif" alt="기업의 일반적인 구매 프로세스 19단계"></li>
					<li><img src="/m/images/sub/srm_arrow2.gif" alt=""></li>
					<li><img src="/m/images/sub/srm_step11.gif" alt="신성 SRM 구매 프로세스 11단계"></li>
				</ul>
			</section>
		</div>
	</div>
 				</div>
 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>