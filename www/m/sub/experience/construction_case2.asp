<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<script>
		function frmSubmit() {
			$("#search_form").submit();
		}
	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">구축사례</dt>
 			<dd>
 				<ul>
 					<li><a href="business.asp">사업소개</a></li>
 					<li><a href="it.asp">IT 구매제안</a></li>
 					<li><a href="srm.asp">신성SRM</a></li>
 					<li><a href="construction_case.asp">구축사례</a></li>
 					<li><a href="cooperative.asp">협력사</a></li>
 				</ul>
 			</dd>
 		</dl>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request.Form("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request.Form("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "BOARD_v1"
Dim query_where			: query_where		= " b_part = 'board09' AND b_cate = '솔루션' "
Dim query_orderby		: query_orderby		= " ORDER BY option_notice DESC, b_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
 				<h3>구축사례</h3>
 			</div>
 			<div id="content" class="case">
 				<ul id="tabMenu_s2" class="mb30">
 					<li rel="m1"  onclick="location.href='construction_case.asp'">하드웨어</li>
 					<li rel="m2" class="active" onclick="location.href='construction_case2.asp'">솔루션</li>
 				</ul>
 				<div class="sch clfix">
 					<!--form name="sch_Form" method="POST" action="#">
 						<input type="hidden" name="s_i" value="all">
 						<select name="p1" id="part1_code">
 							<option value="">&nbsp; ::: 카테고리 ::: </option>
 							<option value="1527743407">&nbsp; 컴퓨터군</option>
 						</select>
 						<input type="text" name="s_o" placeholder="검색어를 입력하세요.">
 						<a href="#">검색</a>
 					</form-->
					<form name="search_form" id="search_form" method="post">
						<select name="fieldname" class="AXSelect vmiddle">
							<option value="b_title" <% If fieldname = "" Or fieldname = "b_title" Then %> selected<% End If %>>제목</option>
							<option value="b_text" <% If fieldname = "b_text" Then %> selected<% End If %>>내용</option>
						</select>
						<input type="text" name="fieldvalue" value="<%=fieldvalue%>" class="AXInput vmiddle">
						<a href="javascript:;" onclick="frmSubmit();">검색</a>
					</form>
 				</div>
 				<div class="article">
 					<ul>
<%
If rs.bof Or rs.eof Then
%>
						<li><table class="article_tbl"><tr><td colspan="3" align="center">등록된 데이터가 없습니다.</td></tr></table></li>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>
						<li>
							<table class="article_tbl">
								<caption class="blind">구축사례-솔루션</caption>
								<colgroup>
									<col width="60px">
									<col width="202px">
									<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<td class="taC"><%=intNowNum%></td>
										<td class="article_img">
										<% If rs("file_1") <> "" Then %>
											<a href="./construction_case2_view.asp?idx=<%=rs("b_idx")%>"><img src="/upload/avanboard_v3/<%=rs("file_1")%>"></a>
										<% End If %>
										</td>
										<td class="article_con">
											<p class="tit"><a href="./construction_case2_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a> </p>
											<p class="date">등록일 : <%=rs("b_writeday")%> <span class="pdL30">조회수 : <%=rs("b_read")%></span></p>
											<a href="./construction_case2_view.asp?idx=<%=rs("b_idx")%>"><%=Cut(rs("b_text"), 90, "...")%></a>
										</td>
									</tr>
								</tbody>
							</table>
						</li>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
					</ul>
 				</div>
 				<!--버튼-->
 				<div class="ntb-listbtn-area mgT10">
 				</div>

				<%call Paging_user("")%>
 				<!--div class="page">
 					<ul class="clfix">
 						<li><a href="#">&lt;&lt;</a></li>
 						<li class="on"><a href="#">1</a></li>
 						<li><a href="#">2</a></li>
 						<li><a href="#">3</a></li>
 						<li><a href="#">4</a></li>
 						<li><a href="#">5</a></li>
 						<li><a href="#">6</a></li>
 						<li><a href="#">7</a></li>
 						<li><a href="#">8</a></li>
 						<li><a href="#">9</a></li>
 						<li><a href="#">&gt;&gt;</a></li>
 					</ul>
 				</div-->
 			</div>
 		</div>
 	</div>
 	<div id="A_Footer">
 		<!-- #include virtual="/m/_inc/footer.asp" -->
 	</div>

 	</div>
 	</div>
 </body>

 </html>