<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include virtual="/m/_inc/head.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 					<dl class="sub_menu">
 							<dt class="tit">사업소개</dt>
 							<dd>
 								<ul>
 									<li><a href="business.asp">사업소개</a></li>
 									<li><a href="it.asp">IT 구매제안</a></li>
 									<li><a href="srm.asp">신성SRM</a></li>
 									<li><a href="construction_case.asp">구축사례</a></li>
 									<li><a href="cooperative.asp">협력사</a></li>
 								</ul>
 							</dd>
 						</dl>

 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
 				<div class="title">
 					<h3>사업소개</h3>
 				</div>
 				<div id="content" class="exBsCon">

 					<!-- part1 -->
 					<div class="part1">
 						<h4 class="mb30"><span>㈜신성씨앤에스</span>는 유비쿼터스 환경에서 최적화된 <span>Solution / Infra / Service</span>를 제공하는 <br>Business Enable로서 고객의 가치 창조에 기여하는 파트너가 되고자 합니다.</h4>
 						<ul>
 							<li class="bs1">
 								<div>
 									<p>Solution</p>
 									<img src="/m/images/sub/bs_titimg1.jpg" alt="">
 								</div>
 								<dl>
 									<dt>고객의 Needs 구현을 위한 최적화된 <span class="cobalt">IT Solution</span> 제공</dt>
 									<dd>
 										<p>ㆍ신성 Recovery</p>
 										<p>ㆍBackup Solution</p>
 										<p>ㆍVMWARE 가상화</p>
 										<p>ㆍ서버 이중화</p>
 										<p>ㆍ화상회의 솔루션</p>
 									</dd>
 								</dl>
 							</li>
 							<li class="bs2">
 								<div>
 									<p>Infra</p>
 									<img src="/m/images/sub/bs_titimg2.jpg" alt="">
 								</div>
 								<dl>
 									<dt>고객의 경영환경에 최적화된 <span class="skyblue">H/W 및 S/W Infra</span> 제공</dt>
 									<dd>
 										<p>ㆍ서버/스토리지 기기</p>
 										<p>ㆍPC/노트북/복합기</p>
 										<p>ㆍNetwork 장비</p>
 										<p>ㆍ전산 소모품성 자재</p>
 									</dd>
 								</dl>
 							</li>
 							<li class="bs3">
 								<div>
 									<p>Service</p>
 									<img src="/m/images/sub/bs_titimg3.jpg" alt="">
 								</div>
 								<dl>
 									<dt>고객의 경쟁력 강화에 최적화된 <span class="whblue">Total Service</span> 제공</dt>
 									<dd>
 										<p>ㆍ특화분야 IT Consultating</p>
 										<p>ㆍSystem Integration</p>
 										<p>ㆍIT Outsourcing</p>
 										<p>ㆍMaintenance</p>
 										<p>ㆍ자산관리 Solution</p>
 									</dd>
 								</dl>
 							</li>
 						</ul>
 					</div>
 					<!-- //part1 -->

 					<!-- part2 -->
 					<div class="part2">
 						<h4 class="mb30">Infra 총판 사업부터 솔루션 구축 및 유지보수까지 <br><span>IT Total 서비스</span>를 제공하고 있습니다.</h4>
 						<ul class="busi_service">
 							<li><img src="/m/images/sub/busi_ser01.jpg" alt=""><strong>서버 및 스토리지</strong>
 								<p>HP골드파트너 (NT서버, WS, HPN 장비,
 									Storage), EMC, Hitachi 스토리지 등</p>
 							</li>
 							<li><img src="/m/images/sub/busi_ser02.jpg" alt=""><strong>PC</strong>
 								<p>HP총판, 삼성스타점, 엘지대리점 PC제품의
 									제안, 판매 (표준화), 유지보수 제공</p>
 							</li>
 							<li><img src="/m/images/sub/busi_ser03.jpg" alt=""><strong>프린팅</strong>
 								<p>프린터, 복합기, 토너, 직접판매, 임대서비스,
 									MPS 사업</p>
 							</li>
 							<li><img src="/m/images/sub/busi_ser04.jpg" alt=""><strong>소프트웨어</strong>
 								<p>Acronis, Veeam, IBM, Mentech,
 									Vmware 전문 파트너</p>
 							</li>
 							<li><img src="/m/images/sub/busi_ser05.jpg" alt=""><strong>통신개통 및 판매</strong>
 								<p>법인 및 개인 통신 서비스 개통 및 장비
 									제공사업</p>
 							</li>
 							<li><img src="/m/images/sub/busi_ser06.jpg" alt=""><strong>보안</strong>
 								<p>Thales HSM, 보안장비 보안용지 솔루션
 									(SecudocuTM)</p>
 							</li>
 							<li><img src="/m/images/sub/busi_ser07.jpg" alt=""><strong>전자기기 부문</strong>
 								<p>기업 전산 시스템 관리를 위한 PC부품 및
 									다양한 H/W 장비의 ONE STOP
 									제공 서비스</p>
 							</li>
 							<li><img src="/m/images/sub/busi_ser08.jpg" alt=""><strong>리스/렌탈</strong>
 								<p>IT장비 (PC, 서버, 프린터, 복합기 등)
 									일반설비, 전문설비의 효과적인
 									리스/렌탈 서비스</p>
 							</li>
 						</ul>


 						<!-- //div -->

 					</div>
 					<!-- //part2 -->

 				</div>
 			</div>
 		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>