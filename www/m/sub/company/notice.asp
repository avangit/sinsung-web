<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">공지사항</dt>
 			<dd>
 				<ul>
 					<li><a href="management.asp">경영철학</a></li>
 					<li><a href="growth.asp">회사성장</a></li>
 					<li><a href="greeting.asp">대표 인사말</a></li>
 					<li><a href="team.asp">팀을 만나다</a></li>
 					<li><a href="history.asp">연혁</a></li>
 					<li><a href="prize.asp">수상실적</a></li>
 					<li><a href="notice.asp">공지사항</a></li>
 					<li><a href="map.asp">찾아오시는 길</a></li>
 				</ul>
 			</dd>
 		</dl>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 15
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "BOARD_v1"
Dim query_where			: query_where		= " b_part = 'board01' AND display_mode <> 'srm' "
Dim query_orderby		: query_orderby		= " ORDER BY option_notice DESC, b_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call DbOpen()

Set rs = dbconn.execute(sql)
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
			<div class="title">
 					<h3>공지사항</h3>
 				</div>
 				<div id="content">
 					<table id="empTable" class="notice">
 						<caption class="blind">공지사항 게시판</caption>
 						<colgroup>
 							<col width="10%">
 							<col width="">
 							<col width="10%">
 							<col width="12%">
 						</colgroup>
 						<tbody>
 							<tr>
 								<th>번호</th>
 								<th>제목</th>
 								<th>조회수</th>
 								<th>작성일</th>
 							</tr>
<%
If rs.bof Or rs.eof Then
%>
							<tr><td colspan="4" align="center">등록된 데이터가 없습니다.</td></tr>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
%>
 							<tr>
 								<td>
								<% If rs("option_notice") = True Then %>
									<img src="/m/images/common/icon_notice.gif" alt="공지">
								<% Else %>
									<%=intNowNum%>
								<% End If %>
								</td>
 								<td class="left"><a href="./notice_view.asp?idx=<%=rs("b_idx")%>"><%=Cut(rs("b_title"),20,"...")%></a></td>
 								<td><%=rs("b_read")%></td>
 								<td><%=Replace(rs("b_writeday"),"-","/")%></td>
 							</tr>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 						</tbody>
 					</table>


 					<!--버튼-->
 					<div class="ntb-listbtn-area mgT10">
 					</div>


 					<!--검색폼-->
 					<div class="ntb-search-area">
 						<form name="search_form" action="" method="post">
 							<select name="fieldname" class="AXSelect vmiddle">
 								<option value="b_title" <% If fieldname = "" Or fieldname = "b_title" Then %> selected<% End If %>>제목</option>
 								<option value="b_text" <% If fieldname = "b_text" Then %> selected<% End If %>>내용</option>
 							</select>
 							<input type="text" name="fieldvalue" value="<%=fieldvalue%>" class="AXInput vmiddle">
 							<input type="submit" value="검색" class="AXButton">
 						</form>
 					</div>

					<% Call Paging_user("") %>
 					<!--div class="page">
 						<ul class="clfix">
 							<li><a href="#">&lt;&lt;</a></li>
 							<li class="on"><a href="#">1</a></li>
 							<li><a href="#">&gt;&gt;</a></li>
 						</ul>
 					</div-->
 				</div>
 			</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>