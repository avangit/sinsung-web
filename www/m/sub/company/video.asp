<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">동영상</dt>
 			<dd>
 				<ul>
 					<li><a href="plan.asp">우리성장</a></li>
 					<li><a href="culture.asp">우리문화</a></li>
 					<li><a href="video.asp">동영상</a></li>
 					<li><a href="person.asp">인재상</a></li>
 					<li><a href="welfare.asp">복지제도</a></li>
 					<li><a href="recruit.asp">인재채용</a></li>
 				</ul>
 			</dd>
 		</dl>
<%
Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
Dim intTotalCount, intTotalPage

Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
Dim intPageSize			: intPageSize 		= 9
Dim intBlockPage		: intBlockPage 		= 10

Dim query_filde			: query_filde		= " * "
Dim query_Tablename		: query_Tablename	= "BOARD_v1"
Dim query_where			: query_where		= " b_part = 'board03' "
Dim query_orderby		: query_orderby		= " ORDER BY option_notice DESC, b_idx DESC"

If Len(fieldvalue) > 0 Then
	query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Call dbopen

Set rs = dbconn.execute(sql)
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
				<div class="title">
 					<h3>동영상</h3>
 				</div>
 				<div id="content" class="videoCon">

 					<dl class="mb20">
 						<dt><i class="fa fa-video-camera skyblue" aria-hidden="true"></i>트러스타의 문화, 학습과 성장, 시스톰의 일상 영상입니다. </dt>
 						<dd>건조한 일터에서 함께 웃을 수 있는 포인트를 찾기 위해 시작한 문화가 이제는 <em style="color:#2f7bb5;">‘다 같이 참여하는 문화’</em>로 고도화되었습니다.</dd>
 					</dl>
 					<div>
 						<div class="video_wrap">
 							<ul class=" clfix">
<%
If rs.bof Or rs.eof Then
%>
								<li>등록된 데이터가 없습니다.</li>
<%
Else
	rs.move MoveCount
	Do While Not rs.eof
		b_addtext1 = rs("b_addtext1")
		arr_b_addtext1 = Split(b_addtext1, " ")

		For i = 0 To Ubound(arr_b_addtext1) - 1
			If InStr(arr_b_addtext1(i), "src=") > 0 Then

				b_addtext1_content = arr_b_addtext1(i)
				Exit For
			End If
		Next

		b_addtext1_final = Replace(Right(Trim(b_addtext1_content), 12), Chr(34), "")
%>
 								<li>
 									<a href="./video_view.asp?idx=<%=rs("b_idx")%>">
 										<h6><%=Cut(rs("b_title"),20,"...")%></h6>
 										<div class="img">
 											<div class="hover"><i>+</i></div>
 											<img src="http://i1.ytimg.com/vi/<%=b_addtext1_final%>/0.jpg" border="0">
 										</div>

 										<div class="vidoe-con">&nbsp;<%=Cut(rs("b_text"),60,"...")%> &nbsp;</div>
 										<p class="date"><%=b_writeday%></p>
 									</a>
 								</li>
<%
		intNowNum = intNowNum - 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 							</ul>
 						</div>

 						<!--버튼-->
 						<div class="ntb-listbtn-area mgT10">
 						</div>

 						<!--검색폼-->
 						<div class="ntb-search-area">
							<form name="search_form" action="" method="post">
								<select name="fieldname" class="AXSelect vmiddle">
									<option value="b_title" <% If fieldname = "" Or fieldname = "b_title" Then %> selected<% End If %>>제목</option>
									<option value="b_text" <% If fieldname = "b_text" Then %> selected<% End If %>>내용</option>
								</select>
								<input type="text" name="fieldvalue" value="<%=fieldvalue%>" class="AXInput vmiddle">
								<input type="submit" value="검색" class="AXButton">
							</form>
 						</div>

						<% Call  Paging_user("") %>
 						<!--div class="page">
 							<ul class="clfix">
 								<li><a href="#">&lt;&lt;</a></li>
 								<li class="on"><a href="#">1</a></li>
 								<li><a href="#">2</a></li>
 								<li><a href="#">3</a></li>
 								<li><a href="#">4</a></li>
 								<li><a href="#">&gt;&gt;</a></li>
 							</ul>
 						</div-->

 					</div>
 				</div>

 			</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>