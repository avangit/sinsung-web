<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">우리성장</dt>
 			<dd>
 				<ul>
 					<li><a href="plan.asp">우리성장</a></li>
 					<li><a href="culture.asp">우리문화</a></li>
 					<li><a href="video.asp">동영상</a></li>
 					<li><a href="person.asp">인재상</a></li>
 					<li><a href="welfare.asp">복지제도</a></li>
 					<li><a href="recruit.asp">인재채용</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>우리성장</h3>
			</div>
			<div id="content" class="planCon">
			<div class="tit" style="background:none">
				<h4>성장 컨셉 및 목적</h4>
				<p class="blueTxt">최고의 조직, 최고의 트러스타</p>
				<p class="txt">밑바탕에는 가치, 인재, 지식경영을 실천합니다.<br>우리는 지속적으로 52주 문화플랜, 학습과 성장, 시스톰을 통해서 행복한 트러스타가 되기 위한 노력을 합니다.</p>
			</div>

			<div class="planBx" style="background:url(/m/images/sub/plan_bg.jpg) no-repeat 0 0;padding-top:280px;margin-top:30px; background-size: 100%;">
				<ul>
					<li>
						<p class="ico_img"><img src="/m/images/sub/ico_img01.gif" alt=""></p>
						<dl>
							<dt>가치경영</dt>
							<dd>회사의 비전과 사명, 핵심가치가 명확하다. </dd>
							<dd>사회와 이웃에게 공헌한다는 자부심이 있다. </dd>
							<dd>영업 전문가 조직으로 정직하게  일하며, 올바르게 수익을 낸다.</dd>
						</dl>
					</li>
					<li>
						<p class="ico_img"><img src="/m/images/sub/ico_img02.gif" alt=""></p>
						<dl>
							<dt>인재경영</dt>
							<dd>돈보다는 사람을 키우는 것이  중요하다. </dd>
							<dd>재능과 강점이 발휘된다. </dd>
							<dd>성장에 대한 열망과 집요함으로  단합된 팀웍이 핵심이다.</dd>
							<dd>행복한 직원이 답이다.</dd>
						</dl>
					</li>
					<li>
						<p class="ico_img"><img src="/m/images/sub/ico_img03.gif" alt=""></p>
						<dl>
							<dt>지식경영</dt>
							<dd>성과와 피드백이 명확하다.</dd>
							<dd>지식을 나눔으로써 함께 성장한다. </dd>
							<dd>고객 중심으로 일한다.</dd>
						</dl>
					</li>
				</ul>
			</div>
			<div class="tit">
				<h4 class="plan_pt">우리의 성장 <b>PLAN</b></h4>
				<p class="blueTxt">돈보다 일 중심, 일보다 사람중심</p>
				<p class="txt">장기vision 사람이 성장해야 조직은 존재한다.<br>그러므로 우리는 계속해서 학습과 훈련을 반복해야 합니다.</p>
			</div>
			<div class="pyramid">
			<ul>
					<li class="py1">재무성과</li>
					<li class="py2">고객만족</li>
					<li class="py3">프로세스</li>
					<li class="py4">학습성장</li>
				</ul>
				<div>
					<dl class="py1">
						<dt>재무적성과</dt>
							<dd>성과달성시 이익분배, 사회에 기여, 봉사</dd>
					</dl>
					<dl class="py2">
						<dt>고객만족</dt>
							<dd>만족한 고객이 최선의 마케팅이다.</dd>
							<dd>KSP(KIND, SPEED, PERFECT A/S)</dd>
					</dl>
					<dl class="py3">
						<dt>프로세스</dt>
							<dd>시스템 - 생산성 개선</dd>
							<dd>혁신을 통한 변화</dd>
					</dl>
					<dl class="py4">
						<dt>학습성장</dt>
							<dd>서비스교육<br>- 년1회 외부교육, 내부 고객접점(MOT) 교육</dd>
							<dd>지식교육<br>- 월1회 독서토론&amp;피드백 / CDP에 따른 <br>경력계발 / IT 외부교육</dd>
							<dd>경영자<br>- MBA / 경영자학교 / 코칭 등</dd>
							<dd>리더<br>- 리더MBA / 직장리더학교 / 팀장리더십스쿨 / 팀장역량강화스쿨</dd>
							<dd>전 구성원<br>- 직장인학교 /  감사경영</dd>
					</dl>
				</div>

			</div>
		</div>
		<div class="plan_award">
		<div class="tit">
			<h4>강소기업 인증</h4>
		</div>
		<ul>
			<li><img src="/m/images/sub/plan_img_01.jpg" alt=""><p>Inno Biz</p>2020 .01. 20 ~ 2020. 01. 19</li>
			<li><img src="/m/images/sub/plan_img_02.jpg" alt=""><p>벤처기업확인서</p>2020 .01. 17 ~ 2022. 01. 16</li>
			<li><img src="/m/images/sub/plan_img_03.jpg" alt=""><p>성과공유기업 확인서</p>2020 .01. 10 ~ 2021. 01. 09</li>
			<li><img src="/m/images/sub/plan_img_04.jpg" alt=""><p>청년 친화 강소기업 선정서</p>2020 .01. 01 ~ 2021. 12. 31</li>
			<li><img src="/m/images/sub/plan_img_05.jpg" alt=""><p>서울형 강소기업 선정</p>2018 .08. 10 ~ 2020 .12 .31</li>

		</ul>

		</div>
		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>