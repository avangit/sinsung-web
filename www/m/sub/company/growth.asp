<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">회사성장</dt>
 			<dd>
 				<ul>
 					<li><a href="management.asp">경영철학</a></li>
 					<li><a href="growth.asp">회사성장</a></li>
 					<li><a href="greeting.asp">대표 인사말</a></li>
 					<li><a href="team.asp">팀을 만나다</a></li>
 					<li><a href="history.asp">연혁</a></li>
 					<li><a href="prize.asp">수상실적</a></li>
 					<li><a href="notice.asp">공지사항</a></li>
 					<li><a href="map.asp">찾아오시는 길</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
				<h3>회사성장</h3>
			</div>
			<div id="content" class="growth">
					<h4>신용등급 <span>BB+</span> / 현금흐름등급 <span>B</span> / Watch등급 <span>정상</span> <br><em> (평가기관 : ㈜이크레더블 2018년 재무결산일)</em></h4>
				<div class="img_box">
					<img src="/m/images/sub/growth.jpg" alt="회사성장 그래프"/>
				</div>
			</div>
		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>