<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">동영상</dt>
 			<dd>
 				<ul>
 					<li><a href="plan.asp">우리성장</a></li>
 					<li><a href="culture.asp">우리문화</a></li>
 					<li><a href="video.asp">동영상</a></li>
 					<li><a href="person.asp">인재상</a></li>
 					<li><a href="welfare.asp">복지제도</a></li>
 					<li><a href="recruit.asp">인재채용</a></li>
 				</ul>
 			</dd>
 		</dl>
<%
b_idx = SQL_Injection(Trim(Request.QueryString("idx")))

Call DbOpen()

sql = "UPDATE BOARD_v1 SET b_read = b_read + 1 WHERE b_idx = " & b_idx
dbconn.execute(sql)

sql = "SELECT * FROM BOARD_v1 WHERE b_idx = " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	b_title = rs("b_title")
	b_text = rs("b_text")
	b_addtext1 = rs("b_addtext1")
	b_read = rs("b_read")
	b_writeday = rs("b_writeday")
End If

rs.Close
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
				<div class="title">
 					<h3>동영상</h3>
 				</div>
 				<div id="content" class="videoCon">

 					<dl class="mb20">
 						<dt><i class="fa fa-video-camera skyblue" aria-hidden="true"></i>트러스타의 문화, 학습과 성장, 시스톰의 일상 영상입니다. </dt>
 						<dd>건조한 일터에서 함께 웃을 수 있는 포인트를 찾기 위해 시작한 문화가 이제는 <em style="color:#2f7bb5;">‘다 같이 참여하는 문화’</em>로 고도화되었습니다.</dd>
 					</dl>
 					<div>



 						<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
 							<caption>게시판 내용</caption>
 							<colgroup>
 								<col width="12%">
 								<col width="">
 								<col width="12%">
 								<col width="15%">
 							</colgroup>
 							<thead>
 								<tr>
 									<th colspan="4" class="r_none"> <%=b_title%></th>
 								</tr>
 							</thead>
 							<tbody>
 								<tr>
 									<th>등록일</th>
 									<td class="left"><%=Left(b_writeday, 10)%></td>
 									<th>조회수</th>
 									<td><%=b_read%></td>
 								</tr>
 								<!--
	<tr>
		<th>E-mail</th>
		<td class="left">psh@sinsungcns.com&nbsp;</td>
		<th>작성자</th>
		<td><span class='bold'>신성씨앤에스</span></td>
	</tr>
	-->

 								<tr>
 									<td colspan="4" class="init">
 										<div class="content-area">
 											<div class="taC pdB50"><%=b_addtext1%></div>
											<%=b_text%>
 										</div>

 									</td>
 								</tr>

 								<!-- 파일첨부 -->

 								<!-- //파일첨부 -->

 							</tbody>
 						</table>

 						<!--버튼-->
 						<div class="ntb-tb-view-btn" style="width:100%">
 							<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='video.asp'">
 							<div class="btnr">

 							</div>
 						</div>


 						<div class="ntb-tb-view-reply">

 							<!--이전글 시작-->
 							<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
 								<caption>게시판 이전/다음글</caption>
 								<colgroup>
 									<col width="12%">
 									<col width="">
 								</colgroup>
 								<tbody>
<%
sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board03' AND b_idx < " & b_idx & " ORDER BY b_idx DESC"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 									<tr>
 										<th width="12%">이전글</th>
 										<td width="88%" class="left font_gray">
 											<a href="./video_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 										</td>
 									</tr>
<%
End If

rs.Close

sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board03' AND b_idx > " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 									<!--다음글 시작-->
 									<tr>
 										<th width="12%">다음글</th>
 										<td width="88%" class="left font_gray">
 											<a href="./video_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 										</td>
 									</tr>
<%
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 								</tbody>
 							</table>
 						</div>





 						<!-- 코멘트 시작 -->

 						<!-- 코멘트 종료-->
 					</div>
 				</div>
 			</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>