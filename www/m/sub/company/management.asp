<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">경영철학</dt>
 			<dd>
 				<ul>
 					<li><a href="management.asp">경영철학</a></li>
 					<li><a href="growth.asp">회사성장</a></li>
 					<li><a href="greeting.asp">대표 인사말</a></li>
 					<li><a href="team.asp">팀을 만나다</a></li>
 					<li><a href="history.asp">연혁</a></li>
 					<li><a href="prize.asp">수상실적</a></li>
 					<li><a href="notice.asp">공지사항</a></li>
 					<li><a href="map.asp">찾아오시는 길</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
 			<div class="title">
				<h3>경영철학</h3>
			</div>
			<div id="content" class="manaCon">

			<!-- section -->
			<div class="section part01">
				<p class="cont_tit">사명과 비전</p>
				<p class="mng_tit">신 (信) + 성 (成)<br>
				+ <span class="cobalt">C (commercial)</span> + <span class="skyblue">N (network)</span> + <span class="blue">S (services)</span></p>
				<p class="mng_bg">제조사와 고객사를 이어주는 서비스</p>
				<ul>
					<li class="mission">
						<dl>
							<dt>사명</dt>
							<dd><span>IT 전문가로서 기업의 올바른 업무환경 구현을 위해<br>최고의 솔루션을 지속적으로 제공한다</span><br>
							As an IT expert we keep providing you with best solution<br>to help you have a right work environment.</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>2025<br>비전</dt>
							<dd class="coBox"><span>글로벌 IT 선두주자!</span><br>Global IT leader!</dd>
						</dl>
					</li>
					<li class="goal">
						<dl>
							<dt>비전<br>목표</dt>
							<dd>
								<span>1</span><br>
								동종업계 1위<br>
								NPS 5년 연속 1위<br>
								취업희망 회사 1위
							</dd>
							<dd>
								<span>10</span><br>
								국내외 지사 10개 설립<br>
								(국내 5개, 해외 5개)
							</dd>
							<dd>
								<span>100</span><br>
								신성인 100명
							</dd>
							<dd>
								<span>1000</span><br>
								 연 매출액 1000억 달성
							</dd>
						</dl>
					</li>
					<li class="value">
						<dl>
							<dt>핵심<br>가치</dt>
							<dd>
								<span>신뢰</span><br>
								Trust
							</dd>
							<dd>
								<span>열정</span><br>
								Passion
							</dd>
							<dd>
								<span>성장</span><br>
								Growth
							</dd>
							<dd>
								<span>고객중심</span><br>
								Customer-focues
							</dd>
							<dd>
								<span>감사정신</span><br>
								Thankful heart
							</dd>
						</dl>
					</li>
					<li class="ability">
						<dl>
							<dt>핵심<br>역량</dt>
							<dd>지식추구<br>정도경영</dd>
							<dd class="pt15">고객에 대한<br>감사로<br>고객을<br>기억하는 것</dd>
							<dd>직원들의<br>경영자 마인드</dd>
							<dd style="line-height:50px;">가격 경쟁력</dd>
							<dd>빠르고 정확한<br>피드백</dd>
							<dd style="padding-top:25px;height:65px;">제조사를<br>능가하는<br>서비스</dd>
						</dl>
					</li>
					<li class="value">
						<dl>
							<dt>핵심<br>습관</dt>
							<dd>
								약속한 시간내<br> 피드백
							</dd>
							<dd>
								감사한 마음으로<br> 반갑게 솔톤(♪) <br>전화응대
							</dd>
							<dd>
								'who' 보다 'how'<br> 집중 문제해결
							</dd>
							<dd>
								출퇴근 시<br> 주변 정리정돈
							</dd>
							<dd>
								풍부한<br>감사표현
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //section -->

			<!-- section -->
			<div class="section part02">
				<p class="cont_tit">핵심가치</p>
				<img src="/m/images/sub/value.gif" alt="">
			</div>
			<!-- //section -->

			<!-- section -->
			<div class="section part03">
				<p class="cont_tit">핵심역량 Big 6</p>
				<dl>
					<dt style="background:#29adc8">1</dt>
					<dd>지식추구 정도경영</dd>
					<dt style="background:#3d90c0">2</dt>
					<dd>고객에 대한 감사로 고객을 기억하는 것</dd>
					<dt style="background:#2f7bb5">3</dt>
					<dd>직원들의 경영자 마인드</dd>
				</dl>
				<dl>
					<dt style="background:#1f60a7">4</dt>
					<dd>가격 경쟁력</dd>
					<dt style="background:#0c3993">5</dt>
					<dd>빠르고 정확한 피드백</dd>
					<dt style="background:#0c2262">6</dt>
					<dd>제조사를 능가하는 서비스</dd>
				</dl>
			</div>
			<!-- //section -->

			<div class="section part05">
				<p class="cont_tit">핵심습관 Big 5</p>
				<dl>
					<dt style="background:#29adc8">1</dt>
					<dd><p>우리는 문제 발생 시 <span>약속한 시간내 피드백</span>을 합니다.</p></dd>
					<dt style="background:#3d90c0">2</dt>
					<dd><p>우리는 전화를 받을 때 <span>감사한 마음으로 반갑게 <br>솔톤</span>으로 전화를 받습니다.</p></dd>
					<dt style="background:#2f7bb5">3</dt>
					<dd><p>우리는 문제 발생시 <span>‘WHO’보다 ‘HOW’에 집중</span>합니다.</p></dd>
					<dt style="background:#1f60a7">4</dt>
					<dd><p>우리는 <span>출퇴근 시 주변 정리정돈</span>을 잘 합니다.</p></dd>
					<dt style="background:#0c3993">5</dt>
					<dd><p>우리는 감사를 표현할 일이 있을 때 <span>감사를 아끼지 않습니다.</span>
					</p></dd>
				</dl>
			</div>



			<!-- section -->
			<div class="section part04">
				<p class="cont_tit">비전차트(2025)</p>
				<img src="/m/images/sub/visionchart.gif" alt="">
			</div>
			<!-- //section -->

			<!-- section -->
			<div class="section part06">
				<p class="cont_tit">비전차트(2025) ver.2</p>
				<div>
					<img src="/m/images/sub/vision_ver2_01.jpg" alt=""><img src="/m/images/sub/vision_ver2_02.jpg" alt=""><img src="/m/images/sub/vision_ver2_03.jpg" alt=""><img src="/m/images/sub/vision_ver2_04.jpg" alt="">
				</div>
			</div>
			<!-- //section -->
		</div>
		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>