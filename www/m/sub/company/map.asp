 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">찾아오시는 길</dt>
 			<dd>
 				<ul>
 					<li><a href="management.asp">경영철학</a></li>
 					<li><a href="growth.asp">회사성장</a></li>
 					<li><a href="greeting.asp">대표 인사말</a></li>
 					<li><a href="team.asp">팀을 만나다</a></li>
 					<li><a href="history.asp">연혁</a></li>
 					<li><a href="prize.asp">수상실적</a></li>
 					<li><a href="notice.asp">공지사항</a></li>
 					<li><a href="map.asp">찾아오시는 길</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>찾아오시는 길</h3>
			</div>
			<div id="content" class="map">
			<div id="daumRoughmapContainer1464588300537" class="root_daum_roughmap root_daum_roughmap_landing" style="width: 580px"></div>
			<script charset="UTF-8" class="daum_roughmap_loader_script" src="http://dmaps.daum.net/map_js_init/roughmapLoader.js"></script><script charset="UTF-8" src="http://t1.daumcdn.net/kakaomapweb/place/jscss/roughmap/bdd89fff/roughmapLander.js"></script>
			<script charset="UTF-8">
				new daum.roughmap.Lander({
					"timestamp" : "1464588300537",
					"key" : "bzjg",
					"mapWidth" : "580",
					"mapHeight" : "419"
				}).render();
			</script>
			<p class="cont_tit mt30">(주)신성씨엔에스</p>
			<dl style="border-top:2px solid #2570c3;">
				<dt>주소</dt>
				<dd>서울 구로구 디지털로 272, 한신IT타워 5층 </dd>
			</dl>
			<dl>
				<dt>Call Us </dt>
				<dd>02-867-2626</dd>
			</dl>
			<dl>
				<dt>Fax </dt>
				<dd>02-867-2621</dd>
			</dl>
			<dl>
				<dt>E-Mail </dt>
				<dd>psh@sinsungcns.com</dd>
			</dl>
			<p class="btn_print fl_right"><a onclick="javascript:print(document.getElementById('content').innerHTML)" style="cursor:pointer"><img src="/m/images/sub/icon_print.png" alt="">&nbsp;&nbsp;출력하기</a></p>
		</div>
		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>