 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">공지사항</dt>
 			<dd>
 				<ul>
 					<li><a href="management.asp">경영철학</a></li>
 					<li><a href="growth.asp">회사성장</a></li>
 					<li><a href="greeting.asp">대표 인사말</a></li>
 					<li><a href="team.asp">팀을 만나다</a></li>
 					<li><a href="history.asp">연혁</a></li>
 					<li><a href="prize.asp">수상실적</a></li>
 					<li><a href="notice.asp">공지사항</a></li>
 					<li><a href="map.asp">찾아오시는 길</a></li>
 				</ul>
 			</dd>
 		</dl>
<%
b_idx = SQL_Injection(Trim(Request.QueryString("idx")))

Call DbOpen()

sql = "UPDATE BOARD_v1 SET b_read = b_read + 1 WHERE b_idx = " & b_idx
dbconn.execute(sql)

sql = "SELECT * FROM BOARD_v1 WHERE b_idx = " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	b_title = rs("b_title")
	b_text = rs("b_text")
	file_1 = rs("file_1")
	file_2 = rs("file_2")
	file_3 = rs("file_3")
	b_read = rs("b_read")
	b_writeday = rs("b_writeday")
End If

rs.Close
%>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
			<div class="title">
 					<h3>공지사항</h3>
 				</div>
 				<div id="content">
 					<table class="ntb-tb-view" style="width:100%" cellpadding="0" cellspacing="0">
 						<caption>게시판 내용</caption>
 						<colgroup>
 							<col width="12%">
 							<col width="">
 							<col width="12%">
 							<col width="15%">
 						</colgroup>
 						<thead>
 							<tr>
 								<th colspan="4" class="r_none"> <%=b_title%></th>
 							</tr>
 						</thead>
 						<tbody>
 							<tr>
 								<th>등록일</th>
 								<td class="left"><%=b_writeday%></td>
 								<th>조회수</th>
 								<td><%=b_read%></td>
 							</tr>
 							<tr>
 								<th>파일</th>
 								<td class="left" colspan="3">
								<% If file_1 <> "" Then %>
 									<img src="/images/common/file.gif" alt="파일"> <a href="/download.asp?fn=<%=escape(file_1)%>&ph=avanboard_v3" class="pdR20"><%=file_1%></a>
								<% End If %>
								<% If file_2 <> "" Then %>
 									<br><img src="/images/common/file.gif" alt="파일"> <a href="/download.asp?fn=<%=escape(file_2)%>&ph=avanboard_v3" class="pdR20"><%=file_2%></a>
								<% End If %>
								<% If file_3 <> "" Then %>
 									<br><img src="/images/common/file.gif" alt="파일"> <a href="/download.asp?fn=<%=escape(file_3)%>&ph=avanboard_v3" class="pdR20"><%=file_3%></a>
								<% End If %>
								</td>
 							</tr>
 							<tr>
 								<td colspan="4" class="init">
 									<div class="content-area">
 										<p><%=b_text%></p>
 									</div>

 								</td>
 							</tr>
 						</tbody>
 					</table>
 					<!--버튼-->
 					<div class="ntb-tb-view-btn" style="width:100%">
 						<input type="button" value=" 목록 " class="AXButton Classic" onclick="location.href='notice.asp'">
 						<div class="btnr">

 						</div>
 					</div>
 					<div class="ntb-tb-view-reply">

 						<!--이전글 시작-->
 						<table width="100%" cellpadding="0" cellspacing="0" class="ntb-tb-view">
 							<caption>게시판 이전/다음글</caption>
 							<colgroup>
 								<col width="12%">
 								<col width="">
 							</colgroup>
 							<tbody>
<%
sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board01' AND display_mode <> 'srm' AND b_idx < " & b_idx & " ORDER BY b_idx DESC"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 								<tr>
 									<th width="12%">이전글</th>
 									<td width="88%" class="left font_gray">
 										<a href="./notice_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 									</td>
 								</tr>
<%
End If

rs.Close

sql = "SELECT TOP 1 b_idx, b_title FROM BOARD_v1 WHERE b_part = 'board01' AND display_mode <> 'srm' AND b_idx > " & b_idx
Set rs = dbconn.execute(sql)

If Not rs.eof Then
%>
 								<!--다음글 시작-->
 								<tr>
 									<th width="12%">다음글</th>
 									<td width="88%" class="left font_gray">
 										<a href="./notice_view.asp?idx=<%=rs("b_idx")%>"><%=rs("b_title")%></a>
 									</td>
 								</tr>
<%
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 							</tbody>
 						</table>
 					</div>
 				</div>
 			</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>