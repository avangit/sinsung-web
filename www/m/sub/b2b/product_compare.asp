  <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<%
g_idx =  SQL_Injection(Trim(Request("n")))
mode =  SQL_Injection(Trim(Request("m")))
message =  SQL_Injection(Trim(Request("message")))

If g_idx = "" Then
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

If Left(g_idx, 1) = "," Then
	list_g_idx = Split(g_idx, ",")

	For k = 0 To Ubound(list_g_idx)
		If list_g_idx(k) <> "" Then
			If CInt(k) < CInt(Ubound(list_g_idx)) Then
				test = ","
			Else
				test = ""
			End If
				var_g_idx = var_g_idx & list_g_idx(k) & test
		End If
	Next

'	g_idx = var_g_idx
End If

arr_g_idx = Split(g_idx, ",")
%>
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
	<script src="/m/js/product.js"></script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<!-- 서브 타이틀 -->
			<div class="title">
				<h3>제품비교</h3>
			</div>
<%
	ReDim sql(3), rs(3)

	Call DbOpen

	For i = 0 To Ubound(arr_g_idx)
		If arr_g_idx(i) <> "" And Not (ISNULL(arr_g_idx(i))) Then
			sql(i) = "SELECT * FROM GOODS WHERE g_idx = '" & arr_g_idx(i) & "' ORDER BY g_idx DESC"
			Set rs(i) = dbconn.execute(sql(i))
		End If
	Next
%>
			<div id="cpCon">
				<!--div class="blueBarTit">
					<p>Highlight : </p>
					<ul class="chk">
						<li><input type="radio" name="highlightrad" id="None_radio" value="None" checked=""> <label for="None_radio">Off</label></li>
						<li><input type="radio" name="highlightrad" id="Similar_radio" value="Similar"> <label for="Similar_radio">Similarities</label></li>
						<li><input type="radio" name="highlightrad" id="Differences_radio" value="Differences"> <label for="Differences_radio">Differences</label></li>
					</ul>
				</div-->
				<table class="cpTable" id="compareTbl">
					<colgroup>
<%
					For i = 0 To Ubound(arr_g_idx)
%>
						<col style="width:14%">
<%
					Next
%>
						<!--col style="width:14%">
						<col style="width:14%"-->
					</colgroup>
 					<form name="remove_Form" method="post" action="./product_compare.asp">
					<input type="hidden" name="n" value="<%=g_idx%>">
					<tbody>
						<tr class="pdThum">
							<th>Product Thumbnail</th>
<%
	For i = 0 To Ubound(arr_g_idx)
		If arr_g_idx(i) <> "" And Not (ISNULL(arr_g_idx(i))) Then
			If Not(rs(i).eof) Then
				Do While Not rs(i).eof
%>
							<td>
								<input type="hidden" name="del_idx<%=i%>" value="<%=rs(i)("g_idx")%>">
								<p><a href="product_view.asp?idx=<%=rs(i)("g_idx")%>"><img src="/upload/goods/<%=rs(i)("g_simg")%>" alt="<%=rs(i)("g_name")%>"></a></p>
								<a href="#;" class="reBtn" rel="<%=rs(i)("g_idx")%>" data="<%=Ubound(arr_g_idx)%>">REMOVE</a>
							</td>
							<!--td>
								<p><a href="product_view.asp"><img src="/m/images/sub/product_img.jpg" alt=""></a></p>
								<a href="#;" class="reBtn" rel="2236">REMOVE</a>
								<input type="hidden">
							</td-->
<%
					rs(i).MoveNext
				Loop
			End If

			rs(i).MoveFirst
		End If
	Next
%>
						</tr>

						<tr class="pdName">
							<th>Product Name</th>
<%
	For i = 0 To Ubound(arr_g_idx)
		If arr_g_idx(i) <> "" And Not (ISNULL(arr_g_idx(i))) Then
			If Not(rs(i).eof) Then
				Do While Not rs(i).eof
%>
							<td>
								<a href="product_view.asp?idx=<%=rs(i)("g_idx")%>"><em><%=rs(i)("g_name")%></em></a>
							</td>
<%
					rs(i).MoveNext
				Loop
			End If

			rs(i).MoveFirst
		End If
	Next
%>
							<!--td>
							<a href="product_view.asp">
								<em>삼성 노트북 NT951XCJ-K582S</em>
								CPU Intel® Core™ i5-10210U 1.60 G(Up to 4.. </a>
							</td-->
						</tr>
<%
	strSQL = "SELECT opt_name FROM cate_option"
	Set optRS = dbconn.execute(strSQL)

	If Not(optRS.eof) Then
		Do While Not optRS.eof
%>
						<tr class="subheader">
							<th class="depth"><%=optRS("opt_name")%></th>
<%
			For i = 0 To Ubound(arr_g_idx)
				If arr_g_idx(i) <> "" And Not (ISNULL(arr_g_idx(i))) Then
					If Not(rs(i).eof) Then
						Do While Not rs(i).eof
							g_optionList = rs(i)("g_optionList")
							arr_g_optionList = Split(g_optionList, ",")

							If Ubound(arr_g_optionList) > 0 Then
								For j = 1 To Ubound(arr_g_optionList)
									arr_g_optionList1 = Split(arr_g_optionList(j-1), "||")

									If optRS("opt_name") = arr_g_optionList1(0) Then
%>
							<td><%=arr_g_optionList1(1)%></td>
<%
									Else
										If 1 = 0 Then
%>
							<!--td></td-->
<%
										End If
									End If
								Next
							End If

							rs(i).MoveNext
						Loop

						rs(i).MoveFirst
					End If
				End If
			Next
%>
						</tr>
<%
			optRS.MoveNext
		Loop
	End If

	optRS.close
	Set optRS = Nothing
%>
						<!--tr class="subheader">
							<th class="depth">옵션2</th>
							<td></td>
							<td></td>
						</tr>
						<tr class="subheader">
							<th class="depth">옵션3</th>
							<td></td>
							<td></td>
						</tr-->

					</tbody>
				</table>
				<!--table class="cpTable" id="compareTbl">
					<colgroup>
						<col style="width:14%">
						<col style="width:14%">
						<col style="width:14%">
					</colgroup>
					<form name="remove_Form" method="POST" action="/sub/b2b/product_compare.php"></form>
					<input type="hidden" name="arr_goods_list" value="@2238@2236@2235@2234@">
					<tbody>
						<tr class="pdThum">
							<th>Product Thumbnail</th>
							<td>
								<p><a href="product_view.asp"><img src="/m/images/sub/product_img.jpg" alt=""></a></p>
								<a href="#;" class="reBtn">REMOVE</a>
								<input type="hidden">
							</td>
							<td>
								<p><a href="product_view.asp"><img src="/m/images/sub/product_img.jpg" alt=""></a></p>
								<a href="#;" class="reBtn" rel="2236">REMOVE</a>
								<input type="hidden">
							</td>
						</tr>

						<tr class="pdName">
							<th>Product Name</th>
							<td>
								<a href="product_view.asp">
									<em>삼성 노트북 NT931QCG-K582D</em>
									CPU Intel® Core™ i5-1035G4 1.10G(up 3.70 G.. </a>
							</td>
							<td>
							<a href="product_view.asp">
									<em>삼성 노트북 NT951XCJ-K582S</em>
									CPU Intel® Core™ i5-10210U 1.60 G(Up to 4.. </a>
							</td>
						</tr>

						<tr class="subheader">
							<th class="depth">옵션1</th>
							<td></td>
							<td></td>
						</tr>
						<tr class="subheader">
							<th class="depth">옵션2</th>
							<td></td>
							<td></td>
						</tr>
						<tr class="subheader">
							<th class="depth">옵션3</th>
							<td></td>
							<td></td>
						</tr>

					</tbody>
				</table-->
			</div>
		</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>