 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_shopFunc.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<link rel="stylesheet" href="/m/_css/slick-theme.css">
 	<link rel="stylesheet" href="/m/_css/slick.css">
 	<script src="/m/js/slick.js"></script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
<script>
	$(document).ready(function() {

			$(".tab_contents").hide();
			$(".sub_con_tab li:first").addClass("active").show();
			$(".tab_contents:first").show();

			$(".sub_con_tab li").click(function() {
				$(".sub_con_tab li").removeClass("active");
				$(this).addClass("active");
				$(".tab_contents").hide();
				var activeTab = $(this).find("a").attr("href");
				$(activeTab).fadeIn();
				return false;
			});
		$('.product_catecory_inner').slick({
			infinite: false,
			slidesToScroll: 1,
			arrows: false,
//			   autoplay: true,
//      autoplaySpeed: 2000,
			 variableWidth: true
		});

					$('.slider-for').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
//				slidesToShow: 5,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				dots:false,
				centerMode: true,
				focusOnSelect: true,
				variableWidth: true
			});
	});
function goBack() {
  window.history.back();
}
</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
<%
	g_idx = SQL_Injection(Trim(Request.QueryString("idx")))

	Call DbOpen

	sql2 = "SELECT b.c_name FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE g_idx = '" & g_idx & "'"
	Set rs4 = dbconn.execute(sql2)

	If Not rs4.eof Or rs4.bof Then
		cateName = rs4("c_name")
	End If

	rs4.close
%>
 		<div id="A_Container_Wrap" class="bg_c">
 			<div class="sub_tit2">
				<button class="btn_back" onclick="goBack()">뒤로가기</button>
				<h2><%=cateName%></h2>
			</div>
 			<div id="A_Container" class="pd0">
<%
ReDim g_bimg(4), g_menual(4)

sql = "SELECT * FROM GOODS WHERE g_type = 1 AND g_display <> 'srm' AND g_act = 'Y' AND g_idx = '" & g_idx & "'"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	g_name = rs("g_name")
	g_spec = rs("g_spec")
	g_intro = rs("g_intro")
	g_optionList = rs("g_optionList")
	g_simg = rs("g_simg")
	For i = 1 To 5
		g_bimg(i-1) = rs("g_bimg" & i)
	Next
	g_memo = rs("g_memo")
	g_video = rs("g_video")
	For j = 1 To 5
		g_menual(j-1) = rs("g_menual" & j)
	Next
Else
	Call jsAlertMsgBack("일치하는 정보가 없습니다.")
End If

sql = "UPDATE GOODS SET g_read = g_read + 1 WHERE g_type = 1 AND g_display <> 'srm' AND g_act = 'Y' AND g_idx = '" & g_idx & "'"
dbconn.execute(sql)

rs.Close
%>
 				<div id="sub_contents" class="product">
 					<!--div class="inner">
 						<div class="product_search">
 							<span>전체 (123)</span>
 							<div class="right">
 								<select>
 									<option>인기순</option>
 									<option>최신순</option>
 									<option>낮은 가격순</option>
 									<option>높은 가격순</option>
 								</select>
 							</div>
 						</div>
 					</div-->

					<!-- 슬라이드영역-->
					<div class="pdtop">
						<div class="inner">
							<div class="prov_slide">
								<div class="slider-for">
								<% If g_simg <> "" Then %>
									<img src="/upload/goods/<%=g_simg%>">
								<% End If %>
<%
							For i = 1 To 5
								If g_bimg(i-1) <> "" Then
%>
									<img src="/upload/goods/<%=g_bimg(i-1)%>">
<%
								End If
							Next
%>
								</div>
								<div class="slider-nav">
									<div class="img_box">
										<img src="/upload/goods/<%=g_simg%>">
									</div>
<%
							For i = 1 To 5
								If g_bimg(i-1) <> "" Then
%>
									<div class="img_box">
										<img src="/upload/goods/<%=g_bimg(i-1)%>">
									</div>
<%
								End If
							Next
%>
								</div>
							</div>

							<!--제품설명영역-->

							<div class="prov_cont">
								<ul>
									<li>
										<p>제품코드 : #<%=g_idx%></p>
										<h3><%=g_name%></h3>
									</li>
									<li class="line">
										<p class="tit">주요특징 <span><%=g_spec%></span></p>
										<p class="tit">제품개요 <span><%=g_intro%></span></p>
									</li>

									<li class="btn_list"><button class="btn_blue" onclick="location.href='onlineReceive_estimate.asp?idx=<%=g_idx%>'">견적요청</button>
										</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="inner">
						<div class="pv_tab_area">
							<ul class="sub_con_tab">
								<li><a href="#pro_view_01">제품사양</a></li>
								<li><a href="#pro_view_02">제품상세</a></li>
								<li><a href="#pro_view_03">제품영상</a></li>
								<li><a href="#pro_view_04">제품 매뉴얼</a></li>
							</ul>
							<div class="tab_contents " id="pro_view_01">
								<table class="table pv_table">
									<tbody>
<%
	arr_g_optionList = Split(g_optionList, ",")

	If Ubound(arr_g_optionList) > 0 Then
		For i = 0 To Ubound(arr_g_optionList) - 1
			arr_g_optionList1 = Split(arr_g_optionList(i), "||")
%>
										<tr>
											<th><%=Trim(arr_g_optionList1(0))%></th>
											<td><%=Trim(arr_g_optionList1(1))%></td>
										</tr>
<%
		Next
	End If
%>
									</tbody>
								</table>
							</div>

							<div class="tab_contents " id="pro_view_02">
								<div class="dateil_area">
									<%=g_memo%>
								</div>

							</div>
<%
sql = "SELECT b_addtext1 FROM BOARD_v1 WHERE b_part = 'board08' AND b_idx = " & g_video
Set rs = dbconn.execute(sql)
%>
							<div class="tab_contents " id="pro_view_03">
								<div class="video_area">
								<% If Not(rs.eof) Then %>
									<%=rs("b_addtext1")%>
								<% End If %>
								</div>
							</div>
<%
rs.close

For j = 1 To 5
	If g_menual(j-1) <> "" Then
		If j = 1 Then
			arr_var = arr_var & g_menual(j-1)
		Else
			arr_var = arr_var & "," & g_menual(j-1)
		End If
	End If
Next

sql = "SELECT file_1 FROM BOARD_v1 WHERE b_part = 'board07' AND b_idx IN (" & arr_var & ")"
Set rs = dbconn.execute(sql)
%>
							<div class="tab_contents " id="pro_view_04">
								<table class="tab_table">
									<tbody>
<%
	If Not(rs.eof) Then
		Do Until rs.Eof
%>
										<tr>
											<th><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download><%=rs("file_1")%> <!--p>20MB</p--></a></th>
											<td><a href="/download.asp?fn=<%=escape(rs("file_1"))%>&ph=avanboard_v3" download>다운로드</a></td>
										</tr>
<%
			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing

	Call DbClose()
%>
									</tbody>
								</table>
							</div>
						</div>
					</div>
 				</div>

 			</div>
 			<div id="A_Footer">
 				<!-- #include virtual="/m/_inc/footer.asp" -->
 			</div>
 		</div>
 	</div>
 </body>

 </html>