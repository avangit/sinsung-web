 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<link rel="stylesheet" href="/m/_css/slick-theme.css">
 	<link rel="stylesheet" href="/m/_css/slick.css">
 	<script src="/m/js/slick.js"></script>
	<script src="/m/js/jquery.navgoco.js"></script>
	<script src="/js/product.js"></script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script>
 		$(document).ready(function() {
 			$('.product_catecory_inner').slick({
 				infinite: false,
 				slidesToScroll: 1,
				 variableWidth: true,
				 autoplay: false,
 			});
 		});

		$(document).ready(function() {
		var posY;

		function bodyFreezeScroll() {
			posY = $(window).scrollTop();
			$("html").addClass('fix');
			$("html").css("top", -posY);
		}

		function bodyUnfreezeScroll() {
			$("html").removeAttr('class');
			$("html").removeAttr('style');
			posY = $(window).scrollTop(posY);
		}
		$('.btn_filter').click(function() {
			$('.pop_filter').fadeIn();
//			$('.sub_tit2').fadeOut();
			$('.sub_tit4').css('top', '0px');
				bodyFreezeScroll();

		});
		$('.popup .btn_close').click(function() {
			$('.popup').fadeOut();
			$('.sub_tit4').css('top', '80px');

			bodyUnfreezeScroll()

		});

			$(".nav").navgoco({
				caretHtml: '',
				accordion: false,
				openClass: 'open',
				save: true,
				cookie: {
					name: 'navgoco',
					expires: false,
					path: '/'
				},
				slide: {
					duration: 400,
					easing: 'swing'
				},
			});
			$("#collapseAll").click(function(e) {
				e.preventDefault();
				$(".nav").navgoco('toggle', false);
			});

			$("#expandAll").click(function(e) {
				e.preventDefault();
				$(".nav").navgoco('toggle', true);
			});

	});
function goBack() {
  window.history.back();
}

 	</script>
 </head>
<%
Dim fieldname 	: fieldname 	= SQL_Injection(Trim(Request("fieldname")))
Dim fieldvalue 	: fieldvalue 	= SQL_Injection(Trim(Request("fieldvalue")))
Dim orderby		: orderby		= SQL_Injection(Trim(Request("orderby")))
Dim intNowPage	: intNowPage 	= SQL_Injection(Trim(Request("page")))
Dim cate		: cate			= Request.QueryString("c")
ReDim arr_option(20)

If intNowPage = "" Then
	intNowPage = 1
End If

For k = 1 To 20
	arr_option(k) = Trim(Request("option" & k))
Next

Call DbOpen()

sql = "SELECT c_name FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' AND c_code LIKE '" & Left(cate, 2) & "%'"
Set rs = dbconn.execute(sql)

If Not rs.eof Then
	cateName1 = rs("c_name")
End If

rs.close
%>
 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<div class="sub_tit2 sub_tit4">
						<button class="btn_back" onclick="location.href='/m/sub/b2b/product_list.asp'">뒤로가기</button>
						<h2><%=cateName1%></h2>
					</div>
 		<div id="A_Container_Wrap">
 			<div id="A_Container" class="pd0">

 				<div id="sub_contents" class="product">
<%
	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 6) = '000000' AND c_code LIKE '" & Left(cate, 2) & "%' AND SUBSTRING(c_code, 4, 1) <> '0' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
 					<div class="product_catecory product_catecory2">
						<ul class="product_catecory_inner">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
							<li><a href="?c=<%=rs("c_code")%>"><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close

	For k = 1 To 20
		If arr_option(k) <> "" Then
			If InStr(arr_option(k), ",") = 0 Then
				query_where = query_where & " AND a.g_optionList LIKE '%" & arr_option(k) & "%'"
'			Else
'				arr_option2 = Split(arr_option(k), ", ")
'				For arr = 0 To Ubound(arr_option2)
'					If arr <> Ubound(arr_option2) Then
'						query_where2 = " a.g_optionList LIKE '%" & arr_option2(arr) & "%' OR"
'					End If
'				Next
			End If
		End If
	Next

	If cate <> "" Then
		If Right(cate, 8) = "00000000" Then
			query_where = query_where & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
		ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
			query_where = query_where & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
		ElseIf Mid(cate, 6, 1) <> 0 Then
			query_where = query_where & " AND a.g_category = '" & cate & "'"
		End If

		sql = "SELECT c_name FROM CATEGORY WHERE c_code = '" & cate & "'"
		Set rs = dbconn.execute(sql)

		If Not(rs.bof Or rs.eof) Then
			cateName = rs("c_name")
		End If

		rs.close

		sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y' " & query_where & ""

	Else
		cateName = "전체제품"
		sql = "SELECT ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y'"
	End If

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		TotalCnt = rs("CNT")
	End If

	rs.close
%>
						</ul>
					</div>
					<form name="search_form" action="" method="post">
 					<div class="inner">
 						<div class="product_search product_search2">
							<span><%=cateName%> (<%=TotalCnt%>)</span>
							<div class="right">
								<select name="orderby" onchange="document.search_form.submit();">
									<option value="g_read">인기순</option>
									<option value="g_insertDay">최신순</option>
									<option value="g_Money_asc">낮은 가격순</option>
									<option value="g_Money_desc">높은 가격순</option>
								</select>
								<button type="button" class="btn_filter">필터</button>
							</div>
						</div>
 					</div>
					</form>
<%
Dim intTotalCount, intTotalPage
Dim intPageSize			: intPageSize 		= 3
Dim intBlockPage		: intBlockPage 		= 5

Dim query_filde			: query_filde		= " g_idx, g_name, g_simg, g_Memo "
Dim query_Tablename		: query_Tablename	= "GOODS"
Dim query_where			: query_where		= " g_type = 1 AND g_display <> 'srm' AND g_act = 'Y'"

If cate <> "" Then
	If Right(cate, 8) = "00000000" Then
		query_where = query_where & " AND g_category LIKE '" & Left(cate,2) & "%'"
	ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
		query_where = query_where & " AND g_category LIKE '" & Left(cate,4) & "%'"
	ElseIf Mid(cate, 6, 1) <> 0 Then
		query_where = query_where & " AND g_category = '" & cate & "'"
	End If
End If

	For k = 1 To 20
		If arr_option(k) <> "" Then
			If InStr(arr_option(k), ",") = 0 Then
				query_where = query_where & " AND g_optionList LIKE '%" & arr_option(k) & "%'"
			Else
				query_where = query_where & " OR (g_optionList LIKE '%" & arr_option(k) & "%')"
			End If
		End If
	Next

If orderby = "" Then
	query_orderby = " ORDER BY g_idx DESC"
Else
	If orderby = "g_Money_asc" Then
		query_orderby = " ORDER BY g_Money ASC"
	ElseIf orderby = "g_Money_desc" Then
		query_orderby = " ORDER BY g_Money DESC"
	Else
		query_orderby = " ORDER BY " & orderby & " DESC"
	End If
End If

Call intTotal

Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

sql = getQuery

Set rs = dbconn.execute(sql)
%>
					<form name="list" method="post" action="./product_compare.asp" style="margin:0;">
					<input type="hidden" name="n">
					<input type="hidden" name="m">
					<input type="hidden" name="message">
 					<ul class="pro_list pro_list03">
<%
If rs.bof Or rs.eof Then
%>
						<li align="center">등록된 제품이 없습니다.</li>
<%
Else
	i = 0
	Do While Not rs.eof
%>
 						<li>
 							<div class="img_box"><a href="product_view.asp?idx=<%=rs("g_idx")%>">
 									<div class="chk_box">
										<input id="pro0<%=i%>" type="checkbox" value="<%=rs("g_idx")%>">
										<label for="pro0<%=i%>"><span></span></label>
 									</div><img src="/upload/goods/<%=rs("g_simg")%>" alt="<%=rs("g_name")%>">
 								</a></div>
 							<div class="cont_box">
 								<a href="product_view.asp?idx=<%=rs("g_idx")%>">
 									<p class="pro_tit"><%=rs("g_name")%></p>
 									<span># <%=rs("g_idx")%></span>
 									<span class="hide"><%=rs("g_Memo")%></span>
 								</a>
 							</div>
 							<div class="a_box">

 								<label for="prod"><span></span></label>
 								<a href="javascript:GetCheckbox(document.list, 'compare', '옵션비교');">옵션비교</a>
 								<a href="./onlineReceive_estimate.asp?idx=<%=rs("g_idx")%>">견적요청</a>
 							</div>
 						</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
%>
 					</ul>
					</form>
 					<div class="inner">
 						<button class="btn_more" onclick="location.href='?c=<%=cate%>&p=<%=intNowPage+1%>'">
 							제품 더 불러오기 (+10)
 						</button>
 					</div>
 				</div>

					<div class="popup pop_filter">
<!--						<div class="pop_wrap">-->
							<div class="sub_tit2 sub_tit3">
								<h2>필터</h2>
								<button class="btn_close">닫기</button>
<!--						</div>-->
							</div>
								<div id="filter_wrap">
									<div class="lnb">
										<ul class="nav">
<%
	sql = "SELECT opt_name, opt_content FROM cate_option"
	Set rs = dbconn.execute(sql)

	If Not rs.eof Then
		i = 1
		Do While Not rs.eof
			opt_name = Trim(rs("opt_name"))
			opt_content = Trim(rs("opt_content"))

			arr_opt_content = Split(opt_content, ", ")
%>
											<li <% If i = 1 Then %>class="open"<% End If %>>
												<a href="#"><span><%=opt_name%></span></a>
												<ul class="ss_menu">
													<li><a href="#.asp">
															<div class="chk_box">
														<% For j = 0 To Ubound(arr_opt_content) %>
															<div class="ft_chk">
																<input id="fliter_<%=opt_name&j%>" type="checkbox" name="option<%=i%>" value="<%=opt_name%>||<%=arr_opt_content(j)%>" <% If arr_option(i) = opt_name&"||"&arr_opt_content(j) Then %> checked<% End If %>>
																<label for="fliter_<%=opt_name&j%>"><span></span><%=arr_opt_content(j)%></label>
															</div>
														<% Next %>
															</div>
														</a></li>
												</ul>
											</li>
<%
			i = i + 1
			rs.MoveNext
		Loop
	End If

	rs.close
	Set rs = Nothing
%>
										</ul>

									</div>

								</div>

					</div>
 			</div>
<%
Call DbClose()
%>
 			<div id="A_Footer">
 				<!-- #include virtual="/m/_inc/footer.asp" -->
 			</div>
 		</div>
 	</div>
 </body>

 </html>