<% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" />
 	<link rel="stylesheet" href="/m/_css/slick-theme.css">
 	<link rel="stylesheet" href="/m/_css/slick.css">
 	<script src="/m/js/slick.js"></script>
	<script src="/m/js/product.js"></script>
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script>
 		$(document).ready(function() {
 			$('.product_catecory_inner').slick({
 				infinite: false,
 				slidesToScroll: 1,
 				variableWidth: true
 			});
 		});

 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<div id="A_Container_Wrap">
 			<div id="A_Container" class="pd0">
 				<div id="sub_contents" class="product">
<%
	cate = Request.QueryString("c")

	Call DbOpen

	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
 					<div class="product_catecory">
 						<ul class="product_catecory_inner">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
 							<li <% If Left(cate, 2) = Left(rs("c_code"), 2) Then %> class="on"<% End If %>><a href="product_category.asp?c=<%=rs("c_code")%>"><i></i><%=rs("c_name")%></a></li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
 							<!--li><a href="product_category.asp">데스크탑/서버</a></li>
 							<li><a href="product_category.asp">디스플레이/TV</a></li>
 							<li><a href="product_category.asp">화상회의</a></li>
 							<li><a href="product_category.asp">복합기/프린터</a></li>
 							<li><a href="product_category.asp">네트워크</a></li>
 							<li><a href="product_category.asp">태블릿/모바일</a></li>
 							<li><a href="product_category.asp">부품/소프트웨어</a></li>
 							<li><a href="product_category.asp">가전</a></li-->
 						</ul>
 					</div>
<%
	If cate <> "" Then
		If Right(cate, 8) = "00000000" Then
			query_where = query_where & " AND a.g_category LIKE '" & Left(cate,2) & "%'"
		ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
			query_where = query_where & " AND a.g_category LIKE '" & Left(cate,4) & "%'"
		ElseIf Mid(cate, 6, 1) <> 0 Then
			query_where = query_where & " AND a.g_category = '" & cate & "'"
		End If

		sql = "SELECT b.c_name, ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y' " & query_where & " GROUP BY b.c_name"
	Else
		If Len(fieldvalue1) > 0 Then
			query_where = query_where &" AND g_name LIKE '%" & fieldvalue1 & "%' "
		End If

		sql = "SELECT '전체제품' AS c_name, ISNULL(COUNT(a.g_category), 0) AS CNT FROM GOODS a INNER JOIN CATEGORY b ON a.g_category = b.c_code WHERE a.g_type = 1 AND a.g_display <> 'srm' AND a.g_act = 'Y' " & query_where & ""
	End If

	Set rs = dbconn.execute(sql)

	If Not(rs.bof Or rs.eof) Then
		cateName = rs("c_name")
		TotalCnt = rs("CNT")
	End If

	rs.close
%>
					<form name="search_form" action="" method="post">
 					<div class="inner">
 						<div class="product_search">
 							<span><%=cateName%> (<%=TotalCnt%>)</span>
 							<div class="right">
								<select name="orderby" onchange="document.search_form.submit();">
									<option value="g_read" <% If orderby = "g_read" Then %> selected<% End If %>>인기순</option>
									<option value="g_insertDay" <% If orderby = "g_insertDay" Then %> selected<% End If %>>최신순</option>
									<option value="g_Money_asc" <% If orderby = "g_Money_asc" Then %> selected<% End If %>>낮은 가격순</option>
									<option value="g_Money_desc" <% If orderby = "g_Money_desc" Then %> selected<% End If %>>높은 가격순</option>
								</select>
 							</div>
 						</div>
 					</div>
					</form>
<%
	Dim fieldname 	: fieldname 	=  SQL_Injection(Trim(Request("fieldname")))
	Dim fieldvalue 	: fieldvalue 	=  SQL_Injection(Trim(Request("fieldvalue")))
	Dim orderby		: orderby		= SQL_Injection(Trim(Request("orderby")))
	Dim intTotalCount, intTotalPage

	Dim intNowPage			: intNowPage 		= SQL_Injection(Trim(Request("page")))
	Dim intPageSize			: intPageSize 		= 4
	Dim intBlockPage		: intBlockPage 		= 5

	Dim query_filde			: query_filde		= " g_idx, g_name, g_simg, g_Memo "
	Dim query_Tablename		: query_Tablename	= "GOODS"
	Dim query_where			: query_where		= " g_type = 1 AND g_display <> 'srm' AND g_act = 'Y'"
	Dim query_orderby		: query_orderby		= " ORDER BY g_idx DESC"

	If cate <> "" Then
		If Right(cate, 8) = "00000000" Then
			query_where = query_where & " AND g_category LIKE '" & Left(cate,2) & "%'"
		ElseIf Mid(cate, 4, 1) <> 0 And Right(cate, 5) = "00000" Then
			query_where = query_where & " AND g_category LIKE '" & Left(cate,4) & "%'"
		ElseIf Mid(cate, 6, 1) <> 0 Then
			query_where = query_where & " AND g_category = '" & cate & "'"
		End If
	End If

	If Len(fieldvalue) > 0 Then
		query_where = query_where &" AND "& fieldname & " LIKE '%" & fieldvalue & "%' "
	End If

	If Len(fieldvalue1) > 0 Then
		query_where = query_where &" AND g_name LIKE '%" & fieldvalue1 & "%' "
	End If

	If orderby = "" Then
		query_orderby = " ORDER BY g_idx DESC"
	Else
		If orderby = "g_Money_asc" Then
			query_orderby = " ORDER BY g_Money ASC"
		ElseIf orderby = "g_Money_desc" Then
			query_orderby = " ORDER BY g_Money DESC"
		Else
			query_orderby = " ORDER BY " & orderby & " DESC"
		End If
	End If

	Call intTotal

	Dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

	sql = getQuery

	Set rs = dbconn.execute(sql)
%>
 					<ul class="pro_list pro_list03">
					<form name="list" method="post" style="margin:0;">
					<input type="hidden" name="n">
					<input type="hidden" name="m">
					<input type="hidden" name="message">
<%
If rs.bof Or rs.eof Then
%>
						<li>등록된 제품이 없습니다.</li>
<%
Else
	i = 1
	rs.move MoveCount
	Do While Not rs.eof
%>
 						<li>
 							<div class="img_box"><a href="product_view.asp?idx=<%=rs("g_idx")%>">
 									<div class="chk_box">
 										<input type="checkbox" id="pro0<%=i%>" value="<%=rs("g_idx")%>">
 										<label for="pro0<%=i%>"><span></span></label>
 									</div><img src="/upload/goods/<%=rs("g_simg")%>" alt="<%=rs("g_name")%>">
 								</a></div>
 							<div class="cont_box">
 								<a href="product_view.asp?idx=<%=rs("g_idx")%>">
 									<p class="pro_tit"><%=rs("g_name")%></p>
 									<span># <%=rs("g_idx")%></span>
 									<p class="hide"><%=rs("g_Memo")%></p>
 								</a>
 							</div>
 							<div class="a_box">
 								<label for="prod"><span></span></label>
 								<!--a href="javascript:GetCheckbox(document.list, 'compare', '옵션비교');">옵션비교</a-->
 								<a href="javascript:GetCheckbox(document.list, 'request', '견적요청');">견적요청</a>
 							</div>
 						</li>
<%
		i = i + 1
		rs.MoveNext
	Loop
End If

rs.Close
Set rs = Nothing

Call DbClose()
%>
 					</ul>
					</form>
 					<div class="inner">
 						<button class="btn_more">
 							제품 더 불러오기 (+10)
 						</button>
 					</div>
 				</div>

 			</div>
 			<div id="A_Footer">
 				<!-- #include virtual="/m/_inc/footer.asp" -->
 			</div>
 		</div>
 	</div>
 </body>

 </html>