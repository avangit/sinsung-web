 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
 	<!-- #include virtual="/m/_inc/head.asp" -->
 	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<link rel="stylesheet" href="/m/_css/style.css" type="text/css">
 	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
 	<script>
 		$(function() {
 			$("#tabM_Con > div").hide();
 			$("#tabM_Con > div:first").show();

 			$("#tabMenu_s li").click(function() {
 				$("#tabMenu_s li").removeClass("active");
 				$(this).addClass("active");
 				$("#tabM_Con > div").hide()
 				var activeTab = $(this).attr("rel");
 				$("#" + activeTab).fadeIn();
 			});
 		});
 	</script>
 </head>

 <body>
 	<div id="A_Wrap">
 		<div id="A_Header" class="active">
 			<!-- #include virtual="/m/_inc/header.asp" -->
 		</div>
 		<!-- 서브 네비게이션 -->
 		<dl class="sub_menu">
 			<dt class="tit">백업 및 복구</dt>
 			<dd>
 				<ul>
 					<li><a href="recovery.asp">신성 리커버리</a></li>
 					<li><a href="backup.asp">백업 및 복구</a></li>
 					<li><a href="server.asp">서버이중화</a></li>
 					<li><a href="virtual.asp">가상화</a></li>
 					<li><a href="document.asp">문서중앙화</a></li>
 				</ul>
 			</dd>
 		</dl>
 		<div id="A_Container">
 			<!-- 서브컨텐츠 -->
 			<div id="A_Container_C">
 				<!-- 서브 타이틀 -->
 			<div class="title">
			<h3>백업 및 복구</h3>
			<a href="../support/onlineReceive_solution.asp?c=2"><img src="/m/images/sub/driver-icon01.png"> 솔루션 상담요청</a>
		</div>

		<ul id="tabMenu_s" class="mgT50">
			<li rel="m1" class="active">Acronis</li>
			<li rel="m2">Veeam</li>
			<li rel="m3">NetBackup</li>
			<li rel="m4">Arcserve</li>


		</ul>
		<div id="tabM_Con">
			<div id="m1" style="display: block;">
				<!--Acronis-->
				<div id="content" class="solution acr">
					<h3>Acronis® 를 선택해야 하는 5가지 KEY</h3>
					<div class="acr1">
						<ul>
							<li>1. Windows 및 Linux 머신을 단 몇 분 안에 안정적으로 복구</li>
							<li>2. 하나의 솔루션에서 시스템 재해 복구와 데이터 보호</li>
							<li>3. 실제에서 가상 시스템 환경으로의 원활한 전환</li>
							<li>4. 모든 유형의 환경, 플랫폼과 미디어를 위한 하나의 완전한 솔루션</li>
							<li>5. 특정한 유형의 머신 또는 하드웨어에 대한 의존성이 없음</li>
						</ul>
						<h4>하나의 통합 솔루션으로</h4>
						<div class="gyBox">
							<p class="g1">모든 파일, 폴더, DB,<br>애플리케이션과 OS를<br>동시에 한 번에 백업</p>
							<p class="g2">전체 시스템의 즉각적인 <br>복원 및 이기종 하드웨어로<br>베어메탈 복원</p>
							<p class="g3">개별 파일 백업, <br>데이터의 카탈로그,<br>검색과 Indexing</p>
							<p class="g4">물리적 또는 가상 디스크,<br>테이프, 네트워크<br>저장 장치에 백업</p>
						</div>
					</div>
					<h3>Acronis® Backup의  활용 분야</h3>
					<div class="acr2">
						<ul>
							<li>
								<img src="/m/images/sub/img_acr_ab1.gif" alt="">
								<dl>
									<dt>x86 시스템 재해 복구</dt>
										<dd>OS 및 애플리케이션 SW의 오류</dd>
										<dd>패치SW 및 업그레이드 오류</dd>
										<dd>악성 코드 및 바이러스 감염 오류</dd>
										<dd>사용자 부주의로 인한 시스템 오류</dd>
										<dd>하드웨어 불량으로 인한 문제 발생시 기타 불명확한 오류에 시스템 중단</dd>
								</dl>
							</li>
							<li>
								<img src="/m/images/sub/img_acr_ab2.gif" alt="">
								<dl>
									<dt>시스템 통합 유지 및 이전</dt>
										<dd>수많은 윈도우/리눅스 서버 OS 및 애플리케이션 SW 효율적 관리</dd>
										<dd>원격지 환경에서의 셀프서비스</dd>
										<dd>노후 시스템 신규 장비로의 이전</dd>
										<dd>새로운 업무시스템 환경의 설치 관리 </dd>
										<dd>S/W 불법 사용에 대한 내부 관리</dd>
										<dd>미션 크리티컬 시스템 연속성 유지</dd>
								</dl>
							</li>
							<li>
								<img src="/m/images/sub/img_acr_ab3.gif" alt="">
								<dl>
									<dt>가상화 환경 구축 및 재해 복구</dt>
										<dd>물리적 환경 → 가상화 환경(P2V) 이전</dd>
										<dd>가상화 환경 → 물리적 환경(V2P) 이전</dd>
										<dd>가상화 환경 → 가상화 환경(V2V) 이전</dd>
										<dd>가상화 시스템에 대한 백업 및 복구</dd>
										<dd>Vmware, Microsoft, Citrix, RedHat, Parallels 등 다양한 서버 기반 가상화 환경 지원</dd>
								</dl>
							</li>
							<li>
								<img src="/m/images/sub/img_acr_ab4.gif" alt="">
								<dl>
									<dt>데이터 백업/복구</dt>
										<dd>전체 · 증분 · 차등백업 및 복구관리</dd>
										<dd>백업 데이터의 중복 제거 관리</dd>
										<dd>DB 및 문서, 멀티미디어 파일 백업/복구 관리</dd>
										<dd>파티션 · 파일 · 폴더 단위 백업 및 복구관리</dd>
										<dd>다양한 스케줄 템플릿을 통한 자동 백업관리</dd>
								</dl>
							</li>
						</ul>
					</div>
					<h3>Acronis® Backup 기능</h3>
					<div class="acr3">
						<h4><span>Acronis®<br>Backup 기능</span></h4>
						<dl class="box1">
							<dt>주요 기능</dt>
								<dd class="bold">Window server 2012 지원</dd>
								<dd>빠르고 간편한 사용자 인터페이스</dd>
								<dd class="bold">카탈로그 검색</dd>
								<dd>테이프, 디스크, 네트워크 공유, FTP, CD/DVD 백업 관리</dd>
								<dd>자동으로 오프사이트로 백업 복제</dd>
								<dd>여러 위치로 자동 백업 스테이징</dd>
								<dd class="bold">리눅스 및 윈도우즈 간 이기종 복원</dd>
								<dd>시스템 중단 시 Acronis Secure Zone을<br>사용해 복구 모드 전환 (F11 부트)</dd>
								<dd>재해 복구 계획 자동 생성</dd>
								<dd>백업 계획 내보내기와 불러오기</dd>
								<dd>볼트 권한</dd>
								<dd>내장된 백업 구성표로 백업 스케줄링</dd>
								<dd>BIOS, UEFI MBR, GPT 디스크 지원</dd>
								<dd>압축 및 256비트 AES 암호화</dd>
								<dd class="bold">관리 콘솔에서 원격으로 리눅스 에이전트 설치</dd>
								<dd class="bold">Oracle Enterprise Linux 및 Oracle VM 지원</dd>
						</dl>
						<dl class="box2">
							<dt>가상화 지원</dt>
								<dd>에이전트 없는 VM 백업</dd>
								<dd>vCenter와 통합</dd>
								<dd>Instant Restore</dd>
								<dd>동시 백업, LAN-free 백업</dd>
								<dd>Agent for ESX(i)</dd>
								<dd>Hyper-V 클러스터와 CSV</dd>
								<dd class="bold">RedHat 가상화 지원 강화</dd>
						</dl>
						<dl class="box3">
							<dt>다양한 MS Application 백업 지원</dt>
								<dd>데이터 중복제거 옵션으로 고속 백업</dd>
								<dd>이미지 백업으로C 개별 파일 및<br>전체 데이터 복구 가능</dd>
								<dd>Single-Pass 백업으로 시스템 및<br>DB 영역의 동시 백업</dd>
								<dd>SQL Data 카탈로그와 인스턴스<br>및 데이터 베이스 검색</dd>
						</dl>
					</div>
					<h3>Acronis® 서버 제품의 특장점</h3>
					<div class="acr4">
						<h4>전세계의 대표적으로 검증된 윈도우즈·리눅스 서버 시스템 및 데이터 백업 솔루션</h4>
						<h5>대표적인 특장점</h5>
						<table>
							<colgroup>
								<col width="200px"><col width="">
							</colgroup>
							<tbody><tr>
								<th>1. 무중단 설치</th>
								<td>재부팅 없이 SW 설치 및 바로 백업 가능</td>
							</tr>
							<tr>
								<th>2. 라이브 백업</th>
								<td>백업과 동시에 시스템 중단 없이 다른 작업 가능</td>
							</tr>
							<tr>
								<th>3. 다양한 OS 지원</th>
								<td>Windows 2003에서 2012까지, 레드햇, 수세,<br>센트OS, 우분투, 아시아눅스 등 다양한 Linux 서버 지원</td>
							</tr>
							<tr>
								<th>4. UEFI 부트방식 지원</th>
								<td>BIOS방식대비 다양한 인터페이스를 통한 성능향상</td>
							</tr>
							<tr>
								<th>5. 통합 중앙 관리</th>
								<td>최대 1,000대 까지의 서버를 중앙집중관리</td>
							</tr>
							<tr>
								<th>6. 자동 스케쥴링</th>
								<td>하노이타워, GFS, 커스텀 등 스케쥴링 템플릿 제공</td>
							</tr>
							<tr>
								<th>7. 다양한 백업모드</th>
								<td>전체·증분·차등, 파티션·파일·폴더단위 데이터백업</td>
							</tr>
							<tr>
								<th>8. 이기종 환경 복원</th>
								<td>Linux 및 Windows 이기종 서버의 시스템 및 데이터 이전</td>
							</tr>
							<tr>
								<th>9. 가상화 환경 이전</th>
								<td>VMware, Microsoft, Citrix, Redhat, Oracle가상화<br>백업 및 복원</td>
							</tr>
							<tr>
								<th>10. 백업 자동 복제</th>
								<td>생성된 백업아카이브를 최대 5대까지 다른 저장매체로<br>(SAN, NAS, Tape, FTP 등) 자동 복제</td>
							</tr>
							<tr>
								<th>11. LAN-free 백업</th>
								<td>SAN으로 직접 연결된 스토리지 구간 직접 엑세스</td>
							</tr>
							<tr>
								<th>12. 가상머신 동시 백업</th>
								<td>호스트에 등록된 VM을 최대 10대까지 동시 백업</td>
							</tr>
							<tr class="last">
								<th><span>13. 데이터 중복제거</span></th>
								<td>중복제거로 디스크 용량 80%까지 재활용</td>
							</tr>
						</tbody></table>
						<div>
							<p class="n1">아크로니스<br>중앙관리 콘솔</p>
							<dl class="n2">
								<dt>Windows Server</dt>
									<dd>2003~2012 R2</dd>
							</dl>
							<dl class="n3">
								<dt>Linux Server</dt>
									<dd>Redhat, Suse, Cent OS,<br>Fedora, Ubuntu, Asianux</dd>
							</dl>
							<dl class="n4">
								<dt>가상화 서버환경</dt>
									<dd>Vmware, Microsft, Citrix,<br>Redhat, Oracle Parallels</dd>
							</dl>
							<p class="n5">아크로니스<br>스토리지 노드</p>
							<ul class="list">
								<li>문서자료</li>
								<li style="margin-left:30px;">OS</li>
								<li style="margin-left:48px;">DB</li>
								<li style="margin-left:32px;">응용SW</li>
								<li style="float:right">사용자설정</li>
							</ul>
							<ul class="last">
								<li>SAN, NAS, DAS,<br>Tape, FTP, Local Disk,<br>USB, CD, DVD</li>
								<li>외부데이터센터,<br>클라우드 저장소</li>
							</ul>
						</div>
					</div>
					<h3>Acronis® 도입 효과</h3>
					<div class="acr5">
						<!--시간가치-->
						<div class="box1">
							<h4>시간가치</h4>
							<div>
								<h5>복구목표시간(RTO: Recovery Time Objective)<span>“2시간”</span></h5>
								<div>
									<table>
										<colgroup>
											<col width="30%"><col width="">
										</colgroup>
										<tbody><tr>
											<th>아크로니스</th>
											<td>기존 윈도우 백업 및 복구 시간</td>
										</tr>
									</tbody></table>
									<ul>
										<li>0시간</li>
										<li style="margin-left:75px">2시간</li>
										<li>8시간</li>
									</ul>
								</div>
								<h5>복구목표시점(RPO: Recovery Point Objective)<span>“최소 1분 단위”</span></h5>
								<div>
									<table>
										<colgroup>
											<col width="20%"><col width="">
										</colgroup>
										<tbody><tr>
											<th>아크로니스</th>
											<td>기존 윈도우 백업 및 복구 기준 1일</td>
										</tr>
									</tbody></table>
									<ul>
										<li>0분</li>
										<li style="margin-left:55px">1분</li>
										<li>1일</li>
									</ul>
								</div>
								<h5>재해발생지역의 20개 서버 혹은 200개 PC의 복원 시간<span>“1일”</span></h5>
								<div>
									<table>
										<colgroup>
											<col width="35%"><col width="">
										</colgroup>
										<tbody><tr>
											<th>아크로니스</th>
											<td>기존 평균 복구 시간</td>
										</tr>
									</tbody></table>
									<ul>
										<li>0분</li>
										<li style="margin-left:116px">1일</li>
										<li>3일</li>
									</ul>
								</div>
							</div>
						</div>
						<!--시간가치-->
						<!--비용가치-->
						<div class="box2">
							<h4>비용가치</h4>
							<div>
								<h5>20대 서버의 예기치 않은 재해 발생시<br><span>“재해복구 비용 80%이상 절감”</span></h5>
								<p>평균 3일 이상 기간과 많은 인력 소요 되나, 아크로니스의 즉각복원(Instant Restore)기능 활용으로 절감</p>
								<h5>바이러스 감염 등의 200대 PC 재해 발생시<br><span>“재해복구 비용 80%이상 절감”</span></h5>
								<p>평균 3일 이상 기간과 많은 인력 소요, 아크로니스 보안영역(Secure Zone)을 활용(셀프서비스 복원)</p>
								<div>
									<table>
										<colgroup>
											<col width="20%"><col width="">
										</colgroup>
										<tbody><tr>
											<th>아크로니스</th>
											<td>기존 윈도우 복구 비용</td>
										</tr>
									</tbody></table>
									<ul>
										<li>0%</li>
										<li style="margin-left:53px">20%</li>
										<li>100%</li>
									</ul>
								</div>
								<h5>아크로니스 복구목표시간(RTO)에 만족시<br><span>“이중화 시스템에 대한 투자비용 절감”</span></h5>
								<p>이중화 시스템의 동일한 스팩의 하드웨어 구매 불필요</p>
								<div>
									<table>
										<colgroup>
											<col width="50%"><col width="">
										</colgroup>
										<tbody><tr>
											<th>아크로니스</th>
											<td>이중화 시스템 구축에 필요한 하드웨어 비용</td>
										</tr>
									</tbody></table>
									<ul>
										<li>0%</li>
										<li style="margin-left:180px">50%</li>
										<li>100%</li>
									</ul>
								</div>
							</div>
						</div>
						<!--비용가치-->
					</div>
				</div>
				<!--//Acronis-->
			</div>




			<div id="m2" style="display: none;">
				<!--veeam Backup-->
				<div id="content" class="solution veeam">
					<p class="sol_tit">Veeam Backup</p>
					<p class="cont_btxt">Veeam 솔루션은 On-Premise 부터 Private/ Public Cloud 까지 데이터 가용성을 위한 모든 기능이 탑재 되어 있습니다. <br>
					15분 미만의 RTPO™을 지원 하기 위한 다양한 기술을 포함 하고 있습니다.<br><br>
					비즈니스 요구사항 및 예산에 맞는 가용성 솔루션 선택이 가능합니다. </p>
					<!-- section -->
					<div class="section veeamL">
						<ul>
							<li>
								<div>
									<dl>
										<dt class="forestgreen sol_stit">대기업<br>Veeam Availability Suite</dt>
										<dd class="cont_txt">Veeam® Availability Suite™ 는 Veeam Backup &amp; Replication™ 의 <br>업계
										최고의 백업, 복원 및 복제 기능과 Veeam ONE™의 고급 모니터링,<br> 보고
										및 용량 계획 기능을 결합합니다.&nbsp; <br>Veeam Availability Suite는 하나의 관리
										콘솔에서 가상, 물리적, 클라우드의&nbsp;모든&nbsp;작업 부하에&nbsp;대한 가용성을 제공하고 VMware vSphere 및 Microsoft Hyper-V에 대한 최고의 위치에서
										모든 응용 프로그램에 대한 가용성, 모든 클라우드의 모든 데이터에 대한
										Veeam의 리더십 지위를 확장합니다. 고객이 완전히 레거시 백업을 영구히
										제거 할 수 있게 해줍니다! <br>고속 복구, 데이터 손실 방지, 검증 된 복구
										가능성, 활용된 데이터 및 완전한 가시성을 포함한 5가지 핵심 기능으로
										모든 환경을 안정적으로 보호하고 관리합니다.</dd>
									</dl>
									<ul>
										<li><a href="../support/onlineReceive_solution.asp?c=2">제품 상담하기</a></li>
										<li><a href="/download/veeam_proposal_including_customer_case_sinsungcns.pdf" target="_blank">제품 자료 다운로드</a></li>
									  <li><a href="https://www.veeam.com/virtual-machine-backup-solution-free.asp" target="_blank">무료 체험하기</a></li>
									</ul>
								</div>
								<p class="veeamC1"><img src="/m/images/sub/veeamC1.gif" alt=""></p>
							</li>
							<li>
								<div>
									<dl>
										<dt class="forestgreen sol_stit">중기업<br>Veeam 백업 및 복제</dt>
										<dd class="cont_txt">Veeam®&nbsp;백업 및 복제™&nbsp;9.5 업데이트3&nbsp;에 대한 가용성을 제공합니다.<br>
										모든&nbsp;워크로드-virtual, 물리적, 클라우드-단일 관리 콘솔에서 <br>모든 앱 # 1
										가용성에 VMware vSphere 및 Microsoft Hyper-V에 대한 최고의 되는 것을
										Veeam의 리더십 위치를 확장, 모든 클라우드의 모든 데이터.&nbsp;</dd>
									</dl>
									<ul>
										<li><a href="../support/onlineReceive_solution.asp?c=2">제품 상담하기</a></li>
										<li><a href="/download/veeam_proposal_including_customer_case_sinsungcns.pdf" target="_blank">제품 자료 다운로드</a></li>
										<li><a href="https://www.veeam.com/vm-backup-recovery-replication-software.asp" target="_blank">무료 체험하기</a></li>
									</ul>
								</div>
								<p><img src="/m/images/sub/veeamC2.gif" alt=""></p>
							</li>
							<li>
								<div>
									<dl>
										<dt class="forestgreen sol_stit">중소기업(250명 미만)<br>Veeam Backup Essentials </dt>
										<dd class="cont_txt">Veeam Backup Essentials™&nbsp;는 단일 관리 콘솔에서 가상, 물리적, <br>클라우드
										등의&nbsp;모든&nbsp;워크로드에&nbsp;대한 가용성을 제공하고 VMware vSphere 및 Micro-
										soft Hyper-V에 적합한 Veeam의 리더십 위치를 확장하여<br> 모든 애플리케이션
										에 대한 가용성 1위, 모든 클라우드.&nbsp;Veeam Backup Essentials는 Veeam
										Availability Suite™와 동일한 엔터프라이즈 급 기능을 제공하며 최대 6개의
										CPU 소켓 또는 50개의 VM을 갖춘 250명 미만의 직원 및 환경을 갖춘
										소기업을 위해 설계되었으며 최대 60% 이상의 비용을 절감합니다.
										</dd>
									</dl>
									<ul>
										<li><a href="../support/onlineReceive_solution.asp?c=2">제품 상담하기</a></li>
										<li><a href="/download/veeam_proposal_including_customer_case_sinsungcns.pdf" target="_blank">제품 자료 다운로드</a></li>
										<li><a href="https://www.veeam.com/smb-vmware-hyper-v-essentials.asp" target="_blank">무료 체험하기</a></li>
									</ul>
								</div>
								<p><img src="/m/images/sub/veeamC3.gif" alt=""></p>
							</li>
						</ul>

					</div>
					<!-- //section -->

					<!-- section -->
					<div class="section vSpec">
						<p class="sol_tit">제품의 특징 및 장점</p>

						<!-- 우월성 -->
						<div>
							<p class="forestgreen sol_stit">타사 솔루션 대비 우월성</p>
							<table class="veeamtb">
								<colgroup>
									<col width="40%">
									<col width="">
									<col width="">
								</colgroup>
								<thead>
									<tr>
										<th>구분</th>
										<th>Veeam B&amp;R Solution</th>
										<th>기존 백업솔루션 (E사, V사)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>운영 VM에 대한 복제 (운영서비스 DR)</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""><br>별도 라이선스 추가없이 복제기능 사용</td>
										<td><img src="/m/images/sub/icon_false.png" alt=""></td>
									</tr>
									<tr>
										<td>Agentless방식의 백업 및 복구 (VM전체)</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""></td>
										<td><img src="/m/images/sub/icon_true.png" alt=""></td>
									</tr>
									<tr>
										<td>Agentless방식의 파일 레벨의 복구</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""><br>All OS and File Systems</td>
										<td><img src="/m/images/sub/icon_false.png" alt=""><br>Agent 구성 필요, Windows, Linux 지원</td>
									</tr>
									<tr>
										<td>Agentless방식의 어플리케이션 레벨의 복구</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""><br>Oracle, MSSQL, Exchange, SharePoint, AD</td>
										<td><img src="/m/images/sub/icon_false.png" alt=""><br>Agent 구성 필요, Oracle 미지원</td>
									</tr>
									<tr>
										<td>VMware, Hyper-V의 증분 백업 (CBT)</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""><br>자체 CBT 기능 지원</td>
										<td><em class="red">Partial</em></td>
									</tr>
									<tr>
										<td>백업 이미지로부터 즉각 복구</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""><br>All OS 지원</td>
										<td><em class="red">Partial</em><br>일부 OS 미지원</td>
									</tr>
									<tr>
										<td>자동 백업 및 복제 검증</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""></td>
										<td><img src="/m/images/sub/icon_false.png" alt=""></td>
									</tr>
									<tr>
										<td>백업 및 복제 이미지의 테스트용 활용 (Copy데이터활용)</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""></td>
										<td><img src="/m/images/sub/icon_false.png" alt=""></td>
									</tr>
									<tr>
										<td>VMware, Hyper-V 인프라환경 관리</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""></td>
										<td><img src="/m/images/sub/icon_false.png" alt=""></td>
									</tr>
									<tr>
										<td>Public, Private Cloud 연동</td>
										<td class="green">high</td>
										<td class="red">Low</td>
									</tr>
									<tr>
										<td>스토리지 스냅샷 및 중복제거기능 연동</td>
										<td class="green">high</td>
										<td class="red">Low</td>
									</tr>
									<tr>
										<td>효율적인 도입 및 운영 비용</td>
										<td><img src="/m/images/sub/icon_true.png" alt=""><br>VMware 방식과 유사, 물리적 소켓기준<br>가상화 인프라 시스템에 적합</td>
										<td><img src="/m/images/sub/icon_false.png" alt=""><br>서버, 기능 또는 용량 기준<br>Legacy 인프라 시스템에 적합</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- //우월성 -->

						<!-- 5가지 주요기능 -->
						<div class="Vfeature">
							<p class="forestgreen sol_stit">5가지 주요 기능</p>
							<ul>
								<li>
									<dl>
										<dt>
											<img src="/m/images/sub/ico_ft1.gif" alt=""><br>
											고속복구
										</dt>
										<div>
											<dd>즉각복구</dd>
											<dd>파일레벨복구</dd>
											<dd>어플리케이션레벨복구</dd>
										</div>
									</dl>
								</li>
								<li>
									<dl>
										<dt>
											<img src="/m/images/sub/ico_ft2.gif" alt=""><br>
											데이터 손실방지
										</dt>
										<div>
											<dd>스토리리스냅샷 연동</dd>
											<dd>백업과 복제</dd>
											<dd>Cloud 연동 백업 및 복제</dd>
										</div>
									</dl>
								</li>
								<li>
									<dl>
										<dt>
											<img src="/m/images/sub/ico_ft3.gif" alt=""><br>
											복구검증성
										</dt>
										<div>
											<dd>자동 백업검증</dd>
											<dd>자동 복제검증</dd>
										</div>
									</dl>
								</li>
								<li>
									<dl>
										<dt>
											<img src="/m/images/sub/ico_ft4.gif" alt=""><br>
											데이터 활용성
										</dt>
										<div>
											<dd>테스트용도 전용 환경 <br>자동구성</dd>
											<dd>백업/복제데이터의 활용</dd>
											<dd>On-Demand Sandbox for Storage Snapshot </dd>
										</div>
									</dl>
								</li>
								<li>
									<dl>
										<dt>
											<img src="/m/images/sub/ico_ft5.gif" alt=""><br>
											완벽한 가시성
										</dt>
										<div>
											<dd>가상화환경 리포팅 및 <br>모니터링</dd>
											<dd>가상화환경 관리포탈</dd>
										</div>
									</dl>
								</li>
							</ul>
						</div>
						<!-- //5가지 주요기능 -->

						<!-- Veeam Agents -->
						<div class="Vagent">
							<p class="forestgreen sol_stit">Veeam Agents</p>
							<p class="sl_bulltt">Openstack과 물리 환경에 대한 완벽한 보호</p>
							<ul>
								<li class="cont_btxt">OS 이미지 백업 </li>
								<li class="cont_btxt">P2V, P2C 등 Cloud 환경에 완벽 대응</li>
								<li class="cont_btxt">통합 운영 또는 독립적 운영 지원</li>
							</ul>
						</div>
						<!-- //Veeam Agents -->



					</div>
					<!-- //section -->

					<!-- section -->
					<div class="section">
						<p class="sol_tit">간단하고 강력한 보호</p>
						<div class="bg_ft2">
							<dl>
								<dt class="firstdt">Hypervisor 레벨 백업 미지원
								가상환경 <br>(Openstack/KVM/Xen,…)</dt>
								<dd class="ft2_btt">물리 서버</dd>
								<dd>Windows </dd>
								<dd>Linux </dd>
								<dd>Unix (TBD)</dd>
							</dl>
							<dl>
								<dt>데이터 보호</dt>
								<dd>OS 이미지 백업</dd>
								<dd>데이터 백업</dd>
								<dd>DB/ Application 지원</dd>
							</dl>
							<dl class="mr0">
								<dt>효율적인 관리</dt>
								<dd>CBT 지원</dd>
								<dd>Cloud migration</dd>
								<dd>독립모드 또는 관리모드</dd>
							</dl>
						</div>
					</div>
					<!-- //section -->

					<!-- section -->
					<div class="section induL">
						<p class="sol_tit">Industry Leader</p>
						<div>
							<p class="forestgreen sol_stit">주요 리서치 기관에서 Veeam 을 Industry Leader로 다년간 선정 하였습니다.</p>
							<ul>
								<li>
									<div>
										<p class="lineImg"><img src="/m/images/sub/indu_gartner.gif" alt=""></p>
										<p class="indutxt">2017 Magic Quadrant for <br>
										Enterprise Backup and <br>
										Recovery Software</p>
										<img src="/m/images/sub/indu_img1.gif" alt="">
									</div>
								</li>
								<li>
									<div>
										<p class="lineImg"><img src="/m/images/sub/indu_idc.gif" alt=""></p>
										<p class="indutxt">2016H1 Semiannual <br>
										Software Tracker Report</p>
										<img src="/m/images/sub/indu_img2.gif" alt="">
									</div>
								</li>
								<li>
									<div>
										<p class="lineImg limg3"><img src="/m/images/sub/indu_ovum.gif" alt=""></p>
										<p class="indutxt">Decision Matrix: Data Availability <br>
										and Protection Solutions <br>
										for the Cloud Era, 2016-17</p>
										<img src="/m/images/sub/indu_img3.gif" alt="">
									</div>
								</li>
							</ul>
						</div>

						<div class="bg_satif">
							<p class="forestgreen sol_stit mt50">업계 평균을 넘는 고객 만족도</p>
							<dl>
								<dt class="green">2.4 times better than industry average</dt>
								<dd style="font-size:20px;font-family:'Nanum Square';"><b>KPIs (ratings from 1 to 10):</b></dd>
								<dd>
									Satisfaction: <b>9.0</b><br>
									Likelihood to recommend: <b>9.1</b><br>
									Likelihood to renew: <b>9.1</b><br>
									Product features: <b>8.9</b><br>
									Sales effectiveness: <b>9.2</b>
								</dd>
							</dl>
						</div>
					</div>
					<!-- //section -->

					<!-- section -->
					<div class="section Vexpect">
						<p class="sol_tit">Veeam 도입 시 기대 효과</p>
						<p class="cont_btxt">주요 리서치 기관에서 Veeam 을 Industry Leader로 다년간 선정 하였습니다.</p>
						<p class="sl_bulltt mt20 mb30">Veeam 도입 시 정성적/ 정량적인 개선 효과</p>
						<ul>
							<li style="margin-bottom: 30px;">
								<p style="background:url(/m/images/sub/bg_vexp1.gif) no-repeat  0 0 ; background-size: 100%;">정성적 효과</p>
								<dl>
									<dt>1. 완벽한 데이터 보호</dt>
									<dd>RPTO &lt;= 15min</dd>
									<dd>장애 발생 시 가장 최적화된 복구 시나리오 제공(57가지)</dd>
								</dl>
								<dl>
									<dt>2. 운영의 편리성</dt>
									<dd>Agentless 방식으로 간단 하게 </dd>
									<dd>단 한번의 백업 정책으로 간단히 </dd>
								</dl>
								<dl>
									<dt>3. 진보 된 기술력</dt>
									<dd>업계 최초의 신기술 다수 보유</dd>
									<dd>백업/ 복제를 단 한번에 </dd>
									<dd>Cloud 및 DR 에 대한 확장 성 및 데이터 이동성 </dd>

								</dl>
							</li>
							<li>
								<p style="background:url(/m/images/sub/bg_vexp2.gif) no-repeat  0 0; background-size: 100%;">정량적 효과</p>
								<dl>
									<dt>1. 직접 비용 절감</dt>
									<dd>물리적인 CPU 기반의 라이선스 </dd>
									<dd>Core 개수/ VM 개수 / 용량에 무관</dd>
									<dd>고가의 백업 스토리지 불필요 / appliance 등 HW lock free  </dd>

								</dl>
								<dl>
									<dt>2. 간접 비용 절감</dt>
									<dd>WAN 가속기 내장 / 중복 제거 수행 으로 회선 비용 절감</dd>
									<dd>테스트 환경 및 개발 환경 지원 </dd>
									<dd>장애에 대한 사전 대응력 증가</dd>


								</dl>
							</li>
						</ul>
					</div>
					<!-- //section -->



				</div>
				<!--//veeam Backup-->
			</div>






			<div id="m3" style="display: none;">
				<!--NetBackup-->

				<div id="content" class="solution backup">
					<p class="sol_tit">백업 및 복구 제품 </p>
					<p class="cont_btxt">신성씨앤에스의 백업 및 복구 시스템을 통해 복잡성을 해소하고 확장을 통해 비즈니스 성장을 지원하십시오. </p>
					<!-- section -->
					<div class="section sl_fl_list">
						<ul>
							<li class="mh85">
								<dl>
									<dt class="skyblue sol_stit">NetBackup </dt>
									<dd class="oneview_img"><img src="/m/images/sub/backup01.jpg" alt=""></dd>
									<dd class="cont_txt">NetBackup은 엔터프라이즈 환경의 데이터를
									보호하는 데 <br>따른 복잡성을 해소하고 고객의
									성장에 발맞춰 확장하며 <br>더 우수한 민첩성을
									제공함으로써 IT가 단순한 비용 센터가 아닌<br>
									비즈니스 조력자로서의 역할을 수행하도록
									지원합니다. </dd>
									<dd class="sl_list_txt">단일 통합 솔루션으로 포인트 제품 대체</dd>
									<dd class="sl_list_txt">통합 백업 플랫폼으로 개선 </dd>
									<dd class="sl_list_txt">중앙의 정책 기반 관리를 통해 생산성 향상</dd>
									<dd class="sl_list_txt">비지니스 중단 없이 페타바이트 단위의
									  데이터 보호</dd>
									<dd class="sl_list_txt">치솟는 스토리지 인프라스트럭처 비용 절감</dd>
									<dd class="sl_list_txt">빠른 속도로 방대한 데이터 검색 및 복구</dd>
									<dd class="sl_list_txt">안전하게 퍼블릭 클라우드 인프라스트럭처 및
									  스토리지 활용</dd>
									<dd class="sl_list_txt">가능한 시점에서 사용자에게 셀프 서비스 제공
									</dd><dd class="sl_list_txt">데이터를 파악하여 통찰력 확보</dd>
									<dd class="sl_list_txt">최대 30배 빠른 속도로 클라우드 스토리지에 백업
									</dd><dd class="sl_list_txt">추가 클라우드 스토리지 제공업체 및 게이트웨이
									  지원</dd>
									<dd class="sl_list_txt">VMware, Hyper-V, SQL을 위한 향상된 기능 제공</dd>
									<dd class="sl_list_txt">NetApp cDOT와 통합</dd>
									<dd class="sl_list_txt">NetBackup Self Service 및 Information Map 도입</dd>
								</dl>
							</li>
							<li class="mh85">
								<dl>
									<dt class="skyblue sol_stit">NetBackup Appliance </dt>
									<dd class="oneview_img"><img src="/m/images/sub/backup02.jpg" alt=""></dd>
									<dd class="cont_txt">NetBackup Appliance는 성능 및 비용 면에서
									최적화된 하드웨어에 NetBackup Platform의
									모든 기능을 구현하여 백업, 중복 제거, 스토리지
									기능을 모두 갖춘 단일 턴키 솔루션입니다. </dd>
									<dd class="sl_list_txt"><b>NetBackup 5200 Series</b> : 다용도의 비용 효율적인
									통합 백업 어플라이언스로 NetBackup 도메인의
									마스터 서버, 미디어 서버 혹은 두 가지 서버에
									모두 구축할 수 있습니다. <br>
									또한 중복 제거되었거나 중복 제거되지 않은
									데이터를 위한 스토리지 용량 확장을 지원합니다.</dd>
									<dd class="sl_list_txt"><b>NetBackup 5300 Series</b> : 고밀도 스토리지를 포함
									한 성능에 최적화된 통합 백업 미디어 서버로
									기업의 까다로운 성능, 용량 및 레질리언스 요구
									사항을 충족합니다.</dd>
								</dl>
							</li>
							<li class="mh85 last">
								<dl>
									<dt class="skyblue sol_stit">Backup Exec </dt>
									<dd class="oneview_img"><img src="/m/images/sub/backup03.jpg" alt=""></dd>
									<dd class="cont_txt">Backup Exec 15는 업계 최고의 백업 및 복구
									솔루션입니다. <br>빠르고 유연한 복구 기능으로
									편리하게 물리적 시스템 또는 가상 시스템
									백업을 수행하십시오.</dd>
									<dd class="sl_list_txt">가상 시스템과 물리적 시스템을 위한 통합 솔루션</dd>
									<dd class="sl_list_txt">디스크, 테이프, 클라우드까지 지원하는 하이브리드 아키텍처</dd>
									<dd class="sl_list_txt">강력하고 유연하며 편리한 솔루션</dd>
									<dd class="sl_list_txt">VMware, Hyper-V 또는 Citrix에서 실행 중인가상 환경</dd>
									<dd class="sl_list_txt">Microsoft 및 Linux 기반의 물리적 시스템</dd>
									<dd class="sl_list_txt">Exchange, SQL, SharePoint, Active Directory, Enter-
									prise Vault, Oracle, Lotus Domino와 같은 중요 애플리케이션 및 데이터베이스</dd>
									<dd class="sl_list_txt">Exchange 중복 제거 및 가상 시스템 백업 속도
									  대폭 향상</dd>
									<dd class="sl_list_txt">LTO 테이프 드라이브 사용 시 백업 시간 최대 62%  단축</dd>
									<dd class="sl_list_txt">VMware 및 Hyper-V 가상 시스템을 위한 지능형
									  백업으로 가상 시스템 보호 최적화</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt class="skyblue sol_stit">Desktop and Laptop Option </dt>
									<dd class="oneview_img"><img src="/m/images/sub/backup04.jpg" alt=""></dd>
									<dd class="cont_txt">Desktop and Laptop Option은 네트워크 연결
									상태에 관계없이 Windows 및 Mac의 데이터를
									지속적으로 보호하는 간편한 파일 백업 복구 솔루션입니다.</dd>
									<dd class="sl_list_txt">매우 손쉬운 구축</dd>
									<dd class="sl_list_txt">중앙에서 조직 전체의 백업 및 복구 작업 관리</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt class="skyblue sol_stit">System Recovery </dt>
									<dd class="oneview_img"><img src="/m/images/sub/backup05.jpg" alt=""></dd>
									<dd class="cont_txt">System Recovery는 서버, 데스크탑, 랩탑을 위해
									강력한 이미지 기반 백업 및 재해 복구 기능을
									제공하는 통합 솔루션으로, <br>다운타임이나 재해
									발생 시 신속한 업무 복구를 지원합니다.</dd>
									<dd class="sl_list_txt">가상 시스템과 물리적 시스템을 모두 보호하는
									통합 이미지 기반 단일 솔루션입니다.</dd>
									<dd class="sl_list_txt">사용자의 업무 생산성 저하 없이 서버, 가상
									시스템, 데스크탑, 랩탑을 자동으로 백업합니다.</dd>
									<dd class="sl_list_txt">빠르고 유연하며 안정적으로 데이터 및 시스템을
									복구합니다.</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //section -->
				</div>
				<!--//NetBackup-->
			</div>
			<div id="m4" style="display: none;">
				<!--Arcserve-->
				<div id="content" class="solution arc">
					<h3>Overview</h3>
					<h4>Key Features</h4>
					<div class="kfBox">
						<dl>
							<dt>PROTECTION FOR VIRTUAL SERVERS</dt>
								<dd>물리 Linux Server 뿐만 아니라, Vmware ESX, MS Hyper-V,<br>Citrix XenServer에서 VM으로 운영되는 Linux 시스템도 보호</dd>
							<dt>SINGLE SNAPSHOT BACKUP WITH GRANULAR RECOVERY</dt>
								<dd>한번의 전체 시스템 백업과 빠른 File-Level 단위 복원 지원</dd>
							<dt>HARDWARE-INDEPENDENT BARE METAL RECOVERY(BMR)</dt>
								<dd>이기종 H/W간의 BMR 복원 지원</dd>
							<dt>MILITARY-GRADE ENCRYPTION</dt>
								<dd>여러 등급의 백업 암호화 지원</dd>
							<dt>MILITARY-GRADE ENCRYPTION</dt>
								<dd>여러 등급의 백업 암호화 지원</dd>
						</dl>
						<dl>
							<dt>COMPRESSION</dt>
								<dd>스토리지 효율성과 비용절감을 위한 다양한 압축 방식 제공</dd>
							<dt>DEDUPLICATION</dt>
								<dd>소스레벨 중복제거 및 글로벌 중복제거 기능으로 스토리지 효율성과<br>비용절감 효과 증대</dd>
							<dt>WEB-BASED MANAGEMENT CONSOLE</dt>
								<dd>‘https : // &lt;servername&gt; : 8014’ 를 통해 언제 어디서든 접근 가능</dd>
							<dt>VARIOUS LOG</dt>
								<dd>작업 및 로그 정보, 백업 서버<br>정보 이외에 스토리지 사용량뿐만 아니라 자원의 사용<br>상황 등 최신의 정보가 표시</dd>
							<dt>LIMIT BACKUP WRITE SPEED CONTROL</dt>
								<dd>네트워크 부하 조절을 위한 전송 속도 제어 기능(NAS운영시 효율)</dd>
						</dl>
					</div>
					<h3>Background</h3>

					<div class="kf2Box qna">
						<h3>Q &amp; A</h3>
						<table>
							<colgroup>
								<col width="32%"><col width="68%">
							</colgroup>
							<tbody><tr>
								<th>질문</th>
								<th>답변</th>
							</tr>
							<tr>
								<td>arcserve UDP Standard for Linux는 어떻게 라이선스 해야 하나요?</td>
								<td class="bd_blue">보호되는 각 Linux 노드에 대해서만 라이선스 구매가 필요</td>
							</tr>
							<tr>
								<td>arcserve UDP Standard for Linux의 백업 서버와 보호되는 Linux 노드에서 지원되는 Linux 버전은 무엇입니까?</td>
								<td class="bd_blue">백업 서버 : RHEL 및 CentOS 6.0~ 보호되는 Linux 노드 : RHEL 또는 CentOS 5.0-5.9, 6.0~## x86과 x64 하드웨어도 모두 지원</td>
							</tr>
							<tr>
								<td>하나의 백업 작업에서 여러 노드를 백업 할 수 있습니까?</td>
								<td class="bd_blue">네, 노드의 그룹&nbsp; 관리가 가능합니다.<br>또한 작업을 실행할 때 선택한 노드에서만 실행 옵션을 선택할 수 있어 유연한 백업 역시 가능.</td>
							</tr>
							<tr>
								<td>Live CD는 무엇인가요?</td>
								<td class="bd_blue">Arcserve UDP Standard for Linux의 Live CD는 Linux를 임시로 시작하는 데 사용 Live CD가 시작되면 Linux가 네트워크에서 사용 가능한 상태 또한 복원을 위한 Web Console을 임시로 제공합니다.</td>
							</tr>
						</tbody></table>
					</div>
					<div class="kf2Box fn">
						<h3>결론</h3>
						<dl>
							<dt>장점</dt>
								<dd>서버에만 설치하면되기 때문에 백업 환경의 구축 시간을 단축 할 수 있습니다. </dd>
								<dd>또한 쉘 스크립트 동작 요구 사항을 확인하고 베어 메탈 복구 용 Live CD를 만듭니다.</dd>
								<dd>일반적으로 웹 브라우저에서 모든 백업 / 복구 작업을 수행 할 수 있습니다. </dd>
								<dd>클라이언트에 액세스하는 소프트트웨어도 필요하지 않기 때문에 여러 백업 서버를 한 번 관리 할 수 있습니다.</dd>
								<dd>Web 기반의 관리 인터페이스에서 개별 작업 뿐만 아니라 전체 작업의 상태 </dd>
								<dd>스토리지 사용량, 백업 서버 설정 및 리소스 사용량까지 중앙에서 파악할 수 있습니다.</dd>
								<dd>NFS 및 CIFS에서 데이터에 액세스하는 표준 스토리지에 모두 호환되고 내부 디스크와 일부 Removable  디스크(외장 USB HDD)도 사용할 수<br>있습니다. </dd>
								<dd>암호화 및 압축 기능을 사용것이 더 백업 작업을 강화할 수 있습니다.</dd>
								<dd>백업 마법사를 이용하여 보호하려는 Linux 서버의 백업 작업을 쉽게 파악할 수 있다.</dd>
								<dd>원하는 일정에 따라 전체 백업과 증분 백업 결합하여 설정이 가능함으로 유연한 백업 운용이 가능합니다.</dd>
								<dd>복원 마법사를 이용하여 File-Level의 복원이 더 쉽게 실행할 수 있습니다.</dd>
							<dt>The Hidden Cost of Complexity</dt>
								<dd>구축의 간편함, 관리와 운영의 편리성을 토대로 보이지 않는 비용 절감 효과의 극대화</dd>
						</dl>
					</div>
				</div>
				<!--//Arcserve-->
			</div>
		</div>

	</div>

 			</div>
 		<div id="A_Footer">
 			<!-- #include virtual="/m/_inc/footer.asp" -->
 		</div>

 	</div>
 	</div>
 </body>

 </html>