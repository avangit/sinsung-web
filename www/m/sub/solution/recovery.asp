 <% @CODEPAGE="65001" language="vbscript" %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
 <!DOCTYPE html>
 <html lang="ko">

 <head>
     <!-- #include virtual="/m/_inc/head.asp" -->
     <!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
     <link rel="stylesheet" href="/m/_css/style.css" type="text/css">
     <!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
     <script type="text/javascript" src="/m/js/tytabs.jquery.min.js"></script>

     <script>
         $(function() {
             $("#tabsholderR-n").tytabs({
                 tabinit: "1",
                 fadespeed: "fast"

             });
             //리스트(ul) 자동 onoff
             $('ul.faqUl').delegate('.jq_toggle', 'click', function() {
                 $(this).parent().siblings('.on').toggleClass('on');
                 $(this).parent().siblings().children('.jq_hide').slideUp('fast');
                 $(this).parent().toggleClass('on');
                 $(this).siblings('.jq_hide').stop('true', 'true').slideToggle('fast');
             });
         });

     </script>
 </head>

 <body>
     <div id="A_Wrap">
         <div id="A_Header" class="active">
             <!-- #include virtual="/m/_inc/header.asp" -->
         </div>
         <!-- 서브 네비게이션 -->
         <dl class="sub_menu">
             <dt class="tit">신성 리커버리</dt>
             <dd>
                 <ul>
                     <li><a href="recovery.asp">신성 리커버리</a></li>
                     <li><a href="backup.asp">백업 및 복구</a></li>
                     <li><a href="server.asp">서버이중화</a></li>
                     <li><a href="virtual.asp">가상화</a></li>
                     <li><a href="document.asp">문서중앙화</a></li>
                 </ul>
             </dd>
         </dl>
         <div id="A_Container">
             <!-- 서브컨텐츠 -->
             <div id="A_Container_C" class="recoConN">
                 <!-- 서브 타이틀 -->
                 <div class="title">
                     <h3>신성 리커버리</h3>
                     <a href="../support/onlineReceive_solution.asp?c=1"><img src="/m/images/sub/driver-icon01.png" alt="멤버십 인증서 신청"> 솔루션 상담요청</a>
                 </div>

                 <div class="imageArea"><img src="/m/images/sub/visual_recovery.jpg"></div>
                 <div class="titleReN">
                     <h2>SINSUNG Recovery</h2>
                     <p>시스템과 데이터 백업이 동시에 가능한<br> <strong>신성 스마트 리커버리</strong></p>
                     <p class="tit_b">어떠한 장애 시에도 재부팅 만으로 백업 및 복구가 가능합니다.</p>
                 </div>
                 <a href="../support/onlineReceive_solution.asp?c=1" class="btnS">솔루션 상담요청</a>

                 <div id="tabsholderR-n">
                     <ul class="tabs" id="goA">
                         <li id="tab1" class="current"> 시스템 백업</li>
                         <li id="tab2">데이터 백업</li>
                         <li id="tab3">도입효과</li>
                         <li id="tab4">특징 / 기능비교</li>
                         <li id="tab5">기술개요</li>
                     </ul>
                     <div class="recoTabConN">
                         <div id="content1" class="tabscontent recoContent" style="display: block;">
                             <!--<a href="#;" class="btnS">솔루션 상담요청</a>-->
                             <ul class="imgRecoN">
                                 <li>
                                     <p>전산장애로 인한 거래 마비</p>
                                     <img src="/m/images/sub/img_re_11.jpg">
                                 </li>
                                 <li>
                                     <p>원인을 알 수 없는 이유로 인한 정보전산망 마비</p>
                                     <img src="/m/images/sub/img_re_12.jpg">
                                 </li>
                                 <li>
                                     <p>바이러스로 인한 PC감염</p>
                                     <img src="/m/images/sub/img_re_13.jpg">
                                 </li>
                                 <li>
                                     <p>서버다운으로 인한 데이터손실</p>
                                     <img src="/m/images/sub/img_re_14.jpg">
                                 </li>
                             </ul>
                             <dl class="txt">
                                 <dt>불시에 닥칠 IT 재해를 대비해야 합니다.</dt>
                                 <dd>
                                     외부로부터 실시간 수발신 되는 <span>Email 바이러스로 인해 갑자기 PC 작동 멈춤으로 인해</span> 발을 동동 굴러본 적이 있습니다.<br>
                                     랜섬웨어와 같은 복구가 어려운 신종 <span>바이러스에 감염이 되었을 때 우리는 복구솔루션이 마련되어 있지 않아,</span> 고스란히 피해를 떠안아야<br>합니다.<br>
                                     기존의 백신, 방화벽 등의 사전보안 솔루션으로 보호할 수 없는 어떠한 문제에서도 신속하고 편리하게 업무용 컴퓨터를 보호합니다.<br>
                                     기업과 기관의 중요한 문서에 대한 실시간 이력관리 백업 서비스를 통하여 업무의 연속성을 보장하고, 효율적으로 관리 운영이 가능한<br>솔루션입니다.<br>
                                     사전 보안 솔루션의 한계로 인하여 이제 사후 보안/보호 솔루션은 반드시 필요합니다.
                                 </dd>
                             </dl>
                             <dl class="ltDl-n">
                                 <dt>PC 단말보안의 필요성<p></p>
                                 </dt>
                                 <dd>0 Day Attack으로 인한 사전보안의 무력화</dd>
                                 <dd>3.20, 랜섬웨어와 같은 신종 바이러스 출현 </dd>
                                 <dd>패킷이나 파일 패턴 비교 필터링 방식의 한계 </dd>
                                 <dd>대용량 USB, 클라우드 HDD 자료 이동 </dd>
                                 <dd>장애 발생 후 파괴된 데이터를 복원할 수 없음</dd>
                             </dl>
                             <dl class="rtDl-n">
                                 <dt>실제사례 <span>(2014년 3월 20일 악성코드 공격)</span>
                                     <p></p>
                                 </dt>
                                 <dd><img src="/m/images/sub/img_re_15.jpg"></dd>
                             </dl>
                         </div>
                         <div id="content2" class="tabscontent recoContent" style="display: none;">
                             <ul class="imgRecoN">
                                 <li>
                                     <p>데이터가 중요한 오피스 환경</p>
                                     <img src="/m/images/sub/img_re_21.jpg">
                                 </li>
                                 <li>
                                     <p>오피스 작업 중 갑작스럽게 정지된 PC</p>
                                     <img src="/m/images/sub/img_re_22.jpg">
                                 </li>
                                 <li>
                                     <p>PC상태로 인해 날아간 데이터</p>
                                     <img src="/m/images/sub/img_re_23.jpg">
                                 </li>
                                 <li>
                                     <p>불안정한 PC상태로 인한 원치않는 시간 낭비</p>
                                     <img src="/m/images/sub/img_re_24.jpg">
                                 </li>
                             </ul>
                             <dl class="txt">
                                 <dt>데이터는 회사의 중요한 시간 자원의 산출물입니다.</dt>
                                 <dd>오피스 환경에서 누구나 한 번 쯤은 몇 시간 공들여 작성 중인 <span>파일이 불안정한 PC 상태로 인해 저장되지 않아</span> 원치 않는 시간 낭비를 한 적이 있습니다.</dd>
                             </dl>
                             <h3>
                                 <p></p>S/W 재설치로 인한 비용산출
                             </h3>
                             <div class="reDateCon">
                                 <dl>
                                     <dt>일반적인 장애현상</dt>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data11.gif" alt="">
                                         <div>
                                             <p><span>1. 부팅이 안될 때</span></p>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data12.gif" alt="">
                                         <div>
                                             <p><span>2. 알 수 없는 에러가 났을 때</span></p>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data13.gif" alt="">
                                         <div>
                                             <p><span>3. 원하지 않는 사이트에 접속될 때</span></p>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data14.gif" alt="">
                                         <div>
                                             <p><span>4. 블루 스크린이 발생하여<br>&nbsp;&nbsp;&nbsp;시스템이 갑자기 멈추었을 때</span></p>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data15.gif" alt="">
                                         <div>
                                             <p><span>5. 컴퓨터가 갑자기 느려질 때</span></p>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data16.gif" alt="">
                                         <div>
                                             <p><span>6. 바이러스에 감염되었을 때</span></p>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data17.gif" alt="">
                                         <div>
                                             <p><span>7. 포멧, 윈도우 재설치가 필요할 때</span></p>
                                         </div>
                                     </dd>
                                 </dl>
                                 <dl>
                                     <dt>일반적인 해결방법</dt>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data21.gif" alt="">
                                         <div>
                                             <div>
                                                 <ul>
                                                     <li class="time">5분</li>
                                                     <li>PC 리부팅</li>
                                                 </ul>
                                             </div>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data22.gif" alt="">
                                         <div>
                                             <div>
                                                 <ul>
                                                     <li class="time">15분</li>
                                                     <li>백신 검사로 PC최적화</li>
                                                 </ul>
                                             </div>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data23.gif" alt="">
                                         <div>
                                             <div>
                                                 <ul>
                                                     <li class="time">60분</li>

                                                     <li>- 전산 팀 이동시간 및 원격지원 <br>등으로 인한 업무 로스시간</li>
                                                     <li>- 장애접수</li>
                                                 </ul>
                                             </div>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data24.gif" alt="">
                                         <div>
                                             <div>
                                                 <ul>
                                                     <li class="time">3시간</li>
                                                     <li>- 데이터 백업(용량에 따라 시간 상이)</li>
                                                     <li>- HDD 복구 불가시 외부업체 의뢰<br>(통상 7일소요) * 복구여부 불확실</li>
                                                 </ul>
                                             </div>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data25.gif" alt="">
                                         <div>
                                             <div>
                                                 <ul>
                                                     <li class="time">30분</li>
                                                     <li>PC 초기화</li>
                                                 </ul>
                                             </div>
                                         </div>
                                     </dd>
                                     <dd>
                                         <img src="/m/images/sub/icon_re_data26.gif" alt="">
                                         <div>
                                             <div>
                                                 <ul>
                                                     <li class="time rong_t">1시간30분~ 2시간</li>
                                                     <li>소프트웨어 설치 </li>
                                                 </ul>
                                             </div>
                                         </div>
                                     </dd>
                                     <dd class="last">
                                         <div>
                                             <p><span>PC 1대당 최소 소요시간</span>6시간 30분</p>
                                         </div>
                                     </dd>
                                 </dl>
                             </div>
                         </div>
                         <div id="content3" class="tabscontent recoContent" style="display: none;">
                             <h2>신성리커버리의 도입효과</h2>
                             <h3>
                                 <p></p>리커버리 도입전
                             </h3>
                             <p class="img100"><img src="/m/images/sub/img_re_effect1.gif"></p>
                             <div class="reBar">
                                 <p>주) 1년 비용발생 정량산출 : 35건(s/w+etc) 중 50% 17건 * 6h 30 = 110h 30<span>약 14일 소요 * 100,000(1일 용역비) = 1,400,000원 / 1,400,000 * 12개월 = 16,800,000원</span></p>
                             </div>
                             <h3>
                                 <p></p>리커버리 도입 후
                             </h3>
                             <dl class="reEffectDl-n">
                                 <dt><span>1</span>5년이면 6990만원의 비용을 절감 가능</dt>
                                 <dd>
                                     <dl>
                                         <dt>비용 절감</dt>
                                         <dd class="img">
                                             <img src="/m/images/sub/img_re_effect2.gif">
                                             <p>* 주) 1Copy 5만원 기준</p>
                                         </dd>
                                     </dl>
                                 </dd>
                                 <dt><span>2</span>데이터와 시스템의 동시 관리</dt>
                                 <dd>
                                     <dl>
                                         <dt>시스템 순간 백업/복구</dt>
                                         <dd>순간복구기술은 볼륨쉐도우서비스(VSS) 기술을 이용하여 디스크를 동적 백업하여 가상디스크(Virtual Disk)에 시점별로 저장하고, 장애 발생시 여러 개의 시점 중 최적 시점으로 신속하게 복구합니다.<br>x86/x64 Windows system에 적용 가능합니다.</dd>
                                     </dl>
                                 </dd>
                                 <dd>
                                     <dl>
                                         <dt>시스템 파티션 백업/복구</dt>
                                         <dd>파티션(MBR)보호 기술은 시스템 부팅실패의 주요원인인 파티션파괴를 미연에 방지하기 위한 기술임과 동시에 시스템 순간복구 시스템을<br>원활하게 작동하도록 하는 기술입니다.<br>(MBR 캡처및하드디스크파티션복구기술)</dd>
                                     </dl>
                                 </dd>
                                 <dd>
                                     <dl>
                                         <dt>데이터 스케쥴 백업/복구</dt>
                                         <dd>데이터 스케줄백업/복구기술은 시스템의 데이터손실을 미연에 방지하고자 추가된 기술입니다.<br>이 기술은 서비스 실행모드로 동작되도록 개발하여 시스템의 로그오프 상태에서도 완벽하게 백업을 수행 합니다.</dd>
                                         <dd>사용자가 편집 생성한 문서파일은 기관의 중요정보자산으로 PC시스템과 함께 보호해야 할 중요자산입니다.<br>시스템이 정상화 된다 하더라도 업무에 필요한 정보가 없거나, 편집이 완료된 문서만으로는 데이터의 재가공 및 추가적인 업무진행에 있어<br>많은 어려움이 따르는 것이 현실입니다.</dd>
                                         <dd>PC 조기 복구솔루션의 데이터백업은 사용자가 주로 사용하는 파일의 확장자를 대상으로 백업하며, 변경된 자료의 이력까지 안전하게<br>관리하여, 중요자원을 보호합니다.</dd>
                                     </dl>
                                 </dd>
                             </dl>
                         </div>
                         <div id="content4" class="tabscontent recoContent" style="display: none;">
                             <h2>특징</h2>
                             <ul class="reFtUl-n">
                                 <li class="re1">
                                     <div>빠르다</div>
                                     <ul>
                                         <li>어떠한 장애 시에도 재부팅 만으로 O.K.</li>
                                         <li>이력 관리를 통한 데이터 복구도 한번에</li>
                                     </ul>
                                 </li>
                                 <li class="re2">
                                     <div>쉽다</div>
                                     <ul>
                                         <li>별도의 진단 없이 원 클릭 복구</li>
                                         <li>전산지식이 없어도 복구 후 바로 사용 가능</li>
                                     </ul>
                                 </li>
                                 <li class="re3">
                                     <div>편하다</div>
                                     <ul>
                                         <li>모든 시스템의 자동화(백업 및 복구)</li>
                                         <li>필요에 따라 사용이 가능</li>
                                     </ul>
                                 </li>
                             </ul>
                             <p class="img100"><img src="/m/images/sub/img_feature.gif"></p>
                             <h2 class="mt80">주요기능</h2>
                             <h3 class="mt45">
                                 <p></p>시스템 순간 백업 / 복구
                             </h3>
                             <dl class="reFtDl-n">
                                 <dt>빠르고 안정적인 순간 백업 / 복구</dt>
                                 <dd>하드디스크 스냅샷 기술을 이용한 순간 백업/복구 기능은 시스템 체감속도 저하 없이 순간적인 백업 및 복구기능을 제공합니다.</dd>
                                 <dt>긴급복구기능</dt>
                                 <dd>윈도우 장애시, 부팅 전 긴급복구모드를 제공하여 간편하게 시스템을 복원할 수 있습니다.</dd>
                                 <dt>안정적인 복구</dt>
                                 <dd>바이러스나 악성코드 감염시에도 순간복구기능을 이용하면 감염된 상태를 완벽하게 복원할 수 있습니다. </dd>
                                 <dt>MBR 보호기능</dt>
                                 <dd>루트킷 바이러스 및 각종 원인에 의한 MBR의 변형을 보호 합니다. </dd>
                             </dl>
                             <h3 class="mt45">
                                 <p></p>다양한 백업/복구 옵션
                             </h3>
                             <dl class="reFtDl-n">
                                 <dt>수동 백업/복구</dt>
                                 <dd>사용자가 필요할 때 수동으로 백업/복구 합니다.</dd>
                                 <dt>시작복구</dt>
                                 <dd>컴퓨터가 부팅될 때 마다 사용자가 지정한 최적의 시점으로 자동 복구 됩니다. PC방, 학교, 은행 객장 등, 불특정 다수의 고객이<br>이용하는 PC에 적합합니다.</dd>
                                 <dt>스케쥴 복구</dt>
                                 <dd>지정한 시간에 PC를 자동으로 복구합니다. 날짜, 요일 뿐만 아니라 PC 미사용시간을 감지하여 일정시간동안 PC를 사용하지<br>않고 있을 경우 자동으로 복구를 설정할 수 있습니다. PC방, 학교, 은행 객장 등, 불특정 다수의 고객이 이용하는 PC에 적합합니다.</dd>
                                 <dt>스케쥴 백업</dt>
                                 <dd>지정한 시간에 PC를 자동으로 백업함으로서 부주의로 인한 데이터 망실을 미연에 방지할 수 있습니다.</dd>
                                 <dt>실시간 데이터백업</dt>
                                 <dd>고객이 작성하는 문서 등의 데이터를 실시간으로 백업합니다.</dd>
                             </dl>
                             <h3 class="mt45">
                                 <p></p>유용한 부가기능
                             </h3>
                             <dl class="reFtDl-n">
                                 <dt>데이터 이력관리</dt>
                                 <dd>백업되는 문서등 데이터를 버전별로 관리합니다. 최대 10개까지 관리하므로 데이터 망실에 대한 완벽한 해결책을 드립니다.</dd>
                                 <dt>백업제외폴더 기능</dt>
                                 <dd>백업시 제외되는 폴더를 지정함으로써, 복구시에 유실 혹은 초기화 되는 데이터를 미연에 방지할 수 있습니다.<br>패치데이터 폴더나 기타 보안폴더에 적합합니다.</dd>
                                 <dt>백업확장자 지정</dt>
                                 <dd>모든 데이터를 백업함으로써 발생하는 무분별한 용량낭비를 차단하고 사용자가 지정하는 데이터를 지정하여 백업할 수<br>있습니다.</dd>
                                 <dt>복구시점 마운트 기능</dt>
                                 <dd>타사제품의 경우, 백업한 복구시점으로 PC자체를 복구해야만 그 시점의 데이터에 접근할 수 있는 반면, 신성 리커버리 솔루션은<br> 다양한 복구 시점을 드라이브로 마운트하여, 해당 시점으로 복구할 필요 없이 언제라도 원하는 시점의 데이타를<br>검색, 열람, 취득할 수 있습니다.</dd>
                             </dl>
                             <h2 class="mt80">주요기능</h2>
                             <table class="reFtTable">
                                 <colgroup>
                                     <col width="20%">
                                     <col width="20%">
                                     <col width="20%">
                                     <col width="20%">
                                     <col width="20%">
                                 </colgroup>
                                 <tbody>
                                     <tr>
                                         <th>구분</th>
                                         <th class="pnt">스냅샷 방식<br>SINSUNG Smart Recovery</th>
                                         <th>이미지 방식</th>
                                         <th>버퍼 방식</th>
                                         <th>레지스트리 방식</th>
                                     </tr>
                                     <tr>
                                         <td>백업 저장 방식</td>
                                         <td class="pnt">원본 유지형</td>
                                         <td>이미지 백업</td>
                                         <td>변경된 파일 버퍼에 저장</td>
                                         <td>레지스트리 이미지 백업</td>
                                     </tr>
                                     <tr>
                                         <td>복구 방식</td>
                                         <td class="pnt">유지된 원본 디스크로 복구</td>
                                         <td>저장된 이미지 복구</td>
                                         <td>저장된 버퍼의 내용 복구</td>
                                         <td>저장된 레지스트리 파일 복구</td>
                                     </tr>
                                     <tr>
                                         <td>복구 시점 개수</td>
                                         <td class="pnt">10개 이상</td>
                                         <td>1개</td>
                                         <td>1개</td>
                                         <td>여러 개 자동 및<br>수동 시점 생성</td>
                                     </tr>
                                     <tr>
                                         <td>저장 공간</td>
                                         <td class="pnt">별도의 공간이 필요 없음</td>
                                         <td>동일 용량 크기의<br>별도 저장 매체</td>
                                         <td>버퍼 용량<br>(최대 3~10GB)</td>
                                         <td>HDD 남은 용량의 최대 12%</td>
                                     </tr>
                                     <tr>
                                         <td>단일 파티션 보호</td>
                                         <td class="pnt">O</td>
                                         <td>O</td>
                                         <td>O</td>
                                         <td>O</td>
                                     </tr>
                                     <tr>
                                         <td>다중 파티션 보호</td>
                                         <td class="pnt">O</td>
                                         <td>X</td>
                                         <td>X</td>
                                         <td>X</td>
                                     </tr>
                                     <tr>
                                         <td>증분 백업 지원<br>(신규 백업 시)</td>
                                         <td class="pnt">O<br>(변경분 증가)</td>
                                         <td>X<br>(동일 용량 증가)</td>
                                         <td>X<br>(동일 용량 증가)</td>
                                         <td>X<br>(동일 용량 증가)</td>
                                     </tr>
                                     <tr>
                                         <td>복구 시간</td>
                                         <td class="pnt">1~60초<br>(백업 용량과 무관)</td>
                                         <td>수분 ~<br>(1GB당 1분 소요)</td>
                                         <td>수분 ~ 수십분<br>(백업 용량과 무관)</td>
                                         <td>수분 ~<br>(백업 용량에 비례)</td>
                                     </tr>
                                     <tr>
                                         <td>설치 시간</td>
                                         <td class="pnt">1분 내외</td>
                                         <td>수분 ~ 수십분</td>
                                         <td>수분 ~ 수십분</td>
                                         <td>수분 ~</td>
                                     </tr>
                                     <tr>
                                         <td>복구 확률</td>
                                         <td class="pnt">100%</td>
                                         <td>100%</td>
                                         <td>95%</td>
                                         <td>95%</td>
                                     </tr>
                                     <tr>
                                         <td>안정성<br>(장기간 사용시)</td>
                                         <td class="pnt">좋음</td>
                                         <td>백업 데이터 파손</td>
                                         <td>백업 데이터 파손</td>
                                         <td>백업 데이터 파손</td>
                                     </tr>
                                     <tr>
                                         <td>공용 PC실</td>
                                         <td class="pnt">적합</td>
                                         <td>적합</td>
                                         <td>적합</td>
                                         <td>적합</td>
                                     </tr>
                                     <tr>
                                         <td>폴더 제외 설정<br>(단일 드라이브)</td>
                                         <td class="pnt">O</td>
                                         <td>X<br>(별도 솔루션 필요)</td>
                                         <td>X<br>(별도 솔루션 필요)</td>
                                         <td>X<br>(별도 솔루션 필요)</td>
                                     </tr>
                                     <tr>
                                         <td>폴더 제외 설정<br>(복수 드라이브)</td>
                                         <td class="pnt">O<br>(C:, D: 모두 지원)</td>
                                         <td>X<br>(기능 없음)</td>
                                         <td>X<br>(기능 없음)</td>
                                         <td>X<br>(기능 없음)</td>
                                     </tr>
                                 </tbody>
                             </table>
                         </div>
                         <div id="content5" class="tabscontent recoContent" style="display: none;">
                             <h3 class="mt0">
                                 <p></p>복구단계
                             </h3>
                             <ul class="skillStepUl-n">
                                 <li class="step1">
                                     <div>Step 1</div>
                                     <dl>
                                         <dt>1단계</dt>
                                         <dd>관리자가 지정한 최적환경<br>으로 작동중인 윈도우<br>시스템</dd>
                                     </dl>
                                 </li>
                                 <li class="step2">
                                     <div>Step 2</div>
                                     <dl>
                                         <dt>2단계</dt>
                                         <dd>복구 프로그램 설치시<br>타 시스템과 연동을 위한<br>데이터 저장경로는 저장</dd>
                                         <dd>중요 데이터는 C파티션의<br>보호영역이 변화할 수 없게<br>보호</dd>
                                         <dd>새로 쓰여지는 데이터는<br>복구 영역에 기록</dd>
                                     </dl>
                                 </li>
                                 <li class="step3">
                                     <div>Step 3</div>
                                     <dl>
                                         <dt>3단계</dt>
                                         <dd>윈도우손상, 바이러스감염,<br>사용자 실수 등으로 문제가<br>발생한 시스템의 복구영역<br>을 삭제</dd>
                                     </dl>
                                 </li>
                                 <li class="step4">
                                     <div>Step 4</div>
                                     <dl>
                                         <dt>4단계</dt>
                                         <dd>시스템 복구 후 관리자가<br>지정한 최적환경으로 시스템<br>유지</dd>
                                     </dl>
                                 </li>
                             </ul>
                             <h2 class="mt40">개념도</h2>
                             <ul class="procSkill">
                                 <li class="proc1">
                                     <div>
                                         <p>시스템 스냅샷 백업 기술<br>시스템 스냅샷 복구 기술</p>
                                     </div>
                                 </li>
                                 <li class="proc2">
                                     <div>
                                         <p>데이터 스케줄 백업 기술<br>데이터 중복 방지 기술</p>
                                     </div>
                                 </li>
                                 <li class="proc3">
                                     <div>
                                         <p>시스템 보호 기술<br>데이터 보호 기술</p>
                                     </div>
                                 </li>
                             </ul>
                             <div class="re_img"></div>
                             <h2 class="mt50">운영환경</h2>
                             <div class="systemSkill-n">
                                 <h3>
                                     <p></p>시스템 운영 환경 및 최소 요구 사양
                                 </h3>
                                 <div>
                                     <img src="/m/images/sub/img_re_system.gif" alt="">
                                     <p>다양한 OS 지원</p>
                                 </div>
                                 <ul>
                                     <li>Microsoft Windows XP</li>
                                     <li>Intel Pentium 이상</li>
                                     <li>Microsoft Windows Vista</li>
                                     <li>512M RAM 이상</li>
                                     <li>Microsoft Windows 7 (32/64bit)</li>
                                     <li>저장 공간 10GB 이상</li>
                                     <li>Microsoft Windows 8</li>
                                     <li>FAT32, NTFS, GPT 지원</li>
                                     <li class="last">IDE, SCSI, SATA, SSD 지원</li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div id="faqCon">
                     <h3 id="goB">자주 묻는 질문
                     <p>신성리커버리에 관련해 궁금하신 점이 있으신가요? <br>
                     FAQ에서 먼저 문제해결에 대한 도움을 받을 수 있습니다.</p></h3>
                     <ul class="btn">

                         <li><a href="/download/sinsungcns_Smart_Recovery_Manual.pdf" target="_blank">설치매뉴얼 다운로드</a></li>
                         <li><a href="/download/sinsungcns_Smart_Recovery_FAQ.pdf" target="_blank">FAQ 다운로드</a></li>
                     </ul>
                     <ul class="faqUl">
                         <li>
                             <label class="jq_toggle tit">
                                 <span class="tit">Q.</span>
                                 <span class="txt">PC 백업 후 백업된 파일은 어디에 보관 됩니까?</span>
                             </label>
                             <p class="jq_hide" style="">
                                 <span class="tit">A.</span>
                                 <span class="txt">PC 백업을 수행하고 나면 백업 이미지 파일이 만들어집니다. 이 이미지 파일은 OS가 설치된 드라이브(파티션)에 저장됩니다. <br>이런 방식의 장점은 PC 복구 시 백업이미지를 따로 보관할 필요가 없다는 것 입니다.</span>
                             </p>
                         </li>
                         <li class="">
                             <label class="jq_toggle tit">
                                 <span class="tit">Q.</span>
                                 <span class="txt">데이터 백업은 무엇 때문에 필요 합니까?</span>
                             </label>
                             <p class="jq_hide" style="display: none;">
                                 <span class="tit">A.</span>
                                 <span class="txt">정해진 스케쥴에 따라 미리 저장된 시점으로 복구가 가능합니다. <br>
                                     하지만 저장된 시점과 실제 failover가 발생된 시점이 차이가 있을 것이고, 그에 따른 업무로의 복귀에 시간이 소요됩니다.<br>
                                     업무 북귀 시간을 최소화 하기 위해 부가적으로 실시간 데이터 백업을 지원합니다. <br>
                                     (기본 M.S Office 및 한글 관련 확장자 백업지원 / 확장자 추가 가능)
                                 </span>
                             </p>
                         </li>
                         <li>
                             <label class="jq_toggle tit">
                                 <span class="tit">Q.</span>
                                 <span class="txt">본 제품 설치 후 파티션을 변경 해도 됩니까?</span>
                             </label>
                             <p class="jq_hide" style="">
                                 <span class="tit">A.</span>
                                 <span class="txt">절대로 변경하지 마십시오. 만약 파티션 크기 및 개수를 변경하면 해당 파티션 뿐만 아니라 다른 파티 션까지도 모든 데이터를 잃게 됩니다. <br>
                                     따라서 만약 변경할 일이 생기면 우선 중요한 파일을 반드시 외 장 디스크에 백업하고, 그리고 본 제품을 삭제한 후 파티션 변경을 하십시오.</span>
                             </p>
                         </li>
                         <li>
                             <label class="jq_toggle tit">
                                 <span class="tit">Q.</span>
                                 <span class="txt">디스크 공간이 매우 부족합니다. 어떻게 해야 합니까?</span>
                             </label>
                             <p class="jq_hide" style="">
                                 <span class="tit">A.</span>
                                 <span class="txt">디스크 공간이 부족한 이유는 백업된 시점들이 너무 많이 쌓인 경우 입니다.<br>
                                     이런 경우는 ⓐ 부팅 시 응급복구 메뉴로 들어가서 Defrag 메뉴를 선택한 후, [시점리스트]에서 불필요한 시점들을 삭제합니다. <br>(도스 응급복구 메뉴가 텍스트 메뉴인 [시점리스트]가 나타나지 않습니다.) <br>
                                     ⓑ 그리고 Defrag를 수행 합니다. ⓒ 디스크 공간이 원래 부족한 경우는 Windows Update(윈도우 업데이트)를 끄고 사용 하십시오.
                                 </span>
                             </p>
                         </li>
                         <li>
                             <label class="jq_toggle tit">
                                 <span class="tit">Q.</span>
                                 <span class="txt">PC 부팅 속도가 매우 느려졌습니다. 어떻게 해야 합니까?</span>
                             </label>
                             <p class="jq_hide" style="">
                                 <span class="tit">A.</span>
                                 <span class="txt">PC 부팅 속도가 느려지는 이유는 최초 설치 후, 변경된 파일이 많아 시점을 백업하는 데 시간이 오래 걸리기 때문입니다. <br>
                                     신성스마트리커버리는 부팅 시 백업을 하기 위해서 최초 시점과 현재 드라이브 상태를 비교하게 되는 데 <br>이때 파일 변화량이 많으면 비교 시간이 많이 걸리게 됩니다.
                                     이런 경우는 ⓐ먼저 시점을 삭제하여 디스크 공간을 확보합니다. <br>ⓑ그리고 ‘현재 시점 초기화’를 수행하십시오. 또는 프로그램을 삭제 후 재설치 하십시오.<br><br>
                                     ※ 여러가지 방법으로도 해결되지 않으면 현재 시점으로 프로그램을 삭제한 후 재설치 하십시오. <br>
                                     (프로그램 삭제시 절대 최초 시점으로 삭제하지 마십시오. 이렇게 하면 최초 시점으로 복구한 후 삭제하게 되어 현재 시점의 데이터는모두 사라집니다.)</span>
                             </p>
                         </li>
                         <li>
                             <label class="jq_toggle tit">
                                 <span class="tit">Q.</span>
                                 <span class="txt">데이터가 백업되는 저장위치를 지정 할 수 있나요?</span>
                             </label>
                             <p class="jq_hide" style="">
                                 <span class="tit">A.</span>
                                 <span class="txt">결론적으로는 없습니다. 시스템 복구 정보가 저장되는 위치는 운영체제가 설치된 드라이브의 빈공간입니다. <br>
                                     따라서 드라이브의 여유 공간이 부족한 곳에 설치하면 복구로서의 기능을 제대로 사용할 수없습니다.<br>이 때문에 10GB 미만의 여유공간에서는 설치를 막고 옵션에서 저용량 설치를 클릭해야만 설치가 가능합니다. </span>
                             </p>
                         </li>
                         <li>
                             <label class="jq_toggle tit">
                                 <span class="tit">Q.</span>
                                 <span class="txt">빈공간에 데이터가 저장된다고 하였는데, 눈으로 확인 가능 한지 그리고 저장할수록 용량을 많이 차지하지는 않습니까?</span>
                             </label>
                             <p class="jq_hide" style="">
                                 <span class="tit">A.</span>
                                 <span class="txt">빈공간에 저장되는 데이터는 윈도우에서 확인이 불가능 합니다. 간혹 드라이브의 데이터 위치를 파악하는 소프트웨어가 있는데 <br>
                                     그런 소프트웨어에서 복구 영역이 알 수 없는 영역으로 나옵니다. 눈으로 확인이 가능하다 해도 그 영역을 변경하거나 삭제는 불가 합니다. <br>
                                     또 빈공간의 영역이 계속 증가하지는 않습니다. 이유는 스냅샷을 매일 생성하고 마지막에 스냅샷을 지우고 자체 조각모음을 실시하기 때문에
                                     공간이 계속 증가하지는 않습니다. 하지만 사용자의 데이터 이동량이 많을 때는 여유공간이 많이 줄어들 수 있습니다. <br>
                                     이때는 실제 사용하는 데이터를 삭제하고 최적화 버튼을 클릭하고 재부팅 하면
                                     여유공간이 다시 확보되는 것을 눈으로 확인이 가능합니다.<br><br>
                                     ※ 제약사항 : 스냅샷이 빈공간에 저장되므로 윈도우PE와 같은 부팅매체로 부팅하여 임의로 C: 드라이브에 데이터를 복사하게 되면 빈공간에 저장된 스냅샷 정보가 파괴됩니다.
                                     이럴땐 블루스크린 및 복구로서의 기능을 상실하게 됩니다. <br>이점 꼭 주의하시기 바랍니다.</span>
                             </p>
                         </li>
                         <li>
                             <label class="jq_toggle tit">
                                 <span class="tit">Q.</span>
                                 <span class="txt">논리적/물리적 드라이브를 몇 개까지 백업이 가능한가요?</span>
                             </label>
                             <p class="jq_hide" style="">
                                 <span class="tit">A.</span>
                                 <span class="txt">데이터 백업은 윈도우 설치된 드라이브의 백업과 #backup 폴더가 존재하는 드라이브의 백업이 가능합니다. <br>그러니까 C:, D: 드라이브 2개의 드라이브만 백업이 가능합니다.</span>
                             </p>
                         </li>
                     </ul>
                 </div>

             </div>
         </div>
         <div id="A_Footer">
             <!-- #include virtual="/m/_inc/footer.asp" -->
         </div>

     </div>
     </div>
 </body>

 </html>