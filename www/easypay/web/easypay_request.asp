	<!-- #include file = "./easypay_client.asp"     --> <!-- ' library [수정불가] -->
<%
    '/* -------------------------------------------------------------------------- */
    '/* ::: 처리구분 설정                                                          */
    '/* -------------------------------------------------------------------------- */
    TRAN_CD_NOR_PAYMENT    = "00101000"   '// 승인
    TRAN_CD_NOR_MGR        = "00201000"   '// 변경

    '/* -------------------------------------------------------------------------- */
    '/* :::  지불정보 설정                                                         */
    '/* -------------------------------------------------------------------------- */
    GW_URL                 = "testgw.easypay.co.kr"  '// Gateway URL ( test )
   'GW_URL                 = "gw.easypay.co.kr"      '// Gateway URL ( real )
    GW_PORT                = "80"                    '// 포트번호(변경불가)

    '/* -------------------------------------------------------------------------- */
    '/* ::: 지불 데이터 셋업 (업체에 맞게 수정)                                    */
    '/* -------------------------------------------------------------------------- */
    '/* ※ 주의 ※                                                                 */
    '/* cert_file 변수 설정                                                        */
    '/* - pg_cert.pem 파일이 있는 디렉토리의  절대 경로 설정                       */
    '/* log_dir 변수 설정                                                          */
    '/* - log 디렉토리 설정                                                        */
    '/* log_level 변수 설정                                                        */
    '/* - log 레벨 설정                                                            */
    '/*  (1 to 99(높을수록 상세))                                                  */
    '/* -------------------------------------------------------------------------- */
    CERT_FILE              = "http://sinsung.solutionhosting.co.kr/easypay/web/cert/pg_cert.pem"
    LOG_DIR                = "http://sinsung.solutionhosting.co.kr/easypay/web/log"
    LOG_LEVEL                 = 1
    '/* -------------------------------------------------------------------------- */
    '/* ::: 승인요청 정보 설정                                                     */
    '/* -------------------------------------------------------------------------- */
    '//헤더
    tr_cd            = request("EP_tr_cd")            '// [필수]요청구분
    trace_no         = request("EP_trace_no")         '// [필수]추적고유번호
    order_no         = request("EP_order_no")         '// [필수]주문번호
    mall_id          = request("EP_mall_id")          '// [필수]가맹점 MID
    '//공통
    encrypt_data     = request("EP_encrypt_data")     '// [필수]암호화 데이타
    sessionkey       = request("EP_sessionkey")       '// [필수]암호화키

    '/* -------------------------------------------------------------------------- */
    '/* ::: 변경관리 정보 설정                                                     */
    '/* -------------------------------------------------------------------------- */
    mgr_txtype       = request("mgr_txtype")          '// [필수]거래구분
    mgr_subtype      = request("mgr_subtype")         '// [선택]변경세부구분
    org_cno          = request("org_cno")             '// [필수]원거래고유번호
    mgr_amt          = request("mgr_amt")             '// [선택]부분취소/환불요청 금액
    mgr_rem_amt      = request("mgr_rem_amt")         '// [선택]부분취소 잔액
    mgr_bank_cd      = request("mgr_bank_cd")         '// [선택]환불계좌 은행코드
    mgr_account      = request("mgr_account")         '// [선택]환불계좌 번호
    mgr_depositor    = request("mgr_depositor")       '// [선택]환불계좌 예금주명


    '/* -------------------------------------------------------------------------- */
    '/* ::: IP 정보 설정                                                           */
    '/* -------------------------------------------------------------------------- */
    client_ip        = request.ServerVariables( "REMOTE_ADDR" )    '// [필수]결제고객 IP

    '/* -------------------------------------------------------------------------- */
    '/* ::: 요청전문                                                               */
    '/* -------------------------------------------------------------------------- */
    mgr_data    = ""     '// 변경정보


    '/* -------------------------------------------------------------------------- */
    '/* ::: 결제 결과                                                              */
    '/* -------------------------------------------------------------------------- */
    res_cd     = ""
    res_msg    = ""

    '/* -------------------------------------------------------------------------- */
    '/* ::: EasyPayClient 인스턴스 생성 [변경불가 !!].                             */
    '/* -------------------------------------------------------------------------- */
    Set cLib = New EasyPay_Client              ' 전문처리용 Class (library에서 정의됨)
    cLib.InitMsg

    Set Easypay = Server.CreateObject( "ep_cli_com.KICC" )
    'Set Easypay = Server.CreateObject( "ep_cli64_com.KICC64" ) '64bit 경우
    Easypay.EP_CLI_COM__init GW_URL, GW_PORT, CERT_FILE, LOG_DIR, LOG_LEVEL

      if TRAN_CD_NOR_PAYMENT = tr_cd then

    '/* ---------------------------------------------------------------------- */
    '/* ::: 승인요청                                                           */
    '/* ---------------------------------------------------------------------- */
    Easypay.EP_CLI_COM__set_enc_data trace_no, sessionkey, encrypt_data

      elseif TRAN_CD_NOR_MGR = tr_cd then

    '/* ---------------------------------------------------------------------- */
    '/* ::: 변경관리 요청                                                      */
    '/* ---------------------------------------------------------------------- */
    mgr_data = cLib.SetEntry( "mgr_data" )
    mgr_data = cLib.SetValue( "mgr_txtype",    mgr_txtype,    chr(31) )
    mgr_data = cLib.SetValue( "mgr_subtype",   mgr_subtype,   chr(31) )
    mgr_data = cLib.SetValue( "org_cno",       org_cno,       chr(31) )
    mgr_data = cLib.SetValue( "mgr_amt",       mgr_amt,       chr(31) )
    mgr_data = cLib.SetValue( "mgr_rem_amt",   mgr_rem_amt,   chr(31) )
    mgr_data = cLib.SetValue( "mgr_bank_cd",   mgr_bank_cd,   chr(31) )
    mgr_data = cLib.SetValue( "mgr_account",   mgr_account,   chr(31) )
    mgr_data = cLib.SetValue( "mgr_depositor", mgr_depositor, chr(31) )
    mgr_data = cLib.SetValue( "req_ip",        client_ip,     chr(31) )
    mgr_data = cLib.SetDelim( chr(28) )
    cLib.InitMsg

    Easypay.EP_CLI_COM__set_plan_data mgr_data

      end if

'/* -------------------------------------------------------------------------- */
'/* ::: 실행                                                                   */
'/* -------------------------------------------------------------------------- */
    if tr_cd <> "" then
        tx_res_data = Easypay.EP_CLI_COM__proc ( tr_cd, mall_id, client_ip, order_no )
        res_cd      = Easypay.EP_CLI_COM__get_value( "res_cd"          )    '// 응답코드
        res_msg     = Easypay.EP_CLI_COM__get_value( "res_msg"         )    '// 응답메시지
    else
        res_cd  = "M114"
        res_msg = "연동 오류|tr_cd값이 설정되지 않았습니다."
    end if

    '/* -------------------------------------------------------------------------- */
    '/* ::: 결과 처리                                                              */
    '/* -------------------------------------------------------------------------- */
    r_cno              = Easypay.EP_CLI_COM__get_value( "cno"             )     '//PG거래번호
    r_amount           = Easypay.EP_CLI_COM__get_value( "amount"          )     '//총 결제금액
    r_order_no         = Easypay.EP_CLI_COM__get_value( "order_no"        )     '//주문번호
    r_auth_no          = Easypay.EP_CLI_COM__get_value( "auth_no"         )     '//승인번호
    r_tran_date        = Easypay.EP_CLI_COM__get_value( "tran_date"       )     '//승인일시
    r_escrow_yn        = Easypay.EP_CLI_COM__get_value( "escrow_yn"       )     '//에스크로 사용유무
    r_complex_yn       = Easypay.EP_CLI_COM__get_value( "complex_yn"      )     '//복합결제 유무
    r_stat_cd          = Easypay.EP_CLI_COM__get_value( "stat_cd"         )     '//상태코드
    r_stat_msg         = Easypay.EP_CLI_COM__get_value( "stat_msg"        )     '//상태메시지
    r_pay_type         = Easypay.EP_CLI_COM__get_value( "pay_type"        )     '//결제수단
    r_mall_id          = Easypay.EP_CLI_COM__get_value( "mall_id"         )     '//가맹점 Mall ID
    r_card_no          = Easypay.EP_CLI_COM__get_value( "card_no"         )     '//카드번호
    r_issuer_cd        = Easypay.EP_CLI_COM__get_value( "issuer_cd"       )     '//발급사코드
    r_issuer_nm        = Easypay.EP_CLI_COM__get_value( "issuer_nm"       )     '//발급사명
    r_acquirer_cd      = Easypay.EP_CLI_COM__get_value( "acquirer_cd"     )     '//매입사코드
    r_acquirer_nm      = Easypay.EP_CLI_COM__get_value( "acquirer_nm"     )     '//매입사명
    r_install_period   = Easypay.EP_CLI_COM__get_value( "install_period"  )     '//할부개월
    r_noint            = Easypay.EP_CLI_COM__get_value( "noint"           )     '//무이자여부
    r_part_cancel_yn   = Easypay.EP_CLI_COM__get_value( "part_cancel_yn"  )     '//부분취소 가능여부
    r_card_gubun       = Easypay.EP_CLI_COM__get_value( "card_gubun"      )     '//신용카드 종류
    r_card_biz_gubun   = Easypay.EP_CLI_COM__get_value( "card_biz_gubun"  )     '//신용카드 구분
    r_cpon_flag        = Easypay.EP_CLI_COM__get_value( "cpon_flag"       )     '//쿠폰사용유무
    r_bank_cd          = Easypay.EP_CLI_COM__get_value( "bank_cd"         )     '//은행코드
    r_bank_nm          = Easypay.EP_CLI_COM__get_value( "bank_nm"         )     '//은행명
    r_account_no       = Easypay.EP_CLI_COM__get_value( "account_no"      )     '//계좌번호
    r_deposit_nm       = Easypay.EP_CLI_COM__get_value( "deposit_nm"      )     '//입금자명
    r_expire_date      = Easypay.EP_CLI_COM__get_value( "expire_date"     )     '//계좌사용만료일
    r_cash_res_cd      = Easypay.EP_CLI_COM__get_value( "cash_res_cd"     )     '//현금영수증 결과코드
    r_cash_res_msg     = Easypay.EP_CLI_COM__get_value( "cash_res_msg"    )     '//현금영수증 결과메세지
    r_cash_auth_no     = Easypay.EP_CLI_COM__get_value( "cash_auth_no"    )     '//현금영수증 승인번호
    r_cash_tran_date   = Easypay.EP_CLI_COM__get_value( "cash_tran_date"  )     '//현금영수증 승인일시
    r_cash_issue_type  = Easypay.EP_CLI_COM__get_value( "cash_issue_type" )     '//현금영수증발행용도
    r_cash_auth_type   = Easypay.EP_CLI_COM__get_value( "cash_auth_type"  )     '//인증구분
    r_cash_auth_value  = Easypay.EP_CLI_COM__get_value( "cash_auth_value" )     '//인증번호
    r_auth_id          = Easypay.EP_CLI_COM__get_value( "auth_id"         )     '//PhoneID
    r_billid           = Easypay.EP_CLI_COM__get_value( "billid"          )     '//인증번호
    r_mobile_no        = Easypay.EP_CLI_COM__get_value( "mobile_no"       )     '//휴대폰번호
    r_mob_ansim_yn     = Easypay.EP_CLI_COM__get_value( "mob_ansim_yn"    )     '//안심결제 사용유무
    r_ars_no           = Easypay.EP_CLI_COM__get_value( "ars_no"          )     '//전화번호
    r_cp_cd            = Easypay.EP_CLI_COM__get_value( "cp_cd"           )     '//포인트사/쿠폰사
    r_pnt_auth_no      = Easypay.EP_CLI_COM__get_value( "pnt_auth_no"     )     '//포인트승인번호
    r_pnt_tran_date    = Easypay.EP_CLI_COM__get_value( "pnt_tran_date"   )     '//포인트승인일시
    r_used_pnt         = Easypay.EP_CLI_COM__get_value( "used_pnt"        )     '//사용포인트
    r_remain_pnt       = Easypay.EP_CLI_COM__get_value( "remain_pnt"      )     '//잔여한도
    r_pay_pnt          = Easypay.EP_CLI_COM__get_value( "pay_pnt"         )     '//할인/발생포인트
    r_accrue_pnt       = Easypay.EP_CLI_COM__get_value( "accrue_pnt"      )     '//누적포인트
    r_deduct_pnt       = Easypay.EP_CLI_COM__get_value( "deduct_pnt"      )     '//총차감 포인트
    r_payback_pnt      = Easypay.EP_CLI_COM__get_value( "payback_pnt"     )    '//payback 포인트
    r_cpon_auth_no     = Easypay.EP_CLI_COM__get_value( "cpon_auth_no"    )     '//쿠폰승인번호
    r_cpon_tran_date   = Easypay.EP_CLI_COM__get_value( "cpon_tran_date"  )     '//쿠폰승인일시
    r_cpon_no          = Easypay.EP_CLI_COM__get_value( "cpon_no"         )     '//쿠폰번호
    r_remain_cpon      = Easypay.EP_CLI_COM__get_value( "remain_cpon"     )     '//쿠폰잔액
    r_used_cpon        = Easypay.EP_CLI_COM__get_value( "used_cpon"       )     '//쿠폰 사용금액
    r_rem_amt          = Easypay.EP_CLI_COM__get_value( "rem_amt"         )     '//잔액
    r_bk_pay_yn        = Easypay.EP_CLI_COM__get_value( "bk_pay_yn"       )     '//장바구니 결제여부
    r_canc_acq_date    = Easypay.EP_CLI_COM__get_value( "canc_acq_date"   )     '//매입취소일시
    r_canc_date        = Easypay.EP_CLI_COM__get_value( "canc_date"       )     '//취소일시
    r_refund_date      = Easypay.EP_CLI_COM__get_value( "refund_date"     )     '//환불예정일시

'/* -------------------------------------------------------------------------- */
'/* ::: 가맹점 DB 처리                                                         */
'/* -------------------------------------------------------------------------- */
'/* 응답코드(res_cd)가 "0000" 이면 정상승인 입니다.                            */
'/* r_amount가 주문DB의 금액과 다를 시 반드시 취소 요청을 하시기 바랍니다.     */
'/* DB 처리 실패 시 취소 처리를 해주시기 바랍니다.                             */
'/* -------------------------------------------------------------------------- */

if res_cd = "0000" then

    bDBProc = "true"     '// DB처리 성공 시 "true", 실패 시 "false"

    if bDBProc = "false" then

        '// 승인요청이 실패 시 아래 실행
        if TRAN_CD_NOR_PAYMENT = tr_cd then

            tr_cd = TRAN_CD_NOR_MGR
            cLib.InitMsg

            mgr_data = cLib.SetEntry( "mgr_data" )

            if  r_escrow_yn <> "Y" then
                mgr_data = cLib.SetValue( "mgr_txtype",    "40",   chr(31) )
            else
                mgr_data = cLib.SetValue( "mgr_txtype",    "61",   chr(31) )
                mgr_data = cLib.SetValue( "mgr_subtype",   "ES02", chr(31) )
            end if

            mgr_data = cLib.SetValue( "org_cno",       r_cno,    chr(31) )
            mgr_data = cLib.SetValue( "order_no",      order_no, chr(31) )
            mgr_data = cLib.SetValue( "req_ip",        client_ip, chr(31) )
            mgr_data = cLib.SetValue( "req_id",        "MALL_R_TRANS", chr(31) )
            mgr_data = cLib.SetValue( "mgr_msg",       "DB 처리 실패로 망취소", chr(31) )

            mgr_data = cLib.SetDelim( chr(28) )
            cLib.InitMsg

            Easypay.EP_CLI_COM__init GW_URL, GW_PORT, CERT_FILE, LOG_DIR, LOG_LEVEL
            Easypay.EP_CLI_COM__set_plan_data mgr_data

            tx_res_data = Easypay.EP_CLI_COM__proc ( tr_cd, mall_id, client_ip, order_no )
            res_cd      = Easypay.EP_CLI_COM__get_value( "res_cd"          )    '// 응답코드
            res_msg     = Easypay.EP_CLI_COM__get_value( "res_msg"         )    '// 응답메시지
            r_cno       = Easypay.EP_CLI_COM__get_value( "cno"             )    '// PG거래번호
            r_canc_date = Easypay.EP_CLI_COM__get_value( "canc_date"       )    '//취소일시

        end if
    end if

 end if

'/* -------------------------------------------------------------------------- */
'/* ::: Library Cleanup                                                        */
'/* -------------------------------------------------------------------------- */
Easypay.EP_CLI_COM__cleanup
set Easypay = nothing
set cLib = nothing

%>
<html>
<meta name="robots" content="noindex, nofollow">
<script type="text/javascript">
    function f_submit(){
        document.frm.submit()
    }
</script>

<body onload="f_submit()">
<form name="frm" method="post" action="./result.asp">
    <input type="hidden" id="res_cd"           name="res_cd"          value="<%=res_cd%>">              <!-- 결과코드 //-->
    <input type="hidden" id="res_msg"          name="res_msg"         value="<%=res_msg%>">             <!-- 결과메시지 //-->
    <input type="hidden" id="cno"              name="cno"             value="<%=r_cno%>">               <!-- PG거래번호 //-->
    <input type="hidden" id="amount"           name="amount"          value="<%=r_amount%>">            <!-- 총 결제금액 //-->
    <input type="hidden" id="order_no"         name="order_no"        value="<%=r_order_no%>">          <!-- 주문번호 //-->
    <input type="hidden" id="auth_no"          name="auth_no"         value="<%=r_auth_no%>">           <!-- 승인번호 //-->
    <input type="hidden" id="tran_date"        name="tran_date"       value="<%=r_tran_date%>">         <!-- 승인일시 //-->
    <input type="hidden" id="escrow_yn"        name="escrow_yn"       value="<%=r_escrow_yn%>">         <!-- 에스크로 사용유무 //-->
    <input type="hidden" id="complex_yn"       name="complex_yn"      value="<%=r_complex_yn%>">        <!-- 복합결제 유무 //-->
    <input type="hidden" id="stat_cd"          name="stat_cd"         value="<%=r_stat_cd%>">           <!-- 상태코드 //-->
    <input type="hidden" id="stat_msg"         name="stat_msg"        value="<%=r_stat_msg%>">          <!-- 상태메시지 //-->
    <input type="hidden" id="pay_type"         name="pay_type"        value="<%=r_pay_type%>">          <!-- 결제수단 //-->
    <input type="hidden" id="mall_id"          name="mall_id"         value="<%=r_mall_id%>">           <!-- 가맹점 Mall ID //-->
    <input type="hidden" id="card_no"          name="card_no"         value="<%=r_card_no%>">           <!-- 카드번호 //-->
    <input type="hidden" id="issuer_cd"        name="issuer_cd"       value="<%=r_issuer_cd%>">         <!-- 발급사코드 //-->
    <input type="hidden" id="issuer_nm"        name="issuer_nm"       value="<%=r_issuer_nm%>">         <!-- 발급사명 //-->
    <input type="hidden" id="acquirer_cd"      name="acquirer_cd"     value="<%=r_acquirer_cd%>">       <!-- 매입사코드 //-->
    <input type="hidden" id="acquirer_nm"      name="acquirer_nm"     value="<%=r_acquirer_nm%>">       <!-- 매입사명 //-->
    <input type="hidden" id="install_period"   name="install_period"  value="<%=r_install_period%>">    <!-- 할부개월 //-->
    <input type="hidden" id="noint"            name="noint"           value="<%=r_noint%>">             <!-- 무이자여부 //-->
    <input type="hidden" id="part_cancel_yn"   name="part_cancel_yn"  value="<%=r_part_cancel_yn%>">    <!-- 부분취소 가능여부 //-->
    <input type="hidden" id="card_gubun"       name="card_gubun"      value="<%=r_card_gubun%>">        <!-- 신용카드 종류 //-->
    <input type="hidden" id="card_biz_gubun"   name="card_biz_gubun"  value="<%=r_card_biz_gubun%>">    <!-- 신용카드 구분 //-->
    <input type="hidden" id="cpon_flag"        name="cpon_flag"       value="<%=r_cpon_flag%>">         <!-- 쿠폰사용유무 //-->
    <input type="hidden" id="bank_cd"          name="bank_cd"         value="<%=r_bank_cd%>">           <!-- 은행코드 //-->
    <input type="hidden" id="bank_nm"          name="bank_nm"         value="<%=r_bank_nm%>">           <!-- 은행명 //-->
    <input type="hidden" id="account_no"       name="account_no"      value="<%=r_account_no%>">        <!-- 계좌번호 //-->
    <input type="hidden" id="deposit_nm"       name="deposit_nm"      value="<%=r_deposit_nm%>">        <!-- 입금자명 //-->
    <input type="hidden" id="expire_date"      name="expire_date"     value="<%=r_expire_date%>">       <!-- 계좌사용만료일 //-->
    <input type="hidden" id="cash_res_cd"      name="cash_res_cd"     value="<%=r_cash_res_cd%>">       <!-- 현금영수증 결과코드 //-->
    <input type="hidden" id="cash_res_msg"     name="cash_res_msg"    value="<%=r_cash_res_msg%>">      <!-- 현금영수증 결과메세지 //-->
    <input type="hidden" id="cash_auth_no"     name="cash_auth_no"    value="<%=r_cash_auth_no%>">      <!-- 현금영수증 승인번호 //-->
    <input type="hidden" id="cash_tran_date"   name="cash_tran_date"  value="<%=r_cash_tran_date%>">    <!-- 현금영수증 승인일시 //-->
    <input type="hidden" id="cash_issue_type"  name="cash_issue_type" value="<%=r_cash_issue_type%>">   <!-- 현금영수증발행용도 //-->
    <input type="hidden" id="cash_auth_type"   name="cash_auth_type"  value="<%=r_cash_auth_type%>">    <!-- 인증구분 //-->
    <input type="hidden" id="cash_auth_value"  name="cash_auth_value" value="<%=r_cash_auth_value%>">   <!-- 인증번호 //-->
    <input type="hidden" id="auth_id"          name="auth_id"         value="<%=r_auth_id%>">           <!-- PhoneID //-->
    <input type="hidden" id="billid"           name="billid"          value="<%=r_billid%>">            <!-- 인증번호 //-->
    <input type="hidden" id="mobile_no"        name="mobile_no"       value="<%=r_mobile_no%>">         <!-- 휴대폰번호 //-->
    <input type="hidden" id="mob_ansim_yn"     name="mob_ansim_yn"    value="<%=r_mob_ansim_yn%>">      <!-- 안심결제 사용유무 //-->
    <input type="hidden" id="ars_no"           name="ars_no"          value="<%=r_ars_no%>">            <!-- 전화번호 //-->
    <input type="hidden" id="cp_cd"            name="cp_cd"           value="<%=r_cp_cd%>">             <!-- 포인트사/쿠폰사 //-->
    <input type="hidden" id="cpon_auth_no"     name="cpon_auth_no"    value="<%=r_cpon_auth_no%>">      <!-- 쿠폰승인번호 //-->
    <input type="hidden" id="cpon_tran_date"   name="cpon_tran_date"  value="<%=r_cpon_tran_date%>">    <!-- 쿠폰승인일시 //-->
    <input type="hidden" id="cpon_no"          name="cpon_no"         value="<%=r_cpon_no%>">           <!-- 쿠폰번호 //-->
    <input type="hidden" id="remain_cpon"      name="remain_cpon"     value="<%=r_remain_cpon%>">       <!-- 쿠폰잔액 //-->
    <input type="hidden" id="used_cpon"        name="used_cpon"       value="<%=r_used_cpon%>">         <!-- 쿠폰 사용금액 //-->
    <input type="hidden" id="rem_amt"          name="rem_amt"         value="<%=r_rem_amt%>">           <!-- 잔액 //-->
    <input type="hidden" id="bk_pay_yn"        name="bk_pay_yn"       value="<%=r_bk_pay_yn%>">         <!-- 장바구니 결제여부 //-->
    <input type="hidden" id="canc_acq_date"    name="canc_acq_date"   value="<%=r_canc_acq_date%>">     <!-- 매입취소일시 //-->
    <input type="hidden" id="canc_date"        name="canc_date"       value="<%=r_canc_date%>">         <!-- 취소일시 //-->
    <input type="hidden" id="refund_date"      name="refund_date"     value="<%=r_refund_date%>">       <!-- 환불예정일시 //-->
</form>
</body>
</html>