<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="robots" content="noindex, nofollow" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
    function change(url)
    {
        document.getElementById("main").src = url;
    }

    function resizeHeight(fr)
    {
        var new_height = window.frames[0].document.body.scrollHeight;
        if(new_height < 500)
        {
            fr.height = 500;
        }
        else
        {
            fr.height=new_height;
        }
    }

    function f_submit()
    {
        var frm = document.frm;
        frm.target = "hiddenifr";
        frm.submit();
    }
</script>
<title>신성CNS 결제창</title>
</head>
<body onload="f_submit();">
    <form name="frm" method="post" action="https://testpg.easypay.co.kr/webpay/MainAction.do"> <!-- 테스트 -->
    <!--form name="frm" method="post" action="https://pg.easypay.co.kr/webpay/MainAction.do"--> <!-- 운영 -->
        <input type="hidden" id="EP_mall_id"           name="EP_mall_id"            value="<%=request("EP_mall_id") %>" />
        <input type="hidden" id="EP_mall_nm"           name="EP_mall_nm"            value="<%=request("EP_mall_nm") %>" />
        <input type="hidden" id="EP_order_no"          name="EP_order_no"           value="<%=request("EP_order_no") %>" />
        <input type="hidden" id="EP_pay_type"          name="EP_pay_type"           value="<%=request("EP_pay_type") %>" />
        <input type="hidden" id="EP_currency"          name="EP_currency"           value="<%=request("EP_currency") %>" />
        <input type="hidden" id="EP_product_nm"        name="EP_product_nm"         value="<%=request("EP_product_nm") %>" />
        <input type="hidden" id="EP_product_amt"       name="EP_product_amt"        value="<%=request("EP_product_amt") %>" />
        <input type="hidden" id="EP_return_url"        name="EP_return_url"         value="<%=request("EP_return_url") %>" />
        <input type="hidden" id="EP_ci_url"            name="EP_ci_url"             value="<%=request("EP_ci_url") %>" />
        <input type="hidden" id="EP_lang_flag"         name="EP_lang_flag"          value="<%=request("EP_lang_flag") %>" />
        <input type="hidden" id="EP_charset"           name="EP_charset"            value="<%=request("EP_charset") %>" />
        <input type="hidden" id="EP_user_id"           name="EP_user_id"            value="<%=request("EP_user_id") %>" />
        <input type="hidden" id="EP_memb_user_no"      name="EP_memb_user_no"       value="<%=request("EP_memb_user_no") %>" />
        <input type="hidden" id="EP_user_nm"           name="EP_user_nm"            value="<%=request("EP_user_nm") %>" />
        <input type="hidden" id="EP_user_mail"         name="EP_user_mail"          value="<%=request("EP_user_mail") %>" />
        <input type="hidden" id="EP_user_phone1"       name="EP_user_phone1"        value="<%=request("EP_user_phone1") %>" />
        <input type="hidden" id="EP_user_phone2"       name="EP_user_phone2"        value="<%=request("EP_user_phone2") %>" />
        <input type="hidden" id="EP_user_addr"         name="EP_user_addr"          value="<%=request("EP_user_addr") %>" />
        <input type="hidden" id="EP_user_define1"      name="EP_user_define1"       value="<%=request("EP_user_define1") %>" />
        <input type="hidden" id="EP_user_define2"      name="EP_user_define2"       value="<%=request("EP_user_define2") %>" />
        <input type="hidden" id="EP_user_define3"      name="EP_user_define3"       value="<%=request("EP_user_define3") %>" />
        <input type="hidden" id="EP_user_define4"      name="EP_user_define4"       value="<%=request("EP_user_define4") %>" />
        <input type="hidden" id="EP_user_define5"      name="EP_user_define5"       value="<%=request("EP_user_define5") %>" />
        <input type="hidden" id="EP_user_define6"      name="EP_user_define6"       value="<%=request("EP_user_define6") %>" />
        <input type="hidden" id="EP_product_type"      name="EP_product_type"       value="<%=request("EP_product_type") %>" />
        <input type="hidden" id="EP_product_expr"      name="EP_product_expr"       value="<%=request("EP_product_expr") %>" />
        <input type="hidden" id="EP_usedcard_code"     name="EP_usedcard_code"      value="<%=request("EP_usedcard_code") %>" />
        <input type="hidden" id="EP_quota"             name="EP_quota"              value="<%=request("EP_quota") %>" />
        <input type="hidden" id="EP_os_cert_flag"      name="EP_os_cert_flag"       value="<%=request("EP_os_cert_flag") %>" />
        <input type="hidden" id="EP_noinst_flag"       name="EP_noinst_flag"        value="<%=request("EP_noinst_flag") %>" />
        <input type="hidden" id="EP_noinst_term"       name="EP_noinst_term"        value="<%=request("EP_noinst_term") %>" />
        <input type="hidden" id="EP_set_point_card_yn" name="EP_set_point_card_yn"  value="<%=request("EP_set_point_card_yn") %>" />
        <input type="hidden" id="EP_point_card"        name="EP_point_card"         value="<%=request("EP_point_card") %>" />
        <input type="hidden" id="EP_join_cd"           name="EP_join_cd"            value="<%=request("EP_join_cd") %>" />
        <input type="hidden" id="EP_kmotion_useyn"     name="EP_kmotion_useyn"      value="<%=request("EP_kmotion_useyn") %>" />
        <input type="hidden" id="EP_vacct_bank"        name="EP_vacct_bank"         value="<%=request("EP_vacct_bank") %>" />
        <input type="hidden" id="EP_vacct_end_date"    name="EP_vacct_end_date"     value="<%=request("EP_vacct_end_date") %>" />
        <input type="hidden" id="EP_vacct_end_time"    name="EP_vacct_end_time"     value="<%=request("EP_vacct_end_time") %>" />
        <input type="hidden" id="EP_prepaid_cp"        name="EP_prepaid_cp"         value="<%=request("EP_prepaid_cp") %>"     />
        <input type="hidden" id="EP_disp_cash_yn"      name="EP_disp_cash_yn"       value="<%=request("EP_disp_cash_yn") %>" />
        <input type="hidden" id="EP_cert_type"         name="EP_cert_type"          value="<%=request("EP_cert_type") %>" />

    </form>
    <!--iframe id="hiddenifr" name="hiddenifr" width="100%" frameborder="0" src="./iframe_req.asp" scrolling="0" onload="resizeHeight(this)" /-->
</body>
</html>
