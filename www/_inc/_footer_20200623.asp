	</div>
	<div id="A_Footer">
		<div class="ft_logo"><img src="/images/common/logo_ft.png" alt="SINSUNGCNS CNS" /></div>
		<ul class="sns_links">
			<li><a href="#" target="_blank">카카오톡</a></li>
			<li><a href="#" target="_blank">페이스북</a></li>
			<li><a href="#" target="_blank">유튜브</a></li>
			<li><a href="#" target="_blank">블로그</a></li>
		</ul>
		<div class="ft_links">
			<ul class="inner">
				<li>
					<p class="tit">제품구매</p>
					<ul>
						<li></li>
					</ul>
				</li>
				<li>
					<p class="tit">솔루션</p>
					<ul>
						<li><a href="/sub/solution/recovery.asp">신성 리커버리</a></li>
						<li><a href="/sub/solution/backup.asp">백업 및 복구</a></li>
						<li><a href="/sub/solution/server.asp">서버이중화</a></li>
						<li><a href="/sub/solution/virtual.asp">가상화</a></li>
						<li><a href="/sub/solution/document.asp">문서중앙화</a></li>
					</ul>
				</li>
				<li>
					<p class="tit">화상회의</p>
					<ul>
						<li><a href="/sub/conference/conference.asp">화상회의 H/W솔루션</a></li>
						<li><a href="/sub/conference/cf_sw.asp">화상회의 S/W</a></li>
					</ul>
				</li>
				<li>
					<p class="tit">고객서비스</p>
					<ul>
						<li><a href="/sub/support/drivers7.asp">신성케어 7drivers</a></li>
						<li><a href="/sub/support/service.asp">맞춤형 세팅 서비스</a></li>
						<li><a href="/sub/support/it_data2.asp">기술자료 설치 매뉴얼</a></li>
						<li><a href="/sub/support/talk.asp">카톡 1:1 서비스</a></li>
						<li><a href="/sub/support/onlineReceive.asp">기술지원문의</a></li>
						<li><a href="/sub/support/demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
					</ul>
				</li>
				<li>
					<p class="tit">우리의 경험</p>
					<ul>
						<li><a href="/sub/experience/business.asp">사업소개</a></li>
						<li><a href="/sub/experience/it.asp">IT 구매제안</a></li>
						<li><a href="/sub/experience/srm.asp">신성 SRM</a></li>
						<li><a href="/sub/experience/construction_case.asp">구축사례</a></li>
						<li><a href="/sub/experience/cooperative.asp">협력사</a></li>
					</ul>
				</li>
				<li>
					<p class="tit">인재경영</p>
					<ul>
						<li><a href="/sub/company/plan.asp">우리성장</a></li>
						<li><a href="/sub/company/culture.asp">우리문화</a></li>
						<li><a href="/sub/company/video.asp">동영상</a></li>
						<li><a href="/sub/company/person.asp">인재상</a></li>
						<li><a href="/sub/company/welfare.asp">복지제도</a></li>
						<li><a href="/sub/company/recruit.asp">인재채용</a></li>
					</ul>
				</li>
				<li>
					<p class="tit">회사소개</p>
					<ul>
						<li><a href="/sub/company/management.asp">경영철학</a></li>
						<li><a href="/sub/company/growth.asp">회사성장</a></li>
						<li><a href="/sub/company/greeting.asp">대표 인사말</a></li>
						<li><a href="/sub/company/team.asp">팀을 만나다</a></li>
						<li><a href="/sub/company/history.asp">연혁</a></li>
						<li><a href="/sub/company/prize.asp">수상실적</a></li>
						<li><a href="/sub/company/notice.asp">공지사항</a></li>
						<li><a href="/sub/company/map.asp">찾아오시는 길</a></li>
					</ul>
				</li>
				<li>
					<p class="tit">견적문의</p>
					<ul>
						<li><a href="/sub/b2b/estimate_list.asp">맞춤사양견적</a></li>
						<li><a href="/sub/b2b/onlineReceive2.asp">일반견적</a></li>
						<li><a href="/sub/b2b/onlineReceive_solution.asp">솔루션상담견적</a></li>
						<li><a href="/sub/b2b/promotion.asp">프로모션</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="ft_txt">
			<ul>
				<li>08389 서울시 구로구 디지털로 272 한신 IT타워 5F</li>
				<li>Tel : 02-867-3007</li>
				<li>Fax : 02-867-2328</li>
				<li>Email : sklee@sinsungcns.com</li>
			</ul>
			<ul>
				<li>사업자 등록번호 : 119-86-18434</li>
				<li>통신판매업 신고번호 : 제 2016-서울구로-0842호</li>
				<li>개인정보책임자 : 박세화</li>
			</ul>
			<p>홈페이지에 게제된 모든 글과 이미지 등의 정보는 저작권법에 따라 보호를 받는 ㈜신성씨앤에스의 저작물이므로 무단 전재나 복제를 금합니다.</p>
			<p class="copy">COPYRIGHT (C)SINSUNGCNS CORP. ALL RIGHTS RESERVED.</p>
		</div>
		<button type="button" class="btn_top">Top</button>
		<span id="underLayer0" class=""></span>
	</div>

</div>
</div>
</body>
</html>