<%
fieldvalue1 	=  SQL_Injection(Trim(Request("fieldvalue1")))
%>
<div class="util_area">
	<div class="inner02">
		<ul>
		<% If Request.Cookies("U_ID") = "" Then %>
			<li><a href="/sub/member/login.asp">로그인</a></li>
		<% Else %>
			<li><a href="/sub/member/logout.asp">로그아웃</a></li>
			<li><a href="/sub/member/mypage.asp">회원정보수정</a></li>
		<% End If %>
			<li><a href="/sub/experience/construction_case.asp">구축사례</a></li>
			<li class="c_bk"><a href="/sub/support/drivers7.asp">신성케어 멤버십</a></li>
			<li class="c_bk"><p>구매상담 02-867-3007</p></li>
		</ul>
	</div>
</div>
<div class="logo_area">
	<div class="inner02">
		<h1 class="logo">
			<a href="/">SINSUNG CNS</a>
		</h1>
		<form name="search_form" class="search_form" method="post" action="/sub/b2b/product_list.asp">
			<input type="text" name="fieldvalue1" placeholder="검색어를 입력해주세요." value="<%=fieldvalue1%>">
			<button class="btn_search" type="button" onclick="if(document.search_form.fieldvalue1.value == '') {alert('제품명을 입력하세요');return false;} else {document.search_form.submit(); }">검색</button>
		</form>
	</div>
</div>
<%
	Call dbopen

	sql = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 8) = '00000000' ORDER BY c_sunse"
	Set rs = dbconn.execute(sql)
%>
<div class="gnb_area">
	<div class="inner02">
		<ul class="gnb">
			<li class="gnb01">
				<a href="/sub/b2b/product_list.asp">제품구매</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li class="lnb_b2b">
							<a href="/sub/b2b/product_list.asp">B2B 제품</a>
							<div class="b2b_prd_menu">
								<div class="b2b_inner">
									<ul class="depth01">
<%
	If Not(rs.bof Or rs.eof) Then
		Do While Not rs.eof
%>
										<li>
											<p class="depth01_tit"><%=rs("c_name")%></p>
											<ul class="depth02">
<%
			sql2 = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 6) = '000000' AND c_code LIKE '" & Left(rs("c_code"), 2) & "%' AND SUBSTRING(c_code, 4, 1) <> '0' ORDER BY c_sunse"
			Set rs2 = dbconn.execute(sql2)

			If Not(rs2.bof Or rs2.eof) Then
				Do While Not rs2.eof

%>
												<li>
													<a href="/sub/b2b/product01.asp?c=<%=rs2("c_code")%>"><%=rs2("c_name")%></a>
													<ul class="depth03">
<%
					sql3 = "SELECT * FROM CATEGORY WHERE RIGHT(c_code, 4) = '0000' AND c_code LIKE '" & Left(rs2("c_code"), 4) & "%' AND SUBSTRING(c_code, 6, 1) <> '0' ORDER BY c_sunse"
					Set rs3 = dbconn.execute(sql3)

					If Not(rs3.bof Or rs3.eof) Then
						Do While Not rs3.eof
%>
														<li><a href="/sub/b2b/product_list.asp?c=<%=rs3("c_code")%>"><%=rs3("c_name")%></a></li>
<%
							rs3.MoveNext
						Loop
					End If

					rs3.Close
					Set rs3 = Nothing
%>
													</ul>
												</li>
<%
					rs2.MoveNext
				Loop
			End If

			rs2.Close
			Set rs2 = Nothing
%>
											</ul>
										</li>
<%
			rs.MoveNext
		Loop
	End If

	rs.Close
%>
									</ul>
									<div class="brand">
										<ul>
											<li><img src="/images/common/brand_logo01.png" alt="NAVER" /></li>
											<li><img src="/images/common/brand_logo02.png" alt="Daum" /></li>
											<li><img src="/images/common/brand_logo02.png" alt="kakao" /></li>
											<li><img src="/images/common/brand_logo04.png" alt="Melon" /></li>
											<li><img src="/images/common/brand_logo05.png" alt="genie" /></li>
											<li><img src="/images/common/brand_logo01.png" alt="NAVER" /></li>
											<li><img src="/images/common/brand_logo02.png" alt="Daum" /></li>
											<li><img src="/images/common/brand_logo02.png" alt="kakao" /></li>
											<li><img src="/images/common/brand_logo04.png" alt="Melon" /></li>
											<li><img src="/images/common/brand_logo05.png" alt="genie" /></li>
											<li><img src="/images/common/brand_logo01.png" alt="NAVER" /></li>
											<li><img src="/images/common/brand_logo02.png" alt="Daum" /></li>
											<li><img src="/images/common/brand_logo02.png" alt="kakao" /></li>
											<li><img src="/images/common/brand_logo04.png" alt="Melon" /></li>
											<li><img src="/images/common/brand_logo05.png" alt="genie" /></li>
										</ul>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</li>
			<li class="gnb02">
				<a href="/sub/solution/recovery.asp">솔루션</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="/sub/solution/recovery.asp">신성 리커버리</a></li>
						<li><a href="/sub/solution/backup.asp">백업 및 복구</a></li>
						<li><a href="/sub/solution/server.asp">서버이중화</a></li>
						<li><a href="/sub/solution/virtual.asp">가상화</a></li>
						<li><a href="/sub/solution/document.asp">문서중앙화</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb03">
				<a href="/sub/conference/conference.asp">화상회의</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="/sub/conference/conference.asp">화상 회의 H/W 솔루션</a></li>
						<li><a href="/sub/conference/cf_sw.asp">화상 회의 S/W</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb04">
				<a href="/sub/support/drivers7.asp">고객서비스</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="/sub/support/drivers7.asp">신성케어 7drivers</a></li>
						<li><a href="/sub/support/service.asp">맞춤형 세팅 서비스</a></li>
						<li><a href="/sub/support/it_data2.asp">기술자료설치매뉴얼</a></li>
						<li><a href="/sub/support/talk.asp">카톡1:1서비스</a></li>
						<li><a href="/sub/support/onlineReceive.asp">기술지원 문의</a></li>
						<li><a href="/sub/support/demo.asp">데모장비 신청 및 테스트 의뢰</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb05">
				<a href="/sub/experience/business.asp">우리의 경험</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="/sub/experience/business.asp">사업소개</a></li>
						<li><a href="/sub/experience/it.asp">IT구매제안</a></li>
						<li><a href="/sub/experience/srm.asp">신성SRM</a></li>
						<li><a href="/sub/experience/construction_case.asp">구축사례</a></li>
						<li><a href="/sub/experience/cooperative.asp">협력사</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb06">
				<a href="/sub/company/plan.asp">인재경영</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="/sub/company/plan.asp">우리성장</a></li>
						<li><a href="/sub/company/culture.asp">우리문화</a></li>
						<li><a href="/sub/company/video.asp">동영상</a></li>
						<li><a href="/sub/company/person.asp">인재상</a></li>
						<li><a href="/sub/company/welfare.asp">복지제도</a></li>
						<li><a href="/sub/company/recruit.asp">인재채용</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb07">
				<a href="/sub/company/management.asp">회사소개</a>
				<div class="bg_blue">
					<ul class="lnb">
						<li><a href="/sub/company/management.asp">경영철학</a></li>
						<li><a href="/sub/company/growth.asp">회사성장</a></li>
						<li><a href="/sub/company/greeting.asp">대표 인사말</a></li>
						<li><a href="/sub/company/team.asp">팀을 만나다</a></li>
						<li><a href="/sub/company/history.asp">연혁</a></li>
						<li><a href="/sub/company/prize.asp">수상실적</a></li>
						<li><a href="/sub/company/notice.asp">공지사항</a></li>
						<li><a href="/sub/company/map.asp">찾아오시는 길</a></li>
					</ul>
				</div>
			</li>
			<li class="gnb08">
				<a href="/sub/support/estimate_list.asp">견적문의</a>
				<div class="bg_blue">
					<ul class="lnb">
<!--						<li><a href="/sub/support/estimate_list.asp">맞춤사양견적</a></li>-->
						<li><a href="/sub/support/onlineReceive2.asp">일반견적요청</a></li>
						<li><a href="/sub/support/onlineReceive_solution.asp">솔루션 상담요청</a></li>
						<li><a href="/sub/support/promotion.asp">프로모션</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</div>